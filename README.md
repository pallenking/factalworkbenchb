#  Factal Workbench




			FactalWorkbench README
UPDATED: 2017 0314


   FactalWorkbench is a Mac OS X Application designed to define, visulaize,
and simulate networks created by the HaveNWant Schemata.  HaveNWant
Networks are currently defined in code, viewed as 3D objects in OpenGL, and
simulated as it operates in an environment. More about both the HaveNWant
Schemata and the FactalWorkbench at http://brain-gears.blogspot.com.

   FactalWorkbench is development code, designed for research. What is here
has not yet undergone alpha or beta testing. It frequently crashes. However, for
the motivated, there are several ways that it can still be used:

   ==== 1. FactalWorkbench.dmg -- A pre-compiled version of FactalWorkbench is
available as a .dmg . It is known to work on Macintosh OS X El
Capitan, Version 10.11.6, and has many interesting Networks
predefined. It should be easy to install on a Mac.

   Once installed into /Applications, it can be:

	a) URL -- activated by issuing an URL like:
	   factalWorkbench://A.%20Small%20Machines/A.%20Umbrella from
	   Sifari or any other application (e.g. Keynote). (Note that
	   space (' ') must sometimes be encoded as %20 for this to work.)

	b) Scene menue -- Start up FactalWorkbench from the desktop and use the Scene menu
	   to accesses about 20 interesting HaveNWant Networks, that have
	   been previously defined.  For instance, try Scene|

	c) Regress menue -- allows the next Menue (M) or Regression (R) test
	   to be quickly executed. M is for demoable networks, R for tests.

   Try typing 's' to go on to the next pattern, or using a GUI if it
   is included.

   The mouse rotator for the
   	   Left Drag Up/Down		-- shift image in Y
	   Left Drag Left/Right		-- spin image about Y axis
	   Right Drag Up/Down		-- view more from top/bottom
	   Wheel Up/Down 			-- Zoom Out/In
	   ...

     === 2. FactalWorkbench.xcodeproj -- Source versions is known to work on
Xcode 7.3.1 (7D1014).  Currently, a Debug image is created.  With the
sources in Xcode, there comes the ability to specify your own
HaveNWant networks, and connect them with your own environments.  I
would be happy to consult on such projects if needed.

	To use, select the network to run in Dna01.mm:
		by prefixing one macro (e.g. r(), R(), m(), or M()) with an 'x'
			changing it to xr(), xR(), xm(), or xM().
		Only one line in this file may have a prefixed 'x'.
		If no network is 'x'ed, no tests will run


HELP: need ENVIRONMENTAL VARIABLES defined for
	1. Xcode requires that the home directory contain a symbolic link to factal workbench
		cd; ln -s src/factalworkbenchb/.lldbinit .  This is a bug in Xcode, not to look in
		project for this.  https://github.com/alloy/lldb-is-it-not doesn't help
	2. FactalWorkbench.mm: variable @"dataDirectory" is a hack.
	3. Supertux.mm: supertux2executableDir last worked in 2017, needs help


Suggestions for new or improved features and feedback,
	both at the code and project goals levels.
	
		Allen King
		allen@a-king.us
		http://brain-gears.blogspot.com

180303 UPDATE:

