# Useful Debug Commands:
define sp
	   p [self print]
	   end

define sp0
	   p [self print0]
	   end

#define po			print-object
define po
	   expr _NSPrintForDebugger
	   end

#define tl			p this->loc
#define tc			p this->printComposition(99,0)
#define tn			p this->name
#define tt			p globalTime
#define pc			p print_contents()
#define rundone		p rundone()
#define gdbinit		source ~/.gdbinit
#define u0			up 0
#define m			make

# breakpoint if exception thrown
define be	 		break objc_exception_throw
#define bException
#	b objc_exception_throw
#	end

set print static-members off
#set radix 16
set confirm no
#define po
#	print-object
#	end
#alias po print-object

#define sth
#	step
#	next
#	step
#	step
#	fin
#	p ecb_()
#	up 0
#	end

# "this location" macro:
define tl
	p this->loc
	end

# "print display3D()" macro:
define pd
	p display3D()
	end

# "this composition" macro:
define tc
	p this->printComposition(99,0)
	end

# "this name" macro:
define tn
	p this->name
	end

## watch this thread macro:
#define tw
#	call th_watch()
#	call nobreak()
#	end

#define nobreak
#	call nobreak()
#	end

## what ecb am I running
#define te
#	call ecb_()
#	end

## print all events
#define events
#	call events()
#	end

# print time
define tt
	p globalTime
#	p th_time
	end

# print print_contents()
define pc
	p print_contents()
	end

# 
define rundone
	p rundone()
	end

# reread this file macro:
define gdbinit
	source ~/.gdbinit		# this may or may not be right for you
	end

set print pretty on
#set prompt --->
set verbose on

# ==================== save keystrokes ====================
define u0
	up 0
	end

define m
	make
	end

# to make addresses repeatable between runs, start gdb with:
#	setarch i386 -R gdb --annotate=3 ...
# http://gcc.gnu.org/wiki/DebuggingGCC
# Address space layout randomization (ASLR)


# for debug:
#define malloc_check
#     set env MALLOC_CHECK_ 1
#     end
#     unset env MALLOC_CHECK_

# ----- display _vptr with .gdbinit macros:
# USAGE:
# p *this                     yields v_ptr.Actor = 0xAAAA
# set vptr=0xAAAA
# vptr
# vptr

define vptr
      x *(void **)vptr
      set vptr += sizeof(void *)
      end

set disable-randomization on
set verbose off