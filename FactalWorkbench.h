//  FactalWorkbench.h -- User Code interface to FactalWorkbench C2015PAK

/**
FactalWorkbench.h:  --> 1h
FactalWorkbench.mm: --> 1m

1m	userSimulationPreferences							EXPERIMENTATION
2h	macros for: xm,xM,xr,xR to enable active, simCtl	EXPERIMENTATION

5m	Leafs, Bundles										LIBRARY
4m	Composite Generators: aMarkovPort					LIBRARY
4h	aXXX() part generators								LIBRARY
5h	Leaf network definitions							LIBRARY
6h	common Bundles										LIBRARY

1h	macros for: names, flip, spin, placement, 			LANGUAGE
3h	global variables to bind to those macros			LANGUAGE

2m	global variables to bind to those macros			BINDING
3m	binding macros:checkTest							BINDING
 */

#import "HnwObject.h"
@class Bundle, Branch, Link;
@class Leaf;
@class Tunnel;
@class Net, Brain;
@class Actor;
@class MatrixActor;
@class Mirror;
@class WriteHead;
@class GenAtom;
@class SoundAtom;
@class Splitter;
@class Known;
@class Modulator;
@class Previous, Ago, Bulb;
@class Rotator;
@class BundleTap, WorldModel;
@class TimingChain;
@class Generator, ShaftBundleTap;
@class SupertuxWorldModel;
@class Label;

id eventUnrand(id events);

#pragma mark - Source Code Macros:
#define n(name)			@"named":@#name
#define o(n, ob)		[ob named:@#n]
#define ol(myLevel, name, part)		(myLevel<=level2gen? o(name, part) : @0)

 // Short cut PRODUCTION's, first his highest priority
#define flip			@"flip":@"1"
#define flip1			@"flip":@"1"
#define flip0			@"flip":@"0"

#define spin$0			@"spin":@"0"	// Hash pair
#define spin_0			@"spin:0"		// Array control string
#define spin$R			@"spin":@"1"
#define spin_R			@"spin:1"
#define spin$1			@"spin":@"1"
#define spin_1			@"spin:1"
#define spin$2			@"spin":@"2"
#define spin_2			@"spin:2"
#define spin$L			@"spin":@"3"
#define spin_L			@"spin:3"
#define spin$3			@"spin":@"3"
#define spin_3			@"spin:3"

 // sugar for placement of self
#define selfLinkY		@"placeSelf":@"link"
#define selfStackX		@"placeSelf":@"+xcc"
#define selfStackX_		@"placeSelf":@"+x<c"
#define selfStackY		@"placeSelf":@"+ycc"
#define selfStackZ		@"placeSelf":@"+zcc"
#define behindOthers	@"placeSelf":@"-zcc"
 // sugar for placement of parts (within self)
#define partsLinkY		@"placeParts":@"link"
#define partsStackX		@"placeParts":@"+xcc"
#define partsStackY		@"placeParts":@"+ycc"
#define partsStackZ		@"placeParts":@"+zcc"

#define CameraHsuz(h,s,u,z) @"camera":@{@"poleHeight":(@h), @"poleSpin":(@s), @"horizonUp":(@u), @"zoom":(@z)}
#define SIM_OFF			@"simEnable":@0
#define SIM_ON			@"simEnable":@1
#define VEL(v)			@"linkVelocityLog2":@v

#define HcMenu			@"controlBlock":@"menu"
#define HcRegress		@"controlBlock":@"regress"
#define HcLine			@"line":@__LINE__			// source line #
#define HcName(n)		@"name":@#n					// name
#define HcSelected		@"selectedWithX":@1

  // Define the kind of tests the user may use
 // (Only evaluatie "content" if test is the selected test.)
			// menu
#define      m(content)														   \
	if (Brain *brain = checkTest(@{HcMenu,   HcLine}))						   \
		uBuilt_brain = [brain build:content];
#define     xm(content)														   \
	if (Brain *brain = checkTest(@{HcMenu,   HcLine,HcSelected}))			   \
		uBuilt_brain = [brain build:content];
#define    xxm(content)														   \
	if (Brain *brain = checkTest(@{HcMenu,   HcLine}))						   \
		uBuilt_brain = [brain build:content];

			// named menu
#define      M(name,content)												   \
	if (Brain *brain = checkTest(@{HcMenu,   HcLine,HcName(name)}))			   \
		uBuilt_brain = [brain build:content];
#define     xM(name,content)												   \
	if (Brain *brain = checkTest(@{HcMenu,   HcLine,HcName(name), HcSelected}))\
		uBuilt_brain = [brain build:content];
#define    xxM(name,content)												   \
	if (Brain *brain = checkTest(@{HcMenu,   HcLine,HcName(name)}))			   \
		uBuilt_brain = [brain build:content];

			// regression
#define      r(content)														   \
	if (Brain *brain = checkTest(@{HcRegress,HcLine}))						   \
		uBuilt_brain = [brain build:content];
#define     xr(content)														   \
	if (Brain *brain = checkTest(@{HcRegress,HcLine,HcSelected}))			   \
		uBuilt_brain = [brain build:content];
#define    xxr(content)														   \
	if (Brain *brain = checkTest(@{HcRegress,HcLine}))						   \
		uBuilt_brain = [brain build:content];

			// named regression
#define      R(name,content)												   \
	if (Brain *brain = checkTest(@{HcRegress,HcLine,HcName(name)}))			   \
		uBuilt_brain = [brain build:content];
#define     xR(name,content)												   \
	if (Brain *brain = checkTest(@{HcRegress,HcLine,HcName(name), HcSelected}))\
		uBuilt_brain = [brain build:content];
#define    xxR(name,content)												   \
	if (Brain *brain = checkTest(@{HcRegress,HcLine,HcName(name)}))			   \
		uBuilt_brain = [brain build:content];


#pragma mark - linkage from source code

Brain *			checkTest(NSDictionary *contentType); // so above macros compile in tests

#define setParentMenu(named) sceneMenuePath = named
extern	id		sceneMenuePath;				// menue path of current test
	// NEW SEMANTICS (NOT YET WORKING)
	//  e.g: with currentPath of "/a/b":
	//       "///newPath" --> "/a/b/newpath"
	//		 "//newPath"  --> "/a/newpath"
	//		 "/newPath"   --> "/newpath"

extern	int		uWant_menuNo;				// BROKEN, was wantMenuNo
extern	int		uWant_regressNo;			//         was wantRegressNo
extern	id		uWant_name;

extern	bool	uWant_itBuilt;


extern	int		uAt_menuNo;					// current of menue type
extern	int		uAt_regressNo;				// current of regress type
extern	int		uAt_allNo;					// current of any type (for yeuchs)


extern	id		uBuilt_brain;				// output part
extern	id		uBuilt_metaInfo;			// output metainfo about part

#pragma mark - aka: Dashboard
id userSimulationPreferences();

#pragma mark - HaveNWant Element Definitions
//////////////////////////////////////////////////
/////     Bundles of Ports                  /////
////////////////////////////////////////////////

#pragma mark Atoms
//Port			*aPort			(id pri=nil,		 			id etc=nil);
GenAtom			*aGenAtom		(								id etc=nil);
SoundAtom		*aSoundAtom		(								id etc=nil);
Mirror			*aMirror		(id pri=nil,					id etc=nil);

Link			*aLink			(id pri=nil, id sec=nil,		id etc=nil);

Previous		*aPrevious		(id pri=nil,					id etc=nil);
Ago				*anAgo			(id pri=nil,					id etc=nil);

Modulator		*aModulator		(id pri=nil,					id etc=nil);
Rotator			*aRotator		(id pri=nil,					id etc=nil);
Known			*aKnown			(id pri=nil,					id etc=nil);

#pragma mark Splitters
Splitter		*aSplitter		(id shareKind,id pri=nil,id sec=nil,id etc=nil);
Splitter		*aBroadcast		(id pri=nil, id sec=nil,		id etc=nil);
Splitter		*aBayes			(id pri=nil, id sec=nil,		id etc=nil);
Splitter		*aMaxOr			(id pri=nil, id sec=nil,		id etc=nil);
Splitter		*aMinAnd		(id pri=nil, id sec=nil,		id etc=nil);
//Splitter		*aMax2min		(id pri=nil, id sec=nil,		id etc=nil);
Splitter		*aHamming		(id pri=nil, id sec=nil, 		id etc=nil);
Splitter		*aMultiply		(id pri=nil, id sec=nil,		id etc=nil);
Splitter		*aSequence		(id pri=nil, id sec=nil, 		id etc=nil);
Splitter		*aKNorm			(id pri=nil, id sec=nil, 		id etc=nil);
Splitter		*aBulb			(id pri=nil, id sec=nil,		id etc=nil);

// Add: Emphasis

#pragma mark BundleTap
BundleTap		*aBundleTap		(								id etc=nil);
BundleTap		*aBundleTap		(int limit,						id etc=nil);

TimingChain		*aTimingChain	(								id etc=nil);
WorldModel 		*aWorldModel	(								id etc=nil);

id				aGenerator		(			  					id etc=nil);
id				aGenerator		(int limit,  					id etc=nil);

ShaftBundleTap	*aShaftBundleTap(								id etc=nil);

SupertuxWorldModel *aSupertux	(			id runCommand,		id etc=nil);
SupertuxWorldModel *aSupertux	(int limit, id runCommand,		id etc=nil);

#pragma mark Nets
Net				*aNet			(					NSArray *parts		  );
Net				*aNet			(NSDictionary *etc, NSArray *parts		  );

Bundle			*aBundle		(								id etc=nil);
Tunnel			*aTunnel		(								id etc=nil);

Leaf			*aLeaf			(								id etc=nil);
Leaf			*aLeaf			(NSString *type, NSDictionary *bindings,
												NSArray *parts,	id etc=nil);
Brain			*aBrain			(NSArray *parts);
Brain			*aBrain			(NSDictionary *etc, NSArray *parts);

Actor			*anActor		(								id etc=nil);
//MatrixActor  	*aMatrixActor	(								id etc=nil);

WriteHead		*aWriteHead		(								id etc=nil);

#pragma mark Support
Label			*aLabel			(id text, int fontNumber,id jog,id etc=nil);

#pragma mark Composite
id				aMarkovPort		(id hamming=nil, id maxor=nil,	id etc=nil);
extern id didat_bundle;
extern id didat_speak_bundle;
extern id didat_sound_bundle;
extern id didat_speak_sound_bundle;

extern id   dat_bundle;
extern id   dat_sound_bundle;
extern id     t_bundle;
extern id     t_sound_bundle;
extern id wheel_bundle;
id				aDiDa(id diArg, id daArg, id tArg,				id etc=nil);

Branch			*aBranch(										id etc=nil);

id				stopIfNotLevel(int);

#pragma mark - Leaf Definitions
//////////////////////////////////////////////////
/////     Leaf Definitions                  /////
////////////////////////////////////////////////
 // -------- Null element
id aNilLeaf			(id etc1=nil, id etc2=nil);						// USED 1x
id aGenLeaf			(id etc1=nil, id etc2=nil);
id aGenSoundLeaf	(id etc1=nil, id etc2=nil, id etc3=nil);
 // -------- Generic Splitter
id aSplitterLeaf	(id etc1=nil, id etc2=nil);
id aGenSplitterLeaf	(id etc1=nil, id etc2=nil, id etc3=nil);
 // -------- Broadcast
id aBcastLeaf		(id etc1=nil, id etc2=nil);
id aGenBcastLeaf	(id etc1=nil, id etc2=nil, id etc3=nil);
id aGenSoundBcastLeaf(id etc1=nil,id etc2=nil, id etc3=nil, id etc4=nil);
 // -------- Max
//id of_max			(id etc1=nil, id etc2=nil);
id aGenMaxLeaf		(id etc1=nil, id etc2=nil, id etc3=nil);
id aGenSoundMaxLeaf	(id etc1=nil, id etc2=nil, id etc3=nil, id etc4=nil);
id aGenMaxsqLeaf	(id etc1=nil, id etc2=nil, id etc3=nil);
id aBcastMaxLeaf	(id etc1=nil, id etc2=nil);
 // -------- Bayes
id aBayesLeaf		(id etc1=nil, id etc2=nil);						// UNUSED
id aGenBayesLeaf	(id etc1=nil, id etc2=nil, id etc3=nil);		// UNUSED
 // -------- Mod
id aModLeaf			(id etc1=nil, id etc2=nil);						// USED 1x
id anOrModBcastLeaf	(id etc1=nil, id etc2=nil, id etc3=nil, id etc4=nil);
 // -------- Rotator
id aRotLeaf			(id etc1=nil, id etc2=nil);						// UNUSED
id aRotBcastLeaf	(id etc1=nil, id etc2=nil, id etc3=nil);
 // -------- Branch
id aBranchLeaf		(id etc1=nil, id etc2=nil);						// UNUSED
 // -------- Bulb
id aBulbLeaf		(id etc1=nil, id etc2=nil);
id aGenBulbLeaf		(id etc1=nil, id etc2=nil, id etc3=nil);		// UNUSED

 // -------- Previous
id aPrevBcastLeaf	(id etc1=nil, id etc2=nil, id etc3=nil);
id aGenPrevBcastLeaf(id etc1=nil, id etc2=nil, id etc3=nil, id etc4=nil);// DEFAULT EVIDENCE for Previous
id aGenSoundPrevBcastLeaf(id etc1=nil, id etc2=nil, id etc3=nil, id etc4=nil, id etc5=nil);
id aGenPrevLeaf		(id etc1=nil, id etc2=nil, id etc3=nil);
id aFlipPrevLeaf	(id etc1=nil, id etc2=nil);
id aPrevLeaf		(id etc1=nil, id etc2=nil);

 // -------- Ago
id aGenAgoLeaf		(id etc1=nil, id etc2=nil, id etc3=nil);		// UNUSED
id anAgoMaxLeaf		(id etc1=nil, id etc2=nil, id etc3=nil);		// UNUSED

id anAgoDistCombLeaf(id etc1=nil, id etc2=nil, id etc3=nil, id etc4=nil);

 // -------- Report an ERROR
id anError 			(id string);


#pragma mark - Common Bundle Definitions
////////////////////////////////////////////////
/////     Bundles of Ports                  /////
////////////////////////////////////////////////
extern id _bundle, a_bundle, ab_bundle, abc_bundle, abcd_bundle, abcde_bundle;
extern id abcdef_bundle, abcdefg_bundle, abcdefgh_bundle, abc_def_bundle;
extern id d_bundle, drm_bundle, drmfsltD_bundle;
extern id e_bundle;
extern id i_bundle, ij_bundle, ijk_bundle;
extern id z_bundle, yz_bundle, xyz_bundle, wxyz_bundle;

extern id y0x0_bundle, y1x1_bundle, y2x2_bundle, y3x3_bundle;
extern id y4x4_bundle, y5x5_bundle, y6x6_bundle;
extern id (^y4x4_bundle_)(id, id);

#pragma mark - Common Event Definitions
#define p1x1(val) @[		\
	@"y0x0="#val, ]
#define p2x2(val) @[			\
	@"y0x0="#val, @"y0x1="#val, \
	@"y1x0="#val, @"y1x1="#val, ]
#define p3x3(val) @[						  \
	@"y0x0="#val, @"y0x1="#val, @"y0x2="#val, \
	@"y1x0="#val, @"y1x1="#val, @"y1x2="#val, \
	@"y2x0="#val, @"y2x1="#val, @"y2x2="#val, ]
#define p4x4(val) @[										\
	@"y0x0="#val, @"y0x1="#val, @"y0x2="#val, @"y0x3="#val, \
	@"y1x0="#val, @"y1x1="#val, @"y1x2="#val, @"y1x3="#val, \
	@"y2x0="#val, @"y2x1="#val, @"y2x2="#val, @"y2x3="#val, \
	@"y3x0="#val, @"y3x1="#val, @"y3x2="#val, @"y3x3="#val, ]
#define p5x5(val) @[													  \
	@"y0x0="#val, @"y0x1="#val, @"y0x2="#val, @"y0x3="#val, @"y0x4="#val, \
	@"y1x0="#val, @"y1x1="#val, @"y1x2="#val, @"y1x3="#val, @"y1x4="#val, \
	@"y2x0="#val, @"y2x1="#val, @"y2x2="#val, @"y2x3="#val, @"y2x4="#val, \
	@"y3x0="#val, @"y3x1="#val, @"y3x2="#val, @"y3x3="#val, @"y3x4="#val, \
	@"y4x0="#val, @"y4x1="#val, @"y4x2="#val, @"y4x3="#val, @"y4x4="#val, ]
#define p6x6(val) @[																	\
	@"y0x0="#val, @"y0x1="#val, @"y0x2="#val, @"y0x3="#val, @"y0x4="#val, @"y0x5="#val, \
	@"y1x0="#val, @"y1x1="#val, @"y1x2="#val, @"y1x3="#val, @"y1x4="#val, @"y1x5="#val, \
	@"y2x0="#val, @"y2x1="#val, @"y2x2="#val, @"y2x3="#val, @"y2x4="#val, @"y2x5="#val, \
	@"y3x0="#val, @"y3x1="#val, @"y3x2="#val, @"y3x3="#val, @"y3x4="#val, @"y3x5="#val, \
	@"y4x0="#val, @"y4x1="#val, @"y4x2="#val, @"y4x3="#val, @"y4x4="#val, @"y4x5="#val, \
	@"y5x0="#val, @"y5x1="#val, @"y5x2="#val, @"y5x3="#val, @"y5x4="#val, @"y5x5="#val, ]
