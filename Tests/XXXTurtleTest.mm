////  TurtleTest.m -- run regression tests C2012PAK
//
//// STATUS 121110:
////  1. cycles through most tests just fine
////  3. some tests fail on rerun
////  4. how to report failure (e.g. in panic)?
////
//// CURIOS:
////  1. runs very fast
////  2. mouse pan/zoom inactive
//
//#import <GHUnit/GHUnit.h>
//#import "Brain.h"
//#import "SimNsWc.h"
//#import "WorldTurtle.h"
//#import "Common.h"
//
//////////////////////////////////////////////////////////////////////////
//@interface TurtleTest: GHTestCase
//    @property (nonatomic, retain) SimNsWc *simNsWc;
//@end
//
//////////////////////////////////////////////////////////////////////////
//@implementation TurtleTest
//
//- (void)setUp {
//    @autoreleasepool		// required so construction debris autoreleases
//	  {
//		Brain *brain = Brain.shared;	// set Brain for testing:
//		brain.simDisableBreaks = 1;					// ignore debug breaks
//		brain.simExitWhenDone = 1;					// stop tests when done
//
//		self.simNsWc = [[[SimNsWc alloc] init] autorelease];
//		self.simNsWc.appController = self;		// weak
//	  }
//  }
//- (void)tearDown {
//    @autoreleasepool {		// required so destroyed objects autoreleases
//		self.simNsWc = nil;
//		[ObjectHW resetObjectMonitors];
//	  }
//  }
//
//- (void)doTest:(int)n; {
//  
//	id metaInfo=0;	// a place to return results
//	panic("Things have shifted here");
////	id info = [Builder findRegressionInfoWithin:[Model01 allTests] testNumber:n kind:metaInfo];
////	char **args = [Builder findRegression:n];
//
//	[self.simNsWc buildConfigurationFromArgs:args];
//	[self.simNsWc nextFrame];			// start the test
//	[self.simNsWc awaitTestDone];		// wait for test to complete
//  }
//
//#define test_(__n) \
//- (void)test_##__n   {	[self doTest:__n];    }
//
//test_(01);
//test_(02);
//test_(03);
//test_(04);
//test_(05);
//test_(06);
//test_(07);
//- (void)test_08   {	[self doTest:8];    }		//test_(8);
//- (void)test_09   {	[self doTest:9];    }		//test_(9);
//test_(10);
//test_(11);
//test_(12);
//test_(13);
//test_(14);
//test_(15);
//test_(16);
//test_(17);
//test_(18);
//
//@end
