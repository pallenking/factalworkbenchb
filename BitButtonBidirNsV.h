// BitButtonBidirNsV.h -- Bidirectional Button  C2015PAK

#import "BidirNsV.h"
@class Port, InspectNsVc, GenAtom, BundleTap;


@interface BitButtonBidirNsV : BidirNsV

	  /// NsV Items for Button widget:
	 // These properties are filled in by IB:
//	@property (nonatomic, retain)			Bundle		*targBun;
//  @property (nonatomic, retain/*strong*/)	NSString	*portPath (BidirNsV.mm)
//  @property (nonatomic, retain/*strong*/)	NSString	*clockPath (BidirNsV.mm)

	@property (nonatomic, retain/*strong*/)	NSButton	*button;	// DownNUpButton
    @property (nonatomic, retain/*strong*/)	NSString	*title;
    @property (nonatomic, retain/*strong*/)	NSString	*sound;

	 // This was put in about 4/5/16, while working on Turtle34. Not sure why
	@property (nonatomic, assign)			char		retract;

	 // weed out multiple UP's (Morse)BidirSliders
	@property (nonatomic, assign)			char		wasPressedOnce;

	 /// Links to BundleTap
//	@property (nonatomic, retain/*strong*/)	BundleTap	*generator;
//	@property (nonatomic, retain/*strong*/)	GenAtom		*genAtom;

- (IBAction)	buttonTapped:(id)sender;
- (NSString *)	name;

@end
