// WorldModel.mm -- Basic discrete time/value data source C2017PAK

#import "WorldModel.h"

#import "SimNsWc.h"
#import "SimNsVc.h"
#import "TimingChain.h"
#import "Brain.h"
#import "View.h"
#import "FactalWorkbench.h"
#import "Id+Info.h"
#import "Common.h"
#import "NSCommon.h"
#import "GlCommon.h"
#import <GLUT/glut.h>
#import "Colors.h"
@implementation WorldModel

#pragma mark 1. Basic Object Level
- init; {						self = [super init];

	self.prob 			= nan("unset");
	self.delayedEvents 	= @[].anotherMutable;
	return self;
}

- (void) dealloc {
	self.events			= nil;
	self.delayedEvents 	= nil;

    [super dealloc];
}

#pragma mark 4. Factory
- (id) atomsDefinedPorts;	{	id rv = [super atomsDefinedPorts];
//	rv[@"S"]		= @"pc ";	// Secondary				(always create)
	return rv;
}

WorldModel *aWorldModel(id etc) {
	WorldModel *newbie = anotherBasicPart([WorldModel class]);
XX	newbie = [newbie build:etc];		// (perhaps nil)
	return newbie;
}

- (id) build:(id)info;	{					[super build:info];

	 /// If useless, add nothing (return nil)
	if ([self parameter:@"abort"])
		return @0;

//! xzzy1 BundleTap::	1. eventLimit		:<int>			-- Limit on generated events
//! xzzy1 BundleTap::	2. events			:<eventList>	-- List of events
//! xzzy1 BundleTap::	3. prob				:<probability>	-- random with probability
//!	xzzy1 BundleTap::							x			--		x==1 (all default to 0)
//!	xzzy1 BundleTap::							x=.4		--		x==0.4


	if (id events = [self takeParameter:@"events"]) {
		if (NSNumber *n 		= coerceTo(NSNumber, events))
			assert([n floatValue]==0.0, (@"(NSNumber)0 is nil events; n!=0 is error"));
		else
			self.events 		= events;	//eventUnrand(rawEvents);
	}

	if (id eventLimit			= [self takeParameter:@"eventLimit"])
		self.eventLimit 		= [eventLimit intValue];

	self.prob					= nan("unset");
	if (id str=[self takeParameter:@"prob"])
		self.prob				=	[str floatValue];

	return self;
}

- (void) postBuild; {						[super postBuild];

	Port *pPort 				= self.pPort;	 // find where our P-Port connects
	id timingChainPort			= pPort.portPastLinks;
XX	self.timingChain 			= mustBe(TimingChain, [timingChainPort parent]);	//****
}

/* Functionality by Example:
 
Example of 5 samples of bundle with leafs named "a" and "b" with random 1-ness
 of 0.6 and 0.7 resp., set up to play again:
 
	id foo = @[ @"repeat 5", @[@"a=rnd .6",@"b=rnd .7"], @"again"];
 
Reserved Symbols:
	"again"
	"repeat <n>"	unroll the array that follows <n> times
	@0				do not preclear

String form for Name/Values:
	String:			Set bundle's leaf
	"a"				a to value 1.0
	"a=0.5"			a to value 0.5
	"a=rnd 0.5"		a to 1 with prob(0.5)
    "a=rVal 0.5"	a to random value >0 and <0.5
  */

 // 161201 This code is a shadow of what it started out as, and could be rebuilt
id eventUnrand(id events) {
	NSArray *eventsArray = mustBe(NSArray, events);
	NSMutableArray *rv = @[].anotherMutable;
	[[@"" addF:@"- - - - - eventUnrand(%@)", [events pp]] ppLog];

	 // paw through all elements of outter array
	for (int i=0, n=(int)eventsArray.count; i<n; i++) {
		id eventI = eventsArray[i];

		if (NSString *eventIString = [eventI asString]) {

			 // .., "repeat count", <body>, ..
			if ([eventIString hasPrefix:@"repeat"]) {
				id nStr = [eventIString substringFromIndex:6];
				int n = [nStr intValue];
				id body = eventsArray[++i];			// <body> --> eventUnrand(<body>)
				for (int j=0; j<n; j++)

XR					[rv addObject:eventUnrand(body)];
			}

			 // .., "again"]
			else if ([eventIString hasPrefix:@"again"]) {
				//panic(@"wtf");
				[rv addObject:eventIString];	// just pass thru, processed elsewhere
			}

			 // .., valueSpec, ...		// valueSpec = <name>["=" [("rnd" <prob>) | "rVal" <maxVal>)]]
			else {
				id name = eventIString;				// initial guess: no "="

				  // names are space-separated or quoted (REALLY?)
				 // signal may contain a value (e.g. @"foo=0.7")
				NSArray *comp = [eventIString componentsSeparatedByString:@"="];
				float value = 1.0;
				if (comp.count==2 and [comp[0] length]!=0) {
					 // it is legal for names to start wit an "="
					name = comp[0];
					id rawValue = comp[1];
					if ([rawValue asString]) {
						if (float v = [rawValue floatValue])	// e.g: a=0.5

							 // value = comp[1], as a float
							value = v;			// what if valid value? (value==0.)

						else if ([rawValue hasPrefix:@"rnd"]) {	// e.g. a=rnd 0.5

							 // value = 1, with probability prob
							id probStr = [rawValue substringFromIndex:3];
							float prob = [probStr floatValue];
							value = randomProb(prob);		// 1 with prop
						}
						else if ([rawValue hasPrefix:@"rVal"]){	// e.g. a=rVal 0.5

							 // value = random boxcar, between 0 and value rVal
							float rVal = [[rawValue substringFromIndex:5] floatValue];
							value = randomDist(0, rVal);
						}
						else
							panic(@"");
					}
				}
				if (value) {
					id absSignal = value==1? [name addF:@"=1"]: [name addF:@"=%.3f", value];
					[rv addObject:absSignal];
				}
			}
		}
		else
			panic(@".., other, ...");
	}
	NSPrintf(@"    - - - - - returns '%@'\n", [rv pp]);
	return rv;
}


#pragma mark 7-. Simulation Actions
- (void) reset; {								[super reset];
	
	self.eventNow			= 0;
	self.eventIndex			= 0;
}

#pragma mark 7. Simulator Messages
///***///***///***///***///***///***///***///***///***///***///***///***///
///***///***///***///***///***///***///***///***///***///***///***///***///
///***///***///***///***///***///***///***///***///***///***///***///***///

  //// Prosses keyboard key and control release of events[]
 ///
- (bool) processKey:(char)character modifier:(unsigned long)modifier; {

	 // Single Step
	if (!(modifier & FWKeyUpModifier)) {		////// key DOWN ///////
		int was = self.eventLimit;
		if (character == 'S') {					//// S ////
			if (self.eventLimit < 0)				// currently no limits
				self.eventLimit		= self.eventNow;	// STOP by limiting events
			else  									// currently limited
				self.eventLimit		= -1;				// turn limits OFF
			[self logEvent:@"\n=== EVENT: Key 'S' DOWN: eventLimit=%d(was %d) eventNow=%d", self.eventLimit, was, self.eventNow];
		}
		if (character == 's') {					//// s ////
			if (self.eventLimit < 0)				// currently no limits
				self.eventLimit		= self.eventNow+1;	// stop after 1 event
			else 									// currently limited
				self.eventLimit		++;					// add one more
			[self logEvent:@"\n=== EVENT: Key 's' DOWN: eventLimit=%d(was %d) eventNow=%d", self.eventLimit, was, self.eventNow];
		}
		if ( character == '?' ) {				//// ? ////
			NSPrintf(@"   === BundleTap %@ commands:\n"
					"\ts			-- single step generator					\n"
					"\tS			-- toggle unlimited generation				\n",
					self.timingChain.fullName);
			return false;
		}
	}
	else 									////// key UP //////////
		panic(@"");
		//[self logEvent:@"\n=== EVENT: Key 's' UP, userUpEvent=1"];

	return true;
}

 // return next event generated by WorldModel, and it's simple methods
- (id) dequeEvent; {
	id rv = nil;
	if ((rv = [self.delayedEvents dequeFromHead]))
		return rv;
		
	if (self.eventNow >= self.eventLimit and self.eventLimit>=0)
		return nil;					// Event Generation is limited by count

	  /// Event is an Array
	 ///
	if(NSArray *events = coerceTo(NSArray, self.events)){
		 // Any events in array?
		if (self.eventIndex < events.count) {
			rv = self.events[self.eventIndex++];

			if ([rv isEqualTo:@"again"]) {
				[self.timingChain resetForAgain];
				self.eventIndex = 0;
				rv = self.events[self.eventIndex++];
				assert(![rv isEqualTo:@"again"], (@"'Again' cannot be first element of array"));
			}
		}
	}

	  /// Events is a NSNumber: float is prob; or int is nil (or epoch)
	else if (NSNumber *e = coerceTo(NSNumber, self.events))
		rv = e;
	  /// .Prob is set directly
	else if (!isNan(self.prob))
		rv = @(self.prob);

	self.eventNow			+= rv!=nil;			// next event if one

	if (rv != nil)
		self.flash = self.brain.flashFrames;

	return rv;
}

- (void) addDelayedEvent:event; {
	[self.delayedEvents addObject:event];
}

#pragma mark 8. Reenactment Simulator
- (int) unsettledPorts;		{	return 0; 	}

float wmDiam = 6;

#pragma mark 9. 3D Support
- (Bounds3f) gapAround:(View *)v; {
	float x = wmDiam;
	Bounds3f b = Bounds3f( -x,  0, -x, x, 3, x);	//hack: x compensates for border
	return b;
}
#pragma mark 11. 3D Display
- (void) drawFullView:(View *)v context:(RenderingContext *)rc; {
	
	rc.color = colorBrown;
	glPushMatrix();
		float r = self.brain.bitRadius;
		glTranslatefv(v.bounds.center() + Vector3f(0, 0.5*r, 0));
		glRotatef(90, 1,0,0);//radius, cut, slines, stacks
		glScalef(1.0, 1.0, 0.26);
		myGlSolidHemisphere(r*wmDiam, 0.5, 16, 16);
	glPopMatrix();
	[super drawFullView/*Ports*/:v context:rc];	// paint Ports and bounding box
}


#pragma mark 15. PrettyPrint
///////////////////////// Stringsss for Printing  /////////////////////////
- (NSString *) pp1line:aux; {		id rv=[super pp1line:aux];
	
	rv=[rv addF:@"sClk=%@ ", self.timingChain.fullName16];

	rv=[rv addF:@"now=%d limit=%d ", self.eventNow, self.eventLimit];
	if (self.events) {
		rv=[rv addF:@"event %d of events%@", self.eventIndex, [self.events pp]];
		if (self.eventIndex >= self.events.count)
			rv=[rv addF:@"EXCEEDS LIMIT"];
	}

	if (!isNan(self.prob))
		rv=[rv addF:@"prob=%.2f ", self.prob];

	return rv;
}

@end
