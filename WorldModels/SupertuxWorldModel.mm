//  SupertuxWorldModel.mm -- Supertux game, driven by LowerBrain C2011 King Software

/* to do:
 1. package up supertux2 app
 */


 // Supertux C++
#import "LowerBrain.hpp"
#import "Datum.hpp"

 // HaveNWant Objc:
#import "Brain.h"
#import "Id+Info.h"
#import "SupertuxWorldModel.h"
#import "Splitter.h"
#import "GenAtom.h"
#import "Bundle.h"
#import "Leaf.h"
#import "Link.h"
#import "FactalWorkbench.h"

#import "Common.h"
#import "NSCommon.h"

@implementation SupertuxWorldModel

#pragma mark 4. Factory
SupertuxWorldModel *aSupertux(id runCommand, id etc) {
	return aSupertux(-1, runCommand, etc);
}
SupertuxWorldModel *aSupertux(int limit, id runCommand, id etc) {
	id info = @{@"stuxCommand":runCommand,
				@"eventLimit":[NSNumber numberWithInt:limit], etcEtc};

	SupertuxWorldModel *newbie = anotherBasicPart([SupertuxWorldModel class]);
XX	newbie = [newbie build:info];
	return newbie;
}

- (id) build:(id)info; {	[super build:info];

//! xzzy1 SupertuxGen::	1. stuxEndTime		:float			-- of run, in stux time
//!	xzzy1 SupertuxGen::	2. stuxCommand		:""				-- command for starting board
//!	xzzy1 SupertuxGen::	3. supertuxPortClientDebug:@int
//! xzzy1 SupertuxGen::	4. supertuxSensoryInfoDebug:@int
	
	self.stuxEndTime = HUGE;
	if (id stuxEndTime = [self takeParameter:@"stuxEndTime"])
		self.stuxEndTime = [stuxEndTime floatValue];

	if (NSString *runCmd = [self takeParameter:@"stuxCommand"]) {
		  // config and start Supertux server,
		 // with arguments for SuperTux game driver
		char *argv[] =   {(char *)"./supertux2",
			(char *)"--run_tux",
			(char *)"--seed", (char *)"98765432",
			(char *)"--section", (char *)runCmd.UTF8String,
			(char *)"data/levels/world1/01 - Welcome to Antarctica.stl", 0};
		
		int argc=0;
		for (; argv[argc]; argc++)
			;

		// create a primitive brain to control the envirnoment
		simpleBrain = new LowerBrain();   // or RelatorBrain();
		simpleBrain->inz(argc, argv);
		
		game = new Supertux;
#ifdef xxx
		bool helpMode = false;
		char *host = (char *)"127.0.0.1";
		int portNo = 3576;
		char *password = (char *)"rall35Tu7";
	//	char supertuxServer[] = "supertux2";
		char *seed = 0;
		bool run_tux = false;
		bool noTux  = false;
#endif		
		NSString *ena = [self takeParameter:@"supertuxPortClientDebug"];
		game->portClientDebug(ena.intValue);
		NSString *ena2 = [self takeParameter:@"supertuxSensoryInfoDebug"];
		game->sensoryInfoDebug(ena2.intValue);
		game->inz(argc, argv);
		// create the Brain to control
		//   ==== ADD MANY ARGS HERE:
		//        --help				display this help message\n"
		//        --host HOST			contact server named HOST\n"
		//        --port PORT			contact server on port PORT\n"
		//        --password PASSWORD	use PASSWORD when contacting server\n"
		//        --seed SEED			start server with random seed SEED\n"
		//        --run_tux			start a tux server on LOCALHOST\n"
		//        --recordFile FILE	record game sensations experienced to FILE\n"
		//        --playFile FILE		play back sensations from FILE\n"

		fovia = NSMakeRect(-2, -3, 5, 5);	// relative to tux
	//	fovia = NSMakeRect(10, 9, 5, 5);	// fixed in center of screen
		game->setFoviaSize(fovia.origin.x*16, fovia.origin.y*16,
						   fovia.size.width*16, fovia.size.height*16);
		
		cmd = commandDatumNULL;
		senses = sensoryDatumNULL;
		sensesFull = 0;
	}
	return self;
}

#pragma mark 8. Reenactment Simulator
//**//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//
//***//*//*//*//*//*//*//*//  Reenactment Simulator  //*//*//*//*//*//*//*//*//
//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*////

- (NSString *) eventReady; {	// rv: true-->valid data ready
	//assert(brain, (@"use BundleTap instead"));

	 // Run Supertux a while:
    if (!sensesFull) {
		 // === get a motor command based on senses
        cmd = simpleBrain->chooseAction(senses);
        printf("simpleBrain chooses buttons %08x\n", cmd.buttons);
        
		 // === run command in the Brain, return senses
        senses = game->takeAction(cmd);
        sensesFull = 1;
    }
	
	if (senses.scenery == 0)
        return nil;					/// not completely built

	NSPrintf(@"--- game->currentGameTime() = %f ---\n", game->currentGameTime());
	if (self.stuxEndTime>=0 and game->currentGameTime()>=self.stuxEndTime) {
		self.brain.simEnable --;	/// End of Run
		return [@"" addF:@"Supertux time %.2f within range", game->currentGameTime()];
	}
	
	 // we have new data
	return nil;
}

- (void) loadTargetBundle:event; {

	if ([self eventReady])
		return;

	panic(@"");
//	[self.timingChain loadPreClear];

	worldLoc tuxLoc = game->getTuxLoc();
	std::vector<screenPiece>::const_iterator sp;
	for(sp=senses.scenery->begin(); sp!=senses.scenery->end(); ++sp) {
		screenPiece &s = (screenPiece &)*sp;	// gdb doesn't like C++ vector
		if (s.h!=16 or s.w!=16)					// only look at 16x16 icon blocks
			continue;
		
		 // posn of block
		int x = SpSignExtendP(s.x);         // [[hide SpSignExtendP, BLK_W, ...]]
		int y = SpSignExtendP(s.y);
		
		  // Supertux screen is 25x19.  We take the central 5x5 to start.
		 // this is rather ad-hoc right now
		int foviaX = fovia.origin.x;
		int foviaY = fovia.origin.y;
		int xf = foviaX<0? tuxLoc.x/16+foviaX: foviaX;
		int yf = foviaY<0? tuxLoc.y/16+foviaY: foviaY;
		int xd = x - xf;
		int yd = y - yf;
		
		Bundle *retina = mustBe(Bundle, 0);//[self.targetBundle partNamed:@"retina"]);
		assert(retina, (@"Supertux couldn't find its retina"));

		 // set our output con
		if (xd>=0 and xd<fovia.size.width and yd>=0 and yd<fovia.size.height) {
			//if (1)
			//	xd = fovia.size.width - xd - 1;
			Bundle *retinaY		= mustBe(Bundle, [retina  partAtIndex:yd]);
			Leaf   *retinaXY	= mustBe(Leaf,   [retinaY partAtIndex:xd]);
			Port    *r			= [retinaXY port4leafBinding:@"G"];
			assert(r, (@"SupertuxWorldModel: retina(%d,%d) not in target", xd, yd));

			float value = 1;	// Looping in GenAtom needs review!
//			if (GenAtom *genAtom	= coerceTo(GenAtom, r.atom))
//				genAtom.inValue		= value;
//			else
				r.valueTake	= value;

		}
	}
	sensesFull = 0;

	  /////// 4. Super COMPLETEs
	 //
//	return [super loadTargetBundle:];
//	self.eventNow++;				// retire current event
	[self.brain kickstartSimulator];		// start simulator

	return;
}
#pragma mark 15. PrettyPrint
    ///\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\///
   ///\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\///
  ///\\//\\//\\//\\//\\//\\//\\// PRINTOUT //\\//\\//\\//\\//\\//\\//\\//\\///
 ///\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\///
///\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\///
- (NSString *) pp1line:aux; {		id rv = [super pp1line:aux];

//	if (game)   {
//		printf("%*s-game:", indent*3+3, "");
//		game->printSummary();
//	}
	return [rv addF:@" brain=%lx, game=%lx",
		(unsigned long)self.brain, (unsigned long)game];
}

@end
