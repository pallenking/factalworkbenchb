//  TurtlePenWorldModel.mm -- The 2D Environment of a Turtle in a Pen C2016PAK
#import "TurtlePenWorldModel.h"

/* to do:
 */

 // HaveNWant Objc:
#import "Brain.h"
#import "TurtlePenWorldModel.h"
#import "Splitter.h"
#import "Bundle.h"
#import "Leaf.h"
#import "Link.h"
#import "FactalWorkbench.h"

#import "Common.h"
#import "NSCommon.h"

@implementation TurtlePenWorldModel

#pragma mark 4. Factory
TurtlePenWorldModel *aTurtlePenGenerator(id etc) {
	return aTurtlePenGenerator(-1, etc);
  }
TurtlePenWorldModel *aTurtlePenGenerator(int eventLimit, id etc) {

	TurtlePenWorldModel *newbie = anotherBasicPart([TurtlePenWorldModel class]);
XX	[newbie build:@{etcEtc}];
	return newbie;
  }

- (id) build:(id)info; {	[super build:info];

//! xzzy1 TurtlePenGen	1. boardSize		:"width,height"	-- 
	
	if (id string = [self parameterInherited:@"boardSize"]) {
		;
	  }
	return self;
  }

#pragma mark 8. Reenactment Simulator
//**//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//
//***//*//*//*//*//*//*//*//  Reenactment Simulator  //*//*//*//*//*//*//*//*//
//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*////

- (NSString *) eventReady; {	// rv: true-->valid data ready
	return @"TurtlePen always ready";
  }
float activation(float theta);
float activation(float theta) {		// activation peaks at 0, is 1.0 wide
	float peak = 1.0 - fabs(theta);
	float rv = max(0.0, peak);
	return rv;
  }
float force(float theta);
float force(float theta) {			// symmetric about 0.0
	float rv = theta<-1? 0: theta<0? -1: theta<1? 1: 0;
	return rv;
  }

- (void) loadTargetBundle:event; {

panic(@"");

	  // output activations upward
	 // input wants, weighted sum to force
	for (int k=0; k<2; k++) {
		Wheel *w = &wheels[k];
		w->dTread = 0.0;
		int i=-1; for (id bitName in wheel_bundle) { i++;
			Port *b = mustBe(Port, 0);//[self.targetBundle genPortOfLeafNamed:bitName]);

			float theta = w->tread - int(w->tread/4);
			float act = activation(theta - i);
			b.valueTake = act;

			float want = b.valueGet;
			float forc = force(theta - i);
			float delta = want * forc;
			
			NSPrintf(@"%3d: bitName='%@' act=%.3f want=%.3f force=%.3f delta=%.3f\n", i, bitName, act, want, forc, delta);

			w->dTread += 0.01 * delta;
		  }
	  }


  }
#pragma mark 15. PrettyPrint
    ///\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\///
   ///\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\///
  ///\\//\\//\\//\\//\\//\\//\\// PRINTOUT //\\//\\//\\//\\//\\//\\//\\//\\///
 ///\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\///
///\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\///
- (NSString *) pp1line:aux; {		id rv = [super pp1line:aux];

//	if (game)   {
//		printf("%*s-game:", indent*3+3, "");
//		game->printSummary();
//	  }
	return @"MORE WORK here";
  }

@end
