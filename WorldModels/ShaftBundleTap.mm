//  ShaftBundleTap.mm -- Stepper Motor with inertia C2016PAK
#import "ShaftBundleTap.h"

/* to do:
 */

#import "TimingChain.h"
#import "ShaftBundleTap.h"
#import "Id+Info.h"
#import "Brain.h"
#import "Leaf.h"
#import "View.h"
#import "FactalWorkbench.h"		// wheel_bundle hack

#import <GLUT/glut.h>
#import "GlCommon.h"
#import "Common.h"
#import "NSCommon.h"

@implementation ShaftBundleTap

#pragma mark 4. Factory

ShaftBundleTap *aShaftBundleTap(id etc) {
	ShaftBundleTap *newbie = anotherBasicPart([ShaftBundleTap class]);
XX	newbie = [newbie build:etc];
	return newbie;
}

- (id) build:(id)info; {	[super build:info];
	
//!	xzzy1 ShaftGen::    1. nPoles			:n				-- of shaft

	self.nPoles = 4;
	if (id nPoles = [self takeParameter:@"nPoles"])
		self.nPoles = [nPoles intValue];

	return self;
}

#pragma mark 8. Reenactment Simulator
/// //*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//* //
/// /*//*//*//*//*//*//*//    Reenactment Simulator   //*//*//*//*//*//*//*/ //
/// *//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*// //

/// ShaftBundleTap is kind of an odd duck here: it is a generator, but
/// acts on every cycle, not using the loadTargetBundle: interface.

- (NSString *) eventReady; {
	return nil;								// never an event to generate
}
- (void) loadTargetBundle:event; {	// gets here if another generator has event; ignore
	panic(@"");
}

- (void) simulateDown:(bool)downLocal; {

	if (downLocal) {				//============: going DOWN ================
		  // output activations upward
		 // input wants, weighted sum to force
		int nPoles = self.nPoles;
		float force = 0.0;

		 // for all Ports in Bundle:
		for (int i=0; i<nPoles; i++) {

			float poleITread = self.tread - i;
			poleITread -= nPoles * round(poleITread/nPoles);
			 // Pole i tread now centered about 0:
			assert(poleITread>=-nPoles/2.0 and poleITread<=nPoles/2.0, (@""));

			 // second try
			float have=0, forceGeom=0;
			if (abs(poleITread) < 0.5) {		// inside my pole
				have = 1.0;
				forceGeom = poleITread * 2;			// linear pull to 0
			}
			else								// inside some other pole
				forceGeom = poleITread<0.0? -1.0: poleITread>0.0? 1.0: 0.0;

			Port *portI = [self getPort:i];		// get Port i
			portI.valueTake = have;
			if (portI.valueChanged)
				[[@"" addF:@"%@| ShaftBundleTap: new have (%.3f)", portI.fullName16, have] ppLog];
			Port *wPort = portI.connectedTo;
			if (wPort.valueChanged)
				[[@"" addF:@"%@| ShaftBundleTap: new want (%.3f)", portI.fullName16, wPort.value] ppLog];
			float want = portI.connectedTo.valueGet;
			float dForce = want * forceGeom;
			force += dForce;
			//NSPrintf(@"%3d: have=%.3f want=%.3f dForce=%.3f\n",	i, have, want, dForce);
		}
		self.tread -= force * 0.05;		// y stu  0.005

		 // if tread has changed, keep simulator running
		if (force != 0)
			[self.brain kickstartSimulator];
	}
	[super simulateDown:downLocal];
}

- (Port *) getPort:(int)i; {
	assert(i<7, (@""));
	NSString *bitName = [@"" addF:@"%c", "abcdefg"[i]];

	Bundle *targetBundle = self.targetBundle;
	assert(targetBundle, (@"targetBundle of '%s' is nil", self.fullNameC));

	Port *port = [targetBundle genPortOfLeafNamed:bitName];
	assert(port, (@"%@: didn't find Port '%@', or it has no 'G' port", self.fullName, bitName));
	mustBe(Port, port);//
	return port;
}

#pragma mark 9. 3D Support
- (Bounds3f) gapAround:(View *)v;	{
	float r = self.brain.bitRadius;
	return Bounds3f(-5*r, -5*r, -5*r,	 5*r, 5*r, 5*r);}


#pragma mark 11. 3D Display
//// // // // // // // // // // // // // // // // // // // // // // // ///
/// // // // // // // // // // 3D Display  // // // // // // // // // ////
// // // // // // // // // // // // // // // // // // // // // // // // //
- (void) drawFullView:(View *)v context:(RenderingContext *)rc; {
	float r = self.brain.bitRadius;
	Vector3f siz = v.bounds.size();
	float radius = 2*r;

	glPushMatrix();
		glRotatef(90, 0,1,0);
		glRotatef(90, 1,0,0);

		for (int i=0; i<self.nPoles; i++) {
			glPushMatrix();
				float poleInDegrees = 360 * i / self.nPoles;
				glRotatef(poleInDegrees, 1,0,0);
				glTranslatef(0, 0, radius+r);

//				rc.color = portI.colorOfValue;

				Port *portI = [self getPort:i];
				rc.color = colorOf2Ports(0.0, portI.connectedTo.value, 0);
				myGlSolidCylinder(r, r/2, 16, 1);	// (radius, length, ...)
				glTranslatef(0, 0, r/2);
				rc.color = colorOf2Ports(portI.value, 0.0, 0);
				myGlSolidCylinder(r, r/2, 16, 1);	// (radius, length, ...)

				 /// Label Pole 'a', 'b', ...
				glPushMatrix();
					//glTranslatefv(v.bounds.center());
					Vector3f inCameraShift = Vector3f(0.0, 0.0, 3);	// in "facing camera" coordinates
					Vector2f spot2labelCorner = Vector2f(0.5, 0.5);	// box center --> llc
					rc.color = colorBlue;
					char poleChar = 'a' + i;
					id poleStr = [@"" addF:@"%c", poleChar];
					myGlDrawString(rc, poleStr, -1, inCameraShift, spot2labelCorner, 3);
				glPopMatrix();
			glPopMatrix();
		}

		float treadInDegrees = 360 * self.tread / self.nPoles;
		glRotatef(treadInDegrees, 1,0,0);

		rc.color = colorRed;
		glTranslatef(0, 0, radius/2);
		myGlSolidCylinder(radius/10, radius, 16, 1);	// (radius, length, ...)
		glTranslatef(0, 0, -radius/2);

		rc.color = colorBlack;
		glRotatef(90, 0,1,0);
		glutSolidTorus(radius/6, radius, 3, self.nPoles*4);// innerRadius, outerRadius, nsides, nRings
	glPopMatrix();

	//[super drawFullView/*Ports*/:v context:rc];	// paint Ports and bounding box
}

#pragma mark 15. PrettyPrint
    ///\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\///
   ///\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\///
  ///\\//\\//\\//\\//\\//\\//\\// PRINTOUT //\\//\\//\\//\\//\\//\\//\\//\\///
 ///\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\///
///\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\///
- (NSString *) pp1line:aux; {		id rv = [super pp1line:aux];

	rv=[rv addF:@"nPoles=%d, tread=%.3f", self.nPoles, self.tread];
	return rv;
}

@end
