// Generator.mm -- generates stimulus for a HaveNWant network C2013

#import "Part.h"
#import "FactalWorkbench.h"

#import "Common.h"
#import "NSCommon.h"
#import "Id+Info.h"

id aGenerator   (int limit, id etc) {
	id info					= [etc anotherMutable];
	info[@"eventLimit"]		= [NSNumber numberWithInt:limit];

	Generator *newbie 		= aGenerator(info);
	return newbie;
}

id aGenerator   (id  etc) {
	id msg=[@"" addF:@"Expanding aGenerator(%@)\n", [etc pp]];
	id name 				= etc[@"named"]?: @"g";
	id gen 					= [name addF:@"Gen"];		/// My name

	/// A Generator has 3 parts:
	id mod 					= [name addF:@"Mod"];
	id clk	 				= [name addF:@"Clk"];
	id tap					= [name addF:@"Tap"];
	id wmNeeded				= nil;

	id genArgs				= @{@"named":gen, flip}.anotherMutable;
	if (id _flip 			= etc[@"flip"])		// (ugly:)
		genArgs[@"flip"]	= [@"" addF:@"%d", [genArgs[@"flip"] intValue] ^ [_flip intValue]];
	msg = [msg  addF:@" ----->         aNet(%@)\n", [genArgs pp]];

	 /// WORLD MODEL (wm):
	// WorldModel.mm -- Basic discrete time/value data source
	id wmArgs				= @{@"named":mod, flip}.anotherMutable;
	if (id events 			= etc[@"events"])
		wmArgs[@"events"] 	= wmNeeded = events;
	if (id prob 			= etc[@"prob"])
		wmArgs[@"prob"] 	= wmNeeded = prob;
	if (id eventLimit		= etc[@"eventLimit"])
		wmArgs[@"eventLimit"]= eventLimit;

	if (wmNeeded)
		msg = [msg  addF:@"  +      aWorldModel(%@)\n", [wmArgs pp]];
	else
		wmArgs				= @{@"abort":@1};	// make no World Model


	 /// TIMING CHAIN: Discrete Time Events
	id tcArgs				= @{@"named":clk, @"P=":tap, flip}.anotherMutable;
	if (wmNeeded)
		tcArgs[@"S="] 		= mod;			// connect mod up to timingChain
	if (id addBundleTap		= etc[@"addBundleTap"])
		tcArgs[@"addBundleTap"] = addBundleTap;
	msg = [msg  addF:@"   +    aTimingChain(%@)\n", [tcArgs pp]];


	 /// BUNDLE TAP: (bt) -- Connects to HnW Network
	id btArgs				= @{@"named":tap, flip}.anotherMutable;
	if (id nib 				= etc[@"nib"])
		btArgs[@"nib"] 		= nib;
	if (id P				= etc[@"P"])		// target of bundleTap
		btArgs[@"P"] 		= P;
	if (id resetTo			= etc[@"resetTo"])
		btArgs[@"resetTo"]	= resetTo;
	msg = [msg  addF:@"    +     aBundleTap(%@)\n", [btArgs pp]];

	if (id incrementalEvents = etc[@"incrementalEvents"])
		btArgs[@"incrementalEvents"] = incrementalEvents;

	[msg ppLog];

	id rv = aNet(genArgs, @[
		aWorldModel	(wmArgs),
		aTimingChain(tcArgs),
		aBundleTap	(btArgs),
	  ]);
	return rv;
}


