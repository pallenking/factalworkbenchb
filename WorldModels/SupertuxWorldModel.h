//  SupertuxWorldModel.h -- Supertux game, driven by LowerBrain C2011 King Software

#import "WorldModel.h"
#import "ProtoBrain.hpp"
#import "Supertux.h"

@interface SupertuxWorldModel: WorldModel    {
		ProtoBrain *simpleBrain;          // C++ class
		Supertux *game;
		
		CommandDatum cmd;           // buttons to Supertux
		SensoryDatum senses;        // screen blocks from supertuxp
		int sensesFull;
		NSRect fovia;               // rect of screen icons for fovia
    }

	@property (nonatomic, assign) float stuxEndTime;

@end
