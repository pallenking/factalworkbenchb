//  TimingChain.h -- Insert samples into HaveNWant Network C2017PAK

/*!
	Manages discrete time step Data

	Takes data from WorldModels

	Puts data to BundleTaps of HaveNWant Network
		Supports multiple BundleTaps

	Supports key DOWN and UP animations
		Supports async Data by changing clocks to Previous units
		Supports Conceive and Labor messages to WriteHeads
 */

//#import "Part.h"
#import "Atom.h"
@class BundleTap, WorldModel, GenAtom;

@interface TimingChain : Atom//Part

	 /// our BOSSES: (presuming sensor orientation)
	@property (nonatomic, retain) WorldModel *worldModel;	// of WorldModels

	 /// our WORKERS:
	@property (nonatomic, retain) BundleTap *bundleTap;		 // of Bundle
	@property (nonatomic, retain) BundleTap *addBundleTap;// another bundleTap 

	 /// our Status:
    @property (nonatomic, retain) id		eventX;		// event being executed
	@property (nonatomic, assign) char		state;		// of timing chain
	 // A user key or button is down, halt insertion sequence: (takeEvent/releaseEvent)
	@property (nonatomic, assign) char		userDownPause;

	  /// Basic Op Mode: Variations in Insertion Cycle:
	 // Issue previous clock late in the cycle
	@property (nonatomic, assign) char		asyncData;	// change clocking
	//
	// Synchronous Data: (lists, sync world model)
	// 	downstroke: capture old data at time t, then load new data of t+1.
	//		Calculate Unknowns, and conceive a network modification to learn.
	//	upstroke: release the modification and allow the Network to settle
	//
	// Asynchronous Gui Data with Snapshot:
	//	User adjusts sliders and buttons, changes propigate through the network
	//	User presses button to capture existing data. (optional)
	//

		//______Sync_Data:______________________Async_Data:_________
		//		01: when DOWN:                 	01: when DOWN:
		//		- sim_clockPrevious;			 - nop (don't clock or
		//		- load TargetBundle:;					change data)
		//		02: when SIM SETTLED  			02: when SIM SETTLED
		//		- sim_writeHeadConcieve;		- sim_writeHeadConcieve;
		//		- sim_writeHeadLabor;			- sim_writeHeadLabor;
		//		03: when UP						03: when UP
		//		- sim runs						- sim runs
		//		03: when SIM SETTLED			03: when SIM SETTLED
		//		 - nop							- sim_clockPrevious;
		//										- ?retract;
		//		04: SIM SETTLED					04: when SIM SETTLED
		//		- done							- done

	  // Set by user (esp PushButtonBidirNsV.mm):

//	@property (nonatomic, assign) char		incrementalEvents;

	 // Retract 1:N assertion when button UP
	@property (nonatomic, assign) Port 		*retractPort;

#pragma mark 7. Simulator Messages
- (bool) 		processKey:(char)character modifier:(unsigned long)modifier;
- (void) 		takeEvent:event;
- (void) 		releaseEvent;
- (void) 		resetForAgain;

#pragma mark 8. Reenactment Simulator
- (void) 		cycleStateMachine;

@end
