//  BundleTap.h -- an Atom which generates data for a Bundle C2014PAK

/*!
	Loads discret time discrete value data into analog HaveNWant Bundle
	Resets Bundle at start of run
	Data may come from GUI
	Data may come from worldModel.
		a) Incremental change model, including "again"
		b) anonValue ("a" -> "a=<ananVlue>")
		c) random data for initial testing
		d) per-run random data
		e) Epoch Marks
*/

#import "Atom.h"
@class Bundle, TimingChain;

@interface BundleTap : Atom

	 /////// Sources of data:

	 // Construction properties
    @property (nonatomic, retain) NSString *inspectorNibName;		// "nib"
    @property (nonatomic, retain) id		resetTo;				// event at reset
	@property (nonatomic, assign) float		heightAlongRightSide;	// vert placement
	@property (nonatomic, assign)/*IBOutlet*/char incrementalEvents;// Next event inherets previous

	@property (nonatomic, assign) char		inspectorAlreadyOpen; 	// kind of a hack

	  // Sometimes just a name and no floating point value is specified.
	 //   e.g: "a". What is meant is a=anonValue. Typically anonValue = 1.0
	@property (nonatomic, assign) float		anonValue;

	 /////// where we put our data
	@property (nonatomic, retain) Bundle	*targetBundle;

#pragma mark 8. Reenactment Simulator
- (void)		loadPreClear;
- (void)		loadTargetBundle:event;
- (void) 		resetForAgain; 

@end
