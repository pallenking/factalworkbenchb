//  ShaftBundleTap.h -- Stepper Motor with inertia C2016PAK

//#import "WorldModel.h"
#import "BundleTap.h"

@interface ShaftBundleTap : BundleTap
//@interface ShaftBundleTap : WorldModel

	@property (nonatomic, assign) int	nPoles;

	  // distance tread of wheel (on ground) has moved forward
	 //   (one revolution does tread += nPoles)
	@property (nonatomic, assign) float tread;


@end
