//  TurtlePenWorldModle.h -- The 2D Environment of a Turtle in a Pen C2016PAK

#import "WorldModel.h"

typedef struct {
	float tread;
	float dTread;
  } Wheel;

@interface TurtlePenWorldModel : WorldModel {
	Wheel wheels[2];
  }

//	Wheel *wheels;

//	@property (nonatomic, assign) float Wheel wheel;

	@property (nonatomic, assign) float treadA;
	@property (nonatomic, assign) float treadB;


@end
