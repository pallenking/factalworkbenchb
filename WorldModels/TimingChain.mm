//  TimingChain.mm -- Insert samples into HaveNWant Network C2017PAK

#import "TimingChain.h"
#import "WorldModel.h"
#import "Brain.h"
#import "BundleTap.h"
#import "Path.h"
#import "GenAtom.h"
#import "FactalWorkbench.h"
#import "View.h"

#import "Id+Info.h"
#import "Common.h"
#import "NSCommon.h"
#import "GlCommon.h"
#import <GLUT/glut.h>
#import "Colors.h"

@implementation TimingChain

#pragma mark 1. Basic Object Level
- init; {											self = [super init];

	self.asyncData 			= false;
	return self;
}

- (void) dealloc {
	self.worldModel			= nil;
	self.bundleTap			= nil;
	self.addBundleTap		= nil;
	self.eventX				= nil;

    [super dealloc];
}

#pragma mark  4a Factory Access						// Methods defined by Factory
- (id) atomsDefinedPorts;	{				id rv = [super atomsDefinedPorts];
	
	rv[@"S"]		= @"pc";	// Secondary (always create)
	return rv;
}

#pragma mark 4. Factory
TimingChain *aTimingChain(id etc) {
	TimingChain *newbie = anotherBasicPart([TimingChain class]);
XX	newbie = [newbie build:etc];
	return newbie;
}

- (id) build:(id)info;	{					[super build:info];

//!	xzzy1 BundleTap::	1. addBundleTap :<string> --  
//!	xzzy1 BundleTap::	2. asyncData    :<bool>   --  computNClock v.s. clockNCompute

	self.addBundleTap	= [self takeParameter:@"addBundleTap"];
	self.asyncData 		= (char)[[self parameterInherited:@"asyncData"] intValue];

	return self;
}

/// Occurs after build, when the object is embodied...

- (void) postBuild; {						[super postBuild];

	if (self.postBuilt==0) {	// when never been initialized before
		self.postBuilt 			= 1;

		// User specifies as P and S Ports, but needed in self.worldModel
		if (Port *myPPort 		= self.pPort.portPastLinks) {
			Atom *myPAtom 		= mustBe(Atom, myPPort.atom);
			if (coerceTo(BundleTap, myPAtom))
				self.bundleTap	= mustBe(BundleTap, myPAtom);
			else
				panic(@"TimingChain's P Port must be connected to a BundleTap\n"
						"\tSometimes this is from an auto-inserted");
		}
		if (Port *mySPort = self.sPort.portPastLinks) {
			Atom *mySAtom 		= mustBe(Atom, mySPort.atom);
			if (id s = coerceTo(WorldModel, mySAtom))
				self.worldModel	= s;
			else
				panic(@"TimingChain's S Port must be connected to a WorldModel");
		}
		 // resolve addBundleTap
		if (id addBundleTap 	= self.addBundleTap) {
			mustBe(NSString, self.addBundleTap);
			Path *obtPath		= [Path pathWithName:self.addBundleTap];
	XX		Part *bt 			= (id)[self resolveOutwardReference:obtPath openingDown:false];

			if (id b = coerceTo(BundleTap, bt))
				self.addBundleTap = b;
			else
				panic(@"TimingChain's addBundleTap must be connected to a BundleTap");
		}

	  	 // Let our Brain know of us:
		[self.brain.timingChains addObject:self];
	}
}

#pragma mark 7-. Simulation Actions
- (void) reset; {								[super reset];
	
	self.state 				= 0000;
	self.userDownPause		= 0;
	
//XX[self.bundleTap reset];
}

- (void) resetForAgain; {
XX		[self.bundleTap	   resetForAgain];
XX		[self.addBundleTap resetForAgain];
}

#pragma mark 7. Simulator Messages
  /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// ///
 /// /// /// /// /// /// ///  INTERFACE TO GET DATA  /// /// /// /// /// /// ///
/// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// ///

  //// Prosses keyboard key and control release of events[]
 ///
//SwiftFactals: 	func process(with nsEvent:NSEvent, inView view:View?) -> Bool
- (bool) processKey:(char)character modifier:(unsigned long)modifier; {
	if (!(modifier & FWKeyUpModifier)) {////// key DOWN ///////

XX		if ([self.worldModel processKey:character modifier:modifier])
			 // World Model recognized the key, see if an event results
			if (id event = self.worldModel.dequeEvent) {

				[self takeEvent:event];		// A keystroke generates an event
 // 180806 Tried to remove to make  ^listenSpeak() work
//				self.asyncData = false;		// set to process as Synchronous	//why //xyzzy11c
			}
	}
	else if (self.userDownPause)		//////// key UP ///////
		[self releaseEvent];				// retract event

	return true;
}

  // -- // -- // -- // -- // -- // -- // -- // -- // -- // -- // -- // -- //
 // -- // -- // -- // -- // - Major Interface: - // -- // -- // -- // -- //
// -- // -- // -- // -- // -- // -- // -- // -- // -- // -- // -- // -- //

 // Get an event from users (e.g. PushButtonBidirNsV, keyboard, ...)
- (void) takeEvent:(id)event; {

	if (self.state == 0){	/// FREE TO TAKE EVENT
		[self logEvent:@"    TimingChain: Take Event '%@'", [event pp]];
		assert(self.eventX==nil, (@""));
		self.eventX			= event;	// Symbolic, Destined for targetBundle

		 // The following defaults can be changed
		self.retractPort	= 0;		// default param
//		self.asyncData		= true;		// default param						//xyzzy11c
		self.userDownPause	= 1;		// assert lock, which blocks till up

		self.state 			= 0001;		// Start Timing Chain
	}
	else {					/// BUSY
		[self.worldModel addDelayedEvent:event];
		[self logEvent:@"    TimingChain Busy, adding '%@' to delayed events Que", [event pp]];
	}
	return;
}
- (void) releaseEvent; {
	[self logEvent:@"    TimingChain: Release Event"];
	self.brain.simEnable	++;			// restart, perhaps even before stopping
	self.userDownPause		= 0;		// assert lock, which blocks till up
//	self.retractPort.valueTake = 0.0;
	return;
}

#pragma mark 8. Reenactment Simulator
//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*
//**//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//
//***//*//*//*//*//*//      Reenactment Simulator     //*//*//*//*//*//*//*
//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*
//**//*//*//*//*//*/s*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//

- (void) cycleStateMachine; {
	Brain *brain		= self.brain;
	id eventX 			= self.eventX;

	int nextState		= 0;
	switch (self.state) {
	default:					panic(@"state=%04o UNDEFINED", self.state);

	break; case 000:			/// Idle. (Needs -takeEvent: to activate)
		return;

	break; case 0001:			/// When Settled do 'ad1:?cPrev,lData'
		assert(brain.simEnable>=1, (@"Simulator must be enabled to process an event"));
		if (!brain.settled) 		// Sim unsettled or not enabled
			return;						// do nothing
													// ## 1. Await Sim Settled
		if ( self.asyncData)
			[self logEvent:@"//// %02o=>State; Sim Settled; Asynchronous Data Mode: nop", self.state];
		if (!self.asyncData) {
			[self logEvent:@"//// %02o=>State; Sim Settled; Synchronous Data Mode: cPrev;lData", self.state];

XX			[brain sendMessage:sim_clockPrevious];	// ## 2. do EARLY Clk Previous

XX			[self.bundleTap loadTargetBundle:eventX];//## 3. load target bundle
XX			[self.addBundleTap loadTargetBundle:eventX];//	 (and its alternate)
		}
		self.eventX				= nil;		// done with eventX, even if async

		nextState				= 0002;
	break; case 0002:			/// When Settled do 'ad2:Conceive'
		assert(brain.simEnable>=1, (@""));
		if (!brain.settled) 		// First, Await simSettled
			return;
		[self logEvent:@"|||| %02o=>State; Sim Settled; Now do 'ad2:Conceive'", self.state];
													// ## 4. Await Sim Settled

XX		[brain sendMessage:sim_writeHeadConcieve];	// ## 5. do: CONCEIVE:
XX		[brain sendMessage:sim_writeHeadLabor];		// ## 6. do: LABOR, BIRTH:


		   ///(unfortunately conceive leaves SIM unsettled//newb->birth canal
		  ///    To keep prev activations levels frozen and newborn in canal,
		 ///		disable simulator.
		brain.simEnable			--;		// usually --> simEnable<=0 --> don't run
		// 171021 If release occurs before here, ++ and -- still cancel to +=0


		nextState				= 0003;
	break; case 0003:			/// When Settled AND UsrUp do '?cPrev ?retract'
		if (self.userDownPause)		// do not pass if user pause is down
			return;
		if (!brain.settled) 		// Await simSettled
			return;
		[self logEvent:@"|||| %02o=>State: userUpEvent and Sim Settled.  Now do 'ad3:?cPrev'", self.state];
			
													// ## 8. Let Newbie run
		self.userDownPause	= 0;	// turn it OFF

		 //	 !asyncData used in Morse Code (F1, F2, F3)
		if (self.asyncData) 						// ## 9. LATE Previous Clk
XX			[brain sendMessage:sim_clockPrevious];

		if (self.retractPort)
			self.retractPort.valueTake = 0.0;
		self.retractPort		= nil;


		nextState				= 0004;
	break; case 0004:			/// When Settled, we're done!
		assert(brain.simEnable==1, (@""));
		if (!brain.settled) 		// Await simSettled
			return;
		[self logEvent:@"\\\\\\\\ %02o=>State; Sim Settled;  EVENT DONE", self.state];


		nextState				= 0000;				// ** 11. go idle
	}

	self.state = nextState;					// Enter next state
	return;
}

- (int) unsettledPorts;		{			return 0;				}
- (id) ppUnsettledString;	{			return nil;				}


#pragma mark 9. 3D Support
- (Bounds3f) gapAround:(View *)v; {
	Bounds3f b = Bounds3f( -2, -1, -2, 		2, 1,  2);
	return b;
}

- (Matrix4f) positionOfPort:(Port *)port inView:(View *)bitV; {
	Vector3f bitSpot = zeroVector3f;
	
	if ([port.name isEqualToString:@"P"])
		return [port suggestedPositionOfSpot:Vector3f(0,-2, 0)];

	if ([port.name isEqualToString:@"S"])
		return [port suggestedPositionOfSpot:Vector3f(0,2, 0)];

XR	return [super positionOfPort:port inView:bitV];
}

#pragma mark 11. 3D Display
- (void) drawFullView:(View *)v context:(RenderingContext *)rc; {
	assert(self.state<5, (@""));

	FColor colors[] = {colorBlue, colorGreen, colorYellow, colorOrange, colorRed};
	rc.color = colors[self.state];
	glPushMatrix();
		float r = self.brain.bitRadius;
		glTranslatefv(v.bounds.center());// + Vector3f(2*r, 2*r, 0));
		glRotatef(90, 0,0,1);
		glutSolidCube(2*r);
		glRotatef(45, 1,0,0);
		glutSolidCube(2*r);        // radius, height, slices, stacks, ends
	glPopMatrix();
	glPushMatrix();
		rc.color = colorBrown;
		glTranslatef(0, 0.5*r, 0);
		glRotatef(90, 1,0,0);
		myGlSolidCylinderCommon(2*r, 0.8*r, 8, 1, true);
	glPopMatrix();

	[super drawFullView/*Ports*/:v context:rc];	// paint Ports and bounding box
}

#pragma mark 15. PrettyPrint
///////////////////////// Stringsss for Printing  /////////////////////////
- (NSString *) pp1line:aux; {		id rv=[super pp1line:aux];

	rv=[rv addF:@"bunTap='%@' ", self.bundleTap.name];
	if (self.addBundleTap) {
		if (BundleTap *bt = coerceTo(BundleTap, self.addBundleTap))
			rv=[rv addF:@"o='%@' ", bt.name];
		else if (id bt = coerceTo(NSString, self.addBundleTap))
			rv=[rv addF:@"o='%@' ", bt];
		else
			panic(@"");
	}
	rv=[rv addF:@"wModel='%@' ", self.worldModel.name];
	
	if (self.eventX)
		rv=[rv addF:@"eventX:%@ ", [self.eventX pp]];
	
	rv=[rv addF:@"in STATE %03o ", self.state];

	rv=[rv addF:@"(uDnPause=%d) ", self.userDownPause];

	return rv;
}

@end
