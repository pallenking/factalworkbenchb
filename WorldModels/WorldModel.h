// WorldModel.h -- Basic discrete time/value data source C2017PAK

/*!
	Generates discrete events
		from source code
		from random number (for quick testing)
		to/from a file (unimplemented)
		from specific code (e.g. SupertuxWorldModel
	Limits the number of events automatically released
 */


#import "Atom.h"
@class TimingChain;

@interface WorldModel : Atom//Part

	@property (nonatomic, assignWeak) TimingChain *timingChain;
	@property (nonatomic, retain) NSMutableArray *delayedEvents;


	 //////////    E V E N T    OUTPUT METERING         /////////
	@property (nonatomic, assign) int			eventNow;	// current event number (>=1)
	@property (nonatomic, assign) int			eventLimit;	// stop if eventNow would be > eventLimit;
															//  (-1 implies no limit)
	 //////////    E V E N T    G E N E R A T I O N     /////////
	 // Events in Hash
    @property (nonatomic, retain) NSArray		*events;	// to get event data
    @property (nonatomic, assign) int			eventIndex;	// current index in events
	 // Simple Probability
	@property (nonatomic, assign) float			prob;		// nan is OFF

#pragma mark 4. Factory
   id			eventUnrand(id events);

#pragma mark 7. Simulator Messages
- (bool) 		processKey:(char)character modifier:(unsigned long)modifier;
- (id)			dequeEvent;			// return the next event generated or nil
- (void)		addDelayedEvent:event;

@end
