# some temporary (helpful) definitions to save typing:
######## COMMANDS for ./supertux 
define rr
	run data/levels/world1/01\ -\ Welcome\ to\ Antarctica.stl
	end
define rc
	run --remote-control data/levels/world1/01\ -\ Welcome\ to\ Antarctica.stl
	end

#define ki
#    p kill()
#    end
define KILL
    p kill(0, 9)
    end

######## COMMANDS for ./brain_v*
define rbc
	run --run_tux --seed 98765432 --section drop-jump
	end


########

# reread this file macro:
define gdbinit
	source .gdbinit
	end

define m
	make
	end

define j
	jam
	end

# to make addresses repeatable between runs, start gdb with:
#       setarch i386 -R gdb --annotate=3 ...
# http://gcc.gnu.org/wiki/DebuggingGCC

#set radix 16
set confirm no
set print static-members off

#define po
#	print-object
#	end
#alias po print-object
# print print_contents()
define pc
	p print_contents()
	end

define u0
	up 0
	end

define ret0
	return
	up 0
	end

#set print pretty on
#set prompt --->
set verbose on
