// Supertux.mm -- creates a simulated Brain using the Supertux game
/*
	PROVIDES: high-level access to supertux Brain

	MANAGES: start supertux process (optional)
			 multiple buffers per screen
			 permanent strings

	USES: PortClient, to connect to and communicate with the supertux game
		  SensoryInfo, to digest meaning of screen rectangles
 */

#include "Supertux.h"
char *supertux2executableDir = (char *)		// N.B: NO TRAILING /
	//"/Users/allen/src/supertuxNbrain/supertux/";
	"/Volumes/Macintosh HD/Users/allen/src/supertuxNbrain/supertux";

char *supertux2executableName = (char *)"supertux2";

#include "SensoryInfo.h"
#include "PortClient.hpp"
	inz_typePrintStrings;
#include "Common.h"

#include <signal.h>

Supertux::Supertux()  {
	online = 0;
	supertux_pid = 0;						// pid of supertux game
	supertuxPortClient = 0;
	recordFile = playFile = 0;
	portClientDebug_ = 0;

	sinfo = 0;
	for (unsigned int i=0; i</*screenPiece::*/LAST_PERM; i++)
		permStrings[i] = 0;
	nextPermIndex = /*screenPiece::*/FIRST_PERM;
}

Supertux::~Supertux() {
	delete sinfo;
	delete supertuxPortClient;
	if (supertux_pid)
		; // kill -9 supertux_pid
}

int Supertux::inz (int &argc, char *argv[]) {
	bool helpMode = false;
	char *host = (char *)"127.0.0.1";
	int portNo = 3576;
	char *password = (char *)"rall35Tu7";
//	char supertuxServer[] = "supertux2";
	char *seed = 0;
	bool run_tux = false;
	bool noTux  = false;
	currentGameTime_ = 0.0;

	for (int i=1; i<argc; i++) 	{// PARSE COMMAND LINE ARGS
		if (argv[i] == 0)
			continue;						// ignore already digested args
		std::string arg(argv[i]);
		bool understood = true;

		if (arg == "--help" || arg == "-h") 
			helpMode = true;
		else if (arg == "--host") {
			argv[i] = 0;
			host = argv[++i];
		}
		else if (arg == "--port") {
			argv[i] = 0;
			if (sscanf(argv[++i], "%d", &portNo) != 1)
				helpMode = true;
		}
		else if (arg == "--password") {
			argv[i] = 0;
			password = argv[++i];
		}
		else if (arg == "--seed") {
			argv[i] = 0;
			seed = argv[++i];
		}
		else if (arg == "--run_tux") 
			run_tux = true;
		else if (arg == "--noTux") 
			noTux = true;
		else if (arg == "--playFile") {
			argv[i] = 0;
			playFile = fopen(argv[++i], "r");
		}
		else if (arg == "--recordFile")  {
			argv[i] = 0;
			recordFile = fopen(argv[++i], "w");
		}
		else if (arg == "--noTux")
			noTux = true;
		else
			understood = false;

		if (understood) 
			argv[i] = 0;			// mark as processed
	}
	removeNullArgs(argc, argv);

	if (helpMode) {					// DISPLAY HELP MESSAGE
		printf(
			"brain_v1 [options]\n"
			"	--help				display this help message\n"
			"	--host HOST			contact server named HOST\n"
			"	--port PORT			contact server on port PORT\n"
			"	--password PASSWORD	use PASSWORD when contacting server\n"
			"	--seed SEED			start server with random seed SEED\n"
			"	--run_tux			start a tux server on LOCALHOST\n"
		    "	--recordFile FILE	record game sensations experienced to FILE\n"
			"	--playFile FILE		play back sensations from FILE\n"
			);
		exit(1);
	}
	if (noTux)
		return 0;
    
    if (run_tux) {

		if (pid_t rv_pid = fork()) {	// START SERVER WORLD OF SUPERTUX

			 // PARENT returns:
			if (rv_pid < 0) {				// parent fork error
				perror("ERROR, unable to fork supertux server");
				exit(0);
			}
			else {                      // parent
				printf("Parent process returns, sleeping\n");
				sleep(5);				// for good luck, to allow child to start?
				supertux_pid = rv_pid;
			}
		}
		else {

			 // CHILD returns:
			char pathname[1000];
			getcwd(pathname, 1000);
			printf("\n**** Supertux server startup ***\n");
			printf("**** (FW executes in: '%s')\n", pathname);

			chdir(supertux2executableDir);
			getcwd(pathname, 1000);
			assertC(strlen(pathname)<1000-100, ("buf too small"));
			printf("**** Stux (child) to execute in: '%s'\n", pathname);
			sprintf(pathname, "%s/%s", supertux2executableDir, supertux2executableName);

			sigset_t intMask;
			sigemptyset(&intMask);
			sigaddset(&intMask, SIGINT);
			sigprocmask(SIG_BLOCK, &intMask, 0);

            // why are these args hard wired?
			printf("**** execlp starting supertux2:\n\n");
//			printf("**** execlp(supertux2, supertux2, '%s', '%s', '0')\n",
//                   "--remote-control",
//				   "data/levels/world1/01 - Welcome to Antarctica.stl");
			fflush(stdout);		// await output, then:

			execlp(
					pathname, pathname,
					"--remote-control",
					"data/levels/world1/01 - Welcome to Antarctica.stl",
					(char *)NULL);
			perror("**** execlp returned ");

			printf("********* child to start running soon *****\n");
            freopen("/dev/NULL", "a", stdout);
            freopen("/dev/NULL", "a", stderr);

			exit(0);
		}
	}

	if (playFile==0) {
		// open connection to supertux
		supertuxPortClient = new PortClient;	// connection to supertux
		supertuxPortClient->debug = portClientDebug_;
		while (supertuxPortClient->contactServer(host, portNo, password, seed) < 0)
			sleep(1);
		online = 1;
	}
	sinfo = new SensoryInfo;                    // create an analyzer for sensory information
	sinfo->debug = sensoryInfoDebug_;
	return 0;
}

void Supertux::portClientDebug (int val)	  {		portClientDebug_  = val; }
void Supertux::sensoryInfoDebug (int val)	  {		sensoryInfoDebug_ = val; }
void Supertux::setFoviaSize (int x, int y, int h, int w)   {
	assertC(sinfo, ("why?"));
	sinfo->foviaDrawSize(x,y,h,w);
}
worldLoc Supertux::getTuxLoc()				  {	return sinfo->getTuxLoc();	 }
float Supertux::currentGameTime()			  {	return currentGameTime_;	 }


//int Supertux::inzViews (void *textureView, void *textureView, void *mapViewin)   {
////int Supertux::inzViews (GeneralViewin *textureView, GeneralViewin *textureView, GeneralViewin *mapViewin)   {
////	sinfo = new SensoryInfo;
//    sinfo->inzViews(textureView, textureView, mapViewin);			// analyzer of sensory information
//    return 0;
//}

// Run the Supertux Brain with a command (buttons + time)
SensoryDatum Supertux::takeAction(CommandDatum &cmd) {
	SensoryDatum rv;
	if (playFile) {
		// cmd is ignored
		//  rv = ...; 
	}
	else {
		portBuf buf;
		if (!online)
			return sensoryDatumNULL;
		
		// handle some special conditions before going to game
		switch (cmd.buttons) {
			break; case bbRECORD:
				buf.buttons = 'D';
				sprintf(buf.buf, "%s", cmd.comment);
				panicC("not yet coded");
				
			break; case bbPLAY:
				buf.buttons = 'E';
				sprintf(buf.buf, "%s", cmd.comment);
				panicC("not yet coded");
				
			break; case bbJOYSTICK:
				buf.buttons = 'E';
				buf.buf[0] = '\0';
				panicC("not yet coded");
		}
		
		// === send buttons to supertux
		buf.buttons = cmd.buttons;
		currentGameTime_ += buf.time = cmd.duration;
		sprintf(buf.buf, cmd.comment, buf.time);
		printf("== ################ %s generates buttons 0x%03x at %.4f(+=%.4f)\n", 
				   buf.buf, buf.buttons, currentGameTime_, cmd.duration);

		supertuxPortClient->sendBuffer(buf, 'B');		// ##########
		
		if (cmd.buttons == bb(EXIT)) {
			sleep(1);	
			return sensoryDatumNULL;
		}
//		if (cmd.maxChunkTime <= 0 && cmd.buttons == bb(BREAKPOINT))
//			doBreakpointInstruction;
		
		// ============================== get return screen info, and parse it
		// receive results and display:
		for (char cmd='?'; cmd != 'l'; ) {
			supertuxPortClient->receiveBuffer(buf);		// ##########
			
			cmd = buf.cmd;
			for (int len=0, i=0; i<buf.len; i+=len)  {
				screenPiece *s = (screenPiece *)&buf.buf[i];
				char *rcvdString = 0, *defStr = (char *)"";
				len = sizeof(screenPiece);
				int strNum = /*screenPiece::*/NO_STRING;
				
				// retrieve possible trailing string from buffer
				if (s->stringNum==/*screenPiece::*/STRING || 
					     s->stringNum==/*screenPiece::*/PERM_STRING) {
					rcvdString = (char *)s + len;
					len += strlen(rcvdString) + 1;
					if (s->stringNum==/*screenPiece::*/PERM_STRING)   {
						// enter permanent string into hash
						permStrings[strNum=nextPermIndex++] = strdup(rcvdString);
						defStr = (char *)"DEF ";
                	}
				}
				// look up previously defined permanent string
				if (s->stringNum >= /*screenPiece::*/FIRST_PERM) {
					assertC(s->stringNum<nextPermIndex,
                        ("%d --> Permanent string #%d not yet defined", s->uid, s->stringNum));
					rcvdString = permStrings[strNum=s->stringNum];
				}
				
				// tell user via callout the screen piece found
				// Coordinates are normalized to a w=400, h=300 screen here.
				int x = SpSignExtendP(s->x)/2, y = SpSignExtendP(s->y)/2;
                
				sinfo->registerScreenPiece(buf.time,    // ##########
                       s->type, x, y, s->w/2, s->h/2,
                       defStr, strNum, rcvdString, s->uid);
			}
        }
		rv = sinfo->analyze();                          // ##########
        
		sinfo->drawScreen();
	}
	if (recordFile)
		;// writeSensoryDatum(&rv, recordFile: write out rv

	return rv;
}

void Supertux::printContents(int indent, char *tag) {
    printf("%sSupertux:\n", tag);
}

void Supertux::printSummary(/*int indent, char *tag*/) {
//	printf("%*s%sSupertux:", indent*3, "", tag);
    sinfo->SensoryInfo::printSinfo();
//	printf("\n");
}

