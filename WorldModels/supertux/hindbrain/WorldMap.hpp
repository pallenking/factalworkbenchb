// WorldMap.hpp -- keep track of a scenery rectangles in a world map

#ifndef __WorldMap_hpp__
#define __WorldMap_hpp__

#include <vector>
#include <map>
#include <string>

#include "SceneryRect.hpp"
#include "portDefs.hpp"
//#import "vmath.hpp"
#include "vector.hpp"
#define Vector2f Vector
class SceneryRect;

class WorldMap {
  public:
	WorldMap();
	~WorldMap();
	void recordMapElement(int type, int buid, int x, int y);
	void analyze();
	int setViewin(void *w);
	Vector2f getCamera();
	char *getZone();

  private:
	int debug;
	SceneryRect *here_now;
	SceneryRect *known_world;

	 // ========== VISUAL CONTEXT
	Vector2f cam;
	void *mapViewin;
};

#endif // __WorldMap_hpp__
