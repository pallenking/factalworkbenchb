// SceneryRect.cpp -- receive screen information from supertux, for brains

#include "SceneryRect.hpp"
#include "portDefs.hpp"
#include "Common.h"
#include <stdlib.h>

SceneryRect::SceneryRect(int w_, int h_, char *zone) {
	printf("%lx: %d x %d '%s'\n", (unsigned long)this, w_, h_, zone);
	zoneName = zone;
	_h = h_;
	_w = w_;
	data = (ubid *)calloc(_h * _w, sizeof(ubid));
	assertC(data, ("bad calloc"));
}

SceneryRect::~SceneryRect() {
	free(data);		data = 0;
}

void SceneryRect::clear() {
	printf("(SceneryRect)@%lx::clear()\n",  (unsigned long)this);
	for (int i=_w*_h-1; i>=0; i--) 
		data[i] = zeroBuid;
}

char * SceneryRect::getZoneName()   {		return zoneName; }

// poor man's iterator:'
ubid *SceneryRect::nextPoint(int &x, int &y) {
	 // address next point
	if (x<0 || y<0)			// start state
		x = y = 0;
	else if (++x < _w)		// next point
		;						// not end of line		
	else if (++y < _h)		// next line
		x = 0;					// not end of page
	else   {					// end of page
		x = y = -1;				// reset to start (out of kindness)
		return 0;				// NULL signifies end 
	}
	return &at(x,y);
}

ubid &SceneryRect::at(int x, int y) {
	int ind = y*_w + x;
	if (!(x>=0 && x<_w && y>=0 && y<_h && ind >=0 && ind < _w*_h))
		panicC("Illegal SceneryRect reference .at(%d,%d)", x, y);
	return data[ind];
}

 // Place Scenery Rectangle "b" at x,y of self, and return the correlation
float SceneryRect::correlate(SceneryRect &b, int x, int y) {
	int aX = 0, aY=0, aW=_w,   aH=_h;		// "A" == "this"
	int bX = 0, bY=0, bW=b._w, bH=b._h;
	int width, height;

	// take AND of two rectangles
	if (x > 0) {
		aX += x; aW -= x;
	}
	else {
		bX -= -x;
		bW += x;
	}
	width = min(aW, bW);
//	width = std::min(aW, bW);

	if (y > 0) {
		aY = y;
		aH -= y;
	}
	else {
		bY = -y;
		bH += y;
	}
	height = min(aH, bH);
//	height = std::min(aH, bH);

	// paw through the rectangle, recording overlaps
	int match = 0, tested=0;
	for (int x=0; x<width; x++) {
		for (int y=0; y<height; y++) {
			ubid au = at(x+aX, y+aY);
			ubid bu = b.at(x+bX, y+bY);
			if (au.type==BLOCK_HARD || au.type==BLOCK_HARD) {
				tested++;
				if (au == bu)
					match++;			// both have same map entry	
			}
		}
	}
	return tested? match/(float)tested: 0;
}

void SceneryRect::add_elements(SceneryRect &b, int x, int y) {
	int aX = 0, aY=0, aW=_w,   aH=_h;		// "A" == "this"
	int bX = 0, bY=0, bW=b._w, bH=b._h;
	int width, height;

	// take AND of two rectangles
	if (x > 0) {
		aX += x;
		aW -= x;
	}
	else {
		bX -= -x;
		bW += x;
	}
	width = min(aW, bW);
//	width = std::min(aW, bW);

	if (y > 0) {
		aY = y;
		aH -= y;
	}
	else {
		bY = -y;
		bH += y;
	}
	height = min(aH, bH);
//	height = std::min(aH, bH);

	// paw through the rectangle, recording overlaps
	for (int x=0; x<width; x++)
		for (int y=0; y<height; y++)
			at(x+aX, y+aY) = b.at(x+bX, y+bY);
}

void SceneryRect::print()   {
	std::map<ubid, char> printedString;
	printedString[(ubid)  {0,0}] = '.';
	char nextChar = '!';

	for (int y=0; y<_h; y++) {
		for (int x=0; x<_w; x++) {
			ubid ub = at(x,y);
			if (printedString.count(ub) == 0)
				printedString[ub] = nextChar++;
			printf ("%c", printedString[ub]);
		}
		printf ("\n");
	}
}
