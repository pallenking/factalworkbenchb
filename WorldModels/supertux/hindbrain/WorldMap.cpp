// WorldMap.cpp -- keep track of a scenery rectangles in a world map

//#include <SDL/SDL.h>
#include "WorldMap.hpp"
#include "SensoryInfo.h"
#include "SceneryRect.hpp"
#include "Common.h"

WorldMap::WorldMap() {
	int xx=26, yy=19;						// PAK100515
	here_now = new SceneryRect(xx, yy, 0);
	printf("WorldMap.here_now = %lx\n", (unsigned long)here_now);
	known_world = new SceneryRect(130, 50, (char *)"theKnownZone");
	cam = Vector2f(5, 15);
	debug = 10;
	mapViewin = 0;
}

WorldMap::~WorldMap(){
//	delete here_now;
//	delete known_world;
}

void WorldMap::recordMapElement(int type, int buid, int x, int y) {
// 	if (!here_now)
// 		here_now = new SceneryRect(mapViewin->w/BLK_W, mapViewin->h/BLK_W);
// 	if (!known_world)
// 		known_world = new SceneryRect(400, 100);

	if (type == (int)BLOCK_START) {
		printf("here_now(@%lx)->clear()\n", (unsigned long)here_now);
		here_now->clear();
	}
	else
		here_now->at(x, y) = (ubid)  {type, buid};
}

//#import "GeneralViewin.h"
int WorldMap::setViewin(void *w)              {   mapViewin = w;return 0;             }
//int WorldMap::setViewin(GeneralViewin *w)
Vector2f WorldMap::getCamera()				  {	return cam; 						}
char * WorldMap::getZone() 					  {	return known_world->getZoneName();	}

void WorldMap::analyze() {
	int zoom=3, z2=3;//, winW=0, winH=0;
	Vector2f ku(4, 90);//int kstack0=4, kuY0=90;

	if (mapViewin) {				// clear the screen
        panicC("xx");
//		mapViewin->drawBox(0, 0, mapViewin->width()-1, mapViewin->height()-1, 0x000000);
//		SDL_Rect rect =   {0, 0, mapViewin->w, mapViewin->h};
//		SDL_FillRect(mapViewin, &rect, 0x000000);
								// display the "Here and Now" rectangle
		for (int x=-1, y=-1; ubid *u = here_now->nextPoint(x, y); )  {
			int color = 0;
//??			int color = u->type? mapViewin->int2color(u->type): mapViewin->rgb[1];
//??            mapViewin->drawBox(4+z2*x, 4+z2*y, z2, z2, color);
//			SDL_Rect rect1 =   {4+z2*x, 4+z2*y, z2, z2};
//			SDL_FillRect(mapViewin, &rect1, color);
		}						
								// Outline of Known Universe		
// 		drawDottedRect(mapViewin, ku.x, ku.y, z2*known_world->w(), 
// 					   z2*known_world->h(), 0xF0F0F0F0, mapViewin->rgb[2]);
//								// Outline of here_now in Known Universe
// 		drawDottedRect(mapViewin, ku.x+z2*cam.x, ku.y+z2*cam.y, z2*here_now->w(), 
// 					   z2*here_now->h(), 0xEEEEEEEE, rgb[2]);
								// display Known Universe
		for (int x=-1, y=-1; ubid *u = known_world->nextPoint(x, y); )  {
			if (u->type != BLOCK_UNDEF)  {
//??				int color = u->type? mapViewin->int2color(u->type): mapViewin->rgb[1];
//??				mapViewin->drawBox(ku.x+z2*x, ku.y+z2*y, z2, z2, color);
//				SDL_Rect rect1 =   {ku.x+z2*x, ku.y+z2*y, z2, z2};
//				SDL_FillRect(mapViewin, &rect1, color);
			}
		}
	}
//	if (debug > 8)
//		SDL_Flip(mapViewin);

	  // Finde out how to posn the new here_now on the known_world
	 // make increasingly bigger boxes starting with zero length (special case)
	int dd=6;
	float maxOverlap=-1;
	Vector2f best;
	for (int len=0; len<12; len+=2) {
		int x= -len/2, y= -len/2;
		 // trace around a box whose side is length len
		for (int dir=0, dist=1; dist <= 4*len +(len==0); dist++) {
			float overlap = known_world->correlate(*here_now, cam.x+x,cam.y+y);
			if (maxOverlap < overlap) {
				maxOverlap = overlap;
				best = Vector2f(x,y);
			}
			//printf("(%d,%d): %.4f\n", x, y, overlap);
#ifdef xxx
			if (mapViewin) {		// display correlation map
				int color = (int)(overlap * (numRgb-numPaletteV1));
				mapViewin->drawBox(ku.x+z2*x, ku.y+z2*y, z2, z2, color);
//				SDL_Rect rect1 =   {zoom*(x+dd)+120 ,zoom*(y+dd), zoom, zoom};
//				SDL_FillRect(mapViewin, &rect1, int2color(color));
			}
#endif
			x += dir==0? 1: dir==2? -1: 0;
			y += dir==1? 1: dir==3? -1: 0;
			if (len)
				dir += dist%len == 0;
		}
	}

	 // Update camera posn to that with highest correlation
	cam += best;
	known_world->add_elements(*here_now, cam.x, cam.y);
#ifdef xxx
	if (mapViewin) {			// display best correlation point as WHITE
		SDL_Rect rect1 =   {zoom*(best.x+dd)+120, zoom*(best.y+dd),
						  3*zoom/4, 3*zoom/4};
		SDL_FillRect(mapViewin, &rect1, rgb[4]);
								// display the new Here and Now rectangle
		drawDottedRect(mapViewin, ku.x+z2*cam.x, ku.y+z2*cam.y,
					   z2*here_now->w(), z2*here_now->h(), 0x11111111, rgb[4]);
	}
#endif
//	if (debug > 8)
//		SDL_Flip(mapViewin);
//	best.x = best.x;
}

//static int patternPhase = 0;
//void putPoint(SDL_Surface *win, int x_, int y_, int patrn, int colr);
//void putPoint(SDL_Surface *win, int x_, int y_, int patrn, int colr)   {
//	if (patrn & (1 << patternPhase++))   {
//		SDL_Rect rect =   {x_, y_, 1, 1};
//		SDL_FillRect(win, &rect, colr);
//	}
//	patternPhase &= 0x1f;
//}
//
//void WorldMap::drawDottedRect(SDL_Surface *win, int x, int y, 
//							  int w, int h, int patrn, int colr)  {
//	for (int i=0; i<w; i++)
//		putPoint(win, x+i, y, patrn, colr);
//	for (int i=0; i<h; i++)
//		putPoint(win, x+w-1, y+i, patrn, colr);
//	for (int i=0; i<w; i++)
//		putPoint(win, x+w-1-i, y+h-1, patrn, colr);
//	for (int i=0; i<h; i++)
//		putPoint(win, x, y+h-1-i, patrn, colr);
//}
