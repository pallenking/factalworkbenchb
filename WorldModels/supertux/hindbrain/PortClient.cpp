// PortClient.cpp -- connect to a remote computer for a data source
/*
	PROVIDES: a port connection to a remote client for data exchange
	AGNOSTIC TO: the contents of the information exchanged
	MANAGES: server name:port, password, seed, buffer passing
*/
#include "PortClient.hpp"
#include "Common.h"
//#include "NSCommon.h"  // 171022: incompatibale headers

#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <inttypes.h>
#include <unistd.h>
#include <wordexp.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <netdb.h>

#define XK_MISCELLANY			// pull in standard unicode symbols

const int presumedVerMajor = 1;
const int presumedVerMinor = 2;

PortClient :: PortClient () {
	mySocket = -1;
	debug = 0;
}

int PortClient :: contactServer (char* server, unsigned long port, char* passwd, char *seed) {
	assertC(mySocket<0, ("contacting with open socket"));
	mySocket = socket(PF_INET, SOCK_STREAM, IPPROTO_IP);	// Create Socket
	if (mySocket < 0)
		return mySocket;

	struct sockaddr_in sin;			// Setup the socket and IP's
	sin.sin_family = AF_INET;
	if (!server[0])
		sin.sin_addr.s_addr = htonl (INADDR_ANY);
	else {
		if ((sin.sin_addr.s_addr = inet_addr (server)) == INADDR_NONE) {
			struct hostent *hp;
			hp = gethostbyname (server);
			if (!hp) {
				fprintf (stderr, "unknown host: %s\n", server);
				return -1;
			}
			memcpy (&sin.sin_addr.s_addr, hp->h_addr_list[0], hp->h_length);
		}
	}
	sin.sin_port = htons (port);
	
	if (connect (mySocket, (struct sockaddr *) &sin, sizeof (sin))) {
		perror ("  Supertux2 game server");
		close(mySocket);
		mySocket = -1;
		return -1;
	}
	
	// send in password
	portBuf sendBuf, verBuf;
	strncpy(sendBuf.buf, passwd, maxBufLength);
	sendBuffer(sendBuf, 'P');

	// receive version information
	receiveBuffer(verBuf);
	int n = sscanf(verBuf.buf, "portDef Version %d.%d",&verMajor,&verMinor);

	if (verBuf.cmd != 'v' || n != 2) {
		printf("error in version: actually received '%c':'%s'\n", 
			   verBuf.cmd, verBuf.buf);
		return mySocket = -1;
	}  
	printf("Connected to Supertux game server '%s:%d' uses portDef.hpp"
		   "-ver%d.%d\n", server, (int)port, verMajor, verMinor);
	assertC(presumedVerMajor==verMajor && presumedVerMinor==verMinor,
		   ("Supertux simple brain (this program) compiled with  portDef.hpp"
			"-ver%d.%d.\n", presumedVerMajor, presumedVerMinor));
	
	// send in random seed
	if (seed && seed[0]) {
		strcpy(sendBuf.buf, seed);
		sendBuffer(sendBuf, 'R');
		// receive ack
		receiveBuffer(verBuf);		// no error checking for now
	}
	return 0;
}

int PortClient :: sendBuffer (portBuf &buf, char cmd) {
//150408- Xcode 6.3 (6D570) won't compile
///	assertC(this!=0, ("Sending via NULL PortClient object"));
//	if (mySocket<0 && resurect)
//		contactServer();
	if (mySocket<0)
		return -1;
	buf.cmd = cmd;
	buf.len = strlen(buf.buf)+1;

	char *bufP = (char *)(&buf.firstWireVar[0]);
	int len2send = portBufOffsetOf(buf) - portBufOffsetOf(firstWireVar[0])
														  + buf.len;
	for (int wrote;  len2send>0;  bufP +=wrote, len2send -=wrote) {
		wrote = (int)send(mySocket, bufP, len2send, 0);
		assertC(wrote!=0, ("Send to Supertux: wrote 0 bytes"));
		if (wrote < 0) {
			perror("Error writing socket to Supertux");
			exit(0);
		}
	}
	if (debug)
		printf("---- simple brain sent: '%s'\n", portDefsStr(buf));

	return 0;
}

int PortClient :: receiveBuffer (portBuf &buf) {
	int maxSize = sizeof(portBuf) - portBufOffsetOf(firstWireVar);
	int offsetOfBuf = portBufOffsetOf(buf) - portBufOffsetOf(firstWireVar);
	int nRcvd = 0;
	int lenRqd = 0;							// length required (0 if known)

//	int optLen = 1000;
//	char optBuf[1000];
//	if (int rv=getsockopt(mySocket, SOL_SOCKET, "", optBuf, &optLen))
//		printf("rv=%x\n", rv);

	while (lenRqd==0 || nRcvd < lenRqd) {
		int rcvd = (int)recv(mySocket, (void *)&buf, maxSize, 0);
		assertC(rcvd!=0, ("Reading from Supertux: read 0 bytes"));
		if (rcvd < 0) {
			perror("Error reading socket from Supertux");
			exit(0);
		}

		nRcvd += rcvd;
		maxSize -= rcvd;
		if (!lenRqd && nRcvd > offsetOfBuf)		// if buf.len is in
			lenRqd = offsetOfBuf + buf.len;			// compute final length
	}
	if (debug)
		printf("---- simple brain received: '%s'\n", portDefsStr(buf));
	return 1;						// return number of bytes in buf
}
