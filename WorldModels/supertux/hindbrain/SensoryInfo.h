// SensoryInfo.hpp -- receive screen information from supertux, for brains

#ifndef __SensoryInfo_hpp__
#define __SensoryInfo_hpp__

#include "portDefs.hpp"
#include "Datum.hpp"
class WorldMap;

#include <vector>
#include <map>
#include <string>

// ========================= SCENERY BLOCKS ==============================
const static int BLK_W = 16;

inline bool operator<(const ubid&a, const ubid&b) 	  { 
	return a.type< b.type || a.uid< b.uid; 			}
inline bool operator==(const ubid&a, const ubid&b) 	  { 
	return a.type==b.type && a.uid==b.uid;			}

// ============================= CLASS ==================================
class SensoryInfo {
  public:
	SensoryInfo();
    void inzViews(void *typeView_, void *textureView_, void *mapView_);

	~SensoryInfo();
	void reset();
	void registerScreenPiece(float, int type,int x,int y,int h,int w, 
							 char *defStr, int strNum, char *str, int uid);
	SensoryDatum analyze();
	void drawScreen();
    void drawTextureOnView(void *view);
    void    drawTypeOnView(void *view);
    void     drawMapOnView(void *view);
	void foviaDrawSize(int x, int y, int w, int h);
	worldLoc getTuxLoc();
//	SensoryDatum sensoryDatum();
    void printSinfo();
	void printMatchStrings();
	void printCurScreen();
	int debug;


  private:
      // ========== ENCODING MAPS:
	 // map textureFilename <--> INT (used for unknown blocks)
	std::map<std::string, int> blockNum;
	std::vector<std::string> blockStr;

	 // map ubid --> matchstring
	std::map<ubid, std::string> matchStrings;

	 // graphing icons
	std::map<std::string, int>colorOf;
	int colorNdx;
	std::map<std::string, int> rgbOf;

    
     // ========== GETTING SCREEN INFO:
	std::vector<screenPiece> cur_screen;			// RAW INTERNAL DATA
	float time;										// #####
	std::vector<screenPiece> scenery;				// RAW INTERNAL DATA

	 // ========== WORLD MAP
	// BRAIN ASSUMPTION: blocks are BLK_WxBLK_W
	int Xhist[BLK_W], Yhist[BLK_W];					// #####?
	WorldMap *worldMap;

	 // ========== COINS:
	short coinState;
	short coinValue;								// #####

	 // ========== BADGUYS:
	std::vector<aobj> animates;						// ##### tux is item 0!
	 // ========== TUX (self):
	worldLoc tuxLoc;								// #####

	// -------------------- SDL stuff (temporary home
	void *textureView;
	void *typeView;
	void *mapView;
	
	short foviaX, foviaY, foviaW, foviaH;
};

#endif // ___hpp__
