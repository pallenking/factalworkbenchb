// Supertux.hpp -- connect to the Supertux game for a simulated Brain.

#ifndef __Supertux_hpp__
#define __Supertux_hpp__

#include "SensoryInfo.h"
#include "portDefs.hpp"
#include "ProtoBrain.hpp"

class SensoryInfo;
class PortClient;

class Supertux {//:Part:A ctor
  public:
	Supertux();
	~Supertux();
	int inz(int &argc, char *argv[]);
	SensoryDatum takeAction(CommandDatum &cmd);
	void printContents(int indent, char *tag);
    void printSummary(/*int indent, char *tag*/);
    void setFoviaSize (int x, int y, int h, int w);
    float currentGameTime_;
    float currentGameTime();

	FILE *recordFile, *playFile;

	int online;						// connected to game (else OFF)
	int supertux_pid;				// (move to PortClient?)

	PortClient *supertuxPortClient;	// access game via this
    void portClientDebug (int val);
    char portClientDebug_;

	SensoryInfo *sinfo;				// analyzes sensory information
    void sensoryInfoDebug (int val);
    char sensoryInfoDebug_;
    worldLoc getTuxLoc();

	 // "permanent" strings are only sent once, then remembered by both sides
	char *permStrings[/*screenPiece::*/LAST_PERM];  // first 3 entries unused
	unsigned int nextPermIndex;
};

#endif // __Supertux_hpp__
						   
