// PortClient.hpp -- connect to a remote computer for a data source

#ifndef __PortClient_hpp__
#define __PortClient_hpp__

#include "portDefs.hpp" 	// data format sent across socket

typedef unsigned char uchar;

typedef struct   {
	long start; 
	long done;
} bufAsTimerVals;

class PortClient {
  public:
	PortClient ();
	int contactServer(char *server, unsigned long port, char *name,char *seed);
	int sendBuffer(portBuf &buf, char cmd);
	int receiveBuffer(portBuf &buf);
	char *Host;
	int verMajor, verMinor;
	int debug;

  private:
//	char *server;
//	unsigned long port;
//	char *passwd;
	int resurect;
		// 0: death is forever
		// 1: attempt reconnection on next send

	int mySocket;
};

#endif	// __PortClient_hpp__

