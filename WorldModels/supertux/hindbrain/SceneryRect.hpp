// SceneryRect.hpp -- a rectangular pattern of scenery blocks

#ifndef __Scenery_Rect_hpp__
#define __Scenery_Rect_hpp__

#include "SensoryInfo.h"

class SceneryRect {
  public:
	SceneryRect(int w, int h, char *zone=0);
	~SceneryRect();
	void clear();
	char *getZoneName();
	ubid *nextPoint(int &x, int &y);
	ubid &at(int x, int y);
	float correlate(SceneryRect &b, int x, int y);
	void add_elements(SceneryRect &b, int x, int y);
	void print();
	int w()   { return _w; }
	int h()   { return _h; }

  private:
	char *zoneName;
	short _w;	
	short _h;	
	ubid *data;
};

#endif //__Scenery_Rect_hpp__
