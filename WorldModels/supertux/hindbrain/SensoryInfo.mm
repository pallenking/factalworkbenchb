// SensoryInfo.mm -- receive screen information from supertux, for brains

#include "D2View.h"
#include "SensoryInfo.h"
#include "Common.h"
#include "WorldMap.hpp"
#import <Cocoa/Cocoa.h>
#import "AppController.h"

ubid zeroBuid =   {0,0};

void drawTextureOnView_C(void *source, void *onView);
void drawTypeOnView_C(void *source, void *onView);
void drawMapOnView_C(void *source, void *onView);

SensoryInfo::SensoryInfo()
		: cur_screen(400), scenery(400), animates(1) {
    cur_screen.clear();
	time = -1;
    scenery.clear();
    animates.clear();

	coinState = 0;
	coinValue = -1;
	colorNdx = 0;
	animates[0] = (aobj)  {BLOCK_UNDEF,0,0,0};	// [0] = tux, undefined now
    worldMap = 0;

	debug = 0;
//**/		0;		// absolutely quiet
//**/		2;		// one line each pass		    <<== NORMAL2
//**/		3;		// a few lines each pass		<<== NORMAL
//**/		5;		// odities
//**/		6;		// all rectangles received
//**/		7;		// histograms
     
	textureView = typeView = mapView = 0;
//	panicC("Need to fix frame positioning");
    if (0) {
		NSRect frame = NSMakeRect(0,300, 400,300);// = demoAuxFrame(0,300, 400,300);
        NSWindow *textureNsWindow = [[NSWindow alloc] initWithContentRect:frame
            styleMask:NSTitledWindowMask | NSResizableWindowMask |  NSMiniaturizableWindowMask
            backing:NSBackingStoreBuffered defer:NO];
		textureNsWindow.frameOrigin = frame.origin;	// Why is this needed?
        textureView = [[D2View alloc] initWithWindow:textureNsWindow];
        [(D2View *)textureView setDrawingMethod:&drawTextureOnView_C object:this];
    }
    if (1) {
		NSRect frame = NSMakeRect(0,300, 400,300);// = demoAuxFrame(0,0, 400,300);//(0,570, 400,300)
        NSWindow *typeNsWindow = [[NSWindow alloc] initWithContentRect:frame
            styleMask:NSTitledWindowMask | NSResizableWindowMask |  NSMiniaturizableWindowMask
            backing:NSBackingStoreBuffered defer:NO];
		typeNsWindow.frameOrigin = frame.origin;	// Why is this needed?
        typeView = [[D2View alloc] initWithWindow:typeNsWindow];
        [(D2View *)typeView setDrawingMethod:&drawTypeOnView_C object:this];
    }
    if (0) {
		NSRect frame = NSMakeRect(0,300, 400,300);// = demoAuxFrame(0,300, 400,300);
        NSWindow *mapNsWindow = [[NSWindow alloc] initWithContentRect:frame
            styleMask:NSTitledWindowMask | NSResizableWindowMask |  NSMiniaturizableWindowMask
            backing:NSBackingStoreBuffered defer:NO];
		mapNsWindow.frameOrigin = frame.origin;	// Why is this needed?
        mapView = [[D2View alloc] initWithWindow:mapNsWindow];
        [(D2View *)mapView setDrawingMethod:&drawMapOnView_C object:this];
    }
}
SensoryInfo::~SensoryInfo() {
	delete worldMap;
}

void SensoryInfo::reset()   {}

// This data structure is used to convert block names into block UID (buid)s
typedef struct {
	short base;			// the base uid
	char xArg1;			// weight of first %d in match
						// 	 0 implies there is no %d of first match
						// 	-1 implies %d of first match is ignored
	char xArg2;			// weight of second %d in match
	const char *match;	// template for matching	// PAK-MAC
	char type;	
} knownBlock;

knownBlock knownBlocks[] = {
	 // Tux:
	  { 1, 1, 0, "jump-%d.1",				BLOCK_TUX, 		},
	  {10, 1, 0, "walk-%d.1",				BLOCK_TUX, 		},
	  {20, 1, 0, "stand-%d.1",			BLOCK_TUX, 		},
 	  {40, 1, 0, "gameover-%d.1",			BLOCK_TUX,		},
 	  {50, 1, 0, "idle-%d.1",				BLOCK_TUX,		},

	 // Badguys:
 	  {  1, 1, 0, "squished-left.%d",		BLOCK_BADGUY,	},
// 	  { 10, 1, 0, "snowball/left-%d.1",	BLOCK_BADGUY,	},
// 	  { 20, 1, 0, "mr_iceblock/left-%d.1",BLOCK_BADGUY,	},
 	  { 30, 1, 0, "sport-left-%d.1",		BLOCK_BADGUY,	},

	 // Coins:
	  {  1, 1, 0, "coin-%d.1",			BLOCK_COIN, 	},
	  { 10, 1, 0, "coins-%d.1",			BLOCK_COIN, 	},

	 // Hard Scenery:
 	  {  1, 1, 0, "brick%d.1", 			BLOCK_HARD, 	},
 	  {  6, 0, 0, "infoblock.1",			BLOCK_HARD, 	},
 	  { 12, 1, 0, "full-%d.1",			BLOCK_HARD, 	},
 	  { 20, 1, 0, "concave.%d",			BLOCK_HARD, 	},

// 	 // Goes on the Map:
 	  {  1, 1, 0, "cloud.%d",				BLOCK_MAP, 		},
// 	  { 10, 0, 0, "full-2.1",				BLOCK_MAP, 		},
// 	  { 11, 0, 0, "left-2.1",				BLOCK_MAP, 		},
 	  { 30, 1, 0, "background.%d",		BLOCK_MAP,	 	},

	 // Ignore:
 	  {  1,-1, 0, "convex.%d",		BLOCK_IGNORE, 	}, 	// background
 	  {  4, 0, 0, "arctis.1",			BLOCK_IGNORE, 	},
	  {  5, 1, 0, "arctis_top.%d",	BLOCK_IGNORE, 	},
	  {  6, 1, 0, "foo.%d",			BLOCK_IGNORE, 	},	// ???
	  { 10, 1, 0, "empty.%d",			BLOCK_IGNORE, 	},	// ???
	  { 20, 1, 0, "no filename.%d",	BLOCK_IGNORE, 	},	// fileless in supertux
 	  { 30, 0, 0, "grass2.1",			BLOCK_IGNORE, 	},
//	  {0, "50<1>",					BLOCK_IGNORE,	},
//	  {0, "COINS<1>",					BLOCK_IGNORE,	},

	  {  0, 0, 0, 0, 0}		// END OF TABLE
};
// subarea is enclosed in <> e.g. <3> or <%d>


void SensoryInfo::registerScreenPiece(float t, int type, int x, int y,
              int w, int h, char *defStr, int strNum, char *str, int uid) {
// called once for each block on the screen.
//   resets data structures on first (START_SCREEN) block
//   displays raw blocks in textureView window.
//	 detect coinValue (aka viability)
//	 decompresses wire encoding (blockNum)
//	 converts raw blocks to known blocks (using knownBlocks)
//	 cur_screen -- known blocks (INTERNAL)
//	 scenery -- known scenery blocks
//	 makes histogram to find "fractional block" posn

	if (debug >= 6)
		printf("%3d --> %-10s (%4d,%4d)+(%4d,%4d) %s%d:'%s'\n", uid,
			   typePrintString[type], x, y, h, w, defStr, strNum, str);

	 // display information on screen
	if (textureView) {
		if (type == (int)/*screenPiece::*/START_SCREEN)  {
            panicC("xx");
//			textureView->drawBox(0, 0, textureView->width(), textureView->height(), 0x000000);
			colorOf["arctis"] = 0;//black
		}
		else if (type == (int)/*screenPiece::*/SURFACE) {
			  // str is an identifier of the texture, in the form of name:pos.
			 // display box color according to name, and dot color according to pos
			char name[100];
			int pos = 0, colorNum;
			if (sscanf(str, "%s %d", name, &pos) != 2)	// if not in this format
				strncpy(name, str, 100);
			
            panicC("xx");
//			if (colorOf.count(name))
//				colorNum = colorOf[name];
//			else
//				colorNum = colorOf[name] = colorNdx = textureView->int2colorNum(colorNdx++);
//			
//			textureView->drawBox(x, y, w-1, h-1, textureView->rgb[colorNum]);
//			textureView->drawBox(x+2, y+1, 5, 5, textureView->int2color(pos));
		}
	}
	//======================= START of SCREEN ==================
	if (type == (int)/*screenPiece::*/START_SCREEN) {
		cur_screen.clear();
		for (int i=0; i<BLK_W; i++)
			Xhist[i] = Yhist[i] = 0;
		time = t;
		return;
	}

	 // look for two back to back messages: 1) "COINS 1", 2) "<DDD> 1"
	if (type == (int)/*screenPiece::*/TEXT) {
		if (strcmp(str, "COINS 1")==0)
			coinState = 1;
		else if (coinState == 1) {
			int i;
			if (sscanf(str, "%d 1", &i) == 1)
				coinValue = i;
			coinState = 0;
		}
		return;
	}
	else	
		coinState = 0;

	 // nothing interesting in FILLRECT's
	if (type == (int)/*screenPiece::*/FILLRECT) 
		return;	

 	 // remember piece's string as a number
 	std::string s(str);
 	if (blockNum.count(s) == 0) {		// if unknown string
 		int k = (int)blockNum.size()+3;
 		blockNum[s] = k;					// map string->index
 		blockStr.resize(k+1, "");
 		blockStr[k] = s;					// map index->string
 	}
 	int stringNum =	blockNum[s];		// get index (number) of string

	 // see if the block is in knownBlocks, and construct it's buid
	knownBlock *kb = knownBlocks;
	int buid, match = 0, spType;
	for (int i=0; kb->match && !match; i++, kb++) {
		int arg1=0, arg2=0;
		int nArgs = (kb->xArg1!=0) + (kb->xArg2!=0);
		buid = kb->base;
		if (nArgs == 0)
			match = strcmp(str, kb->match) == 0;
		else {
			match = sscanf(str, kb->match, &arg1, &arg2) == nArgs;
			buid += arg1 * (kb->xArg1>0? kb->xArg1: 0);
			buid += arg2 * (kb->xArg2>0? kb->xArg2: 0);
		}
	}
	if (match) {
		kb--;
		spType = kb->type;
		ubid val =   {spType, buid};
		if (matchStrings.count(val) == 0)			// unknown string
			matchStrings[val] = kb->match;				// remember it
		else if (matchStrings[val] !=  kb->match)	// known string, check
			panicC("type %x|%x: already defined as '%s': new='%s'", spType,buid,
				  matchStrings[val].c_str(), kb->match);

		 // Special patch for grass2:
		if (strcmp(kb->match, "grass2 1") == 0 && w==2*BLK_W)
			w = BLK_W;
	}
	else {
		spType = BLOCK_UNKNOWN;
		buid = stringNum;
		if (debug >= 5)
			printf("# %.4f: UNKNOWN %-10s (%4d,%4d)+(%4d,%4d)  '%s'\n", t,
				   typePrintString[type], x, y, h, w, str);
	}

	 // save piece in cur_screen:
	screenPiece sp = {spType, buid, x, y, w, h, uid};
	cur_screen.push_back(sp);

	 // make a histogram, to determine the fraction of a block
	int maxVal = (INT_MAX / BLK_W) * BLK_W;			// want modulus of non-neg
	int x1 = (x + (x<0? maxVal: 0))%BLK_W, y1 = (y + (y<0? maxVal: 0))%BLK_W; 
	assertC(x1>=0 && x1<BLK_W && y1>=0 && y1<BLK_W, ("bad hist index"));
	Xhist[x1]++;
	Yhist[y1]++;
}

aobj tuxAnimates;
bool closestToTux(const aobj &a, const aobj &b);
bool closestToTux(const aobj &a, const aobj &b) {
	return abs(tuxAnimates.x - a.x) + abs(tuxAnimates.y - a.y) <
		   abs(tuxAnimates.x - b.x) + abs(tuxAnimates.y - b.y);
}

// analyze cur_screen 
//	 determines fractional posn (origin % 16)
//	 create vector of animate objects, sorted by distance from tux
//	 record aligned blocks in worldMap
//	 displays known block in textureView window
//	 returns SensoryDatum =   {time, cur_screen, animates, tuxLoc, coinValue}
	
SensoryDatum SensoryInfo::analyze() {
	int Xmax=0, Ymax=0;
	unsigned int Xi=BLK_W,  Yi=BLK_W;
	if (debug >= 7) for (int i=0; i<8; i++)
		printf("%9d%5d%9d%5d%9d%5d%9d%5d\n", Xhist[i],Yhist[i], Xhist[i+8],
			   Yhist[i+8], Xhist[i+16],Yhist[i+16], Xhist[i+24],Yhist[i+24]);
	for (int i=0; i<BLK_W; i++) {              // Find most popular X and Y
		if (Xmax < Xhist[i]) {
			Xmax = Xhist[i];
			Xi = i;
		}
		if (Ymax < Yhist[i]) {
			Ymax = Yhist[i];
			Yi = i;
		}
	}
	if (debug >= 3)
		printf("Fractional posn: (%d,%d)\n", Xi, Yi);

	if (worldMap)
        worldMap->recordMapElement(BLOCK_START, 0,0,0);

	 // process screen pieces, sort them by their types
	int blocks=0, unaligned=0;
	unsigned int Xip1=((Xi+1)%BLK_W), Xim1=((Xi+BLK_W-1)%BLK_W);
	unsigned int Yip1=((Yi+1)%BLK_W), Yim1=((Yi+BLK_W-1)%BLK_W);
	int mwX = 400, mwY=300;

    scenery.clear();
    animates.clear();
	animates.push_back((aobj)  {BLOCK_UNDEF,0,0,0});// [0] = tux, undefined now

	std::vector<screenPiece>::const_iterator sp;
    for(sp=cur_screen.begin(); sp!=cur_screen.end(); ++sp) {
		screenPiece &s = (screenPiece &)*sp;	// gdb doesn't like C++ vector
// 		if (debug >= 6)
// 			printf("%3d --> %-10s (%4d,%4d)+(%4d,%4d) %d:'%s'\n", s.uid,
// 				   typePrintString[s.type], s.x, s.y, s.h, s.w, s.stringNum,
// 				   matchStrings[(ubid)  {s.type, s.stringNum}].c_str());

		char *printString = 0, ok=1;
		int x = SpSignExtendP(s.x);
        int y = SpSignExtendP(s.y);

		switch ((int)s.type) {
			break; case BLOCK_UNDEF:
				printString = (char *)"UNDEFINED";
			break; case BLOCK_UNKNOWN:
				printString = (char *)"EXCEPTION";
			break; case BLOCK_START:
				printString = (char *)"START";
			break; case BLOCK_IGNORE:

			break; case BLOCK_HARD:
				   case BLOCK_MAP:
				 // x and y match (+-1 seems to be rounding issues)
				ok &= s.x%BLK_W == Xi || s.x%BLK_W == Xip1 || s.x%BLK_W == Xim1;
				ok &= s.y%BLK_W == Yi || s.y%BLK_W == Yip1 || s.y%BLK_W == Yim1;

				ok &= (int)s.h == BLK_W || s.h == BLK_W+1 || s.h == BLK_W-1;
				ok &= (int)s.w == BLK_W || s.w == BLK_W+1 || s.w == BLK_W-1;
	//			ok &= s.type != 4 || s.stringNum != 20;	// force orphanage

				if (ok) {
					if (x>=0 && x<mwX && y>=0 && y<mwY) {
						blocks++;
						screenPiece sNew = s;
						sNew.x = x/BLK_W;
						sNew.y = y/BLK_W;
						if (worldMap)
							worldMap->recordMapElement(sNew.type, sNew.stringNum, sNew.x, sNew.y);
						scenery.push_back(sNew);
					}
					//else
					//	printString = "OutOfBounds";
				}
				else {
					printString = (char *)"UNALIGNED";
					unaligned++;
				}

			break; case BLOCK_COIN:
			break; case BLOCK_BADGUY:
				animates.push_back((aobj) {s.type, s.stringNum, x, y});
			break; case BLOCK_TUX:
				if (animates[0].type==BLOCK_UNDEF) ;		// first tux
				else
					assertC(animates[0].x==x &&animates[0].y==y,("Bad 2'nd tux"));
				animates[0] = (aobj)  {s.type, s.stringNum, x, y};
		}
		
		if (printString) { 							// bad blocks
			s.type = BLOCK_IGNORE;
			if (debug >= 5) {							// print
				printf("# %.4f %s (%4d,%4d)+(%4d,%4d) %2d:%3d='%s'", time,
					   printString, SpSignExtendP(s.x), SpSignExtendP(s.y), 
					   s.h, s.w, s.type, s.stringNum, 
					   matchStrings[(ubid)  {s.type, s.stringNum}].c_str());
				if (strcmp(printString, "UNALIGNED") == 0)
					printf(" %d/%d %d.%d.%d.%d", s.x%BLK_W, s.y%BLK_W, 
						   Xim1,Xi,Xip1, Yi);
				printf("\n");
			}
		}
	}
	// sort the animates so closest to tux is least index.
	tuxAnimates = animates[0];
	std::sort(animates.begin(), animates.end(), &closestToTux);

	if (debug >= 3)
		printf("blocks=%d, unaligned=%d\n", blocks, unaligned);
	Vector cam(0,0);                            // presume 0,0 if no worldMap
    tuxLoc.zone = (char *)"";
    if (worldMap) {
        worldMap->analyze();
        cam = worldMap->getCamera();
        tuxLoc.zone = worldMap->getZone();
    }
	tuxLoc.x = cam.x*BLK_W + animates[0].x;
	tuxLoc.y = cam.y*BLK_W + animates[0].y;

	SensoryDatum rv;
	rv.time = time;
	rv.scenery = &scenery;
//	rv.cur_screen = &cur_screen;
	rv.animates = &animates;
	rv.tuxLoc = tuxLoc;
	rv.coinValue = coinValue;
	return rv;
}

void SensoryInfo::foviaDrawSize(int x, int y, int w, int h) {
	foviaX = x;
	foviaY = y;
	foviaW = w;
	foviaH = h;
}
worldLoc SensoryInfo::getTuxLoc()   {  return tuxLoc;      }

void SensoryInfo::drawScreen() {	// Draw on screen
//void SensoryInfo::display()   {
    [(D2View *)textureView display];
    [(D2View *)typeView display];
    [(D2View *)mapView display];
}

void drawTypeOnView_C(void *source, void *onView) {
    ((SensoryInfo *)source)->drawTypeOnView(onView);
}
void SensoryInfo::drawTypeOnView(void *view_) {
    D2View *view = (D2View *)view_;
    [view drawBox:[view frame] color:0];      // clear screen:

    // draw screen pieces
    std::vector<screenPiece>::const_iterator sp;
    for(sp=cur_screen.begin(); sp!=cur_screen.end(); ++sp) {
        screenPiece &s = (screenPiece &)*sp;	// gdb doesn't like C++ vector
        int x = SpSignExtendP(s.x), y = SpSignExtendP(s.y);
        const int d = 0;
        if (debug >= 5)							// print
            printf("%3d --> %-10s (%4d,%4d)+(%4d,%4d)\n", s.uid,
                   typePrintString[s.type], s.x, s.y, s.h, s.w);
        
        if (s.type == BLOCK_IGNORE) {
            [view drawBox:NSMakeRect(x+d  ,y+d  , s.w-1,s.h-1) color:s.type];
            [view drawBox:NSMakeRect(x+d+0,y+d+0, s.w-2,s.h-2) color:s.type];
            [view drawBox:NSMakeRect(x+d+2,y+d+0, 2,    2    ) color:s.stringNum];
        }
        else if (s.type == BLOCK_UNKNOWN)
            [view drawBox:NSMakeRect(x+d+0,y+d+0, s.w-1,s.h-1) color:0];
        else {
            [view drawBox:NSMakeRect(x+d+0,y+d+0, s.w-1,s.h-1) color:s.type];
            [view drawBox:NSMakeRect(x+d+2,y+d+1, 5,    5    ) color:s.stringNum];
		}
	}
    // draw output rectangle
	int xf = foviaX<0? tuxLoc.x/16*16+foviaX: foviaX;
	int yf = foviaY<0? tuxLoc.y/16*16+foviaY: foviaY;
    [view drawDottedRect:NSMakeRect(xf, yf, foviaW, foviaH) color:0];
}

void drawTextureOnView_C(void *source, void *view) {
    ((SensoryInfo *)source)->drawTextureOnView(view);
} // no texture currently:
void SensoryInfo::drawTextureOnView(void *vieww)     { panicC("unipmlemented"); }


void drawMapOnView_C(void *source, void *view) {
    ((SensoryInfo *)source)->drawMapOnView(view);
}// no maps currently
void SensoryInfo::drawMapOnView(void *vieww)         { panicC("unipmlemented"); }


////////
std::map<std::string, int>colorOf;
int colorNdx;
std::map<std::string, int> rgbOf;

void SensoryInfo::printSinfo() {
    printf("time:%.3f cur_screen:%ld, scenery:%ld, coin:%d:%d tux(%d,%d)\n",
           time, cur_screen.size(), scenery.size(),
           coinState, coinValue, tuxLoc.x, tuxLoc.y);
}

 // ============================== GDB
// in GDB: p sinfo.printMatchStrings()
void SensoryInfo::printMatchStrings() {
	std::map<ubid, std::string>::const_iterator t1;
	for (t1=matchStrings.begin(); t1 != matchStrings.end(); t1++)
		printf("matchStrings[%x:%x] == '%30s'\n", t1->first.type,t1->first.uid,
			   t1->second.c_str());
}

 // in GDB: p sinfo.printCurScreen()
void SensoryInfo::printCurScreen() {
	std::vector<screenPiece>::const_iterator sp;
	for (sp=cur_screen.begin(); sp != cur_screen.end(); sp++)   {
		const char *str = sp->type != BLOCK_UNKNOWN?
			matchStrings[(ubid)  {sp->type, sp->stringNum}].c_str():// known
			blockStr[sp->stringNum].c_str();					  // unknown

		printf("# (%4d,%4d)+(%4d,%4d) %2d:%3d='%s'\n", 
			   SpSignExtendP(sp->x), SpSignExtendP(sp->y), sp->h, sp->w,
			   sp->type, sp->stringNum, str);
	}
}
