// MoveOver.hpp -- jump over badguy

#ifndef __MoveOver_hpp__
#define __MoveOver_hpp__
#include "MoveTux.hpp"

class MoveOver: public MoveTux {
  public:
    float umph;
	MoveOver(Pool *parent_=0);
	CommandDatum nextCmd(SensoryDatum senses);
	float vehemence(SensoryDatum senses);
};

#endif // __MoveOver_hpp__


