// MoveClimb.cpp -- simple strategies for moving tux

#include "MoveClimb.hpp"
#include "SensoryInfo.h"

#include <vector>

float MoveClimb::vehemence(SensoryDatum senses) {
//150408- Xcode 6.3 (6D570) won't compile
//	if (&senses == 0 || senses.animates == 0)
		return 0;
	std::vector<aobj> &an = senses.animates[0];
	
	umph = -10;
	if (an.size() >= 2 and an[0].type == BLOCK_TUX and an[1].type != BLOCK_BADGUY) {
		int distx = an[0].x - an[1].x;
		if (distx < 0)
			distx = -distx;
//		int distx = abs(an[0].x - an[1].x);
		if (distx < 80)
			umph = 5;
	}
	return umph;
}

CommandDatum MoveClimb::nextCmd(SensoryDatum senses) {
	return (CommandDatum)  {umph, bb(RIGHT)|bb(JUMP), 0.1, 0, (char *)"MoveClimb.cpp"};
}
