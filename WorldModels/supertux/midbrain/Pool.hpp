// Pool.hpp -- a group of competing reactors

#ifndef __Pool_hpp__
#define __Pool_hpp__

#include "Datum.hpp"
#include <vector>
#include "Reactor.hpp"
class MoveTux;

class Pool: public Reactor {
  public:
	Pool();
	~Pool();
	void reset();
	void addMember(MoveTux *m);
	MoveTux *winner(SensoryDatum senses);

  private:
	std::vector<MoveTux *> members;
};

#endif // __Pool_hpp__


