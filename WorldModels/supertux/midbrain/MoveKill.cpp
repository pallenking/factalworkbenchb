// MoveKill.cpp -- simple strategies for moving tux

#include "MoveKill.hpp"
#include "SensoryInfo.h"

#include <vector>

MoveKill::MoveKill(Pool *pool_) 
		: MoveTux(pool_)	
  {}

float MoveKill::vehemence(SensoryDatum senses) {
	umph = -10;
//150408- Xcode 6.3 (6D570) won't compile
//	if (&senses and senses.animates)
	if (senses.animates)
	  {
		std::vector<aobj> &an = senses.animates[0];
		if (an.size()>=2 and an[0].type==BLOCK_TUX and an[1].type==BLOCK_BADGUY)   {
			int distx = an[0].x - an[1].x;
			if (distx < 0)
				distx = -distx;
//			int distx = abs(an[0].x - an[1].x);
			if (distx < 80)
				umph = 5;
		}
	}
	return umph;
}

CommandDatum MoveKill::nextCmd(SensoryDatum senses) {
	return (CommandDatum)  {umph, bb(RIGHT)|bb(JUMP), 0.1, 0, (char *)"MoveKill.cpp"};
}
