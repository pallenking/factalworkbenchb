// MoveTux.cpp -- simple strategies for moving tux

//#include "../../src/portDefs.hpp"
#include <string>

#include "MoveTux.hpp"
#include "Pool.hpp"
#include "SensoryInfo.h"
#include "Common.h"
extern /*Main.cpp*/SensoryInfo *sinfo;

MoveTux::MoveTux(Pool *pool_) {
	parent = pool_;
	parent->addMember(this);
}

MoveTux::~MoveTux()   {}

void MoveTux::reset()   {}

float MoveTux::vehemence(SensoryDatum senses) {
	return 0;
}

CommandDatum MoveTux::nextCmd(SensoryDatum senses) {
	return (CommandDatum)  {0,0,0,0};
}
