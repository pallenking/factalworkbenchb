// MoveCommands.cpp -- supply commands from a list

#include <string>

#include "MoveCommands.hpp"
#include "Pool.hpp"
#include "SensoryInfo.h"
#include "Common.h"

MoveCommands::MoveCommands(Pool *pool_) 
		: MoveTux(pool_)   {}
MoveCommands::~MoveCommands()   {}

CommandDatum commands[] = {
	// buttons			//Tcmd  //Tchunk // comment/name
	  {5,	0,					0,		0.1,	(char *)"#right sequence ... "},
//	  {bb(RIGHT),				2.2031,	0.1,	(char *)"right for %.2f"},
	  {5,	bb(RIGHT),			3.0,	0.1,	(char *)"right for %.2f"},
	  {5,	bb(RIGHT)|bb(JUMP),	0.5,	0.1,	(char *)"jump for %.2f"},
	  {5,	bb(RIGHT),			2.3,	0.1,	(char *)"right for %.2f"},
	  {5,	bb(RIGHT)|bb(JUMP),	0.1,	0.1,	(char *)"jump for %.2f"},
	  {5,	bb(RIGHT),			1.0,	0.1,	(char *)"right for %.2f"},
	  {5,	bb(RIGHT)|bb(JUMP),	2.0,	0.1,	(char *)"jump for %.2f"},
	  {5,	bb(RIGHT),			1.0,	0.1,	(char *)"right for %.2f"},
	  {5,	bb(RIGHT)|bb(JUMP),	2.0,	0.1,	(char *)"jump for %.2f"},
	  {5,	bb(RIGHT),			9.8,	0.1,	(char *)"right for %.2f"},
	  {5,	bb(BREAKPOINT),		1.,		1.0,	(char *)" exit game"},
	  {5,	bb(EXIT),			1.,		1.0,	(char *)"exit game"},

	  {5,	0,					1.0,	0.1,	(char *)"#drop-jump sequence"},
	  {5,	bb(JUMP)|bb(LEFT),	0.8,	0.1,	(char *)"jump for %.2f"},
//	  {bb(JUMP),				0.1,	0.1,	(char *)"jump for %.2f"},
	  {5,	bbSTOP_SEQ,			1e9,	0.1,	(char *)"give control to the game for %.2f"},
	
	  {5,	0,					1.0,	0.1,	(char *)"#drop-loop sequence"},
	  {5,	bb(JUMP)|bb(LEFT),	0.8,	0.1,	(char *)"jump for %.2f"},
	//	  {bb(JUMP),				0.1,	0.1,	(char *)"jump for %.2f"},
	  {5,	bbSTOP_SEQ,			1e9,	0.1,	(char *)"give control to the game for %.2f"},
	
	  {5,	0,					0.1,	0.1,	(char *)"#walk-around: no keys for %.2f"},
	  {5,	0,					1.,		0.1,	(char *)"no keys to start for %.2f"},
	  {5,	bb(RIGHT),			1.,		0.1,	(char *)"right for %.2f"},
	  {5,	0,					1.,		0.1,	(char *)"no keys to start for %.2f"},
	  {5,	bb(LEFT),			1.,		0.1,	(char *)"left for %.2f"},
	  {5,	bb(EXIT),			1.,		0.1,	(char *)" exit game"},

	  {5,	0,					0,		0.1,	(char *)"#test sequence ... "},
	  {5,	bb(RIGHT),			1,		0.1,	(char *)"right for %.2f"},
	  {5,	bbRECORD,			0,		0,		(char *)"foobarron"},
	  {5,	bbJOYSTICK,			0,		0,		(char *)""},
	  {5,	bbPLAY,				0,		0,		(char *)"foobarron"},
	  {5,	bb(RIGHT),			10,		0.1,	(char *)"right for %.2f"},
	  {5,	bb(EXIT),			1.,		1.0,	(char *)"exit game"},

	  {5,	0,					0,		0.1,	(char *)"#joystick test sequence ... "},
	  {5,	bbJOYSTICK,			0,		0,		(char *)""},
	  {5,	bb(EXIT),			1.,		1.0,	(char *)"exit game"},

	  {5,	-2,					0,		0.1,	(char *)""}
};

void MoveCommands::reset(char *section) {
	MoveTux::reset();
	commandNo = -1;
	timeLeft = 0;				// initially nothing to do

	if (section) {
		while (commands[++commandNo].buttons != -2)
			if (commands[commandNo].comment[0] == '#' &&
					strncmp(section, commands[commandNo].comment+1,
						strlen(section)) ==0)  {
				timeLeft += commands[commandNo].duration;
				break;
			}
	}
	else
		commandNo = 0;
	printf("MoveCommands::reset(%s)\n", section);
	assertC(commands[commandNo].buttons!=-2,
			   ("Failed to find section '%s'", section));
}

float MoveCommands::vehemence(SensoryDatum senses) {
	while (timeLeft<=0 && commands[commandNo].buttons != bbSTOP_SEQ) {
		// read button commands from list:
		commandNo++;
		printf("=== %d: buttons 0x%03x for %.4f. %s\n", commandNo,
			   commands[commandNo].buttons,
			   commands[commandNo].duration, commands[commandNo].comment);
		timeLeft += commands[commandNo].duration;	// add in new command duration
	}
	return (commands[commandNo].buttons!=bbSTOP_SEQ)? 10: // try to win bid
													 -10; // no more commands -- don't bid
}

CommandDatum MoveCommands::nextCmd(SensoryDatum senses) {
	CommandDatum rv = commands[commandNo];
	rv.duration = min(timeLeft, rv.maxChunkTime);	// truncate if it runs over
//	rv.duration = std::min(timeLeft, rv.maxChunkTime);	// truncate if it runs over
	timeLeft -= rv.duration;
//	rv.maxChunkTime = timeLeft -= rv.duration;
	return rv;
}
