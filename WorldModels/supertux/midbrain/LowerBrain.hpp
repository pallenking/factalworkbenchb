// LowerBrain.hpp -- A simple brain built of C++ actors

#ifndef __ActorBrain_hpp__
#define __ActorBrain_hpp__

#include "ProtoBrain.hpp"

#include "MoveTux.hpp"
#include "MoveCommands.hpp"
#include "Pool.hpp"

class LowerBrain: public ProtoBrain {
  public:
	LowerBrain();
	~LowerBrain();
	CommandDatum chooseAction(SensoryDatum senses);
	void inz(int &argc, char *argv[]);

	Pool *motorPool;
	MoveCommands *motorCmd;
	MoveTux *motorRight;
	MoveTux *motorOver;
	MoveTux *motorKill;
};

#endif // __ActorBrain_hpp__
