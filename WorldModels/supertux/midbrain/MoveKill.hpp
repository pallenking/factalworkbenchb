// MoveKill.hpp -- simple strategies for moving tux

#ifndef __MoveKill_hpp__
#define __MoveKill_hpp__
#include "MoveTux.hpp"

class MoveKill: public MoveTux {
  public:
    float umph;
	MoveKill(Pool *parent_=0);
	CommandDatum nextCmd(SensoryDatum senses);
	float vehemence(SensoryDatum senses);

};

#endif // __MoveKill_hpp__


