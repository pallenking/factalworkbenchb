// MoveCommands.hpp -- supply commands from a list

#ifndef __MoveCommands_hpp__
#define __MoveCommands_hpp__
#include "MoveTux.hpp"

class MoveCommands: public MoveTux {
  public:
	MoveCommands(Pool *parent_=0);
	~MoveCommands();
	void reset(char *selection=0);
	float vehemence(SensoryDatum senses);
	CommandDatum nextCmd(SensoryDatum senses);

  private:
	int commandNo;
	float timeLeft;
};

#endif // __MoveCommands_hpp__


