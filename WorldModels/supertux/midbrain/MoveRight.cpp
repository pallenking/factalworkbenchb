// MoveRight.cpp -- move tux right, with small vehemence

#include "MoveRight.hpp"

MoveRight::MoveRight(Pool *pool_) 
		: MoveTux(pool_) {
}

float MoveRight::vehemence(SensoryDatum senses) {
	return 1;
}

CommandDatum MoveRight::nextCmd(SensoryDatum senses) {
	printf("-------------- time %f --------------------\n", senses.time);
	
	float t = senses.time - 10.5;	// 10.5 HACK!!!
	int i = t/5.0;
	int button = (t<0.0 or (i&1))?bb(RIGHT):bb(LEFT);
	return (CommandDatum)  {1, button, 0.1, 0, (char *)"MoveRight.cpp"};
}

