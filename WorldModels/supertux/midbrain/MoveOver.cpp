// MoveOver.cpp -- jump over badguy

#include "MoveOver.hpp"
#include "SensoryInfo.h"

#include <vector>

MoveOver::MoveOver(Pool *pool_) 
		: MoveTux(pool_)	
  {}


float MoveOver::vehemence(SensoryDatum senses) {
	umph = 0;
//150408- Xcode 6.3 (6D570) won't compile
//	if (&senses and senses.animates)
	if (senses.animates)
	  {
		umph = -10;
		std::vector<aobj> &an = senses.animates[0];
		
//		static FILE *debugFooFile=0;
//		if (!debugFooFile) ///Users/allen/Library/Developer/Xcode/DerivedData/sitree3-crlhqktwailkqudbiwxlyqbdugjm/Build/Products/Debug/debugFooFile
//			debugFooFile = fopen("debugFooFile", "w");
//		fprintf(debugFooFile,"------------ %f -- Tux: %d.%d(%x,%x),\t\tSpikey: %d.%d(%x,%x)\n",
//				senses.time, an[0].type, an[0].uid, an[0].x, an[0].y,
//				an[1].type, an[1].uid, an[1].x, an[1].y);
//		fflush(debugFooFile);

		if (an.size()>=2 and an[0].type==BLOCK_TUX and an[1].type==BLOCK_BADGUY) {
			int distx = an[0].x - an[1].x;
			if (distx < 0)
				distx = -distx;
//			int distx = abs(an[0].x - an[1].x);
			if (distx < 80)
				umph =  5;
		}
	}
	return umph;
}

CommandDatum MoveOver::nextCmd(SensoryDatum senses) {
	return (CommandDatum)  {umph, bb(JUMP), 0.05, 0, (char *)"MoveOver.cpp"};
//	return (CommandDatum)  {bb(RIGHT)|bb(JUMP), 0.1, 0, (char *)"MoveOver.cpp"};
}
