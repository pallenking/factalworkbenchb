// MoveTux.hpp -- simple strategies for moving tux

#ifndef __MoveTux_hpp__
#define __MoveTux_hpp__

#include "Supertux.h"
#include "Reactor.hpp"
class Pool;

class MoveTux: public Reactor {
  public:
	MoveTux(Pool *parent_=0);
	virtual ~MoveTux();
	virtual void reset();
	virtual float vehemence(SensoryDatum senses);
	virtual CommandDatum nextCmd(SensoryDatum senses);

  private:
	Pool *parent;	
};

#endif // __MoveTux_hpp__


