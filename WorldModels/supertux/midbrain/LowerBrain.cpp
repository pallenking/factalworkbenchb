// LowerBrain.cpp -- A simple brain built of C++ actors

/* 130519 Snapshot of operation:
	MoveOver:		5 says jump right if dist(tux, spikey)<80?
		else	  -10
	MoveRight:		1 says right or left
	MoveCommands:	
 */

#include "LowerBrain.hpp"

#include "MoveRight.hpp"
#include "MoveOver.hpp"
#include "MoveKill.hpp"

#include <stdio.h>
#include <string>
#include <map>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include "Common.h"		// THIS MUST BE LAST, TO AVOID CONFLICTS WITH MIN/MAX

LowerBrain::LowerBrain () {
	motorPool = 0;
	motorCmd = 0;
	motorRight = 0;
	motorOver = 0;
	motorKill = 0;
}

LowerBrain::~LowerBrain ()    {}

void LowerBrain::inz(int &argc, char *argv[]) {
	char *section = 0;

	for (int i=1; i<argc; i++) {	// PARSE COMMAND LINE ARGS
		if (argv[i] == 0)
			continue;						// ignore already digested args
		std::string arg(argv[i]);
		bool understood = true;

		if (arg == "--section")
		  {
			argv[i] = 0;
			section = argv[++i];
		}
		else
			understood = false;

		if (understood) 
			argv[i] = 0;			// mark as processed

	}
//	if (/* DISABLES CODE */ (0)/*helpMode*/) {			// DISPLAY HELP MESSAGE
//		printf(
//			"brain_v1 [options]\n"
//			"	--section STRING	comment for first command to be sent\n"
//			);
//		exit(1);
//	}
	removeNullArgs(argc, argv);
	
	 // build up motor system comprising brains:
	motorPool = new Pool();
	motorCmd = new MoveCommands(motorPool);
	motorRight = new MoveRight(motorPool);
	motorOver = new MoveOver(motorPool);
//	motorKill = new MoveKill(motorPool);
	motorCmd->reset(section);
	motorRight->reset();
	motorOver->reset();
//	motorKill->reset();
}

CommandDatum LowerBrain::chooseAction(SensoryDatum senses) {
	// === get a motor command
	CommandDatum cmd = ((MoveTux *)(motorPool->winner(senses)))->nextCmd(senses);
	return cmd;
}
