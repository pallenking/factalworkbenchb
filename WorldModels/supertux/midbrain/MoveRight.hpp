// MoveRight.hpp -- move tux right, with small vehemence

#ifndef __MoveRight_hpp__
#define __MoveRight_hpp__
#include "MoveTux.hpp"

class MoveRight: public MoveTux {
  public:
	MoveRight(Pool *parent_=0);
	CommandDatum nextCmd(SensoryDatum senses);
	float vehemence(SensoryDatum senses);

  private:
};

#endif // __MoveRight_hpp__


