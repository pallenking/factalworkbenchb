// MoveClimb.hpp -- simple strategies for moving tux

#ifndef __MoveClimb_hpp__
#define __MoveClimb_hpp__
#include "MoveTux.hpp"

class MoveClimb: public MoveTux {
  public:
    float umph;
	float vehemence(SensoryDatum senses);
	CommandDatum nextCmd(SensoryDatum senses);
};

#endif // __MoveClimb_hpp__


