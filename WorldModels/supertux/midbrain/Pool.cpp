// Pool.cpp -- a group of competing reactors

#include "Pool.hpp"
#include "MoveTux.hpp"

Pool::Pool() 
		: members(0)  {
}

Pool::~Pool()   {}

void Pool::reset()   {}

void Pool::addMember(MoveTux *m) {
	members.push_back(m);
}

MoveTux *Pool::winner(SensoryDatum senses) {
	float maxVe = -1;
	MoveTux *r, *maxRe=0;
	std::vector<MoveTux *>::const_iterator iter;
	for (iter=members.begin(); iter!=members.end(); iter++)   {
		float ve = (*iter)->vehemence(senses);
		if (maxVe <= ve)   {
			maxVe = ve;
			maxRe = *iter;
       }
	}
	return maxRe;
}
