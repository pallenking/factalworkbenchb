// portDefs.hpp -- defines structures past to and from supertux
// used by Supertux (.../supertux/src/control/remotecontrol.cpp) game server
// used by Tux's Brain (.../tux_brain/hindbrain/*)

#ifndef __portDefs_hpp__
#define __portDefs_hpp__

#define portDefVerMajor 1
#define portDefVerMinor 2

/* =========================
Terminology: Wire = that part of structure which goes out over the "wire"

The buffer structure has 3 levels of information:
	A) local		(0..&firstWireVar)			lives only on local computer.
												Not put out over the "wire" 
	B) low-level	(&firstWireVar..&buf[0])	low-level port protocol header
	C) high-level	(&buf[0] .. end)			high-level user data
*/

typedef struct   {			// ========== beginning of local area
//	long timestamp;
	long firstWireVar[0];	// ========== beginning of low-level "wire" area:
	char cmd;
//----------------------------------- Client->Server:
//		'P'		password/seed					(wants version response)
//		'R'		set random seed					(wants OK response)
//		'D'		record demo, buf=filename		(wants ack/nack)
//		'J'		input from Joystick				(wants ack/nack)
//		'E'		playback demo, buf=filename		(wants ack/nack)
//		'B'		button info 					(wants screen response)
//		'N'		noop command (does nothing)		(wants ack/nack)
//		'S'		set parameters					(NOT IMPLEMENTED YET)
//----------------------------------- Server->Client:
//		'v'			server version info
//		's','l'		screen info, ('l' on last packet)
//		'e'			error
//		'n'		negative acknowlege
//		'e'		error

//	char seqNo;
							// ========== beginning of low-level "wire" area:
	int buttons;				// control buttons in effect
        // SEE button<*> BELOW
	float time;					// CS: game time duration for these buttons
								// SC: total elapsed game time
	unsigned short len;			// in bytes of buf
#	define maxBufLength 10000
	char buf[maxBufLength];
							// ========== end of buffer
/* 060415
 *	When maxBufLength was 255, the receiver would only get 2 portBufs.  Surfing
 *		http://www.psc.edu/networking/projects/tcptune/
 *		http://libassa.sourceforge.net/libassa-manual/C/x1586.html
 *			/Every TCP socket has a pair/
 *	  It seems that linux has only two kernel socket buffers, and the server
 *		can overflow that limit in the client.
 *	  Using large buffers seems to get around that problem
 */

} portBuf;

static const int buttonLEFT 		= 0x00000001;				
static const int buttonRIGHT		= 0x00000002;
static const int buttonUP 			= 0x00000004;
static const int buttonDOWN			= 0x00000008;
static const int buttonJUMP			= 0x00000010;
static const int buttonACTION		= 0x00000020;
static const int buttonPAUSE_MENU	= 0x00000040;
static const int buttonMENU_SELECT	= 0x00000080;
// game control
// RESERVED (see Main.cpp)			  0x2-------;
static const int buttonBREAKPOINT	= 0x40000000;
static const int buttonEXIT			= 0x80000000;


//#define portBufOffsetOf(x) ((int)     (&((portBuf *)0)->x))  //in going to Mac/64
#define   portBufOffsetOf(x) ((long int)(&((portBuf *)0)->x))

/* (portBuf).buf has the following format: [screenPiece | string+]*
   screenPiece is struct defined below, describing numeric properties of screen
   string is null terminated character string
   (portBuf).len is length of buf, in characters
 */


//==================================
extern const char *typePrintString[];
#define inz_typePrintStrings const char *typePrintString[] = \
	  {"UNDEF", "FIRST", "SURFACE", "SURFACE_PART", 			 \
	 "GRADIENT", "TEXT", "FILLRECT", "TEXT"}	// KEEP IN SYNC WITH 'type'

#define bigStruct
#ifdef bigStruct //-------------------------------------------------------------

typedef struct   {
	unsigned type		:4;
	unsigned stringNum	:12;
	unsigned x			:16;
	unsigned y			:16;
	unsigned w			:16;
	unsigned h			:16;
	unsigned uid		:16;
} screenPiece;
        // values for type:
static const unsigned UNDEF =	 		0;	// should never occur
static const unsigned START_SCREEN =	1;	// first screen piece
static const unsigned SURFACE =			2;	// defined by supertux...
static const unsigned SURFACE_PART =	3;
static const unsigned GRADIENT =		4;
static const unsigned TEXT =			5;
static const unsigned FILLRECT =		6;
        // values for stringNum
static const unsigned NO_STRING =		0;
static const unsigned STRING =			1;
static const unsigned PERM_STRING =		2;
static const unsigned FIRST_PERM =		3;	//..n	previously sent str
static const unsigned LAST_PERM =		4095;


#define SPbitsXY 16

#else    // !bigStruct -------------------------------------------------------------

typedef struct   {
	unsigned type		:3;
		static const unsigned UNDEF =	 		0;	// should never occur
		static const unsigned START_SCREEN =	1;	// first screenePiece
		static const unsigned SURFACE =			2;	// defined by supertux...
		static const unsigned SURFACE_PART =	3;
		static const unsigned GRADIENT =		4;
		static const unsigned TEXT =			5;
		static const unsigned FILLRECT =		6;
	unsigned stringNum	:7;
		static const unsigned NO_STRING =		0;
		static const unsigned STRING =			1;
		static const unsigned PERM_STRING =		2;
		static const unsigned FIRST_PERM =		3;	//..n	previously sent str
		static const unsigned LAST_PERM =		127;
//#		define type_
	unsigned x			:10;
	unsigned y			:10;
	unsigned w			:7;				// width/16
	unsigned h			:7;				// height/16
} screenPiece;
#define SPbitsXY 10
#endif

inline int SpSignExtendP(int x) 
  {	return x<(1<<(SPbitsXY-1))? x: x-(1<<SPbitsXY);
}
//#define SpSignExtendP(x) (x<(1<<(SPbitsXY-1))? x: x-(1<<SPbitsXY))

//==================================
static char tmpBuf[50];
#include <stdio.h>
static inline char * portDefsStr(portBuf &buf)   {
	sprintf(tmpBuf, "c='%c'=%02x b=%03x t=%.4f: '%s'", buf.cmd, 
			(unsigned int)buf.cmd, buf.buttons, buf.time, buf.buf);
//			buf.buf[0]==1? "": buf.buf);
	return tmpBuf;
}

#endif //__portDefs_hpp__
