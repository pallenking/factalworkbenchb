// ProtoBrain.hpp -- a prototype simple brain interface, for Main to build and run

#ifndef __ProtoBrain_hpp__
#define __ProtoBrain_hpp__

#include "Datum.hpp"

class ProtoBrain {
  public:
	ProtoBrain();
	virtual ~ProtoBrain();

	virtual void inz(int &argc, char *argv[]);
	virtual CommandDatum chooseAction(SensoryDatum senses);

};

#endif // __ProtoBrain_hpp__
