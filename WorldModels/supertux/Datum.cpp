/*
 *  Datum.cpp
 *  Copyright 2011 King Software. All rights reserved.
 *
 */

#include "Datum.hpp"
#include "Common.h"
//#include "NSCommon.h"  // 171022: incompatibale headers

void printSensoryDatum(SensoryDatum *sd) {
    writeSensoryDatum(sd, stdout);
}
void writeSensoryDatum(SensoryDatum *sd, FILE *f) {
	fwrite("screenPiece:", 13, 1, f);
	fwrite(&sd->time, sizeof(float), 1, f);
	fwrite(&sd->tuxLoc, sizeof(worldLoc), 1, f);
	
	char buf[40]; sprintf(buf, "animates[%d]:", (int)sd->animates->size());
	fwrite(buf, strlen(buf), 1, f);
	std::vector<aobj>::const_iterator ao;
	for (ao=sd->animates->begin(); ao!=sd->animates->end(); ao++) {
		fwrite(&ao, sizeof(aobj), 1, f);
	}
	#define id int
	panicC("rethink");
}
//	sprintf(buf, "cur_screen[%d]:", (int)sd->cur_screen->size());
//	fwrite(buf, strlen(buf), 1, f);
//	std::vector<screenPiece>::const_iterator cs;
//	for (cs=sd->cur_screen->begin(); cs!=sd->cur_screen->end(); cs++)   {
//		fwrite(&cs, sizeof(screenPiece), 1, f);
//	}
//
//void screenPieceWrite(screenPiece *sd, FILE *f)   {
//	fwrite("screenPiece:", 13, 1, f);
//	fwrite(&sd->time, sizeof(float), 1, f);
//	fwrite(&tuxLoc, sizeof(worldLoc), 1, f);
//	std::vector<aobj>::const_iterator ao;
//	for (ao=aobj.begin(); ao!=aobj.end(); ao++)   {
//		;
//	}
//}
