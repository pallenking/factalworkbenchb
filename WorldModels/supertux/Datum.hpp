// Datum.hpp -- data types linking brains to SimulatorController

#ifndef __Datum_hpp__
#define __Datum_hpp__
#include "portDefs.hpp"

// ======================================================================
// ============================ command datum ===========================
// ======================================================================

typedef struct   {
	int umph;			// strength of bid
	int buttons; 		// button state in command
	float duration; 	// total duration of command, in sec
	float maxChunkTime;	// maximum simulation chunk time
	char *comment; 		// (sent over plug)
} CommandDatum;
extern CommandDatum commandDatumNULL;

static const int bbRECORD   = 0x20000001;
static const int bbPLAY     = 0x20000002;
static const int bbJOYSTICK = 0x20000003;
static const int bbSTOP_SEQ = 0x20000004;
#define bb(y) /*portBuf::*/button##y


// ======================================================================
// ============================ sensory datum ===========================
// ======================================================================

// unique block identifier
typedef struct ubid   {
	unsigned type :4;
	unsigned uid  :12;	
} ubid;

extern ubid zeroBuid;
inline int buid2int(int type, int uid)	  { return (type<<12) + uid; }
//inline int buid2int(buid a) 			  { return (a->type<<12) + a->uid; }

// types of blocks
const static unsigned BLOCK_UNDEF	= 0;	// not a block at all
const static unsigned BLOCK_UNKNOWN	= 1;	// exception case, undigested
const static unsigned BLOCK_START	= 2;	// tag for first ubid in sequence
const static unsigned BLOCK_IGNORE	= 3;	// known to be irrelavent
const static unsigned BLOCK_MAP		= 4;	// use in mapping
const static unsigned BLOCK_HARD	= 5;	// use in mapping, can stand on
const static unsigned BLOCK_COIN	= 6;	// mostly visual effect, GOOD
const static unsigned BLOCK_BADGUY	= 7;	// moving critter
const static unsigned BLOCK_TUX		= 8;	// self

// ------------------------- ANIMATE OBJECTS ---------------------------
typedef struct aobj   {
	unsigned type :4;
	unsigned uid  :12;
	short x, y;
} aobj;		// unique block identifier

// ------------------------- TUX's physical ----------------------------
typedef struct   {
	char *zone;
	short x, y;
} worldLoc;

//----------------------------------------------------------------------
#include <vector>
typedef struct   {
	float time;									// time of sample

	worldLoc tuxLoc;							// physical of tux		
	std::vector<aobj> *animates;				// vector of critters, 0=tux
	std::vector<screenPiece> *scenery;          // immovable scenery
//	std::vector<screenPiece> *cur_screen;		// vector of blocks
	short coinValue;							// current reward

//		scenery;
} SensoryDatum;
extern SensoryDatum sensoryDatumNULL;

void printSensoryDatum(SensoryDatum *sd);
void writeSensoryDatum(SensoryDatum *sd, FILE *f);


#endif //__Datum_hpp__
