// BitButtonBidirNsV.mm -- Bidirectional Button  C2015PAK

#import "BitButtonBidirNsV.h"
#import "Brain.h"
#import "Part.h"
#import "Port.h"
#import "GenAtom.h"
#import "Path.h"
#import "View.h"
#import "Bundle.h"
#import "Leaf.h"
#import "Brain.h"
#import "BundleTap.h"
#import "Common.h"
#import "NSCommon.h"
#import "DownNUpButton.h"
#import "Id+Info.h"
#import "InspectNsVc.h"
#import "SimNsWc.h"
#import "SampleClock.h"

@implementation BitButtonBidirNsV

#pragma mark 1. Basic Object Level					// Basic objects

- (NSString *)	name;			{			return self.title;				}

- (void) dealloc;				{
	
	self.button			= nil;
	self.title			= nil;
	self.sound			= nil;

	[super dealloc];
}

#pragma mark 4. Factory								// Compound objects
- (void) buildBidirViewWithTarg:(Bundle *)targBun; {

	 // Create the Button
	self.button = [[[DownNUpButton alloc] initWithFrame:NSZeroRect] autorelease];
	[self addSubview:self.button];
	self.button.target = self;						// Left-clicks come to self
	self.button.action = @selector(buttonTapped:);
	[self.button sendActionOn: NSLeftMouseDownMask | NSLeftMouseUpMask];
	self.button.title = self.title;

	 // Wire up Ports:
	if (self.portPath) {	// name of FW Port specified by "portPath"
		id gen = [targBun genPortOfLeafNamed:self.portPath];

		 // Does the target bundle's chosen leaf port have a GenAtom to connect to:
		if (Port *genPort = coerceTo(Port, gen)) {
			GenAtom *genAtom = mustBe(GenAtom, genPort.atom);
			if (id sound = self.sound) {
				[genAtom logEvent:@"has BitButtonBidirNsV '%@': pushing sound %@",	self.title, sound];
				genAtom.sound = sound;
			  }
		  }
		else if (NSNumber *genNumber = coerceTo(NSNumber, gen))
			panic(@"Target bundle's chosen leaf does not have a GenAtom");
			//assert([genNumber intValue]==0, (@"@0 is ignore, others not permitted"));
		else
			[targBun logEvent:@"\nButtonBidirView '%@': no reference found\nERROR\n", self.title];
	  }
	assert(self.clockPath==nil, (@""));

	[super buildBidirViewWithTarg:targBun];		// sets self.targBun = targBun;
  }

- (void) layout {										[super layout];

	/*  170116PAK CAUSES:
	 *	BitButtonBidirNsV or one of its superclasses may have overridden -layout
	 *	without calling super. Or, something may have dirtied layout in the
	 *	middle of updating it. Both are programming errors in Cocoa Autolayout.
	 *	The former is pretty likely to arise if some pre-Cocoa Autolayout class
	 *	had a method called layout, but it should be fixed.
	 *
	 *	study updateViewConstraints;
	 */
	self.button.frame = self.bounds;
  }

#pragma mark 7. Simulator Messages

- (IBAction)buttonTapped:(id)sender {
	int pressed					= (int)[sender isPressed];
	if (sender != self.button)
		NSPrintf(@" ****** Window was regenerated; Something changes here. (May be OK)\n");

	 // Find the generator we are connected to:
	View *v						= mustBe(View, [self.inspectNsVc representedObject]);
	Part *part					= mustBe(Part, v.part);
	Brain *brain				= part.brain;

	  // A tapped Button with a clockPath computes a 1:N code on downstroke, and
	 //  				clocks the state (Previous) on upstroke.
	if (self.clockPath) {
		 // For some reason, DownNUpButton issues a downPress occurs before an upPress. Detect and weed out
		//assert(!(pressed and self.wasPressedOnce), (@"got a 'pressed twice' situation -- document why"));
		if (pressed and !self.wasPressedOnce) {			/// ++ Button DOWN ++
			self.wasPressedOnce 			= true;
			[brain logEvent:@"\n=== DOWN EVENT: '%@' Button of BitButtonBidirNsV", self.title];// on button
///
		  }
		else if (!pressed) {							/// ++ Button UP ++
			self.wasPressedOnce	= false;

			[brain logEvent:@"\n=== UP EVENT: '%@' Button of BitButtonBidirNsV", self.title];// on button
///
		  }
	  }
  }
@end
