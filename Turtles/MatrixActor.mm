//  MatrixActor.mm -- A Matrix forumlation of an Actor (OLD) C2013PAK
// Abandoned.  Needs love.

#ifdef PRETTY_CRUFTY_CODE
#import "Brain.h"
#import "Id+Info.h"
#import "MatrixActor.h"
#import "FactalWorkbench.h"
#import "Splitter.h"
#import "Bundle.h"
#import "Leaf.h"
//#import "GenAtom.h"
#import "Atom.h"
#import "Colors.h"

#import "View.h"
#import "Common.h"
#import "NSCommon.h"
#include <stdlib.h>
#import <GLUT/glut.h>

 // A commonly used distribution:
// float expWeighted (float behind)   {	return expf(-10.0*behind); }

@implementation MatrixActor

#pragma mark 1. Basic Object Level

- (void) dealloc {
	
	[super dealloc];
}

#pragma mark  3. Deep Access

- (id) deepMutableCopy; {				id rv = [super deepMutableCopy];
	panic(@"");
	return rv;
}

#pragma mark 4. Factory
MatrixActor *aMatrixActor(id info) {
	MatrixActor *newbie = anotherBasicPart([MatrixActor class]);

XX	newbie = [newbie build:info];
	return newbie;
}

- (id) build:(id)info; {	[super build:info];

//!	xzzy1 MatrixActor:: 1. 	templates					: @{...}

	if (id viewAsAtom	= [self parameterInherited:@"templates"])
		self.viewAsAtom = [mustBe(NSNumber, viewAsAtom) intValue];
	return self;
}

//
// // common Setter, keeps evidence, beta, state, delta, and context in sequence
//- (void)resizeConEviMatrices_nTemplars:(int)nTemplars; {

	int nEviLeaves = self.evi.nLeaves;
	int nConLeaves = self.con.nLeaves;
	bool dEvi = nEviLeaves != self.nEvi;
	bool dCon = nConLeaves != self.nCon;

	if (dEvi) {
	}

	if (newEvi!=self.nEvi or newK!=self.nTemplars) {		// BETA
		float *newBeta = (float *)calloc( newEvi * newK, sizeof(float));
//		if (self.beta) {			// copy beta->newBeta
//			for (int k=0; k<self.nTemplars; k++)
//				for (int l=0; l<self.nEvi; l++)
//					newBeta[l+newEvi * k] = self.beta[l+self.nEvi * k];
//			free(self.beta); self.beta = 0;
//		}
//		self.beta = newBeta;
	}
	
//	if (newK!=self.nTemplars)   {						// TEMPLAR STATE
//		self.state = (templarState *)realloc(self.state, newK * sizeof(templarState));
//		for (int k=self.nTemplars; k<newK; k++)
//			self.state[k] = (templarState)  {0,0,0,0,0};
//	}
	
	if (newK!=self.nTemplars or newM!=self.nCon) {		// DELTA
		assert(newM>=self.nCon, (@"check/debug this case"));
		float *newDelta = (float *)calloc( newK * newM, sizeof(float));
//		if (self.delta) 		// copy Delta->newDelta
//		  {
//			for (int k=0; k<self.nTemplars; k++) 			// for everything defined so far
//				for (int m=0; m<self.nCon; m++)			// copy old delta's to new
//					newDelta[k+newK * m] = self.delta[k+self.nTemplars * m];
//			
//			free(self.delta); self.delta = 0;
//		}
//		self.delta = newDelta;
	}
	panic(@"watch this:");
//	if (newM!=self.nCon)							// CAUSE
//		[self.con padToNReferents:newM up:0];
	
	self.nEvi = newEvi;		// access base objects
	self.nTemplars = newK;
	self.nCon = newM;
}

//- (float &) betaL :(int)l k:(int)k; {
//	assert(l>=0 and l<self.nEvi, (@"betaL illegal l=%d", l));
//	assert(k>=0 and k<self.nTemplars, (@"betaL illegal k=%d", k));
//	return self.beta[l + self.nEvi * k];
//}
//- (float &) deltaK:(int)k m:(int)m; {
//	assert(k>=0 and k<self.nTemplars, (@"deltaK illegal k=%d", k));
//	assert(m>=0 and m<self.nCon, (@"deltaK illegal m=%d", m));
//	return self.delta[k + self.nTemplars * m];
//}

#pragma mark 8. Reenactment Simulator
//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//
//**//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*/
//***//*//*//*//*//*//*//*//  Reenactment Simulator  //*//*//*//*//*//*//*/
//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//
//**//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*/
- (void) simulateDown:(bool)downLocal; {
	return [super simulateDown:downLocal];
	panic(@"");

	if (!downLocal) {				//simStepUp
		panic(@"");	//self.nEvi==[self.evi nBitsInSubtree], ("nEvi=%d but ||evi||=%d", self.nEvi, [self.evi nBitsInSubtree]));
		
		/*__block*/ int changed=0;
		do {	//////
				////// See "Modeling in Sparse Spaces" Fig 11: for data flow
				//////

			 // BETA MATRIX		
			self.maxActivation = 0.0;
			for (int k=0; k<self.nTemplars; k++) {							/// K TEMPLARS ///
				/*__block*/ float rawActivation = 1.0;

				__block int l = 0;
				[self.evi forAllLeafs:^(Leaf *tb) {
					Port *genPort = mustBe(Port, [tb port4leafBinding:@"G"]);
					genPort.valueTake		= 0;
//					if (GenAtom *genAtom	= coerceTo(GenAtom, genPort.atom))
//						genAtom.inValue		= 0;
//					else
//						genPort.valueTake	= 0;

					panic(@"");
//					assert(l<self.nEvi, (@""));
//	//				assert([r isKindOfClass:[Splitter class]], (@"must be Splitter"));
//					if ([self betaL:l k:k] == 1.0) {
//						float activation = r.value;  //******//
//						rawActivation = std::min(rawActivation, activation);
//					}
//					l++;
				} ];										//##BLOCK
//				self.state[k].rawActivation = rawActivation;
//				self.maxActivation = max(self.maxActivation, self.state[k].rawActivation);
			}
				
			 // CENTRAL
			float weightedBehindBSum = 0.0; //Templar's Behind-ness
			for (int k=0; k<self.nTemplars; k++) { 						/// K TEMPLARS ///
//				float behindB = self.maxActivation - self.state[k].rawActivation;
//				float weightedBehindB = (*self.hB)(behindB);
//				self.state[k].weightedBehindB = weightedBehindB;
//				weightedBehindBSum += weightedBehindB;
			}

			 // Normalized 
//			for (int k=0; k<self.nTemplars; k++)							/// K TEMPLARS ///
//				self.state[k].activation = self.state[k].weightedBehindB / weightedBehindBSum;
			
panic(@"");
//			 /// DELTA MATRIX
//			__block int m=-1;
//			[self forAllLeafs:^(Part *r) {
//				panic(@"");
////				m++;		//### RECURSIVE
////				assert(m<self.nEvi, (@""));
////				assert([r isKindOfClass:[Splitter class]], (@"must be Splitter"));
////				self.maxActivation = 0;
////				for (int k=0; k<self.nTemplars; k++)							/// K TEMPLARS ///
////					if ([self deltaK:k m:m] == 1.0)
////						self.maxActivation = max(self.maxActivation, self.state[k].activation);
////				
////				r.valueTake = self.maxActivation;						  //******//
////				changed |= r.valueChanged;
//			}];											//##BLOCK
//				
//			m=-1;
//			[self forAllLeafs:^(Part *r) {
//				panic(@"");
////				m++;
////				assert([r isKindOfClass:[Splitter class]], (@"must be Splitter"));
////				self.maxActivation = 0;
////				for (int k=0; k<self.nTemplars; k++)							/// K TEMPLARS ///
////					if ([self deltaK:k m:m] == 1.0)
////						self.maxActivation = max(self.maxActivation, self.state[k].activation);
////				
////				r.valueTake = self.maxActivation;						  //******//
////				changed |= r.valueChanged;
//			}];											//##BLOCK
//			
//			if (changed)
//			[self logEvent:@"simStepUp: ev<%@> -> ca<%@>",
//						 [self.evi ppTreeOfNames], [self/*.con*/ ppTreeOfNames]];
///			logEventO LD(localDown, @"simStepUp: ev<%@> -> ca<%@>",
///						 [self.evi ppTreeOfNames], [self/*.con*/ ppTreeOfNames]);
//			changed = 0;		// set to 1 in debugger to repeat
		} while (changed==1);
	}
	else   {

		int changed=0;
		do {		/// DELTA MATRIX
//			for (int k=0; k<self.nTemplars; k++) // clear templar desires
//				self.state[k].desire = 0;		//<<tkk.d>>
//
//			 // for all users
//			for (int m=0; m<self.nCon; m++) {						/// M CAUSES ///
//				float weightedBehindCSum = 0.0;
//				for (int k=0; k<self.nTemplars; k++) {					/// K TEMPLARS ///
//					float behindC = self.maxActivation - self.state[k].activation;
//					float weightedBehindC = (*self.hC)(behindC);
//					weightedBehindCSum += weightedBehindC;
//					self.state[k].weightedBehindC = weightedBehindC;
//				}
//					// Normalize distribution, and modulate desires.
//				for (int k=0; k<self.nTemplars; k++)	/// K TEMPLARS ///
//					self.state[k].desire += self.state[k].weightedBehindC / weightedBehindCSum;
//			}

			 /// CENTRAL
			
			 /// BETA MATRIX == Push each templar's desire back to the evi
			for (int k=0; k<self.nTemplars; k++) {						/// K TEMPLARS ///
				 // 1. Compute state[k].weightedBehindA and their sum
				/*__block*/ float weightedBehindASum = 0.0;
				/*__block*/ int l=-1;
				[self.evi forAllLeafs:^(Leaf *r) {
					panic(@"");
//					l++;
//					// assert([r isKindOfClass:[Splitter class]], (@"must e Splitter"));
//
//					float betaLK = [self betaL:l k:k];
//					if (betaLK == 1.0) {
//						float behindA = self.state[k].rawActivation - r.value;
//						float weightedBehindA = (*self.hA)(behindA);
//						self.state[k].weightedBehindA = weightedBehindA;
//						weightedBehindASum += weightedBehindA;
//					}
				}];										//##BLOCK
				// 2. their sum by desire
//				self.state[k].rawDesire = weightedBehindASum * self.state[k].desire;
			}

			 // BETA MATRIX again, this time to compute evi desires
			/*__block*/ int l=-1;
panic(@"");
//			[self forAllLeafs:^(Part *r) {
//				l++;
//				assert([r isKindOfClass:[Splitter class]], (@"must e Splitter"));
//
//				 // Sum desire over all templars:
//				float desire = 0;
//				for (int k=0; k<self.nTemplars; k++)						/// K TEMPLARS ///
//					desire += self.state[k].rawDesire * self.state[k].weightedBehindA;
//
//				panic(@"");
////				changed |= r.desire != desire;
////				r.desire = desire;
//			}];										//##BLOCK
//			
//			if (changed)  {
//	//			logEve xx nt(@"simStepDown ev<%@> -> ca<%@>", [self.evi ppTreeOfNames],
//	//					 [self.con ppTreeOfNames]);
//				changed = 0;		// set to 1 to repeat
//			}
		}
		while (changed==1);
	}
}
#pragma mark 11. 3D Display
//// // // // // // // // // // // // // // // // // // // // // // // //
/// // // // // // // // // // 3D Display  // // // // // // // // // // 
// // // // // // // // // // // // // // // // // // // // // // // // //


- (void)drawFullView:(View *)v context:(RenderingContext *)rc; {
	return [super drawFullView:v context:rc];

	float dY = self.nTemplars*2;			// height being displayed
	float y0 = -dY/2.0 + 1.0;		// everybody's bottom
	
	float dXb = self.nEvi * 2;
	float dXc = 2 * 2;
	float dXd = self.nCon * 2;		// total = (nEvi+2+nCon)*u
	
	float dX  = dXb + dXc + dXd;	// width being displayed
	float xB = -dX/2.0;				// Beta matrix's left side
	float xC = xB + dXb;			// Center's left side
	float xD = xC + dXc;			// Delta matrix's left side
	float xE = xD + dXd;			// Delta matrix's right side
	
	///// ///// ///// BETA Matrix for Basis ///// ///// /////
	 // 1. Basis RECTANGLE outline
	rc.color = colorPurple;
	glBegin(GL_LINE_STRIP);
	glVertex3f(xB,     y0-1.0,    0);
	glVertex3f(xB,	   y0-1.0+dY, 0);
	glVertex3f(xB+dXb, y0-1.0+dY, 0);
	glVertex3f(xB+dXb, y0-1.0,    0);
	glVertex3f(xB,     y0-1.0,    0);
	glEnd();
	
	/*__block*/ float x = xB + 1.0;
	/*__block*/ int l=-1;
//	[self forAllLeafs:^(Part *r) {
//		 // 2. Basis vertical lines
//		panic(@"");
////		float desi = [self fooValueXX];
////		
////		float desi2 = [self value:0];
////		
////		[rc renderGlcolorOfBidir:[self value:0] :desi];//];
////		[rc renderGlcolorOfBidir:r.sensation :r.desire];
//		glBegin(GL_LINE_STRIP);
//		glVertex3f(x, y0-1.0+dY, 0);
//		glVertex3f(x, y0-1.0,    0);	// to evi factal:
//		glEnd();
//		
//		 // 3. Beta Matrix elements
//		float y = -dY/2 + 1.0;
//		rc.color = colorBlack;
//		for (int k=0; k<self.nTemplars; k++)
//			if ([self betaL:l k:k]) {
//				glPushMatrix();
//					glTranslatef(x, y, 0);
//					glutSolidCube(1.0);
//				glPopMatrix();
//				y += 2.0;
//			}
//		x += 2.0;
//	}];										//##BLOCK
//	
	///// ///// ///// CENTER Templar processing: ///// ///// /////
	// RECTANGLE for CENTER matrix
	glBegin(GL_QUADS);
	glVertex3f(xC+1.0,     y0-1.0,    0.0);
	glVertex3f(xC+1.0,	  y0-1.0+dY, 0.0);
	glVertex3f(xC-1.0+dXc, y0-1.0+dY, 0.0);
	glVertex3f(xC-1.0+dXc, y0-1.0,	 0.0);
	glEnd();
	
	
	///// ///// ///// DELTA Matrix for Cause ///// ///// /////
	// 1. Cause RECTANGLE outline
	glBegin(GL_LINE_STRIP);
	glVertex3f(xD,     y0-1.0,     0);
	glVertex3f(xD,	   y0-1.0+dY, 0);
	glVertex3f(xD+dXd, y0-1.0+dY, 0);
	glVertex3f(xD+dXd, y0-1.0,     0);
	glVertex3f(xD,     y0-1.0,     0);
	glEnd();
	
	x = xD + 1.0;
	/*__block*/ int m=-1;
//	[self forAllLeafs:^(Part *r) {
//		m++;
//		assert([r isKindOfClass:[Splitter class]], (@"must e Splitter"));
//		
//		// 2. Vertical lines for causes
//		panic(@"");
////		[rc renderGlcolorOfBidir:[self value:0] :[self value:0]];
////		[rc renderGlcolorOfBidir:value[0]:value[1]];
////		[rc renderGlcolorOfBidir:r.sensation:r.desire];
//		glBegin(GL_LINE_STRIP);
//		glVertex3f(x, y0-1.0,    0);	// to causes factal:
//		glVertex3f(x, y0-1.0+dY, 0);
//		
//		// wire to causes
//		//			if ([v existingViewOfModel:self.causes])   {
//		//				Vector3f factalPosition = caM.posn.getTranslation();
//		//				glVertex3fv(factalPosition);
//		//			}
//		glEnd();
//		
//		// 3. Delta matrix elements
//		float y = y0;
//		rc.color = colorBlack;
//		for (int k=0; k<self.nTemplars; k++) {			// Delta matrix elements
//			if ([self deltaK:k m:m]) {
//				glPushMatrix();
//					glTranslatef(x, y, 0);
//					glutSolidCube(1.0);
//				glPopMatrix();
//			}
//			y += 2.0;
//		}
//		x += 2.0;
//	}];										//##BLOCK
	
	///// ///// ///// Horizontal Templar Lines ///// ///// /////
	float y = y0;
	for (int k=0; k<self.nTemplars; k++) {
//		rc.color = colorOf2Ports(self.state[k].activation, self.state[k].desire, 0);
		glBegin(GL_LINES);
		glVertex3f(xB, y, 0);
		glVertex3f(xE, y, 0);
		glEnd();
		y += 2.0;
	}
}
#pragma mark 15. PrettyPrint
    ///\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\///
   ///\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\///
  ///\\//\\//\\//\\//\\//\\//\\// PRINTOUT //\\//\\//\\//\\//\\//\\//\\//\\///
 ///\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\///
///\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\///
- (NSString *) pp:aux; {
	id rv = [super pp:aux];

	if (self.nTemplars > 0) {
		rv=[rv addF:@" k: Beta[l,k]%*s", self.nEvi*2-7, ""];
		rv=[rv addF:@"rawAct behiB behiC Act   Des  Delta[k,m]\n"];
	}
	for (int k=0; k<self.nTemplars; k++) {
//		templarState *s = &self.state[k];
		rv=[rv addF:@"%3d: <<", k];
		 // evi:
		for (int l=0; l<self.nEvi; l++) {
			float betaLK = 0;//[self betaL:l k:k];
			rv=[rv addF:@"%c ", betaLK==1.0? '1': betaLK==0.0? '0': '*'];
		}
		 // wta:
//		rv=[rv addF:@" %.3f %.3f %.3f %.3f %.3f ", s->rawActivation,
//			   s->weightedBehindB, s->weightedBehindC, s->activation, s->desire];
		// users:
		for (int m=0; m<self.nCon; m++) {
			float deltaKM = 0;//[self deltaK:k m:m];
			rv=[rv addF:@"%c ", deltaKM==1.0? '1': deltaKM==0.0? '0': '*'];
		}
		rv=[rv addF:@">>\n"];
	}
	if (self.evi)
		rv=[rv addF:@"-evi=%@\n", self/*.con*/.name];
	return rv;
}
@end
#endif
