//  MatrixActor.h -- A Matrix forumlation of an Actor (OLD) C2013PAK

#import "Actor.h"
@class Bundle;

//typedef float (*weightFunction)(float);
//float expWeighted (float behind);

@interface MatrixActor : Actor

    @property (nonatomic, assign) int nEvi;     // number of lower evi shares
    @property (nonatomic, assign) int nCon;     // number of users
    @property (nonatomic, assign) int nTemplars;// number of templars

    @property (nonatomic, retain) NSMutableArray *templars;
f
@end

@interface Templar : Actor

     /// list of indices
    @property (nonatomic, retain) NSMutableArray *evi;
    @property (nonatomic, retain) NSMutableArray *con;

@end

