//  NSCommon.h -- common Cocoa (NS) helpers

#import <Cocoa/Cocoa.h>

float detents(float val, NSArray *detents);
void commonNSunitTest();

int NSPrintf(NSString *fmtStr, ...) __attribute__((format(NSString, 1, 2)));
//int NSPrintf(char *fmtStr, ...) ERROR NSPrintf needs @"" format string
void printNSRect(NSString *prefix, NSRect rect, NSString *postfix);

void printNSNumber        (NSString *prefix, NSNumber     *a, int indent);
void printNSScanner       (NSString *prefix, NSScanner    *a, int indent);
void printNSString        (NSString *prefix, NSString     *a, int indent);
void printNSSet           (NSString *prefix, NSSet        *a, int indent);
void printNSArray         (NSString *prefix, NSArray      *a, int indent);
void printNSArrayFormatted(NSString *prefix, NSArray      *a, SEL printMethod, int indent);
void printNSDictionary    (NSString *prefix, NSDictionary *a, int indent);
void printNSColor         (NSString *prefix, NSColor      *a, int indent);

 // return the address of pointer b, if it is a kind of a, else nil
#define coerceTo(a, b) ({id __b=(b); [__b isKindOfClass:[a class]]? (a *)__b: nil;})

 // return the address of pointer b, if it is a kind of a, else PANICs
#define mustBe(a, b)   (  {	id __b=(b);	[__b isKindOfClass:[a class]]? (a *)__b: \
		({panic(@"mustBe: '%s' is not a kind of '%s'", #b, #a); (a *)nil;});	})




//#define panicC()	panicC("")	can't overload macro args
#define panic(fmt...)					\
//({do {									\
//	panic_gutsX(fmt);					\
////	doBreakpointInstruction;			\
//	  {	int i;							\
//		for (i=0; i<100; i++);			\
//	}									\
//} while (0); (long int)0;})
  // N.B: This must work in .m, .mm and .cpp files,
 // but .cpp does not like id and
// __cplusplus is true for .mm

void panic_gutsX(NSString *msg, ...) __attribute__((format(NSString, 1, 2)));

#undef assert			// this is needed, because of conflicts in
// /usr/lib/gcc/i386-redhat-linux/4.0.0/../../../../include/c++/4.0.0/cassert
// For this fix to work, the '#include "Common.h"' must be last.

#define assert(predicate, printflist)	\
	({do   {                            \
		if ((predicate)==0)             \
			panic printflist;           \
	} while (0);	0;})

