//  NSCommon.mm -- common Cocoa (NS) helpers

//https://www.gnu.org/software/libc/manual/html_atom/Printf-Extension-Example.html
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <printf.h>

#import "HnwObject.h"
#import "NSCommon.h"
#import "Common.h"			// problems if before HnwObject.h

void panic_gutsX (NSString *msg, ...) {

	fflush(stdout);					// purge any previous output
	NSPrintf(@"\\\\\n \\\\\n  \\\\");
	int nDashes = 40;
	for (int i=0; i<nDashes; i++)
		NSPrintf(@"-");
//		printf("%s", "-");
	printf(" %s ERROR/WARNING ----\n", fWallTime((char *)"%y%m%d/%H:%M:%S"));

	static char message[1000];
	va_list ap;
	va_start(ap, msg);
	id rv = [[[NSString alloc] initWithFormat:msg arguments:ap] autorelease];
//	vsprintf(message, msg, ap);
	va_end(ap);

	NSPrintf(@"   >>-- %@\n  //-----------------------------", rv);
	NSPrintf(@"----------------------------------------\n //\n//\n");
}

//--------------
//static int printf_arginfo_M(const struct printf_info *info, size_t n, int *argtypes) {
//	if (n > 0) 
//		argtypes[0] = PA_POINTER;
//	return 1;
//}
//static int printf_output_M(FILE *stream, const struct printf_info *info, const void *const *args) {
//	const unsigned char *mac;
//	int len;
//	mac = *(unsigned char **)(args[0]);
//	len = fprintf(stream, "%02x:%02x:%02x:%02x:%02x:%02x",
//			mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
//	return len;
//}
////int register_MB() {
////	uint8_t mac[6] = { 0x00, 0x11, 0x22, 0x33, 0x44, 0x55 };
////	  // printf.h: NS doesn'tsupport the GLIBC register_printf_function() or
////	 //  FreeBSD register_printf_render_std(), because they affect printf globally and are unsafe.
////	register_printf_function ('B', printf_output_B, printf_arginfo_B);
////	return 0;
////};
//--------------

float detents(float val, NSArray *detents) {
	float length = .05;
	float valI1 = -HUGE_VALF;
	for (NSNumber *detent in detents) {
		float valI = detent.integerValue;
		if (val <= valI) {				// we have found a superior point
			if ( valI1==valI1 ) {			// there is a previous value
				float minPt = valI1+length/2, maxPt = valI-length/2;
				if (val < minPt)
					val = minPt;
				else if (val > maxPt)
					val = minPt;
				else
					val = valI1 + (val-minPt)/(maxPt-minPt);
			}
			else {							// there is no previous value
				float minPt = valI1+length/2, maxPt = valI-length/2;
				if (val < minPt)
					val = minPt;
				else if (val > maxPt)
					val = minPt;
				else
					val = valI1 + (val-minPt)/(maxPt-minPt);
			}
		}
	}
	return 0;
}

void printNSRect(NSString *prefix, NSRect rect, NSString *postfix) {
	NSPrintf(@"%@=o(x%.0f,y%.0f)s(w%.0f,h%.0f)%@", prefix, rect.origin.x,
			 rect.origin.y, rect.size.width, rect.size.height, postfix);
}

int NSPrintf(NSString *fmtStr, ...) {
	va_list argp;
	va_start(argp, fmtStr);
	
	NSString *str = [[[NSString alloc] initWithFormat:fmtStr arguments:argp] autorelease];
    printf("%s", [str UTF8String]);

	va_end(argp);
    return -1;  // not same return as printf
}

void printNSNumber(NSString *prefix, NSNumber *a, int indent)   {
    if (prefix == 0)
        prefix = [NSString stringWithFormat:@"(%@ *)", [a className] ];
    NSPrintf(@"%@ %p = %d = 0x%x\n", prefix, a, [a intValue], [a intValue]);
}

void printNSScanner(NSString *prefix, NSScanner *a, int indent) {
    if (prefix == 0)
        prefix = [NSString stringWithFormat:@"(%@ *)", [a className] ];
    NSPrintf(@"%@ %p is at %d in @\"%@\"\n", prefix, a, (int)[a scanLocation], [a string]);
}

void printNSString(NSString *prefix, NSString *a, int indent) {
    if (prefix == 0)
        prefix = [a className];
    NSPrintf(@"(%@ *)%p = @\"%@\" ", prefix, a, a);
}

void printNSSet(NSString *prefix, NSSet *a, int indent) {
    if (prefix == 0)
        prefix = [a className];
    int n = (int)[a count];
    if (n == 0)
        NSPrintf(@"%@ empty", prefix);
    else  {
        NSEnumerator *enumerator = [a objectEnumerator];
//		NSPrintf(@"%@ array of %d: ", prefix, n);
		NSPrintf(@"%@ array of %d: [[", prefix, n);
        id ob;
        for (int i=0 ; i<n; i++) {
            ob = [enumerator nextObject];
			panic(@"");
            if (/* DISABLES CODE */ (0))//[ ob respondsToSelector:@selector(namex) ])
                NSPrintf(@"%@ ", [ob name]);
            else if ([ ob respondsToSelector:@selector(UTF8String) ])
                printf("\"%s\" ", [ob UTF8String]);
            else
                NSPrintf(@"(%@ *)%p ", [ob className], ob);
            if (i<n-1 and i%10==6)
                printf("\n%*s", indent*3+3, "");
		}
		printf("]]");
	}
}
void printNSArray(NSString *prefix, NSArray *a, int indent) {
    if (prefix == 0)
        prefix = [a className];
    int n = (int)[a count], i=-1;
    if (n == 0)
        NSPrintf(@"%@ empty ", prefix);
    else {
        NSPrintf(@"(%@ *)%p array of %d: [", prefix, a, n);
        for (id obj in a) {
            if ([ obj respondsToSelector:@selector(name) ])
                NSPrintf(@"%@ ", [obj name]);
            else if ([ obj respondsToSelector:@selector(UTF8String) ])
                printf("\"%s\" ", [obj UTF8String]);
            else
                NSPrintf(@"(%@ *)%p ", [obj className], obj);
            if (++i<n-1 and i%10==6)
                printf("\n%*s", indent*3+3, "");
		}
		printf("]");
	}
}
void printNSArrayFormatted(NSString *prefix, NSArray *a, SEL printMethod, int indent) {
    //    SEL pMethod = printMethod? printMethod: 0;//@selector(selector);
    int n = (int)[a count], i=-1;
    if (n == 0)
        NSPrintf(@"%@ empty", prefix);
    else {
//		NSPrintf(@"%@ ", prefix);
		NSPrintf(@"%@ array of %d: [", prefix, n);
        for (HnwObject *b in a) {
            if (printMethod == 0)
                printf("%s ", [[b name] UTF8String]);
            else {
                // printf("%s=", [b namex]);
                [b performSelector:printMethod];
                printf(" ");
            }
            if (++i<n-1 and i%10==6)
                printf("\n%*s", indent*3+3, "");
		}
		printf("]");
	}
}

void printNSDictionary(NSString *prefix, NSDictionary *a, int indent) {
    if (prefix == 0)
        prefix = [a className];
    int n = (int)[a count];
    if (n == 0)
        NSPrintf(@"%@ empty\n", prefix);
    else {
        NSPrintf(@"%@: dictionary of %d {\n", prefix, n);
        NSEnumerator *enumerator = [a keyEnumerator];
        for (NSString *key in enumerator)  {
            printf("%*s", indent*3+3, "");
            id value = [a objectForKey:key];
            //if ([value isKindOfClass:[xxx class]])
            NSPrintf(@"%@ --> %@\n", key, value);
		}
		printf("}");
	}
}
void printNSColor(NSString *prefix, NSColor *a, int indent) {
    if (prefix == 0)
        prefix = [a className];
    NSPrintf(@"%@: color ", prefix);
    
    CGFloat rgba[4];
    [a getRed:rgba+0 green:rgba+1 blue:rgba+2 alpha:rgba+3];
    for (int i=0; i<4; i++)
        printf("%f ", rgba[i]);
    printf("\n");
}


