// SystemTypes.h -- common system types C2013PAK

#ifndef SystemTypes_h

#import "vmath.hpp"		//http://bartipan.net/vmath/doc/

#import "Colors.h"

 // FactalWorkbench EVENT Types:
#define definedFwTypes													\
	fwTypeDefn(sim_nop=0,				@"nop")					/* */   \
	fwTypeDefn(sim_writeHeadConcieve,	@"writeHeadConcieve")	/* */   \
	fwTypeDefn(sim_writeHeadLabor,		@"writeHeadLabor")		/* */   \
	fwTypeDefn(sim_clockPrevious,		@"clockPrevious")		/* */   \
/*	fwTypeDefn(sim_reconfigure,			@"reconfigure")			   */   \

#define fwTypeDefn(val, string) val,
typedef enum   { definedFwTypes } FwType;				//1: typedef:
#undef fwTypeDefn
#define fwTypeDefn(val, string) string,
static NSArray	*fwTypeDefnNames = @[ definedFwTypes ];	//2: names array:

 // =============== Rendering Modes determine OGL coloration ==============
#define definedRenderModes										/* 0: values	*/\
	renderKind(render3DUndefined=0, @"3DUndef")    /* undefined mode			*/\
	renderKind(render3Dlighting,    @"3Dlighting") /* realistic color in 3D view*/\
	renderKind(render3Dcartoon,     @"3Dcartoon")  /* cartoon colors in 3D view */\
	renderKind(renderColorTags,     @"3DColorTags")/* colors in 3D view for pic */\
	renderKind(renderFindTagObject, @"3DFindTag")  /* find object with tag color*/

#define renderKind(val, string) val,
typedef enum   { definedRenderModes } RenderModes;			// 1: typedef:
#undef renderKind

#define renderKind(val, string) string,
static NSArray	*renderModeKindArray = @[ definedRenderModes ];	// 2: renderMode kind array:
RenderModes		renderModeKindOfString(NSString *str);

 // =============== Event types ==============
typedef struct {

	FwType			fwType;

	unsigned char	nsType;
				// As defined in NSEvent.NSEventType:
			   //NSLeftMouseUp NSRightMouseDown NSRightMouseUp NSMouseMoved
			  //NSLeftMouseDragged NSRightMouseDragged
			 //NSMouseEntered NSMouseExited
			//NSKeyDown NSKeyUp NSFlagsChanged (deleted PAK170906)
		   //NSPeriodic NSCursorUpdate NSScrollNSTablet NSTablet
		  //NSOtherMouse NSOtherMouseUp NSOtherMouseDragged
		 //NSEventTypeGesture NSEventTypeMagnify NSEventTypeSwipe NSEventTypeRotate
		//NSEventTypeBeginGesture NSEventTypeEndGesture NSEventTypeSmartMagnify NSEventTypeQuickLook

	unsigned char	buttonNumber;		// 0=left, ...
	unsigned char	clicks;				// 1, 2, 3?

	unsigned char	key;

	unsigned long	modifierFlags;
		  // As defined in NSEvent.modifierFlags:
		 // NSAlphaShiftKeyMask NSShiftKeyMask NSControlKeyMask NSAlternateKeyMask
		// NSCommandKeyMask NSNumericPadKeyMask NSHelpKeyMask NSFunctionKeyMask

	Vector3f		mousePosition;	// after[self convertPoint:[theEvent locationInWindow] fromView:nil]

	Vector3f		deltaPosition;	// since last time, in screen units
	Vector3f		deltaPercent;	// since last time, in percent of screen

	float			scrollWheelDelta;
	
} FwEvent;

const unsigned long int FWKeyUpModifier = 1;	// an extension of NSEvent modifierFlags

//@class View;

#define assignWeak assign
static int nibsWidth = 368;			// hack


#define SystemTypes_h
#endif
