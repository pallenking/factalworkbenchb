// Common.h -- generic helpful utilities

#ifndef __Common_hpp__
#define __Common_hpp__

// useful c libraries everybody should know about (spread the word!)
#include <stdio.h>
#include <string.h>
#include <stdarg.h>			// linux
#include <stdlib.h>			// linux

// use externC on C .h-files that C++ uses
#ifdef __cplusplus
#	define externC extern "C"
#else
#	define externC extern
#endif

typedef unsigned short		US;
typedef unsigned short		USHORT;
typedef unsigned char		UC;
typedef unsigned int		UI;
typedef long unsigned int	LUI;

#ifndef self
	extern void *self;		// for non OBJC routines
#endif
extern void *panicsSelf;

#define oldPanic
//#ifdef oldPanic
// This is i386-specific:
#define doBreakpointInstruction         \
	asm volatile ("	int3	" : :);

//#define panicC()	panicC("")	can't overload macro args
#define panicC(fmt...)					\
//({do {									\
//	panic_gutsC(fmt);					\
////	doBreakpointInstruction;			\
//	  {	int i;							\
//		for (i=0; i<100; i++);			\
//	}									\
//} while (0); (long int)0;})
  // N.B: This must work in .m, .mm and .cpp files,
 // but .cpp does not like id and
// __cplusplus is true for .mm

void panic_gutsC(const char *fmt, ...) __attribute__ ((format (printf, 1, 2)));

//? #undef assert			// this is needed, because of conflicts in
#define assertC(predicate, printflist)	\
//	({do   {                            \
//		if ((predicate)==0)             \
//			panicC printflist;           \
//	} while (0);	0;})
//#endif

externC double runTime();						// time in seconds into run

externC char *wallTime();
externC char *fWallTime(char *fmt);

#define min(A, B) (  {typeof (A) __a=(A), __b=(B); (__a<__b)? __a: __b; })
#define max(A, B) (  {typeof (A) __a=(A), __b=(B); (__a>__b)? __a: __b; })
#define abs(A)	  (  {typeof (A) __a=(A); (__a>0)? __a: -__a; })
#define square(A) (  {typeof (A) __a=(A); __a * __a; })

externC int randomProb (float p);				// 1 with prob p
externC float randomDist (float a, float b);	// float boxcar, a..b, incl
void setRandomSeed (long int seed);

externC void removeNullArgs(int &argc, char *argv[]);

#endif //__Common_hpp__
