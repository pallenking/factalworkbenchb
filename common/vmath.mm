/*
 * vmath, set of classes for computer graphics mathemtics.
 * Copyright (c) 2005-2006, Jan Bartipan < barzto at gmail dot com >
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 *
 * - Redistributions of source code must retain the above copyright 
 *   notice, this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright 
 *   notice, this list of conditions and the following disclaimer in 
 *   the documentation and/or other materials provided with the 
 *   distribution.
 * - Neither the names of its contributors may be used to endorse or 
 *   promote products derived from this software without specific 
 *   prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS 
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE 
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */

#import "vmath.hpp"
#include <math.h>
#import "Brain.h"
#import <Cocoa/Cocoa.h>
#import <GLUT/GLUT.h>
#import "Common.h"

#ifdef VMATH_NAMESPACE
namespace VMATH_NAMESPACE
  {
#endif

template class Vector2<float>;
template class Vector2<double>;
template class Vector3<float>;
template class Vector3<double>;
template class Vector4<float>;
template class Vector4<double>;
template class Matrix3<float>;
template class Matrix3<double>;
template class Matrix4<float>;
template class Matrix4<double>;
template class Quaternion<float>;
template class Quaternion<double>;
template class Bounds3<float>;
template class Bounds3<double>;

void print (Vector3d val) {
	printf("(%.2f, %.2f, %.2f)", val.x, val.y, val.z);
}

Vector4f const vector4fNan  = Vector4f(nan("null Vector4f"), 0, 0, 1);


const int isNan(float const &p) {
	return p!=p;
}

//template <class T>
//    const int isNan(Vector3<T> const &p) {
//        return p.x!=p.x or p.y!=p.y or p.z!=p.z;
//  }
const int isNan(Vector3f const &p) {
	return p.x!=p.x or p.y!=p.y or p.z!=p.z;
}
const int isNan(Vector3d const &p) {
	return p.x!=p.x or p.y!=p.y or p.z!=p.z;
}

//template <class T>
//    const int isNan(Vector4<T> const &p) {
//        return p.x!=p.x or p.y!=p.y or p.z!=p.z or p.w!=p.w;
//    }
const int isNan(Vector4d const &p) {
	return p.x!=p.x or p.y!=p.y or p.z!=p.z or p.w!=p.w;
}
const int isNan(Vector4f const &p) {
	return p.x!=p.x or p.y!=p.y or p.z!=p.z or p.w!=p.w;
}

//template <class T>
//    const int isNan(Bounds3<T> const &p) {
//        return isNan(p.point0()) or isNan(p.point1());
//    }
const int isNan(Bounds3f const &p) {
    return isNan(p.point0()) or isNan(p.point1());
}
const int isNan(Bounds3d const &p) {
    return isNan(p.point0()) or isNan(p.point1());
}

const int isNan(Matrix3f const &p) {
	for (int i=1; i<3; i++)
		for (int j=1; j<3; j++)
			if (isNan(p.at(i,j)))
				return 1;
	return 0;
}
const int isNan(Matrix3d const &p) {
	for (int i=1; i<3; i++)
		for (int j=1; j<3; j++)
			if (isNan(p.at(i,j)))
				return 1;
	return 0;
}

const int isNan(Matrix4f const &p) {
	for (int i=1; i<3; i++)
		for (int j=1; j<3; j++)
			if (isNan(p.at(i,j)))
				return 1;
	return 0;
}
const int isNan(Matrix4d const &p) {
	for (int i=1; i<3; i++)
		for (int j=1; j<3; j++)
			if (isNan(p.at(i,j)))
				return 1;
	return 0;
}

//template <class T>
//    const Vector3<T> minOf(const Vector3<T> &a, const Vector3<T> &b) {
//        return Vector3<T>(std::min(a.x, b.x), std::min(a.y, b.y), std::min(a.z, b.z));
//    }
const Vector3f minOf(const Vector3f &a, const Vector3f &b) {
	if (isNan(a))	// (if any part is nan, the whole vector is)
		return b;
	if (isNan(b))
		return a;
    return Vector3f(min(a.x, b.x), min(a.y, b.y), min(a.z, b.z));
//  return Vector3f(std::min(a.x, b.x), std::min(a.y, b.y), std::min(a.z, b.z));
}
const Vector3d minOf(const Vector3d &a, const Vector3d &b) {
	if (isNan(a))	// (if any part is nan, the whole vector is)
		return b;
	if (isNan(b))
		return a;
    return Vector3d(min(a.x, b.x), min(a.y, b.y), min(a.z, b.z));
//  return Vector3d(std::min(a.x, b.x), std::min(a.y, b.y), std::min(a.z, b.z));
}

//template <class T>
//    const Vector3<T> maxOf(const Vector3<T> &a, const Vector3<T> &b) {
//        return Vector3<T>(std::max(a.x, b.x), std::max(a.y, b.y), std::max(a.z, b.z));
//    }
const Vector3f maxOf(const Vector3f &a, const Vector3f &b) {
	if (isNan(a))	// (if any part is nan, the whole vector is)
		return b;
	if (isNan(b))
		return a;
    return Vector3f(max(a.x, b.x), max(a.y, b.y), max(a.z, b.z));
//  return Vector3f(std::max(a.x, b.x), std::max(a.y, b.y), std::max(a.z, b.z));
}
const Vector3d maxOf(const Vector3d &a, const Vector3d &b) {
	if (isNan(a))
		return b;
	if (isNan(b))
		return a;
    return Vector3d(max(a.x, b.x), max(a.y, b.y), max(a.z, b.z));
//  return Vector3d(std::max(a.x, b.x), std::max(a.y, b.y), std::max(a.z, b.z));
}

Matrix4f rotationMatrixVerticalizing(
	Vector3f uY,						// the vector that maps to iY
	Vector3f iY
				) {
	Vector3f uX = uY.crossProduct(iY);	// the vector that maps to iX
	if (uX.length() <  1e-4)			// already verticalized?
		return Matrix4f();					// return identity mtx

	Vector3f uZ = uX.crossProduct(uY);	// the vector that maps to iZ
	uX.normalize();
	uY.normalize();
	uZ.normalize();

	Matrix4f rv(uX[0], uX[1], uX[2], 0,	// a backward matrix,
				uY[0], uY[1], uY[2], 0,	 // which takes verticalized
				uZ[0], uZ[1], uZ[2], 0,	  // to unverticalized
				    0,	   0,     0, 1);
	rv.inverse();							// invert it forward
    return rv;
}
NSString *pp(Bounds3f bound) {
	Vector3f b0 = bound.point0();
	Vector3f b1 = bound.point1();

	id rv=@"";
	if (isNan(b0) or isNan(b1))
		rv = @"       nan        ";
	else
		for (int dim=0; dim<3; dim++)
//			rv=[rv stringByAppendingFormat:@"%.0f<%.0f%s", b0[dim], b1[dim], dim<2? ",":""];
			rv=[rv stringByAppendingFormat:@"%.1f<%.1f%s", b0[dim], b1[dim], dim<2? ",":""];
	return rv;
}


NSString *pp(Vector3f vect) {
	if (isNan(vect))
		return @"( nan )";
	if (vect.length()< EPSILON)
		return @"(  0  )";
	NSString *rv = @"(";
	for (int i=0; i<3; i++)
		rv = [rv stringByAppendingFormat:@"%.1f%s", vect[i], i!=2?",":")"];
	return rv;
}

NSString *pp3XZ(Vector3f vect) {
	if (isNan(vect))
		return @"( nan )";
	NSString *rv = [NSString stringWithFormat:@"(%.3f,,%.3f)", vect[0], vect[2]];
	return rv;
}

NSString *pp(Matrix3f m) {
	NSString *rv = @"I";

	  // Look for the case when all rows are dominated by one column:
	 // Definition: a row is DOMINATED when one col
	char  dominantAxis4col[3];
	float dominantValu4col[3];
	bool allDominated = true;
	for (int i=0; i<3; i++) {

		 // Check if column is dominated by one entry:
		dominantValu4col[i] = 0;					// non found yet
		dominantAxis4col[i] = -1;				// non found yet
		for (int j=0; j<3; j++)
			if (fabs( m.at(i,j) ) > EPSILON) {		// non-zero column found
				if (dominantAxis4col[i] == -1) {		// first non-zero
					dominantAxis4col[i] = j;
					dominantValu4col[i] = m.at(i,j);
				}
				else {								// 2'nd non-zero
					dominantAxis4col[i] = -1;
					dominantValu4col[i] = nan("");		// mark as error
					allDominated = false;
					break;
				}
			}
	}

	 // Are all the rows dominated by one column
	if (allDominated) {
	
		 // Are all dominated by same value?
		float domVal    =			fabs(dominantValu4col[0]);
		bool allValSame = (domVal - fabs(dominantValu4col[1])) < EPSILON;
		allValSame     &= (domVal - fabs(dominantValu4col[2])) < EPSILON;

		const char *axisNames = (char *)"xXyYzZ";
		if (allValSame) {
			id aNames = @"";
			for (int j=0; j<3; j++) {
				int k = 2*dominantAxis4col[j] + (dominantValu4col[j]<0);
				aNames = [aNames stringByAppendingFormat:@"%c", axisNames[k]];
			}
			const id shortINames = @{@"xyz":@"", @"Xyz":@"X", @"xYz":@"Y", @"xyZ":@"Z"};
			id srv = shortINames[aNames]?: aNames;
			rv = [rv stringByAppendingString:srv];
			if (fabs(domVal - 1.0) > EPSILON )
				rv = [rv stringByAppendingFormat:@"%.2f", fabs(dominantValu4col[0])];
		}

		 // Rows are all dominated, but by different values
		else
			for (int j=0; j<3; j++)
				rv = [rv stringByAppendingFormat:@"%c%.2f", axisNames[2*j], dominantValu4col[j]];
	}
	else {
		for (int j=0; j<3; j++)
			rv = [rv stringByAppendingFormat:@"[ %10.3f %10.3f, %10.3f]  \n",
						m.at(j,0), m.at(j,1), m.at(j,2)];

	}
	return rv;
}
NSString *pp(Matrix4f m) {
	Matrix3f rot = m.getRotation();
	id rotStr = pp(rot);
	Vector3f tran = m.getTranslation();
	// N.B: m.at(3,3) is not printed
	id tranStr = pp(tran);
	return [rotStr stringByAppendingString:tranStr];
}

NSString *pp2d(Matrix4f m) {
	NSString *rv = @"[ ";

	for (int i=0; i<3; i++)
		rv = [rv stringByAppendingFormat:@"[ %10.3f %10.3f, %10.3f]  \n",
					m.at(i,0), m.at(i,1), m.at(i,2)];
	return rv;
}

#ifdef VMATH_NAMESPACE
}
#endif
