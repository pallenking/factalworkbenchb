//  XCodes.h -- X markings on code lines  C140929

#ifndef FactalWorkbench_XCodes_h
#define FactalWorkbench_XCodes_h

/*
 * XCodes are small two-character comments which can be embedded in the source code.
 * They denote the kind of that source line, which is useful information to have in search returns
 *
 * Only 2 XCodes kinds are currently defined:
 */
#define XX  /* DECISION POINT -- Mark the place to watch, has no effect on the code */
#define XR	/* RECURSIVE CALL */
/// Problems:
#define XD	/* METHOD DEFINITION */
#define XI	/* METHOD IMPLEMENTATION */
//#define MA	/* METHOD ADVERTIZEMENT */
//#define MI	/* METHOD IMPLEMENTATION */

#endif
