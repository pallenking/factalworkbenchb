// common.mm -- things accumulated over the DECADES that are generically helpful

/* to do
 1. very crufty, weed and modularize
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <stdarg.h>
#include <sys/time.h>
#include <time.h>
#include "Common.h"

void *self=0;

//void exit();

//void *panicsSelf=0;		// HnwObject *

//#ifdef oldPanic
void panic_gutsC(const char *msg, ...) {

	fflush(stdout);					// purge any previous output
	printf("\\\\\n \\\\\n  \\\\");
	int nDashes = 40;
//    if (panicsSelf)  {      // add name of HnwObject that caused panic
//        const char *nam = (char *)"<Not a HnwObject>";
//		nam = "--";	// 140825 Splitter Workbench
//        printf(" '%s': ", nam);
//        nDashes -= strlen(nam) + 2;
//        if (nDashes<0)
//            nDashes = 0;
//    }
	for (int i=0; i<nDashes; i++)
		printf("%s", "-");
	printf(" %s ERROR/WARNING ----\n", fWallTime((char *)"%y%m%d/%H:%M:%S"));

	static char message[1000];
	va_list ap;
	va_start(ap, msg);
	vsprintf(message, msg, ap);
	va_end(ap);

	printf("   >>-- %s\n  //-----------------------------", message);
	printf("----------------------------------------\n //\n//\n");
}
//#endif
#pragma mark - Random Numbers
int randomProb (float p) {				// 1 with prob p
	float r = randomDist(0, 1);
	//printf("+++++++++ %f > arg %f --> %d\n", r, p, r<p);
	return r < p;
}

float randomDist (float a, float b) {	// float boxcar, a..b, incl
    if (a == b)
        return a;
	static const int big = 0x7fffffff;
//??	assert(b>=a, (@"illegal call, a>b"));
	long int val = random();
	float frac = ((float)(big&val))/((unsigned int)big + 1);
	float rv = a + (b-a) * frac;
	if (rv < b)						// we never want to return the upper value.
		return rv;
	else
		return randomDist(a,b);		// N.B: This has an infinite probabilistic tail
}

void setRandomSeed (long int seed) {
	static char seedState[256];			// currently, only one random generator supplied
    initstate((int)seed, seedState, 128);
}

#pragma mark - Run/Wall Time
double runTime () {								// time since first call here
	static double startingTime = -1;
	struct timezone timeZone =   {0, 0};
	struct timeval timePtr;

	gettimeofday(&timePtr, &timeZone);

	if (startingTime < 0)
		startingTime = timePtr.tv_sec + timePtr.tv_usec * 1.0e-6;
	return timePtr.tv_sec - startingTime + timePtr.tv_usec * 1.0e-6;
}

externC char *wallTime ()				  {	return fWallTime((char *)"%c");		}
externC char *fWallTime (char *fmt) {
	static char buf[100];
	time_t systemTime;
	time(&systemTime);
	strftime(buf, 100, fmt, localtime(&systemTime));
	return buf;
}

  // this routine repacks args, to remove NULL args.
 // in multi-"main(argv)" systems, understood args are nulled out.		PAK110729
externC void removeNullArgs(int &argc, char *argv[])   {
	int inInd=0, outInd=0;
	for (; inInd<argc; inInd++)
		if (argv[inInd])
			argv[outInd++] = argv[inInd];
    int returnArgc =outInd;
	while (outInd<argc)
		argv[outInd++] = argv[inInd];
    argc = returnArgc;
}

