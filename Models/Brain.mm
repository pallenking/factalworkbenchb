//  Brain.mm -- The brain of a Part tree, managing config, simulation C2012PAK C2015PAK

#import "Brain.h"
#import "SimNsVc.h"
#import "SimNsWc.h"
#import "SimNsV.h"
#import "PushButtonBidirNsV.h"
#import "View.h"

#import "Part.h"
#import "Path.h"
#import "Link.h"
#import "WorldModel.h"
#import "TimingChain.h"
#import "BundleTap.h"
//#import "GenAtom.h"
#import "Model01.h"
#import "FactalWorkbench.h"
#import "Common.h"
#import "NSCommon.h"
#import "Id+Info.h"

@implementation Brain
#pragma mark 0. Class Objects
Brain *currentBrain	= nil;	// the current brain being debugged (must enhance for multi-windows)
Brain *bootBrain	= nil;	// a brain to be used when currentBrain==nil

+ (void) setCurrentBrain:(Brain *)aBrain; {	currentBrain = aBrain;	}

 // some (e.g. Id+Info.h) don't know about Brain.  This is a hack:
+ (Brain *) currentBrain {
	if (currentBrain == nil) {		// regular simulation brain not instantiated
		if (bootBrain == nil) {			// make a temporary brain for booting
			bootBrain = anotherBasicPart([Brain class]);
			[bootBrain defaultSimParams]; // Simulator Defaults:
			[bootBrain setSimParams:userSimulationPreferences()]; // Transient Simulator Defaults:	(from FactalWorkbench.mm)
		}
		return bootBrain;
	}
	return currentBrain;
}

#pragma mark 1. Basic Object Level
- (id) init; {											self = [super init];

	self.logNumber			= 1;
	self.wireNumber			= 1;	/// ??? ///
//	self.commonPath			= [[[Path alloc] init] autorelease];
//	self.commonPath.name	= @"commonPath";
	self.timingChains		= @[].anotherMutable;	// array of HnwIoControllers
	return self;
}

- (void) dealloc {
	self.simNsWc			= nil;		// (weak)
	self.sourceLineInfo		= nil;
	self.camera				= nil;
//	self.commonPath			= nil;
	self.timingChains		= nil;

		  // Release all NSString's defined in simulatorControl
		 #define  aString(name_, value_) self.name_ = nil;	// release retained
		#define    aBool(name_, value_)						// no action required
       #define     aInt(name_, value_)						// "
      #define   aFloat(name_, value_)
     #define  aDouble(name_, value_)
    #define aRContxt(name_, value_)
   /***************************************/
  /***/   forAllBrainParameters   /***/		 // This 1 line expands to dozens of free's
 /***************************************/		// and hence generates much code
  #undef aString
   #undef aBool
    #undef aInt
     #undef aFloat
      #undef aDouble
	   #undef CTL
        #undef aRContxt

	[super dealloc];
}


#pragma mark 2. 1-level Access
//// //// //// //// //// //// //// //// //// //// //// //// //// //// //// ////
//// //// //// //// //// //// //// //// //// //// //// //// //// //// //// ////
//// //// //// //// //// //// //// //// //// //// //// //// //// //// //// ////

 /// A Brain has many properties
#define idTo_bool(value_)			(bool)[value_ intValue]
#define idTo_NSString(value_)		value_
#define idTo_char(value_)			(char)[value_ intValue]
#define idTo_double(value_)			(double)[value_ floatValue]
#define idTo_float(value_)			(float)[value_ floatValue]
#define idTo_int(value_)			(int)[value_ intValue]
#define idTo_renderModes(value_)	renderModeKindOfString(value_)

#define toId_bool(value_)			[NSNumber numberWithBool:	value_]
#define toId_NSString(value_)		value_
#define toId_char(value_)			[NSNumber numberWithChar:	value_]
#define toId_double(value_)			[NSNumber numberWithDouble:	value_]
#define toId_float(value_)			[NSNumber numberWithFloat:	value_]
#define toId_int(value_)			[NSNumber numberWithInt:	value_]
#define toId_renderModes(value_)	@0	// WTF

- (void) defaultSimParams; {

	 // Converters from NSString to various types. (This pushes cpp a port.)
	#define CTL(parmType, parmName, parmDefault) \
				self.parmName = idTo_ ## parmType(@#parmDefault);

	   ///// Set 	Default Values 		per forAllBrainParameters, where
	  ///
		 #define  aString(name_, value_) CTL(NSString,   name_, value_)
		#define    aBool(name_, value_) CTL(bool,       name_, value_)
       #define     aInt(name_, value_) CTL(int,        name_, value_)
      #define   aFloat(name_, value_) CTL(float,      name_, value_)
     #define  aDouble(name_, value_) CTL(double,     name_, value_)
    #define aRContxt(name_, value_) CTL(renderModes,name_, value_)	// buggy for render3DMode

  /**************************************/
 /***/	 forAllBrainParameters  /***/		 // This 1 line expands to dozens of Properties
/**************************************/		// and hence generates much code
	#undef CTL
}

 // Returns the id version of val for the parameter named key
- (id) getParameter:key; {
	
	   ///// Get 		Value 		for keys defined in forAllBrainParameters, where
	  ///
	 // If key matches parmName, return value:
	#define CTL(parmType, parmName, parmDefault)		\
		if ([key isEqualToString:@#parmName])			\
			return toId_ ## parmType(self.parmName);

	  /**************************************/
	 /***/	 forAllBrainParameters		/***/		 // This 1 line expands to dozens of Properties
	/**************************************/		// and hence generates much code

	return nil;
}

- (void) setParameter:key toValue:val; {
	
	   ///// Set 		Value 		of key, for keys defined in forAllBrainParameters, where
	  ///
	 // If key matches parmName, set its value:
	#undef CTL
	#define CTL(parmType, parmName, parmDefault)		\
		if ([key isEqualToString:@#parmName])			\
			self.parmName = idTo_ ## parmType(val);
	
	  /**************************************/
	 /***/	 forAllBrainParameters		/***/		 // This 1 line expands to dozens of Properties
	/**************************************/		// and hence generates much code
	
}

 // expands hash --> root's various simulation parameters
- (id) setSimParams:(NSDictionary *)params; {

	for (id key in params) {	  // for each key:

		[self setParameter:key toValue:params[key]];
		
	}

	// Useful Debug Code:
	//    aFloat (boundsDrapes,           0.10)/* size of bounds box corners (.08)*/
	//	    aRContxt(render3DMode,render3Dcartoon)
	//		CTL(renderModes,render3DMode,render3Dcartoon)
	//		hashVal = givenHash[@"render3DMode"];
	//		self.render3DMode = idTo_renderModes(hashVal? hashVal: render3Dcartoon);
	//	if (/* DISABLES CODE */ (0)) {
	//		NSPrintf(@"V^V^V^V^V^V^V^V  givenHash[@\"render3DMode\"] = %@ ", givenHash[@"render3DMode"]);
	//		NSPrintf(@" self.render3DMode = %d = %@\n", self.render3DMode,
	//									renderModeKindArray[self.render3DMode]);
	//	}
    #undef aString
     #undef aBool
      #undef aInt
       #undef aFloat
        #undef aDouble
	     #undef CTL
          #undef aRContxt

	 // Initialize RANDOM NUMBER generator
	long int randomSeed = self.randomSeed;
	id m = randomSeed? @"from config": @"randomly (from clock)";
	if (randomSeed == 0)				// no seed spec'd
		randomSeed = time(0);				// use wall clock
	setRandomSeed(randomSeed);
	[self logEvent:@"random seed = %ld set %@; random0=%08lX", randomSeed, m, random()];

	return self;
}

//// //// //// //// //// //// //// //// //// //// //// //// //// //// //// ////
//// //// //// //// //// //// //// //// //// //// //// //// //// //// //// ////
//// //// //// //// //// //// //// //// //// //// //// //// //// //// //// ////

#pragma mark 3. Deep Access
- (Brain *) brain; {		return self;		}	// we are our own brain

- (id) deepMutableCopy; {					Net *rv = [super deepMutableCopy];

//	return (id)panic(@"not expected");
	return nil;
}

 // AppController invokes this:
#pragma mark 4. Factory								// Compound objects
Brain *aBrain(NSArray *parts) {		return aBrain(nil, parts); }
Brain *aBrain(NSDictionary *etc, NSArray *parts) {
	id info = @{@"parts":parts, etcEtc};

	Brain *newbie = anotherBasicPart([Brain class]);
XX	newbie = [newbie build:info];
	return newbie;
}

Brain *aBrain_selectedBy(int menuNo, int regressNo, id soughtName) {
	id sourceLineInfo = @{}.anotherMutable;

	  // 1. Get Unwired brain from Model01.mm, by menu, regress, or X
	 //
XX	if (Brain *brain = accessTestLibrary(menuNo, regressNo, soughtName, true, sourceLineInfo)) { // for aBrain
		brain.sourceLineInfo = sourceLineInfo;
		currentBrain = brain;					// expedience

		  // 3. Groom Part (Wire up, DAG order, lldb names, ...)
		 //
		[brain logEvent:@"\n""Groom this brain generated by TestLibrary()"];
	XX	[brain groomModel];

	XX	[brain reset];

		int portCount = brain.portCount;
		brain.sourceLineInfo[@"windowName"] =	// append port count
				[@"" addF:@"%@ (%d Ports)", brain.sourceLineInfo[@"windowName"], portCount];
		return brain;
	}
	return nil;								// no brain found
}

// SwiftFactals: func buildTest(named name:String?) -> PartClosure?
id accessTestLibrary(int want_menuNo, int want_regressNo, id want_name, bool want_itBuild, id want_metaInfo) {

	 // Set GLOBALS to guide generation in testLibrary()
	uWant_menuNo		= want_menuNo;
	uWant_regressNo		= want_regressNo;
	uWant_name			= want_name;
	uWant_itBuilt		= want_itBuild;

	uAt_menuNo			= 1;				// use cardinal numers, as we scan tests
	uAt_regressNo		= 1;
	uAt_allNo			= 1;

	uBuilt_brain		= 0;				// no part built yet
	uBuilt_metaInfo		= want_metaInfo;	// pass in some metaInfo

	return testLibrary();		// select with the uWant_... args
}

/// The brain in the Test Library selected (via 'x' or menue/regress number)
/// is ordered built by the [x][r R m M] macros, and comes out here:

- (id) build:(id)info; {					[super build:info];

//!	xzzy1 Brain::		1. camera		:<hash>
//!	xzzy1 Brain::		2. hundreds of keys, in Brain.h

	assert(self.flipped==0, (@"Brains must be installed right-side up, and may not be flipped."));

	  // Information destined for Window Conbtroller gets extracted and held here
	 // till brain is installed in simNsWc
	self.camera			= [self takeParameter:@"camera"];

	 // set simulation parameters in the construction hash of the brain itself
	[self setSimParams:self.parameters];

	return self;
}

- (id) groomModel; {

	 ///// 1. Debug names for LLDB (first to make debugging easier)
	[self setAllFullNamesLldb];

	[@"\n""Unwired Part:" ppLog];
	[[self ppPart:@{@"deapth":@-1, @"ppPorts":@1, @"ppParameters":@1}]  ppLog];

	   /// 2. WIRES, defined as parameters, get added to the model here.
	  ///	Done in two phases, since it may change the net we're walking.

	 /// 2a. Gather Wires as wirelist
	[self logEvent:@"GATHER WIRES from part '%@':", self.name];
	id wirelist = @[].anotherMutable;

XX	[self gatherWiresInto:wirelist];	// 1. get wire list outside the tree

	 /// 2b. apply wirelist to Net
	[self logEvent:@"\n""ADD WIRES to part:"];
XX	wireViaList(wirelist);					// 2. apply list


	 /// 3. Finish Building (e.g. of Generators)
	[self forAllParts:^(Part *p) {
XX		[p postBuild];
	}];

	 /// 4. Order Nets by connections (wires)
	[self orderPartsByConnections];			// order dags by wires (for Actors)
	
	 /// 5. SPLITTERS: Fix values inside
	[self setDistributions];

	 /// 6. Do again, since things may have changed
	[self setAllFullNamesLldb];

	return self;
}

#pragma mark 7-. Simulation Actions

 // Test if simulator is settled:
- (bool) settled; {

	 // Scan all Ports, finding ones which haven't settled
	int ports  = self.unsettledPorts;

	 // Chits taken out by users, not yet returned.
	bool links = self.unsettledOwned != 0;

	 // Number of extra cycles required
	bool extra = self.extraCycles > 0;
	
	return !ports and !links and !extra;
}


- (void) reset; {
	[self logEvent:@"\n""Reset:"];
	[super reset];
	
	self.timeNow			= 0;
}

#pragma mark 7. Simulator Messages
- (bool) processKey:(char)character modifier:(unsigned long)modifier; {
	Brain *brain = self.brain;
	id rv = @"";
	
	if (!(FWKeyUpModifier & modifier)) {		///// Key DOWN ///////

		 // Sim EVENTS
		if (character == ' ') {					//// " " ////
			assert(self.simEnable!=-1, (@"171206 restart-before-stop shouldn't be going on now"));
			self.simEnable = self.simEnable<1;		// ..,-1,0 --> 1; 1,2..-->0
			NSPrintf(@"++++++++++ simEnable=%d extraCycles=%d\n",	self.simEnable, self.extraCycles);
			 // (Not using ppLog, as one wants log numbers to be independent of simEnable ON/OFF
		}

		else if (character == 'b') {			//// b ////
			printf("\n\n Debugger Hints:\n"
				   "\tpi <expr>  -- print <expr> as Info\n"
				   "\tpp <expr>  -- print <expr> as Part\n"
				   "\tpb <expr>  -- print <expr> as Part with Ports\n"
				   "\tpl <expr>  -- print <expr> as Part with Ports and Links separate\n"
				   "\tp1 <expr>  -- print <expr> as Part, 1 level deep\n"
				   "\tp2 <expr>  -- print <expr> as Part, 2 levels deep\n"
				   "\t<expr> may be s:self, m:whole_model, v:whole_view\n"
				   "\te.g. ppm, pbm, ppv, pbs, pb foo\n"
				);
			panic(@"keyboard break");					// break to debugger
		}

		////////////////////////////////
		//////// Part / View //////////
		////////////////////////////////
		 // print out partss, views
		else if (character == 'm') {			//// m ////
			printf("\n- Current Part:\n");
			pp(self, 0);					// print Atoms
		}
		else if (character == 'M') {			//// M ////
			printf("\n- Current Part and Ports:\n");
			pp(self, 1);					// print Atoms and Ports
		}
		else if (character == 'L') {			//// L ////
			printf("\n- Current Part, Ports, Links and Labels:\n");
			pp(self, 2);					// print Atoms, Ports, Links and Labels
		}
		else if (character == 'R') {			//// R ////
			printf("\n- Current Brain parameters\n");
			pp(self, -1);					// print brain parameters
		}
		else if (character == 'u') 				//// u ////
			NSPrintf(@"\n- simEna:%d %@\n", self.simEnable, self.ppSimState);

		  ///// Key DOWN ///////
		 //// KeyboardDat: Check bundleTaps for interest in "key DOWN" event
		else {
			for (TimingChain *timingChain in self.timingChains) {
				mustBe(TimingChain, timingChain);
XX				if ([timingChain processKey:character modifier:modifier])
					rv=[rv addF:@"    Brain recognizes Key '%c' DOWN: simEnable=%d", character, self.simEnable];
			}
		}

		if (character == '?') {			//// ? ////
			printf ("   === Brain      commands:\n"
					"\t' '		-- HALT/RUN simulator						\n"
					"\t'm'		-- print Model								\n"
					"\t'M'		-- print Model with Ports					\n"
					"\t'L'		-- print Model with Ports, Links and Labels	\n"
					"\t'R'		-- print brain Simulator Parameters			\n"
					"\t'u'		-- print reasons the simulator is unsettled \n"
					"\t'b'		-- BREAK to debugger in SimNsWc				\n"
					);
			return false;
		}
		return [rv length]!=0;
	}

	  ///// Key UP ///////
	 //// KeyboardDat: Check bundleTaps for interest in "key UP" event
	else for (TimingChain *timingChain in self.timingChains)
		if ([mustBe(TimingChain, timingChain) processKey:character modifier:modifier])
			rv=[rv addF:@"    Brain recognizes Key '%c' UP: simEnable=%d", character, self.simEnable];

	return [rv length]!=0;
}

 ///////////////////////// GENERATORS /////////////////////////

- (void) kickstartSimulator; {
	self.extraCycles = 1;		//2
}

#pragma mark 8. Reenactment Simulator
///*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//
//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//
////*//*//*//*//*//*//*//*//    Reenactment Simulator   //*//*//*//*//*//*//*//
///*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//
//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//

- (void) cycleSimulator; {

	 /// Run Timing Chain's State Machine, DEQUE any pending World Model Events
	for (TimingChain *tc in self.timingChains) {
		if (tc.state == 0) {
			if (id event = tc.worldModel.dequeEvent) {
			  // A timingChain dequeues an event:
				[tc takeEvent:event];
//				tc.asyncData = false;		// set to process as Synchronous //strange //xyzzy11c
				tc.userDownPause = 0;		// go right thru, don't pause

				[tc releaseEvent];
				[self kickstartSimulator];
			}
		}
		[tc cycleStateMachine];
	}
	  /// RUN Simulator ONE Cycle: up the entire Network, then Down: ///////
	if (self.simEnable > 0) {				// if enabled
		@autoreleasepool {						// release autoreleased objects
			self.globalDagDirDown	= false;		// ---- run   UP   the hierarchy
			self.timeNow			+= 0.01;
XX			[self simulateDown:false];
			
			self.globalDagDirDown	= true;			// ---- run  DOWN  the hierarchy
			self.timeNow			+= 0.01;
XX			[self simulateDown:true];
		}
		self.extraCycles	-= self.extraCycles!=0;  // count down to zero
	}
}

#pragma mark  9. 3D Support							// 3D Visualization

 /// To include someday:
//- (int) epochBuild:(id)elt_; {
//	if (NSNumber *num = coerceTo(NSNumber, elt_)) {
//		float epochMinAnte = [num floatValue];
//		assert(!isNan(epochMinAnte), (@""));
//		float epochBeingViewed = brain.buildingEpoch;
//		float d = epochBeingViewed - epochMinAnte;
//		return d >= 0? 1: -1;
//	}
//}


#pragma mark 12. Inspectors

- (void) setBuildingEpoch:(int)val; {
	int v = _buildingEpoch;
	_buildingEpoch = val;

	if (v != val) {
		 // redo the whole view, at this new level
		self.someViewDirty = true;
		self.boundsDirty = true;

		[self.simNsWc.simNsVc buildRootFwVforBrain:self];
		[self.simNsWc buildAutoOpenInspectors];
	}
}

// The GUI interfaces should use these:
//      ==3==. interfaces to GUI (.nibs must match)
//	    #define aString(name_, value_) xxx
//	   #define aBool(name_,   value_)
//	  #define aInt(name_,    value_)
//	 #define aFloat(name_,  value_)
//	#define aDouble(name_, value_)
// /***************************************/
// /***/   forAllBrainParameters  /***/		// (This 1 line generates much code)
// /*************************************/
//	#undef aString
//	 #undef aBOOL
//	  #undef aInt
//	   #undef aFloat
//	    #undef aDouble
//       #undef aRContxt


#pragma mark 15. PrettyPrint

- (NSString *) pp1line:aux; {					id rv = [super pp1line:aux];

	rv=[rv addF:@"simEnable=%d, %@", self.simEnable, self.ppSimState];
	return rv;
}

- (NSString *) ppSimState; {
	id s = @"SETTLED";
	if (!self.settled) {
		s = @"UNSETTLED(";
		if (self.unsettledPorts)
			s=[s addF:@"ports:%d ", self.unsettledPorts];
		if (self.unsettledOwned)
			s=[s addF:@"owned:%d ", self.unsettledOwned];
		if (self.extraCycles)
			s=[s addF:@"extra:%d ", self.extraCycles];
		s=[s addF:@")"];
	}
//	id s = self.settled? @"SETTLED":
//			 [@"" addF:@"UNSETTLED (unsettledPorts:%d Owned:%d Cycles:%d)",
//				self.unsettledPorts, self.unsettledOwned, self.extraCycles];
	return s;
}

- (id) ppUnsettledString; {
	id rv = @"";
	if (self.unsettledOwned)
		rv=[rv addF:@"  UnsettledOwned: %d conditons of 'hold tokens'\n", self.unsettledOwned];
	if (self.extraCycles)
		rv=[rv addF:@"  UnsettledCycles (Do at least %d cycles before stoping)\n", self.extraCycles];
	if (id ports = [super ppUnsettledString])
		if (int len = (int)[ports length])
			rv=[rv addF:@"  UnsettledPorts: with changing values:\n%@", ports];
	return rv;
}

- (NSString *) ppSimulationParameters; {
	id rv = [@"" addF:@"(Brain *)'%@'\n", self.fullName];

////     ==3==. define macros to print values
#define  aString(name_, value_) rv=[rv addF:@"%-20s = '%@'\n", #name_, self.name_];
 #define    aBool(name_, value_) rv=[rv addF:@"%-20s = %d\n",   #name_, self.name_];
  #define     aInt(name_, value_) rv=[rv addF:@"%-20s = %d\n",   #name_, self.name_];
   #define   aFloat(name_, value_) rv=[rv addF:@"%-20s = %.2f\n", #name_, self.name_];
    #define  aDouble(name_, value_) rv=[rv addF:@"%-20s = %.2f\n", #name_, self.name_];
     #define aRContxt(name_, value_) rv=[rv addF:@"%-20s = %@\n",   #name_, renderModeKindArray[self.name_]];
      /**************************************/
       /***/  forAllBrainParameters		  /***/		// (This 1 line generates much code)
        /**************************************/
         #undef aString
		  #undef aBOOL
		   #undef aInt
			#undef aFloat
			 #undef aDouble
			  #undef aRContxt

// 151030 N.B: lldb> p brain.ppSimulationParameters stops at about 1000 characters!!

	return rv;
}
@end
