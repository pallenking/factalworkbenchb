//  Tunnel.h -- A Bundle which forms entrance/exit of a tunnel C150529PAK

/*!
	GEOMETRY:
		Bundles have strictly inclusive locations, with inner ones smaller in X
		and Z than the outter ones (like all Parts, like Russian dolls). This
		is as far as bounding and placing goes. 
		
		In the Y dimension, larger bundles draw below smaller, the largest being 
		extra long longest. Thus inner bundles stick out farther than outter 
		ones,  A tree system (I am at level 1 of 2) is used to compute a height below.
 */
#import "Bundle.h"

@interface Tunnel : Bundle

@end
