// Info.mm -- a wrapper for Dictionaries, arrays, or strings C2014 PAK
#ifdef viewOnly
 // using class as a namespace -- no Info

#import "Id+Info.h"
#import "SystemTypes.h"
#import "Common.h"
#import "NSCommon.h"
#import "Object_.h"
#import "Simulator.h"


#pragma mark id + info
@implementation NSObject /*extended here to*/ (info4id)

- (NSString *)		fwStr;		{	return coerceTo(NSString,		self);	}
- (NSArray *)		fwArray;	{	return coerceTo(NSArray,		self);	}
- (NSDictionary *)	fwHash;		{	return coerceTo(NSDictionary,	self);	}
- (NSNumber *)		fwNumber;	{	return coerceTo(NSNumber,		self);	}

	typedef enum {
		transformationLogOFF = 0,			// 0 OFF
		transformationLogCONSEQUENTIAL,		// 1 only consequential decisions
		transformationLogDECISIONS,			// 2 all decisions
		transformationLogEVERYTHING,		// 3 all possible printout
	  } transformationLogLevels;


BOOL isValid(NSInteger location) {	return location != NSNotFound;	}

- (BOOL) sayHi {	printf("hi\n");		return 1;	}

- (BOOL) nonNil {
	if (NSDictionary *infoDict = coerceTo(NSDictionary, self))
		return infoDict.count;
	if (NSArray *infoArray = coerceTo(NSArray, self))
		return infoArray.count;
	if (NSString *infoStr = coerceTo(NSString, self))
		return infoStr.length;
	return 0;
}
- (BOOL) isMutable; {
	if (coerceTo(NSMutableDictionary, self))
		return TRUE;
	if (coerceTo(NSMutableArray, self))
		return TRUE;
	if (coerceTo(NSString, self))
		return TRUE;
	return FALSE;
}

#pragma mark old stuff

//- replaceInAllDictVals:prevVal with:newVal; {
//	id target = self;
//	id targetCow = [target forAllSubDictionaries:
//		^(id info) {									/*BLOCK:*/
//			 // Substitute: value <A><prevVal><B> becomes value <A><newVal><B>
//			NSDictionary *infoDict = coerceTo(NSDictionary, info);
//			NSMutableDictionary *infoDictCow=0;
//
//			id updates = [NSMutableDictionary dictionary];
//			for (id key in infoDict) {
//				id val = infoDict[key];
//				if (NSString *valStr = coerceTo(NSString, val)) {
//					NSRange r = [valStr rangeOfString:prevVal];
//					if (r.location != NSNotFound) {
//						id s = [valStr stringByReplacingCharactersInRange:r withString:newVal];
//						updates[key] = s;	// defer set to not mutate *info
//					  }
//				  }
//			  }
//			if ([updates count]) {
//				if (!infoDictCow)	// copy on write below
//					infoDictCow = [NSMutableDictionary dictionaryWithDictionary:infoDict];
//				for (id k2 in updates)
//					infoDictCow[k2] = updates[k2];
//			  }
//	  		return (id )infoDictCow;
//		  }										];		/*BLOCK*/
//	return targetCow? targetCow: target;
//  }

//- forAllSubDictionaries:(id (^)(id ))block; {
//	id info = self;
//	if (NSDictionary *infoDict = coerceTo(NSDictionary, info)) {
//		NSMutableDictionary *cowDict=0;	// Hungarian: CopyOnWrite
//
//		  // the top level dictionary may have the pattern:
//         //
//		if (id rv = block(info))     // call user's block to handle infoDict
//			cowDict = (id)rv;
//		NSDictionary *curDict = cowDict? cowDict: infoDict;
//
//		 // paw through the subKeys to see if any values need processing
//		for (id subInfo in curDict) {		// each subKey
//			id subValue = curDict[subInfo];
//			if (id rv = [subValue forAllSubDictionaries:block])  {
//				 // process COWrite:
//				if (!cowDict)					// create a writeable copy
//					cowDict = (id)[NSMutableDictionary dictionaryWithDictionary:infoDict];
//				cowDict[subInfo] = rv;			// change it
//			  }
//		  }
//		return (id )cowDict;
//	  }
//	else if (NSArray *infoArray = coerceTo(NSArray, info)) {
//		NSMutableArray *cowArray=0;		// no copy yet
//		int i=-1; for (id subInfo in infoArray) {i++;
//
//					//### recursive call
//			if (id cowDict = [subInfo forAllSubDictionaries:block])  {
//				 // process return of changed copied data:
//				if (!cowArray)					// create a writeable copy
//					cowArray = (id)[NSMutableArray arrayWithArray:infoArray];
//				[cowArray replaceObjectAtIndex:i withObject:cowDict];
//			  }
//		  }
//		return (id )cowArray;
//	  }
//	else if (NSString *infoStr = coerceTo(NSString, info))
//		return (id )nil;		// strings are of no concern
//	else
//		panic("info type undefined");
//
//	return (id )nil;
//}

#pragma mark Pretty Print
- (NSString *)ppStr		{		return [self ppStr:1:99:0:0];				}
- (NSString *)ppStrCr	{		return [self ppStr:1:99:1:0];				}

- (NSString *)ppStr:indent {	return [self ppStr:1:99:1:indent];			}

- (NSString *)ppStrComma {	return [self ppStr:0:99:0:0];					}
void pInfo(id info)		 {	NSPrintf(@"		# %@\n", [info ppStr:1:99:0:0]);}
void pInfo(id info, BOOL brackets, int levels, int layers2Cr) {
	NSPrintf(@"		# %@\n", [info ppStr:brackets:levels:layers2Cr:0]);
}

- (NSString *)ppStr:(BOOL)brackets :(int)layers {
	return [self ppStr:brackets :layers :0:0];
  }

- (NSString *)ppStr:(BOOL)brackets :(int)layers :(int)layers2Cr :(NSString *)indent; {
	id info = self;
	NSString *rv=@"";
	int funnyMode=0;
	if (!indent)
		indent = @"";

	if (info) {
		if (NSDictionary *infoDict = coerceTo(NSDictionary, info)) {
			if (layers >= 1) {
				NSString *comma = @"";
				if (!info[@"-foreignHash-"]) {			// ignore foreign hashes
					if (brackets)
						rv=[rv stringByAppendingFormat:@"{"];
					id sortedKeys = [infoDict.allKeys sortedArrayUsingComparator:
						^NSComparisonResult(id obj1, id obj2)  {		/* BLOCK */
							NSString *v1 = coerceTo(NSString, obj1);
							int val1 = v1.strValence;
							NSString *v2 = coerceTo(NSString, obj2);
							int val2 = v2.strValence;

							if (val1 < val2)
								return NSOrderedDescending;
							if (val1 > val2)
								return NSOrderedAscending;

							if (obj1 < obj2)
								return NSOrderedDescending;
							if (obj1 > obj2)
								return NSOrderedAscending;

							return NSOrderedSame;
						  } ];											/* BLOCK */
					for (NSString *key in sortedKeys) {
						id val = info[key];
						id keyStr = key.ppStr;			// #### RECURSION:
						id valStr = [val ppStr];	// #### RECURSION:

						if (![keyStr hasPrefix:@"\"--"]) {
							if (layers2Cr>=0)
								rv=[rv stringByAppendingFormat:@" %@", comma];
							else
								rv=[rv stringByAppendingFormat:@"\n%@", indent];
							rv=[rv stringByAppendingFormat:@"%@:%@", keyStr, valStr];
							comma = @",";
						  }
						else {	// key "--..." dos
							rv=[rv stringByAppendingFormat:@"%@:@%d",key,(int)[val count]];
						  }
					  }
					if (brackets)
						rv=[rv stringByAppendingFormat:@"}"];
				  }
				else {			// -foreignHash- doesn't show contents
					rv= [rv stringByAppendingFormat:@"{%d}", (int)infoDict.count];
				  }
			  }
			else
				rv= [rv stringByAppendingFormat:@"{..%d..}", (int)infoDict.count];
		  }
		else if (NSArray *infoArray = coerceTo(NSArray, info)) {
			if (layers >= 1) {
				if (brackets)
					rv=[rv stringByAppendingString:@"["];

				NSString *comma = @"";
				for (id  subInfo in infoArray) {
						// #### RECURSION:
					rv= [rv stringByAppendingFormat:@"%@ %@",comma,[subInfo ppStr:TRUE:layers-1]];
					comma = @",";
				  }
				if (brackets)
					rv= [rv stringByAppendingString:@"]"];
			  }
			else
				rv= [rv stringByAppendingFormat:@"[..%d..]", (int)infoArray.count];
		  }
		else if (coerceTo(NSString, info))
			rv= [rv stringByAppendingFormat:@"\"%@\"", info];

		else if (coerceTo(NSNumber, info))
			rv= [rv stringByAppendingFormat:@"%@", info];

		else if (coerceTo(Simulator, info))
			rv= [rv stringByAppendingFormat:@"Simulator, "];
	  }
	else
		rv = @"<nil>";
	return rv;
  }

- (void) logOperationStart:opStr type:type indent:indent target:target; {
	NSPrintf(@"\n%@=== (%@) %@\n", indent, type, opStr);
	NSPrintf(  @"%@  = pattern: %@\n", indent, self.ppStr);
	NSPrintf(  @"%@  = target:  %@\n", indent, [target ppStr]);
}
- (BOOL) logOperationEnd:indent msg:msg state:tranState; {
	if (!msg) {
		NSPrintf(@"%@ == state:   %@\n", indent, [tranState ppStr:[indent indentMore]]);
		return TRUE;
	  }
	else {
		NSPrintf(@"%@ == state:   FAIL: %@\n", indent, msg);
		return FALSE;
	  }
}

#pragma mark Productions
- doNextTransformation; {
	assert(self /*and sim*/, ("required"));
	typedef enum {
		transformationLogOFF = 0,			// 0 OFF
		transformationLogCONSEQUENTIAL,		// 1 only consequential decisions
		transformationLogDECISIONS,			// 2 all decisions
		transformationLogEVERYTHING,		// 3 all possible printout
	  } transformationLogLevels;
//	NSPrintf(@"logLevel = %d\n", logLevel);

//!	if (logLevel >= transformationLogDECISIONS)
//!		NSPrintf(@"\n#####%*s+ %@\n", self.indent*3, "", self.strSummary);
//!	assert([self.name isEqualToString:sim.breakAtBuilder]==0, ("Builder Breakpoint encountered"));

	id grammar = ((id)self)[@"--grammar-"];
	id sim     = ((id)self)[@"-sim-"];

	 // production is of form predicate:product
	for (id predicateRO in grammar) {
		id predicate = [predicateRO mutableCopy];	// (matching mutates it)
		id production = grammar[predicate];
		id infoPre = [self ppStr];

		static int transformationNumber=0;
		transformationNumber++;
//!		BOOL atTransformationBreak = ++transformationNumber==sim.breakAtTransformationNumber;
//!		BOOL printBefore = atTransformationBreak or logLevel>=transformationLogDECISIONS;
//!		if (printBefore)
//!		assert (!atTransformationBreak, ("breakAtTransformationNumber = %d", transformationNumber));

		NSPrintf(@"\n##Predicate    ,,,,,,%@ ,,,,,,\n", [predicate ppStr:0:99]);

		id tranState = @{}.mutableCopy;		// requirement state of the production

		 //////////////// attempt MATCH
		if ([predicate attemptMatchTo:self withState:tranState indent:@""]) {

//			if (logLevel>=transformationLogCONSEQUENTIAL and !printBefore)
//				NSPrintf(@"Transformation %-3d: %@\n", transformationNumber, [predicate ppStr]);
//			if (logLevel >= transformationLogDECISIONS)
			NSPrintf(@"FIRE with transState: %@\n",	[tranState ppStrCr]);
			NSPrintf(@"##Production   ,,,,,,%@ ,,,,,,\n", [production ppStrComma]);

			 ////////////// SUBSTITUTE in tranState!
			[production fireWithTerms:tranState onto:self indent:@""];
			id product = self;
//			id product = [production fireWithTerms:tranState onto:self];
			assert (![product isEqualTo:@"ERROR"], ("predicate error"));
			assert(coerceTo(NSMutableDictionary, product), (""));

//			if (logLevel >= transformationLogDECISIONS)
				NSPrintf(@"--------\t%@ +(tranState=%@) ===> %@ \n",
						[predicate ppStr], [tranState ppStr], [product ppStr]);

		panic("");
//			if (NSArray *removeList = coerceTo(NSArray, tranState[@"-consumedKeys-"])) {
//				for (id elt in removeList) {
//					assert(self[elt], ("element %s to remove not present", [elt UTF8String]));
//					[self.info removeObjectForKey:elt];
//				  }
//				[tranState removeObjectForKey:@"-consumedKeys-"];
//			  }
//
//			if (logLevel >= transformationLogCONSEQUENTIAL)
//				NSPrintf(@"### ,%@, ===>  ,%@,\n", infoPre, [self.info ppStr]);
//
//			 // Retract terms of predicate, add terms of product
//			assert([self.info nonNil], (""));
			return [self doNextTransformation];
//			return [self doTransformations:grammar log:logLevel];
		  }
	  }
	return nil;
  }
- (int) strValence {
	assert(coerceTo(NSString, self), ("presumed"));

	if ([(id)self isEqualToString:@"-"])
		return -2;
	if ([(id)self hasPrefix:@"-"])
		return -2;
	if ([(id)self hasPrefix:@"for<"] and [(id)self hasSuffix:@">in"])
		return 7;
	NSDictionary *valences = @{@"new":@5,
							   @"new":@5,
							   @"Factal":@3,
							   @"hasBundle":@-1};
	NSNumber *valNum = coerceTo(NSNumber, valences[self]);
	if (int valence = (int)valNum.integerValue)
		return valence;

	return 0;
}

- (id) decodeNextGltArg; {
	id rvDecode = @{}.mutableCopy;
	if (NSString *selfStr = coerceTo(NSString, self)) {
		NSInteger p1 = [selfStr rangeOfString:@"<"].location;
		NSInteger p2 = [selfStr rangeOfString:@">"].location;

		if (!isValid(p1) or !isValid(p2)) 			//// e.g. pre<var>post
			return rvDecode;		// no substitutions
		else {
			assert(p2>p1, ("invalid use of '<' and '>' in string syntax"));
			rvDecode[@"valid<>"] = @1;
			if (p1 > 0)
				rvDecode[@"extraChars"] = @1;

			rvDecode[@"pre"]  = [selfStr substringToIndex:p1];
			rvDecode[@"post"] = [selfStr substringFromIndex:p2+1];

			NSString *var  = [selfStr substringWithRange:(NSRange){p1+1, p2-p1-1}];
			NSInteger p3 = [var rangeOfString:@" "].location;
			if (isValid(p3)) {			// var = <type name>
				id type = [var substringToIndex:p3];
				id name = [var substringFromIndex:p3+1];
				[rvDecode pushUniqueKey:@"name" val:name];
				[rvDecode pushUniqueKey:@"type" val:type];
			  }
			else
				[rvDecode pushUniqueKey:@"name" val:var];

			return rvDecode;
		  }
	  }
	else
		panic("");
	return nil;
  }

- objcCallInContext:context to:target; {
	int logLevel = 10;//sim.logTransformationLevel;
	NSDictionary *contextHash = coerceTo(NSDictionary, context);assert(contextHash, (""));
	NSArray *argNameArray = coerceTo(NSArray, context[@"-args-"]);
	assert(argNameArray, (""));
	id eltName	= argNameArray[0];
	id subsName = argNameArray[1];
	id tarName	= argNameArray[2];
	// Hungarian: SUBstitutionS TARget

	if (NSString *selfString = coerceTo(NSString, self)) {

		  //   VARIABLE SUBSTITUTION
		 //
		if ([selfString isEqualToString:@"-var<>applyTo<>-"]) {
				panic("-var<>applyTo<>-");
		}
		
		  //   ITERATION
		 //
		else if ([selfString isEqualToString:@"-for<>in<>applyTo<>-"]) {
			id subs = context[subsName];
			id tar	= context[tarName];//--- now have information from the caller

			 // Iterate over HASH
			if (NSDictionary *subsDict = coerceTo(NSDictionary, subs)) {

				 // Iterate over keys in a Hash, patterns to match
				[subsDict fireWithTerms:context onto:target indent:@""];////// STRING //////
				id product = target;


//				for (id subsKey in subsDict) {		// do all substitutions on target
//					id subsVal = subsDict[subsKey];
//					 // replace key with val
//					NSPrintf(@"replace key '%@' with val '%@' within'%@'\n",
//							[subsKey ppStr], [subsVal ppStr], [tar ppStr]);
//
//					//[tar applyConstraints:tranState to:hash];
//					
//
//					 // target is HASH: do for each key
//					if (NSDictionary *tarHash = coerceTo(NSDictionary, tar)) {
//						id rvTarHash = tarHash.mutableCopy;
//						for (id tarKey in tarHash) {
//							id tarVal = tarHash[tarKey];
//
//							
//							int i=3;
//						  }
//					  }
//					else
//						panic("");
//
//					[self doTransformations:nil log:9];
//				  }
			  }

//			 // target is HASH: do for each key
//			if (NSDictionary *tarHash = coerceTo(NSDictionary, tar)) {
//				for (id tarKey in tarHash) {
//					id tarVal = tarHash[tarKey];
//
//					 // Iterate for key/element of a Hash,
//					if (NSDictionary *subsDict = coerceTo(NSDictionary, subs)) {
//						for (id subsKey in subsDict) {		// do all substitutions on target
//							id subsVal = subsDict[subsKey];
//							 // replace key with val
//							NSPrintf(@"replace key '%@' with val '%@' within'%@'\n",
//									[subsKey ppStr], [subsVal ppStr], [tar ppStr]);
//
//							[self doTransformations:nil log:9];
//						  }
//					  }
//					else if (NSArray *subsDict = coerceTo(NSArray, subs)) {
//						for (id key in subsDict) {
//							[self doTransformations:nil log:9];
//						  }
//					  }
//					else
//						panic("");
//
//				  }
//			  }
			else
				panic("");

//			assert(coerceTo(NSArray, value1), (""));
			panic("-for<>in<>-");
		}
		
		  //   -BUILD-
		 //
		else if ([selfString isEqualToString:@"-build-" ]) {
			if (logLevel >= transformationLogCONSEQUENTIAL)
				NSPrintf(@"--------\t%@:%@ with self.info=%@\n",
						 eltName, [subsName ppStr], [self ppStr]);
			// build a Plug with the name prodVal
			if (NSString *prodValString = coerceTo(NSString, subsName)) {
				
				panic("");
	//*build*/	self.newbie = [self buildObjecOfClass:prodVal usingInfo :self.info];
				
				if (logLevel >= transformationLogCONSEQUENTIAL) {
					NSPrintf(@"built ------=> ");
	//?				[self.newbie printObject:-5];
				}
			}
			// build a bundle with
			else if (NSArray *prodValArray = coerceTo(NSArray, subsName)) {
				
				// build the enclosing bundle
	//*build*/	self.newbie = [self buildObjecOfClass:@"Bundle" usingInfo :self.info];
	//?			id newbieName = self.newbie.name? self.newbie.name: @"-";
	//?			NSPrintf(@"\t\t\t self.newbie %@:%@\n", newbieName, self.newbie);
				
				for (id structSub in prodValArray) {
	//?				id c = [self buildObjecOfClass:@"Bundle" usingInfo :self.info];
					int aa=33;
				}
			}
			// build out self with hash
			else if (NSDictionary *prodValDictionary = coerceTo(NSDictionary, subsName)) {
				panic("");
			}
		}
		else  // Just copy it into self.info
			panic("objcCallInContext: string '%s' incomprehensible", ((NSString *)self).UTF8String);
		return nil;
	  }
	else
		panic("why not a string?");
	return nil;
  }
//			 // structure:ARRAY --> build each element
//			if (NSArray *inclValArray = coerceTo(NSArray, inclVal)) {
//				for (id structSub in inclValArray) {
//	/*build*/		Container *c = [self.builder buildSubComponent:structSub];
////					[self addBundle:coerceTo(Plug, c)];
//
//			 // structure:DICTIONARY --> build single element with dictionary
//			else if (NSDictionary *inclValHash = coerceTo(NSDictionary, inclVal)) {
//	/*build*/	Container *c = [self.builder buildOverAs:inclVal];
////				[self addBundle:coerceTo(Plug, c)];
//
//			 // structure:STRING --> parse string, make bundle
//			else if (NSString *inclValStr = coerceTo(NSString, inclVal)){
//				 // see if generators have expanded description
//				expandedDescription rv = [Bundle expandBundleDescription:inclValStr];
//				if (rv.description) {				// did parse uncover a definition?
//					inclValStr = rv.description;				// use expanded definition
////					[self applyExpandedDescription:rv];	// tell generator of description's values
//				  }
//
//				  // Construct insides of Bundle:
//				NSPrintf(@"%*s Adding sub-Bundles per string:@\"%@\"\n", self.builder.indent*3+3, "", inclValStr);
//				NSMutableArray *tokens = [[inclValStr componentsSeparatedByString:@" "] mutableCopy];;
//
////	/*build*/	[self addBundleElementsPerTokens:tokens];
//
//				 assert(tokens.count==0, ("tokens not used. (Indicate string protocol error.)"));
//
////				[self addTreeReferentFactalsInContainer:self up:1]; // Add in UPosts to tagged Factals
//			  }
	//				else if ([prodKey isEqualToString:@"-flattenArray-"]) {
	////					for (id elt in prodVal) {
	////						if (coerceTo(NSString, elt)) {
	////							self.info[elt] = @"???";
	//				else if ([prodKey isEqualToString:@"-flattenHash-" ]) {
	////					for (id hash in product[prodKey]) {
	////						for (id a in hash) {
	////							self.info[a] = product[prodKey];

@end

#pragma mark Number + info
////////////////////////////////////////////////////////////////////////
//////////////////////////////// NUMBER ////////////////////////////////
////////////////////////////////////////////////////////////////////////
@implementation NSNumber /*extended here to*/ (info4NSNumber)
- (BOOL) attemptMatchTo:target withState:tranState indent:indent; {
	[self logOperationStart:@"attemptMatchTo" type:@"(Number *)" indent:indent target:target];
	[self logOperationEnd:indent msg:@"Number never matches anything" state:0];
	return FALSE;
  }
- (void) fireWithTerms:tranState onto:target indent:indent; {
	[self logOperationStart:@"fireWithTerms" type:@"(Number *)" indent:indent target:target];
	[self logOperationEnd:indent msg:@"Number never matches anything" state:0];
  }
@end


#pragma mark String + info
////////////////////////////////////////////////////////////////////////
//////////////////////////////// STRING /////////////////////////////////
////////////////////////////////////////////////////////////////////////
@implementation NSString /*extended here to*/ (info4NSString)// a Category of STRING //////

// Hungarian:
//	target:				pattern to build, needing reduction
//	pattern:			potential understanding of target
//	tranState:		truths to make pattern apply to target; nil=match impossible

/* Strings may represent either object references or string productions

	"<type name>"		
			-- match any target which is of this type
				if ([target isType:type]) tState[name] = target
			-- replace with -- target

	"WW<typeA nameA>XX<typeB nameB>YY"
			-- matches "WW<aaa>XX<bbb>YY" with tState:
				if ([aaa isType:typeA]) tState[nameA] = aaa
				if ([bbb isType:typeB]) tState[nameB] = bbb
			-- replace with -- "WW<>XX<>YY":[aaa, bbb]
 */

- (BOOL) attemptMatchTo:target withState:tranState indent:indent; {////// STRING //////
	[self logOperationStart:@"attemptMatchTo" type:@"(String *)" indent:indent target:target];

	 ////// target "-..." never matches anything
	if (coerceTo(NSString, target) and [target hasPrefix:@"-"])
		return [self logOperationEnd:indent msg:@"target has leading '-'" state:tranState];

	assert(coerceTo(NSString, self), ("must be NSString"));

	 // Decode self								// IN: "pre<type name>post"
	NSString *failReason=0;						// presume success
	for (id nextStr=0, str=self; str; str=nextStr) {
		id strDECODE = [str decodeNextGltArg];	// OUT: type,name,pre,post,extraChars,
		if (!strDECODE[@"valid<>"])
			break;
//			return [self logOperationEnd:indent msg:@"no valid braces '<>'" state:tranState];

		 // CHECK TYPE if specified
		static NSDictionary *type2NSObject = @{
				@"array":@"NSArray",
				@"str":@"NSString",
				@"varHash":@"NSDictionary",
				@"hash":@"NSDictionary"};
		if (id type = strDECODE[@"type"]) {
			Class typesClass = NSClassFromString(type2NSObject[type]);
			if (![target isKindOfClass:typesClass]) {// type match?
				failReason = [catF:
					@"target is '%@' NOT type '%@' as pattern requires", [target className], type];
				break;
//				return [self logOperationEnd:indent msg:failReason state:tranState];
			  }
		  }

		if (id name = strDECODE[@"name"]) {	// was our name decoded in self?

			  // pattern e.g: "pre<typeRName>..." matches target
			 //
			[tranState pushUniqueKey:name val:target];	// possible match

			  // pattern e.g: "<typeRName>" matches target
			 //
			if (!strDECODE[@"extraChars"]) {	// if no extra chars (pre,post)
				if (nextStr)
					failReason = @"only on the highest level";
				break;
			  }
		  }

		 // STRING on STRING:
		else if (coerceTo(NSString, target)) {
			id targetDECODE = [target decodeNextGltArg];

			 ////// e.g. pattern @"xxx" matches target @"xxx"
			if ([self isEqualToString:target])
				break;								// SUCCESS

			 ////// e.g. pattern @"for<arg>in" matches target @"for<val>in" with var[<arg>] = <val>
			if (	[strDECODE[@"pre"]  isEqualTo:targetDECODE[@"pre"]] and
					[strDECODE[@"post"] isEqualTo:targetDECODE[@"post"]]) {
				if (id var = strDECODE[@"name"]) {
					id val;
					if (!(val = targetDECODE[@"name"]))
						val = target;
					assert(coerceTo(NSString, val), ("thin thread -- val is not NSString"));
					[tranState pushUniqueKey:var val:val];
				  }
			  }
		  }
		else
			failReason = @"illegal";

		if (failReason)					// Exit if there was an error
			return [self logOperationEnd:indent msg:failReason state:tranState];
				
		nextStr = strDECODE[@"post"];
	  }
	return [self logOperationEnd:indent msg:failReason state:tranState];
  }

- (void) fireWithTerms:tranState onto:target indent:indent; {////// STRING //////
	[self logOperationStart:@"fireWithTerms" type:@"(String *)" indent:indent target:target];

	for (id subSelf=self, post; [subSelf length]; subSelf=post) {
		assert(coerceTo(NSString, subSelf), ("why not?"));

		 // apply each reference			// e.g. "-for<kv>in<V>-" or "in<V>-"
		id subSelfDECODE = [subSelf decodeNextGltArg];

		if (id name = subSelfDECODE[@"name"]) { // was a name decoded?
			panic("honney, I'm home!");
			if (id namesValue = tranState[name]) {				// is that name defined in transState?
				int i=33;
			  }
			[tranState pushUniqueKey:name val:target];
		  }
		post = subSelfDECODE[@"post"];				// e.g. "in<V>-"			"-"
	  }
	[self logOperationEnd:indent msg:0 state:tranState];
//  	NSPrintf(@"%@  ====== now target is: ,%@,\n", indent, [target ppStr]);
  }

- (NSString *) indentMore; {  return [self stringByAppendingString:@"   "];  }

@end

#pragma mark Array + info
////////////////////////////////////////////////////////////////////////
//////////////////////////////// ARRAY /////////////////////////////////
////////////////////////////////////////////////////////////////////////
@implementation NSArray /*extended here to*/ (info4NSArray)	// a Category

- (BOOL) attemptMatchTo:target withState:tranState indent:indent; {////// ARRAY //////
	[self logOperationStart:@"attemptMatchTo" type:@"(Array *)" indent:indent target:target];

	  //////// Matching against ARRAY:
	 //
	id tentativeState=[tranState mutableCopy];
	[tranState pushToQueNamed:@"-subTransState-" object:tentativeState];

	if (NSArray *targetArray = coerceTo(NSArray, target)) {
		for (id targetArrayElt in targetArray) {
				//#### RECURSIVE:
			if ([self attemptMatchTo:targetArrayElt withState:tentativeState indent:indent]) {
				return [self logOperationEnd:indent msg:0 state:tranState];
//				NSPrintf(@"%@ == state:   %@\n", indent, [tranState ppStrCr]);
//				return TRUE;
			  }
		  }
	  }
	return [self logOperationEnd:indent msg:@"no array element matched" state:tranState];
  }

- (void) fireWithTerms:tranState onto:target indent:indent; {			////// ARRAY //////
	[self logOperationStart:@"fireWithTerms" type:@"(Array *)" indent:indent target:target];
	id rv = @[].mutableCopy;
	if (NSArray *selfArray = coerceTo(NSArray, self))
		for (id targetArrayElt in selfArray) {
			panic("");									//#### RECURSIVE
			[self fireWithTerms:tranState onto:target indent:[indent indentMore]];
			[rv addObject:target];
		  }
	[self logOperationEnd:indent msg:0 state:tranState];
}
@end

#pragma mark Hash + info
////////////////////////////////////////////////////////////////////////
//////////////////////////////// HASH //////////////////////////////////
////////////////////////////////////////////////////////////////////////
@implementation NSDictionary /*extended here to*/ (info4NSDictionary)	// a Category

 // pattern:self matchesWith:target
- (BOOL) attemptMatchTo:target withState:tranState indent:indent; {			////// HASH //////
	[self logOperationStart:@"attemptMatchTo" type:@"(Hash *)" indent:indent target:target];

	 // Return the first match found
	for (id selfKey in self) {								// PATTERN
		id selfVal = self[selfKey];
		if (NSDictionary *targetDict = coerceTo(NSDictionary, target)){ //TARGET
			for (id targetKey in targetDict) {
				id targetVal = targetDict[targetKey];
				id indentMore = [indent indentMore];

				 // **======= 1. Match keys				//#### RECURSIVE
				id trialState = [tranState mutableCopy];
				if ([selfKey attemptMatchTo:targetKey withState:trialState indent:indentMore]) {

					 // ========= 2. Match values		//#### RECURSIVE
					if ([selfVal attemptMatchTo:targetVal withState:trialState indent:indentMore]) {
									
						///// COMMIT TO MATCH -- accept tranState
						[tranState removeAllObjects];
						[tranState addEntriesFromDictionary:tranState];
						
						[tranState pushToQueNamed:@"-consumedKeys-" object:targetKey];

						return [self logOperationEnd:indent msg:nil state:tranState];
					  }
				  }
			  }
		  }
	  }
	return [self logOperationEnd:indent msg:@"no key:val matched" state:tranState];
  }

//	logEvent(facingNothing, @"\n%@=== (Hash *) attemptMatchTo\n%@  = pattern: %@\n",
//			indent, indent,self.ppStr);
//	Object_ *ob = [Object_ another];//[[[Object_ alloc] init] autorelease];
//	[ob logEventGUTS:(char *)__FILE__  :facingNothing :  @"hi-there\n" ];

//	logEvent(facingNothing, @"hi-there\n");
//#define logEvent(a, ...) [self logEventGUTS:(char *)__FILE__  :a : __VA_ARGS__ ]
//- (void) logEventGUTS:(char *)prefix :(facing)place :(NSString *)msg, ...
//										__attribute__((format(NSString, 3, 4)));
//	Object_ *plg = [Plug another];//[[[Object_ alloc] init] autorelease];
//	plg.print;

- (void) fireWithTerms:tranState onto:target indent:indent; {////// HASH //////
	[self logOperationStart:@"fireWithTerms" type:@"(Hash *)" indent:indent target:target];

	for (id selfKey in self) {
		id selfVal = self[selfKey];
			//									HASH on HASH:
		if (NSDictionary *targetDict = coerceTo(NSDictionary, target)) {//TARGET
			for (id targetKey in targetDict) {
				id targetVal = targetDict[targetKey];
				id indentMore = [indent indentMore];

				 // ========= 1. FIRE on keys		//#### RECURSIVE
				[selfKey fireWithTerms:tranState onto:targetKey indent:indentMore];

				 // ========= 2. FIRE on values		//#### RECURSIVE
				[selfVal fireWithTerms:tranState onto:targetVal indent:indentMore];
									
						///// COMMIT TO MATCH -- accept tranState
				[tranState pushToQueNamed:@"-consumedKeys-" object:targetKey];
//				[tranState pushUniqueKeysOf:keyConstraints];
			  }
		  }
		else
			panic("code HASH on XXX");
		//id rv = [modedKey objcCallInContext:tranState to:target];
//		[tranState pushUniqueKey:modedKey val:modedVal];
	  }
	[self logOperationEnd:indent msg:0 state:tranState];
  }

- (void) pushUniqueKey:key val:val; {
	assert(key, ("key is nil"));
	assert(!self[key], ("key '%s' already in hash '%s'", [key ppStr].UTF8String, [self ppStr].UTF8String));
	[(id)self setObject:val forKey:key];
  }

- (void) pushUniqueKeysOf:(NSDictionary *)from; {
	for (id key in from) {
		assert(!self[from], ("key already in hash"));
		[(id)self setObject:from[key] forKey:key];
	  }
  }
	
- (void) pushToQueNamed:que object:val; {
	if (!self[que])	// special entry with list of keys to go
		[(id)self setObject:@[].mutableCopy forKey:que];
	[self[que] addObject:val];			// add key to list
  }

//- (id) objectForKeyedSubscript:(id)key;	{			// id = compiler[@"key"]
//	id rv = [self objectForKey:key];
//	NSPrintf(@"/\\/\\/ [%@ objectForKeyedSubscript:%@] ---> %@\n", self, key, rv);
//	return rv;
//  }
//- (void)setObject:(id)obj forKeyedSubscript:(id <NSCopying>)key; {
//	NSPrintf(@"/\\/\\/ [%@ forKeyedSubscript:%@ setObject:%@]\n",
//								self, key, obj);
//	return [self setObject:obj forKeyedSubscript:key];
//  }
//
@end


////http://stackoverflow.com/questions/18356747/objective-c-message-forwarding-with-forwardingtargetforselector-not-always-worki
//- (BOOL) respondsToSelector:(SEL)aSelector {
//	panic("respondsToSelector");
//	NSDictionary *selfDict = coerceTo(NSDictionary, self);
//	return [super respondsToSelector:aSelector] || [selfDict respondsToSelector:aSelector];
//  }
//
//- (void)forwardInvocation:(NSInvocation *)anInvocation {
//	panic("forwardInvocation");
//	if (NSDictionary *selfDict = coerceTo(NSDictionary, self)) {
//	    if ([selfDict respondsToSelector:[anInvocation selector]])
//			return [anInvocation invokeWithTarget:selfDict];
//	  }
//    else
//        [super forwardInvocation:anInvocation];
//  }
//
//- (NSMethodSignature *)methodSignatureForSelector:(SEL)selector {
//	panic("methodSignatureForSelector");
//	if (NSDictionary *selfDict = coerceTo(NSDictionary, self))
//		return [selfDict methodSignatureForSelector:selector];
//    return [super methodSignatureForSelector:selector];
//  }

//id deepMutableCopy(id inf) {
//	if (NSArray *infoArray = coerceTo(NSArray, inf)) {
//        NSMutableArray *rv = [NSMutableArray array];
//		for (id subInfo in infoArray)
//            [rv addObject:deepMutableCopy(subInfo)];
//        return (id )rv;
//      }
//
//	if (NSDictionary *infoDict = coerceTo(NSDictionary, inf)) {
//        NSMutableDictionary *rv = [NSMutableDictionary dictionary];
//		for (id subInfo in infoDict)
//            rv[subInfo] = deepMutableCopy(infoDict[subInfo]);
//        return (id )rv;
//	  }
//    
//	if (NSString *infoStr = coerceTo(NSString, inf))
//        return (id )[NSString stringWithString:infoStr];
//	if (NSNumber *infoNum = coerceTo(NSNumber, inf))
//        return (id )[NSNumber numberWithInteger:infoNum.intValue];
//
//	panic("info type '%s' undefined", inf.className.UTF8String);
//	return nil;
//}
//
#endif