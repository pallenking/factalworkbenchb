// ParameterPort.mm -- Bit-level access to secondary messenger, global inherited values

#import "ParameterPort.h"

@implementation ParameterPort
//------------------------------ Construction ------------------------------------
#pragma mark  1. Basic Object Level					// Basic objects

- (void) dealloc {
	self.parameterName	= nil;

	[super dealloc];
}

//------------------------------ Navigation -- to move in networks of Parts -----
#pragma mark  6. Navigation
 // do not follow ParameterPorts
- (Port *) skipOverInterveningLinks;					{	return nil;		}

@end
