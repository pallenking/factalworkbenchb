// ParameterPort.mm -- Bit-level access to secondary messenger, global inherited values

#import "Port.h"

@interface ParameterPort : Port

	@property (nonatomic, retain) NSString	*parameterName;

@end
