// Net.mm -- Semantic Network C2015PAK

#import "Net.h"
//#import "Label.h"
#import "Bundle.h"
#import "View.h"
#import "Brain.h"
#import "Path.h"
#import "Common.h"
#import "NSCommon.h"
#import "Atom.h"
#import "Id+Info.h"
#import <GLUT/glut.h>
#import "GlCommon.h"
#import "Model01.h"
#import "FactalWorkbench.h"

@implementation Net

#pragma mark 1. Basic Object Level
- init; {								self = [super init];

	self.minSize = nanVector3f;					// no minimum size declared
	self.placeParts = str2placeType(@"link");	// this default is bold!
	return self;
}

#pragma mark  3. Deep Access

- (id) deepMutableCopy; {		Net *rv = [super deepMutableCopy];

	rv.minSize = self.minSize;
	return rv;
}

#pragma mark  4a Factory Access						// Methods defined by Factory
- (id) atomsDefinedPorts;	{			id rv = [super atomsDefinedPorts];

	[rv removeObjectForKey:@"P"];		// okay, but...
	[rv removeObjectForKey:@"S"];
	rv[@"E"]		= @"p d";
	return rv;								
}
- (Port *) enable;		{	return [self getPortNamed:@"E" down:true];		}

#pragma mark 4. Factory
Net *aNet(NSArray *parts) {		return aNet(nil, parts); }
Net *aNet(NSDictionary *etc, NSArray *parts) {

	id info = @{@"parts":parts?:@0, etcEtc};

	Net *newbie = anotherBasicPart([Net class]);
XX	newbie = [newbie build:info];
	return newbie;
}
- (id) build:(id)info; {							[super build:info];

//!	xzzy1 Net::			1. minSize			:"%f,%f,%f"				"3.4,4.4,0"
//!	xzzy1 Net::			2. minHeight		:<floatVal>				"3.4", @345

	if (NSString *minSizeStr = [self takeParameter:@"minSize"]) {
		assert(minSizeStr.asString, (@"arg must be string"));
		Vector3f minSize;
		int n = sscanf(minSizeStr.UTF8String, "%f,%f,%f", &minSize.x, &minSize.y, &minSize.z);
		assert(n==3, (@"%@ decoded %d tokens, should be 3", minSizeStr, n));
		self.minSize = minSize;
	}
	if (NSString *minHeight = [self takeParameter:@"minHeight"]) {
		if (isNan(self.minSize))
			self.minSize = zeroVector3f;
		float y = max([minHeight floatValue], self.minSize.y);
		self.minSize = Vector3f(self.minSize.x, y, self.minSize.x );
	}
	return self;
}

- (bool) applyPropNVal:(NSString *)propNVal; {

	NSArray *tokens = [propNVal componentsSeparatedByString:@":"];
	if (tokens.count == 2) {		// e.g: "minSize:3,4,5"
		panic(@"this should never happen");
		if ([self applyProp:tokens[0] withVal:tokens[1]])
			return true;
		
		if ([tokens[0] isEqualToString:@"minSize"]){// e.g. "minsize:3.4,44,21"
			panic(@"");
			Vector3f minSize;
			int n = sscanf([tokens[1] UTF8String], "%f,%f,%f", &minSize.x, &minSize.y, &minSize.z);
			assert(n==3, (@"%@ decoded %d tokens, should be 3", tokens[1], n));
			self.minSize = minSize;					// set property
			return true;
		}
	}
	return [super applyPropNVal:propNVal];
}

- (bool) applyProp:prop withVal:val; {
	if ([prop isEqualToString:@"minSize"]){	// e.g. "minsize":"3.4,44,21"
		Vector3f minSize;
		int n = sscanf([val UTF8String], "%f,%f,%f", &minSize.x, &minSize.y, &minSize.z);
		assert(n==3, (@""));
		self.minSize = minSize;						// set property
		return true;
	}

XR	return [super applyProp:prop withVal:val];
}

#pragma mark 5. Wire

 // Bundles add for structured links (150722: naissant)
- (Port *) getOpenPort_openingDown:(bool)localDown; {

	 // perhaps the Port is already there
	for (Part *elt in self.parts)				// go through my parts
		if (Port *bElt = coerceTo(Port, elt))		// examine Ports
			if (bElt.flipped==localDown)				// properly flipped
				// BUT THIS ELEMENT MAY NOT BE OPEN!!
				return bElt;
	//	 // perhaps the Port is already there
	//	for (Part *elt in self.parts)
	//		if (Port *bElt = coerceTo(Port, elt))
	//			if (bElt.flipped==localDown)
	//				return bElt;
	//

	 // Presume no existing open -- make a new one!
  	Port *rv = [self addPart:[Port another]];
	rv.name = @"ALL";
	rv.flipped = localDown;
	rv.fullNameLldb = rv.fullName;

	return rv;
}

#pragma mark 9. 3D Support
// \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\    //
//  \\ \\ \\ \\ \\ \\ \\ \\ \ 3D Support\\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\  //
//   \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\//
- (void) boundIntoView:(View *)v; {						[super boundIntoView:v];

	  // minimum size and margins
	 //
	Bounds3f newBounds = v.bounds;
	Vector3f newSize = maxOf(newBounds.size(), self.minSize);
	newBounds.setSize(newSize);
	v.bounds = newBounds;
}

- (Matrix4f) positionOfPort:(Port *)port inView:(View *)bitV; {
	Vector3f bitSpot;

	if ([port.name isEqualToString:@"E"]) {		//// P: Primary
		assert(port.flipped, (@"'ena' should be flipped"));
		bitSpot			= bitV.superView.bounds.centerYBottom();
		bitSpot.y -= 1.0;
		return [port suggestedPositionOfSpot:bitSpot];
	}
	if ([port.name isEqualToString:@"ALL"]) {		//// P: Primary
		bitSpot			= bitV.superView.bounds.centerYBottom();
		bitSpot.y -= 1.0;		// swag now
		return [port suggestedPositionOfSpot:bitSpot];
	}
	
XR	return [super positionOfPort:port inView:bitV];
}

#pragma mark 11. 3D Display
//// // // // // // // // // // // // // // // // // // // // // // // ///
/// // // // // // // // // // 3D Display  // // // // // // // // // ////
// // // // // // // // // // // // // // // // // // // // // // // // //
- (void) drawFullView:(View *)v context:(RenderingContext *)rc; {

	if (self.brain.displayNetNames and !coerceTo(Bundle, self)) {
		glPushMatrix();
			glTranslatefv(v.bounds.point1());
			Vector3f inCameraShift = Vector3f(0.0, 0.0, 2);	// in "facing camera" coordinates
			Vector2f spot2labelCorner = Vector2f(-0.5, -0.5);	// box center --> llc
			rc.color = colorBlue4;
			myGlDrawString(rc, self.name, -1, inCameraShift, spot2labelCorner);
		glPopMatrix();
	}

	[super drawFullView:v context:rc];
}

guiVector3fAccessors4(minSize, MinSize, _minSize);

//- (float) minSizex { return _minSize.x; }
//- (void) setMinSizex:(float)val { _minSize.x = val; }
//- (float) minSizey { return _minSize.y; }
//- (void) setMinSizey:(float)val { _minSize.y = val; }
//- (float) minSizez { return _minSize.z; }
//- (void) setMinSizez:(float)val { _minSize.z = val; };
//- (NSString *) minSize2;		{	return @"minSizeGetter";					}
//- (void) setMinSize2:val;		{	NSPrintf(@"minSizeSetter<-%@", val);		}

- (NSString *) pp1line:aux; {					id rv = [super pp1line:aux];

	if (!isNan(self.minSize) and self.minSize.lengthSq()!=0)
		rv=[rv addF:@"minSize:%@ ", pp(self.minSize)];
	return rv;
}



@end
