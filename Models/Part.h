// Part.h -- a simulation object with physical and containment abilities C2011 King Software
/*!                          The Layers of a Part:


All Parts support:
        1. containment hierarchy:           parts containment other parts
        2. physical display suggestions:	adorn(axis, placeSelf orientation, spots,...)
        3. simulation protocol


------------------------------ Construction ------------------------------------
#pragma mark  0. Class Objects
#pragma mark  1. Basic Object Level					// Basic objects
	Basic, bare objects (usually) have no sub-objects.
#pragma mark  1b Class initializers
#pragma mark  2. 1-level Access
#pragma mark  3. Deep Access

#pragma mark  4a Factory Access						// Methods defined by Factory
#pragma mark  4. Factory							// Compound objects
	Compound objects are created, but totally disembodied, with no knowledge of place in Net
#pragma mark  5. Wire

------------------------------ Navigation -- to move in networks of Parts -----
#pragma mark  6. Navigation

------------------------------ Reenactment Simulator -- simulation protocol ----
#pragma mark  7. Simulator Messages
#pragma mark  8. Reenactment Simulator
#pragma mark  8b Port Access During Simulation

------------------------------ 3D Display -- display as 3D objects -------------
#pragma mark  9. 3D Support							// 3D Visualization
#pragma mark 10. Placement (e.g: self.~:
        flipped:        !=0: we are inserted in the network flipped
                        ==0: we are inserted in the network viewed upright
#pragma mark 11. 3D Display
#pragma mark 12. Inspectors
#pragma mark 13. IBActions							// [[shift around]]
 
------------------------------ Printout -- pretty print ------------------------
#pragma mark 14. Logging							// Console Logic
#pragma mark 15. PrettyPrint
#pragma mark 16. Debugging Aids

160128 Snapshot of subclasses:

Part               simulation objects which are physical things

  Port               the bidirectional connection of one port of info
    Share           Maintains one channel of the plug protocol to a Splitter
      subclasses: Bayes, Hamming, KNorm, MaxOr, MinAnd, Multiply, Sequence, Broadcast

  Atom              a node in the reactive semantic network            
    Splitter        splits one Port into many, in various ways 
    GenAtom         an Atom used by bundleTaps
    Mirror          an atom which mirrors its input to its output
    Modulator       Multiplies upgoing and downgoing signals by control
    Previous        Remembers what happened previously, used for prediction
	Ago				Remembers what happened previously, used for prediction
    WriteHead       Adds new elements to record unknowns
    BundleTap		an element which generates data for a Bundle
    Link            an Atom inserted in Port Connections to add visual delay
//  Known           Accounts for known phenomenon, in reducing the unknown
//  Rotator         Connects the Haves and Wants of 3 ports WIP 

  Net               Semantic Network
	Brain			the entire net of a reactive brain
    Actor           a Net with known upper con and lower evidence

    Bundle          A Net whose subNets are accessable as terminals
	  Leaf			a Net with Port bindings used as the terminals of a Bundle
      Tunnel		A Bundle which forms entrance/exit of a tunnel
//  MatrixActor     A Matrix forumlation of an Actor
//Label             display a text label in 3D space

NSViewController=NsVc, NSView=NsV,  NSWindowController=NsWc, NSWindow=NsW


	Standard Section Order:
			Most Parts have the following sctions, in this order:




http://stackoverflow.com/questions/23743279/disable-dont-reopen-or-reopen-dialog-on-ios:
defaults write com.apple.iphonesimulator NSQuitAlwaysKeepsWindows -bool false
defaults write com.apple.dt.Xcode NSQuitAlwaysKeepsWindows -bool false

 */

#import "HnwObject.h"
#import "RenderingContext.h"
#import "SystemTypes.h"

@class Port;
@class Net, Leaf, Actor;
@class Path;
@class View;
@class Brain;
@class WriteHead;

static const NSArray *placeAxisNames = @[@"X", @"Y", @"Z"];
static const NSArray *placeTypeNames = @[@"?",@"<",@">",@"c"];

 ///// Placement Types:
typedef enum : unsigned char {
/// Parts a, b, and c  can be placed so min, max, or CenTeR align:
	cUndef=0x00,	cMin=0x01,	cMax=0x02,	cCtr=3,		cMask=0x03,
	bUndef=0x00,	bMin=0x04,	bMax=0x08,	bCtr=0xC,	bMask=0x0C,
	aUndef=0x00,	aX  =0x10,	aY  =0x20,	aZ	=0x30,	aMask=0x30,
	aNeg  = 0x40,	aPos=0x00,
	isLink= 0x80,
}		PlaceType;
 // common values:
static const PlaceType nilPlaceType  = (PlaceType)0x0;
 // conversions:
NSString *pp(PlaceType placement);
PlaceType str2placeType(NSString *placeVal);		// e.g: [s w][+ -][x y z]


@interface Part : HnwObject
//  @property (nonatomic, retain) NSString	*name;		// (in parent)

	@property (nonatomic, retain) NSString	*fullNameLldb;// needed for lldb
	@property (nonatomic, assignWeak) Part	*parent;
	@property (nonatomic, retain) NSMutableArray *parts;// of Parts
	@property (nonatomic, retain) NSMutableDictionary *parameters;// with inheritance
	@property (nonatomic, assignWeak) Brain	*brainCache;

	   // Suggestions from the part on construction of View
	  //
     // part properties relating to this view: They are of this Part's detail

	 // To affect the spot this (or a part) is placed, when positioning in view:
	@property (nonatomic, assign) PlaceType	placeSelf;// of cluster (physically and informationally)
	@property (nonatomic, assign) PlaceType	placeParts;

	 // EXTERNAL To affect the orientation of this Parts placement, when positioning in view:
	@property (nonatomic, assign) char		flipped;	// from parent's view
//		#define flippedLower 1
//		#define flippedUpper 0
	 // EXTERNAL
    @property (nonatomic, assign) char		latitude;	// 8 steps from the north pole down
		#define latitudeNorthPole	0					// (any val; neg latitudes work)
		#define latitudeCapricorn	2
		#define latitudeEquator		4
		#define latitudeCancer		6
		#define latitudeSouthPole	8
	 // EXTERNAL
    @property (nonatomic, assign) char		spin;		// about Y axis cw 0,1,2,3 * 90 degrees
    @property (nonatomic, assign) char		shrink;		// smaller or larger as one goes in
														// factor = pow(1.01, shrink)



		// When a View is forced to be atomic, only atomic information about the
	   //	object (name, kind, connections) is displayed.
	  //	 Hopefully, if a view is made atomic, it can be displayed as before when not.
	 // Keep in sync with initialDisplayModeNames;
	typedef enum : unsigned char {
		displayAtomic		=0x02,		// Limit view to atomic
		displayOpen			=0x04,		// Display all contents
		displayInvisible	=0x08,		// Do not display
	} DisplayMode;
	#define initialDisplayModeNames @{@"Atm":@2, @"Opn":@4, @"Inv":@8,}
	// const DisplayMode displayAtomic		=0x02;		// Limit view to atomic
	// const DisplayMode displayOpen		=0x04;
	// const DisplayMode displayInvisible	=0x08;		// Do not display
    @property (nonatomic, assign) DisplayMode initialDisplayMode;	// atomizes
    @property (nonatomic, assign) char		flash;

    @property (nonatomic, assign) Vector3f	jog;		// force position by jog

	 // 171203: THIS IS ODDLY PLACED
	@property (nonatomic, retain) id		writeHead;	// WriteHead that birthed us
		// WriteHead animation:
		// 1. ends of links (if other.part.writeHead.animationOffset)
		//		presumes all links are in the actor.
		// 2. shift the part (and its insides)
		//		part may be in subview of actor

#pragma mark 1. Basic Object Level
//- (void)		reset;

#pragma mark 2. 1-level Access
- (id)			addPart:(Part *)part;
- (id)			addPart:(Part *)part atIndex:(int)index;
- (Part *)		partAtIndex:(int)index;
- (Part *)		partNamed:(NSString *)name;
- (NSUInteger)	indexOfPart:part;
- (int)			numParts;
 // parameters on local object
- (id)			parameter:(id)key;          // == parameters[key]
- (id)			takeParameter:(id)key;      // remove and return
- (id)			parametersWithPrefix:prefix;// ad hoc

#pragma mark 3. Deep Access
 // parameters with structural object inheritance
- (id)			parameterInherited:(id)key;	// climbs object hier, then containment hier.
- (void)		setViewDirty:(bool)val;
- (void)		setBoundsDirty:(bool)val;

 // define an operation to be evaluated for each Leaf
typedef void	(^PartOperation)(Part *);
- (void)		forAllParts:(PartOperation)partOperation;


 //// Accessors of 'brain' support caching:
- (Brain *)		brain;			// go to our parent if brain is nil
- (void)		setBrain:(Brain *)brain;	// set all of our parts

/*!
 fullNames, or pathNames reflect the Part's position in the inclusion hierarcharchy
	examples of fullNames:
		/brain1/actor1/aba			a part named  aba in actor1
		/brain1/actor1/aba.P		the P Port of aba
*/
- (NSString *)	fullName;
- (const char *)fullNameC;
- (NSString *)	fullName16;		// fullName as 16 chars
- (const char *)fullName16C;
- (void)		setAllFullNamesLldb; // these strings be in memory for lldb print summaries
- (void)        setDistributions;
- (int)			portCount;
- (void)		setEnclosedPortWriteHeads:val;

 // Navigation
- (Net *)		enclosingNet;
- (id)			enclosedByClass:(Class)cxlass;
- (bool)		hasAsAncestor:(Part *)ancestor;
- (NSArray *)	ancestorArray;
- (Part *)		ancestorThatsChildOf:(Part *)grandpa;
- (Actor *)		actor;
- (Leaf *)		leaf;

 // Reference by Path
- (Part *)		resolveInwardReference:(Path *)other openingDown:(bool)downInSelf except:exception;
- (Part *)		resolveOutwardReference:(Path *)other openingDown:(bool)downInSelf;

#pragma mark 3b. Facing Down
 // to a related Part:
- (bool)		downInPart:(Part *)part;			// 1 -> I am facing down in part.
 // to the world:
- (bool)		downInWorld;

#pragma mark 4. Factory 
- (id)			build:(id)args;
- (void) 		postBuild;			// SwiftFactals.groomModel()

- (id)			addBasicPart_kind:(id)classname;		// 1. with self as parent
   id   		anotherBasicPart(Class class1);			// 2. no parent
+ (Part *)		conceiveBabyIn:(WriteHead *)wh evi:evi con:con;

- (bool)		applyPropNVal:propNVal;					// spec matches a legal prop and value
- (bool)		applyProp:prop withVal:val;

#pragma mark 4b. Factory Support:
 /// Append etc hashes with minimal efffort. Part flattens out all the etc's
#define etcEtc  @"etc":(etc? : @{})					// helper, to add extra stuff if there
#define etc1etc @"etc":(etc1?: @{})					// helper, to add extra stuff if there
#define etc2etc @"etc":(etc2?: @{})					// helper, to add extra stuff if there
#define etc3etc @"etc":(etc3?: @{})					// helper, to add extra stuff if there
   id			flattenFromInto(id from, id into);

#pragma mark 5. Wire
- (id)			gatherWiresInto:(id)wirelist;
- (void)		addWireToList:wirelist
					   source:fromPort	  sourceModifiers:sourceModifiers
					   target:target			todInSelf:(bool)todInSelf
					  purpose:purpose;
- (NSMutableArray *) parseTargets:targets;
   void			wireViaList(id wires);
- (void)		orderPartsByConnections;

#pragma mark 6. Navigation
//- (Atom *)		connectedToAtom;
- (Port *)		biggestBitOpeningDown:(bool)downInSelf;
- (Port *)		getOpenPort_openingDown:(bool)localDown;

typedef bool	(^PartPredicate)(Part *);
- (id)			searchInsideFor: (PartPredicate)partPredicate;
- (id)			searchOutsideFor:(PartPredicate)partPredicate;
- (id)			searchOutsideFor:(PartPredicate)partPredicate except:exception;

#pragma mark 6b. named access
   Net *		smallestNetEnclosing(Part *m1, Part *m2=nil);

#pragma mark 7-. Simulation Actions
- (void)		reset;

#pragma mark 7. Simulator Messages
  FwEvent		makeFwEvent(FwType fwType);
- (id)			sendMessage:(FwType)fwType;
- (id)			receiveMessage:(FwEvent *)event;

#pragma mark 8. Reenactment Simulator
- (void)		simulateDown:(bool)downLocal;

- (id)			ppUnsettledString;
- (int)			unsettledPorts;

#pragma mark 9. 3D Support
- (Bounds3f)	gapAround:(View *)v;

 // Update view
- (View *)		reViewIntoView:(View *)v;		// options?
	/// Complete tree always generated, except
	///		isAtomic ==> @"initialDisplayMode":<asAtom>

 // To bound and position views in clumps:
- (void)		boundIntoView:(View *)v;
//160824 Usage notes:
//	Part:	1. A subviews: {bound; place }
//	  Atom:   1. Box Around; 2. Replace Ports			v.bounds >> gapAround;
//		Net:	1. minSize
//		  Actor:  1. Bound all; do not place; 2. Place CONtext FIRST at BOTTOM; 3. ...
//		  Bundle: 1. XXgapAroundXX						v.bounds >> gapAround;
//		Ago,GenAtom, BundleTap, Known, Link, Mirror, ...
- (void)		boundAsAtomIntoView:(View *)v;
- (void)		placeIntoView:(View *)v;

- (void)		suggestedSpinInView:(View *)v;
- (Matrix4f)	suggestedPositionOfSpot:(const Vector3f &)place;

#pragma mark 11. 3D Display
- (void)		drawFullView:			 (View *)v context:(RenderingContext *)rc;
- (void)		drawAtomicInView:		 (View *)v context:(RenderingContext *)rc;
- (void)		displayBoundingBoxInView:(View *)v context:(RenderingContext *)rc;
- (void)		displayAxisInView:		 (View *)v context:(RenderingContext *)rc;
- (FColor)		colorOf:(float)ratio;

#pragma mark 12. Inspectors
				/// From GUI:
- (void)		sendPickEvent:(FwEvent *)fwEvent toModelOf:(View *)v;
-				initialDisplayModeStr;

extern Matrix4f spinMatrices[];

#pragma mark 14. Logging
- (void) 		logEvent:(NSString *)msg, ... __attribute__((format(NSString, 1, 2)));
   void  		logCheckBreaks(id brain_);


#pragma mark 15. PrettyPrint
- (NSString *)	ppFlipped;
- (NSString *)	ppSubs:sub prefix:kind;
- (NSString *)	ppSuggestedPosition;

   NSString	*	pp(Part *part);
 const char *	ppC(Part *part);

@end

