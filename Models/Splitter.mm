// Splitter.mm -- splits one Port into many, in various ways C2011PAK

/* to do:
 */

#import "Brain.h"
#import "Splitter.h"
#import "Share.h"
#import "Leaf.h"
#import "Bundle.h"
#import "Link.h"
#import "View.h"
#import "FactalWorkbench.h"

#import "Common.h"
#import "NSCommon.h"
#import "SystemTypes.h"
#import "Id+Info.h"
#import "GlCommon.h"
#import <GLUT/glut.h>

@implementation Splitter

#pragma mark 1. Basic Object Level

#pragma mark  2. 1-level Access

- (NSString *) classCommonName6	{
	return [@"" addN:6 F:@"Sp<%@", self.shareClass? self.shareClass.className:@"nil"];
}


#pragma mark 3. Deep Access
- (id) deepMutableCopy; {		Splitter *rv = [super deepMutableCopy];

	rv.a1						= self.a1;
	rv.combineWinner			= self.combineWinner;

	rv.shareClass0				= self.shareClass0;
	rv.shareClass1				= self.shareClass1;
	rv.shareClass				= self.shareClass;
	rv.isABcast					= self.isABcast;
	rv.onlyPosativeWinners		= self.onlyPosativeWinners;
	rv.upIsDirty				= self.upIsDirty;
	rv.noPosition				= self.noPosition;
	return rv;
}

- (id) className; 		{
	if (Class c = self.shareClass)
		return c.className;

	return [super className];
}

#pragma mark  4a Factory Access						// Methods defined by Factory
- (id) atomsDefinedPorts;	{				id rv = [super atomsDefinedPorts]; // probably returns "P"
	[rv removeObjectForKey:@"S"];	// strange?
	rv[@"sec"]	= @"   ";	// some shared SECondary	(no port, just hook for adding shares)
	rv[@"U"]	= @"p  ";	// Unknown					(If needed)
	rv[@"B"]	= @"p  ";	// Broadcast (of P)			(If needed)
	rv[@"KIND"]	= @"p d";	// Chooses shareProto[0,1]
	return rv;
}
atomsPortAccessors(uPort,		U, false)
atomsPortAccessors(bPort,		B, false)
atomsPortAccessors(kindPort,	KIND, true)

- (void) setKind:(char)val; {
	panic(@"wtf");
}

#pragma mark 4. Factory
 // All Splitters funnel to a common aSplitter routine
Splitter *aBroadcast(id pri, id sec, id etc) { return aSplitter(@"Broadcast",pri,sec, etc); }
Splitter *aMaxOr	(id pri, id sec, id etc) { return aSplitter(@"MaxOr",	pri, sec, etc); }
Splitter *aMinAnd	(id pri, id sec, id etc) { return aSplitter(@"MinAnd",	pri, sec, etc); }
Splitter *aBayes	(id pri, id sec, id etc) { return aSplitter(@"Bayes",	pri, sec, etc); }
Splitter *aHamming	(id pri, id sec, id etc) { return aSplitter(@"Hamming", pri, sec, etc); }
Splitter *aMultiply	(id pri, id sec, id etc) { return aSplitter(@"Multiply",pri, sec, etc); }
Splitter *aKNorm	(id pri, id sec, id etc) { return aSplitter(@"KNorm",	pri, sec, etc); }
Splitter *aSequence	(id pri, id sec, id etc) { return aSplitter(@"Sequence",pri, sec, etc); }
Splitter *aBulb		(id pri, id sec, id etc) { return aSplitter(@"Bulb",	pri, sec, etc); }

Splitter *aSplitter(id share, id pri, id sec, id etc) {
	share = share ?: @0;
	id info = @{@"Share":share, etcEtc}.anotherMutable;
	if (pri)
		info[@"P"] = pri;
	if (sec)
		info[@"sec"] = sec;

	Splitter *newbie = anotherBasicPart([Splitter class]);
XX	newbie = [newbie build:info];
	return newbie;
}

- (void) setShareClass:(Class)val {

	_shareClass = val;					// set value in property

	  /// Related parameters are automatically regenerated
	 ///
	id shareClassName		= val.className;
	self.isABcast			= [shareClassName isEqualToString:@"Broadcast"];

	 // a pragmatic default
	self.onlyPosativeWinners	= [shareClassName isEqualToString:@"MaxOr"] or
							  [shareClassName isEqualToString:@"MinAnd"];
	 /// HACK: Go back to parameters, which override
	if (id onlyPosativeWinners = [self parameterInherited:@"onlyPosativeWinners"])
		self.onlyPosativeWinners = [onlyPosativeWinners intValue];
}

- (id) build:(id)info;	{					[super build:info];

//! xzzy1 Splitter::	1. Share		:StringX			-- kind of gate's shares
//! xzzy1 Splitter::	2. Share		:[String1, String2] -- kind of gate's shares
//! xzzy1 Splitter::	3. kind			:<int>
//! xzzy1 Splitter::	4. Share		:[String1, String2] -- kind of gate's shares
//! xzzy1 Splitter::	5. onlyPosativeWinners :<bool>		-- or no winner if result is <0

	 ///// Set Kind of Splitter, and the kind of any subsequent shares.
	if (id share = [self takeParameter:@"Share"]) {
		 /// 1 Share supplied
		if (NSString *shareStr 	= coerceTo(NSString, share)) {
XX			self.shareClass 	= NSClassFromString(shareStr);	 // get the appropriate class object
			[self logEvent:@"  .shareClass = %@", shareStr];
			assert(self.shareClass, (@""));

			 // Initially both in switching array have this value
			self.shareClass0	= self.shareClass;
			self.shareClass1	= self.shareClass;			
		}
		 /// Array of 2 Shares supplied (for switching by kind:)
		else if (NSArray *shareArray = coerceTo(NSArray, share)) {
			assert(shareArray.count==2, (@"must supply 2 shareClasses"));
			self.shareClass0 	= NSClassFromString(shareArray[0]);
			assert(self.shareClass0, (@""));
			self.shareClass1 	= NSClassFromString(shareArray[1]);
			assert(self.shareClass1, (@""));

			[self logEvent:@"  .shareClass[0..1] = %@; .shareClass = the first", [share pp]];
XX			self.shareClass 	= self.shareClass0;	 // presumes "kind" Port is 0 to start
		}
		else
			panic(@"Share:%s incomprehensible", [share ppC]);
	}
	else {
		self.shareClass 		= NSClassFromString(@"Broadcast");
		self.shareClass0 		= self.shareClass;
		self.shareClass1 		= self.shareClass;
		[self logEvent:@"  .shareClass[*] =\"Broadcast\" BY DEFAULT"];
	}

	self.noPosition 			= [[self takeParameter:@"noPosition"] boolValue];

	return self;
}

- (Share *) anotherShare; {

	Share *share 				= anotherBasicPart(self.shareClass);
	assert(share, (@"nil share prototype"));
	share.flipped 				= 0;					// probably not needed

	[self addPart:share];
	NSInteger ind 				= [self indexOfPart:share];
	assert(ind!=NSNotFound, (@""));
	share.name 					= [@"" addF:@"%ld", ind];
	share.fullNameLldb 			= share.fullName;

	self.upIsDirty 				= 1;			// need downward, then upward
	return share;
}

#pragma mark 6. Navigation
- (Port *) biggestBitOpeningDown:(bool)downInSelf; {
	if (downInSelf)										// trunk?
		return [super biggestBitOpeningDown:downInSelf];	// follow P

	 // Up in self canonic form
	int a 						= self.combineWinner;
	if (a >= 0) {								// if there is a winner
		Part *winrPort 			= [self partAtIndex:a];
		return coerceTo(Port, winrPort);
	}
	Port *rv = nil;
	for (id port_ in self.parts) {
		if (Port *port 			= coerceTo(Port, port_))
			if (!port.flipped == !downInSelf) {
				if ([port.name isEqualToString:@"U"])
					;
				else if (!rv)		// first
					rv 			= port;
				else
					[[@"" addF:@"????? [%@ getBit_...]: ignoring %@, alrady found %@",
								self.name, port.name, rv.name] ppLog:nil];
			}
	}
	return rv;
}

- (Port *) getOpenPort_openingDown:(bool)localDown; {
	if (!localDown)				// if an upward Y-Splitter

		 // upside of splitter can always create another open plug
		return [self anotherShare];					// just have to give it a share

	 // primary end: see super...
	return [super getOpenPort_openingDown:localDown];	// try subclasses
}

 /// A disconnected Share gets deleted
- (void)		portCheck;		{

	 // Check all Shares
	for (id p in self.parts.copy)
		if (Port *port = coerceTo(Port, p))
			if (!port.connectedTo)				// if unconnected
				[self.parts removeObject:port];		// remove
}

#pragma mark 7-. Simulation Actions
- (void) reset; {								[super reset];
	
	assert(self.shareClass,  (@"%@:  splitter.shareClass  = nil!.", self.fullName));
	assert(self.shareClass0, (@"%@:  splitter.shareClass0 = nil!.", self.fullName));
	assert(self.shareClass1, (@"%@:  splitter.shareClass1 = nil!.", self.fullName));
	
	// CHANGED 170419
	self.combineWinner			= -1;		// proportional sharing
	//self.combineWinner		= 0;		// no winner
	
	self.upIsDirty				= 1;		// recalculate all going UP, even if P in is unchanged
	
	//	 // 170311 Insure consistency of newly added parts
	//XX[self distributeTotal:self.pPortIn.valueGet];
	
}

#pragma mark 8. Reenactment Simulator
///*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//
//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//
////*//*//*//*//*//*//*//*//  Reenactment Simulator  //*//*//*//*//*//*//*//*//
///*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//
//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//

 // Use the attached Shares to process data:
- (void) simulateDown:(bool)downLocal; {

	 // Sequence is special: just cycle its part Ports
	if (self.shareClass == [Sequence class])
		return [super simulateDown:downLocal];
	  ////*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*

	if (downLocal) {		//									  ///////////////
							//									 //// DOWN  ////
		[super simulateDown:downLocal];	// step super FIRST		///////////////
		int winner=0;
		static int redo = 0;
		   //													 ///////////////
		  // GATHER/COMBINE from all the SHARES					/// COMBINE ///
		 //													   ///////////////
		// The value a1 in splitter scans through intermediary values: (161023)
		//!						a1
		//!	 Broadcast		Sum(inI)
		//!	 MaxOr:			Max(inI, 0.01)
		//!	 MinAnd:		Min(inI)
		//!	 Bayes			Sum(inI)
		//!	 Hamming:		Sum(inI-cut)+cut		cut is a global constant
		//!	 Multiply:		Prod(inI)
		//!	 KNorm			(Sum(inI^^k))^^(1/k)	k is a global constant
		//!	 Sequence:		  - n.a. -

		 // ==== Initialize
		[self.shareClass combinePre:self];			/*** INZ ***/
		//!<< combinePre >>: OPERATION
		//! DEFAULT:	a1=0.1
		//!	 MaxOr:		a1=-inf
		//!	 MinAnd:	a1=+inf
		//!	 Bayes:		a1=0.1
		//!	 Multiply:	a1=1.0
		//!	 Sequence:   ?

		 // ==== Scan Shares
		self.combineWinner = -1;	// no winner yet
		int i=-1; for (id sc in self.parts) {	i++;
			 /// Go through all Shares:
			if (Share *sh 		= coerceTo(Share, sc)) {// ignore Primary and Unknown Ports
				 /// Gather properties of input value
				Port *shInPort 	= sh.connectedTo;
				bool shareInputChanged = shInPort.valueChanged;
				self.upIsDirty 	|= shareInputChanged;// changed --> dirty

				float val 		= shInPort.valueGet;	// (clears .valueChanged)

				/// Combine into Primary Port												/*** NEXT: ***/
XX				if ([self.shareClass combineNext:self val:val])
					self.combineWinner = i;		// current is winner -- record it
				//!<< combineNext >>: OPERATION		  RETURNS:
				//!	DEFAULT:	a1   += con			false
				//!	 MaxOr:		a1 max= con			==highest if con>0.01
				//!	 MinAnd:	a1 min= con			==lowest
				//!	 Hamming:	a1   += con-cut		false
				//!	 Multiply:	a1   *= con			==lowest
				//!	 KNorm		a1   += con**cp		false
				//!	 Sequence:	  ---				false

				if (shareInputChanged)
					[sh logEvent:@"   COMB:val=%.2f:a1=%.2f cWin=%d", val,
						self.a1, self.combineWinner];
			}
		}														/*** POST: ***/
		 // ==== Post Process
		[self.shareClass combinePost:self];
		//!<< combinePost >>: OPERATION
		//! DEFAULT:			<nothing>
		//!	 Hamming:		a1   += cut
		//!	 KNorm			a1	  = a1**(1/cp)

		 // 160427: New mode: for MaxOr, no signal means no winner
		if (self.onlyPosativeWinners) {			//
			if (self.combineWinner<0) {			// and there is no declared winner
				self.combineWinner = 0;			// declare part 0 (P) the winner
				self.a1 		= 0.0;
			}
		}

		 /// Although Combine goes DOWN, it recalculates two UPward values: B and U
		bool nextValueDifferent = self.pPort.value != self.a1;
		self.upIsDirty |= nextValueDifferent;
		if (nextValueDifferent)  {		// Put to Output
XX			self.pPort.valueTake = self.a1;

			if (self.bPort) {
				[self.bPort logEvent:@"   broadcast =%.2f", self.pPort.value];
XX				self.bPort.valueTake = self.pPort.value;
			}
			if (self.uPort) {
				float unknownValue 	= self.pPortIn.value - self.a1;	// unexplained residue
				unknownValue	= unknownValue<0? 0: unknownValue;	// (never negative)
	
				[self.uPort logEvent:@"   unknown =%.2f-%.2f", self.pPort.value, self.a1];
XX				self.uPort.valueTake = unknownValue;
			}
		}
		 // Clear out runt changes (just read)
		float readClears1 		= self.bPort.connectedTo.valueGet;
		float readClears2 		= self.uPort.connectedTo.valueGet;
	}
																 //////////////////
																//////  UP  //////
	//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//
															  //////////////////
	if (!downLocal) {										 /// DISTRIBUTE ///
															//////////////////
		 // Push a portion of the value down to each Share
		if (self.pPortIn.valueChanged or self.upIsDirty) {	// P Port or internal upIsDirty
			float total = self.pPortIn.valueGet;

XX			[self setDistributions];					/// DOES ALL THE WORK

			 // Process UNKNOWNs (more sense than known)
			if (Port *uPort 	= self.uPort) {
				float accountedFor = self.pPort.value;
								
				float unknownValue = total - accountedFor;
				unknownValue	= unknownValue<0? 0: unknownValue;
	
				[uPort logEvent:@"   DIST U=%.2f (%.3f-%.3f)", unknownValue, total, accountedFor];
XX				uPort.value/*Take*/ = unknownValue;			// silently
			}

			 // Process BROADCASTs (same to all)
			if (self.bPort) {
				[self.bPort logEvent:@"   DIST B=%.2f", total];
XX				self.bPort.value/*Take*/ = total;			// silently
			}
			
			 // Change to Bulb's size requires a resizing:
			if (self.shareClass == [Bulb class]) {
				self.brain.boundsDirty = true;
//				assert([self.shareClass respondsToSelector:@selector(splitter:setToValue:)], (@""));
//				[self.shareClass splitter:self setToValue:total];
			}
		}
		[super simulateDown:downLocal];	// step super AFTER
	}

	if (downLocal) {					//============: going DOWN (either way)
		if (self.kindPortIn.valueChanged) {			// KIND port changes mode
			float valPrev		= self.kindPortIn.valuePrev;
			float valNext		= self.kindPortIn.valueGet; // ( get new value; remove )
			[self logEvent:@" Branch: kind=%.2f (was %.2f)", valNext, valPrev];

			int shareIndex		= valNext > 0.5;
			self.shareClass 	= shareIndex? self.shareClass1 :self.shareClass0;
			assert(self.shareClass, (@"shareClass is nil"));
		}
	}
}


// Presuming Canonic Form (Tree of green leafs)
- (void) setDistributions; {							 /// DISTRIBUTE ///
	self.upIsDirty = 0;							// handshake: clear: soon will be not dirty
	float total = self.pPortIn.valueGet;
	
	if (self.combineWinner >= 0) {	//// Winner (new)://////////////////////
		int i=-1; for (id sc in self.parts) {i++;	/// WINNER TAKE ALL  //
			if (i==0 and self.combineWinner==0)	   ///////////////////////
				[self logEvent:@"No winner: all Shares get 0.0"];
			if (Share *sh 		= coerceTo(Share, sc)) {
				float distribution = 0;				// loosers get nothing
				id msg 			= @"LOOSER";
				if (i==self.combineWinner)   {
					assert(i!=0, (@"0 is primary, not a share"));
					distribution = total;			// winner takes all
					msg 		= @"WINNER";
				}
				[sh logEvent:@"  %@ val=%.2f", msg, distribution];
XX				sh.valueTake 	= distribution;
			}
		}
	}												 ////////////////////
	else if (self.combineWinner < 0) {/// No Winner:/// PROPORTIONAL ///
XX		float bidTotal = [self.shareClass bidTotal:self];//////////////
		//!<< bidSum >>: OPERATION
		//! DEFAULT:	Sum(inI)
		//!	 Broadcast:	1.0
		//!	 Hamming:	1.0
		
		for (Share *sc in self.parts) {
			if (Share *sh = coerceTo(Share, sc)) {
				
XX				float bidOfShare = sh.bidOfShare;
				
				assert(!(bidOfShare!=0 and bidTotal==0), (@"bidOfShare isn't zero, but bidSum is Zero"));
				float distribution = bidOfShare==0? 0.0: // (irrespective of bidTotal)
									  				 total * bidOfShare / bidTotal;
				id wasStr 		= sh.value==distribution? @"(unchanged)":
				[@"" addF:@"(was %.2f)", sh.value];
				[sh logEvent:@"   DIST %.2f*(%.2f/%.2f) %.2f %@",
					total, bidOfShare, bidTotal, distribution, wasStr];
				
				if (sh.value != distribution)
XX					sh.value/*Take*/= distribution;	// silently

//					  // 170725: In an attempt to have the key stop
//					 // between birth and setteling, we try this hack.
//					sh.valuePrev	= distribution;	// vewy siwently
			}
		}
	}
}

#pragma mark 9. 3D Support
// \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\    //
//  \\ \\ \\ \\ \\ \\ \\ \\ \ 3D Support\\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\  //
//   \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\//
- (Bounds3f) gapAround:(View *)v; {
	SplitterSkinSize sss 		= [self.shareClass splitterSkinSizeIn:self]; // to compute GAP AROUND
	return Bounds3f(-sss.width/2,		   0,-sss.deapth/2,
					 sss.width/2, sss.height, sss.deapth/2);
}

Vector3f unknownOffset = Vector3f(0, 2, -2);				// useful, till Stux

- (Matrix4f) positionOfPort:(Port *)port inView:(View *)bitV; {
	Bounds3f splitterP = [self gapAround:bitV];
	Vector3f bitSpot;

	if (coerceTo(Share,port))					//// All Shares go in one spot:
		return [port suggestedPositionOfSpot:
				splitterP.centerYTop()];		/// on top, center

	if ([port.name isEqualToString:@"U"]) {		//// U: Unknown Port
		assert(!port.flipped, (@"'U' shouldn't be flipped"));
		Vector3f bitSpot = splitterP.centerYTop() + unknownOffset;
		bitSpot.y				-= 2;
//		port.latitude			= 2;				// fails...
//		port.spin		 		= (port.spin+3)%4;	// i
		return [port suggestedPositionOfSpot:bitSpot];
	}

	if ([port.name isEqualToString:@"B"]) {		//// B: Broadcast Port
		assert(!port.flipped, (@"'B' shouldn't be flipped"));
		Vector3f bitSpot 		= splitterP.centerYTop() + unknownOffset;
		bitSpot.x				+= 2;
		bitSpot.y				-= 2;
		return [port suggestedPositionOfSpot:bitSpot];
	}

//	if ([port.name isEqualToString:@"M"]) {	//// M: mPort
//		assert(port.flipped, (@"'M' shouldn't be flipped"));
//		Vector3f bitSpot = splitterP.centerYBottom();//splitterP.centerYTop();//splitterP.center();
//		bitSpot.x				+= 2;
//		//centerXLeft.y 		+= 1.0;		// nudge up a little
//		//port.latitude 		= 4;
//		return [port suggestedPositionOfSpot:bitSpot];
//	}

	if ([port.name isEqualToString:@"KIND"])	//// kind Port
		return [port suggestedPositionOfSpot:zeroVector3f];

XR	return [super positionOfPort:port inView:bitV];
}

#pragma mark 11. 3D Display
//// // // // // // // // // // // // // // // // // // // // // // // ///
/// // // // // // // // // // 3D Display  // // // // // // // // // ////
// // // // // // // // // // // // // // // // // // // // // // // // //

- (void) drawFullView:(View *)v context:(RenderingContext *)rc; {

	glPushMatrix();
		rc.color = colorBundlePorts_bias([self colorOf:1]);

		glTranslatefv(v.bounds.centerYTop()); // to the top center of splitter
		glTranslatef(self.brain.displayOffsetX, 0, self.brain.displayOffsetZ);

XX		[self.shareClass drawSplitterSkinInView:v con:rc];

	glPopMatrix();

	 // Draw parallelogram to connect Unknown and Broadcast Ports to body of splitter
	for (id x in @[self.uPort?:@0, self.bPort?:@0])
		if (!coerceTo(NSNumber, x))  {			// connect Unknown to body with polygon:
			float x0 			= x==self.bPort? 2.0: 0.0;
			Vector3f p1 		= v.bounds.centerYTop(), dxz(x0,0,-2.0), dy(0,-1,0);
			glPushMatrix();
				glBegin(GL_POLYGON);
				glVertex3fv(p1+dy);
				glVertex3fv(p1+dy+dy);
				glVertex3fv(p1+dy+dxz);	//dy+
				glVertex3fv(p1+   dxz);	//dy+
				glVertex3fv(p1+dy);
				glEnd();
			glPopMatrix();		// now in center of Splitter
		}

	[super drawFullView:v context:rc];
}

- (FColor) colorOf:(float)ratio {

	if (self.proxyColor)
		return proxyFColor[self.proxyColor];
	return colorOrange;
}


#pragma mark 15. PrettyPrint
    ///\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\///
   ///\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\///
  ///\\//\\//\\//\\//\\//\\//\\// PRINTOUT //\\//\\//\\//\\//\\//\\//\\//\\///
 ///\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\///
///\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\///

- (NSString *)	pp1line:aux; {				id rv=[super pp1line:aux];

	rv=[rv addF:@"a1=%.2f ", self.a1];

	if (self.combineWinner >= 0)
		rv=[rv addF:@"cWin=%d ", self.combineWinner];

	rv=[rv addF:@"%@ ", self.shareClass.className];
	if (0)
		rv=[rv addF:@"[%@,%@] ", self.shareClass0.className, self.shareClass1.className];

	if (self.isABcast)
		rv=[rv addF:@"isABcast "];
	if (self.onlyPosativeWinners)
		rv=[rv addF:@"WTA "];
	if (self.noPosition)
		rv=[rv addF:@"!posn "];
	if (self.upIsDirty)
		rv=[rv addF:@"reComp "];
	return rv;
}

  /// Print a splitter's contents in 1 line
 /// Gater pass loads allNames; Output pass prints with allNames columnar
- (NSString *) printContentsGather:(bool)gather names:(NSMutableArray *)allNames; {
	NSString *rv = [@"" addF:@"%@ %@: ", self.name6, self.classCommonName6];
	int rv0 = (int)rv.length;

	for (Part *element in self.parts) {
		if (Port *port = coerceTo(Port, element)) {
			if ([port.name isEqualToString:@"P"])
				continue;
			Port *p = port.portPastLinks;

			 // rather ad hoc:
			NSArray *tokens = [p.fullName componentsSeparatedByString:@"/"];
			NSString *printName	= tokens[tokens.count-1];			// default

			if (Leaf *leaf = coerceTo(Leaf, [p enclosedByClass:@"Leaf"])) {
				id prev			= [printName containsString:@"+."]? @"+":
								  [printName containsString:@"-."]? @"-": @"";
				printName		= [leaf.name addF:@"%@", prev];
			}

			if (gather)						// first pass
				[allNames addObjectIfAbsent:printName]; // remember names
			else {							// second pass --- output
				NSString *printNameNSeparator = [printName addF:@", "];
				int posn = rv0;					// find printName in allNames
				for (NSString *name in allNames) {
					if ([name isEqualToString:printName])
						break;
					posn += name.length + 2;	// positions used (2, for ", ")
				}
				NSRange r		= {posn, printNameNSeparator.length};

				 // increase size of outBuf to handle printName
				while (rv.length < r.location + r.length + 2)
					rv = [rv addF:@"      "];	// okay if too long

				rv=[rv stringByReplacingCharactersInRange:r withString:printNameNSeparator];
			}
		}
	}
	return rv;
}


@end

