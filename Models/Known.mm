//  Known.mm -- Accounts for known phenomenon, in reducing the unknown. C2014PAK

/// 150323 This functionality has wandered into unknownPort, so only old things use this

#import "Known.h"
#import "Brain.h"
#import "FactalWorkbench.h"

#import "View.h"
#import "Common.h"
#import "NSCommon.h"
#import "GlCommon.h"
#import <GLUT/glut.h>

@implementation Known

static float aPriori = 0.2;

#pragma mark  4a Factory Access						// Methods defined by Factory
- (id) atomsDefinedPorts;	{			id rv = [super atomsDefinedPorts];
//										// probably returns "P"
	rv[@"S"]		= @"pc ";				// known   Port rv[@"P"]		= @"pcd";
	rv[@"T"]		= @"pc ";				// unknown Port
	return rv;								
}
atomsPortAccessors(sPort,		S, false)
atomsPortAccessors(tPort,		T, false)

#pragma mark 4. Factory 
Known *aKnown(id pri, id etc) {
	id info = @{@"P":pri?:@0, etcEtc};

	Known *newbie = anotherBasicPart([Known class]);
XX	newbie = [newbie build:info];
	return newbie;
}

#pragma mark 8. Reenactment Simulator
// /*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*
// *//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*/
// //*//*//*//*//*//*//*//*//    Reenactment Simulator   //*//*//*//*//*//*//*//
// /*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*///
// *//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*////

 // This code has not been debugged since 140501~, when Splitters supported Excesses
- (void) simulateDown:(bool)downLocal; {
	Port *pPortIn			= self.pPortIn;
	Port *sPortIn			= self.sPortIn;
	Port *tPortIn			= self.tPortIn;

	bool simGoingDown		= downLocal;
	if (!simGoingDown) {					//============: going UP
	
		if (pPortIn.valueChanged) {				//####### from SELF Port
			float valPrev	= pPortIn.valuePrev;
			float valNext	= pPortIn.valueGet;
			sPortIn.valueTake = valNext;				///////## to  KNOWN Port

			float unknownHave = valNext - sPortIn.value;		//- knownPort.connectedTo.value;
			unknownHave		= unknownHave>0.0? unknownHave: 0.0;
			tPortIn.valueTake = unknownHave;			//////##to   unknown Port
		}
	}
	else {								//============: going DOWN
		if (tPortIn.valueChanged) {				//####### from unknown Port
			float valPrev	= tPortIn.valuePrev;
			float valNext	= tPortIn.valueGet;//####### from unknown port
			[self logEvent:@" primary=%.2fwas %.2f)", valNext, valPrev];

			pPortIn.valueTake = valNext;				///////## to   SELF Port
		}
		if (sPortIn.valueChanged) {					//####### from KNOWN Port
			float valPrev	= sPortIn.valuePrev;
			float valNext	= sPortIn.valueGet;//####### from PREV port
			[self logEvent:@" known=%.2fwas %.2f)", valNext, valPrev];

			float unknownHave = pPortIn.value - valNext;	// unknown.have is undesired residue
			unknownHave		= unknownHave>0.0? unknownHave: 0.0;
			tPortIn.valueTake = unknownHave;			///////##to   unknown Port
		}													///////## -- nowhere --
	}
	[super simulateDown:downLocal];
}

#pragma mark 9. 3D Support
// \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\    //
//  \\ \\ \\ \\ \\ \\ \\ \\ \ 3D Support\\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\  //
//   \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\//
float knownRadius = 1.0;
float knownWidth  = 6.0;	// 8.0;
float knownHeight = 3.0;
float knownDeapth = 2.0;

- (Bounds3f) gapAround:(View *)v; {
	return Bounds3f(	   -1,          0.0, -knownDeapth/2,
				 knownWidth-1,  knownHeight,  knownDeapth/2);
}

- (Matrix4f) positionOfPort:(Port *)port inView:(View *)bitV; {
	Vector3f bitSpot = zeroVector3f;

	if ([port.name isEqualToString:@"T"])			// CONTROL
		return [port suggestedPositionOfSpot:Vector3f(knownWidth-2, knownHeight-0.5, 0)];

XR	return [super positionOfPort:port inView:bitV];
}

#pragma mark 11. 3D Display
//// // // // // // // // // // // // // // // // // // // // // // // ///
/// // // // // // // // // // 3D Display  // // // // // // // // // ////
// // // // // // // // // // // // // // // // // // // // // // // // //
- (void) drawFullView:(View *)v context:(RenderingContext *)rc; {
	rc.color = colorOrange;
	glPushMatrix();
		glTranslatef(self.brain.displayOffsetX, 1.5, self.brain.displayOffsetZ);

		glPushMatrix();
			glTranslatef(+0.5*knownRadius, 0, 0);
			glScalef(3*knownRadius, 1, knownDeapth);
			glutSolidCube(1.0);
		glPopMatrix();
		glPushMatrix();				//////////////////////////////// 2. known latch
			glTranslatef( 3.5*knownRadius, -0.25, 0);
			glScalef(3*knownRadius, 0.5, knownDeapth);
			glutSolidCube(1.0);
		glPopMatrix();
	glPopMatrix();

	[super drawFullView:v context:rc];				// PRIMARY
}

- (FColor)colorOf:(float)ratio;			{	return colorBlue4;			}//colorOrange

@end
