//  Share.h --		C2013PAK
/*!
    The replicated chunck of a Splitter factal's "multi" (sharing) end.
    Storage place for "Omega"'s share of downward values, bidOfShare
    Algorithmic code using it is in Splitter. (Share is just provides storage.)
 */

@class Splitter;
#import "Port.h"

typedef struct {
	float width, height, deapth;	// Bounds of Splitter, per Share kind
	 // The distance from the center to the arrowhead
	 //  a) dist < radius
	 //  b) dist < inset	WTF: negative values, etc
	float inset;					// from center to Y-most extension of Share
	float radius;					// keepout region
} SplitterSkinSize;

/*!		Implementation Notes:
	1. Get a share's parent with: mustBe(Splitter,self.parent);
	2. Splitter has an accumulator -(float)a1;
	3. - (Class)shareClass;	// works even if no shares
 */

@interface Share : Port


#pragma mark 1. Basic Object Level
//- (float) a1;						- (void) setA1:(float)val;

#pragma mark  3. Deep Access

#pragma mark  8. Reenactment Simulator
 // Combine (many to one)
+ (void)  combinePre :(Splitter *)sp;
+ (bool)  combineNext:(Splitter *)sp val:(float)contrib; //rv = winnerSoFar;
+ (void)  combinePost:(Splitter *)sp;

 // Shares, used for distribution
- (float) bidOfShare;
+ (float) bidTotal:(Splitter *)sp;

+ (SplitterSkinSize) splitterSkinSizeIn:(Splitter *)sp;
+ (void) drawSplitterSkinInView:(View *)v con:(RenderingContext *)rc;
@end

 // various subclasses of Share, each with different system functionality
@interface Broadcast 	:	Share @end
@interface MaxOr 		:	Share @end
@interface MinAnd 		:	Share @end
@interface Bayes 		:	Share @end
@interface Hamming 		:	Share @end
@interface HammingMem 	:	Hamming @end
@interface Multiply 	:	Share @end
	// 150128 generalized form; not used much:
@interface KNorm 		:	Share @end       // has no skin
@interface Sequence 	:	Share @end

	// visual aid, could be combined with Broadcast
@interface Bulb 		:	Broadcast//Share
//+ (void) splitter:(Splitter *)splitter setToValue:(float)total;
@end


/*	Associate:
							  aMaxOr
								|
						 bestMaxorPort
							 /	|
			.---------------'	|
			|					|
		 aHamming -____________ |
		/	|	\			   \|
	[ Worker Unknowns ]	  biggestHamming
						  /		|	  \


					P
				[ BCast ]
		con:		Si

					P
				[  Ago  ]			@"P":con
		con'		S

					P
				[Hamming]



			 Broadcast
			   Latch
			  Rotate <-- sproutMPredicate		[eviUnknowns]
				^
				|
			sproutSBase					[braUnknowns]


 */
