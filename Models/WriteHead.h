//  WriteHead.h -- Adds new elements to record unknowns C2014PAK

#import "Atom.h"
@class Net;
@class Actor;

@interface WriteHead : Atom

	@property (nonatomic, retain) Class		babyPrototype;	// prototype for new element
	@property (nonatomic, retain) Actor		*actor;			// where to add  new element
    @property (nonatomic, assign) float		bulge;			// 0:none, 1:full

/*                                  babyInView
                               baby |     canalOpen	  animationOffset
                               |    |       |       	|
0. Reset                       ->0  ->0     ->0
                                                            
1. conceiveBaby makes baby:    ->b  ->0     ->0
    Port.writeHead = writeHead  (for all Ports in baby)
    baby.writeHead = writeHead	(for just baby)
                                                            
2. Event sim_writeHeadLabor     .   .       ->1			e.g. key up after conceive
                                                            
  Draw baby: In baby.writeHead:
3. animateBirthOfView:          b   =0 ->1  .			animationOffset = computation
4. animateBirthOfView:          b   =1      =1 ->0
5. animateBirthOfView:          b   .       .			animationOffset *= decay

  Draw Port:
6. use animationOffset */
	@property (nonatomic, retain) Part		*baby;			// new element being generated
	@property (nonatomic, assign) char		babyInView;		// baby placed in Views,
	@property (nonatomic, assign) char		canalOpen;		// can give birth
    @property (nonatomic, assign) Vector3f	animationOffset;// current - final

- (Vector3f) animateBirthOfView:(View *)view;

@end
