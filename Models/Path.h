//  Path.h -- specify a relative or absolute path C2015PAK

/*!		Path Semantics:
 
	<atomToken> ::=  <aToken> + <modifiers>
	<bitToken>	 ::=  <bToken> + <modifiers>

NOTATION (pretty vanilla:)
	a) 'c'			-- the character code c	('' is no char)
	b) <varName>	-- the variable named varName
	c) (<operation>)-- a grammar operation  (e.g. "(+)" is the string catenate operator)
	d) (<a>)*		-- do <a> 0 or more times,  (<a>)+ one or more times, ...
	e) ||			-- alternatives,				a || b

SYNTAX:			-<( following lines should match .mm setNameFull )>-
AAA	 ///!	<fullName>  ::=  <partName>  <linkOptA>* <linkOptB>*	// full path with options
	 ///!
BBB	 ///!      <linkOptA>    ::= ('=' ||		// direct connect; no link
	 ///!                         '%' ||		// reverse up/down checking (161113 NOT WORKING)
	 ///!                         '^' ||		// suspend up/down checking
	 ///!                         '!' ||		// position by this path only
	 ///!                         '@' ||		// invisible
	 ///!                                   // unused: #$%
	 ///!                                )* // 0, 1, ... options
	 ///!            EG:   "foo="						// connect to foo, use no link
	 ///!
	 ///!      <linkOptB>    ::=  (, <name> = <val>)*	//NOTE: older form "(,<float>)" with "l" presumed ABANDONED
	 ///!                      <name> in {"l", ...?}
	 ///!            EG:   "/aa.P,l=5"
	 ///!                  "foob!,l=5"      // "" to foo, using this link only, link length 5
	 ///!
	 ///!
CCC	 ///! A "." separates path from namePort:
	 ///!	<partName>   ::=   <atomName>
	 ///!	<partName>   ::=   <atomName> '.' <portChar>
	 ///!	E.G:		 bun/a/prev.P		-- relative form
	 ///!		 /brain1/bun/a/prev.T		-- absolute form
	 ///!			where <portChar> is of the set {P, S, ....)

DDD	 ///!	<atomName>   ::=                    <token>
	 ///!	<atomName>   ::=               '/'  <token>
	 ///!	<atomName>   ::=   <atomName>  '/'  <token>
	 ///!	E.G:		 bun/a/prev			-- relative form
	 ///!		 /brain1/bun/a/prev			-- absolute form

	 ///!  Examples:
	 ///!  bun/a/prev		the Atom bun/a/prev
	 ///!  bun/a/prev.S	the Port bun.a.prev.S
	 ///!  i=				direct link to closet part named "i"
	 ///!  i!^				link to "i", position only by it, suspend checking
	 ///!  a.P				name ends in "a", the Port name is "P"
	 ///!  /aa.P,5			to .../aa.P with length 5.0
 */

#import "HnwObject.h"
@class Part;

@interface Path : HnwObject
//	@property (nonatomic, retain) NSString	   *name;			// in HnwObject

	@property (nonatomic, retain) NSString	   *nameFull;
	@property (nonatomic, retain) NSString	   *nameAtom;
	@property (nonatomic, retain) NSString	   *namePort;		// after last '.'

	@property (nonatomic, retain) NSMutableArray *linkOptions;	// just length now
	@property (nonatomic, assign) bool			direct;			// trailing '='
	@property (nonatomic, assign) bool			flipPort;		// trailing '%'
	@property (nonatomic, assign) bool			noCheck;		// trailing '^'
	@property (nonatomic, assign) bool			dominant;		// trailing '!'
	@property (nonatomic, assign) bool			invisible;		// trailing '@'
//	@property (nonatomic, assign) bool			openingDown;	/// EXPERIMENTAL

	@property (nonatomic, retain) NSString	   *summary;		// for lldb


+ (Path *)		pathWithName:reference;

- (id) takeOptionsFrom:(NSString *) nameNoptions;

- (bool) fullNameMatches:(Part *)part;
- (bool) atomNameMatches:(Part *)part;

- (NSString *)	pp;
- (NSString *)	ppProperties;
- (const char *)ppC;

@end
