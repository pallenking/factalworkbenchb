// SoundAtom.h -- an Atom to connect to Generators C2014PAK

#import "Atom.h"

@interface SoundAtom : Atom

    @property (nonatomic, retain) NSString	*sound;
    @property (nonatomic, retain) NSSound	*soundObject;
/*
	  Ports:
		 S	             S
			         ----o----
			  .-----|L|     in|-----.
			  [      ^       |      ]
sound:		  [      |       |      ]
			  [sound |       |      ]
			  [    ^\|       |      ]
			  '-----|in     |L|-----'
				     ----o----
		 P			     P


 */

    @property (nonatomic, assign) char		playing;

@end
