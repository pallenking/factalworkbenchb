// GenAtom.mm -- an Atom to connect to Generators C2014PAK

#import "GenAtom.h"
#import "Brain.h"
#import "FactalWorkbench.h"
#import "View.h"
#import "Leaf.h"

#import "Common.h"
#import "NSCommon.h"
#import "Id+Info.h"
#import <GLUT/glut.h>
#import "GlCommon.h"

@implementation GenAtom


#pragma mark 1. Basic Object Level
- init; {											self = [super init];

	self.value			= nan("no fixed value");
	return self;
}

#pragma mark  2. 1-level Access

#pragma mark  3. Deep Access
- (id) deepMutableCopy; {				GenAtom *rv = [super deepMutableCopy];

	rv.value			= self.value;
	rv.loop				= self.loop;

	return rv;
}

#pragma mark  4a Factory Access						// Methods defined by Factory
- (id) atomsDefinedPorts;	{				id rv = [super atomsDefinedPorts];

	[rv removeObjectForKey:@"S"];				// no S Port
	rv[@"LOOP"]			= @"p  ";
	return rv;
}
atomsPortAccessors(loopPort,		LOOP,	false)

#pragma mark 4. Factory
GenAtom *aGenAtom(id etc) {
	id info = @{etcEtc}.anotherMutable;

	GenAtom *newbie = anotherBasicPart([GenAtom class]);
XX	newbie = [newbie build:info];
	return newbie;
}

- (id) build:(id)info;	{					[super build:info];

//!	xzzy1 GenAtom:: 	1. value			:float			--
//!	xzzy1 GenAtom:: 	2. loop				:int			--
//!	xzzy1 GenAtom:: 	4. LOOP				:<portName>		--

	if (id value		= [self takeParameter:@"value"])
		self.value		= [value floatValue];
	if (id loop			= [self takeParameter:@"loop"])
		self.loop		= [loop intValue];

	return self;
}

#pragma mark 7-. Simulation Actions
- (void) reset; {								[super reset];

	 // We want self.value to be set up before bindings
	if (!isNan(self.value))						// constant value from self.value
		self.pPort.valueTake = self.value;			// set it
}

#pragma mark 8. Reenactment Simulator
- (void) simulateDown:(bool)downLocal;  {

	if (!downLocal) {				///////// going UP /////////
		float loopVal = self.loopPortIn.valueGet;	// always read LOOP to clear changed
XX		if (self.pPortIn.valueChanged) {
			float pPortInVal = self.pPortIn.valueGet; // always read P to clear changed
			if (loopVal > 0.5 or self.loop) {			// Two causes, port or config
				[self logEvent:@"Loop to value %.2f to sPort", pPortInVal];
				self.pPort.valueTake = pPortInVal;			// looped value from pPortIn
			}
		}
		if (!isNan(self.value))						// constant value from self.value
			self.pPort.valueTake = self.value;			// set it
	}

	return;
}

#pragma mark 9. 3D Support
- (Matrix4f) positionOfPort:(Port *)port inView:(View *)bitV; {

	if ([port.name hasPrefix:@"LOOP"])
		return [port suggestedPositionOfSpot:Vector3f(0, 2, 0)];

XR	return [super positionOfPort:port inView:bitV];
}

- (Bounds3f) gapAround:(View *)v;	{	return Bounds3f(-1.0, 0.0,-1.0,    1.0, 1.0, 1.0);}

#pragma mark 11. 3D Display
- (void) drawFullView:(View *)v context:(RenderingContext *)rc; {
	float r = 3 * self.brain.bitRadius;
	glPushMatrix();
		rc.color = colorYellow;
		glTranslatef(0, 1.0, 0);
		glScalef(r, 0.25, r);
		glutSolidCube(1.0);
	glPopMatrix();

	[super drawFullView:v context:rc];				// PRIMARY
}

- (FColor)colorOf:(float)ratio;		{	return colorOrange;}

#pragma mark 15. PrettyPrint
- (NSString *)	pp1line:aux; {		id rv=[super pp1line:aux];

	if (self.loop)
		rv=[rv addF:@" loop"];
	if (!isNan(self.value))
		rv=[rv addF:@" value=%.2f", self.value];

	return rv;
}

@end
