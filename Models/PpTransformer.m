// PpTransformer.m -- value transformer for IB C2017PAK

#import "PpTransformer.h"
#import "Id+Info.h"

@implementation PpTransformer

+ (Class) transformedValueClass  			{	return [NSObject class];	}
+ (BOOL) allowsReverseTransformation		{	return NO;					}

- (NSString *) transformedValue:(id)value  	{	return [value pp];			}

@end
