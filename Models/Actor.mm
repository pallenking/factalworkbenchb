//  Actor.mm -- a Net with known upper context and lower evidence Bundles C2015PAK

#import "Id+Info.h"
#import "View.h"
#import "Actor.h"

#import "Bundle.h"
#import "Branch.h"
#import "Tunnel.h"
#import "Brain.h"
#import "Model01.h"
//#import "GenAtom.h"
#import "FactalWorkbench.h"

#import <GLUT/glut.h>
#import "GlCommon.h"
#import "Common.h"
#import "NSCommon.h"

@implementation Actor

#pragma mark 1. Basic Object Level	// Basic objects
- init; {						self = [super init];

	self.previousClocks	= @[].anotherMutable;
	self.positionViaCon	= 0;	// 161003 now defaults OFF
	return self;
}

- (void) dealloc {
	self.con			= nil;
	self.evi			= nil;
	self.previousClocks	= nil;
//	self.timingChain	= nil;

	[super dealloc];
}


#pragma mark  2. 1-level Access
- (void) setViewAsAtom:(char)value; {
	_viewAsAtom = value;
	self.brain.someViewDirty = true;
}

#pragma mark  3. Deep Access
- (id) deepMutableCopy; {		Actor *rv = [super deepMutableCopy];

	rv.evi						= self.evi.deepMutableCopy;
	rv.con						= self.con.deepMutableCopy;
	rv.previousClocks			= self.previousClocks.deepMutableCopy;

	rv.positionViaCon			= self.positionViaCon;
	rv.viewAsAtom				= self.viewAsAtom;
	rv.displayInvisibleLinks	= self.displayInvisibleLinks;
	return rv;
}

#pragma mark  4. Factory							// Compound objects
Actor *anActor(id info) {
	Actor *newbie = anotherBasicPart([Actor class]);
XX	[newbie build:info];
	return newbie;
}

   // evi{proto info spin is} eviProto eviInfo eviSpin eviIs evi.structure
  // evi{proto info spin is} eviProto eviInfo eviSpin eviIs evi.structure
 //
- (id) build:(id)info; {		[super build:info];

//! xzzy1 Actor::		1.	evi						: @{...}
//! xzzy1				 a.	evi.structure			: <>
//!	xyzz1				 b.	evi.proto					:<Leaf>
//!	xyzz1				 c.	evi.info					:@{...}
//!	xyzz1				 d.	evi.spin					: <spinXX>
//!	xyzz1				 e.	evi.is						:"aTunnel", "aBundle"
//! xzzy1 Actor::		2.	con						: @{...as evi...} (flipped by default)
//! xzzy1 Actor::		3.	viewAsAtom			: <>
//! xzzy1 Actor::		4.	displayInvisibleLinks	: <bool>
//! xzzy1 Actor::		5.	E						: <enable Actor operation>
//! xzzy1 Actor::		6.	TimingChain:1			: add timing chain module

	    //////////////// MAKE ALL PARTS OF ACTOR: ////////////////
	   ///
	  // EVIDENCE, gather parameters and build
	 //
	self.evi				= [self takeParameter:@"evi"]?: 0;
	self.evi.placeSelf		= str2placeType(@"+Ycc");
	self.evi.name			= @"evi";

	  // CONTEXT, gather parameters and build
	 //
	self.con				= [self takeParameter:@"con"]?: 0;
	self.con.placeSelf		= str2placeType(@"+Ycc");
	self.con.name 			= @"con";

	if (id viewAsAtom = [self parameterInherited:@"viewAsAtom"])
		self.viewAsAtom 	= [mustBe(NSNumber, viewAsAtom) intValue];

	if (id disp = [self parameterInherited:@"displayInvisibleLinks"])
		self.displayInvisibleLinks = [disp intValue];

	 // Color EVIdence Bundle GREEN (
	[self.evi searchInsideFor:^(Part *m) {		//##BLOCK
		coerceTo(Atom, m).proxyColor = 1;			// (see Colors.h: proxyFColor)
		return false;	}];

	 // Color CONtext Bundle RED (
	[self.con searchInsideFor:^(Part *m) {		//##BLOCK
		coerceTo(Atom, m).proxyColor = 2;			// (see Colors.h: proxyFColor)
		return false;	}];


	[self enforceOrder];						// evi on bottom, con on top
	self.parameters[@"addPreviousClock"] = @1;	// Add Previous Clock for me

	return self;
}

/*
	ALGORITHM:	scan through Net,
				move improper forward references infront of us
 */
- (void) orderPartsByConnections; {
	int retryCount = 100;
	
	for (bool orderIsGood=0; !orderIsGood; ) {
		[self enforceOrder];

		 // Make a try to clear out self.parts
		int scanI = 0;
		orderIsGood = 1;				// presume scan succeeds

		 // Scan through our Actor's Parts, from bottom up
		for (; scanI<self.parts.count; scanI++) {
			Part *scanPart = self.parts[scanI];
			if (coerceTo(NSNumber, scanPart))
				;
			 // Look only at Atoms:
			else if (Atom *scanAtom = coerceTo(Atom, scanPart)) {

				 // Scan thru Ports of Atoms:
				for (id scanPort_ in scanAtom.parts) {
					if (coerceTo(NSNumber, scanPort_))
						continue;
					Port *scanPort = coerceTo(Port, scanPort_);
					if (!scanPort.flipped ^ !!scanAtom.flipped) // going down is OKAY
						continue;
					if (!coerceTo(Port, scanPort))			 // Ports are OKAY to check
						continue;

					if (Port *otherPort = coerceTo(Port, scanPort.connectedTo)) {
						Atom *otherAtom = otherPort.atom;		// Identify otherAtom

						 // If ancestor of otherAtom is part of self
		//				Part *othersActorPart = [otherAtom ancestorThatsChildOf:self];
						NSInteger otherI = [self.parts indexOfObject:otherAtom];
						if (otherI==NSNotFound)					// OtherActorPart found in us
							continue;								// (not)
						if (otherI>scanI) {						// atomI is beyond OtherActorPart
							 // pull that worker to just below us
							[self logEvent:@"Actor reordering '%@' to index %d",otherAtom.name, scanI];
							[self.parts removeObject:otherAtom];
							[self.parts insertObject:otherAtom atIndex:scanI];
							 // and start all over again...
							orderIsGood = 0;
							break;
						}
					}
					else
						assert(scanPort.connectedTo==0, (@"peculiar"));
				}
				if (orderIsGood == 0)
					break;
			}
		}
		orderIsGood =  scanI==self.parts.count;
		if (retryCount--<0) {
			panic(@"In ordering Actor Parts, Looped 100x: Actor has an unusual net!");
			break;
		}
	}
	[self enforceOrder];
}

 // ensure EVIdence is first element (if it exists), and CONtext the last (if it exists):
- (void) enforceOrder; {
	if (self.evi) {			/////// evi exists:
		if ([self.parts indexOfObject:self.evi] != 0) {
		  // evi in wrong place:						// if it is not first
			[self.parts removeObject:self.evi];				// remove
			[self.parts insertObject:self.evi atIndex:0];	// at start
			self.evi.parent = self;							// required first time
		}
	}
	if (self.con) {			/////// con exists:
		int count = (int)self.parts.count;
		if ([self.parts indexOfObject:self.con] != count-1) {
		  // con in wrong place:						// if it is last first
			[self.parts removeObject:self.con];				// remove
			[self.parts addObject:self.con];				// add at end
			self.con.parent = self;							// required first time
		}
	}
}

- (id) gatherWiresInto:(id)wirelist; {			[super gatherWiresInto:wirelist];

	 // An enable for previousClock.
	if (id clockEnable = [self takeParameter:@"clockEnable"]) {
		panic(@"this has never been activated");
		Port *enaPort	= [self addPart:[Port another]];
		enaPort.name	= @"Ena";
		enaPort.flipped	= 1;

		if (Actor *actor = self.parent.actor) { //To the tightest enclosing Actor
			[actor.previousClocks addObject:self];
			[self logEvent:@"CLOCK  '%@'.previousClocks now contains '%@'", actor.fullName16, self.fullName16];
		}
	}

	return wirelist;
}

#pragma mark  7. Simulator Messages
- (id) receiveMessage:(FwEvent *)event; {

	 /// Actors convert event:sim_clockPrevious --> clockPrevious
	if (event->fwType == sim_clockPrevious) {
		[self clockPrevious];
		return nil;	 				//  (Do NOT give sim_clockPrevious)
	}

	return [super receiveMessage:event];			// default behavior
}

 // Propigate cockPrevious to all contents registered in previousClocks
- (void) clockPrevious;  {

	self.flash = self.brain.flashFrames/4;	// (visual effect)
	
	Port *enaInPort	= coerceTo(Port, self.enable.connectedTo);
	if (!enaInPort or enaInPort.valueGet>0.5) {			// no enable Port --> enabled
		[self logEvent:@"|| $$ clockPrevious to Actor; send to %d customer(s):",
												(int)self.previousClocks.count];
		for (id user in self.previousClocks)
			[user clockPrevious]; /// Actor got -clockPrevious; send to customer
	}
	else
		[self logEvent:@"|| $$ clockPrevious to Actor: IGNORED"];
}

#pragma mark 8. Reenactment Simulator
- (void) simulateDown:(bool)downLocal;  {

	if (!downLocal) {			///////// going UP /////////enable
		Port *enaInPort	= coerceTo(Port, self.enable.connectedTo);
		float valNext = enaInPort.valueGet;
	}
	[super simulateDown:downLocal];
}


#pragma mark  9. 3D Support							// 3D Visualization
  // Actors do not use superclass methods

- (void) boundIntoView:(View *)v; {
	assert(![v.name isEqualToString:self.brain.breakAtBoundOf], (@"breakAtBoundOf view %@", self.brain.breakAtBoundOf));

	   //// 1. BOUND all, in order of v.superViews (==self.parts):  DO NOT PLACE
			// EVIdence (if it exists)
			// ... other entries ...
			// CONtext (if it exists)
	for (View *subView in v.subViews) {			// Subviews:
		Part *subElt = subView.part;
		if (subView.isAtomic)
			[subElt boundAsAtomIntoView:subView];	// bound subView
		else {
			[subElt suggestedSpinInView:subView];	// spin is part of bound
XR			[subElt boundIntoView:subView];			// bound subView
		}
	}
	v.bounds = nanBounds3f;			// initially nil, to occupy elt 0's origin

	  // 2. PLACE   (CONtext FIRST, at the BOTTOM,
	 // so the assimilated content can be better placed
	if (self.con and self.positionViaCon) {
		assert(v.subViews.count>0, (@"why is't there an element in v?"));
		View *conView = v.subViews[v.subViews.count-1];
		assert(conView.part==self.con, (@"why not?") );
		[self.con placeIntoView:conView];		// should be first
	}
	v.bounds = nanBounds3f;					// remove con from self, but it's still placed

	  // 3. Scan subViews: begin<< [evi], . .. ., [con] >>end
	 // and insure 2..n-1 were at least gardenSize
	Bounds3f eviGardenBounds = nanBounds3f;	// empty
	for (View *subView in v.subViews) {	// Subviews:
		Part    *subElt = subView.part;
		Bundle *subPart = coerceTo(Bundle, subElt);

		  ///// Two special Cases:
		 /// CONtext    (this is processed SECOND)
		if (subPart == self.con/* and self.positionViaCon*/) {
			if (!isNan(eviGardenBounds))	// if already set up 
				v.bounds |= eviGardenBounds;	// lay gardenSize upon v.bounds
		}

XR		[subElt placeIntoView:subView];			// place subView, so we can get our

		  /// EVIdence  (this is processed FIRST)
		 // Now place the garden on top EVIdence
		if (subPart == self.evi/* and self.positionViaCon*/) {
			 // lay gardenSize upon v.bounds
			Vector3f gardenCenter = (v.bounds.size() + self.minSize)/2.0;
			Bounds3f gardenBounds(gardenCenter, self.minSize);
			 // eviGardenBounds includes evi and gardenBounds:
			assert(isNan(eviGardenBounds), (@""));
			eviGardenBounds = v.bounds | gardenBounds;
		}
	}
	if (isNan(v.bounds))			// NO NON-Cohort subviews
		v.bounds = zeroBounds3f;		// use our un-gapped size as zero
}


#pragma mark 11. 3D Display
//// // // // // // // // // // // // // // // // // // // // // // // ///
/// // // // // // // // // // 3D Display  // // // // // // // // // // /
// // // // // // // // // // // // // // // // // // // // // // // // //

- (void) drawFullView:(View *)v context:(RenderingContext *)rc; {
	rc.color = colorRed3;//colorOrange;//Grey9;
	float r = 0.1;//brain.portRadius;
	Vector3f siz = v.bounds.size();
	siz.y = r;

	Vector3f ctr = v.bounds.center();
	siz = v.bounds.size();
	for (int k=0; k<3; k++) {		// 3 Dimensions
		for (int i=0; i<4; i++) {		// 4 corners
			Vector3f ctr = v.bounds.center();
			siz = v.bounds.size();
			switch (k) {
				case 0: ctr.x=i&1? v.bounds.x0: v.bounds.x1; siz.x=siz.z=0.2;
						ctr.z=i&2? v.bounds.z0: v.bounds.z1;		break;
				case 1: ctr.x=i&1? v.bounds.x0: v.bounds.x1; siz.x=siz.y=0.2;
						ctr.y=i&2? v.bounds.y0: v.bounds.y1;		break;
				case 2: ctr.y=i&1? v.bounds.y0: v.bounds.y1; siz.y=siz.z=0.2;	
						ctr.z=i&2? v.bounds.z0: v.bounds.z1;		break;
			}
			if (k != 0)
				siz.y *= 3;			// for easier picking
			glPushMatrix();
				glTranslatefv(ctr);
				glScalefv(siz);
				glutSolidCube(1.0);
			glPopMatrix();
		}
	}
	[super drawFullView:v context:rc];	// paint Ports and bounding box
}

#pragma mark 13. IBActions							// [[shift around]]
//guiVector3fAccessors4(gardenSize, GardenSize, _gardenSize);

//- (IBAction) atomicStateAction:(NSMenuItem *)sender {
//	panic(@"\n\n ***** I'm here\n\n\n");
//}

#pragma mark 15. PrettyPrint

- (NSString *) pp1line:aux; {		id rv=[super pp1line:aux];

	if (self.evi)
		rv=[rv addF:@"evi:'%@' ", self.evi.name];
	if (self.con)
		rv=[rv addF:@"con:'%@' ", self.con.name];

	if (self.previousClocks.count) {
		id separator = @"";
		rv=[rv addF:@"prevClks("];
		for (Part *prevClockClient in self.previousClocks) {
			rv=[rv addF:@"%@%@", separator, prevClockClient.name];
			separator=@",";
		}
		rv=[rv addF:@") "];
	}
	return rv;
}

- (void) printContents; {
	id allNames = @[].anotherMutable;
	for (Part *element in self.parts)
		if (Splitter *se = coerceTo(Splitter, element))
			[se printContentsGather:true names:allNames];

	 // alphebetize all the names found
	allNames = [allNames sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];

	for (Part *element in self.parts)
		if (Splitter *se = coerceTo(Splitter, element)) {
			id contents = [se printContentsGather:false names:allNames];
			NSPrintf(@"%@\n", contents);
		}
}


@end
