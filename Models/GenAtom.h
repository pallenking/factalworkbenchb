// GenAtom.h -- an Atom to connect to Generators C2014PAK

#import "Atom.h"

@interface GenAtom : Atom

    @property (nonatomic, assign) float		value;
    @property (nonatomic, assign) char		loop;
    	// loop!=0 ==> always loop P.in-->P.L
		// loop==0 ==> loop P.in-->P.L if Port LOOP.in>0.5

/*  Shown rightside up, but usually installed upside down

	  Ports:
		LOOP   	     LOOP
				   ----o----
			  .---|L|     in|---.
			  [	          /     ]
			  [	     loop/      ]
gen:		  [			/       ]
			  [	   .->-x-->. val]
			  [    |       | /  ]
			  '---|in     |L|---'
				   ----o----
		 P			   P				        --> slider/button

   		 P	           P
                   ----o----
 				  |L|     in|

 */

@end
