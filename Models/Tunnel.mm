//  Tunnel.mm -- A Bundle which forms entrance/exit of a tunnel C150529PAK

#import "Tunnel.h"
#import "Brain.h"
#import "Port.h"

#import "NSCommon.h"
#import "Common.h"
#import "View.h"
#import <GLUT/glut.h>
#import "GlCommon.h"
#import "FactalWorkbench.h"

@implementation Tunnel

#pragma mark 4. Factory
Tunnel *aTunnel(id etc) {
	id info = @{@"isa":@"tunnel", etcEtc};
	return mustBe(Tunnel, aBundle(info));
}

- (id) build:(id)info;	{

	  /// An "ALL" Port for the tunneled end
	Port *aPort = [self addPart:[Port another]];
	aPort.flipped = true;
	aPort.name = @"ALL";	 // Shouldn't this be "P" ?

XX	[super build:info];
	return self;
}

#pragma mark 9. 3D Support
// \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\    //
//  \\ \\ \\ \\ \\ \\ \\ \\ \ 3D Support\\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\  //
//   \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\//
- (Bounds3f) gapAround:(View *)v; {
	Brain *b = self.brain;
	if (v.heightLeaf == 0)
		 // OUTTERMOST BUNDLE: long cable
		return Bounds3f(-b.bundleRadius, -b.bundleHeight, -b.bundleRadius,
						 b.bundleRadius, 0,                b.bundleRadius);
	else
		 // INNER BUNDLE: short extension
		return Bounds3f(-b.bundleRadius, -b.bitHeight,    -b.bundleRadius,
						 b.bundleRadius, 0,				   b.bundleRadius);
	return nanBounds3f;
}

#pragma mark 11. 3D Display
//// // // // // // // // // // // // // // // // // // // // // // // ///
/// // // // // // // // // // 3D Display  // // // // // // // // // // /
// // // // // // // // // // // // // // // // // // // // // // // // //

   //////////////////////////////
  /// CONCEPT1: Bounding boxes stack,
 ///    with the appearance of "included cables" made by drawing cones.
- (void) drawFullView:(View *)v context:(RenderingContext *)rc; {
	Brain *b				= self.brain;
	float bitRadius			= b.bitRadius;
	rc.color				= [self colorOf:v.heightLeaf/(v.heightTree+0.0)];

	  //////////////////////////////////
	 /// CONCEPT2: Carve out "Steve Jobs" bezels from the corners
	glPushMatrix();
		float	 height		= bitRadius;
		Vector3f topArea	= v.bounds.size()/2;
		Vector3f bottomArea	= topArea - Vector3f(0.3, 0, 0.3);// put a slight taper in it
		Vector3f bottomCtr	= v.bounds.centerYBottom();

		float	 topRadius	= bitRadius * (v.heightTree - v.heightLeaf + 1.5);
		float bottomRadius	= topRadius;			// unless outtermost

		 // The outtermost Bundle has a longer taper to its primary Port
		if (v.heightLeaf == 0) {
			float hPlug		= 1;
			bottomCtr.y	   += hPlug;				// move up over Primary Plug
			height			= b.bundleHeight - hPlug;
			bottomArea		= Vector3f(1, 0, 1);	// Area of Bottom Primary Plug
			bottomRadius	= bitRadius;
		}

		glTranslatefv(bottomCtr);

		myGlBezeledWedge3f(16, height, true, topArea,topRadius,  bottomArea,bottomRadius);
	glPopMatrix();

	[super drawFullView:v context:rc];
}

- (FColor) colorOf:(float)ratio; {
	static FColor inside=  {1.0, 0.9, 0.9,  1},  outside=  {0.65,0.0, 0.0,  1};//161217
//	static FColor inside=  {1.0, 0.9, 0.9,  1},  outside=  {1.0, 0.0, 0.0,  1};//161217
//	static FColor inside=  {1.0, 0.9, 0.9,  1},  outside=  {0.9, 1.0, 0.9,  1};
//	static FColor inside=  {0.8, 0.7, 0.7,  1},  outside=  {0.6, 0.5, 0.5,  1};
//	static FColor inside=  {1.0, 0.9, 0.9,  1},  outside=  {0.9, 1.0, 0.9,  1};
//	static FColor inside=  colorBrownB,  outside=  colorBrownB;
//	static FColor inside=  colorBrownD,  outside=  colorBrownE;
//	static FColor inside=  colorBrownE,  outside=  colorBrownB;
	return lerp(inside, outside, ratio);
}

/* to do
	1. FColor wallColor = colorBlack
	2. bundleRadius
*/
@end
