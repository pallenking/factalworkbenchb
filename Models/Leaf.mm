// Leaf.mm -- each functional port of a Bundle C2015PAK

#import "Leaf.h"
#import "Path.h"
#import "Id+Info.h"
#import "Brain.h"
#import "Port.h"
#import "Atom.h"
#import "GenAtom.h"
#import "SoundAtom.h"
#import "View.h"

#import <GLUT/glut.h>
#import "common.h"
#import "GlCommon.h"
#import "NSCommon.h"
#import "FactalWorkbench.h"

@implementation Leaf

#pragma mark 1. Basic Object Level
- init; {											self = [super init];

	self.placeParts = str2placeType(@"link");
	self.bindings	= @{}.anotherMutable;	// space for my parts (as yet unborn)
	return self;
}

- (void) dealloc; {
	self.type		= nil;
    self.bindings	= nil;
    self.hook		= nil;

    [super dealloc];
}
- (NSString *) bindingsPp; {	return self.bindings.pp; }

#pragma mark  3. Deep Access

- (id) deepMutableCopy; {		Leaf *rv = [super deepMutableCopy];

	rv.type			= [self.type deepMutableCopy];
	rv.bindings		= [self.bindings deepMutableCopy];
	return rv;
}


#pragma mark  4a Factory Access						// Methods defined by Factory
- (id) atomsDefinedPorts;	{				id rv = [super atomsDefinedPorts];

	return rv;
}

#pragma mark 4. Factory
Leaf *aLeaf(NSString *type, NSDictionary *bindings, NSArray *parts, id etc) {
	id info = @{etcEtc}.anotherMutable;
	if (type)
		info[@"type"]		= type;
	if (bindings)
		info[@"bindings"]	= bindings;
	if (parts)
		info[@"parts"]		= parts;
	return aLeaf(info);
}
Leaf *aLeaf(id etc) {
	id info = flattenFromInto(etc, 0);
	Leaf *newbie = anotherBasicPart([Leaf class]);
XX	newbie = [newbie build:info];
	return newbie;
}

- (id) build:(id)info; {		[super build:info];

//!	xzzy1 Leaf::		1. type				:id?
//!	xzzy1 Leaf::		2. bindings			:<hash>

	if (id type = [self takeParameter:@"type"])	// helps debug
		self.type = type;
	if (id bindings = [self takeParameter:@"bindings"])
		self.bindings = bindings;
	return self;
}
- (bool) applyProp:prop withVal:val; {

	 // Leafs get sound names from the .nib file:
	if ([prop isEqualToString:@"sound"]) {	// e.g. "sound:di-sound" or
		if (coerceTo(NSString, val)) {
			Port *sndPPort		= [self port4leafBinding:@"SND"];
			SoundAtom *sndAtom	= mustBe(SoundAtom, sndPPort.atom);
			sndAtom.sound		= val;
		}
		else
			panic(@"sound's val must be string");

		return true;							// found a spin property
	}

XR	return [super applyProp:prop withVal:val];
}


- (Part *) resolveInwardReference:(Path *)path openingDown:(bool)downInSelf except:except; {
	mustBe(Path, path);

	 // path matches self's name
	if ([path atomNameMatches:self]) {		// Terminal's name (w.o. Port) matches
		Part *rv = nil;

		  //////////// Is named port a BINDING? ///////////////////
		 //
		if (id bindingStr = self.bindings[path.namePort]) {
			Path *bindingPath = [Path pathWithName:bindingStr];		// Look inside Leaf
			if (self.brain.logTerminalSearching) {
				[[@"" addF:@"   MATCHES Inward check as Leaf '%@'", self.fullName] ppLog];
				[[@"" addF:@"   Search inward for binding[%@]->'%@'",
												path.namePort, [bindingPath pp]] ppLog];
			}
			 // Look that name up
//XX		rv=[self resolveInwardReference:bindingPath openingDown:downInSelf except:nil]
			for (Part *elt in self.parts) {
				if (coerceTo(NSNumber, elt))
					continue;

				 // Look up internal name
				bool downInElt = !downInSelf ^ !elt.flipped;
XX				if ((rv=[elt resolveInwardReference:bindingPath openingDown:downInSelf except:nil]))
//XX 			if ((rv=[elt resolveInwardReference:bindingPath openingDown:downInElt except:nil]))
						break;					// found
			}
		}
		else if (path.namePort.length == 0) 	// empty string "" in bindings has priority
			panic(@"wtf");
//			rv = self;								// found	(why?)

		if (rv) {
			if (self.brain.logTerminalSearching)
				[[@"" addF:@"   MATCHES Inward check as Leaf '%@'", self.fullName] ppLog];
			return rv;
		}
	}
	if (self.brain.logTerminalSearching)
		[[@"" addF:@"   FAILS   Inward check as Leaf '%@'", self.fullName] ppLog];

	 // Didn't match as Leaf, try normal match:
	return [super resolveInwardReference:path openingDown:downInSelf except:except];
}

#pragma mark 6. Navigation
- (void) forAllLeafs:(LeafOperation)leafOperation  {
	leafOperation(self);
}

- (id) port4leafBinding:(id)name; {
	id binding = self.bindings[name];
	if (NSString *refStr = coerceTo(NSString, binding)) {
		Path *refPS = [Path pathWithName:refStr];
XX		id rv = [self resolveInwardReference:refPS openingDown:false except:nil];
		return rv;
	}
	else if (NSNumber *refNumber = coerceTo(NSNumber, binding))
		assert([refNumber intValue]==0, (@"@0 is ignore, others not permitted"));
	
	else if (Port *refPort = coerceTo(Port, binding))
		panic(@"oops");
	
	return nil;			// not found
}

#pragma mark 9. 3D Support

- (Bounds3f) gapAround:(View *)v; {
		return Bounds3f( -0.5,  0, -0.5,
						  0.5,  0,  0.5 );
}


#pragma mark 11. 3D Display

- (void) drawFullView:(View *)v context:(RenderingContext *)rc; {
	rc.color = colorBrown; //161007: was colorBrownC;

	 // Display Leaf name:
	id display = [self takeParameter:@"displayLeafNames"];//parameterInherited
	if ([coerceTo(NSNumber, display) intValue]) {
		id foo = [self takeParameter:@"displayLeafNames"];//parameterInherited
		glPushMatrix();
			glTranslatefv(v.bounds.centerYTop() + Vector3f(0.0, 0.1, 0.0));
			Vector3f inCameraShift = Vector3f(0.0, 0.0, /*6*/1.0);	// in "facing camera" coordinates
			Vector2f spot2labelCorner = Vector2f(0.5, 0.5);	// box center --> llc
			rc.color = colorBlue4;
			myGlDrawString(rc, self.name, -1, inCameraShift, spot2labelCorner, 0);
		glPopMatrix();
	}

	[super drawFullView:v context:rc];
}

#pragma mark 15. PrettyPrint

- (NSString *) pp1line:aux; {		id rv = [super pp1line:aux];

	if (self.type)
		rv=[rv addF:@"<a%@()> ", mustBe(NSString, self.type.capitalCammel)];
	else
		rv=[rv addF:@"<Nil Type> "];

//	NSString *s = self.type?: @"Nil Type";
//	rv=[rv addF:@"<a%@()> ", mustBe(NSString, s.capitalCammel)];
//
//	rv=[rv addF:@"%@", self.type? mustBe(NSString, s.capitalCammel) :@"<nil type>"];

//	if (NSString *type = self.type)
//		if (mustBe(NSString, type))
//			rv=[rv addF:@"<a%@()> ", type.capitalCammel];
//	else
//			rv=[rv addF:@"<Nil Type> "];

//	rv=[rv addF:@"bindings=%@", self.bindings.pp];
	return rv;
}

@end
