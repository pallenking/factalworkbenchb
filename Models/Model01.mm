//  Model01.mm -- define user Factal Workbench objects  C2014PAK

#import "Model01.h"

#import "Bundle.h"
#import "Tunnel.h"
#import "Brain.h"
#import "Common.h"
#import "NSCommon.h"
#import "Id+Info.h"
#import "FactalWorkbench.h"

// DEFINE HaveNWant networks   MACRO: 	Named		Unnamed
//	A Test registered in Menue:			M(n,c)		m(c), 		n=name
//  A Regression Test					R(n,c)		r(c), 		c=content
//
// Status:		Prepend:	Examples:
//	Idle		<nothing>	  M(n,c),  m(c),  R(n,c)	Other tests
//	Selected	x			 xM(n,c), xm(c), xR(n,c)	One single test
//	Idle		xx			xxM(n,c),xxm(c),xxR(n,c)	Other tests

/* BUGS 171207
1.	when setting GenAtom.val, it isn't propigated to the .nib

 */
id testLibrary() {

///////////////////// Test Environment begins: ////////////////////


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
#pragma mark - Simple Unit Test
/************************/ setParentMenu(@"Simple Units"); /***************/
////////////////////////////////////////////////
/////     qa simple first test              /////
////////////////////////////////////////////////

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
#pragma mark * Unit Test Splitters

R(ABcast,	(@{CameraHsuz(0, 0, 0, 0.8), partsStackY,				@"parts":@[
			o(a,		aMinAnd( 0, 0)),
																		]}	));
 // positioning by wiring
R(Broadcast,	(@{CameraHsuz(0, 45, 0, 1), @"boundsDrapes":@.1,@"fluffLinkGap ":@4.0, @"parts":@[
			o(a,		aBroadcast( 0, 0)),
			o(b,		aBroadcast(@[@"a"],	0)),
//			o(c,		aBroadcast(@[@"a"],	0)),
																		]}	));



 // positioning by wiring
R(test kind,	(@{CameraHsuz(0, 0, 0, 1), @"boundsDrapes":@.1, partsStackX,
				@"speak":@0, @" fluffLinkGap":@4.0, @"parts":@[
			aBroadcast(0, 0, @{@"Share":@[@"MaxOr", @"Broadcast"],
							   @"KIND":@"speak", @"SPEAK":@"speak"}),
																		]}	));

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 // auto Broadcast
#pragma mark * Unit auto-broadcast
R(UMin auto Broadcast, (@{CameraHsuz(0, 0, 0, 1), @"boundsDrapes":@.1,@"parts":@[
			o(a,		aBroadcast(0, 0,						@{flip} )),
			o(b,		aMinAnd(@[@"a"],	0)),
			o(c,		aBroadcast(@[@"a"],	0)),
																		]}	));
 // 160123 ups bug; good 160822
r(			(@{CameraHsuz(0, 0, 0, 0.8), @"boundsDrapes":@.1,		@"parts":@[
		o(bun1,	aNet(@{partsStackX}, @[
			o(x,	aMaxOr(0, @"qb")),
			o(y,	aMaxOr(0, @"qb")),
										])),
			o(qb,	aMinAnd(0, 0)),
																		]}	));
r(			(@{CameraHsuz(0, 0, 0, 0.8), @"boundsDrapes":@.1,		@"parts":@[
		aGenBcastLeaf(),
																		]}	));
r(			(@{CameraHsuz(0, 0, 0, 0.8), @"boundsDrapes":@.1,		@"parts":@[
		aGenAtom(0),
																		]}	));

 // 160413 testing invisible links
/// 170418PAK: BUG positioning
R(invisible links,	(@{CameraHsuz(0, 0, 0, 1), @"boundsDrapes":@.1,
					   @"fluffLinkGap ":@4.0, @"displayAtomNames":@1,@"parts":@[
	o(a,		aBroadcast( 0, 0)),
	o(x,		aBroadcast(@[@"a@"],	0)),
	o(y,		aBroadcast(@[@"a"],	0)),
	/*  x: y: 170418
		a  a		// a visLink2 b, a visLink2 c			GOOD
		a@ a		// height bug (dy = 0.5)				BUG1
		a@ a@		// c is positioned above b, positioned above a
		a  a@		// c is positioned above b, visLink2 a	BUG2
	 */
																		]})	);
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 // 160512 bug invisible "@" FIXED
R(test invisible links,  (@{CameraHsuz(0, 90, 0, 1), @"boundsDrapes ":@"0.1",@"displayAxis ":@1,
							@"l a t e C lkPrevious":@1,@"displayAtomNames":@1,	@"parts":@[
	o(a,		aBroadcast(0,0)),
	o(x,		aBroadcast(0,0, @{@"P": @"a@" })),
	o(y,		aBroadcast(0,0, @{@"P" : @"a@"})),
	o(z,		aBroadcast(0,0, @{@"P" : @"a"})),
//	o(y,		aBroadcast(0,0, @{@"P@":@"a"})),
//	o(z,		aBroadcast(0,0, @{@"P": @"a@"})),

	/*  x: y:  z:  170418
		a  a   a	//	a << (x, y, z)
	   @a@ a   a	//	a << (x, y, z) AND (x < (y, z))
		a@ a@  a	//	a < x < y, a << z
		a  a@  a	//	a << z, a << x < y
		a  a   a@	//
		a@ a   a@	//
		a@ a@  a@	//
		a  a@  a@	//
*/
																				]}	));
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 // Display many spliter bodies at once
#pragma mark * Unit Splitter Skins
M(splitter skins, (@{CameraHsuz(0, 20, 10, 1), partsStackX,			@"parts":@[
		  o(spin,  aNet(@{partsStackZ}, @[
			o(a,		aBroadcast	(0, 0)),
			o(c,		aMaxOr		(0, 0)),
									])),
		  o(spin,  aNet(@{partsStackZ}, @[
			o(d,		aMinAnd		(0, 0)),
			o(e,		aHamming	(0, 0)),
			o(f,		aMultiply	(0, 0)),
									])),
		  o(spin,  aNet(@{partsStackZ}, @[
			o(g,		aSequence	(0, 0)),
			o(h,		aKNorm		(0, 0)),
									])),
																		]}	));
#pragma mark * Bulb / Branch sizing
M(Bulb / Branch Sizing, (@{@"boundsDrapes":@"0.1", @"parts":@[
	    aGenerator(@{n(morse), @"nib":@"LoGen_ab", @"resetTo":@[@"a=0.5"], flip, @"P":@"act1/evi"}),
		anActor(@{n(act1), @"minHeight":@15,
			@"evi":aTunnel(@{@"structure":@[@"a", @"b"], @"proto":aGenLeaf() }),
		    @"parts":@[
		    	aBranch(@{@"S":@"a", @"M":@"a", @"Share":@"Bulb"}),
				aBulb(@"a"),
			], }),
															]} ) )
 // resize Bulb on blink
R(Rlink2, 	(@{CameraHsuz(0, 0, 0, 1), VEL(-4), @"boundsDrapes":@"0.1", @"parts":@[
	aGenerator(0, @{@"nib":@"LoGen_ab", flip, @"P":@"bun1"}),
	aBundle(@{n(bun1), @"structure":ab_bundle, @"proto":aGenBulbLeaf()}),
//	aSplitterLeaf(@{n(b), @"Share":@"Bulb", @"P":@"a", }),
	aBroadcast(0, @[@"a", @"b"], @{flip}),	//
//	aBranchLeaf(@{@"Share":@"Bulb", @"xS":@"a", @"noMInSpeak":@1, }),
										]}) );
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark * Unit Family Portrait
M(splitter skins, (@{CameraHsuz(0, 45, 10, 1), partsStackX, @"boundsDrapes":@.1, @"parts":@[
			aMirror		(0, 0),
//			aLink		(0, 0),
			aRotator	(0, 0),
			aModulator	(0, 0),
			aPrevious	(0, @{spin$L}),
			aTunnel		(@{@"structure":abc_bundle, @"proto":aGenBcastLeaf(), spin$L}),
																		]}	));
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
#pragma mark * Unit Branch Skins
M(Branch Skins,	(@{CameraHsuz(0, 0, 0, 0.8), @"boundsDrapes":@.1,	@"parts":@[
		o(bun,	aBundle(@{@"structure":a_bundle, @"proto":aBcastLeaf() })),
		o(bra,	aBranch(@{@"S":@"a", @"M":@"a"})),
																		]}	));
M(Branch Skins,	(@{CameraHsuz(0, 0, 0, 0.8),	@"parts":@[
		o(bra,	aBranch()),
																		]}	));
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 // 160223 bug in placement of y33 FIXED
#pragma mark * Unit Rot/Prev/Kno/Mod: Skin Spins
M(allSpun,(@{CameraHsuz(0, 30, 20, 1.8), @" XXdisplayLeafNames":@0,	@"parts":@[
				aGenerator(-1, @{@"prob":@0.5, flip, @"P":@"bun"}),
		o(bun,	aBundle(@{@"structure":y4x4_bundle_(@"minSize:30,0,30", spin_R), @"proto":aGenBcastLeaf() })),
		o(y00,	aRotator(	@"y0x0",						@{spin$0})),
		o(y01,	aRotator(	@"y0x1",						@{spin$R})),
		o(y02,	aRotator(	@"y0x2",						@{spin$2})),
		o(y03,	aRotator(	@"y0x3",						@{spin$L})),

		o(y10,	aPrevious(	@"y1x0",						@{spin$0})),
		o(y11,	aPrevious(	@"y1x1",						@{spin$R})),
		o(y12,	aPrevious(	@"y1x2",						@{spin$2})),
		o(y13,	aPrevious(	@"y1x3",						@{spin$L})),

		o(y20,	aKnown(		@"y2x0",						@{spin$0})),
		o(y21,	aKnown(		@"y2x1",						@{spin$R})),
		o(y22,	aKnown(		@"y2x2",						@{spin$2})),
		o(y23,	aKnown(		@"y2x3",						@{spin$L})),

		o(y30,	aModulator(	@"y3x0",						@{spin$0})),
		o(y31,	aModulator(	@"y3x1",						@{spin$R})),
		o(y32,	aModulator(	@"y3x2",						@{spin$2})),
		o(y33,	aModulator(	@"y3x3",						@{spin$L})),
																		]}	));
 // 1700813 BUG Should generate n>1 events
r(			(@{CameraHsuz(0, 30, 20, 1), VEL(-7),					@"parts":@[
				aGenerator(-1,@{n(lo), @"events":@[@"a", @[], @"again"], flip, @"P":@"bun"}),
		o(bun,	aBundle(@{@"structure":a_bundle, @"proto":aGenBcastLeaf() })),
		o(aMod,	aModulator(	@"a",							@{spin$L})),
																		]}	));
 // 170712 Dangling parts
r(			(@{CameraHsuz(0, 30, 20, 1),							@"parts":@[
			   aGenerator(@{n(lo), @"prob":@0.5, flip, @"P":@"bun"}),
			   aBundle(@{n(bun)}),
						   ]}	));
M(allSpun,	(@{CameraHsuz(0, 0, 0, 1),								@"parts":@[
				aGenerator(@{n(lo), @"prob":@0.5, flip, @"P":@"bun"}),
		o(bun,	aTunnel(@{@"structure":ab_bundle, @"proto":aGenBcastLeaf() })),
//		o(bun,	aTunnel(ab_bundle, aGenBcastLeaf())),
		o(u,	aMaxOr(0, @[@"a", @"b"],					@{flip})),
																		]}	));
 // 160214 ensure shares always overlap splitter edge
R(allSpun,	(@{CameraHsuz(0, 0, 0, 1),								@"parts":@[
		o(bun,	aBundle(@{@"structure":a_bundle, @"proto":aGenBcastLeaf() })),
//		o(bun,	aBundle(@{@"structure":a_bundle, aGenBcastLeaf())),
//		o(u,	aMaxOr(0, @[@"a", @"b"],					@{flip})),
																		]}	));
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark * Unit Test: genAtom
R(Unit Test genAtom,(@{CameraHsuz(0, 0, 0, 1),	@"boundsDrapes":@.1,@"parts":@[
	o(didat,	aBundle(@{@"structure":a_bundle, @"proto":aBcastLeaf() })),
//	o(didat,	aBundle(a_bundle, aGenBcastLeaf())),
																		]}	));
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark * Unit Test: Branch
//#warning 160928 BUG: Why must loop=0?
R(new branch,  (@{CameraHsuz(0, 45, 10, 1), @" boundsDrapes":@"0.1",VEL(-4),@"parts":@[	//(0, 0, 0, 0.8)
				aGenerator(@{n(lo), @"events":@[@"d_t", @"d_a", @"d_t",@"d_i",@[], @"again"],
									@"asyncData":@1, flip, @"P":@"evi"}),			//xyzzy11b either
	o(evi,		aTunnel(@{@"structure":didat_bundle, @"proto":aGenBcastLeaf(@{@"loop":@0}) })),
	o(_t,		aBranch(@{@"S":@"d_t", @"Share":@"MaxOr"})),
	o(_ti,		aBranch(@{@"S":@"_t", @"M":@"d_i", spin$2})),//
	o(_ta,		aBranch(@{@"S":@"_t", @"M":@"d_a", spin$2})),
																		]}	));
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark * Unit Test: Sequence
 // test didat button UPWARD
R(test aSequence,(@{CameraHsuz(0, 45, 10, 1), 	@"parts":@[
	aSequence(),
																		]}	));
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark * Unit Test: Ago
 // test didat button UPWARD
R(test anAgo,(@{CameraHsuz(0, 45, 10, 1), 	@"parts":@[
	aGenerator(@{n(lo), @"nib":@"LoGen_morse", @"XasyncData":@1 , flip, @"P":@"didat"}),
	aTunnel(@{@"structure":didat_sound_bundle, @"proto":aGenSoundBcastLeaf(), n(didat)}), //didat_bundle
	anAgo(@"d_i"),
	anAgo(@"d_a"),
	anAgo(@"d_t"),
																		]}	));
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark * Unit Test Broadcast
 // 160905 .B bug
R(test bcast Port,  (@{CameraHsuz(.5, 20, 0, 1), @"boundsDrapes":@0.1, @"displayAxis ":@1, @"parts":@[
		o(main,	aBroadcast(	0,			0,			@{})),
		o(user,	aBroadcast(@"main.B",	0,			@{})),
																		]}	));
// 160903 - 'B' ports
R(test B/U ports,  (@{CameraHsuz(.5, 45, 10, 1.5), @"boundsDrapes":@0.1, @"displayAxis ":@1, // (0, 35, 10, 1)
						@"l a t e C lkPrevious":@1,						@"parts":@[
	anActor(@{n(act1),
		@"evi":aTunnel(@{@"structure":a_bundle, @"proto":aGenBcastLeaf(0,@{@"B":@"y", @"U":@"y"}) }),
		@"parts":@[
		o(y, aBroadcast(0, @[@"a"],				@{flip} )),
		  ],
	}),
	
																		]}	));
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark * Unit Test stopIfNotLevel()
id varBundle0 = @[ ];
id varBundle  = @[ @"a" ];
id varBundle2 = @[ @1,  @"a" ];
id varBundle3 = @[ @0,  @"a" ];
id varBundle1 = @[ @"a", @1,  @"b", @2,  @"c", @3,  @"d", @4,  @"e"];
float minHeight = 6;

M(stopIfNotLevel, (@{CameraHsuz(0, 0, 0, 1), VEL(-8),				@"parts":@[
	aTunnel(@{@"structure":varBundle1, @"proto":aGenBcastLeaf(), n(didat)}),
//				aMirror(@0),		stopIfNotLevel(1),
//				aMirror(@0),		stopIfNotLevel(2),
//				aMirror(@0),		stopIfNotLevel(3),
//				aMirror(@0),		stopIfNotLevel(4),
//				aMirror(@0),
																		]}	));

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark * Unit Test  Link
 // 160831 making links longer
r( (@{CameraHsuz(3, 0, 10, 1.2), VEL(-5),								@"parts":@[
				aGenerator(@{n(lo), @"events":@[@"d_t", @[], @"again"], flip, @"P":@"didat"}),
	o(didat,	aTunnel(@{@"structure":t_bundle, @"proto":aGenBcastLeaf(@{@"loop":@1}) })),
//	o(seq1,		aSequence(@0, @[@"d_t"],@{flip} )), //d_t:len:4
				aMirror(@"d_t"),
																		]}	));
r( (@{CameraHsuz(0, 0, 0, 1), VEL(-7),								@"parts":@[
				aMirror(),
																		]}	));
id bun = @[@"a", @"b", @"c"];
R(linkRotateBug,(@{CameraHsuz(0, 10, 10, 2), VEL(-5), @"boundsDrapes":@.1, @"lineRadius":@0.0, @"parts":@[
	o(bun,	aBundle(@{@"structure":bun, @"proto":aGenBcastLeaf(@{@"value":@1})})),
	o(d,	aBroadcast(0, bun, @{flip, @"jog":@"0,0,0"})),
			aMirror(@"d"),
																		]}	));

//// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// // 160512 bug invisible "@" FIXED
//R(test invisible links,  (@{CameraHsuz(0, 90, 0, 1), @"boundsDrapes ":@"0.1",@"displayAxis ":@1,
//							@"l a t e C lkPrevious":@1,@"displayAtomNames":@1,	@"parts":@[
//	o(aaa,			aBroadcast(0,0)),
//	o(vis,			aBroadcast(0,0, @{@"P": @"aaa" })),
//	o(invis-at,		aBroadcast(0,0, @{@"P@":@"aaa"})),
//	o(invis-at,		aBroadcast(0,0, @{@"P": @"aaa@"})),
//																		]}	));
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark * Unit Test  ParameterPorts
 // 170123 Creation of a ParameterPort
R(test ParameterPorts,  (@{CameraHsuz(0, 90, 0, 1), @"aParam":@0.55,@"parts":@[
	o(a,		aBroadcast(@"aParam", 0)),
																		]}	));
	
R(test ParameterPorts,  (@{CameraHsuz(0, 90, 0, 1), @"speak":@0,	@"parts":@[
    o(speakQ,	aBroadcast()),
	o(base,		aBranch(@{@"SPEAK":@"speak"})),
								   ]}	));

R(Extending P@: to xx,  (@{CameraHsuz(1, -55, 5, 1.8), @"l a t e C lkPrevious":@1, @"parts":@[
    o(a1,		aBroadcast()),
   //o(a2,		aBroadcast()),
    o(base,		aBranch(@{@"S":@"a1", @"U@":@"a1"})),
							   ]}	));

	
/************************/ setParentMenu(@"Hash Bundles"); /***************/
// to test out Hash environment
id (^protos)() = ^id () { return @[aBcastLeaf(), aPrevLeaf(), ]; }; // @[aGenMaxLeaf(), aGenPrevLeaf(), ]


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark * Unit Actors
R(testing Actors,  (@{CameraHsuz(1, 0, 15, 1.0), @"l a t e C lkPrevious":@1, @"fontNumber":@4, @"parts":@[
	anActor(@{n(act1),
		@"con":aTunnel(@{@"structure":@[@"y", @"z"], @"proto":aGenMaxLeaf(), flip}),
		@"evi":aTunnel(@{@"structure":@[@"a", @"b"]}),
															}	),
																		]}	));


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark - Bundle

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark * Unit Bundle Shapes and Spins
/************************/ setParentMenu(@"Bundle Shapes"); /***************/


M(a, (@{@"parts":@[aTunnel(@{@"structure":@"a", @"proto": @[aBcastLeaf(), aPrevLeaf(), ] })] }) );
M(a, (@{@"parts":@[aTunnel(@{@"structure":@"a", @"proto": @[aPrevLeaf(), ] })] }) );
M(a, (@{@"parts":@[aTunnel(@{@"structure":@"a", @"proto":aPrevLeaf() })] }) );
M(a, (@{@"parts":@[aTunnel(@{@"structure":@"a", @"proto":aLeaf(@"prev", nil, @[aPrevious()], nil	) })] }) );
M(a, (@{@"parts":@[aBundle(@{@"structure":@"a", @"proto":aLeaf(@{@"parts":@[aPrevious()]})})] }) );
// FAILS, the above do.

M(a, (@{@"parts":@[aBundle(@{@"structure":@"a", @"proto":aLeaf(@{@"parts":@[aPrevious()]})})] }) );

// WORKS, those below do:
M(a, (@{@"parts":@[aBundle(@{@"structure":@"a", @"proto":aLeaf(@{@"parts":@[aKnown()]})})] }) );
M(a, (@{@"parts":@[aBundle(@{@"structure":@"a", @"proto":aLeaf(@{@"parts":@[aRotator()]})})] }) );
M(a, (@{@"parts":@[aBundle(@{@"structure":@"a", @"proto":aLeaf(@{@"parts":@[aModulator()]})})] }) );
M(a, (@{@"parts":@[aTunnel(@{@"structure":@"a", @"proto":aLeaf(@{@"parts":@[anAgo()]})})] }) );
M(a, (@{@"parts":@[aTunnel(@{@"structure":@"a", @"proto":aLeaf(@{@"parts":@[aBroadcast()]})}) ] }) );
M(a, (@{@"parts":@[aLeaf(@{@"parts":@[aPrevious()]}) ] }) );
M(a, (@{@"parts":@[aTunnel(@{@"structure":@"a", @"proto": @[aBcastLeaf() ] })] }) );
M(a, (@{@"parts":@[aTunnel(@{@"structure":@"a", @"proto":aBcastLeaf() })] }) );

#define tTst(structure)  (@{CameraHsuz(0, 0, 10, 1), @"fontNumber":@6, \
	@"parts":@[	o(bun,	aTunnel(@{@"structure":structure,  @"proto":protos}))] })

M(0,					tTst( @0											 ) );
M(a,					tTst( @"a"											 ) );
M(a:s0, 				tTst( @"a:s0"										 ) );
M(a:sR, 				tTst( @"a:sR"										 ) );
M(a:spin:2, 			tTst( @"a:spin:2"									 ) );

M([a b],				tTst( (@[@"a", @"b"])								 ) );
M([sL a b],				tTst( (@[@"sL", @"a", @"b"])						 ) );
M([spin:R a b], 		tTst( (@[@"spin:R", @"a", @"b"])					 ) );

M({a:@0},				tTst( (@{@"a":@0}									)) );
M({a:@0 b:@1},			tTst( (@{@"a":@0, @"b":@1}							)) );
 // the following should be illegal, but [@"b" intVal]->@0 quietly
M({a:b},				tTst( (@{@"a":@"b"}			/* ILLEGAL */			)) );
M({a:[b c]},			tTst( (@{@"a":@[@"b", @"c"]}						)) );
//M({sp$2},				tTst( (@{spin$2}			/* ??? */				)) );
M({L a[A]},				tTst( (@{spin$L, @"a":@{@"A":@0}}					)) );
M({spin$2 a},			tTst( (@{spin$2, @"a":@0}							)) );
M({spin$2 a:[b c]},		tTst( (@{spin$2, @"a":@[@"b", @"c"]}				)) );
M({ena:spin:1},			tTst( (@{@"ena:spin:1":@0}							)) );
M({ena:spin:0 a:spin:0},tTst( (@{@"ena:spin:0":@0, @"a:spin:0":@1}			)) );
M({L a:spin:0},			tTst( (@{spin$L, @"a:spin:0":@0}					)) );
M({L a:spin:0 b},		tTst( (@{spin$L, @"a:spin:0":@0, @"b":@0}			)) );
M({R a b c},			tTst( (@{spin$R, @"a":@0, @"b":@0, @"c":@0}			)) );
M([L a[L b ]],			tTst( (@{spin$L, @"a":@{spin$L, @"b":@0}}			)) );//+++
M([L a[L b[L c[L d]]]],	tTst( (@{spin$L, @"a":@{spin$L, @"b":@{spin$L, @"c":@{spin$L, @"d":@0}}}} )) );
M([0 b[R B] c:spin:0],	tTst( (@{spin$0, @"b":@{spin$R, @"B":@0}, @"c:spin_1":@0}				  )) );//+++
M([0 b[L B] c:spin:R],	tTst( (@{spin$0, @"b":@{spin$0, @"B:s0":@0}, @"c:spin_1":@0}				  )) );
M([0 b[L ]],			tTst( (@{spin$0, @"b":@{spin$0}}					)) );

 // 170106 editor doesn't find methods
//M([0 a b[L A:spin:L B:spin:L]],				tTst((
//	@{spin$0, @"a":@0,  @"b":@{spin$L, @"A:spin:1":@0,@"B:spin:1":@0}}		)) );
//M({0 a:0 b:{L B:sR] c],tTst(( @{spin$0, @"a":@0,  @"b":@{spin$L, @"B:spin:R":@0}, @"c":@0}		  )) );
//M([L a b:[0 A:spin:0 B:spin:0 ] c:R ],		tTst((
//	@{spin$L, @"a:spin:R":@0,
//			  @"b":@{spin$0, @"A:spin:0":@0, @"B:spin:0":@0},
//			  @"c:spin:R":@0}												)) );
//M([ a b:[L A B:[R w x ] C ] c ],			tTst((
//	@{spin$0, @"a":@0,
//			  @"b":@{spin$L,
//					@"A:spin:R":@0,
//					@"B":@{spin$R, @"w":@0,
//						@"x":@{spin$L, @"f:spin:R":@0, @"g:spin:R":@0}},
//					@"C:spin:R":@0},
//			  @"c":@0}														)) );
// // 160119 A:spin:R bug fixed 160121
//R([x A ],				tTst((@{  @"A:spin:R":@0}							)) );


M([x a b [x A B [x w x [x f g ] ] ] ],		tTst((
	@{spin$L, @"a":@0,			//
			  @"b":@{spin$L, @"A:sR":@0,
							 @"B":@{spin$L, @"w":@0,
											@"x":@{spin$L, @"f":@0, @"g":@0}}}})) );
M(stux 2x2,				tTst((@{spin$0,
	@"joystick":@[spin_R, @"left", @"right"],
	@"mana":    @[spin_R, @"health", @"hunger"],
	@"retina":	@[spin_R, @[spin_L, @"x0:sR", @"x1:sR"], @[spin_L, @"x2:sR", @"x3:sR"]],
																		 })) );
M(stux 5x5,				tTst((@{spin$0,
	@"joystick":@[spin_R, @"left", @"right", @"jump", @"boost"],
	@"mana":    @[spin_R, @"health", @"hunger"],
	@"retina":	y5x5_bundle,
																		 })) );

 // 160121 bug with
	
R(stux 0, (@{CameraHsuz(0, 0, 10, 1), @"fontNumber":@6, @"parts":@[
  o(bun,	aTunnel(@{@"structure":@{@"r":@[@[@"a", @"b"]],},
					  @"proto":protos}))
  ] }));




 // 160320 BUG? bundle rotations (live with it!)
R(Shaft,(@{CameraHsuz(0, 0, 90, 1), @"boundsDrapes":@.1,			@"parts":@[
//	o(gen,	aBundle(@{@"structure":ab_bundle, @"proto":aGenMaxLeaf(0, @{spin$0}, @{spin$0}), flip} )),
//	o(gen,	aBundle(@{@"structure":ab_bundle, @"proto":aGenMaxLeaf(0, @{},		 @{}),		 flip} )),
//	o(gen,	aBundle(@{@"structure":ab_bundle, @"proto":aGenMaxLeaf(0, @{spin$1}, @{}),		 flip} )),
//	o(gen,	aBundle(@{@"structure":ab_bundle, @"proto":aGenMaxLeaf(0, @{spin$2}, @{}),		 flip} )),
//	o(gen,	aBundle(@{@"structure":ab_bundle, @"proto":aGenMaxLeaf(0, @{spin$0}, @{spin$1}), flip} )),
//	o(gen,	aBundle(@{@"structure":ab_bundle, @"proto":aGenMaxLeaf(0, @{},		 @{spin$1}), flip} )),
//	o(gen,	aBundle(@{@"structure":ab_bundle, @"proto":aGenMaxLeaf(0, @{spin$1}, @{spin$1}), flip} )),
	o(gen,	aBundle(@{@"structure":ab_bundle, @"proto":aGenMaxLeaf(0, @{},		 @{}),		 flip, spin$1} )),	// YEA!!!
																		]}	));
// 160317 BUG?: spin:a*Leaf doesn't spin E (live with it!)
r(	(@{CameraHsuz(-2, 30, 20, 1),									@"parts":@[
	o(gen,	aTunnel(@{@"structure":abc_bundle, @"proto":aGenPrevBcastLeaf(0, @{spin$1}), spin$1} )),
																		]}	));
 // 160406 bug  after folding fixed 160408
id greyCode2 = @[@[@"a"], @[@"a", @"z"], @[@"z"], @[], @"again"];
id greyCode3 = @[@[@"z"], @[], @"again"];
R(Shaft Spin 3,(@{CameraHsuz(0, 0, 0, 1), @"boundsDrapes":@.2, @"displayAxis":@0, @"parts":@[
	aGenerator(@{n(lo), @"events":greyCode2, @"addBundleTap":@"hiTap", flip, @"P":@"wheelA/evi"}),
	anActor(@{n(wheelA), @"minHeight":@0.0,
		@"evi":aTunnel(@{@"structure":a_bundle }),
		@"con":aTunnel(@{@"structure":z_bundle, flip }),
	}),
	aBundleTap(@{n(hiTap), @"P":@"wheelA/con"}),//@"events":@[@"a", @"a", @[], @"again"],
//o(bun,aTunnel(@{@"structure":_bundle, aBcastLeaf(), @{flip})),
//	aGenerator(@[],@{@"P":@"bun"}),
																		]}	));
 // 160407 bug in placement fixed 160408
R(Shaft Spin 3,(@{CameraHsuz(0, 0, 90, 1), @"boundsDrapes":@.1, @"displayAxis":@1, @"parts":@[
o(a,aBroadcast(0, 0)),
	aHamming(0, @[@"a"], @{flip}),
	aHamming(0, @[@"a"], @{flip}),
	aHamming(0, @[@"a"], @{flip}),
	aHamming(0, @[@"a"], @{flip}),
	aHamming(0, @[@"a"], @{flip}),
	aHamming(0, @[@"a"], @{flip}),
																		]}	));
 // 160406 bug in placement fixed 160408
R(Shaft Spin 3,(@{CameraHsuz(0, 0, 90, 1), @"boundsDrapes":@0.1, @"displayAxis":@1, @"parts":@[
o(a,aBroadcast(0, 0)),
	aHamming(0, @[@"a"], @{flip}),
	aHamming(0, @[@"a"], @{flip}),
	aHamming(0, @[@"a"], @{flip}),
																		]}	));



//xR(stux 0,			tTst((@{@"r":@[@[@"a", @"b"]],}						)) );	// buggy, fixed
R(stux 0,				tTst((@[@[@"a", @"b"]]								)) );	// buggy, fixed
#pragma mark * Bundle with Random Data
/************************/ setParentMenu(@"Bundles of Random"); /***************/
//#warning for  u uU
M(ups1-rnd,  (@{CameraHsuz(0, 80, 10, 0.8), VEL(-8),				@"parts":@[
			aGenerator(1, @{@"prob":@0.5, flip, @"P":@"bun"}),//@"events":@[@"a", @[]],
	o(bun,	aTunnel(@{@"structure":a_bundle, @"proto":aGenBcastLeaf() })),
//	o(qa,	aMultiply(0, @[@"a,l=5"],						@{flip} )),
	o(qa,	aMirror(@"a,l=5")),
//	o(qa,	aMaxOr(0, @[@"a,l=5"],						@{flip} )),
																		]}	));
M(ups2-ab_,   (@{CameraHsuz(0, 0, 10, 0.8), VEL(-6), 				@"parts":@[
			aGenerator(@{@"events":@[@[@"a"],@[@"b"],@[],@"again"], flip, @"P":@"bun"}),
//			aGenerator(0,@{@"prob":@0.6, flip, @"P":@"bun"}),
	o(bun,	aTunnel(@{@"structure":ab_bundle, @"proto":aGenBcastLeaf() })),
	o(qa,	aMaxOr(0, @[@"a", @"b"],					@{flip} )),
	o(qb,	aMirror(@"qa")),
																		]}	));

id b2D2_bundle	= @{@"board":y2x2_bundle};
M(2x2-randValue,(@{CameraHsuz(0, 30, 20, 1.3), @"fontNumber":@0,	@"parts":@[
	o(bun,	aBundle(@{@"structure":b2D2_bundle, @"proto":aGenBcastLeaf(@{@"value":@1}) })),
//	o(F43,	aBroadcast(0, @[@"y0x0", @"y0x1", @"y1x0", @"y1x1"], @{flip} )), //, @"y1x1"
	o(F43,	aBroadcast(0, @[@"y0x1", @"y1x0", @"y1x1", @"y0x0"], @{flip} )), //, @"y1x1"
	o(F56,	aMirror(@"F43")),
																		]}	));
R(LinkSpinTest,  (@{CameraHsuz(0, 0, 0, 1), @"boundsDrapes":@0.1,	@"parts":@[
	o(bun,	aBundle(@{@"structure":ab_bundle, @"proto":aGenLeaf(@{@"value":@1}) })),
//	o(qa,	aMirror(@"a", @{@"jog":@"0.00,0,0"} )),		// views edge
	o(qa,	aMirror(@"a", @{@"jog":@"0.1,0,0"} )),		// views broadside ++

//	o(bun,	aBundle(@{@"structure":abc_bundle, @"proto":aGenLeaf() })),
//	o(qa,	aMultiply(0, @[@"a", @"b", @"c"], @{flip} )),

//	o(bun,	aBundle(@{@"structure":ab_bundle })),
//	o(qa,	aMultiply(0, @[@"a", @"b"], @{flip} )),
//	o(qb,	aMirror(@"qa")),

//	o(bun,	aBundle(@{@"structure":abcd_bundle })),
//	o(qa,	aMultiply(0, @[@"a"], @{flip} )),
//	o(qc,	aMultiply(0, @[@"b", @"c", @"d", @"d"], @{flip} )),
//	o(qb,	aMirror(@"qa")),
																		]}	));



//160522 bug fixed numerics bad
R(Prev1-aa__,  (@{CameraHsuz(0, 80, 10, 1),							@"parts":@[
			aGenerator(@{@"events":@[@[@"a"],@[@"a"],@[],@[],@"again"],flip, @"P":@"bun"}),
	o(bun,	aTunnel(@{@"structure":a_bundle, @"proto":aGenPrevLeaf() })),
																		]}	));


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark * Bundle Test Ago
M(Ago,(@{CameraHsuz(0, 0, 0, 1),									@"parts":@[
	o(c,	aBroadcast(0, 0,						@{flip} )),
	o(d,	anAgo(@[@"c"])),					// @{flip}
	o(e,	aBroadcast(@[@"d"], 0)),
																		]})	);
R(Ago Bun blinks, (@{CameraHsuz(2, 10, 10, 1),						@"parts":@[
			aGenerator(2, @{@"events":@[@[@"a"],@[@"a"],@[],@[],@"again"],flip, @"P":@"bun"}),
	o(bun,	aTunnel(@{@"structure":a_bundle, @"proto":aGenBcastLeaf()})),
	o(b,	anAgo(@[@"a"])),
	o(c,	aBroadcast(@[@"b"], 0)),
	o(d,	aMirror(@[@"c"])),
//	o(d,	aMirror(@[@"a"])),
																		]}	));
 // 160823 bug in mirror drawing
R(Mirror, (@{CameraHsuz(0, 0, 0, 1),								@"parts":@[
	o(c,	aBroadcast(0, 0)),
	o(d,	aMirror(@[@"c"])),
																		]}	));
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark * Bundle Test: Previous, Rotator, Modulator, Bun,..
 // 160823 bug with bundle sizing
//int x=0/0;
R(PrevBun,(@{CameraHsuz(0, 0, 0, 1), @"boundsDrapes":@0.1, @"displayAxis":@1, @"parts":@[
//	o(ham,	aHamming(0,	0, 								@{flip})),
//	o(PP,	aPort(0,									@{/*flip*/})),
///			 aBundleTap(0, 0.5,							@{flip, @"P":@"evi"})),
//			 aPrevious(@0)),
	o(evi,	aTunnel(@{@"structure":a_bundle, @"proto":aGenBcastLeaf()})),
																		]})	);
////151016: test how M and N changing propigates through a Previous
//M(PrevMN gui,(@{CameraHsuz(0, 30, 10, 1),							@"parts":@[
//					 aGenerator(@{@"nib":0, @"LoGen_abFwdBkw",
//			@{flip, @"P":@"gen", @"resetTo":@[@"a", @"fwd"/*, @"bkw"*/]})),
//	o(gen, aTunnel(@[ @"a", @"b", @"fwd", @"bkw"], aGenBcastLeaf())),
//	o(prev,aPrevious(0,			@{flip, @"mode":@"simMode2", @"bias":@0,
//						@"S":@"a", @"T":@"b", @"M":@"fwd", @"N":@"bkw",	})),
//																		]})	);


//M(Modulator,(@[	aModulator()										])	);
//M(Rotator,	(@[	aRotator  (0,								@{spin$0})]));
//R(RotatorS, (@[@{partsStackX},
//			aRotator(0,									@{spin$0, flip}),
//			aRotator(0,									@{spin$2, flip}),
//																		])	);


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark * Leaf Tests:
#define tstLeaf(x) (@{CameraHsuz(0, 15, 10, 1),							\
		@" boundsDrapes":@0.1, @"fontNumber":@"4", @"parts":@[			\
		x(),												\
															  ]}	)

#pragma mark - Generation Metaphors
/********************/ setParentMenu(@"Generation Metaphors"); /*************/

   // metaphor A: 		S Y N C H R O N O U S
  // 		datum from events[...] presented, by 's' or 'S' keys
 //   \down --> newData appears, (unknowns computed, newborn conceived but inactive)
//	  /up	--> newborn goes live
#pragma mark * A. Sync Bot events, rnd, array
M(A1. Sync Bot Prob,	(@{CameraHsuz(0,0,0,1),									@"parts":@[
	aGenerator(3, @{n(lo), @"P":@"bun1", @"prob":@0.6, flip}),
	aTunnel(@{n(bun1), @"structure":@[@"a", @"b"], @"proto":aGenBcastLeaf()}),
	aMultiply(0, @[@"a", @"b"],@{n(qa), flip} ),
	aMirror(@"qa"),
																		]}	));
M(A2. Sync Bot Events[a],  (@{CameraHsuz(0, 45, 10, 0.8),						@"parts":@[
	aGenerator(@{n(lo), @"P":@"bun", @"events":@[@[@"a"],@[@"b"],@[@"a",@"c"],@[],@"again"],flip}),
	aTunnel(@{n(bun), @"structure":abc_bundle, @"proto":aGenBcastLeaf() }),
	aMaxOr(0, @[@"a", @"b", @"c"], @{flip}),
//	o(qa,	aMaxOr(0, @[@"a,l=5"],						@{flip} )),

																		]}	));
	R(A. Sync Bot Events[] prev,	(@{CameraHsuz(0, 45,  30, 1.5),							@"parts":@[	//(0, 80, 10, 1)
		aGenerator(@{n(lo), @"P":@"act1/evi", @"events":@[@[@"a"],@[@"a"],@[],@[],@"again"],flip}),
		anActor(@{n(act1),	@"minHeight":@(minHeight),
			@"evi":aTunnel(@{@"structure":a_bundle, @"proto":aGenPrevBcastLeaf() }),
		}),
																			]}	));
M(A3. Sync Bot Events +WriteHead,	(@{CameraHsuz(0, 45,  30, 1.5),							@"parts":@[	//(0, 80, 10, 1)
	aGenerator(2, @{n(lo), @"P":@"act1/evi", @"events":@[@[@"a"],@[@"a"],@[],@[],@"again"], flip}),
	anActor(@{n(act1),	@"minHeight":@(minHeight),
		@"evi":aTunnel(@{@"structure":a_bundle, @"proto":aGenPrevBcastLeaf(0,0,@{@"U":@"write"}) }),
		@"con":aBundle(@{@"structure":z_bundle, @"proto":aGenMaxsqLeaf(@{@"value":@1.0},@{@"U":@"write"}), flip }),
	}),
	aWriteHead(@{@"actor":@"act1", @"babyPrototype":@"Hamming"}),
																		]}	));
#pragma mark * B. Sync Bot+Top
id loDat1	= @[@"b",  		   @"a",  @[],   @[],  @"a",  			@"again"];
id hiDat1	= @[@"z",  		   @[],  @"y",  @"z",  @"y",  			@"again"];
id hiLoDat1	= @[@[@"a", @"z"], @"a", @"y",  @"z",  @[@"a", @"y"],  @"again"];
M(B. Sync Bot+Top,(@{CameraHsuz(0, 45, 1, 1.1), @" boundsDrapes":@"0.02", @"displayAxis":@0, @"parts":@[
//	aNet(@{}, @[
		aGenerator( @{n(lo), @"events":hiLoDat1, @"addBundleTap":@"hiTap", @"P":@"act1/evi=", flip}),
		anActor(@{n(act1), @"minHeight":@10.0, spin$1,
			@"evi":aTunnel(@{@"structure":ab_bundle, @"proto":aGenBcastLeaf(0, @{@"U":@"write"}) }),
			@"con":aTunnel(@{@"structure":yz_bundle, @"proto":aGenMaxsqLeaf(0, @{@"U":@"write"},
				 @{spin$0, @"minSize":@"5,0,0"}), @"positionPriorityXz":@1, spin$1, flip}),
		}),
		aBundleTap( @{n(hiTap), @"P":@"act1/con=", @"xS":@"hiMod="}),
		aWriteHead( @{@"actor":@"act1",  @"babyPrototype":@"Hamming"}),
//	]),
																		]}	));


//#pragma mark * X
//M(X. , (@{CameraHsuz(0, 0, 10, 1), VEL(-5),	@"parts":@[	]}	));
//xM(X. , (@{CameraHsuz(0, 0, 10, 1.6), VEL(-5),	@"parts":@[
//	aGenerator(@{n(lo", @"nib":@"LoGen_a", @"P":@"foo", flip}),
//	aTunnel(@{n(foo", @"structure":a_bundle, @"proto":aGenLeaf() }),
//																		]}	));


#pragma mark * C. Sync Top -> serdes+sounds
   // 's' sequence on top plays sequence of sounds on BotGen
  //171205 BUG: would like pressing buttons to unlock sequence
id morseBundle				= 1? didat_sound_bundle : didat_bundle;
M(C. Sync Top -> serdes+sounds, (@{CameraHsuz(0, 0, 10, 1.6), VEL(-5),	@"parts":@[
	1? aGenerator(@{n(lo), @"nib":@"LoGen_morse", @"addBundleTap":@"hiTap",
					/**/@"events":@[@[@"z"], @[], @"again"], @"P":@"didat", flip}):
	   aGenerator(@{n(lo), @"events":@[@[@"z"], @[], @"again"], @"addBundleTap":@"hiTap", @"P":@"didat", flip}),
	aTunnel(@{n(didat), @"structure":morseBundle, @"proto":aGenSoundMaxLeaf(@{@"loop":@1}) }),
	aSequence(@"z",@[@"d_i,l=4", @"d_a", @"d_a", @"d_t", ],	@{n(seq1), flip} ),
	aTunnel(@{n(bun2), @"structure":z_bundle, @"proto":aGenBcastLeaf(), flip}),
	aBundleTap(@{n(hiTap), @"P":@"bun2"}),
																		]}	));

 // Gui 1:N generation:
#pragma mark * D. Gui 1:N Bot, LATE cPrev
//171216 MISSING: Sound
M(D. Gui 1:N Bot, (@{CameraHsuz(0, 0, 10, 1.0), VEL(-5),			@"parts":@[
	aGenerator(@{n(lo), @"nib":@"LoGen_morse", @"P":@"didat", flip}),
	aTunnel(@{n(didat), @"structure":didat_speak_bundle, @"proto":aGenSoundLeaf() }),//didat_speak_sound_bundle
	anAgo(@"d_i", 	@{n(d_iA)}),
	anAgo(@"d_a", 	@{n(d_aA)}),
	anAgo(@"d_t", 	@{n(d_tA)}),
	anAgo(@"speak", @{n(speakA)}),
	aMaxOr(0, @[@"d_iA", @"d_aA", @"d_tA", @"speakA"], @{n(qa), flip} ),
	aMirror(@"qa"),
																		]}	));
	//R(D. gui 1:N bot, (@{CameraHsuz(0, 0, 10, 1.0)}) );

 // metaphor B. clock + datum from sliders 171113BUG
#pragma mark * F. Async(+clk) AGO
M(F. Async(+clk) AGO, (@{VEL(-6),@"flashFrames":@"5",			@"parts":@[
	aGenerator(@{n(lo), @"nib":@"LoGen_a", @"resetTo":@[@"a"], @"P":@"bun", flip}),
	aTunnel(@{n(bun), @"structure":@[@"a"], @"proto":aGenLeaf() } ),
//	aBroadcast(@"a", @{n(aa), }),
//	aBroadcast(@"a", 0, @{n(aa)} ),
	anAgo(@"a", @{n(aa), }),
	aMirror(@"aa"),
																		]}	));

#pragma mark * G. Async + clocked birth
M(G. Async + clocked birth,	(@{CameraHsuz(0, 45,  30, 1.5), @"XasyncData":@1, @"parts":@[	//(0, 80, 10, 1)
	aGenerator(@{n(lo), @"nib":@"LoGen_a", @"P":@"act1/evi=", flip}),
	anActor(@{n(act1),	@"minHeight":@(minHeight),
		@"evi":aTunnel(@{@"structure":a_bundle, @"proto":aGenPrevBcastLeaf(@{},0,@{@"U":@"write"}) }),
		@"con":aBundle(@{@"structure":z_bundle, @"proto":aGenMaxsqLeaf(@{@"value":@1.0},@{@"U":@"write"}), flip }),
	}),
	aWriteHead(@{@"actor":@"act1", @"babyPrototype":@"Hamming"}),
																		]}	));
	R(G. async + clocked birth,	(@{CameraHsuz(0, 45,  10, 1.5), @"boundsDrapes":@"0.1",
				@"XasyncData":@1, @"XreViewConstantly":@1, @"parts":@[	//(0, 80, 10, 1)
		aGenerator(@{n(lo), @"nib":@"LoGen_a", @"resetTo":@[@"a=0.3"], @"P":@"evi=", flip}),
		aBundle(@{n(evi), @"structure":a_bundle, @"proto":aGenBulbLeaf(@{@"Xvalue":@1.0}) }),
		//aGenLeaf(),
																			]}	));
	/// 171203 BUG 180118 FIXED LoGen_a and a disagree at start
	R(G. async + clocked birth,	(@{CameraHsuz(0, 45,  30, 1.5), @"XasyncData":@1, @"parts":@[	//(0, 80, 10, 1)
		aGenerator(@{n(lo), @"nib":@"LoGen_ab", @"resetTo":@"a", @"P":@"evi=", flip}),
		aTunnel(@{n(evi), @"structure":ab_bundle, @"proto":aGenBcastLeaf(@{@"Xvalue":@1.0}) }),
		aMaxOr(0, @[@"a", @"b"],		@{flip} ),
																			]}	));

	R(G. async + clocked birth,	(@{CameraHsuz(0, 45,  30, 1.5), SIM_ON, VEL(-9), @"XasyncData":@1,	@"parts":@[	//(0, 80, 10, 1)
		anActor(@{n(act1),	@"minHeight":@(minHeight),
			@"con":aBundle(@{@"structure":z_bundle, @"proto":aGenMaxsqLeaf(@{@"value":@1.0},@{@"U":@"write"}), flip }),
		}),
		aWriteHead(@{@"actor":@"act1", @"babyPrototype":@"Hamming"}),
																			]}	));
#pragma mark * H. async Top3+Bot3
M(H. Async Top3+Bot3, (@{											@"parts":@[
	aNet(@{flip0}, @[
	o(lo,	aGenerator(0, @{@"nib":@"LoGen_abcd", @"resetTo":@[@"a=0.5"], flip, @"P":@"bun"})),
	o(bun,	aTunnel(@{@"structure":abcd_bundle, @"proto":aGenBcastLeaf() })),
	o(qc,	aMaxOr   (0, @[@"a", @"b", @"c", @"d"],		@{flip} )),
	o(qd,	aMaxOr   (@"qc", @[@"x", @"y", @"z"])),
	o(bun2,	aTunnel(@{@"structure":xyz_bundle, @"proto":aGenBcastLeaf(), flip})),
	o(hi,	aGenerator(@{@"nib":@"HiGen_xyz", @"resetTo":@[@"z=0.5"], @"P":@"bun2"})),
	]),
																		]}	));
#pragma mark * I. async Top+Bot
R(I. async Top+Bot, (@{											@"parts":@[
			aGenerator(@{@"nib":@"LoGen_a", @" resetTo":@[@"a"], flip, @"P":@"bun"}),
	o(bun,	aTunnel(@{@"structure":a_bundle, @"proto":aGenLeaf() })),
			aLink(@"a", @"z"),
	o(bun2,	aTunnel(@{@"structure":z_bundle, @"proto":aGenLeaf(), flip})),
			aGenerator(@{@"nib":@"HiGen_z", @" resetTo":@[@"z"], @"P":@"bun2"}),
																		]}	));
XX	R(Ic. async Top, (@{ 											@"parts":@[
		o(bun2,	aTunnel(@{@"structure":z_bundle, @"proto":aGenBulbLeaf(), flip})),
				aGenerator(@{@"nib":@"HiGen_z", @"resetTo":@[@"z"], @"P":@"bun2"}),
																		]}	));
XX	R(Id. async Bot, (@{												@"parts":@[
				aGenerator(@{@"nib":@"LoGen_a", @" resetTo":@[@"a"], flip, @"P":@"bun"}),
		o(bun,	aTunnel(@{@"structure":a_bundle, @"proto":aGenLeaf() })),
				aMirror(@"a"),//	aMirror(@"z",				@{flip} ),
																		]}	));


#pragma mark * J. async Top
M(J. async Top, (@{												@"parts":@[
	aNet(@{flip0}, @[
		aMirror(@"qc", @{flip}),	//aMirror(@"z",				@{flip0} ),
		aMaxOr   (0, @[@"x", @"y", @"z"], @{n(qc)}),
		aTunnel(@{n(bun2), @"structure":xyz_bundle, @"proto":aGenBcastLeaf(), flip}),
		aGenerator(@{n(hi), @"nib":@"HiGen_xyz", @"resetTo":@[@"z"], @"P":@"bun2"}),
	]),
																		]}	));
XX	R(resetTo should set slider, (@{								@"parts":@[
			aTunnel(@{n(bun2), @"structure":z_bundle, @"proto":aGenBcastLeaf(), flip}),
			aGenerator(@{n(hi), @"nib":@"HiGen_z", @"XresetTo":@[@"z"], @"P":@"bun2"}),
																			]}	));
#pragma mark * K. async Bot; rst Top
M(K. async Bot; rst Top,(@{CameraHsuz(0, 45, 1, 1.1), @"fl":@"", @" boundsDrapes":@"0.02", @"displayAxis":@0, @"parts":@[
	aGenerator( @{n(lo), @"nib":@"LoGen_abc", @"P":@"act1/evi=", flip}),
	anActor(@{n(act1), @"minHeight":@10.0, spin$1,
		@"evi":aTunnel(@{@"structure":abc_bundle, @"proto":aGenBcastLeaf(0, @{@"U":@"write"}) }),
		@"con":aTunnel(@{@"structure":z_bundle, @"proto":aGenMaxsqLeaf(0, @{@"U":@"write"},
		     @{spin$0, @"minSize":@"5,0,0"}), @"positionPriorityXz":@1, spin$1, flip}),
	}),
	aBundleTap( @{n(hiTap), @"resetTo":@[@"z"], @"P":@"act1/con="}),
	aWriteHead( @{@"actor":@"act1",  @"babyPrototype":@"Hamming"}),
																		]}	));


#pragma mark * L. Push-data ??
id ab_fwdBkw_bundle = @[@"a", @"b", @"fwd", @"bkw"];
 // debugging 1:N datu
M(L. Push-data ??, (@{CameraHsuz(0, 45, 10, 1.5), VEL(-2), @"boundsDrapes":@0,
 			@"fontNumber":@"2", @"displayAxis":@0, @"asyncData":@1, @"parts":@[		//xyzzy11 	either
		aGenerator(@{n(word), @"nib":@"LoGen_langAbfb", @"P":@"evi", flip}),//LoGen_lang3Abfb
		aTunnel(@{n(evi), @"structure":ab_fwdBkw_bundle, @"proto":aGenSoundPrevBcastLeaf(0, 0, @{spin$1}) }),
		aMaxOr(0, @[@"a", @"b", @"fwd", @"bkw"], @{flip}),
																		]}	));
	R(F? Push-data, (@{CameraHsuz(0, 45, 10, 1.5), VEL(-2), @"boundsDrapes":@0,
				@"fontNumber":@"2", @"displayAxis":@0, @"asyncData":@1, @"parts":@[	//xyzzy11	either
			aGenerator(@{n(word), @"xS=":@0, @"P":@"evi", @"nib":@"LoGen_langAbfb", flip}),//LoGen_lang3Abfb
			aTunnel(@{n(evi), @"structure":@[@"a:sound:a-sound"], @"proto":aGenSoundPrevBcastLeaf(0, 0, @{spin$1}) }),
																			]}	));

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark - A. Small Machines
/************************/ setParentMenu(@"A. Small Machines"); /***************/
id rainU1U2_bundle = @[@"rain", @"umbrellaA", @"umbrellaB"];
id dry_bundle = @[@"dry"];
/// Objective C code, dialect for construction models
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark * A. UMBRELLA
//URL factalWorkbench://A.%20Small%20Machines/A.%20Umbrella
M(A. Umbrella,(@{CameraHsuz(0, 30, 30, 2), VEL(-5), @"boundsDrapes":@.0,
				@"displayLeafNames":@1, @"fontNumber":@"5", @"parts":@[
			aGenerator(@{@"nib":@"LoGen_umbrella", @"resetTo":@[/*@"a"@"a/gen"*/], flip, @"P":@"bun"}),
	o(bun,	aTunnel(@{@"structure":rainU1U2_bundle, @"proto":aGenBcastLeaf()})),
	o(umbrella,aMaxOr(0, @[@"umbrellaA",@"umbrellaB"], @{flip})),
	o(dry,	aMinAnd(@"z", @[@"rain",@"umbrella"], @{flip})),
	o(bun2,	aBundle(@{@"structure":z_bundle, @"proto":aGenLeaf(), flip})),
			aGenerator(@{@"nib":@"HiGen_z", @"P":@"bun2"}),
																		]}	));
R(name labels,(@{CameraHsuz(0, 0, 0, 1), @"boundsDrapes":@.05, @"fontNumber":@4,  @"parts":@[
//	o(bun,	aBundle(@{@"structure":@[@"A"], aGenBcastLeaf())),
//	o(bun,	aBundle(@{@"structure":@[@"umbrellaBumbrellaA"], aGenBcastLeaf())),
//	o(bun,	aBundle(@{@"structure":@[@"umbrellaBumbrellaA", @"umbrellaBumbrellaB"], aGenBcastLeaf())),
	o(bun,	aTunnel(@{@"structure":rainU1U2_bundle, @"proto":aGenBcastLeaf()})),
//	o(bun,	aBundle(@{@"structure":@[@"ABCDEFG"], aGenBcastLeaf())),
																		]}	));


 // Trying to get new building working:
R(Reenactment Simulator, 	(@{CameraHsuz(0, 0, 0, 0.8),			@"parts":@[
			aGenerator(1, @{@"prob":@0.5, flip, @"P":@"bun1"}),
	o(bun1,	aTunnel(@{@"structure":a_bundle, @"proto":aGenBcastLeaf()})),
	o(qa,	aMultiply(0, @[@"a"],			@{flip} )),
																		]}	));
 // Trying to get new building working:
R(Blink1, 	(@{CameraHsuz(0, 0, 0, 0.8),							@"parts":@[
			aGenerator(50, @{@"prob":@0.6, flip, @"P":@"bun1"}),
	o(bun1,	aTunnel(@{@"structure":abc_bundle, @"proto":aGenBcastLeaf()})),
	o(qa,	aMultiply(0, @[@"a", @"b", @"c"],			@{flip} )),
																		]}	));
// // 160321 bug: placement okay 160411
//R(Blink1, 	(@{CameraHsuz(0x, 0, 0, 0.8), @"boundsDrapes":@.1,	@"parts":@[
//	o(bun1,	aBundle(@{@"structure":abc_bundle, aGenBcastLeaf())),
//																		]}	));
M(Rlink2, 	(@{CameraHsuz(0, 0, 0, 1),								@"parts":@[
			aGenerator(0, @{@"prob":@0.6, flip, @"P":@"bun1"}),
	o(bun1,	aTunnel(@{@"structure":@[@"a", @"b", @"c"], @"proto":aGenBcastLeaf()})),
	o(qa,	aMultiply(0, @[@"a", @"b", @"c"],			@{flip} )),
																		]}	));

 // test the various port macros:
R(Blink1, 	(@{CameraHsuz(0, 0, 0, 0.8),							@"parts":@[
			aGenerator(0, @{@"prob":@0.6, flip, @"P":@"bun"}),
	o(bun,	aTunnel(@{@"structure":abc_bundle, @"proto":aGenBcastLeaf()})),
	o(qa,	aMultiply(0, @[@"a", @"b", @"c"],			@{flip} )),
	o(qb,	aMirror(@"qa")),
																		]}	));

r(			(@{CameraHsuz(0, 0, 0, 0.8), VEL(-7),					@"parts":@[
			aGenerator(1, @{@"prob":@0.6, flip, @"P":@"evi"}),
	o(evi,	aTunnel(@{@"structure":a_bundle, @"proto":aGenBcastLeaf()})),
	o(qa,	aMultiply(0, @[@"a"],						@{flip} )),
																		]}	));

R(fun2, 	(@{CameraHsuz(0, 0, 0, 1),								@"parts":@[//151009+
			aGenerator(-1, @{@"prob":@0.5, flip, @"P":@"bun"}),
	o(bun,	aTunnel(@{@"structure":ab_bundle, @"proto":aGenBcastLeaf() })),
	o(qa,	aMinAnd(0, @[@"a", @"b"],					@{flip})),
	o(qb,	aMaxOr( 0, @[@"a", @"b"],					@{flip})),
			aMirror(@"qa"),
			aMirror(@"qb"),
																		]}	));
// 150430 placement bug fixed 160225
R(fun3, 	(@{CameraHsuz(0, 0, 45/*90*/, 1),						@"parts":@[//141218 +
	o(bun,	aBundle(@{@"structure":ab_bundle, @"proto":aGenBcastLeaf() })),
	o(qa,	aMaxOr (0,@[@"a", @"b"],					@{flip} )),
	o(qb,	aMinAnd(0,@[@"a", @"b"],					@{flip} )),
	o(qc,	aMinAnd(0,@[@"a", @"b"],					@{flip} )),
	o(qd,	aMinAnd(0,@[@"a", @"b"],					@{flip} )),
	o(qe,	aMinAnd(0,@[@"a", @"b"],					@{flip} )),
	o(qf,	aMinAnd(0,@[@"a", @"b"],					@{flip} )),
	o(qg,	aMinAnd(0,@[@"a", @"b"],					@{flip} )),
	o(qh,	aMinAnd(0,@[@"a", @"b"],					@{flip} )),
	o(qi,	aMinAnd(0,@[@"a", @"b"],					@{flip} )),
	o(qj,	aMinAnd(0,@[@"a", @"b"],					@{flip} )),
	o(qk,	aMinAnd(0,@[@"a", @"b"],					@{flip} )),
	o(ql,	aMinAnd(0,@[@"a", @"b"],					@{flip} )),
//	o(qg,	aHamming(0,@[@"a", @"b"],					@{flip} )),
																		]}	));
// 160131 fix Tunnel skin spectral color
R(Seq1, 	(@{CameraHsuz(0, 0, 0, 1),								@"parts":@[//141218 +
	o(bun,	aTunnel(@{@"structure":a_bundle, @"proto":aGenBcastLeaf() })),
																		]}	));
r(			(@{CameraHsuz(0, 0, 0, 1),								@"parts":@[
			aGenerator(@{@"prob":@0.5, flip, @"P":@"bun"}),
	o(bun,	aTunnel(@{@"structure":a_bundle, @"proto":aGenBcastLeaf() })),
	o(qa,	aMinAnd(0, @[@"a"], @{flip} )),
																		]}	));

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark * befAbcd
////////////////////////////////////////////////
/////  7. Previous						   /////
////////////////////////////////////////////////
id pat1 = @[ @[@"a"], @[    ],											@"again"];
id pat2 = @[ @[@"a"], @[@"b"], @[@"c"], @[@"d"], @[@"d"],
			 @[@"c"], @[@"c"], @[@"b"], @[@"b"], @[@"a"],				@"again"];
id pat3 = @[ @[@"a"], @[@"b"], @[@"c"], @[@"d"], @[@"d"],				@"again"];
id pat4 = @[ @[@"a"], @[    ], @[    ], @[    ],						@"again"];
id pat5 = @[ @[@"a"], @[@"b"], @[@"c"], @[@"d"],
			 @[@"d"], @[@"c"], @[@"b"], @[@"a"],						@"again"];

 // 160223 bug in placement of d2c FIXED 160226
M(befAbcd4,	(@{CameraHsuz(0, 30, 10, 1.5),							@"parts":@[
			aGenerator(50, @{@"events":pat5, flip, @"P":@"bun"}),
//			aGenerator(@{@"prob":@0.6, flip, @"P":@"bun"})),
	o(bun,	aTunnel(@{@"structure":abcd_bundle, @"proto":aGenPrevBcastLeaf(0,0,0,@{spin$1}) })),
	o(a2b,	aMultiply(0, @[@"a.-", @"b.+"],			@{flip} )),
	o(b2c,	aMultiply(0, @[@"b.-", @"c.+"],			@{flip} )),
	o(c2d,	aMultiply(0, @[@"c.-", @"d.+"],			@{flip} )),
	o(d2a,	aMultiply(0, @[@"d.-", @"a.+"],			@{flip} )),
	o(fwd,	aMaxOr(   0, @[@"a2b", @"b2c", @"c2d", @"d2a"], @{flip} )),
	o(fMir,	aMirror(  @"fwd")),
	o(b2a,	aMultiply(0, @[@"a.+", @"b.-"],			@{flip} )),
	o(c2b,	aMultiply(0, @[@"b.+", @"c.-"],			@{flip} )),
	o(d2c,	aMultiply(0, @[@"c.+", @"d.-"],			@{flip} )),
	o(a2d,	aMultiply(0, @[@"d.+", @"a.-"],			@{flip} )),
	o(bkw,	aMaxOr(   0, @[@"b2a", @"c2b", @"d2c", @"a2d"], @{flip} )),
	o(rMir,	aMirror(  @"bkw")),
																		]}	));
 // 160223 bug in placement of d2c FIXED 160226
R(befAbcd4,	(@{CameraHsuz(0, 30, 10, 1),							@"parts":@[
	o(bun,	aTunnel(@{@"structure":ab_bundle, @"proto":aGenPrevBcastLeaf() })),
	o(c2d,	aMultiply(0, @[@"b.-", @"b.+"],			@{flip} )),
	o(d2c,	aMultiply(0, @[@"b.+", @"b.-"],			@{flip} )),
																		]}	));

R(befAbc2,	(@{CameraHsuz(0, 30, 10, 0.8),							@"parts":@[
			aGenerator(50, @{@"prob":@0.6,flip, @"P":@"bun"}),
	o(bun,	aTunnel(@{@"structure":ab_bundle, @"proto":aGenPrevBcastLeaf() })),
	o(a2b,	aMultiply(0, @[@"a.-", @"b.+"],			@{flip} )),
	o(b2c,	aMultiply(0, @[@"b.-", @"a.+"],			@{flip} )),
	o(fwd,	aMaxOr(   0, @[@"a2b", @"b2c"],			@{flip} )),
	o(fMir,	aMirror(  @"fwd")),
	o(b2a,	aMultiply(0, @[@"b.-", @"a.+"],			@{flip} )),
	o(c2b,	aMultiply(0, @[@"a.-", @"b.+"],			@{flip} )),
	o(bkw,	aMaxOr(   0, @[@"b2a", @"c2b"],			@{flip} )),
	o(rMir,	aMirror(  @"bkw")),
																		]}	));
 // 160714 Debugging 'S' bug
id patXs = @[@"a", @"b", @"c", @"d", @"again"];
R(befAbc1,	(@{CameraHsuz(0, 30, 10, 0.8),	VEL(-7),				@"parts":@[
			aGenerator(0, @{@"events":patXs, flip, @"P":@"bun"}),
	o(bun,	aTunnel(@{@"structure":abcd_bundle, @"proto":aGenBcastLeaf() })),
	o(and,	aMinAnd(0, @[@"a", @"b", @"c", @"d"],	@{flip})),
																		]}	));

R(befAbcd,	(@{CameraHsuz(0, 30, 10, 0.8),							@"parts":@[
	o(bun,	aBundle(@{@"structure":ab_bundle, @"proto":aGenBcastLeaf() })),
	o(a2b,	aMultiply(@0, @[@"a", @"b"],			@{flip} )),
																		]})	);
r(			(@{CameraHsuz(0, 30, 1, 0.8),							@"parts":@[
	o(bun,	aBundle(@{@"structure":a_bundle, @"proto":aGenPrevBcastLeaf() })),
	o(a2b,	aMultiply(0, @[@"a.-", @"a.+"],			@{flip} )),
	o(mir,	aMirror(  @"a2b")),
																		]}	));

R(befA, 	(@{CameraHsuz(0, 30, 10, 0.8),							@"parts":@[
			aGenerator(0, @{@"prob":@0.6, flip, @"P":@"bun"}),
	o(bun,	aTunnel(@{@"structure":a_bundle, @"proto":aGenPrevBcastLeaf() })),
	o(qc,	aMultiply(0, @[@"a.+", @"a.-"],			@{flip} )),
	o(qd,	aMirror(@"qc")),
																		]}	));
NSArray *event33 = @[	@[@"a"],			// t=0
						@[@"b"],			// t=1
						@[@"c"],			/* t=2 */				@"again" ];
M(ABC,		(@{CameraHsuz(0, 30, 10, 1),							@"parts":@[
			aGenerator(0, @{@"events":event33,flip, @"P":@"bun"}),//20
//			aGenerator(20, @{@"prob":@0.4,		  flip, @"P":@"bun"}),
	o(bun,	aTunnel(@{@"structure":abc_bundle, @"proto":aGenBcastLeaf() })),
	o(qa,	aMaxOr(0, @[@"a", @"b", @"c"],				@{flip} )),
			aMirror(@"qa"),
																		]}	));
r(			(@{CameraHsuz(0, 0, 0, 1),								@"parts":@[
			aGenerator(0, @{@"events":event33,flip, @"P":@"bun"}),
	o(bun,	aTunnel(@{@"structure":ab_bundle, @"proto":aGenBcastLeaf() })),
//	o(qa,	aMaxOr(0, @[@"a", @"b"],					@{flip} )),
//			aMirror(@"qa"),
																		]}	));

// 150130: fails with of_bi, works with aBcastLeaf
R(autoU,	 (@{CameraHsuz(0, 0, 0, 1),								@"parts":@[
				 aGenerator(@{@"prob":@0.6, flip, @"P":@"bun"}),
	o(bun,	aTunnel(@{@"structure":a_bundle, @"proto":aGenBcastLeaf() })),//
	o(qa,	aMaxOr(0, @[@"a"],							@{flip} )),
	o(qb,	aMaxOr(0, @[@"a"],							@{flip} )),
																		]}	));
 // 160123 partsStackX interferes with selfLinkY
R(Auto Broadcast flipped,	(@{CameraHsuz(0, 0, 0, 0.8), partsStackY,@"parts":@[
	o(bun1,	aNet(@{partsStackX}, @[
		o(x,	aMaxOr(0, @"qb"/**/)),
		o(y,	aMaxOr(0, @"qb")),
		])),
	o(bun2,	aNet(@{selfLinkY/*, partsStackX*/}, @[		//
		o(qb,	aModulator(0,							@{spin$0})),	//S+x
		])),
																		]}	));

////////////////////////////////////////////////
/////     All Splitter Bodies			   /////
////////////////////////////////////////////////

 // 116115: note: data flow is NOT linear!
R(tallMan,	(@{CameraHsuz(0, 0, 0, 1), VEL(0),						@"parts":@[
			aGenerator(-1, @{@"prob":@0.5, flip, @"P":@"bun"}),
	o(bun,	aTunnel(@{@"structure":a_bundle, @"proto":aGenBcastLeaf() })),
	o(qb,	aMinAnd(	0, @[@"a"],						@{flip} )),
	o(qc,	aMaxOr(		0, @[@"qb"],					@{flip} )),
	o(qd,	aBroadcast( 0, @[@"qc"],					@{flip} )),
	o(qe,	aKNorm(		0, @[@"qd"],					@{flip} )),
	o(qg,	aKNorm(		0, @[@"qe"],					@{flip} )),
	o(qh,	aMultiply(	0, @[@"qg"],					@{flip} )),
	o(qi,	aSequence(	0, @[@"qh"],					@{flip} )),
			aMirror(@"qi"),
																		]}	));
 // 170206: BUG sim hangs with event in aSequence
R(tallWide0,(@{CameraHsuz(0, 0, 0, 1), VEL(0),						@"parts":@[
			aGenerator(@{@"prob":@0.5, flip, @"P":@"bun"}),
	o(bun,	aTunnel(@{@"structure":ab_bundle, @"proto":aGenPrevBcastLeaf() })),
	o(B0,	aMinAnd(	0, @[@"a.-"],					@{flip} )),
	o(B1,	aMinAnd(	0, @[@"b.-"],					@{flip} )),
	o(C0,	aMaxOr(		0, @[@"B0"],					@{flip} )),
	o(C1,	aMaxOr(		0, @[@"B1"],					@{flip} )),
	o(D0,	aBroadcast(	0, @[@"C0"],					@{flip} )),
	o(D1,	aBroadcast(	0, @[@"C1"],					@{flip} )),
	o(E0,	aKNorm(		0, @[@"D0"],					@{flip} )),
	o(E1,	aKNorm(		0, @[@"D1"],					@{flip} )),
	o(G0,	aKNorm(		0, @[@"E0"],					@{flip} )),
	o(G1,	aKNorm(		0, @[@"E1"],					@{flip} )),
	o(H0,	aMultiply(	0, @[@"G0"],					@{flip} )),
	o(H1,	aMultiply(	0, @[@"G1"],					@{flip} )),
	o(I0,	aSequence(	0, @[@"H0"],					@{flip} )),
	o(I1,	aSequence(	0, @[@"H1"],					@{flip} )),
	o(M0,	aMirror(@"I0" )),
	o(M0,	aMirror(@"I1")),
																		]}	));

M(Example 1,(@{CameraHsuz(0, 30, 10, 1),							@"parts":@[
			aGenerator(@{@"prob":@0.5, flip, @"P":@"bun"}),
//			aGenerator(@{@"events":@0.5, flip, @"P":@"bun"}),
//				aGenerator(@{0, @[@[@"a"],@[@"b"],@"again"],	@{flip, @"P":@"bun"})),

	o(bun,	aTunnel(@{@"structure":ab_bundle, @"proto":aGenPrevBcastLeaf(0,0,0,@{spin$1}) })),	// someday simForward @"bias":@0.44
	o(a1,	aMultiply(0, @[@"a.-", @"b.+"],				@{flip} )),
	o(a2,	aMultiply(0, @[@"a.-", @"b.-"],				@{flip} )),
	o(a3,	aMultiply(0, @[@"a.+", @"b.+"],				@{flip} )),
	o(qc,	aMaxOr(   0, @[@"a1", @"a2", @"a3"],		@{flip} )),
	o(mir,	aMirror(@"qc")),
																		]}	));
R(Example 1,(@{CameraHsuz(0, 0, 0, 1),								@"parts":@[
			aGenerator(0, @{@"prob":@0.25,flip, @"P":@"bun"}),
//			aGenerator(0, @{@"events":@[ @[@"a"], @"again"],		@{flip, @"P":@"bun"})),
	o(bun,	aTunnel(@{@"structure":a_bundle, @"proto":aGenPrevBcastLeaf() })),
																		]}	));
M(Example 2, (@{CameraHsuz(0, 30, 10, 1),							@"parts":@[
			aGenerator(@{@"prob":@0.5, flip, @"P":@"bun"}),
	o(bun,	aTunnel(@{@"structure":ab_bundle, @"proto":aGenPrevBcastLeaf(0,0,0,@{spin$1}) })),
	o(B0,	aMinAnd(0, @[@"a.-", @"b.-"],				@{flip} )),
	o(B1,	aMinAnd(0, @[@"a.+", @"b.+"],				@{flip} )),
	o(C0,	aMaxOr( 0, @[@"B0", @"B1"],					@{flip} )),
	o(C1,	aMaxOr( 0, @[@"B0", @"B1"],					@{flip} )),
	o(D0,	aBroadcast( 0, @[@"C0", @"C1"],				@{flip} )),
			aMirror(@"D0"),
																		]}	));
r(			 (@{CameraHsuz(0, 0, 0, 1),								@"parts":@[
			aGenerator(0, @{@"prob":@0.6, flip, @"P":@"bun"}),
	o(bun,	aBundle(@{@"structure":ab_bundle, @"proto":aGenPrevBcastLeaf() })),
	o(D0,	aBroadcast(0, @[@"a.+", @"b.-"],			@{flip} )),
	o(D1,	aBroadcast(0, @[@"a.-", @"b.+"],			@{flip} )),
																		]}	));

// 160130 bug: aBroadcast bad Fixed: aGenPrevBcastLeaf binding 160202 fixed
r(			 (@{CameraHsuz(0, 0, 0, 1),								@"parts":@[
			aGenerator(50, @{@"events":@[@"a", @0, @"again"], flip, @"P":@"bun"}),
	o(bun,	aBundle(@{@"structure":a_bundle, @"proto":aGenPrevBcastLeaf() })),
	o(D0,	aBroadcast(0, @[@"a.+", @"a.-"],			@{flip} )),
																		]}	));
r(			 (@{CameraHsuz(0, 0, 0, 1), VEL(-6),					@"parts":@[
			aGenerator(50, @{@"events":@[@"a", @[], @"again"], flip, @"P":@"bun"}),
	o(bun,	aBundle(@{@"structure":a_bundle, @"proto":aGenPrevBcastLeaf() })),
	o(D0,	aBroadcast(0, @[@"a.+", @"a.-"],			@{flip} )),
			aMirror(@"D0"),
																		]}	));

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark * Blink, ABC
M(Blink, 	(@{CameraHsuz(0, 0, 0, 1),								@"parts":@[// 150329: short links
			aGenerator(50, @{@"prob":@0.6, flip, @"P":@"bun"}),
	o(bun,	aTunnel(@{@"structure":abcd_bundle, @"proto":aGenBcastLeaf() })),
	o(qa,	aMultiply(0, @[@"a", @"b", @"c"],			@{flip} )),
	o(qb,	aMultiply(0, @[@"c", @"d"],					@{flip} )),
	o(qc,	aMaxOr(   0, @[@"qa", @"qb"],				@{flip} )),
			aMirror(@"qc"),
																		]}	));
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark * Blink, ABC
id abcd$_bundle		= @[/*@"spin_1",*/ @"a", @"b", @"c", @"d"];

 // any value?
R(link, 	(@{CameraHsuz(0, 0, 10, 1), VEL(-6),					@"parts":@[// 150329: short links
			aGenerator(@{@"prob":@0.6, flip, @"P":@"bun"}),
	o(bun,	aTunnel(@{@"structure":ab_bundle, @"proto":aGenBcastLeaf() })),
	o(qa,	aMinAnd(0, @[@"a,l=5", @"b"],				@{flip} )),
			aMirror(@"qa"),
																		]}	));
 // Test of shrink I0.2( 0 )
R(link, 	(@{CameraHsuz(0, 0, 0, 1),@"boundsDrapes":@.1,			@"parts":@[
//			aGenerator(@{@"prob":0.6,								@{flip, @"P":@"bun"})),
	o(bun,	aTunnel(@{@"structure":a_bundle, @"proto":aGenBcastLeaf() })),
	o(qa,	aMinAnd(@"a", 0)),
																		]}	));
 // 160122 genBcast bug
r(			(@{CameraHsuz(0, 0, 0, 1),								@"parts":@[// 150329: short links
//	o(bun,	aGenBcastLeaf()),
	o(bun,	aTunnel(@{@"structure":a_bundle, @"proto":aGenBcastLeaf() })),
																		]}	));


///////////////////////////////////////////////////////////////////////////////
			////////////////////////////////////////////////
			/////				MODELS				   /////
			////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark * Latches
M(Latch 3, (@{CameraHsuz(0, 30, 12, 1.3), @"displayLeafNames":@1,	@"parts":@[
			aGenerator(@{@"nib":@"LoGen_abcde", flip, @"P":@"bun"}),
	o(bun,	aTunnel(@{@"structure":abcde_bundle, @"proto":aGenBcastLeaf() })),
//						@[@"x1", @"x2", @"e", @"e1", @"e2"]
//			aLabel(@"SUPERIORS",		6, @"0, 0, 9"),
	o(qd,	aHamming(0, @[@"d", @"c"],					@{flip} )),
	o(qe,	aHamming(0, @[@"e", @"c"],					@{flip} )),

	o(bunD,	aNet(@{}, @[
		o(md,	aMirror(0, @{flip, @"gain":@0.2, @"jog ":@"0,0,0"})),
		o(qd1,	aBroadcast(@"z", @[@"a", @"qd", @"md"],		@{flip} )),
	  ])),
	o(bunE,	aNet(@{}, @[
		o(me,	aMirror(0, @{flip, @"gain":@0.2, @"jog ":@"10,0,0"})),
		o(qe2,	aBroadcast(@"z", @[@"b", @"qe", @"me"],		@{flip} )),
	  ])),

	o(bun2,	aTunnel(@{@"structure":z_bundle, @"proto":aGenMaxLeaf(), flip})),
			aGenerator(@{@"events":@[], @"resetTo":@"z", @"P":@"bun2"}),
				 														]}	));
 // 160331 bug overlapping parts    okay 160411
R(Latch 3, (@{CameraHsuz(0, 0, 45, 1),								@"parts":@[
	o(bun,	aBundle(@{@"structure":a_bundle, @"proto":aBcastLeaf() })),
	o(ham1,	aHamming(0, @[@"a"],						@{flip} )),
	o(ham2,	aHamming(0, @[@"a"],						@{flip} )),
	o(ham3,	aHamming(0, @[@"a"],						@{flip} )),
	o(ham4,	aHamming(0, @[@"a"],						@{flip} )),
	o(ham5,	aHamming(0, @[@"a"],						@{flip} )),
	o(ham6,	aHamming(0, @[@"a"],						@{flip} )),
//	o(u1,	aBroadcast(0, @[@"a", @"ham1"],				@{flip} )),
//	o(u2,	aBroadcast(0, @[@"a", @"ham2"],				@{flip} )),
				 														]}	));
 // 161105 BUG Hamming share placement
R(Latch 3, (@{CameraHsuz(0, 0, 0, 1.5), @"boundsDrapes":@.1,		@"parts":@[
	o(bun,	aBundle(@{@"structure":ab_bundle, @"proto":aBcastLeaf() })),
	o(ham1,	aHamming(0, @[@"a", @"b"],					@{flip} )),
				 														]}	));
 // 161106 bug 161107 Hamming share placement
R(Latch 3, (@{CameraHsuz(0, 0, 0, 1.5), @"boundsDrapes":@.1, @"displayAxis":@1, @"parts":@[
	o(h1,	aBroadcast(0, 0,							@{flip} )),
	o(h2,	aHamming(0, @[@"h1,l=2"], @{@"jog":@"4,0,0", flip} )),
				 														]}	));
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark Dynamic Bindings
////////////////////////////////////////////////
/////     Dynamic Bindings				   /////
////////////////////////////////////////////////

id y3x3_2_bundle = @{spin$R,
				@"mana":    @[spin_R, @"health", @"hunger"],
				@"retina":  y3x3_bundle};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark * 3x3 empty
M(3x3 empty, (@{CameraHsuz(0, 30, 12, 1.3), @"fontNumber":@0,		@"parts":@[
			aLabel(@"WORLD",			6, @"0,  0, 0"),
			aLabel(@"HaveNWant Schema",	6, @"0, 12, 0"),
			aLabel(@"SUPERIORS",		6, @"0, 22, 0"),
			aGenerator(@{@"events":@[p3x3(rVal 1)], flip, @"P":@"bun"}),
//			aGenerator(0, @{@"prob":@0.4, flip, @"xP":@"bun"}),
	o(bun,	aTunnel(@{@"structure":y3x3_2_bundle, @"proto":aGenBcastLeaf() })),
	o(bun2,	aTunnel(@{@"structure":abcde_bundle, @"proto":aGenBcastLeaf(), flip, @"jog":@"0,5,0"})),
																	]}	));

r(			(@{CameraHsuz(0, 0, 0, 0.8),							@"parts":@[
			 aGenerator(50, @{@"prob":@0.6, flip, @"P":@"retina"}),
	o(retina,aTunnel(@{@"structure":y1x1_bundle, @"proto":aGenBcastLeaf() })), //y0x0_bundle, y2x2_bundle
																		]}	));
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark * Bid 2x2 values
//																			];
M(2x2-randValue,(@{CameraHsuz(0, 30, 20, 1.3), @"fontNumber":@0,	@"parts":@[
//			aGenerator(@{@"events":@[p6x6(rVal 1),@"again"], flip, @"P":@"bun"})),
			aGenerator(@{@"prob":@0.4,flip, @"P":@"bun"}),
	o(bun,	aTunnel(@{@"structure":b2D2_bundle, @"proto":aGenBcastLeaf() })),
	o(F43,	aMaxOr(0, @[@"y0x0", @"y0x1", @"y1x0", @"y1x1"], @{flip} )),
	o(F56,	aMirror(@"F43")),	/* 2D with 1-layer tree	//++1	*/
																		]}	));
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark * Bid 6x6 values
//																			];
id b2D6_bundle	= @{@"board":y6x6_bundle};
M(6x6-randValue,(@{CameraHsuz(0, 30, 20, 1.3), @"fontNumber":@0, @"fluffLinkGap":@6, @"parts":@[
//				aGenerator(@{@"events":@[p6x6(rVal 1),@"again"],	@{flip, @"P":@"bun"})),
			aGenerator(-1, @{@"prob":@0.4, flip, @"P":@"bun"}),
	o(bun,	aTunnel(@{@"structure":b2D6_bundle, @"proto":aGenBcastLeaf() })),
	o(F43,	aMaxOr(0, @[@"y0x0", @"y1x0", @"y0x1", @"y1x1"], @{flip} )),
	o(F44,	aMaxOr(0, @[@"y0x2", @"y1x2", @"y0x3", @"y1x3"], @{flip} )),
	o(F45,	aMaxOr(0, @[@"y0x4", @"y1x4", @"y0x5", @"y1x5"], @{flip} )),
			
	o(F46,	aMaxOr(0, @[@"y2x0", @"y3x0", @"y2x1", @"y3x1"], @{flip} )),
	o(F47,	aMaxOr(0, @[@"y2x2", @"y3x2", @"y2x3", @"y3x3"], @{flip} )),
	o(F48,	aMaxOr(0, @[@"y2x4", @"y3x4", @"y2x5", @"y3x5"], @{flip} )),
			
	o(F49,	aMaxOr(0, @[@"y4x0", @"y5x0", @"y4x1", @"y5x1"], @{flip} )),
	o(F50,	aMaxOr(0, @[@"y4x2", @"y5x2", @"y4x3", @"y5x3"], @{flip} )),
	o(F51,	aMaxOr(0, @[@"y4x4", @"y5x4", @"y4x5", @"y5x5"], @{flip} )),

	o(F52,	aMaxOr(0, @[@"F43", @"F44", @"F46", @"F47"],	  @{flip} )),
	o(F53,	aMaxOr(0, @[@"F45", @"F48"], @{flip} )),
	o(F54,	aMaxOr(0, @[@"F49", @"F50"], @{flip} )),
			
	o(F55,	aMaxOr(0, @[@"F52", @"F53", @"F54", @"F51"],	  @{flip} )),
	o(F56,	aMirror(@"F55")),	/* 2D with 3-layer tree	//++1	*/
																		]}	));

////////////////////////////////////////////////
/////        ASSIMILATORS				   /////
////////////////////////////////////////////////

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark - B. ASSIMILATORS
/************************/ setParentMenu(@"B. Assimilators"); /***************/

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark * Cat Dog
id cadog_bundle = @[@"dog", @"meow", @"purr", @"bark", @"cat"];
id cadog_events_today = @[
	@[@"cat", @"meow"],
	@[@"cat", @"meow", @"purr"],
	@[@"dog", @"bark"],												@"again"];

/*
“cat dog" runs a simple three element sequence. 
The first time through, new Hamming elements are added with the ’s’ key. 
’s’ down presents the pattern to the existing model. 
Unrecognized inputs go to a write head to create a new element, shown. 
’s’ up releases the element to the associative model being formed.
Presumably the model recognizes the pattern, and the unknowns vanish.
*/
xM(cat dog,		(@{CameraHsuz(0, -30, 15, 1.6),  @"displayLeafNames":@1, @"parts":@[
	aGenerator(0, @{@"events":cadog_events_today, flip, @"P":@"act1/evi"}),
	anActor(@{n(act1),
		@"minHeight":@13,
		@"evi":aTunnel(@{@"structure":cadog_bundle, @"proto":aGenBcastLeaf(0, @{@"U":@"write"}) }),
		@"con":aBundle(@{@"structure":z_bundle, @"proto":aGenMaxsqLeaf(@{@"value":@1.0}, @{@"U":@"write"}), flip}),
	}),
	aWriteHead(@{@"actor":@"act1", @"babyPrototype":@"Hamming"}),
																		]}	));

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark * B. Cat Dog Owl
id animals_bundle = @[@"cat", @"dog", @"owl",
					  @"meow", @"arf", @"sc", @"hiss"];
id animals_events_todayD = @[
	@[@"owl", @"hiss"],
	@[@"owl", @"hiss", @"sc"],
	@[@"cat", @"meow"],
	@[@"dog", @"arf"],												@"again"];

//URL factalWorkbench://B.%20Assimilators/B.%20cat%20dog%20owl
M(B. cat dog owl,	(@{CameraHsuz(0, -50, 10, 1.5),  @"displayLeafNames":@1, @"parts":@[
	aGenerator(0, @{@"events":animals_events_todayD,	flip, @"P":@"act1/evi"}),
	anActor(@{ n(act1), @"minHeight":@(minHeight),
		@"evi":aTunnel(@{@"structure":animals_bundle, @"proto":aGenBcastLeaf(0, @{@"U":@"write"}) }),
		@"con":aBundle(@{@"structure":z_bundle, @"proto":aGenMaxsqLeaf(@{@"value":@1.0}, @{@"U":@"write"}), flip}),
	}),
	aWriteHead(@{@"actor":@"act1", @"babyPrototype":@"Hamming"}),
																		]}	));
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark * B1-easy
M(B1-easy,	 (@{CameraHsuz(0, 45, 10, 1.5), 	@"parts":@[
	aGenerator(0, @{@"events":@[@[@"a"], @[], @"again"], flip, @" resetTo":@"a", @"P":@"act1/evi"}),
	anActor(@{n(act1), @"minHeight":@5,
		@"evi":aTunnel(@{@"structure":a_bundle, @"proto":aGenBcastLeaf(0,@{@"xB":@"y", @"U":@"write"}) }),
		@"con":aBundle(@{@"structure":z_bundle, @"proto":aGenMaxsqLeaf(@{@"value":@1.0}, @{@"U":@"write"}), flip }),
	}),
	aWriteHead(@{@"actor":@"act1", @"babyPrototype":@"Hamming"}),
																		]}	));


 // 160223 bug placement bad FIXED 160226
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark * B2-easy
M(B2-easy,	 (@{CameraHsuz(0, 45, 10, 1.5), 	@"parts":@[
	aGenerator(0, @{@"events":@[@[@"a"], @[], @[@"b"], @[], @"again"], flip, @" resetTo":@"a", @"P":@"act1/evi"}),
	anActor(@{n(act1), @"minHeight":@5,
		@"evi":aTunnel(@{@"structure":ab_bundle, @"proto":aGenBcastLeaf(0,@{@"xB":@"y", @"U":@"write"}) }),
		@"con":aTunnel(@{@"structure":yz_bundle, @"proto":aGenMaxsqLeaf(0, @{@"U":@"write"}), flip }),
			  // 171001 -- NOW BOTH y and z are ON
	}),
	aWriteHead(@{@"actor":@"act1", @"babyPrototype":@"Hamming"}),
//. THIS IS A 2-BundleTap TEST
	aGenerator(@{@"events":@[], @"resetTo":@[@"z"], @"P":@"act1/con"}),
																		]}	));
 // 161107 bug 161107 doesn't give birth
 // 170907 BUG Births twice.
R(Dual Birth, (@{CameraHsuz(0, 45, 10, 1), 				@"parts":@[
	aGenerator(0, @{@"events":@[@[], @[@"a"], @"again"],flip, @" resetTo":@"a", @"P":@"act1/evi"}),
	anActor(@{n(act1),	@"minHeight":@5,
		@"evi":aTunnel(@{@"structure":a_bundle, @"proto":aGenBcastLeaf(0, @{@"U":@"write"}) }),
		@"con":aBundle(@{@"structure":z_bundle, @"proto":aGenMaxsqLeaf(@{@"value":@1.0}, @{@"U":@"write"}), flip }),
	}),
	aWriteHead(@{@"actor":@"act1", @"babyPrototype":@"Hamming"}),
																		]}	));

id p4bug0	= @[ @[@"a"], @[],										@"again"];
id p4bug33	= @[ @[@"a"], @[@"b", @"c"], @[@"a", @"d"],				@"again"];

id p2_hard1 = @[@[@"a"], @[@"a", @"b"],								@"again"];
id p2_hard2 = @[@[@"a"], @[@"b"], @[@"a", @"b"],					@"again"];
id p2_easy2 = @[@[@"a", @"b"], @[@"b"],								@"again"];

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark * B2-easy2
M(B2-easy2, (@{CameraHsuz(0, 45, 10, 1.5), 		@"parts":@[
	aGenerator(1, @{@"events":p2_easy2, flip, @"P":@"act1/evi"}),
	anActor(@{n(act1), @"minHeight":@(minHeight),
		@"evi":aTunnel(@{@"structure":ab_bundle, @"proto":aGenBcastLeaf(0, @{@"U":@"write"}) }),
		@"con":aBundle(@{@"structure": z_bundle, @"proto":aGenMaxsqLeaf(@{@"value":@1.0}, @{@"U":@"write"}), flip }),
	}),
	aWriteHead(@{@"actor":@"act1", @"babyPrototype":@"Hamming"}),
																		]}	));
// 161201 BUG BundleTap appears twice FIXED
R(B2-BundleTap appears twice, (@{CameraHsuz(0, 45, 10, 1.5), 		@"parts":@[
	aGenerator(1, @{@"events":p2_easy2, flip, @"P":@"act1/evi"}),
	anActor(@{n(act1), @"minHeight":@(minHeight),
		@"evi":aTunnel(@{@"structure":ab_bundle, @"proto":aGenBcastLeaf(0, @{@"U":@"write"}) }),
		@"con":aBundle(@{@"structure":z_bundle, @"proto":aGenMaxsqLeaf(@{@"value":@1.0}, @{@"U":@"write"}), flip }),
	}),
	aWriteHead(@{@"actor":@"act1", @"babyPrototype":@"Hamming"}),
																		]}	));
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark * B3-conFlip
//////	aLabel(		@"WORLD",   7, @"15,  0, 15"),
//////	aLabel(		@"PARTS",   7, @"0,  20,  0"),
//////	aLabel(		@"UNKNOWN", 7, @"0,   8, 10"),
//////	aLabel(		@"CONTEXTS",7, @"0,  34, 10"),
R(B3-conFlip, (@{CameraHsuz(0, 45, 10, 1.0),						@"parts":@[
	aGenerator(0, @{@"events":p2_easy2, flip, @"P":@"act1/evi"}),
	anActor(@{n(act1),
		@"evi":aTunnel(@{@"structure":a_bundle, @"proto":aGenBcastLeaf() }),
		@"con":aTunnel(@{@"structure":z_bundle, @"proto":aGenMaxsqLeaf(), flip }),
 /// needed?
//	//	@"con.flip":@1,							}),
//	//	@"con.info":@{flip},					}),
	}),
	aLink(@"a", @"z"),
																		]}	));
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark * B4-tree
id p4chain4 = @[ @[@"a"], @[@"a", @"b"], @[@"a", @"b", @"c"],
				 @[@"a", @"b", @"c", @"d"],							@"again"];
id p4chain4b =@[ @[@"a"],											@"again"];

M(B4-hard4,		(@{CameraHsuz(0, -45, 10, 1.5), 					@"parts":@[
	aGenerator(1, @{@"events":p4chain4, flip, @"P":@"act1/evi"}),
	anActor(@{n(act1),
		@"minHeight":@(minHeight),
		@"evi":aTunnel(@{@"structure":abcd_bundle, @"proto":aGenBcastLeaf(0, @{@"U":@"write"}) }),
		@"con":aBundle(@{@"structure":z_bundle, @"proto":aGenMaxsqLeaf(@{@"value":@1.0}, @{@"U":@"write"}), flip }),
	}),
	aWriteHead(@{@"actor":@"act1", @"babyPrototype":@"Hamming"}),
																		]}	));
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark * B4-hard2, bugs
M(B4-runt2, 	(@{CameraHsuz(0, 45, 10, 1.3),						@"parts":@[
	aGenerator(0, @{@"events":p4bug33, flip, @"P":@"act1/evi"}),
	anActor(@{n(act1),
		@"minHeight":@(minHeight),
		@"evi":aTunnel(@{@"structure":abcd_bundle, @"proto":aGenBcastLeaf(0, @{@"U":@"write"}) }),
		@"con":aBundle(@{@"structure":z_bundle, @"proto":aGenMaxsqLeaf(@{@"value":@1.0}, @{@"U":@"write"}), flip }),
	}),
	aWriteHead(@{@"actor":@"act1", @"babyPrototype":@"Hamming"}),
																		]}	));
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark *B4-funnyCase
// This case shows a weakness in the associative memory - building algorithm
id funnyCase = @[@[@"a", @"b"], @[@"c", @"d"],   @[@"a", @"b", @"c", @"d"], @"again"];
M(B4-funnyCase, 	(@{CameraHsuz(0, 45, 10, 1.3),					@"parts":@[
	aGenerator(0, @{@"events":funnyCase, flip, @"P":@"act1/evi"}),
	anActor(@{n(act1),
		@"minHeight":@(minHeight),
		@"evi":aTunnel(@{@"structure":abcd_bundle, @"proto":aGenBcastLeaf(0, @{@"U":@"write"}) }),
		@"con":aBundle(@{@"structure":z_bundle, @"proto":aGenMaxsqLeaf(@{@"value":@1.0}, @{@"U":@"write"}), flip }),
	}),
	aWriteHead(@{@"actor":@"act1", @"babyPrototype":@"Hamming"}),
																		]}	));
	R(B4-funnyCase, 	(@{CameraHsuz(0, 45, 10, 1.3),					@"parts":@[
		aGenerator(0, @{@"events":@[@[@"a", @"b"], @"again"], flip, @"P":@"act1/evi"}),
		anActor(@{n(act1),
			@"minHeight":@(minHeight),
			@"evi":aTunnel(@{@"structure":ab_bundle, @"proto":aGenBcastLeaf(0, @{@"U":@"write"}) }),
			@"con":aBundle(@{@"structure":z_bundle, @"proto":aGenMaxsqLeaf(@{@"value":@1.0}, @{@"U":@"write"}), flip }),
		}),
		aWriteHead(@{@"actor":@"act1", @"babyPrototype":@"Hamming"}),
																			]}	));

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark * B5-rand=7
//id p8X5 = @[ @"repeat 5", @[@"a=rnd .5",@"b=rnd .5",@"c=rnd .5",@"d=rnd .5",
//				@"e=rnd .5",@"f=rnd .5",@"g=rnd .5",@"h=rnd .5"]];
id p8X5 = @[ @"repeat 5", @[@"a=rnd .5",@"b=rnd .5",@"c=rnd .5",@"d=rnd .5",
				@"e=rnd .5",@"f=rnd .5",@"g=rnd .5",@"h=rnd .5"],	@"again"];
 // 160524 BUG placing 4'th Hamming
M(B5-randX7, 		(@{CameraHsuz(0, 45, 10, 1.6),					@"parts":@[
	aGenerator(0, @{@"events":eventUnrand(p8X5), flip, @"P":@"act1/evi"}),
	anActor(@{n(act1),    @"minHeight":@(minHeight),
		@"evi":aTunnel(@{@"structure":abcdefgh_bundle, @"proto":aGenBcastLeaf(0, @{@"U":@"write"}) }),
		@"con":aBundle(@{@"structure":z_bundle, @"proto":aGenMaxsqLeaf(@{@"value":@1.0}, @{@"U":@"write"}), flip }),
	}),
	aWriteHead(@{@"actor":@"act1", @"babyPrototype":@"Hamming"}),
																		]}	));
 // Shooting bug: (hit 'P') FIXED 160524 by expedient: don't position by WriteHeads
R(B5-randX7, (@{CameraHsuz(0, 45, 10, 1),  @"boundsDrapes":@0.1,
						@"displayAxis":@1,							@"parts":@[
	aGenerator(0, @{@"events":@[@"a", @"", @"again"], flip, @"P":@"act1/evi"}),
	anActor(@{n(act1),	@"minHeight":@(minHeight),
		@"evi":aTunnel(@{@"structure":a_bundle, @"proto":aGenBcastLeaf(0, @{@"U":@"write"}) }),
		@"con":aBundle(@{@"structure":z_bundle, @"proto":aGenMaxsqLeaf(@{@"value":@1.0}, @{@"U":@"write"}), flip }),
//		@"con":aBundle(@{@"structure":z_bundle, @"proto":aGenBcastLeaf(@{@"value":@1.0}, @{@"U":@"write"}), flip }),
	}),
	aWriteHead(@{@"actor":@"act1", @"babyPrototype":@"Hamming"}),
																		]}	));
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark * B6 2x2 rand5
id p2x2X5 = @[@"repeat 5", p2x2(rnd 0.7),							@"again"];
id event44 = 0? eventUnrand(p2x2X5): @[@[@"y0x0", @"y0x1", @"y1x0", @"y1x1"], @[@"y0x1", @"y1x0"], @"again"];
M(B6 2x2 rand5,(@{CameraHsuz(-2, 30, 20, 1.8),						@"parts":@[
	aGenerator(@{@"events":event44,flip, @"P":@"act1/evi"}),
	anActor(@{n(act1),	@"minHeight":@(minHeight),
		@"evi":aTunnel(@{@"structure":y2x2_bundle, @"proto":aGenBcastLeaf(0, @{@"U":@"write"}) }),
		@"con":aBundle(@{@"structure":z_bundle, @"proto":aGenMaxsqLeaf(@{@"value":@1.0}, @{@"U":@"write"}), flip }),
	}),
	aWriteHead(@{@"actor":@"act1", @"babyPrototype":@"Hamming"}),
																		]}	));
	id event45 = @[@[@"a", @"b"], @"a", @"b", @"again"];
	R(x,(@{CameraHsuz(-2, 30, 20, 1.8),						@"parts":@[
		aGenerator(@{@"events":event45,flip, @"P":@"act1/evi"}),
		anActor(@{n(act1),	@"minHeight":@(minHeight),
			@"evi":aTunnel(@{@"structure":ab_bundle, @"proto":aGenBcastLeaf(0, @{@"U":@"write"}) }),
			@"con":aBundle(@{@"structure":z_bundle, @"proto":aGenMaxsqLeaf(@{@"value":@1.0}, @{@"U":@"write"}), flip }),
		}),
		aWriteHead(@{@"actor":@"act1", @"babyPrototype":@"Hamming"}),
																			]}	));
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark * B7 3x3 rand5
id p3x3X5 = @[@"repeat 5", p3x3(rnd 0.14),							@"again"];
// 161110 BUG:  @"randomSeed":@352 ineffective
M(B7 3x3 rand5,(@{CameraHsuz(-2, 30, 20, 2),  @"randomSeed":@352,	@"parts":@[
	aGenerator(1, @{@"events":eventUnrand(p3x3X5), flip, @"P":@"act1/evi"}),
	anActor(@{n(act1), @"minHeight":@(minHeight),
		@"evi":aTunnel(@{@"structure":y3x3_bundle, @"proto":aGenBcastLeaf(0, @{@"U":@"write"}) }),
		@"con":aBundle(@{@"structure":z_bundle, @"proto":aGenMaxsqLeaf(@{@"value":@1.0}, @{@"U":@"write"}), flip }),
	}),
	aWriteHead(@{@"actor":@"act1", @"babyPrototype":@"Hamming"}),
																		]}	));
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark * B8 5x5 rand5
id p5x5X5 = @[@"repeat 5", p5x5(rnd 0.14),							@"again"];
M(B8 5x5: random values, (@{CameraHsuz(-2, 30, 20, 1.3), VEL(-3),
						@"fontNumber":@0, @"fluffLinkGap":@6,		@"parts":@[
	aGenerator(@{@"events":eventUnrand(p5x5X5),flip, @"P":@"act1/evi"}),
	anActor(@{n(act1),
		@"minHeight":@(minHeight),
		@"evi":aTunnel(@{@"structure":y5x5_bundle, @"proto":aGenBcastLeaf(0, @{@"U":@"write"}) }),
		@"con":aBundle(@{@"structure":z_bundle, @"proto":aGenMaxsqLeaf(@{@"value":@1.0}, @{@"U":@"write"}), flip }),
	}),
	aWriteHead(@{@"actor":@"act1", @"babyPrototype":@"Hamming"}),
																		]}	));
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark * B9 3x2-pat3
id p3_111 = @[ @[@"a"], @[@"b"], @[@"c"],							@"again"];

id p3_twoOfthree = @[
	@[@"a", @"b"], @[@"b", @"c"], @[@"c", @"a"],
																	@"again"];
id p3_specialCases = @[
	@[@"a",				@"d", @"e", @"f"],
	@[@"a", @"b",	          @"e", @"f"],
	@[@"a", @"b", @"c",	            @"f"],
																	@"again"];

M(B9 3x2-pat3,	(@{CameraHsuz(0, 80, 10, 1.7),						@"parts":@[
//	aGenerator(@{@"events":p3_twoOfthree,  flip, @"P":@"act1/evi"}),
	aGenerator(@{@"events":p3_specialCases,flip, @"P":@"act1/evi"}),
//	aGenerator(@{@"events":p3_111,		  flip, @"P":@"act1/evi"}),
	anActor(@{n(act1), @"minHeight":@(minHeight),
		@"evi":aTunnel(@{@"structure":abc_def_bundle, @"proto":aGenBcastLeaf(0, @{@"U":@"write"}) }),
		@"con":aBundle(@{@"structure":z_bundle, @"proto":aGenMaxsqLeaf(@{@"value":@1.0}, @{@"U":@"write"}), flip }),
	}),
	aWriteHead(@{@"actor":@"act1", @"babyPrototype":@"Hamming"}),
																		]}	));

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark - C. Sequential Assimilators
/************************/ setParentMenu(@"C. Sequential Assimilators"); /***************/

id seq1		= @[@[@"a"],											@"again"];
id seq2		= @[@[@"a"], @[@"b"],									@"again"];
id seq3		= @[@[@"a"], @[@"b"], @[@"c"],							@"again"];
id seq4		= @[@[@"a"], @[@"b"], @[@"c"], @[@"d"],					@"again"];
id seq5		= @[@[@"a"], @[@"b"], @[@"c"], @[@"d"], @[@"e"],		@"again"];
id seq6		= @[@[@"a"], @[@"b"], @[@"c"], @[@"d"], @[@"e"],@[@"f"],@"again"];
id seq4pm	= @[@[@"a"], @[@"c"], @[@"d"], @[@"b"],					@"again"];
id seqBug1	= @[@[@"a"], @[@"b"], @[@"f"], @[@"e"], @[@"e"],		@"again"];

//URL factalWorkbench://C.%20Sequential%20Assimilators/C.%20S3-ring3
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark * C. S3-ring3
M(C. S3-ring3, (@{CameraHsuz(0, -45, 15, 1.5),						@"parts":@[
	aGenerator(0, @{@"events":@[@[@"a"], @[@"b"], @[@"c"], @"again"],flip, @"P":@"act1/evi"}),
	anActor(@{n(act1),	@"minHeight":@(minHeight),
		@"evi":aTunnel(@{@"structure":abc_bundle, @"proto":aGenPrevBcastLeaf(0, 0, @{@"U":@"write"}) }),
		@"con":aBundle(@{@"structure":z_bundle,   @"proto":aGenMaxsqLeaf(@{@"value":@1.0}, @{@"U":@"write"}), flip }),
	 }),
	aWriteHead(@{@"actor":@"act1", @"babyPrototype":@"Hamming"}),
																		]}	));
	R(C. S3-ring3, (@{CameraHsuz(0, -45, 15, 1.5),						@"parts":@[
		aGenerator(0, @{@"events":@[@[@"a"], @[@"b"], @"again"], @"P":@"evi", flip}),
		aTunnel(@{n(evi), @"structure":ab_bundle, @"proto":aGenPrevBcastLeaf(0, 0, @{@"xU":@"write"}) }),
																			]}	));
 // 161025 bug birth hang: Fixed 161113
r(			(@{CameraHsuz(0, 30, 0, 1.0),  SIM_ON,					@"parts":@[//(0, -45, 15, 1.5)
	aGenerator(@{@"events":@[@[@"a"], @[@"a"], @[], @"again"],flip, @"P":@"act1/evi"}),
	anActor(@{n(act1),	@"minHeight":@10,
		@"evi":aTunnel(@{@"structure":a_bundle, @"proto":aGenPrevBcastLeaf(0, 0, @{@"U":@"write"}) }),
		@"con":aBundle(@{@"structure":z_bundle, @"proto":aGenMaxsqLeaf(@{@"value":@1.0},@{@"U":@"write"}), flip }),
		}),
	aWriteHead(@{@"actor":@"act1", @"babyPrototype":@"Hamming"}),
																		]}	));

// WriteHead with Prev's -- 0-port
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark * S1-ring2
//M(debug,	(@{CameraHsuz(0, 45,  30, 1.5),					@"parts":@[]}	));
M(S1-blink,	(@{CameraHsuz(0, 45,  30, 1.5),							@"parts":@[
	aGenerator(0, @{@"events":@[@[@"a"],@[@"a"],@[],@[],@"again"],flip, @"P":@"act1/evi"}),
	anActor(@{n(act1),	@"minHeight":@(minHeight),
		@"evi":aTunnel(@{@"structure":a_bundle, @"proto":aGenPrevBcastLeaf(0,0,@{@"U":@"write"}) }),
		@"con":aBundle(@{@"structure":z_bundle, @"proto":aGenMaxsqLeaf(@{@"value":@1.0},@{@"U":@"write"}), flip }),
	}),
	aWriteHead(@{@"actor":@"act1", @"babyPrototype":@"Hamming"}),
																		]}	));
 // 160807 actors missing 'previous;
R(S1-blink,	(@{CameraHsuz(0, 80,  10, 1),							@"parts":@[
	aGenerator(@{@"events":@[@[@"a"],@[@"a"],@[],@[],@"again"], flip, @"P":@"act1/evi"}),
	anActor(@{n(act1),	@"minHeight":@(minHeight),
		@"evi":aTunnel(@{@"structure":a_bundle, @"proto":aGenPrevBcastLeaf() }),
	}),
																		]}	));
 // 160208 actor positioning bug
R(S1-blink,	(@{CameraHsuz(0, 0, 0, 1),								@"parts":@[
	anActor(@{n(act1),	@"minHeight":@0,
		@"evi":aTunnel(@{@"structure":a_bundle, @"proto":aBcastLeaf() }),
		@"con":aTunnel(@{@"structure":z_bundle, @"proto":aBcastLeaf(), flip }),
	}),
																		]}	));
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark * S2-ring2
// WriteHead with Prev's -- 2-port
// 180805 BUG "again" resets prev's L, so wrap (b- & a+) is bad
M(S2-ring2, (@{CameraHsuz(0, 45, 30, 1),							@"parts":@[
	aGenerator(0, @{@"events":@[@"a", @"b", @"again"], flip, @"P":@"act1/evi"}),
//	aGenerator(@{n(loGen"@[@[@"a"],@[@"a",@"b"],@[],@"again"], flip, @"P":@"act1/evi"})),
	anActor(@{n(act1),	@"minHeight":@(minHeight),
		@"evi":aTunnel(@{@"structure":ab_bundle, @"proto":aGenPrevBcastLeaf(0,0,@{@"U":@"write"}) }),
		@"con":aBundle(@{@"structure":z_bundle, @"proto":aGenMaxsqLeaf(@{@"value":@1.0},@{@"U":@"write"}), flip }),
	}),
	aWriteHead(@{@"actor":@"act1", @"babyPrototype":@"Hamming"}),
																		]}	));

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark * S6-ring6
 //160424 BUG: Hitting 'S' skips writing (b- & c+) Hamming, 's' okay -- want in separate process
// 180805 BUG "again" resets prev's L, so wrap (e- & a+) is bad
id seq6b		= @[@[@"a"], @[@"b"], @[@"c"], @[@"d"],@[@"f"], @[@"e"],@"again"];
M(S6-ring6, (@{CameraHsuz(0, 45, 10, 1.5),	VEL(-3),				@"parts":@[
	aGenerator(-1, @{@"events":seq6b, flip, @"P":@"act1/evi"}),
	anActor(@{n(act1), @"minHeight":@(minHeight),
		@"evi":aTunnel(@{@"structure":abcdef_bundle, @"proto":aGenPrevBcastLeaf(0,@{spin$1}, @{@"U":@"write"}) }),
		@"con":aBundle(@{@"structure":z_bundle, @"proto":aGenMaxsqLeaf(@{@"value":@1.0},@{@"U":@"write"}), flip }),
	}),
	aWriteHead(@{@"actor":@"act1", @"babyPrototype":@"Hamming"}),
																		]}	));


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark - H. Small Worlds
#pragma mark * H1 Necker Cube GUIs
/************************/ setParentMenu(@"Necker Cubes"); /***************/
////////////////////////////////////////////////
/////              Necker Cubes            /////
////////////////////////////////////////////////
// To Do: top levels of Hammings need domain value crossings of some sort.

M(Nec5+gui,	(@{CameraHsuz(0, 0, 0, 2), @" displayPortNames":@1, @"fontNumber":@"6", @"displayLeafNames":@1, @"parts":@[
	aGenerator(@{@"nib":@"Necker5", @"resetTo":@[@"b=0.5", @"c"], flip, @"P":@"bun"}),
	aTunnel(@{n(bun), @"structure":abcde_bundle, @"proto":aGenBcastLeaf() }),
	aMirror      (@"qa",				@{n(qe), flip, @"gain":@0.05}),
	aMirror      (@"qb",				@{n(qf), flip, @"gain":@0.05}),
	aHamming(0, @[@"a", @"b", @"c"],	@{n(qa), flip} ),
	aHamming(0, @[@"c", @"d", @"e"],	@{n(qb), flip} ),
	aMaxOr(@"z",@[@"qa,min=-5", @"qb,min=-5"],	@{n(qc), flip, @"onlyPosativeWinners":@0} ),
	aLabel(		@"A",    6, @"  8, 22, 0"),
	aLabel(		@"B",    6, @"-10, 20, 0"),
	aBundle(@{n(bun2), @"structure":z_bundle, @"proto":aGenLeaf(), flip}),
	aGenerator(@{@"nib":@"HiGen_z", @"P":@"bun2"}),
																]}	));
M(Nec5b+gui,	(@{CameraHsuz(0, 0, 0, 2), @"displayPortNames":@1, @"fontNumber":@"4", @"displayLeafNames":@1, @"parts":@[
	aGenerator(@{@"nib":@"Necker5", @"resetTo":@[@"b=0.5", @"c"], flip, @"P":@"bun"}),
	aTunnel(@{n(bun), @"structure":abcde_bundle, @"proto":aGenBcastLeaf() }),
	aHamming(0, @[@"a", @"b", @"c"],	@{n(qa), flip} ),
	aHamming(0, @[@"d", @"e"],			@{n(qe), flip} ),
	aHamming(0, @[@"c", @"qe"],			@{n(qb), flip} ),
	aMaxOr(  0, @[@"qa", @"qb"],		@{n(qc), flip, @"onlyPosativeWinners":@0} ),
																]}	));
r(			(@{CameraHsuz(0, 0, 0, 1),								@"parts":@[
			aGenerator(@{@"events":@[@[@"a"],@[@"b"],@[],@"again"],flip, @"P":@"bun"}),
//			aGenerator(@[ @[@[@"a"], @[]], @[@"b"],@"again"],@{flip, @"P":@"bun"})),
	o(bun,	aTunnel(@{@"structure":ab_bundle, @"proto":aGenBcastLeaf() })),
			aMirror(@"a"),
			aMirror(@"b"),
																		]}	));
M(Nec7+gui,	(@{CameraHsuz(0, 0, 0, 1), @"displayPortNames":@1, @"fontNumber":@"4", @"displayLeafNames":@1, @"parts":@[
//			aGenerator(50, 0.6,							@{flip, @"P":@"bun"})),
			aGenerator(50, @{@"nib":@"Necker7", @"resetTo":@[@"g=0.5", @"d"], flip, @"P":@"bun"}),
	o(bun,	aTunnel(@{@"structure":abcdefg_bundle, @"proto":aGenBcastLeaf() })),
	o(qa,	aHamming(0, @[@"a", @"b", @"c", @"d"],		@{flip} )),
	o(qb,	aHamming(0, @[@"d", @"e", @"f", @"g"],		@{flip} )),
	o(qc,	aMaxOr(@"z",@[@"qa", @"qb"],				@{flip, @"onlyPosativeWinners":@0} )),
//			aMirror(@"qc", @{@"gain":@1e-6, @"offset":@1}),
	aLabel(		@"A",    6, @"  8, 22, 0"),
	aLabel(		@"B",    6, @"-10, 20, 0"),
	aBundle(@{n(bun2), @"structure":z_bundle, @"proto":aGenLeaf(), flip}),
	aGenerator(@{@"nib":@"HiGen_z", @"P":@"bun2"}),

																		]}	));


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma Connection Network
 // 160906 Shows targets and users of a Connection Network
M(Connection Network, (@{											@"parts":@[
			aGenerator(0, @{@"nib":@"LoGen_abcd", @"resetTo":@[@"a"], flip, @"P":@"bun"}),
//			aGenerator(0, @{@"nib":@"LoGen_abcd", @"resetTo":@[@"a/gen"], flip, @"P":@"bun"}),
	o(bun,	aTunnel(@{@"structure":abcd_bundle, @"proto":aGenBcastLeaf() })),
	o(qc,	aMaxOr   (0, @[@"a", @"b", @"c", @"d"],		@{flip} )),
			//aMirror(@"qc"),	aMirror(@"z",				@{flip} ),
	o(qd,	aMaxOr   (@"qc", @[@"x", @"y", @"z"])),
	o(bun2,	aTunnel(@{@"structure":xyz_bundle, @"proto":aGenBcastLeaf(), flip})),
			aGenerator(@{@"nib":@"HiGen_xyz", @"resetTo":@[@"z"], @"P":@"bun2"}),
																	]}	));
r(			(@{CameraHsuz(0, 45, 10, 1),							@"parts":@[
			aGenerator(0, @{@"nib":@"LoGen_a", flip, @"P":@"bun"}),
	o(bun,	aTunnel(@{@"structure":a_bundle, @"proto":aGenBcastLeaf() })),
			aMirror(@"a"),
																		]}	));

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark * H2 Grace
/************************/ setParentMenu(@"Grace"); /***************/
////////////////////////////////////////////////
/////     Grace							   /////
////////////////////////////////////////////////
									  // First attempts at hallucination:
id walk_bundle	= @{
			@"a":@"b",
//			@"board":	@[/*spin$R*/  @"x0",  @"x1",  @"x2",  @"x3"],
			@"previous":@[/*spin$R,*/ @"x0",  @"x1",  @"x2",  @"x3"],
//			@"previous":@[/*spin$R,*/ @"^x0", @"^x1", @"^x2", @"^x3"],
//			@"dirT":	@{NKZ  @"-t",  @"=t",  @"+t"}, };
			@"dirT":	@[/*spin$R,*/ @"+t",  @"=t",  @"-t"],
			};
id walkB_bundle	= @[@"-t" ];
id walk2_bundle	= @[/*{spin$R,*/ @"x0",  @"x1"/*,  @"x2"*/];
id walk2x2_bundle	= @{@"now" :@[/*spin$R,*/  @"x0",  @"x1"],
				        @"prev":@[/*spin$R,*/ @"^x0", @"^x1"]};	// ?161116? ^ in name is problimatical
r(		(@{CameraHsuz(0, 45, 10, 1),								@"parts":@[
	o(bun,	aBundle(@{@"structure":walk2x2_bundle, @"proto":aGenBcastLeaf() })),
//			i1D4GAll,
																		]}	));
//#define i1D4GAll															\
//		o(Con1,	aMultiply(0, @[@"-t", @"x0.+", @"x1.-"])),					\
//		o(Con2,	aMultiply(0, @[@"=t", @"x0.+", @"x0.-"])),					\
//		o(Con3,	aMultiply(0, @[@"+t", @"x0.+", @"x3.-"])),					\
//		o(Con4,	aMaxOr(   0, @[@"Con1", @"Con2", @"Con3"])),				\
//																			\
//		o(Con5,	aMultiply(0, @[@"-t", @"x1.+", @"x2.-"])),					\
//		o(Con6,	aMultiply(0, @[@"=t", @"x1.+", @"x1.-"])),					\
//		o(Con7,	aMultiply(0, @[@"+t", @"x1.+", @"x0.-"])),					\
//		o(Con8,	aMaxOr(   0, @[@"Con5", @"Con6", @"Con7"])),				\
//																			\
//		o(Con9,	aMultiply(0, @[@"-t", @"x2.+", @"x3.-"])),					\
//		o(Con10,aMultiply(0, @[@"=t", @"x2.+", @"x2.-"])),					\
//		o(Con11,aMultiply(0, @[@"+t", @"x2.+", @"x1.-"])),					\
//		o(Con12,aMaxOr(   0, @[@"Con9", @"Con10", @"Con11"])),				\
//																			\
//		o(Con13,aMultiply(0, @[@"-t", @"x3.+", @"^x0"])),					\
//		o(Con14,aMultiply(0, @[@"=t", @"x3.+", @"^x3"])),					\
//		o(Con15,aMultiply(0, @[@"+t", @"x3.+", @"^x2"])),					\
//		o(Con16,aMaxOr(   0, @[@"Con13", @"Con14", @"Con15"])),			\
//																			\
//		o(Con17,aMaxOr(   0, @[@"Con4", @"Con8", @"Con12", @"Con16"])),	\
//		o(Con18,aMirror(@"Con17"))


//M(Grace2,	(@{CameraHsuz(0, 0, 0, 1),								@"parts":@[
//				aGenerator(0, @[@[@"a"], @[@"a",@"b"], @[@"b"], @"again"],@{flip, @"P":@"bun"})),
//		o(bun,	aTunnel(@{@"structure":ab_bundle, aGenBcastLeaf())),
//		o(qa,	aHamming(0, @[@"a"],						@{flip} )),
//		o(qb,	aHamming(0, @[@"a", @"b"],					@{flip} )),
//		o(qc,	aHamming(0, @[      @"b"],					@{flip} )),
//																		]}	));
M(Grace5,	(@{CameraHsuz(0, 0, 0, 1), VEL(-1),						@"parts":@[
			aGenerator(-1, @{@"events":@[@[@"a"], @[@"a",@"b"], @[@"a",@"b",@"c"],
				@[@"a",@"b",@"c",@"d"], @[@"a",@"b",@"c",@"d",@"e"],
				@[@"b",@"c",@"d",@"e"], @[@"c",@"d",@"e"], @[@"d",@"e"], @[@"e"], @[],
				@"again"], flip, @"P":@"bun"}),
	o(bun,	aTunnel(@{@"structure":abcde_bundle, @"proto":aGenBcastLeaf() })),
	o(qa,	aHamming(0, @[@"a"],						@{flip} )),
	o(qb,	aHamming(0, @[@"a", @"b"],					@{flip} )),
	o(qc,	aHamming(0, @[@"a", @"b", @"c"],			@{flip} )),
	o(qd,	aHamming(0, @[@"a", @"b", @"c", @"d"],		@{flip} )),
	o(qe,	aHamming(0, @[@"a", @"b", @"c", @"d",@"e"],	@{flip} )),
	o(qf,	aHamming(0, @[      @"b", @"c", @"d",@"e"],	@{flip} )),
	o(qg,	aHamming(0, @[            @"c", @"d",@"e"],	@{flip} )),
	o(qh,	aHamming(0, @[                  @"d",@"e"],	@{flip} )),
	o(qi,	aHamming(0, @[                       @"e"],	@{flip} )),
	o(qj,	aMaxOr(  0, @[@"qa",@"qb",@"qc",@"qd",@"qe",@"qf",@"qg",@"qh",@"qi"], @{flip} )),
	o(qk,	aMirror(@"qj")),
																		]}	));
R(Hamming,	(@{CameraHsuz(0, 0, 0, 1),								@"parts":@[
			aGenerator(0, @{@"events":@[@[@"a"], @[@"a",@"b"],@[@"b"],@[]],flip, @"P":@"bun"}),
	o(bun,	aTunnel(@{@"structure":ab_bundle, @"proto":aGenBcastLeaf() })),
	o(qa,	aHamming(0, @[@"a"],						@{flip} )),
	o(qb,	aHamming(0, @[@"a", @"b"],					@{flip} )),
	o(qc,	aHamming(0, @[      @"b"],					@{flip} )),
	o(j,	aMaxOr(  0, @[@"qb",@"qa",@"qc",],			@{flip}  )),
	o(k,	aMirror(@"j")),
																		]}	));

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark * H3 BidBundle attempts

 // 150528 BEST SO FAR
 // 160226 WTF is this???
M(interesting clustering, (@{CameraHsuz(0, 0, 0, 0.8), partsStackY, SIM_OFF, @"parts":@[
	o(bun1,  aNet(@{partsStackX}, @[
		o(ena,	aBroadcast(0, @[@"mod1.T", @"mod2.T"], @{@"jogx":@"5,5,5",flip})),
		o(mod1,	aModulator(0,							@{spin$2})),
		o(mod2,	aModulator(0,							@{spin$2})),
		])),
	o(bun2,	aNet(@{partsStackX}, @[
		o(y,	aMaxOr(0, @[@"mod1",@"mod2"],			@{flip} )),
		o(z,	aMaxOr(0, @[@"mod1",@"mod2"],			@{flip} )),
		])),
																		]}	));

r(			(@{CameraHsuz(0, 0, 0, 0.8), partsStackY,				@"parts":@[
	o(bun1,  aNet(@{partsStackX}, @[
		o(a,	aMinAnd(0,0,							@{flip} )),
		o(b,	aMinAnd(0,0,							@{flip} )),
		])),
	o(bun2,	aNet(@{partsStackX}, @[
		o(y,	aMaxOr(0, @[@"a",@"b"], @{/*, @"jog":@"5,5,5"*/flip})),
		])),
																		]}	));

r(			(@{CameraHsuz(0, 0, 0, 0.8), partsStackY,				@"parts":@[
	o(bun1,	aNet(@{partsStackX}, @[
		o(x,	aMaxOr(0, @[@"qa", @"qb"])), // works if qb removed
		o(y,	aMaxOr(0, @"qb")),
		o(z,	aMaxOr(0, @"qc")),
		])),
	o(bun2,	aNet(@{selfLinkY, partsStackX}, @[
		o(qa,	aModulator(0,							@{spin$0})),
		o(qb,	aModulator(0,							@{spin$0})),	//S+x
		o(qc,	aModulator(0,							@{spin$0})),
//		o(ena,	aBroadcast(0, @[@"qa.T", @"qb.T", @"qc.T"],@{flip, @"jog":@"0, 3, 0"})),
//		o(ena,	aBroadcast(0, @[@"qa.M", @"qb.M", @"qc.M"],@{flip, @"jog":@"0, 3, 0"})), // M->T
		])),
																		]}	));
// // 161007: BUG Links which use "partsStackX" don't display
//r(			(@{CameraHsuz(0, 0, 0, 0.8), partsStackY,			@"parts":@[
////		o(bun2,	aNet(@{selfLinkY}, @[
//		o(bun2,	aNet(@{selfLinkY, partsStackX}, @[
////			o(qa,	aBroadcast(0,							@{spin$0})),
////			o(ena,	aBroadcast(0, @[@"qa"],@{flip})),
//			o(qa,	aModulator(0,							@{spin$0})),
//			o(ena,	aBroadcast(0, @[@"qa.P^"],@{flip})),
//			])),
//																		]}	));
/* with partsStackX
		qa.S	-- draws line
		qa		-- draws line
		qa.T	-- no line
		qa.P^	-- bug
*/
 // 161007: BUG @"qa.P^" doesn't suspend linking
//r(			(@{CameraHsuz(0, 0, 0, 0.8), partsStackY,			@"parts":@[
//			o(ena,	aBroadcast(0, @[@"qa.P^"],@{flip})),
//			o(qa,	aModulator(0)),
//																		]}	));
  // 160123: UNSOLVED BUG: AutoBroadcast doesn't place properly
 //						bun2 must have partsStackX, but includes qbU
r(			(@{CameraHsuz(0, 0, 0, 0.8),							@"parts":@[
	o(bun1,	aNet(@{partsStackX}, @[
		o(x,	aMaxOr(0, @"qb")),
		o(y,	aMaxOr(0, @"qb")),
		])),
	o(bun2,	aNet(@{selfLinkY, partsStackX}, @[		//
		o(qb,	aModulator(0,							@{spin$0})),
		])),
																		]}	));
 // 150622: link painting problem FIXED
r(			(@{CameraHsuz(0, 0, 0, 0.8), partsStackX,				@"parts":@[
	o(bun1,	aNet(@{partsStackX}, @[
		o(qa,	aModulator(0, @{spin$0})),
		o(qb,	aModulator(0, @{spin$0})),
		o(qc,	aModulator(0, @{spin$0})),
		o(ena,	aBroadcast(0, @[@"qa.T", @"qb.T", @"qc.T"], @{flip, @"jog":@"0, 3, 0"})),
		])),
																	]}	));
r(			(@{CameraHsuz(0, 0, 0, 0.8), partsStackX,				@"parts":@[
	o(bun1,	aNet(@{partsStackX}, @[
		o(qa,	aModulator(0, @{spin$0})),
		o(ena,	aBroadcast(0, @[@"qa.T"], @{flip, @"jog":@"0, 3, 0"})),
		])),
																		]}	));

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark * H4 N-Port Worlds
/************************/ setParentMenu(@"Five Port Worlds"); /***************/
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark S1-ring2
id ij_abc_bundle   = @[@"i:@0", @"j:@0", @"a:@1", @"b:@1", @"c:@1"];
// 180805: NEEDS DEBUG
//M(S1-blink,	(@{CameraHsuz(0, 70, 10, 1.4),							@"parts":@[
//	aGenerator(0, @{@"events":@[@"i", @"again"],flip, @"P":@"gen", @"resetTo":@[@"a"]}),
//	aTunnel(@{n(gen), @"structure":ij_abc_bundle,
//              @"proto":@[aGenBcastLeaf(),
//                         aFlipPrevLeaf(@{@"mode":@"simForward", @"bias":@0} )] }),
//	aLink(@"a/prev.S^", @"c/prev.T^"),
//	aLink(@"b/prev.S^", @"a/prev.T^"),	// Wiring to generate patern
//	aLink(@"c/prev.S^", @"b/prev.T^"),
//
//	anActor(@{n(act1),	@"minHeight":@(minHeight),
//			  @"evi":aBundle(@{@"structure":abc_bundle, @"proto":aPrevBcastLeaf(0,@{@"U":@"write"}) }),
//			  @"con":aBundle(@{@"structure":z_bundle, @"proto":aGenMaxsqLeaf(@{@"value":@1.0},@{@"U":@"write"}), flip }),
//		}),
//	aLink(@"gen/a/prev", @"act1/evi/a/prev.P"),
//	aLink(@"gen/b/prev", @"act1/evi/b/prev.P"),	// Wire gen to act1
//	aLink(@"gen/c/prev", @"act1/evi/c/prev.P"),
//
//	aWriteHead(@{@"actor":@"act1", @"babyPrototype":@"Hamming"}),
//															]}	));
//
//	id ij_abc_bundle2   = @[@"i:@0", @"j:@0", @"a:@1", @"b:@1"];
//	R(S1-blink,	(@{CameraHsuz(0, 70, 10, 1.4),							@"parts":@[
//		aGenerator(0, @{@"events":@[@[], @"again"],flip, @"P":@"gen", @"resetTo":@[@"a"]}),
//		aTunnel(@{n(gen), @"structure":ij_abc_bundle2,
//				  @"proto":@[aGenBcastLeaf(),
//							 aFlipPrevLeaf(@{@"mode":@"simForward", @"bias":@0} )] }),
//		aLink(@"b/prev.S^", @"a/prev.T^"),	// Wiring to generate patern
//																]}	));


 // 180805, 170806 BUG birthplace bad
R(S1-blink, (@{CameraHsuz(0, 80, 10, 1),							@"parts":@[
	aNet(@{n(spin), partsStackY}, @[	// bug goes away if enclosing net removed
		aGenerator(0, @{@"events":@[@"a", @[], @"again"],
					flip, @"P":@"act1/evi", @"resetTo":@[@"a"]}),
		anActor(@{n(act1),
			@"minHeight":@(minHeight),
			@"evi":aBundle(@{@"structure":a_bundle, @"proto":aGenBcastLeaf(0,@{@"U":@"write"}) }),
			@"con":aBundle(@{@"structure":z_bundle, @"proto":aGenMaxsqLeaf(@{@"value":@1.0},@{@"U":@"write"}), flip }),
		}),
		aWriteHead(@{@"actor":@"act1", @"babyPrototype":@"Hamming"}),
									] )
																]}	));
 // 160215 bug: Links incorrectly placed. fixed 160213
R(S1-blink,	(@{CameraHsuz(0, 90, 0, 1),								@"parts":@[
	o(gen,	aBundle(@{@"structure":a_bundle, @"proto":aFlipPrevLeaf() })),
			aLink(@"a/prev.S^", @"a/prev.T^"),
																		]}	));








// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark - I. Driving a Chevy
////////////////////////////////////////////////
/////     Driving a Chevy				   /////
////////////////////////////////////////////////
/************************/ setParentMenu(@"Driving a Chevy"); /***************/
//----------------------------------------------------------------------------
id car1_big_bundle= @{
/* C=Car, M=me, A=Action */
/*	M		problem: */	  @"a:sR":@[@"notNearCar",						// !nearCar
/*		Am	action:  */					@"moveToCar",
																	],
/*	C		problem: */	  @"b:sR":@[@"doorClosed",						// !doorOpen
/*	C		problem: */					@"doorLocked",					// !doorUnlocked
/*	C		problem: */						@"doorKeyEmpty",			// !doorKeyInserted
/*	M		problem: */							@"doorKeyNotAtHand",	// !doorKeyAtHand
/*		A	action:  */							@"putKeyInDoor",
/*		A	action:  */						@"twistDoorKey",
/*		A	action:  */					@"pullHandle",
																	],
/*	C		problem: */	  @"c:sR":@[@"seatOccupied",					// !seatEmpty
/*	C		problem: */					@"occupantStaysInSeat",			// !occupantWillVacate
/*		A	action:  */						@"fail",
																	],
/*		A	action:  */	  @"d:sR":@[@"sitInDriversSeat",
/*	C		problem: */				@"ignitionKey_INSERTED",				// !ignitionKeyInserted
/*	M		problem: */					@"ignitionKeyNotAtHand",		// !ignitionKeyInHand
/*		A	action:  */						@"checkPockets",
/*		A	action:  */					@"fail",
/*		A	action:  */				@"ignitionKey_ON",
																	],
/*	C		problem: */	  @"e:sR":@[		@"footOffBrake",					// !footOnBrake
/*		A	action:  */					@"footOnBrake",
/*		A	action:  */				@"ignitionKey_START_ENGINE",
/*	C		problem: */				@"unsafeToMove",					// !safeToMove
/*		A	action:  */					@"wait",
/*		A	action:  */				@"shiftToDrive",
																	],
						};
id car1_little_bundle= @{
/* C=Car, M=me, A=Action */
/*C	prob */	  @"b:sR":@[@"doorOpen",				// !doorOpen
/*C	prob */					@"doorUnlocked",		// !doorUnlocked
/*C	prob */						@"doorKeyEmpty",	// !doorKeyInserted
/*M	prob */							@"doorKeyNotAtHand",// !doorKeyAtHand
/* A act */							@"putKeyInDoor",
/* A act */						@"twistDoorKey",
/* A act */					@"reachForDoorHandle",
/* A act */					@"pullDoorHandle",
																],
/*C	prob */	  @"c:sR":@[@"seatEmpty",				// !seatEmpty
/*C	prob */					@"occupantStaysInSeat",	// !occupantWillVacate
/* A act */						@"fail",
																],
/* A acti */  @"d:sR":@[@"sitInDriversSeat",
																	],
						};
id car1_1_bundle= @[@"doorOpen"];
id car1_2_bundle= @[@"doorOpen", @"seatEmpty", @"doorUnlocked", @"reachForDoorHandle",
					@"pullDoorHandle", @"sitInDriversSeat"];
id carSeq1 = @[
	@[@1., @"doorOpen"],		//	@[@1., @"doorOpen"], doorClosed
	@[@1., @"seatEmpty"],	//	@[@1., @"seatEmpty"],
	@[@1., @"sitInDriversSeat"],
	@"again"	];
M(Chevy 1 Infrastructure, (@{CameraHsuz(0, 250, 20, 1.5), @"fontNumber":@"1",
			  @"XdisplayAtomNames":@1, @"fluffLinkGap":@3,			@"parts":@[
				aGenerator(@{n(lo), @"events":carSeq1, @"incrementalEvents":@1, flip, @"P":@"bun"}),
	o(bun,		aTunnel(@{@"structure":car1_2_bundle, @"proto":0? aGenBulbLeaf():aGenBcastLeaf()})),//

	// To get:	// Do sequence:
//	o(doorUnLocked,aSequence(0,@[@"iHaveKey", @"keyInHand", @"keyInLock", @"ACT_twistKey"],	@{flip} )),		//x
	o(doorOpen,	aSequence(0,@[@"doorUnlocked", @"reachForDoorHandle,l=10", @"pullDoorHandle"],@{flip, @"spin":@1} )),			//x
	o(bodyInSeat,aSequence(0,@[@"doorOpen", @"seatEmpty",
					@"sitInDriversSeat,l=6"],			@{flip} )),			//x?

//	o(ignitionOn,aSequence(0,@[@"insertKey", @"ACT_twistKey"], 	@{flip}	)),
//	o(motorON,	aSequence(0,@[@"ACT_twistKey"],					@{flip}	)),
//	o(imDriver,	aSequence(0,@[@"imInSeat", @"ignitionOn"], 		@{flip}	)),
//	o(carReady,	aSequence(0,@[/*@"ignitionOn"*/ @"motorON"],	@{flip}	)),
//	o(driveToX,	aSequence(0,@[@"imDriver", @"carReady"],		@{flip}	)),
//	o(mirror,	aMirror(@"driveToX",							@{flip}	)),
																		]}	));
M(Chevy 1b Infrastructure, (@{CameraHsuz(0, 250, 20, 1.5), @"fontNumber":@"1",
			  @"XdisplayAtomNames":@1, @"fluffLinkGap":@0,			@"parts":@[

		aGenerator(1, @{n(lo), @"events":carSeq1, @"incrementalEvents":@1, flip, @"P":@"act1/evi"}),
		anActor(@{n(act1),	@"minHeight":@15, spin$L,
			@"evi":aTunnel(@{@"structure":1? car1_little_bundle :car1_1_bundle,	//car1_2_bundle
							 @"proto":0? aGenBulbLeaf(0,@{@"U":@"write"}):aGenBcastLeaf(0,@{@"U":@"write@"})}),
			@"con":aTunnel(@{@"structure":z_bundle, @"proto":aGenMaxsqLeaf(@{@"value":@1.0},@{@"U":@"write@"}), flip }),
		}),
		aGenerator(@{n(hi), @"Xnib":@"HiGen_z", @"resetTo":@[@"z"], @"P":@"act1/con"}),
		aWriteHead(@{@"actor":@"act1", @"babyPrototype":0? @"Hamming":@"HammingMem"}),// //Branch
																		]}	));
	R(Chevy 1b Infrastructure, (@{CameraHsuz(0, 250, 20, 1.5),
				  @"fluffLinkGap":@3,			@"parts":@[

			aGenerator(1, @{n(lo), @"events":@[@"a", @"b", @"again"], flip, @"P":@"act1/evi"}),
			anActor(@{n(act1),	@"minHeight":@15, spin$L,
				@"evi":aTunnel(@{@"structure":ab_bundle, @"proto":aGenBcastLeaf(0,@{@"U":@"write@"})}),
				@"con":aBundle(@{@"structure":z_bundle, @"proto":aGenMaxsqLeaf(@{@"value":@1.0},@{@"U":@"write@"}), flip }),
			}),
			aWriteHead(@{@"actor":@"act1", @"babyPrototype":@"HammingMem"}),// //Branch
																			]}	));

	R(Chevy 1, (@{CameraHsuz(0, 250, 20, 1.5), @"fluffLinkGap":@3,			@"parts":@[
					aGenerator(@{n(lo), @"prob":@0.5, flip, @"P":@"bun"}),
		o(bun,		aTunnel(@{@"structure":a_bundle })),
																			]}	));


//R(Chevy 1 Infrastructure, (@{CameraHsuz(0, 250, 20, 1.5), @"fontNumber":@"1",
//			  @"XdisplayAtomNames":@1, @"fluffLinkGap":@3,			@"parts":@[
//		aGenerator(1, @{n(lo), @"events":carSeq1, @"incrementalEvents":@1, flip, @"P":@"act1/evi"}),
//		anActor(@{n(act1),	@"minHeight":@15, spin$L,
//			@"evi":aTunnel(@{@"structure":car1_tiny_bundle, @"proto":1? aGenBcastLeaf(0,@{@"U":@"write@"}):aGenBcastLeaf()}),
////			@"con":aTunnel(@{@"structure":z_bundle,   @"proto":aGenMaxsqLeaf(@{@"value":@1.0},@{@"U":@"write"}), flip }),
//		}),
////		aGenerator(@{n(hi), @"nib":@"HiGen_z", @"P":@"act1/con"}),
//		aWriteHead(@{@"actor":@"act1", @"babyPrototype":@"Branch"}),//Hamming
//																		]}	));
//
//	R(Chevy 1, (@{CameraHsuz(0, 250, 20, 1.5), @"fluffLinkGap":@3,			@"parts":@[
//					aGenerator(@{n(lo), @"prob":@0.5, flip, @"P":@"bun"}),
//		o(bun,		aTunnel(@{@"structure":a_bundle })),
//																			]}	));






//----------------------------------------------------------------------------
id car2_action_bundle= @[
	@"checkPockets",
	@"checkNightstand",
	@"putKeyInDoor",
	@"twistDoorKey",
	@"pullHandle",
	@"sitInDriversSeat",
	@"checkPockets",
	@"ignitionKey_ON",
	@"moveToCar",
	@"footOnBrake",
	@"ignitionKey_START_ENGINE",
	@"shiftToDrive",
	@"wait",
	@"fail",
					];
id car2_bundle = @[			// small starter
	@"nearCar",
	@"doorOpen",
	@"doorUnlocked",
	@"doorKeyInserted",
	@"doorKeyAtHand",

//	@"seatEmpty",
//	@"occupantWillVacate",
//	@"ignitionKeyInHand",
//	@"footOnBrake",
//	@"safeToMove",
//
//	@"checkNightstand",
//	@"checkPockets",
	@"moveToCar",
//	@"putKeyInDoor",
//	@"twistDoorKey",
//	@"pullHandle",
//	@"sitInDriversSeat",
//
//	@"ignitionKey_INSERTED",
//	@"ignitionKey_ON",
//	@"ignitionKey_START_ENGINE",
//
//	@"footOnBrake",
//	@"shiftToDrive",
//
//	@"wait",
//	@"fail",
  ];
#ifdef never
id car2_bundle =@{@"a:sR":@[@"nearCar",  @"reachForDoorHandle",  @"pullDoorHandle"],
				  @"b:sR":@[@"iHaveKey",
							@[@"keyInHand",@"insertKey"],
							@[@"keyInLock",@"ACT_twistKey"],
						],
				  @"c:sR":@[@"doorUnlocked", @"seatEmpty", @"otherInSeat",  @"ACT_sit", @"imInSeat"],
				  @"d:sR":@[@"carRolling",]};

/*	M		problem: */			@"notNearCar",@"nearCar",			// !nearCar
/*		Am	action:  */				@"moveToCar",

/*	C		problem: */			@"doorClosed",						// !doorOpen
/*	C		problem: */				@"doorLocked",					// !doorUnlocked
/*	C		problem: */					@"doorKeyEmpty",			// !doorKeyInserted
/*	M		problem: */						@"doorKeyNotAtHand",	// !doorKeyAtHand
/*		Am	action:  */							@"checkPockets",
/*		A	action:  */							@"checkNightstand",
/*		A	action:  */							@"fail",
/*		A	action:  */						@"putKeyInDoor",
/*		A	action:  */					@"twistDoorKey",
/*		A	action:  */				@"pullHandle",

/*	C		problem: */			@"seatOccupied",					// !seatEmpty
/*	C		problem: */				@"occupantStaysInSeat",			// !occupantWillVacate
/*		A	action:  */					@"fail",

/*		A	action:  */			@"sitInDriversSeat",
/*	C		problem: */			@"ignitionKey_INSERTED",				// !ignitionKeyInserted
/*	M		problem: */				@"ignitionKeyNotAtHand",		// !ignitionKeyInHand
/*		A	action:  */					@"checkPockets",
/*		A	action:  */				@"fail",
/*		A	action:  */			@"ignitionKey_ON",
/*	C		problem: */			@"footOffBrake",					// !footOnBrake
/*		A	action:  */				@"footOnBrake",
/*		A	action:  */			@"ignitionKey_START_ENGINE",
/*	C		problem: */			@"unsafeToMove",					// !safeToMove
/*		A	action:  */				@"wait",
/*		A	action:  */			@"shiftToDrive",
#endif

 /// A reasonable first sequence to feed the Driver
// This uses incremental mode, so only delta's are shown
id carSeq2 = @[
	@[@1., @"moveToCar"],
	@[@1., @"nearCar"],
	@[@0., @"moveToCar"],
	@[@1., @"doorOpen"],
	@[@1., @"seatEmpty"],
	@[@1., @"sitInDriversSeat"],
	@[@1., @"ignitionKey_INSERTED"],
	@[@1., @"ignitionKey_ON"],
	@[@1., @"footOnBrake"],
	@[@1., @"ignitionKey_START_ENGINE"],
	@[@1., @"safeToMove"],
	@[@1., @"shiftToDrive"],
	@"again",
];

/// An Assimilator with aSequence inner core
// The Assimilator ignores the Sequences in the inner core ;(
// The sequences do not seem to be activated
M(Chevy 2a Assimilator with Sequence parts, (@{CameraHsuz(0, -70, 30, 1.7), @"fontNumber":@"1", @"fluffLinkGap":@3, @"parts":@[
		aGenerator(@{n(lo), @"events":carSeq1, @"eventLimit":@"3", flip, @"P":@"act1/evi"}),
		anActor(@{n(act1),	@"minHeight":@15,
			@"evi":aTunnel(@{@"structure":car2_action_bundle,@"proto":aGenBcastLeaf(0,@{@"U":@"write"}), @"displayLeafNames":@1}), //aGenBcastLeaf
			@"parts":@[
				aSequence(0,0,@{n(main), @"sec":@[@"notNearCar", @"doorClosed", @"seatOccupied"], flip}),
					aSequence(0,0,@{n(notNearCar), @"sec":@[@"moveToCar"], flip}),
					aSequence(0,0,@{n(doorClosed), @"sec":@[@"doorLocked", @"pullHandle"],flip}),
						aSequence(0,0,@{n(doorLocked), @"sec":@[@"doorKeyEmpty", @"twistDoorKey"],flip}),
							aSequence(0,0,@{n(doorKeyEmpty), @"sec":@[@"doorKeyNotAtHand", @"putKeyInDoor"],flip}),
								aSequence(0,0,@{n(doorKeyNotAtHand), @"sec":@[@"checkPockets", @"checkNightstand", @"fail"], flip}),
					aSequence(0,0,@{n(seatOccupied), @"sec":@[@"occupantStaysInSeat"],flip}),
						aSequence(0,0,@{n(occupantStaysInSeat), @"sec":@[@"fail"],flip}),
			  ],
			@"con":aTunnel(@{@"structure":z_bundle,   @"proto":aGenMaxsqLeaf(@{@"value":@1.0},@{@"U":@"write"}), flip }),
			}),
		aGenerator(@{n(hi), @"nib":@"HiGen_z", @"P":@"act1/con"}),
		aWriteHead(@{@"actor":@"act1", @"babyPrototype":@"Hamming"}),
																		]}	));
  /// A Small Assimilator, to work out things
 // 180805 FIXED: 180122 BUG carSeq1 doesn't work
M(Chevy 2b 2a but smaller, (@{CameraHsuz(0, -70, 30, 1.7), @"fontNumber":@"1", @"fluffLinkGap":@3, @"parts":@[
		aGenerator(@{n(lo), @"events":carSeq1, @"eventLimit":@"3", flip, @"P":@"act1/evi"}),
		anActor(@{n(act1),	@"minHeight":@15,
			@"evi":aTunnel(@{@"structure":car2_bundle,@"proto":aGenBcastLeaf(0,@{@"U":@"write"}), @"displayLeafNames":@1}), //aGenBcastLeaf
			@"con":aTunnel(@{@"structure":z_bundle,   @"proto":aGenMaxsqLeaf(@{@"value":@1.0},@{@"U":@"write"}), flip }),
			}),
		aGenerator(@{n(hi), @"nib":@"HiGen_z", @"P":@"act1/con"}),
		aWriteHead(@{@"actor":@"act1", @"babyPrototype":@"Hamming"}),
																		]}	));




//----------------------------------------------------------------------------
 // flashy, but useless
id car3_bundle = @[@"carSeatOccupied", @"meHandTwistKey",
			  @"meFindKey__", @"me.walkTo(car)", @"meWalkToCar",
			  @"meTalkToPerson",@"me_body_sitIn_car"];
///*
//1	meInCar?!
//2		nearCar?!
//			meWalkToCar
//3		thenAA
//4			me_arm_open_door
//5				carDoorUnlocked?!
//6					car_doorLock_keyIns?!
//7						myHandHoldingKey?!
//							me_find(key) ...
//8					thenCC
//						meHandTwistKey
//9			thenBB
//10				carSeatOccupied?
//					meTalkToPerson...
//				me_body_sitIn_car   			**/
//R(Chevy 1, (@{CameraHsuz(0, 30, 20, 1.5),	@"fontNumber":@2,		@"parts":@[
//							aGenerator(@{n(car", @"prob":@0.5,	flip, @"P":@"bun"}),
//		o(bun,				aTunnel(@{@"structure":car1_bundle, @"proto":aGenBcastLeaf() })),
///*1*/	o(meInCar,			aSequence(0,@[@"nearCar",@"thenAA"],	@{flip} )),
///*2*/	o(nearCar,			aSequence(0,@[@"meWalkToCar"],		@{flip} )),
///*3*/	o(thenAA,			aSequence(0,@[@"me_arm_open_door"],	@{flip} )),
///*4*/	o(me_arm_open_door, aSequence(0,@[@"carDoorUnlocked"],	@{flip} )),
///*5*/	o(carDoorUnlocked,	aSequence(0,@[@"myHandHoldingKey", @"thenCC"],@{flip} )),
///*6*/	o(car_doorLk_keyIn, aSequence(0,@[@"myHandHoldingKey",@"thenCC"],@{flip} )),
///*7*/	o(myHandHoldingKey,	aSequence(0,@[@"meFindKey__"],		@{flip} )),
///*8*/	o(thenCC,			aSequence(0,@[@"meHandTwistKey"],	@{flip} )),
///*9*/	o(thenBB,			aSequence(0,@[@"carSeatOccupied"],	@{flip} )),
///*10*/	o(carSeatOccupied,	aSequence(0,@[@"meTalkToPerson"/*, @"me_body_sitIn_car"*/],@{flip} )),
////		@{N_@"mirror",					Mirror(@"meInCar"},
//																		]}	));


id (^aQState)(id base, id etc)  = ^id(id base, id etc) {
	return aNet(etc, @[						// n(base)//@"named":base
		aMaxOr	  (0, 	 base, 	@{n(in), flip}),
		anAgo     (@"in", 	  	@{n(ago)}),
		aBulb	  (@"ago=",	0,	@{n(dist)}),	//aBayes
//		aBroadcast(@"ago=",	0,	@{n(dist)}),	//aBayes
		aBroadcast(0,		0,	@{n(comb), flip, selfStackY}),		// name doesn't work!
	  ]);
	};

M(Chevy 1b Infrastructure, (@{CameraHsuz(0, 0, 20, 1.5), @"fontNumber":@"1",
			  @"XdisplayAtomNames":@1, @"fluffLinkGap":@1,			@"parts":@[

//		aBroadcast(0,0,@{n(aa), flip}),		// name doesn't work!
		aGenerator(0, @{n(lo), @"events":@[@"d_t", @"d_t",@"d_i", @"d_t",@"d_i",@"d_a", @[], @"again", ], flip, @"P":@"act1/evi"}),
		anActor(@{n(act1), @"asyncData":@1, selfLinkY,							//xyzzy11a either
			@"evi":aTunnel(@{@"structure":didat_bundle, @"proto":aGenBcastLeaf(0,@{@"xU":@"write@"})}),
			@"parts":@[
				aQState(@"d_t", @{n(S0)}),
				aQState(@"d_i", @{n(S1)}),
				aQState(@"d_a", @{n(S2)}),	//d_a_
				aLink(@"S0/dist^", @"S1/comb^"),
				aLink(@"S1/dist^", @"S2/comb^"),
//				aModulator(0, @{n(m1), @"xS":@"cc", @"xT":@"d_i", flip, spin$2}),
// ////				aBroadcast(@"bb=",0,	@{n(cc)}),	//aBayes
			  ],
			@"con":aTunnel(@{@"structure":z_bundle, @"proto":aGenMaxsqLeaf(@{@"value":@1.0},@{@"xU":@"write@"}), flip }),
		}),
		aGenerator(@{n(hi), @"Xnib":@"HiGen_z", @"resetTo":@[@"z"], @"P":@"act1/con"}),
//		aWriteHead(@{@"actor":@"act1", @"babyPrototype":0? @"Hamming":@"HammingMem"}),// //Branch
																		]}	));



id s01_bundle = @[@"S0", @"S1"];
R(test Infrastructure, (@{CameraHsuz(0, 0, 0, 1), @"boundsDrapes":@0.1,
			@"debugPositionOf":@"V11", @"displayAtomNames":@1, @"parts":@[

#define TEST 1
//#if TEST == 1
	anAgoDistCombLeaf(),
//#else if TEST==2
//	aBundle(@{n(state), @"structure":@[@"S0"], @"proto":anAgoDistCombLeaf()}),//, @"S1"
//#else if TEST2==3
//	anAgo     (0,			@{n(ago)}),					//@"comb^"
//	aBroadcast(0,		0,	@{n(comb),flip, selfStackY}),		// name doesn't work!
//#else if TEST2==3
	aNet(@[
		aBroadcast(0,		0,	@{n(dist)}),	//aBayes  etc3etc
//		anAgo     (0,			@{n(ago), 		etc1etc}),	//@"comb^"
//		aBroadcast(@"ago=",	0,	@{n(dist),		etc2etc}),	//aBayes
		aBroadcast(0,		0,	@{n(comb),flip, selfStackY}),		// , etc3etc name doesn't work!
	  ]),
//#endif
	aBroadcast(0,		0,	@{n(B), selfStackX}),		// name doesn't work!
	aBroadcast(0,		@[@"B", @"leaf1"],	@{n(C),flip, @"noPosition":@1, /*, selfStackY*/}),		// name doesn't work!

//		anActor(@{n(act1), @"asyncData":@1,										//xyzzy11a unused
//			@"evi":aTunnel(@{@"structure":didat_bundle, @"proto":aGenBcastLeaf(0,@{@"xU":@"write@"})}),
//			@"parts":@[
//				aBundle(@{n(state), @"structure":s01_bundle, @"proto":anAgoDistCombLeaf()}),//, @"S1"
//
//				aModulator(0, @{n(m01), @"S":@"state/S0", @"xP":@"con/S1.in", @"xT":@"d_i", flip, spin$2}),
// //
// //				aMaxOr(0, @[@"S0", @"S1"], @{n(m01), flip}),
// //				aLink(@"S0^", @"S1.in^"),
// //				aLink(@"S1^", @"S0.in^"),
//			  ],
//			@"con":aTunnel(@{@"structure":s01_bundle, @"proto":aBcastLeaf(), flip }),
//		}),

																		]}	));

R(test Infrastructure, (@{CameraHsuz(0, 0, 0, 1), @"boundsDrapes":@0.1,
			@"XdebugPositionOf":@"V11", @"XdisplayAtomNames":@1, @"parts":@[
		o(gen,	aGenAtom(@{flip})),
		o(main,	aBroadcast(@"gen=")),



//	aLeaf(@"genBcast", @{bMain, @"G":@"gen.P", @"R":@"gen.P"}, @[
//		o(gen,	aGenAtom(					  @{etc1etc, flip})),
//		o(main,	aBroadcast(@"gen=", 0,			etc2)),
//											 ], etc3	);}



																		]}	));


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
																		//151031: pretty good:
#pragma mark - D. Shaft Encoders
/************************/ setParentMenu(@"D. Shaft Encoder"); /***************/
id fwdBkw_bundle	= @[            @"fwd", @"bkw"];
id fwd_bundle		= @[            @"fwd"        ];
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark * D1. Shaft Poles 3
//URL factalWorkbench://D.%20Shaft%20Encoder/D1.%20Shaft%20Poles%203
M(D1. Shaft Poles 3,(@{CameraHsuz(0, 0, 0, 1), @"boundsDrapes":@0, @"displayAxis":@0, @"parts":@[
	aShaftBundleTap(@{n(loMod),  @"nPoles":@3, @"P":@"wheelA/evi", flip}),
	anActor			(@{n(wheelA), @"minHeight":@0.0,
		@"evi":aTunnel(@{@"structure":abc_bundle, @"proto":aGenBcastLeaf() }),
		@"con":aTunnel(@{@"structure":xyz_bundle, @"proto":aGenBcastLeaf(), flip}),//aGenBcastLeaf//@"check out aGenBcastLeaf's flip"
		@"parts":@[
			aLink(@"evi/a", @"con/x"),
			aLink(@"evi/b", @"con/y"),
			aLink(@"evi/c", @"con/z"),
		  ],
	}),
	aGenerator(@{n(hi), @"nib":@"HiGen_xyz", @"xx addBundleTap":@"loMod", @"P":@"wheelA/con"}),
																		]}	));


// 160525 BUG Positioning of con/z/main is influenced by con/z/gen AND a
r(		(@{CameraHsuz(0, 0, 0, 1), @"boundsDrapes":@0.1, @"displayAxis":@0, @"parts":@[
	o(a,	aBroadcast(nil, nil, @{flip})),
	o(b,	aBroadcast(@"a=")),
	o(c,	aBroadcast(@"b" )),
////	o(con,	aBundle(@{@"structure":yz_bundle, aBcastLeaf(), @{flip})),		// works
//	o(con,	aBundle(@{@"structure":z_bundle, @"proto":aGenBcastLeaf(), flip})),	// bad
//			aLink(@"a", @"con/z"),
																		]}	));

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark * D2. Unicycle 1
 // 161012 bug placement shifted over. 161114 okay
R(Unicycle1 placement bug,(@{CameraHsuz(0, 90, 0, 1.1),	@"boundsDrapes":@0.2, @"displayAxis":@1, @"parts":@[
			anActor(@{n(wheelA),	@"positionViaCon":@1,	@"minHeight":@0.0,
				@"con":aTunnel(@{@"structure":yz_bundle, spin$1, @"positionPriorityXz":@1, flip}),
				@"parts":@[
					o(hz,	aHamming(@[@"z"], @[@"a"], @{flip})),
					o(hz,	aHamming(@[@"y"], @[@"a"], @{flip})),
				 ],
				@"evi":aTunnel(@{@"structure":a_bundle }),
						} ) ]}	));

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark * D2. Unicycle 3
//URL factalWorkbench://D.%20Shaft%20Encoder/D2.%20Unicycle3
M(D2. Unicycle3,(@{CameraHsuz(0, 30, 20, 1), VEL(-2), @"boundsDrapes":@0, @"displayAxis":@0, @"parts":@[
	aShaftBundleTap(@{@"nPoles":@3, @"P":@"wheelA/evi", flip}),
	anActor(@{n(wheelA),	@"positionViaCon":@1, @"minHeight":@0.0,
		@"con":aTunnel(@{@"structure":fwdBkw_bundle, @"proto":aGenMaxLeaf(), spin$1, @"positionPriorityXz":@1, flip, }),
		@"parts":@[
			aHamming(@[@"fwd"], @[@"a.+", @"b.-"], @{flip}),
			aHamming(@[@"fwd"], @[@"b.+", @"c.-"], @{flip}),
			aHamming(@[@"fwd"], @[@"c.+", @"a.-"], @{flip}),
			aHamming(@[@"bkw"], @[@"a.+", @"c.-"], @{flip}),
			aHamming(@[@"bkw"], @[@"b.+", @"a.-"], @{flip}),
			aHamming(@[@"bkw"], @[@"c.+", @"b.-"], @{flip}),
		  ],
		@"evi":aTunnel(@{@"structure":abc_bundle, @"proto":aGenPrevBcastLeaf(0, @{@"mode":@"netForward", spin$1}) }),
	}),
	aGenerator(@{n(hi), @"nib":@"HiGen_fwdBkw", @"resetTo":@[@"fwd"], @"P":@"wheelA/con"}),
																		]}	));

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark * D2. Unicycle6
M(D2. Unicycle6,(@{CameraHsuz(-2, 60, 10, 1.7),	VEL(-2), @"boundsDrapes":@0, @"displayAxis":@0, @"fontNumber":@1, @"parts":@[
	aShaftBundleTap(@{@"nPoles":@6, @"P":@"wheelA/evi", flip}),
	anActor(@{n(wheelA), @"positionViaCon":@1,	@"minHeight":@0.0,
		@"con":aTunnel(@{@"structure":fwdBkw_bundle, @"proto":aGenMaxLeaf(), @"positionPriorityXz":@1, spin$1, flip, }),
		@"parts":@[
			aHamming(@[@"fwd"], @[@"a.+", @"b.-"], @{flip}),
			aHamming(@[@"fwd"], @[@"b.+", @"c.-"], @{flip}),
			aHamming(@[@"fwd"], @[@"c.+", @"d.-"], @{flip}),
			aHamming(@[@"fwd"], @[@"d.+", @"e.-"], @{flip}),
			aHamming(@[@"fwd"], @[@"e.+", @"f.-"], @{flip}),
			aHamming(@[@"fwd"], @[@"f.+", @"a.-"], @{flip}),
			aHamming(@[@"bkw"], @[@"a.+", @"f.-"], @{flip}),
			aHamming(@[@"bkw"], @[@"b.+", @"a.-"], @{flip}),
			aHamming(@[@"bkw"], @[@"c.+", @"b.-"], @{flip}),
			aHamming(@[@"bkw"], @[@"d.+", @"c.-"], @{flip}),
			aHamming(@[@"bkw"], @[@"e.+", @"d.-"], @{flip}),
			aHamming(@[@"bkw"], @[@"f.+", @"e.-"], @{flip}),
		  ],
		@"evi":aTunnel(@{@"structure":abcdef_bundle, @"proto":aGenPrevBcastLeaf(0, @{@"mode":@"netForward", spin$1}) }),
	}),
	aGenerator(@{n(hi), @"nib":@"HiGen_fwdBkw", @"resetTo":@[@"fwd"], @"P":@"wheelA/con"}),
																		]}	));
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//#pragma mark * D3. Shaft Learn 3 forward/backward
// //  160328 /16313-videos/ShaftLearn
//#define yz04 @[@"y=.4", @"z=.4"]
//id loDat3	= @[@"a",@"b",@"c",@"a",@"b",@"c",@"a", @"c",@"b",@"a",@"c", @"b",@"a",@"c",  @"a",@"b",@"c",@"a", @"c",@"b",@"a",@"c"];
//id hiDat3	= @[@0,  @"y",@"y",@"y",@"y",@"y",@"y", @"z",@"z",@"z",@"z", @"z",@"z",@"z",  yz04,yz04,yz04,yz04, yz04,yz04,yz04,yz04];
//xM(D3. Shaft Learn 3 fw/bk,(@{CameraHsuz(0, 70, 10, 1), @"boundsDrapes":@0, @"displayAxis":@0, @"parts":@[
//	aGenerator(0, @{@"events":loDat3, flip, @"P":@"act1/evi"}),
//	anActor(@{n(act1", @"minHeight":@10.0,
//		@"con":aTunnel(@{@"proto":aGenMaxsqLeaf(0, @{@"U":@"write"}, @{spin$0, @"minSize":@"5,0,0"}),
//						 @"structure":yz_bundle, @"positionPriorityXz":@1, flip}),
//		@"evi":aTunnel(@{@"structure":abc_bundle, @"proto":aGenPrevBcastLeaf(0, @{spin$R}, @{@"U":@"write"}) }),
//		spin$0}),	/// spin$1 causes 161227 BUG Birthplace Bad
//	aWriteHead(@{@"actor":@"act1", @"babyPrototype":@"Hamming"}),
////. THIS IS A 2-BundleTap TEST
//	aGenerator(0, @{@"events":hiDat3, @"P":@"act1/con"}),
//															]}	));
//
//// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//#pragma mark * D3. Shaft Learn 5 forward/backward
// // 160328 /16313-videos/ShaftLearn
//id loDat5	= @[@"a",@"b",@"c",@"d",@"e", @"e",@"d", @"d",@"c", @"c",@"b", @"b",@"a", @"a",@"a", @"again"];
//id hiDat5	= @[@0,  @"x",@"x",@"x",@"x", @"y",@"z", @"y",@"z", @"y",@"z", @"y",@"z", @"y",@"z", @"again"];
//id bun5 = 0? abcde_bundle: @[@"b",@"d",@"a",@"c",@"e"];
//int nEvents = 16;//-1;
// // 161103 BUG context shifts wrt evidence as Hammings added
//M(D3. Shaft Learn 5 fw/bk,(@{CameraHsuz(0, 70, 5, 1.8), @"boundsDrapes":@0, @"displayAxis":@0, @"parts":@[
//	aGenerator(nEvents,@{@"events":loDat5, flip, @"P":@"act1/evi"}),
//	anActor(@{n(act1",							@"positionViaCon":@1, @"minHeight":@10.0,
//		//spin$R, 					// 161229 BUG -- Birthplace bad
//		@"con":aTunnel(@{@"structure":xyz_bundle, spin$1, @"positionPriorityXz":@1, flip,
//						 @"proto":aGenMaxsqLeaf(0, @{@"U":@"write"}, @{spin$0, @"minSize":@"5,0,0"}) }),
//		@"evi":aTunnel(@{@"structure":bun5, @"proto":aGenPrevBcastLeaf(0, @{spin$R}, @{@"U":@"write", spin$R}) }),
//	}),
//	aWriteHead(@{@"actor":@"act1", @"babyPrototype":@"Hamming"}),
////. THIS IS A 2-BundleTap TEST
//	aGenerator(nEvents, @{@"events":hiDat5, @"P":@"act1/con"}),
//																		]}	));



// 160404 cut video 'Four Word Language'
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark - E. Turtles (3-bit language)
/************************/ setParentMenu(@"E. Turtles"); /***************/
//URL factalWorkbench://E.%20Turtles/E.%20Turtle%2034
///180119 BUG (s) -- many!!
M(E. Turtle 34,(@{CameraHsuz(0, 45, 10, 1.5), VEL(-2), @"boundsDrapes":@0,
 		@"fontNumber":@"2", @"displayAxis":@0, @"asyncData":@0, @"parts":@[		//xyzzy11 either

	 // Wheel A
	aNet(@{partsLinkY, selfStackX}, @[
		aShaftBundleTap(@{@"nPoles":@3, spin$1, @"P":@"wheelA/evi", flip}),
		anActor(@{n(wheelA), @"positionViaCon":@1, @"minHeight":@0.0,
			@"con":aBundle(@{@"structure":fwdBkw_bundle, @"proto":aBcastMaxLeaf(), spin$0, @"positionPriorityXz":@2, flip}),
			@"parts":@[
				aHamming(@[@"fwd"], @[@"a.+", @"b.-"], @{flip}),
				aHamming(@[@"fwd"], @[@"b.+", @"c.-"], @{flip}),
				aHamming(@[@"fwd"], @[@"c.+", @"a.-"], @{flip}),
				aHamming(@[@"bkw"], @[@"a.+", @"c.-"], @{flip}),
				aHamming(@[@"bkw"], @[@"b.+", @"a.-"], @{flip}),
				aHamming(@[@"bkw"], @[@"c.+", @"b.-"], @{flip}),
			],
			@"evi":aTunnel(@{@"structure":abc_bundle, @"proto":aGenPrevBcastLeaf(0, @{@"mode":@"netForward"}), spin$1 }),
												}),
		]),
	 // Word module
	aNet(@{partsLinkY, selfStackX_}, @[
		aGenerator(@{n(word), @"P":@"evi", @"nib":@"LoGen_langAbfb", flip}),
		anActor(@{n(word), @"minHeight":@10.0,
			@"con":aBundle(@{@"structure":ab_fwdBkw_bundle, @"proto":aBcastLeaf(@{flip}), spin$1, flip }),
			@"parts":@[
				aLink(@"evi/a",		@"con/a"),
				aLink(@"evi/b",		@"con/b"),
				aLink(@"evi/fwd",	@"con/fwd"),
				aLink(@"evi/bkw",	@"con/bkw"),
						],
			@"evi":aTunnel(@{@"structure":ab_fwdBkw_bundle, @"proto":aGenSoundBcastLeaf(), spin$1 }),
												}),
			]),
	 // Wheel B
	aNet(@{partsLinkY, selfStackX}, @[
		aShaftBundleTap(@{@"nPoles":@4, spin$1, @"P":@"wheelB/evi", flip}),
		anActor(@{n(wheelB), @"positionViaCon":@1, @"minHeight":@0.0,
			@"con":aBundle(@{@"structure":fwdBkw_bundle, @"proto":aBcastMaxLeaf(), spin$0, @"positionPriorityXz":@2, flip }),
			@"parts":@[
				aHamming(@[@"fwd"], @[@"a.+", @"b.-"], @{flip}),
				aHamming(@[@"fwd"], @[@"b.+", @"c.-"], @{flip}),
				aHamming(@[@"fwd"], @[@"c.+", @"d.-"], @{flip}),
				aHamming(@[@"fwd"], @[@"d.+", @"a.-"], @{flip}),
				aHamming(@[@"bkw"], @[@"a.+", @"d.-"], @{flip}),
				aHamming(@[@"bkw"], @[@"b.+", @"a.-"], @{flip}),
				aHamming(@[@"bkw"], @[@"c.+", @"b.-"], @{flip}),
				aHamming(@[@"bkw"], @[@"d.+", @"c.-"], @{flip}),
			  ],
			@"evi":aTunnel(@{@"structure":abcd_bundle, @"proto":aGenPrevBcastLeaf(0, @{@"mode":@"netForward"}), spin$1 }),
				}),
						]),

	 // Language module
	aNet(@{n(lang), partsStackX, selfStackY}, @[
		aNet(@{n(langA), partsStackZ, @"minSize":@"7,0,0"}, @[			//partsLinkY
			aMinAnd(0, @[@"word/con/fwd", @"wheelA/con/fwd"],	@{n(mfA), flip} ),
			aMirror(0, @{n(mirA), @"gain":@0.5, flip}),
			aMinAnd(0, @[@"word/con/bkw", @"wheelA/con/bkw"],	@{n(mbA), flip} ),
			aBroadcast(@"z",@[@"mfA",@"mbA",@"mirA",@"word/con/a"],	@{n(upA), selfLinkY, flip} ),
		  ]),

		aNet(@{n(langB), partsStackZ, @"minSize":@"7,0,0"}, @[			//partsLinkY
			aMinAnd(0, @[@"word/con/fwd", @"wheelB/con/fwd"],	@{n(mfB), flip} ),
			aMirror(0, @{@"gain":@0.5, n(mirB), flip}),
			aMinAnd(0, @[@"word/con/bkw", @"wheelB/con/bkw"],	@{n(mbB), flip} ),
			aBroadcast(@"z",@[@"mfB",@"mbB",@"mirB",@"word/con/b"],	@{selfLinkY, n(upB), flip} ),
		  ]),

		aTunnel(@{n(langC), @"structure":z_bundle, @"proto":aGenMaxLeaf(@{@"value":@1}), flip, selfLinkY}),
//		aGenerator(@{n(langBt), @"nib":@"HiGen_z2", @"resetTo":@"z", @"P":@"langC", selfLinkY}),
//		aGenerator(@{n(lang),   @"nib":@"HiGen_z2",@"XresetTo":@"z", @"P":@"langC", selfLinkY}),
							]),
																		]}	));

  /// BUGS be here!!!!
 ///
R(WORKS Gui 1:N Bot, (@{												@"parts":@[
	aGenerator(@{n(lo), @"nib":@"LoGen_morse", @"P":@"didat", flip}),
	aBundle(@{n(didat), @"structure":@[@"d_i"], @"proto":aGenSoundLeaf() }),
																		]}	));
//R(BROKEN E. Turtle 34,(@{CameraHsuz(0, 45, 10, 1.2), VEL(-2), @"asyncData":@1, @"parts":@[	//xyzzy11 unused
//	anError(@"dual-timingGenerators not supported"),
//	aGenerator(@{n(evid), @"nib":@"LoGen_langAbfb", @"P":@"evi", flip}),
//	aBundle   (@{n(evi),  @"structure":a_bundle, @"proto":1? aGenSoundBcastLeaf() :aGenBcastLeaf(), spin$1 }),//
//	aBundle(   @{n(lang), @"structure":_bundle, @"proto":aGenMaxLeaf(), flip}),
//	aGenerator(@{n(lan), @"Xnib":@"HiGen_z2", @"P":@"lang"}),
//																		]}	));
//R(BROKEN E. Turtle 34,(@{CameraHsuz(0, 45, 10, 1.2), VEL(-2), @"asyncData":@1, @"parts":@[	//xyzzy11 unused
//	anError(@"dual-timingGenerators not supported"),
//	aGenerator(@{n(evid), @"nib":@"LoGen_langAbfb", @"P":@"evi", flip}),
//	aBundle   (@{n(evi),  @"structure":a_bundle, @"proto":1? aGenSoundBcastLeaf() :aGenBcastLeaf(), spin$1 }),//
//	aGenerator(@{n(lan), @"Xnib":@"HiGen_z2", @"P":@"evi"}),
//																		]}	));

//xR(WORKS Gui 1:N Bot, (@{												@"parts":@[
//	aGenerator(@{n(word), @"P":@"evi", @"nib":@"LoGen_langAbfb", flip}),
//	anActor(@{n(word), @"minHeight":@10.0,
//		@"con":aBundle(@{@"structure":ab_fwdBkw_bundle, @"proto":aBcastLeaf(@{flip}), spin$1, flip }),
//		@"parts":@[
//			aLink(@"evi/a",		@"con/a"),
//			aLink(@"evi/b",		@"con/b"),
//			aLink(@"evi/fwd",	@"con/fwd"),
//			aLink(@"evi/bkw",	@"con/bkw"),
//					],
//		@"evi":aTunnel(@{@"structure":ab_fwdBkw_bundle, @"proto":aGenSoundBcastLeaf(), spin$1 }),
//											}),
//																			]} ));




 // debugging 1:N datu
M(F? Push-data, (@{CameraHsuz(0, 45, 10, 1.5), VEL(-2), @"boundsDrapes":@0,
 		@"fontNumber":@"2", @"displayAxis":@0, @"asyncData":@1, @"parts":@[		//xyzzy11 broken (no clock)
 //180806 BUG'sndPPort.atom' is not a kind of 'SoundAtom'
//		aGenerator(@{n(word), @"xS=":@0, @"P":@"evi", @"nib":@"LoGen_langAbfb", flip}),
		aGenerator(@{n(word), @"xS=":@0, @"P":@"evi", @"nib":@"LoGen_lang3Abfb", flip}),
		aTunnel(@{n(evi), @"structure":ab_fwdBkw_bundle, @"proto":aGenPrevBcastLeaf(0, @{spin$1}) }),
		aMaxOr(0, @[@"a", @"b", @"fwd", @"bkw"], @{flip}),
																		]}	));

 // working on ButtonBidir's
R(Debug new generator,(@{CameraHsuz(0, 45, 0, 1.4), VEL(-2), @"boundsDrapes":@0,
 		@"fontNumber":@"2", @"displayAxis":@0, @"asyncData":@1, @"parts":@[		//xyzzy11 broken (no clock)

		aGenerator(@{@"nib":@"LoGen_lang3Abfb", flip, @"P":@"evi"}),
		aTunnel(@{n(evi), @"structure":ab_fwdBkw_bundle, @"proto":aGenBcastLeaf(), spin$1 }),
		aLink(@"evi/a", @"z"),
		aLink(@"evi/b", @"z"),
		aTunnel(@{n(langC), @"structure":z_bundle, @"proto":aGenMaxLeaf(), flip, selfLinkY}),
		aGenerator(@{@"nib":@"HiGen_z2", @"resetTo":@"z", @"P":@"langC", selfLinkY}),
																		]}	));

 // 170814 fixed
R(BUG Turtle 34,(@{CameraHsuz(0, 15, 10, 1.3), VEL(-2), @"boundsDrapes":@0.1,
 		@"fontNumber":@"2", @"displayAxis":@0,						@"parts":@[

		aGenerator(@{@"nib":@"LoGen_lang3Abfb", flip, @"P":@"evi"}),	// BROKEN
//		aGenerator(@{@"nib":@"LoGen_ab", flip, @"P":@"evi"}),		// WORKS
		aTunnel(@{n(evi), @"structure":ab_bundle, @"proto":aGenPrevBcastLeaf(0,0,0, @{spin$1}) }),
																		]}	));



 // 160825 -- gui positioning
r(		(@{CameraHsuz(0, 0, 0, 1), @"boundsDrapes":@.1, @"displayAxis":@1, @"parts":@[
				aGenerator(@{@"nib":@"LoGen_abc", flip, @"P":@"bun"}),	//LoGen_fwd
	o(bun,		aTunnel(@{@"structure":abc_bundle, @"proto":aGenBcastLeaf() })),
//	o(hamA,		aHamming(0, @[@"a"], @{flip})),
//	o(hamB,		aHamming(0, @[@"b"], @{flip})),
																		]}	));
// 160402 fwd bkw A B   WORKS!!!  Explore macro bidirectionality.
/*
	Mode 1: Lang.[A b]: A.[fwd, bkw] -> Lang; B ignored
	
 */
M(Explore macro bidirectionality,(@{CameraHsuz(-4, 20, 10, 1.6), @"boundsDrapes":@0,
		@"displayLeafNames":@1, @"fontNumber":@"2", @"displayAxis":@0, @"parts":@[
	aLabel(		@"A",    6, @"0,   -8, 0"),
	aLabel(		@"B",    6, @"12,  -8, 0"),
	aLabel(		@"Lang", 6, @"18,  -8, 0"),
	 //      A
	o(spinA,	aNet(@{partsLinkY, selfStackX}, @[
					aGenerator(@{@"nib":@"LoGen_fwdBkw", flip, @"P":@"bunA", @"heightAlongRightSide":@0.0}),
		o(bunA,		aTunnel(@{@"structure":fwdBkw_bundle, @"proto":aGenBcastLeaf(), spin$1} )),			//fwd_bundle
						])),
	 //      B
	o(spinB,	aNet(@{partsLinkY, selfStackX}, @[
		o(loGenB,	aGenerator(@{@"nib":@"LoGen_fwdBkw", flip, @"P":@"bunB", @"heightAlongRightSide":@0.5})),
		//o(loGenB,	aGenerator(@[],@{flip, @"P":@"bunB"})),
		o(bunB,		aTunnel(@{@"structure":fwdBkw_bundle, @"proto":aGenBcastLeaf(), spin$1} )),
						])),
	 //      Language module
	o(spinL,	aNet(@{partsLinkY, selfStackX_}, @[
		o(loGenL,	aGenerator(@{@"nib":@"LoGen_abFwdBkw", flip, @"P":@"bunL", @"heightAlongRightSide":@1.0})),
		//o(loGenL,	aGenerator(@[],@{flip, @"P":@"bunL"})),
		o(bunL,		aTunnel(@{@"structure":ab_fwdBkw_bundle, @"proto":aGenBcastLeaf(), spin$1} )),
						])),
	 //		 BIDIRECTIONALITY:
	o(spin4,	aNet(@{partsStackX, selfStackY}, @[
		o(bunA,		aNet(@{partsStackZ, @"minSize":@"7,0,0"}, @[			//partsLinkY
			o(mbA,		aMinAnd(0, @[@"bunL/bkw", @"bunA/bkw"],	@{flip} )),
			o(mirA,		aMirror(0, @{@"gain":@0.2, flip})),
			o(mfA,		aMinAnd(0, @[@"bunL/fwd", @"bunA/fwd"],	@{flip} )),
			o(upA,		aBroadcast(@"z",@[@"mfA",@"mbA",@"mirA",@"bunL/a"],	@{selfLinkY, flip} )),
		  ])),
		o(bunB,		aNet(@{partsStackZ, @"minSize":@"7,0,0"}, @[			//partsLinkY
			o(mbB,		aMinAnd(0, @[@"bunL/bkw", @"bunB/bkw"],	@{flip} )),
			o(mirB,		aMirror(0, @{@"gain":@0.2, flip})),
			o(mfB,		aMinAnd(0, @[@"bunL/fwd", @"bunB/fwd"],	@{flip} )),
			o(upB,		aBroadcast(@"z", @[@"mfB",@"mbB",@"mirB",@"bunL/b"],	@{selfLinkY, flip} )),
		  ])),

	 //		 TOP:
		o(bun4,		aTunnel(@{@"structure":z_bundle, @"proto":aGenMaxLeaf(), flip, selfLinkY})),
					aGenerator(@{@"events":@[],@"resetTo":@"z", @"P":@"bun4", selfLinkY}),
						])),
																		]}	));
M(Explore macro bidirectionality,(@{CameraHsuz(-2, 20, 10, 1.6), @"boundsDrapes":@0,
		@"displayLeafNames":@1, @"fontNumber":@"2", @"displayAxis":@0, @"parts":@[
	aLabel(		@"A",    6, @"0,   -8, 0"),
	aLabel(		@"B",    6, @"12,  -8, 0"),
	aLabel(		@"Lang", 6, @"18,  -8, 0"),
	 //      A
	o(spinA,	aNet(@{partsLinkY, selfStackX}, @[
					aGenerator(@{@"nib":@"LoGen_fwdBkw", flip, @"P":@"bunA", @"heightAlongRightSide":@0.0}),
		o(bunA,		aTunnel(@{@"structure":fwdBkw_bundle, @"proto":aGenBcastLeaf(), spin$1} )),			//fwd_bundle
						])),
	 //      B
	o(spinB,	aNet(@{partsLinkY, selfStackX}, @[
		o(loGenB,	aGenerator(@{@"nib":@"LoGen_fwdBkw", flip, @"P":@"bunB", @"heightAlongRightSide":@0.5})),
		o(bunB,		aTunnel(@{@"structure":fwdBkw_bundle, @"proto":aGenBcastLeaf(), spin$1} )),
						])),
	 //      Language module
	o(spinL,	aNet(@{partsLinkY, selfStackX_}, @[
		o(loGenL,	aGenerator(@{@"nib":@"LoGen_abFwdBkw", flip, @"P":@"bunL", @"heightAlongRightSide":@1.0})),////LoGen_abFwdBkw//LoGen_langAbfb
		o(bunL,		aTunnel(@{@"structure":ab_fwdBkw_bundle, @"proto":aGenBcastLeaf(), spin$1} )),
						])),
	 //		 BIDIRECTIONALITY:
	o(spin4,	aNet(@{partsStackX, selfStackY}, @[
		o(bunA,		aNet(@{partsStackZ, @"minSize":@"7,0,0"}, @[			//partsLinkY
			o(mbA,		aMinAnd(0, @[@"bunL/bkw", @"bunA/bkw"],	@{flip} )),
			o(mfA,		aMinAnd(0, @[@"bunL/fwd", @"bunA/fwd"],	@{flip} )),
			o(upA,		aBroadcast(0, @[@"mfA", @"mbA",/*@"mirA",*/@"bunL/a"],	@{selfLinkY, flip} )),
						aMirror(@"upA"),
		  ])),
		o(bunB,		aNet(@{partsStackZ, @"minSize":@"7,0,0"}, @[			//partsLinkY
			o(mbB,		aMinAnd(0, @[@"bunL/bkw", @"bunB/bkw"],	@{flip} )),
//			o(mirB,		aMirror(0, @{@"gain":@0.2, flip})),
			o(mfB,		aMinAnd(0, @[@"bunL/fwd", @"bunB/fwd"],	@{flip} )),
			o(upB,		aBroadcast(0, @[@"mfB",@"mbB",/*@"mirB",*/@"bunL/b"],	@{selfLinkY, flip} )),
		  ])),

	 //		 TOP:
//		o(bun4,		aTunnel(@{@"structure":z_bundle, @"proto":aGenMaxLeaf(), flip, selfLinkY})),
//					aGenerator(@{@"events":@[],@"resetTo":@"z", @"P":@"bun4", selfLinkY}),
						])),
																		]}	));

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark * OLD Three Bit Lang
 // 170617 bug Birthplace bad
 // 160226 bug FIXED 160317 birth canal doesn't open with button UP
M(BAD: Old Shaft Assim +gui,  (@{CameraHsuz(0, 45, 10, 1.3),	@"fontNumber":@"4", @"parts":@[
	aNet(@{partsStackY},	@[			//partsStackY//partsLinkY
		aGenerator(0, @{@"nib":@"LoGen_fwdBkw", flip, @"P":@"bun", @"resetTo":@[@"a=1", @"fwd=1"]}	),
		aTunnel(@{n(bun), @"structure":@[@"a", @"b", @"c", @"fwd:@1", @"bkw:@1"],
				  @"proto":@[aFlipPrevLeaf(@{@"mode":@"simMode2", @"bias":@0, @"M":@"fwd^", @"N":@"bkw^"}, @{spin$0}),
							 aGenBcastLeaf()]
		}),
		aLink(@"a/prev.S^", @"c/prev.T^"),
		aLink(@"b/prev.S^", @"a/prev.T^"),
		aLink(@"c/prev.S^", @"b/prev.T^"),

		anActor(@{n(act1), @"minHeight":@(minHeight),
			@"con":aTunnel(@{@"structure":xyz_bundle, @"proto":aGenMaxsqLeaf(0,@{@"U":@"write"}, @{spin$0}), flip}),
			@"evi":aBundle(@{@"structure":abc_bundle, @"proto":aPrevBcastLeaf(@{/*selfStackY*/},@{@"U":@"write"})}),
		}),
		aLink(@"bun/a/prev", @"act1/evi/a/prev.P"),
		aLink(@"bun/b/prev", @"act1/evi/b/prev.P"),
		aLink(@"bun/c/prev", @"act1/evi/c/prev.P"),
		aWriteHead(@{@"actor":@"act1", @"babyPrototype":@"Hamming"}),
//. 	THIS IS A 2-BundleTap TEST
		aGenerator(@{@"nib":@"HiGen_xyz", @"resetTo":@[@"z=1"], @"P":@"act1/con"}) ,
							]),
																		]}	));
R(Old Shaft Assim +gui,  (@{CameraHsuz(0, 45, 10, 1.5),	@"fontNumber":@"4", @"parts":@[
	aNet(@{partsStackY},	@[			//partsStackY//partsLinkY
//		aGenerator(0, @{@"events":@[@"a", @[], @"again"], flip, @"P":@"bun"}	),
		aGenerator(0, @{@"nib":@"LoGen_a", flip, @"P":@"bun"}	),
o(bun,	aTunnel(@{@"structure":@[@"a"], @"proto":aGenBcastLeaf() })),
		aMirror(@"a"),
							]),
																		]}	));
id testSeq  = @[@[@"a"], @[],				@"again"];
id testSeq0 = @[@[],						@"again"];				// works
id testSeq1 = @[@[@"a"], @[],@[],@[],		@"again"];				// works
id testSeq2 = @[@[@"a"], @[@"a"],@[],@[],	@"again"];				// works

R(gen+assim+gui,(@{CameraHsuz(0, 70, 10, 1),						@"parts":@[
	aGenerator(0, @{@"events":testSeq, flip, @"P":@"act1/evi", @"resetTo ":@[@"a"]} ),
	anActor(@{n(act1), @"minHeight":@(minHeight),
		@"evi":aTunnel(@{@"structure":a_bundle, @"proto":aGenPrevBcastLeaf(0,0,@{@"U":@"write"})}),
		@"con":aBundle(@{@"structure":z_bundle, @"proto":aGenMaxsqLeaf(@{@"value":@1.0},@{@"U":@"write"}), flip}),
											}),
	aWriteHead(@{@"actor":@"act1", @"babyPrototype":@"Hamming"}),
																		]}	));
M(C. S3-ring3, (@{CameraHsuz(0, -45, 15, 1.5),						@"parts":@[
	aGenerator(0, @{@"events":@[@[@"a"], @[@"b"], @[@"c"], @"again"],flip, @"P":@"act1/evi"}),
	anActor(@{n(act1), @"minHeight":@(minHeight),
		@"evi":aTunnel(@{@"structure":abc_bundle, @"proto":aGenPrevBcastLeaf(0, 0, @{@"U":@"write"})}),
		@"con":aBundle(@{@"structure":@[@"z"], @"proto":aGenMaxsqLeaf(@{@"value":@1.0},@{@"U":@"write"}), flip}),
	}),
	aWriteHead(@{@"actor":@"act1", @"babyPrototype":@"Hamming"}),
																		]}	));
 // 160314 bug Previous doesn't work	160411 okay
R(gen+assim+gui,(@{CameraHsuz(0, 70, 10, 1),						@"parts":@[
				aGenerator(0, @{@"events":testSeq2, flip, @"P":@"bun", @"resetTo ":@[@"a"]}),	//act1/evi
	o(bun,		aTunnel(@{@"structure":a_bundle, @"proto":aGenPrevBcastLeaf() })),
																		]}	));
 // 160225 BUG 1. z not set in pannel after start	persists 160411
R(gen+assim+gui bug,(@{CameraHsuz(0, 10, 20, 1),					@"parts":@[
	o(gen,	aBundle(@{@"structure":xyz_bundle, @"proto":aGenBcastLeaf(), flip} )),
			aGenerator(@{@"nib":@"HiGen_xyz", @"resetTo":@[@"z"], @"P":@"gen"}),
																		]}	));
 // 160122 chaining bundles bug FIXED
R(gen+assim+gui,(@{CameraHsuz(0, 10, 20, 1.5),						@"parts":@[
	o(gen,	aTunnel(@{@"structure":@[@"a", @"b", @"fwd:@1", @"bkw:@1"],
					  @"proto":@[aFlipPrevLeaf(@{@"mode":@"simMode2", @"bias":@0, spin$L }),
								 aGenBcastLeaf()] })),
																		]}	));
 // generator by itself
M(gen only+gui,(@{CameraHsuz(0, 10, 20, 1.5),						@"parts":@[
	o(stacks,aNet(@{partsStackX}, @[
		o(spin,	aNet(@{partsStackY}, @[
					aGenerator(@{@"nib":@"LoGen_fwdBkw",	flip, @"P":@"gen", @"resetTo":@[@"a"]}	),
			o(gen,	aTunnel(@{@"structure":@[@"a", @"b", @"c", @"d", @"fwd:@1", @"bkw:@1"],
							  @"proto":@[aFlipPrevLeaf(@{@"mode":@"simMode2", @"bias":@0, @"M":@"fwd^", @"N":@"bkw^", spin$L }),
										 aGenBcastLeaf()]	})),
					aLink(@"a/prev.S^", @"d/prev.T^"),
					aLink(@"b/prev.S^", @"a/prev.T^"),
					aLink(@"c/prev.S^", @"b/prev.T^"),
					aLink(@"d/prev.S^", @"c/prev.T^"),
						])),
									])),
																		]}	));
	R(gen only+gui,(@{CameraHsuz(0, 10, 20, 1.5),						@"parts":@[	//xyzzy22//
		o(stacks,aNet(@{}, @[
					aGenerator(@{@"nib":@"LoGen_a",	flip, @"P":@"gen", @"resetTo":@[@"a"]}	),
			o(gen,	aTunnel(@{@"structure":@[@"a"]	})),
										])),
																			]}	));

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark * Language Deserializer (ld)
// Marry told Bill that the Mayor of Boston will fix the leak
id ldWords = @[@"Marry", @"told", @"Bill", @"that", @"the", @"Mayor", @"of",
										@"Boston", @"will", @"fix", @"leak"];
 //++++ 160815 Net>Atom>Part bug
R(Language Deserializer,  (@{CameraHsuz(0, 0, 0, 1), @"boundsDrapes":@0.1, @"displayAxis":@1, @"parts":@[
//			aGenerator(0, 0.5,
//		@{flip, @"P":@"gen", @"resetTo":@[/*@"gen/a", @"fwd", @"bkw"*/]}	)),
	o(gen, aTunnel(@{@"structure":a_bundle, @"proto":aGenBcastLeaf() })),
																		]}	));


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark - *. Matrix Actor
/************************/ setParentMenu(@"*. Matrix Actor"); /***************/
////////////////////////////////////////////////
/////      Matrix Actor					   /////	W.I.P.
////////////////////////////////////////////////
//M(actr a yz,(@{CameraHsuz(0, 10, 20, 1),							@"parts":@[
//	aMatrixActor(@{n(act1",
//		@"minHeight":@(minHeight),
//		@"con":aTunnel(@{@"structure":yz_bundle, @"jog":@"0, 0, 0", flip}),
//		@"evi":aTunnel(@{@"structure":ab_bundle, @"proto":aBcastLeaf(), @"jog":@"0, 0, 0"}),
//	}),
//	]}	));

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark - F. Morse Code
/************************/ setParentMenu(@"F. Morse Code"); /***************/
////////////////////////////////////////////////
/////      Morse Code					   /////
////////////////////////////////////////////////
	id  $		= @[@"d_t='_'"									];	// <NIL>
	id  A		= @[@"d_t=A",	@"d_i", @"d_a"					];	// a .-
	id  B		= @[@"d_t=B",	@"d_a", @"d_i", @"d_i", @"d_i"	];	// b -...
	id  C		= @[@"d_t=C",	@"d_a", @"d_i", @"d_a", @"d_i"	];	// c -.-.
	id  D		= @[@"d_t=D",	@"d_a", @"d_i",	@"d_i"			];	// d -..
	id  E		= @[@"d_t=E",	@"d_i"							];	// e .
	id  F		= @[@"d_t=F",	@"d_i",	@"d_i",	@"d_a",	@"d_i"	];	// f ..-.
	id  G		= @[@"d_t=G",	@"d_a",	@"d_a",	@"d_i"			];	// g --.
	id  H		= @[@"d_t=H",	@"d_i",	@"d_i",	@"d_i", @"d_i"	];	// h ....
	id  I		= @[@"d_t=I",	@"d_i",	@"d_i"					];	// i ..
	id  J		= @[@"d_t=J",	@"d_i",	@"d_a",	@"d_a", @"d_a"	];	// j .---
	id  K		= @[@"d_t=K",	@"d_a",	@"d_i",	@"d_a"			];	// k -.-
	id  L		= @[@"d_t=L",	@"d_i",	@"d_a",	@"d_i", @"d_i"	];	// l .-..
	id  M		= @[@"d_t=M",	@"d_a",	@"d_a"					];	// m --
	id  N		= @[@"d_t=N",	@"d_a",	@"d_i"					];	// n -.
	id  O		= @[@"d_t=O",	@"d_a",	@"d_a",	@"d_a"			];	// o ---
	id  P		= @[@"d_t=P",	@"d_i",	@"d_a",	@"d_a", @"d_i"	];	// p .--.
	id  Q		= @[@"d_t=Q",	@"d_a",	@"d_a",	@"d_i", @"d_a"	];	// q --.-
	id  R		= @[@"d_t=R",	@"d_i",	@"d_a",	@"d_i"			];	// r .-.
	id  S		= @[@"d_t=S",	@"d_i",	@"d_i",	@"d_i"			];	// s ...
	id  T		= @[@"d_t=T",	@"d_a"							];	// t -
	id  U		= @[@"d_t=U",	@"d_i",	@"d_i",	@"d_a"			];	// u ..-
	id  V		= @[@"d_t=V",	@"d_i",	@"d_i",	@"d_i", @"d_a"	];	// v ...-
	id  W		= @[@"d_t=W",	@"d_i",	@"d_a",	@"d_a"			];	// w .--
	id  X		= @[@"d_t=X",	@"d_a",	@"d_i",	@"d_i", @"d_a"	];	// x -..-
	id  Y		= @[@"d_t=Y",	@"d_a",	@"d_i",	@"d_a", @"d_a"	];	// y -.--
	id  Z		= @[@"d_t=Z",	@"d_a",	@"d_a",	@"d_i", @"d_i"	];	// z --..
	id  $again  = @[@"again"];
	id  $speak  = @[@"d_t", @[], @"speak=SPEAK"];

	id  A_D		= cat(A, B, C, D,								nil);
	id  A_G		= cat(A, B, C, D, E, F, G,						nil);
	id  A_Z		= cat(A, B, C, D, E, F, G, H, I, J, K, L, M,
					  N, O, P, Q, R, S, T, U, V, W, X, Y, Z,	nil);

#define farView 0		// close: not viewAsAtom, displayInvisibleLinks
//#define farView 1
id viewAsAtom				= @(farView);
id displayInvisibleLinks	= @(1-farView);
morseBundle					= 0? didat_sound_bundle : didat_bundle;


   // Define a 'listen then speak' network, so it can be used with many stimulus
  // Define as block, so it can be a) multiply invoked, b) defined here,
 //    c) not involve #defines, which are displayed on #pragma marks
id (^listenSpeak)(int nEvents, NSArray *events, id etc1, id etc2) = ^id (int nEvents, NSArray *events, id etc1, id etc2) { return
	@{CameraHsuz(1, 135, 0, 1.0), @"boundsDrapes":@"0.0", @"displayAxis":@0, @"displayLeafNames":@1,
							@"asyncData":@1, VEL(-1), etc1etc, @"parts":@[ //@"speak":@0,		//xyzzy11 complex/broken
		etc1[@"gui"]?
		    aGenerator(@{n(morse), @"nib":@"LoGen_morse",                     flip, @"P":@"act1/evi"}):
			aGenerator(@{n(morse), @"events":events, @"nEvents":@(nEvents),   flip, @"P":@"act1/evi"}),
		anActor(@{n(act1),	@"minHeight":@15, @" viewAsAtom":@1, @" displayInvisibleLinks":@1, etc2etc,

			@"con":aBundle(@{}),			/**** CONTEXT BUNDLE: ****/

			@"parts":@[						/**** TREE BASE: ****/
				o(mirror,	aMirror(0,								  @{flip})),
				o(base,	aBranch(@{@"S":@"mirror",
								  @"M":@"d_t",
								  @"U":@"write@",
								  @"Share":@"MaxOr", @"SPEAK@":@"speak",	// 170408PAK remove untested hair.
								})),
					  ],
											/**** EVIDENCE BUNDLE: ****/
			@"evi":aTunnel(@{@"structure":(@[@"speak:@1", @"d_i:sound:di-sound", @"d_t:sound:t-sound", @"d_a:sound:da-sound"]),//@[@"speak:@1", @"d_i", @"d_t", @"d_a"]
							 @"displayLeafNames":@1,
							 @"proto":@[aGenSoundBcastLeaf(		/// 0: bits for d_i, d_a, and d_t:
											@{@"LOOP":@"speak"}, 0,		/// --> to Gen
											@{@"sec":@"write",				// each bit goes to Write Head
											  @"Share":@[@"Broadcast", @"MaxOr"], @"KIND@":@"speak",
											  @"SPEAK@":@"speak", }),	///s --> to Bcast
										aGenBcastLeaf(), ]	/// 1: bit for "speak"
							}),							/// indexed by structure
			}),
		aWriteHead(@{@"actor":@"act1", @"babyPrototype":@"Branch"}),
															]};				};

#pragma mark F0: Listen to Gui
M(F0: Listen to Gui,		(listenSpeak(0,  cat(		$again, nil), @{@"gui":@1}, nil)));
//M(F0: Listen to Gui,		(listenSpeak(0,  cat(E,M,	$again, nil), @{@"gui":@1}, nil)));		// hangs on d_a

R(bug works,				(listenSpeak(0,  cat(E,		$again, nil), nil, nil)));
R(bug fails,				(listenSpeak(3,  cat(E,		$again, nil), nil, nil)));

#pragma mark F1: Listen Again
//M(F1: Listen Again ' ', 	(listenSpeak(0,  cat(		$again, nil), nil, nil)));	// empty
M(F1: Listen Again 'E', 	(listenSpeak(1,  cat(E,		$again, nil), nil, nil)));
M(F1: Listen Again '$E',	(listenSpeak(4,  cat($,T,	$again, nil), nil, nil)));	//?
M(F1: Listen Again 'ET',	(listenSpeak(0,  cat(E,T,	$again, nil), nil, nil)));
M(F1: Listen Again 'EM',	(listenSpeak(1,  cat(E,M,	$again, nil), nil, nil)));
M(F1: Listen Again 'M', 	(listenSpeak(4,  cat(M,		$again, nil), nil, nil)));
M(F1: Listen Again 'NM',	(listenSpeak(6,  cat(N,M,	$again, nil), nil, nil)));
M(F1: Listen Again 'KM',	(listenSpeak(2,  cat(K,M,	$again, nil), nil, nil)));
M(F1: Listen Again 'CQ',	(listenSpeak(11, cat(C,Q,	$again, nil), nil, nil)));
M(F1: Listen Again 'EIS',	(listenSpeak(10, cat(E,I,S,	$again, nil), nil, nil)));
M(F1: Listen Again 'A..G',	(listenSpeak(-1, cat(A_G,	$again, nil), nil, nil)));
M(F1: Listen Again 'A..Z',	(listenSpeak(-1, cat(A_Z,	$again, nil), nil, nil)));
M(F1: Listen Again 'IANM',	(listenSpeak(-1, cat(I,A,N,M,$again,nil), nil, nil)));

#pragma mark F2: Listen/Speak
//M(F2: Listen/Speak ' ', 	(listenSpeak(0,  cat(		$speak, nil), nil, nil)));	// empty
M(F2: Listen/Speak 'E', 	(listenSpeak(-1, cat(E,		$speak, nil), nil, nil)));	// 3
M(F2: Listen/Speak 'ET', 	(listenSpeak(-1, cat(E,T,	$speak, nil), nil, nil)));	// 6 to just before speak ON
M(F2: Listen/Speak 'EI',	(listenSpeak(-1, cat(E,I,	$speak, nil), nil, nil)));	// 7 to just before speak ON
//xM(F2: Listen/Speak 'EI',	(listenSpeak(7,  cat(E,I,	$speak, nil), @{VEL(-1)}, nil)));	// 7 to just before speak ON
M(F2: Listen/Speak 'KR',	(listenSpeak(-1, cat(K,R,	$speak, nil), nil, nil))); // BUGGY!
M(F2: Listen/Speak 'EIS',	(listenSpeak(-1, cat(E,I,S,	$speak, nil), nil, nil)));	// 12
M(F2: Listen/Speak 'CQ',	(listenSpeak(-1, cat(C,Q,	$speak, nil), nil, nil)));	// 13
M(F2: Listen/Speak 'A..D',	(listenSpeak(-1, cat(A_D,	$speak, nil), nil, nil)));
M(F2: Listen/Speak 'A..G',	(listenSpeak(-1, cat(A_G,	$speak, nil), nil, nil)));


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark F3: Speak Fixed Tree
//#define	level2gen 1.0			// minimal	1-layer		E
//#define 	level2gen 1.5			// full		1-layer		Et
//#define 	level2gen 2.0			// partial	2-layer		et I
#define 	level2gen 2.5			// full		2-layer		et Ianm
//#define 	level2gen 3.0			// partial	3-layer		et ianm S
//#define 	level2gen 3.5			// full		3-layer		et ianm Surwdkgo	I

M(F3: Fixed Speak,  (@{CameraHsuz(5, 155, 0, 1.5), VEL(-6), @"boundsDrapes ":@"0.1",@"displayAxis ":@1,	// (1.5, 145, 0, 1.2)
			@"speak":@1, @"asyncData":@1, @"linkRadius":@0.1, @"linkEventRadius":@1.5, @"viewAsAtom":@0, @"parts":@[	//xyzzy11 either
	aGenerator(0, @{@"events":@[], flip, @"P":@"act1/evi"}),	//morse_E
	anActor(@{n(act1), @" viewAsAtom":@1, @"displayInvisibleLinks":@1, @"minHeight":@(minHeight),
			  
		@"con":aBundle(@{@"parts":@[	/**** CONTEXT BUNDLE: ****/
			ol(0.0, base$,	aGenAtom(@{@"P":@"base",@"value":@((int)level2gen==0)})),
			ol(1.0, Ei$,	aGenAtom(@{@"P":@"Ei",	@"value":@((int)level2gen==1)})),
			ol(1.5, Ta$,	aGenAtom(@{@"P":@"Ta",	@"value":@0.0})),
			ol(2.0, Iii$,	aGenAtom(@{@"P":@"Iii",	@"value":@((int)level2gen==2)})),
			ol(2.5, Aia$,	aGenAtom(@{@"P":@"Aia",	@"value":@0.0})),
			ol(2.5, Nai$,	aGenAtom(@{@"P":@"Nai",	@"value":@0.0})),
			ol(2.5, Maa$,	aGenAtom(@{@"P":@"Maa",	@"value":@0.0})),
			ol(3.0, Siii$,	aGenAtom(@{@"P":@"Siii",@"value":@((int)level2gen==3)})), // simple stimulus
			ol(3.5, Uiia$,	aGenAtom(@{@"P":@"Uiia",@"value":@0.0})),
			ol(3.5, Riai$,	aGenAtom(@{@"P":@"Riai",@"value":@0.0})),
			ol(3.5, Wiaa$,	aGenAtom(@{@"P":@"Wiaa",@"value":@0.0})),
			ol(3.5, Daii$,	aGenAtom(@{@"P":@"Daii",@"value":@0.0})),
			ol(3.5, Kaia$,	aGenAtom(@{@"P":@"Kaia",@"value":@0.0})),
			ol(3.5, Gaai$,	aGenAtom(@{@"P":@"Gaai",@"value":@0.0})),
			ol(3.5, Oaaa$,	aGenAtom(@{@"P":@"Oaaa",@"value":@0.0})),
								] } ),
		@"parts":@[						/**** TREE: ****/
			ol(0.0, mirror,	aMirror(0,								  @{flip})),
			#define arg44 @"SPEAK":@"speak", @"Share":@"MaxOr"										// Simplest
			#define arg45 @"SPEAK":@"speak", @"Share":@[@"MaxOr", @"Broadcast"], @"KIND":@"speak"	// Closest to ListenSpeak
			ol(0.0, base,	aBranch(@{@"S":@"mirror",@"M":@"d_t@", arg44, @"jog":@"0,0,2"})),
			ol(1.0, Ei,		aBranch(@{@"S":@"base",	 @"M":@"d_i@", arg44})),
			ol(1.5, Ta,		aBranch(@{@"S":@"base",	 @"M":@"d_a@", arg44})),
			ol(2.0, Iii,	aBranch(@{@"S":@"Ei",	 @"M":@"d_i@", arg44})),
			ol(2.5, Aia,	aBranch(@{@"S":@"Ei",	 @"M":@"d_a@", arg44})),
			ol(2.5, Nai,	aBranch(@{@"S":@"Ta",	 @"M":@"d_i@", arg44})),
			ol(2.5, Maa,	aBranch(@{@"S":@"Ta",	 @"M":@"d_a@", arg44})),
			ol(3.0, Siii,	aBranch(@{@"S":@"Iii",	 @"M":@"d_i@", arg44})),
			ol(3.5, Uiia,	aBranch(@{@"S":@"Iii",	 @"M":@"d_a@", arg44})),
			ol(3.5, Riai,	aBranch(@{@"S":@"Aia",	 @"M":@"d_i@", arg44})),
			ol(3.5, Wiaa,	aBranch(@{@"S":@"Aia",	 @"M":@"d_a@", arg44})),
			ol(3.5, Daii,	aBranch(@{@"S":@"Nai",	 @"M":@"d_i@", arg44})),
			ol(3.5, Kaia,	aBranch(@{@"S":@"Nai",	 @"M":@"d_a@", arg44})),
			ol(3.5, Gaai,	aBranch(@{@"S":@"Maa",	 @"M":@"d_i@", arg44})),
			ol(3.5, Oaaa,	aBranch(@{@"S":@"Maa",	 @"M":@"d_a@", arg44})),
		  ],
										/**** EVIDENCE BUNDLE: ****/
		@"evi":aTunnel(@{@"structure":1? didat_sound_bundle:didat_bundle,
						 @"proto":aGenSoundMaxLeaf(@{@"loop":@1}),}),	// aGenBcastLeaf
	}),
															]} ))
    //170206 wiggles bout right, needs scruitiny.
   // 170130 needs attention
  //  170125 Simplest Serialization Example: works!
 //   161209 fixed bug: Improper deserialization S -> ii, not iii fixed 161111
r( (@{CameraHsuz(1.5, 80, 0, 1.3), VEL(-5), @"boundsDrapes ":@"0.1",@"displayAxis ":@1,	// (1.5, 145, 0, 1.2)
			@"speak":@1, @"asyncData":@1,						@"parts":@[		//xyzzy11 either
	aGenerator(0, @{@"events":@[@"d_t", @"", @"again"], flip, @"P":@"act1/evi"}),
	anActor(@{n(act1),		@" viewAsAtom":@1, @"displayInvisibleLinks":@1,		@"minHeight":@(minHeight),
										/**** CONTEXT BUNDLE: ****/
		@"con":aBundle(@{@"structure":aBundle(@{flip,				@"parts":@[
								ol(3.0, ttt$,	aGenAtom(@{@"P":@"ttt", @"value":@((int)level2gen==3)})),
								ol(2.0, tt$,	aGenAtom(@{@"P":@"tt",	@"value":@((int)level2gen==2)})),
								ol(1.0, t$,		aGenAtom(@{@"P":@"t",	@"value":@((int)level2gen==1)})),
						  ] } ),
						@"proto":aGenMaxLeaf(), flip}),
		@"parts":@[						/**** TREE: ****/
			ol(0.0, base,	aMirror(0,						    @{flip})),
			ol(1.0, t,		aBranch(@{@"S":@"base",	   @"M":@"d_t@", @"SPEAK":@"speak", @"jog":@"0,0,6"})),
			ol(2.0, tt,		aBranch(@{@"S": @"t,l=1.5",@"M":@"d_t@", @"SPEAK":@"speak"})),
			ol(3.0, ttt,	aBranch(@{@"S":@"tt,l=1.5",@"M":@"d_t@", @"SPEAK":@"speak"})),
		  ],
										/**** EVIDENCE BUNDLE: ****/
		@"evi":aTunnel(@{@"structure":1? t_sound_bundle:t_bundle,
						 @"proto":aGenSoundMaxLeaf  (@{@"loop":@1}), }),		// The desired way
//						 @"proto":aGenBcastLeaf(@{@"loop":@1}), }),		// Things wiggle, but incorrectly
	}),
																	]} ))
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
		///	A better layout it constructing the tree upside down
#undef		level2gen
#define 	level2gen 0.5
#pragma mark F4: Speak Fixed OLD

//	@[spin_L, @"Hiiii$", @"Viiia$", @"Siii$",		@"Liaii$", @"_iaia$", @"Riai$"],
//	@[spin_L, @"Fiiai$", @"_iiaa$", @"Uiia$",		@"Piaai$", @"Jiaaa$", @"Wiaa$"],
//			  @"base$",
//	@[spin_L, @"Baiii$", @"Xaiia$", @"Daii$",		@"Zaaii$", @"Qaaia$", @"Gaai$"],
//	@[spin_L, @"Caiai$", @"Yaiaa$", @"Kaia$",		@"_aaaa$", @"_aaai$", @"Oaaa"]];



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//#pragma mark ..... Fixed Trees Level 3 suirwadkngom..
//id alphas3_bundle = @[spin_R,
//	@[spin_L, @"c_u",	@"c_i",		@"c_s",
//			  @"c_w",	@"c_a",		@"c_r"],
//			  @"c_e",   @"c__",     @"c_t",
//	@[spin_L, @"c_k",	@"c_n",		@"c_d",
//			  @"c_o",	@"c_m",		@"c_g"]];
//	o(d2cTree,	aNet(@{flip}, @[			//// ABOVE  -- tree and Chars bundle
//		o(chars,aBundle(@{@"structure":alphas3_bundle, of_genAgo(), @{})),
//		o(l_i,	aDiDa(@"c_s",	@"c_u",		@"c_i",			@{flip, spin$3})),	// s... u..-	i..
//		o(l_a,	aDiDa(@"c_r",	@"c_w",		@"c_a",			@{flip, spin$3})),	// r.-. w.--	a.-
//		o(l_n,	aDiDa(@"c_d",	@"c_k",		@"c_n",			@{flip, spin$3})),	// d-.. k-.-	n-.
//		o(l_m,	aDiDa(@"c_g",	@"c_o",		@"c_m",			@{flip, spin$3})),	// g--. o---	m--
//
//		o(l_e,	aDiDa(@"l_i/ago",@"l_a/ago",@"c_e",			@{flip})),			// i..  a.-		e.
//		o(l_t,	aDiDa(@"l_n/ago",@"l_m/ago",@"c_t",			@{flip})),			// n-.	m--		t-
//
//		o(l__,	aDiDa(@"l_e/ago",@"l_t/ago",@"c__",			@{flip, spin$1})),	// e.	t-		_
//					])),
//				aLink( @"d_t", @"l__/ago"),
//																		]}	));

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//// 161005 Prototype for movie
//#pragma mark F5: Morse Movie
/*

/// 0: ------------------------------- morseBundle, with symbols
	Morse Code affords us an example,
		to recognize simple sequences of symbols.
	It uses three symbols:
		a short d_i,
		a long  d_a, and
		a separator d_t.
/	As the simulation runs
|		green shows sensations we have going up
|		red   shows desirses   we want going down,
|		black is both red and green, and
\		white is neither

///// 1: ------------------------------- Learning the CQ Tree
	Here's a HaveNWant Network
		which listens and creates a tree
			that records what it hears
	We'll send it the letters C and Q:
										Labels	Da-di-da-di-t
												Da-da-di-da-t
/	The blue labels shown are for us to understand
|		the labels C and the Q are not used in computation
\		The computations involve only haves and wants, not labels
/// ------------------------------- Speak from the CQ Tree
	This experientially formed tree, 
		will recognize its sequences if repeated.
	If one of its leafs (at the top) is stimulated,
		the sequence that generated it will be spoken.

/// ------------------------------- HELLO WORLD
	Now a bigger example.
	We'll send the letters in "hello" into an empty Network.
										Labels	H is di-t
												E is da-t
												L is di-da-di-di-t
												O is da-da-da-t
			Note that L is sent twice, and
				is recognized a second time it is heard
	Now we have a second network above it,
		to store sequences of letters,
			such as "h e l l o"
			as words "hello"
	We'll add another world "world".
		The new characters w, r, l and d 
			are added to the lower Actor??
		The new word "word" 
			is added to the second Actor

	A second-level tree is formed, 
			mapping sequences of letters into words
		It stores the sequence of letters h-e-l-l-o
			as a word.
			We label it as "hello".

	A third-level tree is formed,
			mapping sequences of words into sentances
		It stores the sequence of words "hello" and "world"
			as a sentance.

	These Networks, of characters, words, and sentances,
		are bidirectional:
		Stimulating the higher level leaf for this sentance
			causes it to be serialized...
				di-di-di-di-t, di-t, di-da-di-di-t, di-da-di-di-t, da-da-da-t, t,
				di-da-di-di-t, da-da-da-t, di-di-di-t, di-da-di-di-t, da-di-di-t, t, t,
 
 */

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark - Music
/************************/ setParentMenu(@"Music Bayesians"); /***************/
////////////////////////////////////////////////
/////      Music						   /////
////////////////////////////////////////////////

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark Bayes1+gui
M(Bayes1+gui,(@{CameraHsuz(0, 45, 10, 1), 							@"parts":@[
			aGenerator(@{@"nib":@"LoGen_a", @"resetTo":@[/*s@"a"@"a/gen"*/], flip, @"P":@"bun"}),
	o(bun,	aTunnel(@{@"structure":a_bundle, @"proto":aGenBcastLeaf() })),
	o(ab,	aBayes(@[@"a"], @[@"x", @"y", @"z"])),
	o(bun2,	aTunnel(@{@"structure":xyz_bundle, @"proto":aGenBcastLeaf(), flip})),
			aGenerator(@{@"nib":@"HiGen_xyz", @"P":@"bun2"}),
																		]}	));
#pragma mark Bayes2+gui
M(Bayes2+gui,(@{CameraHsuz(0, 45, 10, 1), VEL(-7), 					@"parts":@[
			aGenerator(1, @{@"events":@[@"a", @"again"], @"resetTo":@[/*@"a"*/], flip, @"P":@"bun"}),
	o(bun,	aTunnel(@{@"structure":a_bundle, @"proto":aGenBcastLeaf() })),
//	o(aa,	aBroadcast(@"a")),
	o(ab,	aBayes(@[@"a"], @[@"y",@"z"])),
	o(bun2,	aTunnel(@{@"structure":yz_bundle, @"proto":aGenBcastLeaf(), flip})),
			aGenerator(@{@"events":@[], @"resetTo":@[@"y"], @"P":@"bun2"}),
																		]}	));
 // 161022 bug 161024 -- looping main.P++
xr( (@{CameraHsuz(0, 45, 10, 1), VEL(-3),					@"parts":@[

	o(bun,	aBundle(@{@"structure":a_bundle, @"proto":aGenBcastLeaf(), })),		// A: FAILS (loops)
//	o(a,	aBroadcast()),								// B: WORKS
//	o(a,	aGenBcastLeaf()),								// C: WORKS

			aLink(@"a,l=5", @"z"),
	o(bun2,	aTunnel(@{@"structure":z_bundle, @"proto":aGenBcastLeaf(), flip})),
			aGenerator(@{@"events":@[@"z", @[], @"again"], @"resetTo":@[@"z"], @"P":@"bun2"}),
																		]}	));
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

////151211 happy, shows something, numerics (and even purpouse) bad:
M(music Bundle marPort, (@{CameraHsuz(0, 0, 0, 1),					@"parts":@[
	aGenerator(@{@"nib":@"LoGen_abc", flip, @"P":@"act1/evi"}),
	anActor(@{	n(act1),
		@"con":aTunnel(@{@"structure":yz_bundle, @"proto":aGenBcastLeaf(), flip}),
		@"parts":@[
			o(mar0, aBayes(@[@"a"], 0)),
			o(mar1, aMarkovPort(@[@"mar0"], @[@"b!"] )),
			o(mar2, aMarkovPort(@[@"mar0"], @[@"c!"] )),
			o(ham1,	aHamming(@"z", @[@"mar1/mBay"],				@{flip})),
			o(ham2,	aHamming(@"y", @[@"mar2/mBay"],				@{flip})),
			o(ham3,	aHamming(@"y", @[@"mar1/mBay",@"mar2/mBay"],@{flip})),
		  ],
		@"evi":aTunnel(@{@"structure":abc_bundle, @"proto":aGenBcastLeaf()}),
	}),
	aGenerator(@{@"nib":@"HiGen_yz", @"resetTo":@[@"z"], @"P":@"act1/con"}),
																		]}	));

//+++	151209 not sure about functionality yet +b
M(music <Bundle> seq, (@{CameraHsuz(0, 0, 0, 1),					@"parts":@[
			aGenerator(-1, @{@"events":@[@[@"a"],@[@"b"],@[@"a"],@[@"c"],@[],@"again"], flip, @"P":@"bun"}),
	o(bun,	aTunnel(@{@"structure":abc_bundle, @"proto":aGenBcastLeaf() })),
	o(mar0, aMarkovPort(@[@"a"], @[@"a!"] )),
	o(mar1, aMarkovPort(@[@"mar0/mBay"], @[@"b!"] )),
	o(mar2, aMarkovPort(@[@"mar0/mBay"], @[@"c!"] )),
																		]}	));
 // 151110 Bug: positioning '/r1/b1' by nan Port '/r1/b0U.1'  WORKS
R(Bay abac, (@{CameraHsuz(0, 0, 0, 1),								@"parts":@[
	o(b1,	aBayes(0, 0, @{flip})),
	o(b0,	anAgo(0, @{@"S":@"b1", flip})),
	o(ba,	aRotator(@"b0", @{spin$2})),
																		]}	));
// // 151113 debug dominant positioning WORKS
//r(			 (@{CameraHsuz(0, 0, 0, 1),							@"parts":@[
//	o(bun,	aBundle(@{@"structure":ab_bundle, aGenBcastLeaf())),
//	o(a1,	aMinAnd(@"a")),
//	o(bb,	aMaxOr(0, @[@"a1", @"b!"],					@{flip})),
//																		]}	));
//
//M(1 MarkovPort proto, (@{CameraHsuz(0, 0, 0, 1),					@"parts":@[
//	o(bun,	aBundle(@{@"structure":ab_bundle, aGenBcastLeaf() )),
//	o(spin, aNet(@{flip}, @[
//		o(b1,	aBayes(@"b0", 0,							@{flip})),
//		o(b0,	anAgo(0, @{flip})),
//		o(ba,	aRotator(@"b0",								@{spin$2})),
//		o(bb,	aMaxOr(  @"ba.T", @"a")),
//		o(bc,	aHamming(@"ba.S", @"b")),
//									])),
//																		]}	));

//id doeADear = @[@[@"d"],@[@"r"],@[@"m"], @[],@"again"];
//id bestDayBB = @[@[@"d"],@[@"m"], @[@"d"],@[@"m"], @[@"r"],@[@"d"], @[],@"again"];
 // Early, deletable
//M(doe a dear (minimal),  (@{CameraHsuz(0, 0, 0, 1),					@"parts":@[
//			 aGenerator(0, doeADear,					@{flip, @"P":@"bun"})),
//	o(bun,	aTunnel(@{@"structure":drm_bundle, aGenBcastLeaf(0, @{@"U":@"write"}) )),
//	o(d1,	anAgo(@[@"d"])),
//	o(dr,	aMinAnd(0, @[@"d1", @"r"],					@{flip})),
//	o(dr1,	anAgo(@[@"dr"])),
//	o(dm,	aMinAnd(0, @[@"dr1", @"m"],				@{flip})),
//	o(dm1,	anAgo(@[@"dm"])),
//																		]}	));
 // 151115 prune:
//R(best day,  (@{CameraHsuz(0, 0, 0, 1),							@"parts":@[
//			 aGenerator(0, bestDayBB,					@{flip, @"P":@"bun"})),
//	o(bun,	aTunnel(@{@"structure":drm_bundle, aGenBcastLeaf(0,@{@"U":@"write"}) )),
//
//	o(aa,	aMinAnd(0, @[@"d"],						@{flip})),
//	o(ao,	aMaxOr(0, @[@"aa"],						@{flip})),
//	o(a,	anAgo(@[@"ao"])),
//
//	o(ba,	aMinAnd(0, @[@"a", @"m"],					@{flip})),
//	o(bo,	aMaxOr(0, @[@"ba"],						@{flip})),
//	o(b,	anAgo(@[@"bo"])),
//																		]}	));


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark - Soar
/************************/ setParentMenu(@"Soar"); /***************/
//////////////////////////////////////////////////
/////      Soar								/////
////////////////////////////////////////////////

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark Soar 1
// Soar 1 - work on next state
id soarCmds = @[@[@"ena", @"reset"], @[], @[@"ena", @"shift"], @[], @"again"];
M(Soar 1,(@{CameraHsuz(0, 45, 10, 1.6), VEL(-2), 					@"parts":@[
				aGenerator(0, @{@"events":soarCmds, flip, @"P":@"gen"}),
	o(gen,		aTunnel(@{@"structure":@[@[@"d0", @"d1", @"d2", spin_1], @[@"reset:@1", @"ena:@1", @"shift:@1", spin_1]],
						  @"proto":@[aGenPrevLeaf(),  aGenBcastLeaf()] })),

	o(rstDet,	aMinAnd(0, @[@"ena", @"reset"], @{flip})),
	o(shfDet,	aMinAnd(0, @[@"ena", @"shift"], @{flip})),

	o(rstHam,	aHamming(0,  @[         @"d0.+"], @{flip})),
	o(shfHam1,	aHamming(0,  @[@"d0.-", @"d1.+"], @{flip})),
	o(shfHam2,	aHamming(0,  @[@"d1.-", @"d2.+"], @{flip})),
	o(shfHam,	aBroadcast(0,@[@"shfHam1", @"shfHam2"], @{flip})),

	o(rstRot,	aRotator(0, @{@"P":@"z", @"S":@"rstDet", @"T":@"rstHam", @"leftIsFirst":@1, spin$2, flip})),
	o(shfRot,	aRotator(0, @{@"P":@"z", @"S":@"shfDet", @"T":@"shfHam", @"leftIsFirst":@1, spin$2, flip})),

	o(z,		aMaxOr(0, 0, @{flip})),
				aMirror(@"z"),
																		]}	));


R(linking Prev error,(@{CameraHsuz(0, 45, 10, 1.6), VEL(-2), 		@"parts":@[
	o(gen,		aTunnel(@{@"structure":@[@"d0"], @"proto":aPrevLeaf() })),		/// GOOD
//	o(gen,		aTunnel(@{@"structure":@[@"d0"], @"proto":aFlipPrevLeaf() })),	/// BAD
	o(rstHam,	aHamming(0,  @[@"d0.+"         ], @{flip})),
	o(rstHam,	aHamming(0,  @[@"d0.+", @"d0.-"], @{flip})),
																		]}	));
 // OLD
M(Soar 1,(@{CameraHsuz(0, 45, 10, 1.6), VEL(-2), 					@"parts":@[
				aGenerator(0, @{@"events":soarCmds, flip, @"P":@"bun"}),
	o(bun,		aTunnel(@{@"structure":@[@[@"d0", @"d1", @"d2", spin_1], @[@"reset:@1", @"ena:@1", @"shift:@1", spin_1]],
						  @"proto":@[aGenPrevLeaf(), aGenBcastLeaf()] })),

	o(rstDet,	aMinAnd(0, @[@"ena", @"reset"], @{flip})),
	o(shfDet,	aMinAnd(0, @[@"ena", @"shift"], @{flip})),

	o(seqReset,	aSequence(0, @[@"d2,l=5", @"d1", @"d0"], @{@"Px":@"z", @"Sx":@"detReset", @"jog":@"0,0,1", flip})),
	o(seqShift,	aSequence(0, @[@"d2,l=5", @"d1", @"d0"], @{@"Px":@"z", @"Sx":@"shfDet",   @"jog":@"0,0,-1", flip})),

	o(rstRot,	aRotator(0, @{@"P":@"z", @"S":@"rstDet", @"T":@"seqReset", @"leftIsFirst":@1, spin$2, flip})),
	o(shfRot,	aRotator(0, @{@"P":@"z", @"S":@"shfDet", @"T":@"seqShift", @"leftIsFirst":@1, spin$2, flip})),

//	o(z,		aMaxOr(0, 0, @{flip})),
//				aMirror(@"z"),
	o(bun2,		aTunnel(@{@"structure":z_bundle, @"proto":aGenMaxLeaf(), flip})),
				aGenerator(@{@"events":@[], @"resetTo":@[@"z"], @"P":@"bun2"}),
																		]}	));

//id ctl = aTunnel(@{@"structure":@[@"reset", @"ena", spin_1], aGenBcastLeaf());
//id ctr = aTunnel(@{@"structure":@[@"d1", @"d0"],			  aGenMaxLeaf(),	ctl, @{});

 // 161020 BUG with spinning composite bundles
R(Soar 1,(@{CameraHsuz(0, 45, 10, 1), VEL(-7), 						@"parts":@[
	o(bun,		aTunnel(@{@"structure":@[@"d0", @"d1", @"reset:@1", @"ena:@1", spin_1],
						  @"proto":@[aGenMaxLeaf(),  aGenBcastLeaf()] }) ),
//	o(bun,	aTunnel(@{@"structure":@[@[@"d1", @"d0"]],				aGenMaxLeaf(),
//				aTunnel(@{@"structure":@[@[@"reset", @"ena", spin_1]], aGenBcastLeaf()), @{})),
																		]}	));

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark - TIA
//
//		context[4]
//			learned "fluff"
//		aPrevious[3]
//			*		 *
//		counter[3]		aux[4]
//
//A. The task is set to learn the next input, and it is supervised learning.
//	In our noiseless toy world, we submit an algorithm for
//		building an associative memory which builds associations
//			between before and now.
//		If sufficiently sparse, these memories can 
//			retrieve missing parts of a pattern.
//				thus it truly hallucinates its prediction.
//					and these methods used to think ahead
//	This is a local task in a small domain,
//
//	DEMO: link the associative memories of many may small mdels.
//
//	DEMO: Explore using N-Arm Bandit counters to prune startup runts.
//		(First exploration of optimization.)
//
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark * S3-ring
id abc_ij_bundle   = @[@[@"a", @"b", @"c", spin_1], @[@"i", @"j", spin_1]];
id abc_ijkl_bundle = @[@[@"a", @"b", @"c", spin_1], @[@"i", @"j", @"k", @"l", spin_1]];
id seq3aux = @[		// An interesting test case:
   //  evi, aux -- afterward --> evi, aux
  // go forward:
	@[@"a", @"i"],				// b   ? Study transitions
	@[@"b", @"i"],				// c   ?
	@[@"c", @"i"],				// a   ?
	@[@"a", @"i"],				// b   ?
	@[@"b", @"i"],				// c   ?
	@[@"c", @"i"],				// a   ?
  // go backward:
	@[@"a", @"j"],				// c   ?
	@[@"c", @"j"],				// b   ?
	@[@"b", @"j"],				// a   ?
	@[@"a", @"j"],				// c   ?
	@[@"c", @"j"],				// b   ?
	@[@"b", @"j"],				// a   ?
	@"again",
 ];


//id fb_abc_bundle = @[@[@"name:", @"fwd:@1", spin_1]];
//id fb_abc_bundle = @[@[@"name:", @"fwd:@1", @"bkw:@1", spin_1],
//					 @[@"name:", @"a", @"b", @"c",     spin_1]];

id fb_abc_bundle = @[@"fwd:@1", @"bkw:@1", @"a", @"b", @"c", ];
id f_bundle = @[@"fwd:@1"];

R(test, (@{CameraHsuz(0, 45, 10, 1.5), VEL(-2),						@"parts":@[

	aBundle(@{/*n(bun", */@"structure":fb_abc_bundle,
			  @"proto":@[aFlipPrevLeaf(@{@"mode":@"simMode2", @"bias":@0, @"M":@"fwd^", @"N":@"bkw^"}, @{spin$0}),
						 aGenBcastLeaf()]
	}),
																		]}	))
	
	
R(1-bit, (@{CameraHsuz(0, 45, 10, 1.0), VEL(-6),					@"parts":@[

	aGenerator(@{@"nib":@"LoGen_fwdBkw", flip, @"P":@"bun", @"resetTo":@[@"a"]}	),
	aTunnel(@{n(bun), @"structure":f_bundle, @"proto":@[@0, aGenBcastLeaf()]
	}),
	aMirror(@"fwd"),
//	anActor(@{n(act1", @"minHeight":@(minHeight),
//		@"evi":aBundle(@{@"structure":f_bundle, @"proto":aPrevBcastLeaf(@{spin$1}, @{}), spin$0 }),
////		@"con":aTunnel(@{@"structure":z_bundle, @"proto":aGenMaxsqLeaf(0,@{@"U":@"write"}), @"XXpositionPriorityXz":@2, flip }),
//	}),
//
//	aLink(@"bun/fwd",    @"act1/evi/fwd/prev.P"),

																		]}	));

id test4810 = @[@"fwd", @"fwd", @"fwd", ];
//	s41    Sp<Ham:     a-, b+,             fwd+,       
//	s42    Sp<Ham:             b-, c+,     fwd+, fwd-,       
//	s43    Sp<Ham: a+,                 c-, fwd+, fwd-,       
id test4820 = @[@"fwd", @"fwd", @"fwd", @"fwd", ];
//	s41    Sp<Ham:     a-, b+,             fwd+,       
//	s41P   Sp<Bro:                                             s44.2, z,       
//	s42    Sp<Ham:             b-, c+,     fwd+, fwd-,       
//	s43    Sp<Ham: a+,                 c-, fwd+, fwd-,       
//	s44    Sp<Ham:     a-, b+,                   fwd-,
///	s44    Sp<Ham:                               fwd-, s41P.2,
id test4830 = @[@"fwd", @"fwd", @"fwd", @"fwd", @"fwd", ]; // same as 4820
id test4835 = @[@"fwd", @"fwd", @"fwd",  @"again",];	   // same as 4820

id test4840 = @[@[], @"fwd", @[], @"fwd", @[], @"fwd", ];
//	s41    Sp<Ham: a+, a-,     
//	s42    Sp<Ham:     a-, b+,             fwd+,       
//	s43    Sp<Ham:         b+, b-,               fwd-,       
//	s44    Sp<Ham:             b-, c+,     fwd+,       
//	s45    Sp<Ham:                 c+, c-,       fwd-,       
//	s46    Sp<Ham: a+,                 c-, fwd+,       
id test4842 = @[@[], @"fwd", @[], @"fwd", @[], @"fwd", @[], @"fwd", ];
//	s41    Sp<Ham: a+, a-,     
///	s41P   Sp<Bro:                                             s47.2, z,
//	s42    Sp<Ham:     a-, b+,             fwd+,       
//	s43    Sp<Ham:         b+, b-,               fwd-,       
//	s44    Sp<Ham:             b-, c+,     fwd+,       
//	s45    Sp<Ham:                 c+, c-,       fwd-,       
//	s46    Sp<Ham: a+,                 c-, fwd+,       
//	s47    Sp<Ham: a+, a-,                       fwd-,
///	s47    Sp<Ham:                               fwd-, s41P.2,     

 // Note:
//	s41    Sp<Ham: a+, a-,     
//	s47    Sp<Ham: a+, a-,                       fwd-,

id test4850 = @[@[], @[], @"fwd", @[], @[], @"fwd", @[], @[], @"fwd", @[], @[], @"fwd", ];
//	s41    Sp<Ham: a+, a-,     
//	s41P   Sp<Bro:                                             s49.2, z,       
//	s42    Sp<Ham:     a-, b+,             fwd+,       
//	s43    Sp<Ham:         b+, b-,               fwd-,       
//	s44    Sp<Ham:         b+, b-,								NEW
//	s45    Sp<Ham:             b-, c+,     fwd+,       
//	s46    Sp<Ham:                 c+, c-,       fwd-,       
//	s47    Sp<Ham:                 c+, c-,       				NEW
//	s48    Sp<Ham: a+,                 c-, fwd+,       
//	s49    Sp<Ham:                               fwd-, s41P.2,     


id test4880 = @[@"fwd", @"fwd", @"fwd",
			   @[], @"fwd", @[], @"fwd", @[], @"fwd",
  ];
//	s44    Sp<Ham: a+, a-,                       fwd-,
//	s41    Sp<Ham:     a-, b+,             fwd+,
//	s45    Sp<Ham:         b+, b-,               fwd-,       
//	s42    Sp<Ham:             b-, c+,     fwd+, fwd-,	xxx
//	s46    Sp<Ham:             b-, c+,     fwd+,       
//	s47    Sp<Ham:                 c+, c-,       fwd-,
//	s43    Sp<Ham: a+,                 c-, fwd+, fwd-,	xxx
//	s48    Sp<Ham: a+,                 c-, fwd+,       

id test4882 = @[@"fwd", @"fwd", @"fwd",
			   @[], @"fwd", @[], @"fwd", @[], @"fwd",
			   @[], @[], @"fwd", @[], @[], @"fwd", @[], @[], @"fwd",
			   @[], @[], @[], @"fwd", @[], @[], @[], @"fwd", @[], @[], @[], @"fwd",
  ];
//	s44    Sp<Ham: a+, a-,                       fwd-,   \ XXX
//	s49    Sp<Ham: a+, a-,     
//	???    Sp<Ham:     a-, b+,             fwd+, fwd-,	 \ XXX (MISSING)
//	s41    Sp<Ham:     a-, b+,             fwd+,
//	s45    Sp<Ham:         b+, b-,               fwd-,   \ XXX
//	s50    Sp<Ham:         b+, b-,
//	s42    Sp<Ham:             b-, c+,     fwd+, fwd-,	 \ XXX
//	s46    Sp<Ham:             b-, c+,     fwd+,
//	s47    Sp<Ham:                 c+, c-,       fwd-,   \ XXX
//	s51    Sp<Ham:                 c+, c-,
//	s43    Sp<Ham: a+,                 c-, fwd+, fwd-,   \ XXX
//	s48    Sp<Ham: a+,                 c-, fwd+,

id test4890 = @[@[], @[], @"fwd", @[], @[],
			    @[], @[], @"fwd", @[], @[],
			    @[], @[], @"fwd", @[], @[],
  ];

id test4882c = @[@"c", @[]];


 // 170807 STUDY THIS.
//Terms prefixed with '#' are spurious terms
//
// /// STATIONARY; Why are fwd+ and bkw+ not stationary
//s48    Sp<Ham: a+, a-,                      #fwd-,       
//s49    Sp<Ham: a+, a-,     
//s59    Sp<Ham: "   "          #bkw-,			s49P
//s42    Sp<Ham:         b+, b-,              #fwd-,       
//s43    Sp<Ham:         b+, b-,   
//s54    Sp<Ham:         "+, "-,#bkw-,			s43P
//s45    Sp<Ham:                       c+, c-,#fwd-,       
//s46    Sp<Ham:                       c+, c-, 
//s51    Sp<Ham:                #bkw-, "+, "-,		s46P
//
// /// All terms shown here are essential. 		No # terms
//s41    Sp<Ham:     a-, b+,                   fwd+,       
//s44    Sp<Ham:             b-,       c+,     fwd+,       
//s47    Sp<Ham: a+,                       c-, fwd+,       
//s56    Sp<Ham: "+             #bkw-,     "-  "wd+	s47P
//
// /// Most terms shown here are essential. 
//s50    Sp<Ham:     a-,         bkw+, c+,     
//s53    Sp<Ham:         b+,     bkw+,     c-,       
//s58    Sp<Ham: a+,         b-, bkw+,   
//s61    Sp<Ham: "+          "-  "kw+         #fwd-,	s58P
//
//s56    Sp<Ham:                 bkw-,     "- #"wd+	s47P
//
//s56    Sp<Ham:                 bkw-,     "- #"wd+	s47P
//s56    Sp<Ham:                 bkw-,     "- #"wd+	s47P
M(test48**, (@{CameraHsuz(0, 135, 30, 1.5),  VEL(-2),				@"parts":@[

//	aGenerator(0, @{@"nib":@"LoGen_fwdBkw",@"events":@[@"fwd=rnd 0.05",@"bkw=rnd 0.05",@"again"],flip, @"P":@"bun", @"resetTo":@[@"a"]}	),
	aGenerator(0, @{@"events":@[@"fwd=rnd 0.05",@"bkw=rnd 0.05",@"again"],flip, @"P":@"bun", @"resetTo":@[@"a"]}	),
//	aGenerator(1,@{@"events":@[@"c", @[]],                                flip, @"P":@"bun", @"resetTo":@[@"a"]}	),
//	aGenerator(0,@{@"prob":@0.1,										  flip, @"P":@"bun", @"resetTo":@[@"a"]}	),
//	aGenerator(-1,@{@"events":test4882,									  flip, @"P":@"bun", @"resetTo":@[@"a"]}	),
//	aGenerator(@{@"nib":@"LoGen_fwdBkw",                               flip, @"P":@"bun", @"resetTo":@[@"a"]}	),
	aTunnel(@{n(bun), @"structure":fb_abc_bundle,
			  @"proto":@[aFlipPrevLeaf(@{@"mode":@"simMode2", @"bias":@0, @"M":@"fwd^", @"N":@"bkw^"}, @{spin$0}),
						 aGenBcastLeaf()],	// control;  ^--generator
	}),
	aLink(@"a/prev.S^", @"c/prev.T^"),
	aLink(@"b/prev.S^", @"a/prev.T^"),
	aLink(@"c/prev.S^", @"b/prev.T^"),

	anActor(@{n(act1), @"minHeight":@(minHeight),
		@"evi":aBundle(@{@"structure":fb_abc_bundle, @"proto":aPrevBcastLeaf(@{spin$1}, @{@"U":@"write"}), spin$0 }),
		@"con":aBundle(@{@"structure":z_bundle, @"proto":aGenMaxsqLeaf(@{@"value":@1.0}, @{@"U":@"write"}), @"XXpositionPriorityXz":@2, flip }),
	}),

	aLink(@"bun/fwd",    @"act1/evi/fwd/prev.P"),
	aLink(@"bun/bkw",    @"act1/evi/bkw/prev.P"),
	aLink(@"bun/a/prev", @"act1/evi/a/prev.P"),
	aLink(@"bun/b/prev", @"act1/evi/b/prev.P"),	// Wire gen to act1
	aLink(@"bun/c/prev", @"act1/evi/c/prev.P"),

	aWriteHead(@{@"actor":@"act1", @"babyPrototype":@"Hamming"}),
																		]}	));

 // bug in gen
R(test48**, (@{CameraHsuz(0, 45, 10, 1.0),  VEL(-2),				@"parts":@[

	aGenerator(1,@{@"events":@[@"a", @[], @"again"], flip, @"P":@"bun", @"resetTo":@[@"a"]}	),
	aTunnel(@{n(bun), @"structure":a_bundle,
			  @"proto":@[aGenBcastLeaf()],	// control;  ^--generator
	}),
//	aLink(@"a/prev.S^", @"a/prev.T^"),
	anActor(@{n(act1), @"minHeight":@(minHeight),
		@"evi":aBundle(@{@"structure":a_bundle, @"proto":aPrevBcastLeaf(@{spin$1}, @{}), spin$0 }),
	}),

	aLink(@"bun/a/main", @"act1/evi/a/prev.P"),
																		]}	));



 // CUTE
M(S3-ring rand, (@{CameraHsuz(0, 45, 10, 1.5),  VEL(0),				@"parts":@[
	aGenerator(50, @{@"events":seq3aux, flip, @"P":@"act1/evi"}),
	anActor(@{n(act1), @"minHeight":@(minHeight),
		@"evi":aTunnel(@{@"structure":abc_ij_bundle, @"proto":aGenPrevBcastLeaf(0,@{spin$1}, @{@"U":@"write"}), spin$3 }),
		@"con":aBundle(@{@"structure":z_bundle, @"proto":aGenMaxsqLeaf(@{@"value":@1.0},@{@"U":@"write"}), @"positionPriorityXz":@2, flip }),
	}),
	aWriteHead(@{@"actor":@"act1", @"babyPrototype":@"Hamming"}),
																		]}	));

//B. An associative memory is built as an Actor,
//	which in our example would have 
//		evidence bundle = {counter[3], counter-[3], aux[4], aux-[4]},
//			(where the names ending in - were the value the time before.)
//	Internal to the Actor are networks of Hamming units,
//		the value of each representing its Hamming distance,
//			the number of unsatisfied prerequisites.
//	The Hamming with the smallest output is closest, and is chosen.
//	The experience can be included in the associative memory
//		by adding a new Hamming element to the Actor.
//			whose elements include the unsatisfied bits of evidence.
//
//C. Equivalence Groupings (TBD, Ignored for now)
//
//D. Context
//	The context Bundle of an Actor 
//		produces a valuable summary representation
//			of the evidence below.
//		It determines the value of the Actor in an Evolutionary computing environemt.
//			leist it get reaped. (later)
//	From a context Bundle
//		upward flow is evidence to bosses that
//			this factal's fact is true in the world.
//		downward flow is desire for the world to be in this state.
//	In cases where context is driven by bosses,
//		learning is supervised.
//	In cases where established networks of Hammings choose a context
//		learning is supervised.
//	Other than that, there are some hints:
//		Contexts should be more persistent than their evidence.
//		Over-sampling with allowance to shift edges. (???)
//
//E. Q-Learning (design numerics)
//	Actors support a Q-Learning context per 
//		per current context
//		per desired context and 
//		per action.

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark - Mazlovian Multiplexor
/************************/ setParentMenu(@"Mazlovian Multiplexor"); /***************/
id (^modProtos)() = ^id () { return @[
		anOrModBcastLeaf(0,	@{@"T":@"ena^"}),	// highly repeated data elements
		aRotBcastLeaf(		@{},0,@{flip})		// common control elements
									 ];				};
id (^modProtos0)() = ^id () { return @[
		aModLeaf(			@{@"T":@"ena^",flip},@{spin$2}),
	0?	aGenBcastLeaf(0,	@{@"latitude":@2})		:
		aBcastLeaf(			@{@"latitude":@2}),
									 ];				};

R(3xMaxBcast+port, (@{CameraHsuz(0, 20, 10, 0.8),	@"b oundsDrapes":@0.05, partsStackZ, @"parts":@[
//	aGenerator(-1, @{@"prob":@0.5, flip, @"P":@"act1/evi"}),	// evi has no bundleTaps
	anActor(@{n(act1), @"minHeight":@15, @"viewAsAtom":@1, @"displayInvisibleLinks":@0,
		@"con":aTunnel(@{@"structure":xyz_bundle, flip}),
		@"parts":@[],
		@"evi":aTunnel(@{@"structure":@[@[spin_R, @"ena:@1", @"a", @"b", @"c"],
									    @[spin_R, @"ena:@1", @"a", @"b", @"c"], spin_L, ],
						 @"proto":modProtos() }),
			}),
																		]}	));

 // 170107 modulator bundle test
R(Basic Modulator Tunnel,  (@{CameraHsuz(0, 15, 10, 1),	@" boundsDrapes":@0.05, @"fontNumber":@"4", @"parts":@[
	aNet(@{partsStackY},	@[
o(bun,	aBundle(@{@"structure":@[@[spin_R, @"ena:@1", @"a", @"b", @"c"],
								 @[spin_R, @"ena:@1", @"a", @"b", @"c"], spin_L, ],
				  @"proto":modProtos() })),
							 ]),
																		]}	));
 // 170712 chasing deallocation bug
R(deallocation bug,  (@{CameraHsuz(0, 15, 10, 1),				@"parts":@[
				 o(rot,		aBroadcast()),
				 o(main,	aBroadcast(@"rot=")),
			]
									 }) );

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark - Supertux
/************************/ setParentMenu(@"Supertux"); /***************/
////////////////////////////////////////////////
/////				Stux				   /////
////////////////////////////////////////////////

id y3x3_stux_bundle = @{spin$R,
				@"joystick"	:@[spin_R, @"left", @"right", @"jump"],
				@"mana"		:@[spin_R, @"health", @"hunger"],
				@"retina"	:y3x3_bundle};
id y5x5_stux_bundle = @{spin$R,
				@"joystick"	:@[spin_R, @"left", @"right", @"jump"],
				@"mana"		:@[spin_R, @"health", @"hunger"],
				@"retina"	:y5x5_bundle};


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark * with fixed array
M(Stux fixed, (@{CameraHsuz(0, 90, 45, 1), VEL(-1),					@"parts":@[
			aSupertux(@"drop-jump", @{@"stuxEndTime":@"-10.0", flip, @"P":@"bun"}),
//			aSupertux(0, @"drop-jump", @{@"stuxEndTime":@"-10.0", flip, @"P":@"bun"}),
	o(bun,	aTunnel(@{@"structure":y5x5_stux_bundle, @"proto":aGenBcastLeaf() })),
	o(qa,	aHamming(0, @[@"y1x0"],					@{flip} )),
	o(qb,	aHamming(0, @[@"y1x0", @"y1x1"],			@{flip} )),
	o(qc,	aHamming(0, @[@"y1x0", @"y1x1", @"y1x2"],	@{flip} )),
	o(qd,	aHamming(0, @[@"y1x0", @"y1x1", @"y1x2", @"y1x3"],	@{flip} )),
	o(qe,	aHamming(0, @[@"y1x0", @"y1x1", @"y1x2", @"y1x3", @"y1x4"],@{flip} )),
	o(qf,	aHamming(0, @[         @"y1x1", @"y1x2", @"y1x3", @"y1x4"],@{flip} )),
	o(qg,	aHamming(0, @[                  @"y1x2", @"y1x3", @"y1x4"],@{flip} )),
	o(qh,	aHamming(0, @[                           @"y1x3", @"y1x4"],@{flip} )),
	o(qi,	aHamming(0, @[                                    @"y1x4"],@{flip} )),

	o(qj,	aHamming(0, @[@"y2x0", @"y2x1"],			@{flip} )),
	o(qk,	aHamming(0, @[@"y3x0"],					@{flip} )),
	o(ql,	aHamming(0, @[@"y4x0"],					@{flip} )),

	o(q0,	aMaxOr(0, @[@"qa",@"qb",@"qc",@"qd",@"qe",@"qf",
						@"qg",@"qh",@"qi",@"qj",@"qk",@"ql"],@{flip} )),
	o(mir,	aMirror(@"q0")),
																		]}	));

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma mark * with assimilator
M(Stux+Assim, (@{CameraHsuz(0, 90, 10, 1), VEL(-1),					@"parts":@[
	aSupertux(-1, @"drop-jump", @{@"stuxEndTime":@"-300", flip, @"P":@"act1/evi"}),
	anActor(@{n(act1), @"minHeight":@(minHeight), @" displayInvisibleLinks":@1,
		@"evi":aTunnel(@{@"structure":y5x5_stux_bundle, @"proto":aGenPrevBcastLeaf(0,0,@{@"U":@"write"})}),
		@"con":aBundle(@{@"structure":@[@"z"], @"proto":aGenMaxsqLeaf(@{@"value":@1.0},@{@"U":@"write"}), flip}),
	}),
	aWriteHead(@{@"actor":@"act1", @"babyPrototype":@"Hamming"}),
																		]}	));

////////////////// END OF CODE /////////////////
M(LastMachine, ( @{} ));			// Markers for ^m tests
R(LastMachine, ( @{} ));

	uBuilt_metaInfo[@"nAllItems"]		= [NSNumber numberWithInt:uAt_allNo];
	uBuilt_metaInfo[@"nMenuItems"]		= [NSNumber numberWithInt:uAt_menuNo];
	uBuilt_metaInfo[@"nRegressItems"]	= [NSNumber numberWithInt:uAt_regressNo];
	return uBuilt_brain; //loaded with tesst definitions by the above macros
}

#undef r
#undef xr
#undef m
#undef xm
#undef R
#undef xRs
#undef M
#undef xM

