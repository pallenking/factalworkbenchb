// Splitter.h -- splits one Port into many, in various ways C2011PAK
/*
    Maintains 1-port of information about the world.
        Unf_lipped, it distributes a single lower input to many upper outputs.
        f_lipped, it combines many lower inputs to produce a single upper output.
	A Factal's numerical algorithms are chosen at birth and do not vary during simulation
    Atom Atom 0 is not used. Rather, Share's send numerics up through the multi-cast.
 */

/*		   U1 	   	   U2 	       U3 			   Broadcast   Unknown
		.--o--.		.--o--.		.--o--.			.--o--.		.--o--.
	+---|L|   |-----|L|   |-----|L|   |---------|L|   |-----|L|   |-----+
	|    ^	  v      ^	  v      ^	  v    		 ^	  v    	 ^	  v		|
	|											 |	 nil	 |	 nil	|
	|	^^^  vvv								 |			 |			|
	|	 | 	COMBINE								 |			 |			|
	| SWITCH  |a1					  a1 = B ----'			 |			|
	|    |	  |-----------		 uP - a1 = U ----------------'			|
	|vGet|	  |valueTake												|
	+----|   |L|--------------------------------------------------------+
		 '--o--'
			P (Primary)													*/

#import "Atom.h"
@class Share;

/////////////////////////////////

@interface Splitter: Atom
/*									0	 1	  2	   3
	Initial parts (Splitter):		P,
				  (Splitter):		P,   E,
				  (Branch):			P:d, S:d, L: , M: ,
	(None are Shares)
 */
		//!						a1
		//!	 Broadcast		Sum(inI)
		//!	 MaxOr:			Max(inI, 0.01)
		//!	 MinAnd:		Min(inI)
		//!	 Bayes			Sum(inI)
		//!	 Hamming:		Sum(inI-cut)+cut		cut is a global constant
		//!	 Multiply:		Prod(inI)
		//!	 KNorm			(Sum(inI^^k))^^(1/k)	k is a global constant
		//!	 Sequence:		  - n.a. -

		//!<< combinePre >>: OPERATION
		//! DEFAULT:	a1=0.1
		//!	 MaxOr:		a1=-inf
		//!	 MinAnd:	a1=+inf
		//!	 Bayes:		a1=0.1
		//!	 Multiply:	a1=1.0
		//!	 Sequence:   ?

				//!<< combineNext >>: OPERATION		  RETURNS:
				//!	DEFAULT:	a1   += con			false
				//!	 Broadcast	a1 max= con			false
				//!	 MaxOr:		a1 max= con			==highest if con>0.01
				//!	 MinAnd:	a1 min= con			==lowest
				//!	 Hamming:	a1   += con-cut		false
				//!	 Multiply:	a1   *= con			==lowest
				//!	 KNorm		a1   += con**cp		false
				//!	 Sequence:	  ---				false

		//!<< combinePost >>: OPERATION
		//! DEFAULT:			<nothing>
		//!	 Hamming:		a1   += cut
		//!	 KNorm			a1	  = a1**(1/cp)

				//!<< bidSum >>: OPERATION
				//! DEFAULT:	1.0	(was val)
				//!	 Broadcast:	1.0
				//!	 Hamming:	1.0

		//// operation
    @property (nonatomic, assign) float		a1;				// accumulator
    @property (nonatomic, assign) int		combineWinner;	// Winning Share number
												// >S	Winning Share
												//  0	no winner yet declared
												// -1	proportional sharing
	 //// Configuration					// kind is from Port, chooses 0 or 1:
	@property (nonatomic, assign) Class		 shareClass0, shareClass1; // modulated by "M":
	@property (nonatomic, assign) Class		 shareClass;	// Current
    @property (nonatomic, assign) char		  isABcast;		// for auto-Broadcast operation
    @property (nonatomic, assign) char		  onlyPosativeWinners;// inputs <0 cannot win
    @property (nonatomic, assign) char		  noPosition;

	 //// Operational State:
    @property (nonatomic, assign) char		upIsDirty;	// must recompute upward, no matter what

#pragma mark 1. Basic Object Level

#pragma mark  4a Factory Access						// Methods defined by Factory
atomsPortDefinitions(uPort,		U,		false)
atomsPortDefinitions(bPort,		B,		false)
atomsPortDefinitions(kindPort,	KIND,	true)

#pragma mark 4. Factory
- (Share *)			anotherShare;



#pragma mark 15. PrettyPrint
- (NSString *) printContentsGather:(bool)gather names:(NSMutableArray *)allNames;
	
@end
