// Info.mm -- a wrapper for Dictionaries, arrays, or strings C2014 PAK

 // using class as a namespace -- no Info

#import "Id+Info.h"
#import "SystemTypes.h"
#import "Common.h"
#import "NSCommon.h"
#import "Part.h"
#import "Brain.h"
#import "HnwObject.h"
#import "Model01.h"

#include "XCodes.h"		// define XX and other X code markings


#pragma mark - id + info
  ////////////////////////////////////////////////////////////////////////
 ////////////////////////////  CATEGORY ID  /////////////////////////////
////////////////////////////////////////////////////////////////////////
@implementation NSObject /*extended here to*/ (info4id)

static NSDictionary *infoTypeName2NsTypeName = @{
	@"HashX":@"NSDictionary",
	@"ArrayX":@"NSArray",
	@"StringX":@"NSString",
	@"NumberX":@"NSNumber",
};
																  
//- (NSString *) infoClassName; {								 ///// ID /////
//	for (id typeRName in infoTypeName2NsTypeName) {				
//		id nsTypeName = infoTypeName2NsTypeName[typeRName];
//		if ([self isKindOfClass:NSClassFromString(nsTypeName)])
//			return typeRName;
//	}
//	return nil;
//}

- (bool) isInfoType; {
	for (id typeRName in infoTypeName2NsTypeName)
		if ([self isEqualTo:typeRName])
			return TRUE;
	return FALSE;
}

- (NSDictionary *)	asHash;		{	return coerceTo(NSDictionary,	self);	}
- (NSArray		*)	asArray;	{	return coerceTo(NSArray,		self);	}
- (NSString 	*)	asString;	{	return coerceTo(NSString,		self);	}
- (NSNumber 	*)	asNumber;	{	return coerceTo(NSNumber,		self);	}
- (Part 		*)	asPart;		{	return coerceTo(Part,			self);	}
- (bool) 			isHash;		{	return coerceTo(NSDictionary,	self);	}
- (bool) 			isArray;	{	return coerceTo(NSArray,		self);	}
- (bool) 			isString;	{	return coerceTo(NSString,		self);	}
- (bool) 			isNumber;	{	return coerceTo(NSNumber,		self);	}
- (bool) 			isPart;		{	return coerceTo(Part,			self);	}

- (bool) isMutable; {											///// ID /////
	if (coerceTo(NSMutableDictionary, self))					
		return TRUE;
	if (coerceTo(NSMutableArray, self))
		return TRUE;
	if (coerceTo(NSString, self))
		return TRUE;
	return FALSE;
}

- (bool) isFalseId;			{		return false;		}
- (bool) isTrueId;			{		return false;		}		///// ID /////

// http://useyourloaf.com/blog/2010/04/13/cocoa-naming-conventions-for-memory-allocation.html
/* You take ownership of an object if you create it using a method whose name
   begins with “alloc” or “new” or contains “copy” (for example, alloc, 
   newObject, or mutableCopy), or if you send it a retain message. You are 
   responsible for relinquishing ownership of objects you own using release or 
   autorelease. Any other time you receive an object, you must not release it.
*/
- (id) anotherMutable; {
	return [[self mutableCopy] autorelease];
}


- (bool) isBlock {												///// ID /////
	 // This would be too easy:
	//		return [self isKindOfClass:[NSBlock class]];
	// Instead create an id which is a block:
	if (!blockClass) {
		id block = ^{};
		blockClass = [block class];
		 // find first subclass of NSObject
		while ([blockClass superclass] != [NSObject class])
			blockClass = [blockClass superclass];
	}
    return [self isKindOfClass:blockClass];
}
static Class blockClass = nil;

#pragma mark InfoBasics

 // DEFAULT METHODS
- (id) deepMutableCopy;{panic(@"prototype? never executed!");return self;}

#pragma mark 15. PrettyPrint
- (NSString *) pp; {											///// ID /////
	id aux = @{}.anotherMutable;

XX	id rv = [self pp:aux];

	return rv;
}
- (const char *) ppC; {		return [self pp].UTF8String;	}	///// ID /////

- (NSString *) pp:aux {											///// ID /////
	if ([self isBlock])
		return @"BLOCK";
	panic(@"pp method needs overriding");
	return nil;
	//	return (id)panic(@"pp method needs overriding");
}

- (NSString *) uidStr; {										///// ID /////
	long long uid = (long long)self;							
	return [@"" addF:@"%04llX", (uid >> 4) & 0xFFFF];
}

void pInfo(id info)		{	NSPrintf(@"   %@#\n", [info pp]);}	// linkage to debugger

@end

#pragma mark - Hash + info
  ////////////////////////////////////////////////////////////////////////
 ///////////////////////////  CATEGORY HASH  ////////////////////////////
////////////////////////////////////////////////////////////////////////
@implementation NSDictionary /*extended here to*/ (info4NSDictionary)	// a Category

#pragma mark InfoBasics
- (id) deepMutableCopy; {										//// HASH ////
	id rv = @{}.anotherMutable;
	for (id selfKey in self) {	// do all keys and values:
XR		id selfKeyDeep = [selfKey deepMutableCopy];
		id selfVal = self[selfKey];
XR		id selfValDeep = [selfVal deepMutableCopy];
		
		rv[selfKeyDeep] = selfValDeep;	// new deeply mutable copy
	}
	return rv;
}

#pragma mark 15. PrettyPrint
- (NSString *) pp {	return [self pp:0];}
- (NSString *) pp:aux {											//// HASH ////
	Brain *brain = Brain.currentBrain;

	if (!aux)				// supply if missing
		aux = @{}.anotherMutable;
	if ([aux asString])		// sugar: stringX is indent
		aux = @{@"indent":aux}.anotherMutable;
	id noBrackets = aux[@"noBrackets"];
	id changable = !brain.ppTransShowMutateable? @"":	[self isMutable]? @"+": @"";
	NSString *rv = !noBrackets? [@"" addF:@"{%@", changable]: @"";

	id sortedKeys = self.allKeys;
	NSString *pairSeparator = @" ";
	for (id key in sortedKeys) {
	
		id value = self[key], keyStr=key, valueStr=0;
XX		keyStr = [key pp:aux];
XX		valueStr = [value pp:aux];
		rv=[rv addF:@"%@%@:%@", pairSeparator, keyStr, valueStr];
		pairSeparator = @", ";
	}
	if (!noBrackets)
		rv=[rv add:@"}"];

	return rv;
}

@end

#pragma mark - Array + info
  ////////////////////////////////////////////////////////////////////////
 //////////////////////////////// ARRAY /////////////////////////////////
////////////////////////////////////////////////////////////////////////
@implementation NSArray /*extended here to*/ (info4NSArray)	// a Category

#pragma mark InfoBasics

- (id) deepMutableCopy; {										//// ARRAY ///
	id rv = @[].anotherMutable;
	for (id elt in self)		// do all elements
		[rv addObject:[elt deepMutableCopy]];
	return rv;				// new deeply mutable copy
}

+ (NSArray *) arrayCatenation:(NSArray *)elements; {
	NSArray *rv = @[];
	for (NSArray *element in elements)
		rv = [rv arrayByAddingObjectsFromArray:mustBe(NSArray, element)];
	return rv;
}

 // catenate array0,... into array0
NSArray *cat(NSArray *array0, ...) {
//    va_list ap;
//    va_start(ap, array0);
//	id rv = array0.anotherMutable;
//	while (1) {
//		id elt = va_arg(ap, id);
//		elt = coerceTo(NSArray, elt);
//		if (elt)
//			rv = [rv arrayByAddingObjectsFromArray:elt];
//		else
//			break;
//	}
//	va_end(ap);
//	return rv;

 // WORKS:
    va_list ap;
    va_start(ap, array0);
	id rv = array0.anotherMutable;
	if (rv)
		while (1) {
			id elt = va_arg(ap, id);
			elt = coerceTo(NSArray, elt);
			if (elt)
				rv = [rv arrayByAddingObjectsFromArray:elt];
			else
				break;
		}
	va_end(ap);
	return rv;
}

#pragma mark 15. PrettyPrint
- (NSString *) pp {	return [self pp:0];}	
- (NSString *) pp:aux {											//// ARRAY ///
	Brain *brain = Brain.currentBrain;

	if (!aux or [aux asString])		// sugar:
		aux = [(aux? @{@"indent":aux}:@{}) anotherMutable];
	
	id noBrackets = aux[@"noBrackets"];
	id changable = !brain.ppTransShowMutateable? @"": [self isMutable]? @"+": @"";
	NSString *rv=!noBrackets? [@"" addF:@"[%@", changable]: @"";

	NSString *elementSeparator = @"";
	for (id elt in self) {
		id eltStr;
		if (HnwObject *m = coerceTo(HnwObject, elt))
XX			eltStr = [@"" addF:@"'%@'", m.name];
		else
XX			eltStr = [elt pp:aux];		// #### RECURSION
		rv=[rv addF:@"%@ %@", elementSeparator, eltStr];
		elementSeparator = @",";
	}
	if (!noBrackets)
		rv=[rv add:@"]"];

	return rv;
}
@end

@implementation NSMutableArray /*extended here to*/ (info4NSMutableArray)	// a Category

- (id) addObjectIfAbsent:(id)anObject; {
	if ([self indexOfObject:anObject] != NSNotFound)
		return nil;
XX	[self addObject:anObject];
	return anObject;
}

- (void) setObjectAt:(int)index toValue:obj; {
	while (self.count <= index)				// extend self to have an object[index]
		[self addObject:@0];
	[self replaceObjectAtIndex:index withObject:obj];
}

- (id) dequeFromHead; {
	id rv = nil;
	if (self.count) {
		rv = [[self[0] retain] autorelease];
		 // N.B. 170718PAK: Since this may be the last owner of rv
		  //  retain and autorelease!
		[self removeObjectAtIndex:0];
	}
	return rv;
}
@end

  ////////////////////////////////////////////////////////////////////////
 ////////////////////////  CATEGORY STRING  /////////////////////////////
////////////////////////////////////////////////////////////////////////
#pragma mark - String + info
@implementation NSString /*extended here to*/ (info4NSString)// a Category of STRING //////

#pragma mark InfoBasics
- (const char *) C; {	return self.UTF8String;}

- (id) deepMutableCopy; {										/// STRING ///
	return self.anotherMutable;
}


- (NSString *) add:(id)isPpAble; {								/// STRING ///
	assert([isPpAble isString], (@""));
	return [self stringByAppendingString:isPpAble];
}																  
- (NSString *) addF:(NSString *)fmt,...;	{					/// STRING ///
	if (fmt == nil or [fmt isEqual:@0])
		return self;
	va_list ap;
	va_start(ap, fmt);
	id rv = [[[NSString alloc] initWithFormat:fmt arguments:ap] autorelease];
	va_end(ap);
	return [self stringByAppendingString:rv];
}																  
- (NSString *) addN:(int)n F:(NSString *)fmt,...;	{			/// STRING ///
	if (fmt == nil or [fmt isEqual:@0])
		return [@"" stringByPaddingToLength:n withString:@" " startingAtIndex:0];
	va_list ap;
	va_start(ap, fmt);
	id rv = [[[NSString alloc] initWithFormat:fmt arguments:ap] autorelease];
	va_end(ap);
	assert(n>=0, (@"illegal negative field lenght"));
	rv = [rv stringByPaddingToLength:n withString:@" " startingAtIndex:0];
	return [self stringByAppendingString:rv];
}
- (int) columnCur; {											/// STRING ///
	id lines = [self componentsSeparatedByString:@"\n"];
	assert([lines count]>=1, (@"no lines found"));
	id lastLine = [lines lastObject];
	return (int)[lastLine length];
}
- (NSString *) padToColumn:(int)col; {							/// STRING ///
	int colCur = [self columnCur];
	if (col > colCur)
		return [self stringByAppendingString:[@"" addN:col-colCur F:@""]];
	return self;
}
- (NSString *) capitalCammel; {
	id char0 = [self substringToIndex:1].uppercaseString;
	NSString *rv = [char0 addF:@"%@", [self substringFromIndex:1]];
	return rv;
}

#pragma mark 15. PrettyPrint
- (NSString *) pp {	return [self pp:0];}	
- (NSString *) pp:aux {											/// STRING ///
	Brain *brain = Brain.currentBrain;
	if (self.length == 0)					// nul string
		return @"\"\"";						// always put quotes around
	if (brain.ppUseQuotes)
		return [@"" addF:@"\"%@\"", self];	// with quotes
	return self;							// without quotes
}
int indentCol=5, postdent=3;
- (void) ppLog; {												/// STRING ///
	id aux = @{}.anotherMutable;
	[self ppLog:aux];
}							////////1\\\\\\\\.
- (void) ppLog:aux :fill; {										/// STRING ///
	if (aux == nil)
		aux = @{}.anotherMutable;
	aux[@"fill"] = fill;
	[self ppLog:aux];		////////1\\\\\\\\.
	[aux removeObjectForKey:@"fill"];
}																  
- (void) ppLog:aux; {											/// STRING ///
	Brain *brain = Brain.currentBrain;

	if (!aux or [aux isString])		// sugar:
		aux = [(aux? @{@"indent":aux}:@{}) anotherMutable];

	 // increase
	id indent = aux[@"indent"]?: @"";
	assert([indent isKindOfClass:[NSString class]], (@""));

	int wrapCol = brain.ppLineWidth - postdent;

	id padChar = aux[@"fill"]?:
			  [indent length]? [indent substringFromIndex:[indent length]-1]:
							   @" ";
	id first4columns = [@"" addF:@"%-4d%@", brain.logNumber, indent];
	for (id line in [self componentsSeparatedByString:@"\n"]) {

		 // 150926: blank lines are not numbered
		if ([line length] == 0) {
			printf("\n");
			continue;
		}
		line = [first4columns addF:@"%@", line];
		first4columns = indent;						// other lines are truncated

		 // strip off trailing spaces
		int length = (int)[line length], newLength=length;
		while (newLength>0 and [line characterAtIndex:newLength-1] == ' ')
			newLength--;
		line = [line substringWithRange:NSMakeRange(0, newLength)];

		 // break up into sub-lines
		while ([line length] > wrapCol) {			// line is too big for one line
			NSRange usedUp = {0, wrapCol-1};
			NSPrintf(@"%@\n", [line substringWithRange:usedUp]);
			 // get the next line:
			line = [line stringByReplacingCharactersInRange:usedUp withString:first4columns];
		}

		if (![padChar isEqualToString:@" "]) {
			line=[line addF:@" "];
			line=[line stringByPaddingToLength:wrapCol+postdent withString:padChar startingAtIndex:0];
		}
		NSPrintf(@"%@%s", line, aux[@"no-cr"]? "": "\n");
//		NSPrintf(@"%@\n", line);
	}

	 // break at langLog:
	logCheckBreaks(brain); ////////2\\\\\\\\.
}

@end

#pragma mark - Number + info
  ////////////////////////////////////////////////////////////////////////
 //////////////////////////  CATEGORY NUMBER  ///////////////////////////
////////////////////////////////////////////////////////////////////////
@implementation NSNumber /*extended here to*/ (info4NSNumber)

#pragma mark InfoBasics
																  
- (id) deepMutableCopy; {										/// NUMBER ///
	return self; //NSValue obj's always immutable				
}

- (bool) isFalseId;			{		return [self intValue] == 0;		}
- (bool) isTrueId;			{		return [self intValue] == 1;		}

#pragma mark 15. PrettyPrint
- (NSString *) pp {	return [self pp:0];}	
- (NSString *)pp:aux {											/// NUMBER ///
	return self.stringValue;									
}

@end

