// Leaf.h -- each functional port of a Bundle C2015PAK

#import "Bundle.h"

@interface Leaf : Bundle

	@property (nonatomic, retain) NSString 	*type;	// for printout/debug
	@property (nonatomic, retain) NSMutableDictionary *bindings;// map: external:internal
			//	""	Major output
			//	+	Major output, cur
			//	-	Major output, previous
			//	G	BundleTap connection point (a GenAtom.P)
			//	R	Set the state (esp of a Previouss)

	@property (nonatomic, retain) id		hook;

/* ?? Find Home
	Access Ports:
		P	Primary Port of Atom
		S	Secondary Port of Atom
		T	Terciary, TimeDel, ...
		M	Mode
		L	Latch
 */



- (id)				port4leafBinding:(id)name;

@end
