// Modulator.mm -- Multiplies upgoing and downgoing signals by control C2014PAK

/*
 */

#import "Modulator.h"
#import "Brain.h"
#import "Id+Info.h"
#import "FactalWorkbench.h"

#import "View.h"
#import "Common.h"
#import "NSCommon.h"
#import "GlCommon.h"
#import <GLUT/glut.h>

@implementation Modulator

//#pragma mark 1. Basic Object Level

#pragma mark  2. 1-level Access

#pragma mark  4a Factory Access						// Methods defined by Factory
- (id) atomsDefinedPorts;	{			id rv = [super atomsDefinedPorts];
//										// probably returns "P"
	rv[@"S"]		= @"pc ";
	rv[@"T"]		= @"p  ";
	return rv;
}
atomsPortAccessors(sPort,		S, false)
atomsPortAccessors(tPort,		T, false)
//- (Port *) terciary	{	return [self getPortNamed:@"T" down:false];		}

#pragma mark 4. Factory 

Modulator *aModulator(id pri, id etc) {
	id info = @{etcEtc}.anotherMutable;
	if (pri)
		info[@"P"] = pri;

	Modulator *newbie = anotherBasicPart([Modulator class]);
XX	newbie = [newbie build:info];
	return newbie;
}


#pragma mark 7-. Simulation Actions
- (void) reset; {								[super reset];
	
}

#pragma mark 8. Reenactment Simulator
// /*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*// //
// *//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//* //
// //*//*//*//*//*//*//*//*//    Reenactment Simulator   //*//*//*//*//*//*/ //
// /*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*// //
// *//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//* //

- (void) simulateDown:(bool)downLocal; {
	Port *pPort		= self.pPort;		Port *pPortIn	= self.pPortIn;
	Port *sPort		= self.sPort;		Port *sPortIn	= self.sPortIn;
	Port *tPort		= self.tPort;		Port *tPortIn	= self.tPortIn;

	if (!downLocal) {					//============: going UP
	
		if (pPortIn.valueChanged) {
			float   primaryValPrev	= pPortIn.valuePrev;
			float   primaryValNext	= pPortIn.valueGet;	//## P/SELF changes
			float secondaryVal		= sPortIn.valueGet;
			float terciarylVal		= tPortIn.valueGet;

			// 151007 control=0 times primaryValNext=infinity = 0, so not nan
			sPort.valueTake	= terciarylVal? primaryValNext * terciarylVal: 0.0;
			tPort.valueTake  = secondaryVal? primaryValNext * secondaryVal: 0.0;
			[self logEvent:@"  P=%.2fwas %.2f); now S=%.2f, T=%.2f", primaryValNext,
				primaryValPrev, sPort.value, tPort.value];
		}
	}
	else {								//============: going DOWN
		if (sPortIn.valueChanged) {
			float   primaryVal		= pPortIn.valueGet;
			float secondaryValPrev	= sPortIn.valuePrev;
			float secondaryValNext	= sPortIn.valueGet;	//## S/CUR changes
			float  terciaryVal		= tPortIn.valueGet;

			pPort.valueTake	= secondaryValNext * terciaryVal;
			tPort.valueTake	= secondaryValNext *  primaryVal;
			[self logEvent:@"  S=%.2f(was %.2f); now P=%.2f, T=%.2f",
					secondaryValNext, secondaryValPrev, pPort.value, tPort.value];
		}
		if (tPortIn.valueChanged) {
			float   primaryVal		= pPortIn.valueGet;
			float secondaryVal		= sPortIn.valueGet;
			float terciaryValPrev	= tPortIn.valuePrev;
			float terciaryValNext	= tPortIn.valueGet;	//## T/terciary/

			pPort.valueTake	= secondaryVal * terciaryValNext;
			sPort.valueTake	=   primaryVal * terciaryValNext;
			
			[self logEvent:@"  T=%.2f(was %.2f); now P=%.2f, S=%.2f",
					terciaryValNext, terciaryValPrev, pPort.value, sPort.value];

 // Previous version (above is untested)
//			float controlValPrev = controlInPort.valuePrev;
//			float controlValNext = controlInPort.valueGet;
//			id rv = [@"" addF:@" ctl.val=%.2fwas %.2f)", controlValNext, controlValPrev];
//
//			float priVal = pPortIn.valueGet;		//####### from PRIMARY port
//			float f = sPort.valueTake = priVal * controlValNext;
//			rv = [rv addF:@" pri(%.2f)->sec(%.2f) ", priVal, f];
//
//			float secVal = sPortIn.valueGet;		//####### from SECONDARY Port
//			f       = pPort.valueTake = secVal * controlValNext;
//			log  xx Event(downLocal, @"%@ sec(%.2f)->pri(%.2f) ", rv, secVal, f);
		}
	}
	[super simulateDown:downLocal];
}

#pragma mark 9. 3D Support
// \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\    //
//  \\ \\ \\ \\ \\ \\ \\ \\ \ 3D Support\\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\  //
//   \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\//
float modulatorRadius = 1.0;
float modulatorWidth  = 3.5;		// extra space for modulator
float modulatorHeight = 4.0;
float modulatorDeapth = 3.0;

- (Bounds3f) gapAround:(View *)v; {
		return Bounds3f(  -modulatorDeapth/2,              0.0, -modulatorDeapth/2,
			modulatorWidth-modulatorDeapth/2,  modulatorHeight,  modulatorDeapth/2);
}


- (Matrix4f) positionOfPort:(Port *)port inView:(View *)bitV; {

	if ([port.name isEqualToString:@"S"])		// SECONDARY
		return [port suggestedPositionOfSpot:
							 Vector3f(0, modulatorHeight, 0)];

	if ([port.name isEqualToString:@"T"]) {		// MODULATOR
		port.latitude = 4;
		return [port suggestedPositionOfSpot:
							 bitV.superView.bounds.centerXRight()];
	}

XR	return [super positionOfPort:port inView:bitV];
}

#pragma mark 11. 3D Display
//// // // // // // // // // // // // // // // // // // // // // // // ///
/// // // // // // // // // // 3D Display  // // // // // // // // // ////
// // // // // // // // // // // // // // // // // // // // // // // // //
- (void) drawFullView:(View *)v context:(RenderingContext *)rc; {
	glPushMatrix();
		glTranslatef(self.brain.displayOffsetX, v.bounds.center().y, self.brain.displayOffsetZ);
		rc.color = colorPurple4w;
		glutSolidSphere(1.5, 16, 16);			// mark attachment  spot
	glPopMatrix();

	[super drawFullView:v context:rc];				// PRIMARY
}

- (FColor)colorOf:(float)ratio;		{	return colorPurple4w;/*colorPurple4;*/}//colorOrange

@end
