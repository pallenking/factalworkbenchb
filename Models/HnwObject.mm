
//  HnwObject.mm -- Basic foundational object upon which others are based

/*! toDo to_do to do parameters:
 1. HnwObject allObjectsList doesn't work with automatic tests
 */


#import "HnwObject.h"
#import "Atom.h"	// knarley
#import "Brain.h"
#import "SimNsVc.h"
#import "SimNsWc.h"
#import "Id+Info.h"
#import "Common.h"
#import "NSCommon.h"


@implementation HnwObject

//////////////// SHORT NAMES //////////////////
static NSDictionary *shortNames = @{		// prefix for object naming
	@"Model01"			:@"HC",
	@"Path"				:@"path",
	@"Brain"			:@"brain",
	
	@"HnwObject"		:@"m",
	@"Part"				:@"p",
	
	@"Port"				:@"P",				@"ParameterPort"	:@"Parm",
	@"Splitter"			:@"s",
	@"Share"			:@"sh",
	@"MaxOr"			:@"sOr",			@"MinAnd"			:@"sAn",
	@"Broadcast"		:@"bc",				@"KNorm"			:@"sK",
	@"Hamming"			:@"sH",
	@"Multiply"			:@"sM",				@"Sequence"			:@"sS",
	@"Bayes"			:@"bay",			@"Bulb"				:@"blb",
	
	@"Label"			:@"text",
	@"Atom"				:@"sch",
	@"Mirror"			:@"mir",
	@"GenAtom"			:@"gen",
	@"SoundAtom"		:@"sound",
	@"Previous"			:@"pre",			@"Known"			:@"kno",
	@"Ago"				:@"ago",
	@"Modulator"		:@"mod",			@"Rotator"			:@"rot",
	
	@"Net"				:@"net",
	
	@"Bundle"			:@"bun",			@"Leaf"				:@"leaf",
	@"Tunnel"			:@"tun",
	@"Actor"			:@"tur",
	
	@"MatrixActor"		:@"mA",
	@"BundleTap"		:@"bTap",
	@"WorldModel"		:@"hwrld",
	@"WriteHead"		:@"wri",
	@"WriteHead4Branch"	:@"wBr",
	@"Branch"			:@"bra",
	
	@"BundleTap"		:@"g",				@"TimingChain"		:@"sClk",
	@"Generator"		:@"gen",
	@"ShaftBundleTap"	:@"shaft",
	@"SupertuxWorldModel":@"gS",
	
	@"Link"				:@"l",				@"LinkSegment"		:@"ls",
	@"View"				:@"V",				@"LinkView"			:@"V_",
	@"RenderingContext"	:@"rc",
	};

// Each class has it's own index for default
static NSMutableDictionary *classCurretIndex=[[NSMutableDictionary alloc] init];

id commonNameDefinitions = @{
	@"Tunnel"				:@"TunBun",
	@"SupertuxWorldModel"	:@"StuxWm",
	@"ParameterPort"		:@"PamPrt",
	@"TimingChain"			:@"TimeCh",
	@"BundleTap"			:@"BunTap",
	@"WorldModel"			:@"WModel",
	@"GenAtom"				:@"GenAtm",
	@"SoundAtom"			:@"SndAtm",
};

id classNames3char =		@{		@"LinkView":@"LV_"		};


#pragma mark 1. Basic Object Level

- init; {							self = [super init];

	self.name = [self registerAndName];		// default name
	return self;
}

// Wrapper for SwiftFactals (for GUI of Port, et.al.):
- fwClassName; {	return self.className;										}

 //////////////// TRACK ALL Model's //////////////////
int allObjectsCount			   = 0;	 // count of all objects allocated

  // a list of all HWObjects allocated, for debugging and leak detection.
NSMutableArray *allObjectsList = 0? [[NSMutableArray alloc] init]:nil;
int allObjectsWeakReferences   = 1;	// objects in allObjectsList owned by it
								   // (requires dealloc knowledge)

- (id) registerAndName; {

	 // A global registry of all objects (Just cause I'm old fassion?)
	allObjectsCount++;
    if (allObjectsList) {
		[allObjectsList addObject:self];	// (adds to retainCount)
		if (allObjectsWeakReferences)
			[self release];					// objects in allObjectsList unretained
	}										//		so there is no ownership loops
	
	   //// default NAME of object:
	  // Assign names as <shortName><index> (e.g. G1)
	 // allowed: a-z,A-Z,_.  illegal: " ", ",",
	NSString *shortName = [shortNames objectForKey: [self className]];
	assert(shortName, (@"class '%@' has no short name defined", [self className]));
	
	NSNumber *indexOf = [classCurretIndex objectForKey: shortName];
	int prevClassIndex = indexOf? [indexOf intValue]: 0;
	[classCurretIndex setObject: [NSNumber numberWithInt:prevClassIndex+1] forKey:shortName];
	
	id name = [NSString stringWithFormat:@"%@%d", shortName, prevClassIndex+1];
//	NSPrintf(@"=-=-=-   Allocating (%@ *)%lX '%@'\n", [self className], (LUI)self, name);
	return name;
}

- (id) copyWithZone:(NSZone *)zone		{
//	panicC("");
	return [[self deepMutableCopy] retain];
}

//#define debugRetainRelease
- (void) dealloc {
	if (logDeallocEvents)
		NSPrintf(@"=================== Dealloc %@ %lX '%@' ===\n", [self className], (LUI)self, self.name);

	 // remove reference here
	if (allObjectsList)   {
		int ind = (int)[allObjectsList indexOfObject:self];
		if (ind >= 0)   {
			if (allObjectsWeakReferences)	// if there without ownership-retain
				[self retain];					//		retain so remove won't delete it
			[allObjectsList removeObjectAtIndex:ind];	// remove, but don't dealloc
		}
		else
			panic(@"Freeing Object not in allObjectsList");
	}

	allObjectsCount--;
	assert(allObjectsCount>=0, (@"freeing more objects than allocated"));


	id name		= self.name;
	_name 	= nil;					// accessor has too many side effects.
	[name release];

//	id name		= self.name;
//	self.name 	= nil;

//	id name		= _name;
//	_name		= nil;				// accessor has too many side effects.

#ifdef debugRetainRelease
	if ([name isEqualToString:brain.logAllocationEventsOf])
		[self logEvent:@"= = = = = = > dealloc %p. Came with r=%d", self, (int)[self retainCount]];
#endif

    [super dealloc];					// drives retain count to zero, and the free
}


#pragma mark  3. Deep Access

- (id) deepMutableCopy; {				//HnwObject *rv = [super deepMutableCopy];
	HnwObject *rv = [[[[self class] alloc] init] autorelease];
	rv.name = [self.name deepMutableCopy];	// added deep 150609
//	rv.brain = brain;		// all the same for now		//xyzzy DEEPmUTABLEcOPY
//	rv.parameters = [self.parameters deepMutableCopy];
	return rv;
}

+ (void) resetObjectMonitors {	// reset whole class for new (virgin) run
	 // so new runs have indices starting at 1
    [classCurretIndex removeAllObjects];			// reset class-based counts to 0

	if (allObjectsList) {

		 // test objects should be gone
		assert(allObjectsList.count==allObjectsCount, (@"paranoid check"));

		 // if running in retain reference mode, there will be loops.
		if (!allObjectsWeakReferences)
			[allObjectsList removeAllObjects];

			// DOESN'T WORK YET
		if (allObjectsCount == 0)
			NSPrintf(@"====== No Objects DANGLING =======\n");
		else {
			if (0)
				panic(@"%d objects linger at end of run", allObjectsCount);
			else
				NSPrintf(@"\n====== WARNING:  %d DANGLING Objects at end of run:\n", allObjectsCount);

			for (HnwObject *obj in allObjectsList)
				if (Part *p = coerceTo(Part, obj))
					NSPrintf(@"=-=-=- Dangling: (%@ *)%lX '%@' retainCount=%d\n",
							 [p className], (LUI)p, p.fullName?: p.name?: @"??", (int)p.retainCount);
				else if ([obj respondsToSelector:@selector(name)])
					NSPrintf(@"=-=-=- Dangling: (%@ *)%lX '%@' retainCount=%d\n",
							 [obj className], (LUI)obj, [obj name]?: @"??", (int)obj.retainCount);
				else
					NSPrintf(@"=-=-=- Dangling: (%@ *)%lX retainCount=%d\n",
							 [obj className], (LUI)obj, (int)obj.retainCount);
		}
	}
}


   // Helper Routines
  //
 // Name limited to 6 characters
- (NSString *) name6;  {
       return [@"" addN:6 F:@"%@", self.name];
}

 // C-string name:
//- (const char *) nameC;				{	return self.name.UTF8String;		}

 // name the current object:
- (HnwObject *) named:(NSString *)n;	{	self.name = n;		return self;	}

- (NSString *) classCommonName6;	{
	id className = self.className;
	if (id shortenedClassName = commonNameDefinitions[className])
		className = shortenedClassName;
	id rv = [@"" addN:6 F:@"%@", className];
	return rv;
}
- (NSString *)	addressStr; {
	return [@"" addF:@"%#lx", (LUI)self];
}

#pragma mark 2. 1-level Access

#pragma mark 4. Factory

  // make another HnwObject:
 // Simple constructor with no Builder or info (with just default values)
+ (id) another; {
	 // make another of this class
	HnwObject *rv = [[[[self class] alloc] init] autorelease];
	return rv;
}

//#pragma mark 13. IBActions
//- (IBAction) guiAction1:(NSMenuItem *)sender {
//	printf("\nI am here\n\n");
//}

//RenderModes renderModeKindOfString(NSString *str) {
//    int ak = render3DUndefined;
//    for (NSString *s in renderModeKindArray) {
//        if ([s isEqualToString:str])
//			return (RenderModes)ak;
//        ak++;
//    }
//    return render3DUndefined;
//}

#pragma mark 15. PrettyPrint
  ////////////////////////////////////////////////////////////////////////
 ///////////////////////// PRETTY PRINT's FOR DEBUG /////////////////////
////////////////////////////////////////////////////////////////////////

- (NSString *) pp:aux {
	id nameStr = self.name;
	if (Part *mSelf = coerceTo(Part, self))
		if ([nameStr = mSelf.fullName isEqualToString:@"unset"])
			nameStr = self.name;
	if (!nameStr)
		nameStr = @"\"";
	return [@"" addF:@"(%@ *)\"%@\"", [self className], nameStr];
}

- (NSString *) ppPart;		{	return [self ppPart:0];		}
- (NSString *) ppPart:aux;	{	return @"";}

- (NSString *) pp1line:aux; {
	panic(@"");
	return @"hun";
}
//- (NSString *) ppTodo:aux; {
//	return [self.parameters pp:aux];
//}

- (NSString *) ppRetainCount;  {
	if (coerceTo(Brain, self).ppRetainCount_)
		return [@"" addF:@".%d", (int)self.retainCount];
	return @"";
}

NSArray *int2str = @[@"facingUp", @"facingDown"/*, @"facingNothing"*/];


  ////////////////////////////////////////////////////////////////////////
 //////////////////////// LINKS TO DEBUG ENVIREMENT /////////////////////
////////////////////////////////////////////////////////////////////////
#pragma mark 16. Debugging Aids
/*
p self		summary		(Splitter *) $0 = 0x0000608000105b20 Splitter: @"MinAnd" @"/r2/b"
p *self		?			(Part) $6 = Part: @"/r2/b"

fr v -R *self

lldb cmd		linkage				HnwObject	function
--------		-------				--------	--------
p object		-					-			print summary e.g. (Port *) $4 = 0x00006080000c50f0 @"n3.assim.z.up.P"

po				-					-			print ObjC description e.g. <Port: 0x6080000c50f0>

pp <*HnwObject>	pp					ppPart			e.g. 0f_1_Bit_0.0_P_[0.00]_in[0.00]_n3.assim.z.dn.P
pp <*NSObject>	pp					ppPart

p pp(C++)							NSString *pp(C++)

ps				pp(self,0)			ppPart		multi-line of self and subs
ps1

pm			   printRootPart(0)pp,0	ppPart
pM			   printRootPart(1)pp,1	ppPart
pv				printRootView()->pp->ppPart
ogl				ogl()

pi <id>			pInfo(id)			[id pp]
pi*

[HnwObject print]
[HnwObject pp]	short				pp part

ppp				prepare for debug				set fullName in part tree


 */

- (void) print {
	id aux = @{}.anotherMutable;

	NSPrintf(@"%@\n", [self    ppPart    :aux]);
}
- (void) p {		[self print];		}

// lldb$ pm -- print parts
void printRootPart(int mode) {
	Brain *brain = Brain.currentBrain;
	assert(brain, (@"Brain.currentBrain is nil"));
	pp(brain, mode);
}

void printRootView(int mode) {
	View *rootFwV = Brain.currentBrain.simNsWc.simNsVc.rootFwV;
	if (!rootFwV)
		printf("\nBrain.currentBrain.simNsWc.rootFwV is nil\n\n");
  	pp(rootFwV, mode);
}

 // lldb Entry Points:
void ppId(id object)			{		pp((void *)object, 0);			}
void pp(  id object)			{		pp((void *)object, 0);			}
void pp(  id object, int mode)	{		pp((void *)object, mode);		}

void pp(void *object)			{		pp(object, 0);					}
void pp(void *object, int mode) {
//	printf("pp(%lx, %d)\n", (long int)object, mode);

	 // HnwObject (Views, Parts, ...?)
    if (HnwObject *ob = coerceTo(HnwObject, (id)object)) {
		id aux = @{}.anotherMutable;
		int modeMask = 0x10;
		if (mode & modeMask) 					//  0x10 mask --> ppParams
			aux[@"ppParameters"] = @1;
		mode &= ~modeMask;

		if (mode == 0)						//  0 means just Atoms
			;//aux[@"deapth"]  = @0;
		else if (mode == 1)					//  1 means include Ports
			aux[@"ppPorts"] = @1;
		else if (mode == 2) {				//  2 means include Ports and Links
			aux[@"ppLinks"] = @1;
			aux[@"ppPorts"] = @1;
		}
		else if (mode < 0)					//  n(<0) means deapth -n
			aux[@"deapth"] = @(-mode);

	XX	id str = [ob ppPart:aux];

		NSPrintf(@"%@\n", str);
	}
	 // NS Objects:
    else if ([(id)object isKindOfClass:[NSObject class]]) {
        if (     [(id)object isKindOfClass:[NSSet		 class]])
            printNSSet(       0, (NSSet *)       object, 0);
        else if ([(id)object isKindOfClass:[NSScanner	 class]])
            printNSScanner(   0, (NSScanner *)   object, 0);
        else if ([(id)object isKindOfClass:[NSArray		 class]])
            printNSArray(     0, (NSArray *)     object, 0);
        else if ([(id)object isKindOfClass:[NSDictionary class]])
            printNSDictionary(0, (NSDictionary *)object, 0);
        else if ([(id)object isKindOfClass:[NSString	 class]])
            printNSString(    0, (NSString *)    object, 0);
        else if ([(id)object isKindOfClass:[NSNumber	 class]])
            printNSNumber(    0, (NSNumber *)    object, 0);
        else if ([(id)object isKindOfClass:[NSColor		 class]])
            printNSColor(     0, (NSColor *)     object, 0);
        else
            NSPrintf(@"(%@ *)object -- print method undefined", [(id)object className]);
        
        printf("(retainCount=%d)\n", (int)[(id)object retainCount]);
    }
    else {
        printf("(void *)%p\n", object);
        printf("no more printout possible\n");
    }
}

void pp(long int object) {			pp(object, 0);			}
void pp(long int object,int mode) {pp((void *)object, mode);}

void *pn(char *string) {
	id rv = nil;
	if (string==0 or string[0]=='\0')
		printf("name is '%s' ILLEGAL", string);	// who calls this?
	else {
		NSString *matchString = [NSString stringWithUTF8String:string];
		NSPrintf(@"looking for fullname with substring '%@'\n", matchString);
		
		// for quick debugger location
		assert(allObjectsList, (@"recompile with allObjectsList not 0"));
		for (HnwObject *bo in allObjectsList)  {
			id name = bo.name;
			if (Part *p = coerceTo(Part, bo))
				name = p.fullName;

			if ([name hasSuffix:matchString]) {
//			if ([name containsString:matchString]) {
				//NSPrintf(@"\t(%@ *)%#lx\n", bo.className, (LUI)bo);
				NSPrintf(@" found ((%@ *)0x%lx) at '%@'\n",
					[bo className], (unsigned long)bo, name);
				if (rv == 0)		// Take the first, it's the base
					rv = bo;
				//pp(bo, 0);
				//return bo;
			}
		}
	}
	if (rv)
//		printf("   returns %lx\n", (long int)rv);
		NSPrintf(@"    returns ((%@ *)0x%lx)\n", [rv className], (unsigned long)rv);

	
	
	else
		NSPrintf(@" not found\n");

	
	return rv;
}
void *pn(void) {
	printf("%d defined Parts:\n", allObjectsCount);
	assert(allObjectsCount==allObjectsList.count, (@"mismatch"));
	for (HnwObject *ob in allObjectsList)
		NSPrintf(@"%@ ", ob.name);
	printf("\n");
	return nil;
}

  ///////////////////////////////////////////////////////////////////////
 /////////////////////////     Debugging Aids      /////////////////////
///////////////////////////////////////////////////////////////////////

 /// 1. You can determine what objects are in what autorelease pools by calling _CFAutoreleasePoolPrintPools
//	extern int _CFAutoreleasePoolPrintPools();		// debug

 /// 2. Debugging RETAIN/RELEASE
#ifdef debugRetainRelease
- (id) retain {
	id rv = [super retain];
	if ([self.name isEqualToString:brain.logAllocationEventsOf])
		[self logEvent:@"= = = = = = > retain %p%@", self, self.ppRetainCount];
	return rv;
}
- (id) autorelease {
	id rv = [super autorelease];
	if ([self.name isEqualToString:brain.logAllocationEventsOf])
		[@"" addF:@"= = = = = = > autorelease %p%@", self, self.ppRetainCount];
			[self logEvent:@"= = = = = = > autorelease %p%@", self, self.ppRetainCount];
	return rv;
}
- (oneway void) release {
	if ([self.name isEqualToString:brain.logAllocationEventsOf])
		[@"" addF:@"= = = = = = > release %p%@", self, self.ppRetainCount];
			[self logEvent:@"= = = = = = > release %p%@", self, self.ppRetainCount];
	[super release];
}
#endif

  ////////
 // --- Useful OBJC: _CFPrintForDebugger
// http://www.koders.com/objectivec/fid21DCF064BBB98DD97D6F459C3169226C6D72A9D4.aspx?s=my*+-mysql
 // ---- Useful C++ brain Hooks, back in 2011
// ----- display _vptr with .gdbinit macros:
// USAGE:
// p *this                     yields v_ptr.Net = 0xAAAA
// set vptr=0xAAAA
// vptr
void *vptr = 0;			// used to display _vptr table.  See .gdbinit .

bool logDeallocEvents = false;

@end
