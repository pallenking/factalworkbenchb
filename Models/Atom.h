//  Atom.h -- a node in the reactive semantic network -- C2013PAK

/*!		CONCEPTS
	An Atom communicates with other Atoms using Ports.
	An Atom usually has a Primary Port. Assume there are N other Ports
        N==0:           terminators, mirrors
        N>=0:           for factal shares
        N==1:           Links, Bundles, Ago, ... with two ends
        N==2:           Previous, Rotator(unimplemented as yet)

170202: Snapshot of Port's names:			Branch is a "compound" Atom:
	P		-- Primary   I/O at bottom		Just above L
	S		-- Secondary I/O at top			At bottom
	sec		-- Secondary, optional shares allowed
	T		-- Terciary  I/O at top
	U		-- Unknown   I/O top (optional)
	B		-- Broadcast I/O top (optional)
	L		-- Latch	 I/O Inside
  Operational Modes:
	M		-- 
	N		--			 I/O
	KIND	--  of share I/O
	SPEAK	--  of share I/O


170202: Snapshot of Port definitions in atomsDefinedPorts, by class:
Atom				P
	Mirror			"
	GenAtom		"
	BundleTap		"
	Ago				" S
	Link			" S
	Modulaor		" S	    T
	Rotator		" S	    T
	Known			" S	    T
	Previous		" S	    T	  L M N

	Splitter		"   sec   U B		KIND
		Branch		" S "     " " L M   "    SPEAK

	Net								E
		Actor						"
		Bundle						"
			Leaf					"

Note: Quote (") is ditto, implies INHERITED from superclass
 */

@class Splitter;
@class Link;
#import "Port.h"

@interface Atom : Part

	@property (nonatomic, retain) id	bandColor;	// Port whose value sets band
	@property (nonatomic, assign) char	proxyColor;	// index; 0 = none
	@property (nonatomic, assign) char	postBuilt;	// object has been built

#pragma mark 2. 1-level Access
- (Port *)		getPortNamed:portName down:(int)down;	// Utility routine

#pragma mark  4a Factory Access						// Methods defined by Factory
- (id)			atomsDefinedPorts;		// @"pcd", where p=Port c=Create d=Down

	/// In .h files, for define ports for subclasses
	#define atomsPortDefinitions(varName, portName, portDown)				\
	- (Port *) varName;														\
	- (Port *) varName##In;													\

atomsPortDefinitions(pPort,		P, true)
atomsPortDefinitions(sPort,		S, false)

	/// In .mm files, for construct port methods for subclasses
	/// To subclasses to construct needed in and out Port accessors:
	#define atomsPortAccessors(varName, portName, portDown)						\
	- (Port *) varName; { return [self getPortNamed:@#portName down:portDown]; }\
	- (Port *) varName##In; { return [self varName].connectedTo; }				\

#pragma mark 4. Factory

#pragma mark 6. Navigation
- (Port *)		getPortFacingDown:(bool)localDown;
- (void)		portCheck;

#pragma mark 9. 3D Support
- (Matrix4f)	positionOfPort:(Port *)port inView:(View *)v;


@end


