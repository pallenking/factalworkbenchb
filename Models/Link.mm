// Link.mm -- an Atom inserted in Port Connections to add visual delay C2010PAK

#import "Splitter.h"
#import "Link.h"
#import "Share.h"

#import "LinkView.h"
#import "Actor.h"
#import "Brain.h"
#import "WriteHead.h"
#import "FactalWorkbench.h"

#import "Id+Info.h"
#import "Common.h"
#import "NSCommon.h"
#import "vmath.hpp"
#import "SimNsV.h"
#import "SimNsWc.h"
#import "GlCommon.h"
#import <GLUT/glut.h>

@implementation Link

#pragma mark 1. Basic Object Level
- (id)init {			self = [super init];

	self.length 			= nan("");
	self.minColorVal 		= 0.0;
	self.maxColorVal 		= 1.0;

	self.upConveyor 		= [NSMutableArray array];
	self.downConveyor 		= [NSMutableArray array];
	return self;
}

- (void) dealloc; {
    self.upConveyor 		= nil;
    self.downConveyor 		= nil;

    [super dealloc];
}

#pragma mark  3. Deep Access
- (id) deepMutableCopy; {		Link *rv = [super deepMutableCopy];

	rv.length				= self.length;
	rv.upConveyor			= [self.upConveyor   deepMutableCopy];
	rv.downConveyor			= [self.downConveyor deepMutableCopy];
	return rv;
}

#pragma mark  4a Factory Access						// Methods defined by Factory
// @"pcd", where p=Port c=Create d=Down
- (id) atomsDefinedPorts;	{			id rv = [super atomsDefinedPorts]; // probably returns @"P"
	
	rv[@"S"]		= @"pc ";
	rv[@"pri"]		= @"  d";
	rv[@"sec"]		= @"   ";
	return rv;
}
atomsPortAccessors(sPort,		S, false)		// others inherited

#pragma mark 4. Factory
Link *aLink(id pri, id sec, id etc) {
	id info = @{@"pri":pri?:@0,  @"sec":sec?:@0,  etcEtc};

	Link *newbie = anotherBasicPart([Link class]);
XX	newbie = [newbie build:info];
	return coerceTo(Link, newbie);
}

- (id) build:(id)info;	{					[super build:info];

//!	xzzy1 Link::		1. length			:@"float"		-- force length

	if (id lengthId = [self parameterInherited:@"length"])
		if (float length = coerceTo(NSNumber, lengthId).floatValue)
			self.length = length;
	self.minColorVal = [[self parameterInherited:@"minColorVal"] floatValue]?: 0.0;
	self.maxColorVal = [[self parameterInherited:@"maxColorVal"] floatValue]?: 1.0;

	return self;
}

#pragma mark 6. Navigation
 /// Any Port connecting nowhere causes the whole Link to be deleted
- (void)		portCheck;		{
	id pp = self.parent.parts;

	 /// Disconnect all Ports
	for (id p in self.parts)
		if (Port *port = coerceTo(Port, p))
			port.connectedTo = nil;

	[pp removeObject:self];
}

#pragma mark 7-. Simulation Actions
- (void) reset; {							[super reset];
	
	[self.upConveyor removeAllObjects];
	[self.downConveyor removeAllObjects];
}

#pragma mark 8. Reenactment Simulator
//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//
//**//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//
//***//*//*//*//*//*//*//*//  Reenactment Simulator  //*//*//*//*//*//*//*//*//
//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*////
//**//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//

- (void) simulateDown:(bool)downLocal; {

	 // this routine processes both locally up and down simSteps, per:
	NSMutableArray *conveyor = downLocal? self.downConveyor :self.upConveyor;
	if (conveyor == nil)
		return [super simulateDown:downLocal];	// act without delay

	  /// Up and Down are processed alike!
	 /// Take data from inPort, process through conveyor, and put output into outPort
	Port *inPort	= !downLocal? self.pPort: self.sPort;
	Port *inInPort	= inPort.connectedTo;
	Port *outPort	= !downLocal? self.sPort: self.pPort;

	if (inInPort.valueChanged) {
		float valuePrev = inInPort.valuePrev;
		float valueIn   = inInPort.valueGet;
		
		[inPort logEvent:@"   ENQUE to link %.2f (was %.2f)", valueIn, valuePrev];
		
		  // ENQUEUE the event
		 //
		float initialHeight = !downLocal? 0.0001: 0.9999;
		 // get first LinskSegment in conveyor, (or 0)
		LinkSegment *seg = [conveyor count]? [conveyor objectAtIndex: 0]: 0;
		if (!seg or seg.heightPct!=initialHeight) { // overwrite previous if unmoved
			seg = mustBe(LinkSegment, [LinkSegment another]);
			[conveyor insertObject:seg atIndex:0];
			self.brain.unsettledOwned++;
		}
		seg.val = !downLocal? valuePrev: valueIn;
		assert(     !isNan(seg.val), (@"enqueing nan value to link"));
		assert(!std::isinf(seg.val), (@"enqueing inf value to link"));
		seg.heightPct = initialHeight;
		seg.goingDown = downLocal;
	}

	 // Run the link "CONVEYOR BELT". it takes sensations up and desires down.
	for (int i=0; i<(int)[conveyor count]; i++) {
		LinkSegment *seg = [conveyor objectAtIndex:i];
		float logVel = self.brain.linkVelocityLog2;
		float segmentVelocity = exp2f(logVel);
		float vel = seg.goingDown? -segmentVelocity: segmentVelocity;
		float height = seg.heightPct += vel;

		  // DEQUEUE the event:
		 //
		if (height >= 1 or height <= 0) {     // has a seg gone off an end?
			 // Get the seg with the output:
			LinkSegment *outValSeg = !downLocal?
						(i>0? [conveyor objectAtIndex:i-1]:0):	// up: the prev segment (or nil)
						[conveyor objectAtIndex:i];				// down: this segment (always)
			float outVal = outValSeg? outValSeg.val: inInPort.value;

			 // 161110 160929 unneeded and confusing
			//[outPort logEvent:@"<= DEQUE from link"];
			 // a hack to make the valueTake silent in printout
			outPort.value = outVal;						// TAKE the value quietly (not valueTake)
		
			[conveyor removeObjectAtIndex:i--]; // deque used up top conveyor element
			self.brain.unsettledOwned--;
		}
	}
}

 // Links receive no messages
- (id) receiveMessage:(FwEvent *)event; {		return nil;					}

#pragma mark 9. 3D Support
// \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\    //
//  \\ \\ \\ \\ \\ \\ \\ \\ \ 3D Support\\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\  //
//   \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\//
- (View *) reViewIntoView:(View *)v; {				/* do nothing */
  	assert(![self.name isEqualToString:self.brain.breakAtViewOf],
						(@"Break from breakAtViewOf %@", self.name));
	if (!v) {
		v = [LinkView another];				// newly create a view
		v.part = self;
	}

	assert(v.part==self, (@"caller should ensure v.part==self"));
	return v;
}

- (void) boundIntoView:(View *)v; {			/* do nothing */
	assert(![v.name isEqualToString:self.brain.breakAtBoundOf],
						(@"breakAtBoundOf view %@", self.brain.breakAtBoundOf));
}
- (void) placeIntoView:(View *)v; {			/* do nothing */			}


#pragma mark 11. 3D Display
//// // // // // // // // // // // // // // // // // // // // // // // ///
/// // // // // // // // // // 3D Display  // // // // // // // // // ////
// // // // // // // // // // // // // // // // // // // // // // // // //

-(void) adjustLinksPlaceInView:(LinkView *)v; {
  	assert(![v.name isEqualToString:self.brain.breakAtPositionOf], (@"Break from breakAtPositionOf %@", v.name));
	View  *linkPapaView		= v.superView;
	Part  *linkPapaPart		= self.parent;
	NSString *str			= [@"" addF:@"%@(%@)", linkPapaView.name, linkPapaPart .name];
	//			other0		link0Port	self:		link1Port	other1
	//								.	Link	.
	//				 <====> pPort  '			 '-	sPort <====>


	 // Defunct if either Port missing:
	Port *link0Port			= self.pPort;		//0: worker
	Port *link1Port			= self.sPort;		//1: boss
	if (link0Port==nil or link1Port==nil)
		return;			// defunct link

	 // Defunct if either Port not properly connected:
	Port *other0			= link0Port.connectedTo;			// port our worker connected to
	Port *other1			= link1Port.connectedTo;			// port our boss   connected to
	assert(other0.connectedTo==link0Port and other1.connectedTo==link1Port, (@"improper linkings"));
	if (other0==nil or other1==nil)
		return;			// defunct link

	 // get Views of boss and worker:
	v.other0View			= [linkPapaView searchOut4Part:other0 except:nil];
	v.other1View			= [linkPapaView searchOut4Part:other1 except:nil];
	assert(v.other0View, (@"%@ has no View of '%@'", str, other0.fullName));
	assert(v.other1View, (@"%@ has no View of '%@'", str, other1.fullName));

	 // get their hotspots
	Hotspot other0Hots		= [other0 hotspotOfPortInView:linkPapaView];
	Hotspot other1Hots		= [other1 hotspotOfPortInView:linkPapaView];

	if (isNan(other0Hots))
		[[@"" addF:@"????? the hotspot of the link '%@'s other is undefined", link0Port.fullName] ppLog:nil];
	if (isNan(other1Hots))
		[[@"" addF:@"????? the hotspot of the link '%@'s other is undefined", link1Port.fullName] ppLog:nil];

	Vector3f deltaVect		= other1Hots.center - other0Hots.center;
	float deltaLen			= deltaVect.length();
	
	  // Places the Arrows at each end.
	 //
	// Hungarian: Fraction [0 1] [radius inset]
	float f0r				= other0Hots.radius / deltaLen;	// limit by radius
	float f1r				= other1Hots.radius / deltaLen;	// limited by radius

	 // Inset: allows arrows to go closer to Port than r
	if (deltaVect.y >=0) {						// Insets don't apply to downward links
		float f0Yi			= other0Hots.inset / deltaVect.y;	// limit by height too
		if (f0r > f0Yi and !isNan(f0Yi))			// might be less, inf or nan (if both 0)
			f0r				= f0Yi;						// the TIGHTER  prevails

		float f1Yi			= other1Hots.inset / deltaVect.y;
		if (f1r > f1Yi and !isNan(f1Yi))
			f1r				= f1Yi;
	}
	v.other0Loc				= other0Hots.center + deltaVect * f0r;
	v.other1Loc				= other1Hots.center - deltaVect * f1r;
}

- (void) renderLinkInView:(LinkView *)v with:(RenderingContext *)rc; {
	Brain *brain			= self.brain;
	assert(![v.name isEqualToString:brain.breakAtRenderOf],
							(@"brain.breakAtRenderOf %@", v.name));
	bool downInWorld		= self.downInWorld;
	Port *boss				= self.sPort;
	Port *worker			= self.pPort;

	 /// Skip non-existant ends (Ports) of a Link
	if (!boss or !worker)
		return;

	 /// Skip Invisible Links unless gui says not
	bool brainsDispInvis	= brain.displayInvisibleLinks;
	Actor *actor			= [self.pPortIn enclosedByClass:@"Actor"];
	actor					= actor?:[self.sPortIn enclosedByClass:@"Actor"];
	bool actorsDispInvis	= actor.displayInvisibleLinks;
	if ((self.initialDisplayMode&displayInvisible) and !brainsDispInvis and !actorsDispInvis)
		return;

	 /// we are now in link's superView's coord's.
	[rc startDrawingView:v];		//// push my view onto drawing stack

	 /////////////// NOW READY TO DRAW a LINK: ///////////
	assert(worker.flipped, (@"just checkin"));

	  // if a link is going to a port whose parent has an animation offset,
	 // move the end of the line that's touching it
	Vector3f other0Loc		= v.other0Loc;
	if (WriteHead *wh0		= v.other0View.part.writeHead)
		other0Loc			+= wh0.animationOffset;
	Vector3f other1Loc		= v.other1Loc;
	if (WriteHead *wh1		= v.other1View.part.writeHead)
		other1Loc			+= wh1.animationOffset;

	 // we are in our superView's coordinate system.
	Vector3f delta = other1Loc - other0Loc;
	float totalLength		= delta.length();
	if (totalLength < 0.01) {					// not big enough to draw?
		delta = Vector3f(0, 0.1, 0);				// draw vertical line
		totalLength			= 0.1;
	}
	glPushMatrix();
		glTranslatefv(other0Loc);				// origin at worker
		Vector3f iY  = Vector3f(0.0, 1.0, 0.0);
		Matrix4f rot = rotationMatrixVerticalizing(delta, iY);
		glMultMatrixf(rot);
	
		  ////////////// #### draw Lower Share:
		 ///
		float linkRadius = brain.linkRadius;
		float coneRadius  = 0.3+linkRadius,         coneLength = 1;
		float ellipseDiam = brain.linkEventRadius<0? 0.3+linkRadius:
							brain.linkEventRadius;
	
		///////////// NOW in a frame in which:
		////////////   1. our vector lies on the vertical Y axis
		///////////	   2. worker is at (0,0,0), boss at (0,length,0)

		 // N.B: up/down refer to the canonic (possibly flipped) direction,
		int iUp=0, iDown=0, conveyorInd=0;
		int nUp = (int)[self.upConveyor count];
		int nDown = (int)[self.downConveyor count];

		float valUp   = worker.valDown;		// worker locally down --> value up
		float valDown = worker.valUp;
		bool drawActive = valUp>1e-4 or valDown>1e-4 or linkRadius>0;
	
		if (Port *workerInPort = coerceTo(Share, worker.connectedTo)) {
			//[rc startDrawingView:workerInPort];
			if (drawActive) {
				glPushMatrix();
					rc.color = [workerInPort colorOfValue: workerInPort.value];
					glTranslatef(0, 0, 0);
					glRotatef(-90, 1,0,0);
					glutSolidCone(coneRadius, coneLength, 16, 16);
				glPopMatrix();
			}	//[rc finishDrawingView:self];
		}

		  // Look back At the Camera
		 // //http://www.opengl-tutorial.org/intermediate-tutorials/billboards-particles/billboards/
		Matrix4f atCamera			= getProjectionMatrix().inverse();
        Vector3f atCameraWZToward	= atCamera.row(2);
		float y=atCameraWZToward.x, x=atCameraWZToward.z;	// beileve it or not!
		float theta = -atan2f(y,x);	// +M_PI angle to rotate perspetive by
		Matrix4f aboutY = Matrix4f().createRotationAroundAxis(0, theta, 0);

		glMultMatrixf(aboutY);

		Vector3f separation			= Vector3f(linkRadius/2,0,0);//0.5

		for (int down=0; down<2; down++) {		// Two scans: up->down==0, down->down==1
			 // Starting point
			Port *inReg				= down? self.pPort: self.pPort.connectedTo;
			Port *inDir				= down? self.pPort: self.sPort;
			float prevVal			= inReg.valuePrev;
			// 171113 BUG: The first display after link.P becomes 1 displays
			// doesn't have anything in the conveyor, so the whole link
			// displays green. Next cycle, conveyor is loaded and no more bug.
//			float prevVal			= inReg.value;
			rc.color				= [inDir colorOfValue:prevVal];
			Vector3f s				= separation * (down?1: -1);
			Vector3f prevPt			= Vector3f(s.x, 0, s.z);
			Vector3f curPt			= prevPt;
			

			 // a ###LINE### for Lower Worker Share
			NSArray *conveyor		= down? self.downConveyor: self.upConveyor;
			for (int i=0, n=(int)conveyor.count; i<n; i++) {

				  // Scan UP the line, from WORKER (primary) to BOSS (secondary)
				 //
				LinkSegment *seg    = [conveyor objectAtIndex:i];
				curPt.y				= totalLength * seg.heightPct;

				if (drawActive)
					myGLCable(prevPt, curPt, linkRadius);		// #### draw CABLE

				 // Display Events as ellipsoids
				if (down)					// down --> event = new color
					rc.color		= [inDir colorOfValue:seg.val];//self.sPort
				glPushMatrix();
					glTranslatefv(curPt);
					glScalef(ellipseDiam, ellipseDiam/3.0, ellipseDiam);			// #### draw ELLIPSOID
					glutSolidSphere (1, 4, 3);
				glPopMatrix();
				if (!down)					// up --> event = old color
					rc.color		= [inDir colorOfValue:seg.val];

				prevPt				= curPt;
				drawActive			= valUp>1e-4 or valDown>1e-4 or linkRadius>0;
			}
			
			 // Draw Final segment of link.
			curPt.y = totalLength;
			if (drawActive)
				myGLCable(prevPt, curPt, linkRadius);
		}

		  /// #### draw Upper Share
		 ///
		if (Share *bossInPort = coerceTo(Share, boss.connectedTo)) {
			if (drawActive) {
				glPushMatrix();
					rc.color = bossInPort.colorOfValue;
					glTranslatef(0, totalLength, 0);
					glRotatef(90, 1,0,0);
					glutSolidCone(1*coneRadius, coneLength, 16, 16);
				glPopMatrix();
			}
		}
	glPopMatrix();
	
	 // Link Names:
	if (self.brain.displayLinkNames) {
		glPushMatrix();
			glTranslatefv((other0Loc + other1Loc)/2.0);		// origin at center
			Vector3f inCameraShift = Vector3f(0.0, 0.0, .5);// in "facing camera" coordinates
			Vector2f spotPercent   = Vector2f(0.5, 0.5);	// box center from lower left corner
			rc.color = colorPurple4b;
			myGlDrawString(rc, self.name, -1, inCameraShift, spotPercent);
		glPopMatrix();
	}
	[rc finishDrawingView:v];		//// remove my part from drawing stack
}

- (FColor)colorOf:(float)ratio;   {
	panic(@"never used");
	return (FColor){1.0, 0.5, 0.0,  1};
}

#pragma mark 15. PrettyPrint
    ///\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\///
   ///\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\///
  ///\\//\\//\\//\\//\\//\\//\\// PRINTOUT //\\//\\//\\//\\//\\//\\//\\//\\///
 ///\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\///
///\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\///
- (NSString *)	pp1line:aux; {		id rv=[super pp1line:aux];

	if (self.parts.count!=2)
		rv=[rv addF:@"????? parts==%d (!=2) ????? ", (int)self.parts.count];
	if (id conveyor = self.upConveyor)
		rv=[rv addF:@"%@", [self ppConveyor:conveyor]];
	if (id conveyor = self.downConveyor)
		rv=[rv addF:@"%@", [self ppConveyor:conveyor]];
	return rv;
}

- (NSString *) ppConveyor:conveyor; {
	id rv=@"";
	for (LinkSegment *seg in conveyor)
		rv=[rv addF:@"\n\t\t\t:%@, val=%f, height=%f",
			seg.goingDown? @"Down":@"  Up", seg.val, seg.heightPct];
	return rv;
}
@end

//  define a segment along the length of a link -- C2011 PAK
@implementation LinkSegment
@end
