//  Known.h -- Accounts for known phenomenon, in reducing the unknown. C2014PAK

/// 150323 This functionality has wandered into unknownPort, so only old things use this

/*! Concepts:
	Have flows up, from lower self to the have of the upper port named "known".
	The want flows down from that known port is used to decrease self's have
		as it is applied to the upper port named "unknown".
	Unknown's desire down sent unmodified to self's downward want.
 */

#import "Atom.h"
@interface Known: Atom

//- (Port *) known;				// 'S' Port
//- (Port *) unknown;			// 'T' Port

atomsPortDefinitions(sPort,		S, false)
atomsPortDefinitions(tPort,		T, false)


@end
