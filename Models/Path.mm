//  Path.mm -- specify a relative or absolute path C2015PAK

#import "Path.h"
#import "Atom.h"
#import "Port.h"

#import "Common.h"
#import "NSCommon.h"
#import "Id+Info.h"

@implementation Path

- (id) deepMutableCopy; {		Path *rv = [super deepMutableCopy];
	panic(@"");
    rv.nameFull			= [self.nameFull deepMutableCopy];
    rv.nameAtom			= [self.nameAtom deepMutableCopy];
    rv.namePort			= [self.namePort deepMutableCopy];

    rv.linkOptions		= [self.linkOptions deepMutableCopy];
    rv.direct			= self.direct;		// trailing '='
    rv.noCheck			= self.noCheck;		// trailing '^'
    rv.invisible		= self.invisible;	// trailing '@'
//	rv.openingDown		= self.openingDown;

    rv.summary			= [self.summary deepMutableCopy];
	return rv;
}

- (void) dealloc; {
    self.nameFull		= nil;
    self.nameAtom		= nil;
    self.namePort		= nil;
    self.linkOptions	= nil;
    self.summary		= nil;

    [super dealloc];
}

+ (Path *) pathWithName:nameFull; {
	Path *rv = [[[Path alloc] init] autorelease];
	[rv setNameFull:nameFull];
	return rv;
}


	////////// PARSE nameFull's characteristics into (Path *)self /////////
	/// This routine clears out all state of a Path,
	///		so it is an (unorthodox) initializer
	/// -<( following lines should match Path.h documentation )>-

- (void) setNameFull:(NSString *)nameFull; {
	if (nameFull)
		mustBe(NSString, nameFull);
	
	_nameFull = nameFull;

	  // NOTE: setNameStr can ignore leading '/'s to the atom

	  // process link parameters; those after the first comma
	 /// AAA in Path.h
	NSArray *components	= [nameFull componentsSeparatedByString:@","];
	int n 				= (int)components.count - 1;
	NSString *nameAtom	= components[0];		// first is atom

	self.linkOptions 	= @{}.anotherMutable;
	while (n > 0) {								// all but first --> linkOptions
		id option = components[n--];
		NSArray *nameVal = [option componentsSeparatedByString:@"="];
		assert(nameVal.count==2, (@"Syntax: '%@' not of form <prop>=<val> (HINT: older form was just length, no key)", option ));
		[self.linkOptions setValue:nameVal[1] forKey:nameVal[0]];
		//self.linkOptions[nameVal[0]] = nameVal[1];		// WONT COMPILE
	}
	self.direct = self.flipPort = self.dominant	= self.invisible = self.noCheck	= false;
	nameAtom = [self takeOptionsFrom:nameAtom];

	 ///! CCC in Path.h
	self.namePort		= @"";				/// Initially no Port
	NSInteger suffixLoc = [nameAtom rangeOfString:@"." options:NSBackwardsSearch].location;
	if (suffixLoc != NSNotFound) {
		self.namePort = [nameAtom substringFromIndex:suffixLoc+1];	// after  "."
		nameAtom = [nameAtom substringToIndex:suffixLoc];				// before "."
	}
	self.nameAtom = nameAtom;							/// Name without options
	
	self.summary  = [self pp];			// for debugging
}

 // Options define how the link is formed and displayed:
- (id) takeOptionsFrom:(NSString *) nameNoptions; {
	 /// BBB in Path.h
	while (char optionChar = nameNoptions.length>0? [nameNoptions characterAtIndex:nameNoptions.length-1]: '\0') {
		if (optionChar		== '=')	// Trailing "="				/////<<<< direct
			self.direct = true;			// signifies direct connect, not thru a link
		else if (optionChar == '%')	// Trailing "%"				/////<<<< no checking
			self.flipPort = true;		// flip port's direction
		else if (optionChar == '^')	// Trailing "^"				/////<<<< reverse checking
			self.noCheck = true;		// don't check up/down of associated port.
		else if (optionChar == '!')	// Trailing "!"				/////<<<< dominant
			self.dominant = true;		// dominant in positioning
		else if (optionChar == '@')	// Trailing "@"				/////<<<< invisible
			self.invisible = true;		// invisible
		else
			break;					// not a valid option -- part of the name

		 // remove the character
		nameNoptions = [nameNoptions substringToIndex:nameNoptions.length-1]; // remove trailing modifiers
	}
	self.summary  = [self pp];			// update, for debugging
	return nameNoptions;
}


- (bool) atomNameMatches:(Part *)part;{

	  // Compare Atom names:
	 /// DDD in Path.h
	 //											Hungarian COMPonentS
	NSArray *partComps = [part.fullName componentsSeparatedByString:@"/"];
	NSArray *pathComps = [self.nameAtom componentsSeparatedByString:@"/"];

	 // loop through PATH, in REVERSE order, while scanning PART (in rev too)
	int nPart = (int)partComps.count-1;		// e.g: 3 [ "", brain1, main]
	int nPath = (int)pathComps.count-1;		// e.g: 2         [ "", main]

	 // Check all components specified in Path match
	for (; nPath>=0; nPath--, nPart--) {

		assert(nPart>=0, (@"Path has more components than Part.nameFull"));
		NSString *partComp = partComps[nPart];

		NSString *pathComp = pathComps[nPath];
		if (nPath==0 and pathComp.length==0)	// if first component is ""
			break;									// it matches
	
		 // Failure to match a component
		if (![pathComp isEqualToString:partComp])
			return false;							// Component name mismatch
	}
	 // All Path components match self.fullName
	return true;							// all tests pass
}

- (bool) fullNameMatches:(Part *)part; {

	  // match routine will do these
	 // if part is a PORT
	if (Port *p = coerceTo(Port, part)) {		// if part is     a Port
		if (![self.namePort isEqualToString:p.name])				// portName must match part's name
			return false;
		return [self atomNameMatches:p.atom];	// nameAtom must match parent
//		return [self atomNameMatches:part.parent];	// nameAtom must match parent
	}
	else {										// if not a Port
		if (self.namePort != 0)						// but port name specified
			return false;

		return [self atomNameMatches:part];			// else we have an Atom
	}
}


#pragma mark 15. PrettyPrint

- (NSString *) pp; {
	id rv = [@"" addF:@"%@::%@", self.nameAtom, self.namePort];
	rv=[rv addF:@"%s%s%s%@%s%s", self.direct	 ? "="	:"",
								 self.flipPort   ? "%"	:"",
								 self.noCheck	 ? "^"	:"",
								 self.linkOptions?		:@"",
								 self.dominant	 ? "!"	:"",
								 self.invisible  ? "@"	:""
	   ];
	return rv;
}
- (NSString *) ppProp; {
	id rv = @"";

	self.namePort.length? [self.nameFull addF:@".%@", self.namePort]: self.nameFull;
	rv=[rv addF:@"%s%s%@", self.direct?"=":"", self.noCheck?"^":"", self.linkOptions?:@""];
	return rv;
}

//id initialDisplayModeNames = @{@"Atomic":@2, @"Open":@4, @"Invisible":@8,};
- (NSString *) ppProperties; {
	NSString *rv = @"";
	if (self.direct)
		rv=[rv addF:@"Dir "];		// direct
	if (self.noCheck)
		rv=[rv addF:@"!Ch "];		// noCheck
	if (self.dominant)
		rv=[rv addF:@"Dom "];		// dominant
	if (self.invisible)
		rv=[rv addF:@"Inv "];		// invisible
//	if (self.openingDown)
//		rv=[rv addF:@"oDn "];		// openingDown
//	if ([rv isEqualToString:@""])
//		rv = @"--- ";				// Normal
	rv = rv.length? [@"" addF:@"{%@}", rv]: nil;
	return rv;
}


- (const char *) ppC; {	return [self pp].UTF8String;		}


@end
