//  Branch.h -- an outshoot from a crux, somehow enabled 2016PAK

#import "Splitter.h"

@interface Branch : Splitter

	@property (nonatomic, assign) char		speak;
	/*	Speak controls how the rotator part of a Branch works:
	 *		Initial value is set during construction. (no set --> 0)
	 *		If the Branch has a "speak" Port, it controls this setting during operation.
	 *
	 *	A Branch (as a subclass of Splitter) also has a "kind" @property
	 *		Initial value is set during construction. (no set --> 0)
	 *		If the Branch has a "kind" Port, it controls this setting during operation.
	 *
	 *	Other Splitters (e.g. in the CONtext Bundle) also have "kind" @properties and Ports
	 */

	@property (nonatomic, assign) char		noMInSpeak;
    /*  M-Port is dead in speak mode, signals
        Used by Bulbs in CONtext Bundles.
     */

    @property (nonatomic, assign) float		lPreLatch;

NSString *nextMorseName(NSString *curName, Part *crux);

/* L I S T E N				speak==0;		(Alternate Drawing)
														Note: B ports not shown
			  U1 U2 U3 U4							  U1 U2 U3 U4
	  .--o--.__SW - MAX							 .--o--.__SW - MAX
	U |L|	|___|in |L|/		U	Unknown	  U	 |L|   |___|in  |L|/		U
	P			`--o--'			P	Primary	  P				`--o--'			P
				   |										   |
	L			.--o--.			L	Latch	  L				.--o--.			L
			   <|$|>in|	<- - - ext clk (Loads $)- - - - -> <|$|>in|			<cp
				 /    |								   ______/__  |
				/	   `->[L]						 [in	| 	\ |
	M		  lPre	 		]o	M	mode	  M		o[	  lPre	 X			M
				^	   .----]						 [L|<-=-+---' |
				|	  v	   in								|	  /
				|in |L|										|in |L|
	S			 --o--			S	Secondary S				 --o--			S
										  B

	   -- Unknown   V1 V2 V3 ...
	  [			SW - MAX		]
	  [							]
	  '---------|in |L|/--------'
	P			`--o--'
				   |
	L			.--o--.
			ckk<|$|>in|
				 /    |
				/	   `->[L]
	M		  lPre	 		]o
				^	   .----]
				|	  v	   in
				|in |L|
	S			 --o--

    _________        ______
										  cp   cp		   cp	cp	 cp
	from S.in  .<--- / S.in  (up) ____ATTT*TTTTTTTTTTTTTTTTTTTTTT
			   |				  ____aTTTTTTTTTTTTTTTTTTTTTTTTTT
			   '---> / L.$   (up) ________*bTTTTTTTTTTTTTTTTTTTTT
			   .<--' / P.in  (up) ________*_TTTTTTTTTTTTTTTTTTTTT
			   |
			   '---> / U1.out(up) ________*_TTTTTTTTTTTTTTTTTTTTT

	?		   .<--- / U1.in (dn) ________*______CTTTTTTTTTTTTTTT
	??		   |
	???		   '---> / P.out (dn) ________*______cTTTTTTTTTTTTTTT
			   .<--' / L.in  (dn) ________*______ TTTTTTTTTTTTTTT
			   |
			   '---> / M.out (dn) ________*__bTTTTTTTTTTTTTTTTTTT
				<--- / M.in  (dn) ________*______CTTTTTTTTTTTTTTT

	  P/Lat		---> / S.L   (up) ________*__________ * dTTTTTTTTT
*//// ======================================================================

/* S P E A K					speak==1:
				  U1 U2 U3 U4	 sec	shares
		  .--o--.__SW - MAX
	E	  |L| in|___|in |L|/
	 P	    		`--o--'		 P		Primary (valueGet)
					   |
	L	    		.--o--.		L		Latch
	     /d\F	   <|$|>in|  /A\E		ext clk (Loads $)
		  mode:____lPre	  |
	     /C\D [in		  v
	M	     o[    O	  |		M
	     /b\c [L|<--.	  |
					|	  /
		 /B\F		|in |L|  /a\e
	S				 --o--		S		Secondary

    __OUTSIDE__     _INSIDE_
										    		    cp             cp*
	from P/Lat	---> / L.in   (dn) ____ATTTTTTTTTTTTTTTT*TTTTE_________*_____
					    |
	  to SEC	<--- / S.Lout (dn) ____aTTTTTTTTTTTTTTTT*TTTTe_________*_____
	    |                              '-----. underlings play
	from SEC	---> / S.in   (up) __________BTTTTTTTTTT*TTTTTTTF______*_____
					    |				done : start play
	  to MODE	<--- / M.Lout (dn) __________bTTTc______*______________*_____
	    | 							end play '---.
	from MODE	---> / M.in   (up) ______________CTTTD__*______________*_____
	   				    |                we are next :
	to L.lPre	<--- / L.lPre (up) __________________dTT*TTTTTTTf______*_____
												<clock> : we are hot
	to L.$out	<--- / L.$    (up) _____________________*TTTTTTTTTTTTTT*_____
																			*/

@end
