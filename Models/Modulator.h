// Modulator.h -- Multiplies upgoing and downgoing signals by control C2014PAK


/*! Concepts:
	Have	-- flows up from P to S, modulated by C.in
	Want	-- flows down from S to P, modulated by C.in
	Control	-- .in modulates per above. 
		151213 added: .out is product of P.in and S.in.  
			Now it is symmetric: interchange P, S, and M --> no change in functionality



			[___ _|L]
				o

		P:	 ___o___
			[L|     ]
			 |		|
			 *	   / \
			s t	  /   \
		   /   \ /	   \
		  /			    \
		 /	   / \	     \
		+----- --- -----. \
		|	  |	   \	 \ \
		|	  p t---+	  s p
		|	   *	|	   *
		|	   |	|	   |
		[___ _|L]	[___ _|L]
	S:		o			o		:T

		 ___o___	 ___o___
		[L|     ]	[L|     ]
 */

#import "Atom.h"
@interface Modulator : Atom

atomsPortDefinitions(sPort,		S, false)
atomsPortDefinitions(tPort,		T, false)
//- (Port *) modulator;

@end
