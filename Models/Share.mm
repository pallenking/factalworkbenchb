//  Share.mm -- Maintains one channel of the plug protocol to a Splitter C2013PAK

/* to do:
 */

#import "Share.h"
#import "Brain.h"

#import "WriteHead.h"	// for Hamming birth
#import "FactalWorkbench.h"	// for Hamming birth

#import "Port.h"		// for Sequence only
#import "Atom.h"

#import "Splitter.h"
#import "Link.h"
#import "Leaf.h"
#import "View.h"
#import "Common.h"
#import "NSCommon.h"
#import <GLUT/glut.h>
#import "GlCommon.h"

//##############################################################################
//##############################################################################
//################################## Share: ####################################
//##############################################################################
//##############################################################################
@implementation Share /////////////// The common parts//////////////////////////

#pragma mark x. Numerics
//####################### DEFAULT SHARE NUMERICS ##############################
 //-- combine: (local DOWN)-- Default: //
+ (void) combinePre:(Splitter *)sp; {
	sp.a1			= 0.0;				// Default: set to 0 before scan
}
+ (bool) combineNext:(Splitter *)sp val:(float)val; {
	sp.a1		   += val;				// Default: a1 is a SUM
	return FALSE;						// Never declare a winner
}
+ (void) combinePost:(Splitter *)sp; {	/* Default: do nothing afterward */
	if (isNan(sp.a1) or std::isinf(sp.a1))
		sp.a1		= 0.0;				// Default: remove nan's and inf's
}

 //-- distribute: (local UP)--//
- (float) bidOfShare;		{
	return self.connectedTo.value;			// Default: distribute proportionately according to want
}
+ (float) bidTotal:(Splitter *)sp; {
	float rv		= 0;
	for (Part *p in sp.parts)
		if (Share *sh = coerceTo(Share, p))
			rv	   += sh.bidOfShare;
	return rv;								// Default: denominator
} //##########################################################################

#pragma mark 9. 3D Support
// \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\    //
//  \\ \\ \\ \\ \\ \\ \\ \\ \ 3D Support\\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\  //
//   \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\//
- (Bounds3f) gapAround:(View *)v;	{
	float r4		= self.brain.bitRadius/4;		// make something oddly small
	return Bounds3f(-r4, -2*r4, -r4,    r4,  0,  r4);
}

- (Hotspot) portsHotspot; {
	Hotspot rv	= zeroHotspot;	// Hotspot starts at zero of Share's coord sys

	 // look at particular type of share:
	SplitterSkinSize sss = [self.class splitterSkinSizeIn:mustBe(Splitter, self.parent)]; // to compute HOTSPOT
	rv.radius		= sss.radius;
	rv.inset		= sss.inset;
	rv.center.y	   -= sss.inset;	// move center back by inset

	return rv;
}

+ (SplitterSkinSize) splitterSkinSizeIn:(Splitter *)sp;	{	//{width, height, deapth, inset, radius}
		// inset is from center to Y-most extension of Port keepout region
	float r			= Brain.currentBrain.bitRadius;
	return {4*r,3*r,4*r,   r, 1.5*r };
}

#pragma mark 11. 3D Display
///// // // // // // // // // // // // // // // // // // // //
//// // // // // // // // 3D Display  // // // // // // // //
/// // // // // // // // // // // // // // // // // // // //
+ (void) drawSplitterSkinInView:(View *)v con:(RenderingContext *)rc; {
	panic(@"this kind has no skin defined");
}

@end


#pragma mark - Share Subclasses:

@implementation Broadcast //########################################################
 //-- distribute: (local UP)--//  // all distributions get the full total
- (float) bidOfShare;				{		return 1;			}
+ (float) bidTotal:(Splitter *)sp;	{		return 1;			}
 //-- draw: --//
bool  bigBall	= 0;
float ballMag   = bigBall? 1.5: 1.0;
float extraVert = bigBall? 0.5:   0;
+ (SplitterSkinSize) splitterSkinSizeIn:(Splitter *)sp;	{
	float r = Brain.currentBrain.bitRadius;
	return {2*r*ballMag, 2*r*ballMag+r*extraVert, 2*r*ballMag,  r, r};
}
+ (void) drawSplitterSkinInView:(View *)v con:(RenderingContext *)rc; {
	float r = Brain.currentBrain.bitRadius;
	float bitRadius	= r * ballMag;
	glTranslatef(0, - bitRadius, 0);		//wtf!
//	glutSolidSphere(bitRadius*0.98, 64, 64);// (.98 to remove scallops with Port)
	glutSolidSphere(bitRadius*0.98, 16, 16);// (.98 to remove scallops with Port)
}
@end //#########################################################################


@implementation MaxOr //########################################################
 //-- combine: (local DOWN)--//
+ (void) combinePre:(Splitter *)sp; {
	sp.a1			= -INFINITY;
}
+ (bool) combineNext:(Splitter *)sp val:(float)val; {
	sp.a1			= max(sp.a1, val);

	bool idle		= val<0.01 and sp.onlyPosativeWinners;//special idle mode when no substantial winner
			 // 160427 HACK: special mode: not active
			  // 161020 -- consider replacing with squelch
	bool rv			= (sp.a1==val) and !idle;	//  winner: if we are at max
	return rv;
}
 //-- draw: --//
+ (SplitterSkinSize) splitterSkinSizeIn:(Splitter *)sp;	{
	float r = Brain.currentBrain.bitRadius;		// use Splitter params for Share!!
	return {4*r,1.5*r,4*r,  1*r, 2*r};
//	return {6*r,1.5*r,6*r,  1*r, 2*r};	// whdir (width, height, deapth, inset, radius)
}
+ (void) drawSplitterSkinInView:(View *)v con:(RenderingContext *)rc; {
	Splitter *sp = mustBe(Splitter, v.part);
//	SplitterSkinSize sss = [MaxOr splitterSkinSizeIn:(Splitter *)sp];
	float r = Brain.currentBrain.bitRadius;
	glTranslatef(0, -0.5*r, 0);
	glRotatef(90, 1, 0, 0);
	glScalef(1.0, 1.0, 0.33);
	myGlSolidHemisphere(2*r, 0.5, 16, 16);
//	myGlSolidHemisphere(sss.width/2, 0.5, 16, 16);
}
@end //#########################################################################

@implementation MinAnd //#######################################################
 //-- combine: (local DOWN)--//
+ (void) combinePre:(Splitter *)sp;	  {
	sp.a1			= INFINITY;
}
+ (bool) combineNext:(Splitter *)sp val:(float)val; {
	sp.a1			= min(sp.a1, val);
	bool rv			= sp.a1==val;
	return rv;
}
 //-- draw: --//
+ (SplitterSkinSize) splitterSkinSizeIn:(Splitter *)sp;	{
	float r = Brain.currentBrain.bitRadius;		// use Splitter params for Share!!
	return {3*r,2*r,3*r,  0.5*r, 0.5*r};	//4*r,2*r,4*r
}
+ (void) drawSplitterSkinInView:(View *)v con:(RenderingContext *)rc; {
	float r = Brain.currentBrain.bitRadius;
	glScalef(1.5*r, -1.3*r, 1.5*r);					// makes no gap to plug
	glRotatef(-90, 1,0,0);
	myGlSolidHemisphere(1.0, 0.5, 16, 16);
}
@end //#########################################################################

@implementation Bayes //***#####################################################
   // The upward sensation goes proportionately to the normalized downward desire
  //   Acc1 is sum of downward vals...
 //-- distribute: (local UP)--//

 //-- draw: --//
+ (SplitterSkinSize) splitterSkinSizeIn:(Splitter *)sp;	{
	float r = Brain.currentBrain.bitRadius;		// use Splitter params for Share!!
	return {2*r,2*r,2*r, r, r};
}
+ (void) drawSplitterSkinInView:(View *)v con:(RenderingContext *)rc; {
	float r = Brain.currentBrain.bitRadius;
//	float r	= brain.bitRadius;
	glTranslatef(0, -.5*r, 0);
	glScalef(1.5,1,1.5);
	glutSolidCube(r);
}
@end //#########################################################################

@implementation HammingMem //###################################################
+ (Part *) conceiveBabyIn:(WriteHead *)wh evi:evi con:con; {

	 // Our Leaf has a Chain from hook:
	Leaf *consLeaf = [con enclosedByClass:[Leaf class]];
	if (consLeaf.hook)				// If a term to chain from past
		[evi addObject:consLeaf.hook];	// Add extra term to Hamming

XX	Part *baby = [super conceiveBabyIn:wh evi:evi con:con];

	 // Make an Ago on the side,
	id ago			 	= anAgo(0, @{@"P":baby});
	[wh.actor addPart:ago];
	consLeaf.hook		= ago;				// hang it on hook

	return baby;
}
@end //#########################################################################

@implementation Hamming //####################################################
+ (Part *) conceiveBabyIn:(WriteHead *)wh evi:evi con:con; {

	  // Build this new element inside actor
	 //
XX	Part *baby 			= aHamming(con, evi, @{flip});

	 // Insert baby in its Actor's:
	[wh.actor addPart:baby];
	wh.baby 			= baby;				// Restriction: 1 baby per WriteHead.  BAD!!

	return baby;
}

float cut = 0.9;
 //-- combine: (local DOWN)--//		// Combine:  (SUMi (Xi - cut)) + cut
+ (bool) combineNext:(Splitter *)sp val:(float)val;  {
	float delta		= val - cut;
	sp.a1		   		+= delta;
	return FALSE;		// Never declare a winner; all distributions are 100%
}
+ (void) combinePost:(Splitter *)sp; {
	sp.a1 += cut;
}
 //-- distribute: (local UP)--//  // all distributions get the full total
- (float) bidOfShare;						{		return 1;			}
+ (float) bidTotal:(Splitter *)sp;			{		return 1;			}
 //-- draw: --//
+ (SplitterSkinSize) splitterSkinSizeIn:(Splitter *)sp;	{
	float r = Brain.currentBrain.bitRadius;		// use Splitter params for Share!!
	return {2*r,3*r,2*r, 1.5*r, r};
}
+ (void) drawSplitterSkinInView:(View *)v con:(RenderingContext *)rc; {
	float r = Brain.currentBrain.bitRadius;
	glTranslatef(0, -2, 0);
	glRotatef(-90, 1,0,0);
	glutSolidCone(r, 2*r, 8, 8);		// radii, length, seg, seg
}
@end //#########################################################################


@implementation Multiply //#####################################################
 //-- combine: (local DOWN)--//
+ (void) combinePre:(Splitter *)sp; {
	sp.a1			= 1.0;
}
+ (bool) combineNext:(Splitter *)sp val:(float)val; {
    sp.a1		   *= val;		// a1 is multiply (*=)
	return			false ;//sh.a2 == contrib;
}
 //-- distribute: (local UP)--//
float bidParam = 3;		//
 //-- draw: --//
+ (SplitterSkinSize) splitterSkinSizeIn:(Splitter *)sp;	{
	float r = Brain.currentBrain.bitRadius;		// use Splitter params for Share!!
	return {4*r,3*r,4*r,  2*r,2*r};
}
+ (void) drawSplitterSkinInView:(View *)v con:(RenderingContext *)rc; {
	float r = Brain.currentBrain.bitRadius;
	glTranslatef(0, -1.5*r, 0);
	glScalef(2.3*r, 1.6*r, 2.3*r);				// (rather ad-hoc)
	glRotatef(-90, 1,0,0);
	glRotatef(55, 1,0,0);
	glRotatef(45, 0,0,1);
	glutSolidTetrahedron();
}
@end //#########################################################################

@implementation KNorm //########################################################
float k = 1.0;	// build this out
 //-- combine: (local DOWN)--//
+ (bool) combineNext:(Splitter *)sp val:(float)val; {
	sp.a1 += powf(val, k);
	return false;
}
+ (void) combinePost:(Splitter *)sp; {
	sp.a1 = powf(sp.a1, 1.0/k);
}
 //-- draw: --//
+ (SplitterSkinSize) splitterSkinSizeIn:(Splitter *)sp;	{
	float r = Brain.currentBrain.bitRadius;		// use Splitter params for Share!!
	return {4*r,3*r,4*r, 1.2*r, 1.2*r};
}
+ (void) drawSplitterSkinInView:(View *)v con:(RenderingContext *)rc; {
	float r = Brain.currentBrain.bitRadius;
	glTranslatef(0, -1.05*r, 0);						// Flat ellipse
	glScalef(2.0, 1.1, 2.0);
	glutSolidSphere(r, 16, 16);
}
@end //#########################################################################


@implementation Sequence //#####################################################
 // This presumes numbers are <1.0 for now
+ (void) combinePre:(Splitter *)sp;	{	panic(@"");							}
+ (bool) combineNext:(Splitter *)sp val:(float)val;
								{		panic(@"");	return FALSE;			}
- (void) simulateDown:(bool)downLocal; {
	Splitter *parent		= mustBe(Splitter, self.parent);
	assert(self.class==parent.shareClass, (@"paranoia"));

/* Splitter:								Events: A->a, B->b, ...H->h
		 .----------------> 1.up --v	->__aTTTb___________________________
		 | .--------------< 1.dn TASK1	<-______BTTTC_______________________
		 | |    .--------->	2.up --v	->__________cTTTd___________________
		 | |    | .-------<	2.dn TASK2	<-______________DTTTE_______________
		 | |    | |    .-->	3.up --v	->__________________eTTTf___________
		 | |    | |    | .<	3.dn TASK3	<-______________________FTTTG_______
		 | |    | |    | |
=========^=v====^=v====^=v=== Splitter
=        | |    | |    | |  =
=        |1|    |2|    |3|  =
=    A B | |C D | |E F | |  =		Shares S1, S2, and S3
=   -. ,-|-+. ,-|-<. ,-|-<  =
=  | /=\ |  /=\ |  /=\ | |  =				DETAIL: FLIP FLOP
=  | S=R |  S=R |  S=R | |  =	(/S) Leading edge sets     (\R) Trailing edge resets
=  | ==Q-'  ==Q-'  ==Q-' |  =						   /=\
=  |					 |  =						   S=R
=  |        G H          |  =						   ==Q
=  +--------\=/----------'  =						        Q is output
=  |        R=S             =
=  |        ==Q             =
=  | 		  |          	=
=  |0.--------'			    =		Share 0
=  | |						=
===^=v=======================
   | |
   +----------------------< 0.up		<-__ATTTTTTTTTTTTTTTTTTTTTTTTTTTH___
   | +--------------------> 0.dn		->__________________________gTTTh___
   | |

At time   At Share      We detect		and do    with
Event:  Processed by:  inVar edge	 	action:   var:		Comment:

	A: in 1 going up	 0.up /  --->   set   / 1.up  :a	Primary Starts TASK1

	B: in 1 going down	 1.dn /  --->   clear \ 1.up  :b	TASK1 completion
	C: in 1 going down	 1.dn \  --->   set   / 2.up  :c	Share 1 Starts TASK2

	D: in 2 going down	 2.dn /  --->   clear \ 2.up  :d	TASK2 completion
	E: in 2 going down	 2.dn \  --->   set   / 3.up  :e	Share 2 Starts TASK3

	F: in 3 going down	 3.dn /  --->   clear \ 3.up  :f	TASK3 completion
	G: in 3 going down	 3.dn \  --->   set   / 0.dn  :g	declare our completion

	H: in 0 going down	 0.up \  --->   clear \ 0.up  :a	completion acknowledged
Notes:		/ is rising edge   \ is falling edge
 */

	 // Our share number in parent's parts:
	NSInteger selfNo		= [parent.parts indexOfObject:self];
	assert(selfNo!=NSNotFound, (@"primaryPort should always be found in parts"));

	if (!downLocal) {		// === UP:
		if (selfNo == 1) {		// First Share output (up) is set by primaryPort:
		     // Our parent's primary port:
			Port *primaryPort	= mustBe(Port, parent.pPort);
//			Port *primaryPort	= mustBe(Port, parent.primary);
			Port *primaryInPort	= primaryPort.connectedTo;

			float valuePrev = primaryInPort.valuePrev;	// self's up before
			float value		= primaryInPort.valueGet;	// self's up now

			if (valuePrev<0.5 and value>=0.5)			// RISING EDGE (Event A)
				self.valueTake = 1.0;						// START: / Share 1 output
			if (valuePrev>=0.5 and value<0.5) 			// FALLING EDGE (Event H)
				primaryPort.valueTake = 0.0;				// set next port
		}
		else					// other Share outputs (up)
			;					//		were left there by previous Share's down
	}
	else {					// === DOWN
		 // Output number for next Share
		NSInteger outNo = selfNo+1<parent.parts.count? selfNo+1: 0;
		Port *outPort	= [parent.parts objectAtIndex:outNo];

		Port *inPort	= self.connectedTo;
		float valuePrev = inPort.valuePrev;				// self's down before
		float value		= inPort.valueGet;				// self's down now

		if (valuePrev<0.5 and value>=0.5)				// RISING EDGE (Events C,E)
			self.valueTake   = 0.0;							// clears our output value
		if (valuePrev>=0.5 and value<0.5)				// FALLING EDGE (Events B,D,F)
			outPort.valueTake = 1.0;						// sets the next port output
	}
}
 //-- draw: --//
- (void) placeIntoView:(View *)v; {
	Part *parent = self.parent;
	NSInteger bitNo = [parent.parts indexOfObject:self] - 1;
	NSInteger noBits = parent.parts.count - 1;

	Bounds3f splitterP = [parent gapAround:v];
	Vector3f bitSpot = splitterP.centerYTop();	//Vector3f(1*(bitNo-1),4,0);//zeroVector3f;

	 // Distribute about center.
	bitSpot.x += bitNo - noBits/2.0 + 0.5;

	v.position = [self suggestedPositionOfSpot:bitSpot];
}
+ (SplitterSkinSize) splitterSkinSizeIn:(Splitter *)sp;	{
	float r = Brain.currentBrain.bitRadius;		// use Splitter params for Share!!
	return {4*r,3*r,4*r,   0, 0 };
}

+ (void) drawSplitterSkinInView:(View *)v con:(RenderingContext *)rc; {
	float r = Brain.currentBrain.bitRadius;
	glTranslatef(0, -1.25*r, 0);						// (rather ad-hoc)
	glRotatef(90, 0,1,0);
	myGlSolidCylinder(1.25*r, 4*r, 16, 1);				// (radius, length, ...)
}
@end //#########################################################################


@implementation Bulb //########################################################
 /// A change in value -->  resize / rebound  event
//+ (void) splitter:(Splitter *)splitter setToValue:(float)total; {
//	splitter.brain.boundsDirty = true;
//}
+ (float) radiusForVal:(float)val {
	float v				= min(max(val, 0.0), 1.0);
	return 2.0*v + 1.0;
}

 ////// default numerics
+ (SplitterSkinSize) splitterSkinSizeIn:(Splitter *)sp;	{
	float r 			= [self radiusForVal:sp.pPortIn.value];
	return {r*2, r*2, r*2,   r, r};	// w h d  i r
}

+ (void) drawSplitterSkinInView:(View *)v con:(RenderingContext *)rc; {
	Brain *brain 		= Brain.currentBrain;
	Atom *parent 		= mustBe(Atom, v.part);
	Port *inPort 		= parent.pPortIn;
	float r 			= brain.bitRadius;
	float rad			= [self radiusForVal:inPort.value];		// 1.0<= rad <=4.0

	glPushMatrix();
		rc.color = inPort.colorOfValue;
//		glTranslatefv(v.bounds.centerYBottom()); // to the top center of splitter
//		glTranslatef(brain.displayOffsetX, 1-rad, brain.displayOffsetZ);
		glTranslatef(brain.displayOffsetX, -rad, brain.displayOffsetZ);	// makes Bulb GOOD
	//	glTranslatef(brain.displayOffsetX, rad-2, brain.displayOffsetZ);// makes Branch<Bulb GOOD
		glutSolidSphere(rad, 16,16);
	glPopMatrix();

	 // Label if active
	if (inPort.value >= 0.5) {
		id name = parent.name;
		if (Leaf *leaf = [parent enclosedByClass:@"Leaf"])
			name	= leaf.name;

		glPushMatrix();
			rc.color = colorBlue4;
			glTranslatefv(v.bounds.centerYTop());
			Vector3f inCameraShift = Vector3f(0, 0, 0);			// in "facing camera" coordinates
			Vector2f spot2labelCorner = Vector2f(1, 0);			// box center --> llc
			myGlDrawString(rc, name, -1, inCameraShift, spot2labelCorner);
		glPopMatrix();
	}
}
@end //#########################################################################


//##############################################################################
