// Net.h -- Semantic Network C2015PAK

// 160814: moved, so Nets could have Ports and other properties of Atom

#import "Atom.h"
@interface Net : Atom

	@property (nonatomic, assign) Vector3f minSize;

- (Port *)			enable;

@end
