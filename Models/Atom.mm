//  Atom.mm -- a node in the reactive semantic network -- C2013PAK

#import "Atom.h"
#import "Share.h"	// hack - intertwined
#import "Link.h"	// hack - intertwined
#import "Splitter.h"// hack - intertwined
#import "View.h"
#import "Bundle.h"
#import "TimingChain.h"
#import "Path.h"
#import "Actor.h"

#import "Brain.h"

#import "Common.h"
#import "NSCommon.h"
#import "Id+Info.h"
#import "GlCommon.h"
#import <GLUT/glut.h>

@implementation Atom

#pragma mark 1. Basic Object Level
- (void) dealloc {
	self.bandColor			= nil;

	[super dealloc];
}


#pragma mark 2. 1-level Access
- (Port *) getPortNamed:(NSString *)portName down:(int)down; {
	Port *rv = 0;
	for (Port *b_ in self.parts)
		if (Port *b = coerceTo(Port, b_))
			if ([b.name isEqualToString:portName]) {
				assert(rv==0, (@"More than two %@-Ports", portName));
				assert(!b.flipped==!down, (@"Port '%@' must be facing %s",
										portName, b.flipped? "down":"up"));
				rv = b;
			}
	return rv;
}

#pragma mark  3. Deep Access
- (id) deepMutableCopy; {					Atom *rv = [super deepMutableCopy];

	rv.bandColor		= self.bandColor;
	rv.proxyColor		= self.proxyColor;
	rv.postBuilt		= self.postBuilt;
	return rv;
}


#pragma mark  4a Factory Access						// Methods defined by Factory
- (id) atomsDefinedPorts;	{				id rv = @{}.anotherMutable;

	/// p = primary, c = always create, d = down

	rv[@"P"]		= @"pcd";	// Primary	 (always create)
	rv[@"S"]		= @"pc ";	// Secondary (always create) 	// @"p  " (create if needed)
	return rv;
}

atomsPortAccessors(pPort,		P, true)
atomsPortAccessors(sPort,		S, false)


#pragma mark 4. Factory
- (id) build:(id)info;	{					[super build:info];
 
//! xzzy1 Atom::		1. "bandColor":<Port>		-- Port whose value determines the Bundle's band color
//! xzzy1 Atom::		Create the Ports in atomsDefinedPorts with the 'c' ability
//! xzzy1 Atom::		  E.g: P, pri,evi, S,sec, con, T, E (B, U?)

	  /// Construct EARLY Required Ports (with an ability 'c') here early,
	 ///   during building (not wiring). Wiring is handled later
	NSDictionary *atomsDefinedPorts = mustBe(NSDictionary, self.atomsDefinedPorts);
	for (NSString *portName in atomsDefinedPorts) {		// FOR ALL PORT TYPES:
		NSString *portAbility = atomsDefinedPorts[portName];

		if ([portAbility containsString:@"c"]) { /// ALWAYS CREATE those with 'c' NOW, EARLY
			Port *aPort = [self addPart:[Port another]];
			aPort.flipped = [portAbility containsString:@"d"];
			assert([portName length]!=0, (@"atomsDefinedPorts yields no name"));// 180805 perhaps unneeded
			aPort.name = portName;
		}
	}
/*!////////////////// 160200:
						initialFixedPorts	has	gatherWiresInto
Atom:					-			(initialFixedPorts)	PSTE			primary
		Mirror			P			-			-				-
		GenAtom:	 	P			-			-				-
		BundleTap		P			-			-				-
		Known			PST			-			-				unknown, known
		Modulator		PST			-			-				-
		Previous		PST			-			-				sCurPort, tPrevPort
		Rotator			PST			-			-				first, second
		Splitter:		P			U			U,sec			unknown
	Link:				PS			-			na				-
	WriteHead:			-			-			-				-

	not Port, Net, Bundle, nor Actor?
 *////////////////////

	if (id bandColor = [self takeParameter:@"bandColor"])
		self.bandColor = bandColor;

	return self;
}

- (Part *) resolveInwardReference:(Path *)path openingDown:(bool)downInSelf except:exception; {
	mustBe(Path, path);

	if ([path atomNameMatches:self]) {		// matches as Atom, ignoring Port
		Part *rv = nil;

		if (path.namePort.length) {				// if named Port is specified

			  // Search all existing component Ports:
			 //
			for (Port *port in self.parts) {
				if (coerceTo(Port, port))			// for all Ports in our Atom
					if ([port.name isEqualToString:path.namePort]) {
						assert(rv==0, (@"multiple Ports named '%@' found", path.namePort));
						rv = port;						// matches subcomponent
					}
			}
			 // If not found, perhaps it could be built
			if (rv == 0)
				if (NSString *portAbility = [self atomsDefinedPorts][path.namePort]) {
					Port *aPort = [self addPart:[Port another]];
					aPort.flipped = [portAbility containsString:@"d"];
					aPort.name = path.namePort;
					rv = aPort;						// return newly created Port
				}
		}
		else
			rv = self;							// matches the Atom

		 // report and return results:
		if (rv) {
			if (self.brain.logTerminalSearching)
				[self logEvent:@"   MATCHES Inward check: %@", rv.fullName];
			return rv;
		}
	}

	if (self.brain.logTerminalSearching)
		[self logEvent:@"   FAILS   Inward check"];

	return [super resolveInwardReference:path openingDown:downInSelf except:exception];
}

  // At some later time, after all model components have been realized
 //   GATHER and REMOVE its WIRES:
- (id) gatherWiresInto:(id)wirelist; {		[super gatherWiresInto:wirelist];

	 /// Go through all self.parameters, looking for potential wires to add,
	id retiredKeys					= @[].anotherMutable;
	for (id rawName in self.parameters) {

		   // Remove modifiers from signal name   E.g: "foo=!"
		  //	--> stripedName: 'foo', modifiers: '=', '!'
		int l=(int)[rawName length]-1;
		id sourceModifiers = @"";		// none detected yet!

		 /// search backward over any trailing modifier characters [=%^!@]
		for (char c=0; l>=0 and (c=[rawName characterAtIndex:l]); l--) {
			id cStr = [@"" addF:@"%c", c];	// c is last char, cStr is NSString
			 // Legal Modifiers:
			id modifierChars = @{@"=":@1, @"%":@1,  @"^":@1,  @"!":@1,  @"@":@1, };
			if (!modifierChars[cStr])		// If modifier char is defined
				break;							// no more modifier characters

			 // Found modifier; accumulate in sourceModifiers:
			sourceModifiers=[sourceModifiers addF:@"%c", c];
		}
		id stripedName = [rawName substringToIndex:l+1]; // remove trailing options

		 // If a (stripped) key is known, add a wire for it according to its abilities
		if (NSString *portAbility	= self.atomsDefinedPorts[stripedName]) {
			[retiredKeys addObject:rawName];
			id portsTarget			= self.parameters[rawName];

			 /// If this is a Proper Port, make it if it is missing
			if ([portAbility containsString:@"p"]) {
				// this is a "Proper" port.
				if (Part *p = [self partNamed:stripedName])
					// Check existing part with this name
					assert(coerceTo(Port, p), (@"My part named '%@' should be a Port", p.name));
				else {
					// must make new Port
					Port *aPort		= [self addPart:[Port another]];
					aPort.name		= stripedName;
					aPort.flipped	= [portAbility containsString:@"d"];
				}
			}

			 // Selfs Port Opens Down IN SELF, according to atomsDefinedPorts:
			bool spodInSelf			= [portAbility containsString:@"d"];
			bool todInSelf			= !spodInSelf;// Target Opens Down IN SELF

			 // Enqueue wire:
XX			[self addWireToList:wirelist
						 source:stripedName	  sourceModifiers:sourceModifiers
						 target:portsTarget			todInSelf:todInSelf
													  purpose:rawName];
		}
	}
	[self.parameters removeObjectsForKeys:retiredKeys];

	return wirelist;
}

#pragma mark 6. Navigation

- (Port *) getOpenPort_openingDown:(bool)localDown; {

	 // See if there is already an open port there.
	Port *candidatePort = [self getPortFacingDown:localDown];
	if (!candidatePort) {
		candidatePort = [self addPart:[Port another]];
		candidatePort.flipped = localDown;
	}

	mustBe(Port, candidatePort);				// similarOpenPort of non-Port
	return [candidatePort similarOpenPort];
}

- (Port *) getPortFacingDown:(bool)downInSelf; {
	Port *rv = nil;
	for (Port *port in self.parts)			// Look through my parts
		if (coerceTo(Port, port))				// If it's a Port
			if (port.flipped == downInSelf) {		// With correct facing?
				if (!rv)								// and the First one?
					rv = port;								// Capture it
				else
					[self logEvent:@"IGNORING bitFacing:%d --  %@, already FOUND %@",
										downInSelf, port.fullName, rv.fullName];
			}
	return rv;
}
- (void)		portCheck;		{	/* DEFAULT: do not delete */				}

#pragma mark 7-. Simulation Actions

- (void) reset; {									[super reset];
	
	/// Resolve symbolic references, now that full machine is built.
	if (NSString *prevBandColor = coerceTo(NSString, self.bandColor)) {
		
		// 170402PAK: There is probably a better (more modern) way than this:
		[self searchInsideFor:^(Part *m) {	//##BLOCK
			if (Port *p = coerceTo(Port, m))	// look for Port named bandColor
				if ([p.fullName containsString:self.bandColor]) {
					
					self.bandColor = p;	// replace String with Port
					
					return true;
				}
			return false;	}];
		assert(self.bandColor, (@"Failed to resolve bandColor '%@' in Bundle '%@'",
								prevBandColor, self.fullName));
	}
}

#pragma mark 9. 3D Support
- (Matrix4f) positionOfPort:(Port *)port inView:(View *)bitV; {
	
	if ([port.name hasPrefix:@"P"])
		return [port suggestedPositionOfSpot:zeroVector3f];

	if ([port.name hasPrefix:@"S"])
		return [port suggestedPositionOfSpot:Vector3f(0, 3, 0)];

	panic(@"Port '%@' has no name", port.fullName);
	return Matrix4f();
}

#pragma mark 11. 3D Display
///// // // // // // // // // // // // // // // // // // // // // // // //
//// // // // // // // // // // // // // // // // // // // // // // // ///
/// // // // // // // // // // 3D View  // // // // // // // // // // // /
///// // // // // // // // // // // // // // // // // // // // // // // //
//// // // // // // // // // // // // // // // // // // // // // // // //
- (void) boundIntoView:(View *)v; {				[super boundIntoView:v];

	Bounds3f gapAround = [self gapAround:v];
	v.bounds = v.bounds >> gapAround;		// extend margins

 // place Ports of subviews a second time, this time with correct v.bounds
// removed 160824 returned
	for (View *portV in v.subViews)
		if (coerceTo(Port, portV.part))
XR			[portV.part placeIntoView:portV];
}

- (void) sendPickEvent:(FwEvent *)fwEvent toModelOf:(View *)v; {
	NSPrintf(@"Atom :: Picked '%@' (%d-clicks)\n",self.fullName, fwEvent->clicks);

	NSPrintf(@"picked '%@' Event( mousePosition=%@ delta %@\n", self.name,
		pp(fwEvent->mousePosition), pp(fwEvent->deltaPosition));

	assert(v.part==self, (@"should be!"));

	[super sendPickEvent:fwEvent toModelOf:v];
}

- (void) drawAtomicInView:(View *)v context:(RenderingContext *)rc; {

	 /// Draw Atom's Band Color and String
	if (Port *bandPort = coerceTo(Port, self.bandColor)) {

		float radius	= 1.1;
		float height	= 0.1;
		
		if (bandPort.value > 0.5) {		// banded and ON (>0.5)
			 // BIGGER:
			radius		= 1.33;
			height		= 1.0;
			
			// DISPLAY NAME:
			glPushMatrix();
			glTranslatefv(v.bounds.center());
			Vector3f inCameraShift = Vector3f(0.5, 0.0, 0);
			Vector2f spotPercent = Vector2f(0., 0.);
			
			rc.color = colorBlue;	//colorYellow;
			myGlDrawString(rc, self.name, -1, inCameraShift, spotPercent);
			glPopMatrix();
		}

		// DISPLAY BAND (with size and color determined above)
		rc.color	= bandPort.colorOfValue;	// banded color
		glPushMatrix();
			glRotatef(90, 1,0,0);
			myGlSolidCylinder (radius,	   height,	   16,	   1);
		glPopMatrix();
	}
	[super drawAtomicInView:v context:rc];
}


- (void) drawFullView:(View *)v context:(RenderingContext *)rc; {

	if (self.brain.displayAtomNames) {
		glPushMatrix();
			glTranslatefv(v.bounds.center());
			Vector3f inCameraShift = Vector3f(0.0, 0.0, 3);	// in "facing camera" coordinates
			Vector2f spot2labelCorner = Vector2f(0.5, 0.5);	// box center --> llc
			rc.color = colorRed;
			myGlDrawString(rc, self.name, -1, inCameraShift, spot2labelCorner, 3);
		glPopMatrix();
	}

	[super drawFullView:v context:rc];
}

@end
