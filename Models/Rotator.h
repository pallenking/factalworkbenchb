//  Rotator.h -- Connects the Haves and Wants of 3 ports WIP C2015PAK

/*! Concepts:
		primary.have	--> left.have,
		left.want		--> right.have
		right.want		--> primary.want
 */

#import "Atom.h"
@interface Rotator : Atom


    @property (nonatomic, assign) char leftIsFirst;

- (Port *) first;
- (Port *) second;

@end
