// Info.h -- a wrapper for Dictionaries, arrays, or strings C2014 PAK
#ifdef viewOnly
#import "Object_.h"


@interface NSObject /*extended here to*/ (info4id)	// a Category

- (NSString *)		fwStr;
- (NSArray *)		fwArray;
- (NSDictionary *)	fwHash;
- (NSNumber *)		fwNumber;

BOOL isValid(NSInteger location);
- (BOOL)			sayHi;
- (BOOL)			nonNil;
- (BOOL)			isMutable;

- (NSString *)		ppStr;
- (NSString *)		ppStr:indent;
- (NSString *)		ppStrCr;
- (NSString *)		ppStrComma;
- (NSString *)		ppStr:(BOOL)brackets :(int)layers;
- (NSString *)		ppStr:(BOOL)brackets :(int)layers :(int)layers2Cr :(NSString *)indent;
void				pInfo(id info);
void				pInfo(id info, BOOL brackets, int levels, int layers2Cr);


#pragma mark Productions
- doNextTransformation;
- (int) strValence;
@end

@interface NSNumber /*extended here to*/ (info4NSNumber)			// a Category
- (BOOL) attemptMatchTo:target withState:tranState indent:str;
- (void) fireWithTerms:tranState onto:target indent:str;
@end
@interface NSString /*extended here to*/ (info4NSString)			// a Category
- (BOOL) attemptMatchTo:target withState:tranState indent:str;
- (void) fireWithTerms:tranState onto:target indent:str;
- (NSString *) indentMore;
@end
@interface NSArray /*extended here to*/ (info4NSArray)				// a Category
- (BOOL) attemptMatchTo:target withState:tranState indent:str;
- (void) fireWithTerms:tranState onto:target indent:str;
@end
@interface NSDictionary /*extended here to*/ (info4NSDictionary)	// a Category
- (BOOL) attemptMatchTo:target withState:tranState indent:str;
- (void) fireWithTerms:tranState onto:target indent:str;
 // common hash functions
- (void) pushUniqueKey:key val:val;
- (void) pushUniqueKeysOf:(NSDictionary *)from;

- (void) pushToQueNamed:que object:val;
@end

#endif