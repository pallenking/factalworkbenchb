// Link.h -- an Atom inserted in Port Connections to add visual delay C2010PAK
/*!
	Links are a special kind of Atom, 
		in that they although they have Ports at each of their two ends,
			Links have a separate placement algorithms then other Atoms.
	Links perform two functions:
		1. They delay the propigation of "have"'s and "want"'s by a fixed amount.
		   This is the only delay in the model, as Atoms propigate "have"'s and
		   "want"'s with 0 delay. This simplification could be modified in the future.
		2. Links allow their end Ports to be at different location, aiding 3D construction
		3. Links display "have"'s as green going up, and "want"'s red going down.
 */
/* THINGS TO DO:
	make classes of links.
	allow such a class to declare OFF links as invisible (also con,evi colors)
    improve link display
        optional old rgblack mode               linkOld?
        invisible or line (rgblack) always      linkRadius
        red and green links                     <default>
        invisible or line (rgblack) for zero    linkHide?
        change ellipsoid size                   linkEventSize?
 */
#import "Atom.h"
@class LinkView;

@interface Link : Atom

	@property (nonatomic, assign) float length;
	@property (nonatomic, assign) float minColorVal;
	@property (nonatomic, assign) float maxColorVal;
	@property (nonatomic, retain) NSMutableArray *upConveyor;
	@property (nonatomic, retain) NSMutableArray *downConveyor;


#pragma mark  4a Factory Access						// Methods defined by Factory
	- (Port *) sPort;
	- (Port *) sPortIn;

#pragma mark 11. 3D Display
- (void)		adjustLinksPlaceInView:(LinkView *)v;
- (void)		renderLinkInView:(LinkView *)v with:(RenderingContext *)rc;

#pragma mark 15. PrettyPrint

@end

  ////////////////////////////// Link Segment ///////////////////////////////
 /// define a segment along the length of a link -- C2011 PAK
@interface LinkSegment : HnwObject
    @property (nonatomic, assign) float val;
    @property (nonatomic, assign) float heightPct;
    @property (nonatomic, assign) bool  goingDown;
  // No new methods
@end
