//  Rotator.mm -- Connects the Haves and Wants of 3 ports

/*
 */

#import "Rotator.h"
#import "Brain.h"
#import "FactalWorkbench.h"
#import "Id+Info.h"

#import "View.h"
#import "Common.h"
#import "NSCommon.h"
#import "GlCommon.h"
#import <GLUT/glut.h>

@implementation Rotator

#pragma mark 1. Basic Object Level

#pragma mark 3. Deep Access

- (id) deepMutableCopy; {		Rotator *rv = [super deepMutableCopy];

	rv.leftIsFirst				= self.leftIsFirst;
	return rv;
}

#pragma mark  4a Factory Access						// Methods defined by Factory
- (Port *) first;	{	return self.parts[self.leftIsFirst? 2: 1];		}
- (Port *) second;	{	return self.parts[self.leftIsFirst? 1: 2];		}

- (id) atomsDefinedPorts;	{			id rv = [super atomsDefinedPorts];
//										// probably returns "P"
	rv[@"S"]		= @"pc ";	// sCur;	Secondary Port, value at Current time	rv[@"P"]= @"pcd";	// pPri	Primary Port
	rv[@"T"]		= @"pc ";	// tPrev:	Terciary Port, value at Previous time
	return rv;								
}

#pragma mark 4. Factory 

Rotator *aRotator(id pri, id etc) {
	id info = @{etcEtc}.anotherMutable;
	if (pri)
		info[@"P"] = pri;

	Rotator *newbie = anotherBasicPart([Rotator class]);
XX	newbie = [newbie build:info];
	return newbie;
}

- (id) build:(id)info;	{					[super build:info];

//! xzzy1 Rotator::		1. leftIsFirst	:<bool>

	if (id val = [self parameterInherited:@"leftIsFirst"])
		self.leftIsFirst = [val intValue];
	return self;
}

#pragma mark 8. Reenactment Simulator
///*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//
//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//
////*//*//*//*//*//*//*//*//    Reenactment Simulator   //*//*//*//*//*//*//*//
///*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//
//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//

/*	Canonic Form:		   first: \ \     / / second
								  ^  .___^  v
								   \       /
									\     /
									 \   /
									  ^ v
									  | | primary
 */
- (void) simulateDown:(bool)downLocal; {
	Port *pPort		= self.pPort,		*pPortIn		= self.pPortIn;
	Port *firstPort	= self.first,		*firstPortIn	= firstPort.connectedTo;
	Port *secondPort= self.second,		*secondPortIn	= secondPort.connectedTo;

	if (!downLocal) {					//============: going UP
		if (pPortIn.valueChanged) {
			float valPrev = pPortIn.valuePrev;
			float valNext = pPortIn.valueGet;		//####### from SELF port
			
			firstPort.valueTake = valNext;			//####### to first port
		}
	}

	else {								//============: going DOWN
		if (firstPortIn.valueChanged) {
			float valPrev = firstPortIn.valuePrev;
			float valNext = firstPortIn.valueGet;		//####### from first port
			[self logEvent:@" rotator first port %.2fwas %.2f)", valNext, valPrev];

			secondPort.valueTake = valNext;			//####### to second port
		}
		if (secondPortIn.valueChanged) {
			float valPrev = secondPortIn.valuePrev;
			float valNext = secondPortIn.valueGet;	//####### from PREV port
			[self logEvent:@" rotator port: %.2fwas %.2f)", valNext, valPrev];
			
			pPort.valueTake = valNext;
		}
	}
	[super simulateDown:downLocal];
}

#pragma mark 9. 3D Support
// \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\    //
//  \\ \\ \\ \\ \\ \\ \\ \\ \ 3D Support\\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\  //
//   \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\//
float rotatorRadius = 1.0;
float rotatorWidth  = 3.0;
float rotatorHeight = 3.0;
float rotatorDeapth = 2.0;
float rotatorShift  = 0.5;		// can't be greater than 1!!

- (Bounds3f) gapAround:(View *)v; {
	float xSh=rotatorShift;
	Bounds3f b = Bounds3f(-rotatorWidth/2,           0.0, -rotatorRadius,
						   rotatorWidth/2, rotatorHeight,  rotatorRadius);
	return b;
}

- (Matrix4f) positionOfPort:(Port *)port inView:(View *)bitV; {
	float h				= 1;

	if ([port.name isEqualToString:@"S"]) {			// port part 1 -- SECONDARY
		port.latitude	= -2;
		return [port suggestedPositionOfSpot:Vector3f(-1, rotatorHeight-h, 0)];
	}

	if ([port.name isEqualToString:@"T"]) {			// port part 2 -- CONTROL
		port.latitude	= 2;			//latitudeEquator=4;
		return [port suggestedPositionOfSpot: Vector3f( 1, rotatorHeight-h, 0)];
	}

XR	return [super positionOfPort:port inView:bitV];
}


#pragma mark 11. 3D Display
//// // // // // // // // // // // // // // // // // // // // // // // ///
/// // // // // // // // // // 3D Display  // // // // // // // // // ////
// // // // // // // // // // // // // // // // // // // // // // // // //

- (void) drawFullView:(View *)v context:(RenderingContext *)rc; {
	rc.color = colorPurple4w;
	glPushMatrix();
		glTranslatef(self.brain.displayOffsetX, v.bounds.center().y-0.2, self.brain.displayOffsetZ);
		glPushMatrix();
			glutSolidSphere(1.0, 8, 8);
		glPopMatrix();
	glPopMatrix();

	[super drawFullView:v context:rc];				// PRIMARY
}

- (FColor)colorOf:(float)ratio;		{	return colorPurple4w;/*colorPurple4;*/}//colorOrange

@end
