//  Actor.h -- a Net with known upper context and lower evidence Bundles C2015PAK

/*!
	An Actor is a net with upper EVIdence and lower CONtext Bundles.
	It supports addition of Parts in between these two.

	161205 When an Actor is accessed as a Bundle,
		Its evi (con) Bundle is returned, if accessing from below (above).
 */
#import "Net.h"
@class Bundle, WriteHead, TimingChain;

@interface Actor : Net

	@property (nonatomic, retain) Bundle	*con;		// upper context  information

	@property (nonatomic, retain) Bundle	*evi;		// lower evidence information

	 // Any previousClocks to be routed inside an actor get enabled here
	@property (nonatomic, retain) NSMutableArray *previousClocks;
//	@property (nonatomic, retain) Port		*previousClocksEnable;
//	@property (nonatomic, retain) TimingChain *timingChain;

	@property (nonatomic, assign) char		positionViaCon;	// con (above) involved in positions

	@property (nonatomic, assign) char		viewAsAtom;	// force our content to be Atomic
    @property (nonatomic, assign) char		displayInvisibleLinks;// Ignore link invisibility


#pragma mark  7. Simulator Messages
- (void)			clockPrevious;

#pragma mark 13. IBActions							// [[shift around]]
//- (IBAction)		atomicStateAction:(NSMenuItem *)sender;

#pragma mark 15. PrettyPrint
- (void) printContents;

@end

static const id atomicStateNames = @{@"0":@0, @"1":@1,
			@"OFF":@0, @"atBirth":@2, @"now":@4, @"always":@8};

