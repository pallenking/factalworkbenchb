// Mirror.mm -- an atom which mirrors its input to its output C2014PAK

#import "Mirror.h"
#import "FactalWorkbench.h"

#import "Common.h"
#import "NSCommon.h"
#import "Id+Info.h"
#import <GLUT/glut.h>

@implementation Mirror

#pragma mark  3. Deep Access
- (id) deepMutableCopy; {		Mirror *rv = [super deepMutableCopy];

	rv.gain				= self.gain;
	return rv;
}

#pragma mark  4a Factory Access						// Methods defined by Factory
- (id) atomsDefinedPorts;	{			id rv = [super atomsDefinedPorts];

	[rv removeObjectForKey:@"S"];		// hoakey
	return rv;
}

#pragma mark 4. Factory 
Mirror *aMirror(id pri, id etc) {
	assert(![pri asHash], (@"probably missing worker arg"));
	id info = @{@"P":pri?:@0, etcEtc};

	Mirror *newbie = anotherBasicPart([Mirror class]);
XX	newbie = [newbie build:info];
	return coerceTo(Mirror, newbie);
}

- (id) build:(id)info;	{					[super build:info];

//!	xzzy1 Mirror::		1. gain				:float      -- of loopback N.B: 0 is illegal value
//!	xzzy1 Mirror::		2. offset			:float      -- to out = in * gain + offset

	self.gain 	= [[self takeParameter:@"gain"]   floatValue]?: 1.0;
	self.offset = [[self takeParameter:@"offset"] floatValue]?: 0.0;
	return self;
}

#pragma mark 8. Reenactment Simulator
- (void) simulateDown:(bool)downLocal;  {
	if (!downLocal) {								/////// going UP
		assert(self.parts.count==1, (@"Mirror: %lu plugs illegal", self.parts.count));
		Port *pPort		= self.pPort;
		Port *pPortIn	= self.pPortIn;
		//assert(pPortIn, (@"Mirror has no input"));
		if (pPortIn.valueChanged) {		// ( and do nothing if no pPortIn )
		
			float valNext = pPortIn.valueGet * self.gain + self.offset;

			[self logEvent:@"mirror ===/=\\=/=\\==="];
				  ///////	PUT to my OUTPUT
			pPort.valueTake = valNext;
		}		  ///////
	}
}

#pragma mark 9. 3D Support
// \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\    //
//  \\ \\ \\ \\ \\ \\ \\ \\ \ 3D Support\\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\  //
//   \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\//
- (Bounds3f) gapAround:(View *)v; {
	return Bounds3f(-1.0, 0.0,-1.0,    1.0, 1.0, 1.0);
}

#pragma mark 11. 3D Display
//// // // // // // // // // // // // // // // // // // // // // // // ///
/// // // // // // // // // // 3D Display  // // // // // // // // // ////
// // // // // // // // // // // // // // // // // // // // // // // // //
- (void) drawFullView:(View *)v context:(RenderingContext *)rc; {
	float mirrorThickness = 0.1;
	rc.color = colorPurple4w;
	float l = 4;
	glPushMatrix();
		glTranslatef(0, 1.0 - mirrorThickness + 0.001, 0);// offset from Port
		glScalef(l, 2*mirrorThickness, l);
		glutSolidCube(1.0);
	glPopMatrix();

	[super drawFullView:v context:rc];
}
- (FColor) colorOf:(float)ratio {
	return lerp(colorOrange, colorOrange5, ratio);
}

@end
