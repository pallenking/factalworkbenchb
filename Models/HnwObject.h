//  HnwObject.h -- basic support for objects
/*!
	1. names:				object names, unique name prefixes, search objects by name.
	2. global settings:		accessable by all subclasses
	3. logging facility:	for significant subclass events
	4. printing support:	pretty printing for debugger (gdb, lldb)
 
 
 Debugging Techniques supported:
    1. (C++/OBJC++): display _vptr with .gdbinit macros
    2. OBJC: po command;	(NEEDS UPDATE)
    3. pretty print objects
    4. lookup instantiated object by name, with pn("name").
    5. print current part tree (pm) and view tree (pv)
    6. support for monitoring retain/release actions
    7. debugging view construction (brain.view*Name)
    8. debugging simulations: all; data transfers; per location
 */


/*!  COMMON CODE ABBREVIATIONS (alias Hungarian):
	 In the following, remove all lower case, retain upper case shifted to lower
			e.g. ELemenT --> elt
aNd					ARGument			ARGumentNOmber		BuiLDeR
BuiLDer				CopyOnWrite			DICTionary			DIRection
ELemenT				EVALuateD			EXTernal			Greater or Less Than
INFOrmation			INTernal			LENgth				LeaRNed
MaTriX				NUMber				Number->No			oR
POSitioN			PREFerenceS			PREFerenceS			PROBability
PROPertieS			PROTOtype			Brain				STRing, STRucture
VALueS				VARiable			COLumnS				NICKname
Worker(s)			View				ReturnValue
TO be DOne
CONtext				CONstructor
COMPonentS			EVIdence
World coords


 #####   Hungarian (commonly used short names):  #####
    pp      -- Pretty Print
    str     -- string
 */


#import <Cocoa/Cocoa.h>

 // the string "xzzy1" tags comments
#define xzzy1 // remove string xzzy1 for doxygen

#include "XCodes.h"

@interface HnwObject : NSObject <NSCopying>



    @property (nonatomic, retain) NSString		*name;



#pragma mark 1. Basic Object Level
 /////////////////////////// Create basic part -- no options or wires
- (id)			init;			// builds basic object, with all manditory parts
								// (none of the optional parts)

- (id)			deepMutableCopy;
+ (void)		resetObjectMonitors;

- (NSString *)  name6;
//- (const char*) nameC;
- (HnwObject *)	named:(NSString *)name;
- (NSString *)	classCommonName6;
- (NSString *)	addressStr;


#pragma mark 4. Factory
+ (id)			another;		// (no builder(compiler) involved)

#pragma mark 7-. Simulation Actions
//- (void)		reset;		// Why here in HnwObject?

#pragma mark 15. PrettyPrint
- (NSString *)	ppPart:aux;

- (NSString *)	pp1line:aux;	// returns without a trailing \n

//- (NSString *)  ppSimTime;

- (NSString *)	ppRetainCount;

#pragma mark - Debugging
- (void)		print;			// invokes ppPart
- (void)		p;				// invokes ppPart



////////////// LINKS to degug
void			printRootPart(int mode);
void			printRootView(int mode);

void			ppId(id		object);

void			pp(id		object);
void			pp(id		object,		int mode=0);

void			pp(void	*	object);
void			pp(void *	object,		int mode=0);

void			pp(long int	object);
void			pp(long int	object,		int mode=0);

void *			pn(void);
void *			pn(char *nam);
//NSString *	pp(void *);

@end

extern bool 	logDeallocEvents;

// Old Hungarian, perhaps not even used in this project:
//	coordinate system:
//     "c"=currently attached atom,
//     "r"=root of the tree,
//     "w"=world
//	elemental type:
//		"p" = posn (Vector3),
//		"o" = orientation (Quaternion),
//		"s" = scale (Vector3)
//	"d" = desired point, "0"=origin
//	"1" = inverse: e.g. Vector3 v1 = Vector3(1/v.x, 1/v.y, 1/v.z) Quaternion q1 = q.Inverse()

//	// xform, from c (curAtom) to w (world):
//	Vector3f c0_wp = curAtomSceneAtom->_getDerivedPosition();
//	Quatf    c0_wo = curAtomSceneAtom->_getDerivedOrientation();
//	Vector3f c0_ws = curAtomSceneAtom->_getDerivedScale();
//
//	// xform, from r (root) to to w (world):
//	Vector3f r0_wp = rootSceneAtom->_getDerivedPosition();
//	Quatf    r0_wo = rootSceneAtom->_getDerivedOrientation();
//	Vector3f r0_ws;// = rootSceneAtom->_getDerivedScale();

//	Vector3f d_wp, d_rp, delt;

// transform desired posn from cur to world:
//	d_wp = c0_wp + c0_ws * (c0_wo * d_cp);							// d_cp --> d_wp
//	Vector3f r0_ws1(1/r0_ws.x, 1/r0_ws.y, 1/r0_ws.z);
//	Quatf    r0_wo1 = r0_wo.Inverse();

// transform desired posn from world to root:
//                     d_wp = r0_wp   + r0_ws * (r0_wo * d_rp)		// d_rp --> d_wp
//                     d_wp - r0_wp   = r0_ws * (r0_wo * d_rp)		//
//           r0_ws1 * (d_wp - r0_wp)  =          r0_wo * d_rp		//
// r0_wo1 * (r0_ws1 * (d_wp - r0_wp)) =                  d_rp		// d_rp <-- d_wp
//	d_rp = r0_wo1 * (r0_ws1 * (d_wp - r0_wp));
