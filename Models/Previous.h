// Previous.h -- Remembers what happened previously, used for prediction C2014PAK

#import "Atom.h"

/*! Concepts:
	There are two top Ports, designed for input to association nets:
		sCurPort		-- value for current time (mostly repeating self)
		tPrevPort	-- value in the previous time (before the last clock)

Hungarian: sCurPort expands as -- s: name is 'S'; Cur: for current time; Port, or whatever
	[p s t l][
 */


 // =============== Multiplexor Sources ==============
#define definedPrevMuxSources				\
    prevMuxSource(	fromUNDEF=0, @"-")		\
    prevMuxSource(	fromPPri,	 @"p")		\
	prevMuxSource(	fromSCur,    @"s")		\
	prevMuxSource(	fromTPrev,   @"t")/*d_t*/\
	prevMuxSource(	fromLLatch,  @"l")		\
	prevMuxSource(	fromZero,	 @"0")		\
	prevMuxSource(	fromBias,    @"b")		\
//1: PrevMuxSources:
#define prevMuxSource(val, string) val,
typedef enum : unsigned char { definedPrevMuxSources } PrevMuxSources;
//2: prevMuxSourceNames
#undef prevMuxSource
#define prevMuxSource(val, string) string,
static NSArray *prevMuxSourceNames = @[ definedPrevMuxSources ];


 // =============== Multiplexor Sources ==============
typedef enum : unsigned char {
	prevMinorModeHold=1,
	prevMinorModeMonitor,
	prevMinorModeSimForward,
	prevMinorModeSimBackward,
	prevMinorModeNetForward,
	prevMinorModeNetBackward,
} PrevMinorMode;		// must match the following:
const id prevMinorModeNames = @[@"unused",		@"hold",		@"monitor",
								@"simForward",	@"simBackward",
								@"netForward",	@"netBackward"];


@interface Previous : Atom

	@property (nonatomic, assign) float			bias;		// used for halucination
	@property (nonatomic, retain) id			majorMode;	// set during construction
															// constant during operation
	@property (nonatomic, assign) PrevMinorMode	minorMode;	// changes per M&N

	 // UP
    @property (nonatomic, assign) PrevMuxSources src4sCur;	// bias,  pPri,	lLatch
    @property (nonatomic, assign) PrevMuxSources src4tPrev;	// bias,  pPri,	lLatch
	 // LATCH
    @property (nonatomic, assign) PrevMuxSources src4lLatch;// pPri,  sCur,	tPrev
	 // DOWN
    @property (nonatomic, assign) PrevMuxSources src4pPri;	// zero,  sCur,	tPrev

#pragma mark  7. Simulator Messages
- (void)			clockPrevious;

#pragma mark 15. PrettyPrint
NSString			*pp(PrevMuxSources fs);

@end
