// Info.h -- a wrapper for Dictionaries, arrays, or strings C2014 PAK

@class Part;

@protocol InfoBasics											// INFO BASIS //
- (id)			anotherMutable;				// (caller does not own, must retain)
- (id)			deepMutableCopy;			// (somewhat broken)

#pragma mark 15. PrettyPrint
- (NSString *)	pp;			// autonomous, no aux
- (NSString *)	pp:	aux;
	// noBrackets		-->		suppress printing outter {} or []
	// indent			-->		a string to put at the beginning of each new line

- (const char*) ppC;
@end



																 //// ID //////
////////////////////////////////////////////////////////////////// CATEGORY //
@interface NSObject /*extended here to*/ (info4id) <InfoBasics>
-(NSDictionary*)asHash;
- (NSArray  *)	asArray;
- (NSString *)	asString;
- (NSNumber *)	asNumber;
- (Part *)		asPart;

- (bool) 		isHash;
- (bool) 		isArray;
- (bool) 		isString;
- (bool) 		isNumber;
- (bool)		isPart;

- (bool)		isInfoType;
- (bool)		isMutable;
- (bool)		isFalseId;
- (bool)		isTrueId;
- (bool)		isBlock;

- (NSString *)	uidStr;
 void			pInfo(id info);		// for lldb

@end
																 //// HASH ////
////////////////////////////////////////////////////////////////// CATEGORY //
@interface NSDictionary /*extended here to*/  (info4NSDictionary) <InfoBasics>
@end
																 //// ARRAY ///
////////////////////////////////////////////////////////////////// CATEGORY //
@interface NSArray /*extended here to*/	 (info4NSArray) <InfoBasics>
+ (NSArray *)   arrayCatenation:(NSArray *)elements;
@end
@interface NSMutableArray /*extended here to*/	 (info4NSMutableArray) <InfoBasics>
- (id)	 		addObjectIfAbsent:(id)anObject;
- (void)		setObjectAt:(int)index toValue:obj;
- (id)			dequeFromHead;		// presumes enqueing via -addObject:
@end
   NSArray     *cat(NSArray *array0, ...);

																 /// STRING ///
///////////////////////////////////s/////////////////////////// CATEGORY //
@interface NSString /*extended here to*/		  (info4NSString) <InfoBasics>
- (const char *)C;                          // produce UTF8String
- (void)		ppLog;
- (void)		ppLog:aux;
- (void)		ppLog:aux :fill;
- (NSString *)	add:(id)isPpAble;
- (NSString *)	addF:(NSString *)fmt,...		  __attribute__((format(NSString, 1, 2)));
- (NSString *)	addN:(int)n F:(NSString *)fmt,... __attribute__((format(NSString, 2, 3)));
- (int)			columnCur;
- (NSString *)	padToColumn:(int)col;
- (NSString *)  capitalCammel;
@end
																 //// NUMBER ////
//////////////////////////////////////////////////////////////////// CATEGORY //
@interface NSNumber /*extended here to*/		 (info4NSNumber) <InfoBasics>
@end
