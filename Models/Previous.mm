// Previous.mm -- Remembers what happened previously, used for prediction C2014PAK

/*
 */

#import "Previous.h"
#import "Brain.h"
#import "View.h"

#import "FactalWorkbench.h"
#import "Id+Info.h"
#import "SystemTypes.h"

#import "Common.h"
#import "NSCommon.h"
#import "GlCommon.h"
#import <GLUT/glut.h>

@implementation Previous

static float aPriori = 0.2;

#pragma mark 1. Basic Object Level

- (void) dealloc; {
    self.majorMode = nil;

    [super dealloc];
}

#pragma mark 2. 1-level Access

#pragma mark 3. Deep Access

- (id) deepMutableCopy; {		Previous *rv = [super deepMutableCopy];

	rv.bias					= self.bias;
	rv.majorMode			= [self.majorMode deepMutableCopy];
	rv.minorMode			= self.minorMode;

	rv.src4sCur				= self.src4sCur;
	rv.src4tPrev			= self.src4tPrev;
	rv.src4lLatch			= self.src4lLatch;
	rv.src4pPri				= self.src4pPri;

	return rv;
}

#pragma mark  4a Factory Access						// Methods defined by Factory

- (id) atomsDefinedPorts;	{			id rv = [super atomsDefinedPorts];
										// probably returns P
	rv[@"S"]		= @"pc ";	// sCur;	Secondary Port, value at Current time	//rv[@"P"]		= @"pcd";	// pPri	Primary Port
	rv[@"T"]		= @"pc ";	// tPrev:	Terciary Port, value at Previous time
	rv[@"L"]		= @"pc ";	// lLatch:	the State Port held in the Previous
	rv[@"M"]		= @"p  ";	// mMode	controls dir or fwd
	rv[@"N"]		= @"p  ";	// nMode:	controls bkw
	return rv;								
}
 //
atomsPortAccessors(pPriPort,		P, true)
atomsPortAccessors(sCurPort,		S, false);
atomsPortAccessors(tPrevPort,		T, false);
atomsPortAccessors(lLatchPort,		L, false);
atomsPortAccessors(mModePort,		M, false);
atomsPortAccessors(nModePort,		N, false);

#pragma mark 4. Factory
Previous *aPrevious(id pri, id etc) {
	id info = @{@"bias":@0.0, etcEtc}.anotherMutable;	// 161107 was 0.44
	if (pri)
		info[@"P"] = pri;

	Previous *newbie = anotherBasicPart([Previous class]);
XX	newbie = [newbie build:info];
	return newbie;
}

- (id) build:(id)info;	{					[super build:info];

//!	xzzy1 Previous::	1. bias				:<float>
//!	xzzy1 Previous::	2. mode				:
//! xzzy1 Previous::			@"zero", @"monitor", @"simForward",
//! xzzy1 Previous::			@"simBackward", @"netForward", @"netBackward",

	 // Set bias:
	self.bias = 0.555;
	if (id bias = [self parameterInherited:@"bias"])
		self.bias = [bias floatValue];

	 // Set mode:
	self.majorMode = @"monitor";				// Default is monitor
	self.minorMode = prevMinorModeMonitor;		// ""
	if (id mode = [self parameterInherited:@"mode"]) {		// From factory
		self.majorMode = mode;						// never changes
		NSInteger index = [prevMinorModeNames indexOfObject:mode];
		 // default:
		if ([mode asNumber]) {						// major mode = @0 --> Default
			assert([mode intValue]==0, (@"only number @0 (=null mode) defined"));
			self.majorMode = @"monitor";
			self.minorMode = prevMinorModeMonitor;
		}
		 // minor is static and equal to major
		else if (index != NSNotFound)				// major mode is also a minor mode:
			self.minorMode = (PrevMinorMode)index;		// (presumes order maintained with prevMinorModeNames)

		 // minor depends on major and M (?and N):
		else if ([mode isEqualToString:@"simModeDir"])
			self.minorMode = prevMinorModeSimBackward;	// Default: M==0 ==> Backward
		else if ([mode isEqualToString:@"simMode2"])
			self.minorMode = prevMinorModeHold;			// Default: M==N==0 ==> Hold
		else
			panic(@"Previous: unknown mode:'%@'", mode);
	}
	[self setSrc4:self.minorMode];

	   // Latch Port connects to self
	  // This causes it to be counted in the unsettledCount
	 // and thus it flows out to P,S, or T
	Port *latchPort			= self.lLatchPort? mustBe(Port, self.lLatchPort) :nil;
	latchPort.noCheck		= true;				// of up/down
	latchPort.connectedTo	= latchPort;

	   ////////// check for consistency here... /////
	self.parameters[@"addPreviousClock"] = @1;
	return self;
}

 // Set the Previous's "plumbing", who connects to whom.
- (void) setSrc4:(PrevMinorMode)mode; {
	switch (mode) {
		default:
			panic(@"Previous' minorMode %d illegal", self.minorMode);
		break; case	prevMinorModeHold:
			self.src4pPri		= fromLLatch;
			self.src4sCur		= fromZero;
			self.src4tPrev		= fromZero;
			self.src4lLatch		= fromLLatch;
		break; case	prevMinorModeMonitor:
			self.src4pPri		= fromSCur;
			self.src4sCur		= fromPPri;
			self.src4tPrev		= fromLLatch;
			self.src4lLatch		= fromPPri;
		break; case	prevMinorModeSimForward:
			self.src4pPri		= fromSCur;
			self.src4sCur		= fromBias;
			self.src4tPrev		= fromLLatch;
			self.src4lLatch		= fromSCur;
		break; case	prevMinorModeSimBackward:
			self.src4pPri		= fromTPrev;
			self.src4sCur		= fromLLatch;
			self.src4tPrev		= fromBias;
			self.src4lLatch		= fromTPrev;
		break; case	prevMinorModeNetForward:		// added 160402
			self.src4pPri		= fromSCur;				// <-SNext
			self.src4sCur		= fromBias;
			self.src4tPrev		= fromPPri;				// ->tCur
			self.src4lLatch		= fromLLatch;//fromZero;
		break; case	prevMinorModeNetBackward:
			self.src4pPri		= fromTPrev;
			self.src4sCur		= fromPPri;
			self.src4tPrev		= fromBias;
			self.src4lLatch		= fromLLatch;//fromZero;
	}
}
#pragma mark 7. Simulator Messages
- (id) receiveMessage:(FwEvent *)event; {

	if (event->fwType == sim_clockPrevious) {
		[self logEvent:@"$$$$$$$$ clockPrevious generated by Previous:"];
		[self clockPrevious];	/// Previous got -receiveMessage; send customers
	}
	return nil;			 // do not call super
}						// because the Previous manages everything inside itself.


- (void) clockPrevious;		{

	  // Update LATCH
	 //
	float newVal = nan("do nothing");
	PrevMuxSources lLatchSrc = self.src4lLatch;  // depending on src4lLatchmode
	Port *fromPort =
		lLatchSrc==fromPPri?	self.pPriPort:		// pPri   //
		lLatchSrc==fromSCur?	self.sCurPort:		// sCur   //
		lLatchSrc==fromTPrev?	self.tPrevPort:		// tPrev  //
		lLatchSrc==fromLLatch?	self.lLatchPort:	// lLatch //
								nil;
	id msg = @"clockPrevious; L";
	if (fromPort) {										// If Port to get dat from
		if (Port *fromBitIn = fromPort.connectedTo) {		// and it's connected
			msg=[msg addF:@"(from Port %@) ", prevMuxSourceNames[lLatchSrc]];
			newVal = fromBitIn.valueGet;						// +
		}
		else
			msg=[msg addF:@"disconnected; unchanged"];
	}
	else if (lLatchSrc==fromZero) {						// If data is zero
		newVal = 0.0;
		msg=[msg addF:@"from zero "];
	}
	else
		panic(@"Illegal src4lLatch value %@", pp(lLatchSrc));

	if (!isNan(newVal)) {
		[self logEvent:@"$$$$$$$$ %@= %.2fwas %.2f", msg, newVal, self.lLatchPort.value];
XX		self.lLatchPort.valueTake = newVal;
		[self.brain kickstartSimulator];				// start simulator
	}

	self.flash = self.brain.flashFrames;
//	[msg ppLog];
//	[self logEvent:@"$$$$$$$$ %@", msg];
}

#pragma mark 8. Reenactment Simulator
/// *//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*// ///
/// //*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//* ///
/// /*//*//*//*//*//*//*//    Reenactment Simulator   //*//*//*//*//*//*//*/ ///
/// *//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*// ///
/// //*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//* ///

- (void) simulateDown:(bool)downLocal; {
	Port *pPriPort		= self.pPriPort,		*pPriPortIn	 = pPriPort.connectedTo;
	Port *sCurPort		= self.sCurPort;
	Port *tPrevPort		= self.tPrevPort;
	float lLatchInVal	= self.lLatchPort.valueGet;

	if (!downLocal) {				//============: going UP ==================
		float pPriInVal	= pPriPortIn.valueGet;		// must read every time to settle

		 // UP to 'S' (CUR)							///--> sCur <-- //
		PrevMuxSources sCur = self.src4sCur;
		float nextVal = sCurPort.valueTake =
			sCur==fromBias?		self.bias:				// bias  //
			sCur==fromPPri?		pPriInVal:				// pPri  //
			sCur==fromLLatch?	lLatchInVal:			// lLatch//
			sCur==fromZero?		0.0:					// zero  //
								nan("src4sCur bad");
		assert(!isNan(nextVal), (@"Illegal src4sCur value %@", pp(self.src4sCur)));
		
		 // UP to 'T' (PREV)						///--> tPrev <--///
		PrevMuxSources tPrev = self.src4tPrev;
		nextVal = tPrevPort.valueTake =
			tPrev==fromBias?	self.bias:				// bias  //
			tPrev==fromPPri?	pPriInVal:				// pPri  //
			tPrev==fromLLatch?	lLatchInVal:			// lLatch//
			tPrev==fromZero?	0.0:					// zero  //
								nan("src4tPrev bad");
		assert(!isNan(nextVal), (@"Illegal src4tPrev value %@", pp(self.src4tPrev)));
	}
									//============: going DOWN ================

	else {	// DOWN to 'P' (SELF)					///--> pPri <--///
		float sCurInVal  = sCurPort.connectedTo.valueGet;
		float tPrevInVal = tPrevPort.connectedTo.valueGet;

		PrevMuxSources pPri = self.src4pPri;
		float nextVal = pPriPort.valueTake =
			pPri==fromSCur?		sCurInVal:				// sCur  //
			pPri==fromTPrev?	tPrevInVal:				// tPrev //
			pPri==fromLLatch?	lLatchInVal:			// lLatch//
			pPri==fromZero?		0.0:					// zero  //
								nan("src4pPri bad");
		assert(!isNan(nextVal), (@"Illegal src4pPri value %@", pp(self.src4pPri)));
  
		  ///// M and N Ports control the various PrevMuxSources
		 //
		Port *mModeInPort	= self.mModePort.connectedTo, *nModeInPort = self.nModePort.connectedTo;
		bool cm = mModeInPort.valueChanged,			 cn = nModeInPort.valueChanged;
		if (cm or cn) {
			 // Read M and N mode Ports
			float mModeValue = mModeInPort.valueGet;
			float nModeValue = nModeInPort.valueGet;
//			assert(mModeValue<=0.5 or nModeValue<=0.5, (@"Illegal: M and N Ports are both ON"));
			
			PrevMinorMode nextminorMode = self.minorMode;
			 // here M determines fwd/bkw:
			if ([self.majorMode isEqualToString:@"simModeDir"])
				nextminorMode = mModeValue > 0.5? prevMinorModeSimForward:
												  prevMinorModeSimBackward;
			 // here M determines fwd; N bkw
			else if ([self.majorMode isEqualToString:@"simMode2"])
				nextminorMode = mModeValue > 0.5? prevMinorModeSimForward:
								nModeValue > 0.5? prevMinorModeSimBackward:
												  prevMinorModeHold;
			if (self.minorMode != nextminorMode) {
				self.minorMode  = nextminorMode;
				[self setSrc4:self.minorMode];	// push mode into machine

				[self logEvent:@"Mode Port: %@%@; curMode=%@-->%@",
						cm? [@"" addF:@"M=%.2f ", mModeValue]: @"",
						cn? [@"" addF:@"N=%.2f ", nModeValue]: @"",
						prevMinorModeNames[self.minorMode], [self ppSrc4]];
				[self.brain kickstartSimulator];		// start simulator
			}
		}
	}
	[super simulateDown:downLocal];
}

#pragma mark 9. 3D Support
	float previousWidth  = 6.0;
	float previousHeight = 3.0;
	float previousDeapth = 2.0;
	float previousLatchX = 3.0;

- (Bounds3f) gapAround:(View *)v; {
	Bounds3f b = Bounds3f(			   -1,            0.0, -previousDeapth/2,
						  previousWidth-1, previousHeight,  previousDeapth/2);
	return b;
}

- (Matrix4f) positionOfPort:(Port *)port inView:(View *)bitV; {
	Vector3f bitSpot = zeroVector3f;

	if ([port.name isEqualToString:@"T"])			// Previous Time
		return [port suggestedPositionOfSpot:
				Vector3f(previousWidth-2, previousHeight, 0)];

	if ([port.name isEqualToString:@"L"]) 			// Latch (internal)
		return [port suggestedPositionOfSpot:
				Vector3f(previousLatchX-1, 2.05, 0)];// (.05 -> slightly above)
	
	if ([port.name isEqualToString:@"M"]) {			// Mode
		port.spin = 3;
		port.latitude = previousLatchX;
		return [port suggestedPositionOfSpot:
					Vector3f(previousWidth-4, 1.5, -2)];
	}
	if ([port.name isEqualToString:@"N"]) {	// Mode 2
		port.spin = 3;
		port.latitude = previousLatchX;
		return [port suggestedPositionOfSpot:
					Vector3f(previousWidth-4, 2.5, -2)];
	}
XR	return [super positionOfPort:port inView:bitV];
}

#pragma mark 11. 3D Display
//// // // // // // // // // // // // // // // // // // // // // // // ///
/// // // // // // // // // // 3D Display  // // // // // // // // // ////
// // // // // // // // // // // // // // // // // // // // // // // // //
- (void) drawFullView:(View *)v context:(RenderingContext *)rc; {
	rc.color = colorPurple4w;
	glPushMatrix();
		glTranslatef(self.brain.displayOffsetX, 1.5, self.brain.displayOffsetZ);
		glPushMatrix();						// Box under P, S
			glTranslatef(+0.5, 0, 0);
			glScalef(3, 1, previousDeapth);
			glutSolidCube(1.0);
		glPopMatrix();
		glPushMatrix();						// Box under T
			glTranslatef( 4.5, 0, 0);
			glScalef(1, 1, previousDeapth);
			glutSolidCube(1.0);
		glPopMatrix();

//		rc.color = lerp(colorOfBidirActivations[1], colorOfBidirActivations[0], self.lLatchPort.value);
		rc.color = colorOf2Ports(self.lLatchPort.value, 0.0, 0);

		glPushMatrix();

			glTranslatef(previousLatchX, 0, 0);
			glScalef(2.0, 1.0, previousDeapth + 0.001);
//			glTranslatef(previousLatchX, -0.351, 0);
//			glScalef(0.3, 1.0, previousDeapth + 0.001);
			glutSolidCube(1.0);
		glPopMatrix();

	glPopMatrix();

	[super drawFullView:v context:rc];				// PRIMARY
}

- (FColor)colorOf:(float)ratio;		{	return colorPurple4w;/*colorPurple4;*/}//colorOrange

#pragma mark 15. PrettyPrint
    ///\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\///
   ///\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\///
  ///\\//\\//\\//\\//\\//\\//\\// PRINTOUT //\\//\\//\\//\\//\\//\\//\\//\\///
 ///\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\///
///\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\///
- (NSString *) pp1line:aux; {		id rv = [super pp1line:aux];

	rv=[rv addF:@"%@.%@(%@) ", self.majorMode, prevMinorModeNames[self.minorMode], [self ppSrc4]];
	return rv;
}

- (NSString *) ppSrc4; {
	return [@"" addF:@"P%@S%@T%@L%@", pp(self.src4pPri), pp(self.src4sCur),
									  pp(self.src4tPrev), pp(self.src4lLatch)];
}

NSString * pp(PrevMuxSources fs) {
	return prevMuxSourceNames[fs];
}


@end
