//  Port.mm -- the bidirectional connection of one port of info -- C2014PAK


#import "Port.h"
#import "Atom.h"		// hack
#import "Share.h"		// hack
#import "Splitter.h"	// hack
#import "Bundle.h"		// hack
#import "Leaf.h"		// hack
#import "FactalWorkbench.h"
#import "View.h"
#import "Bundle.h"
#import "Link.h"
#import "Path.h"
#import "Brain.h"
#import "vmath.hpp"
#import "SimNsWc.h"
#import "SimNsVc.h"
#import "InspecVc.h"
#import "Id+Info.h"

#import "Common.h"
#import "NSCommon.h"
#import "GlCommon.h"
#import <GLUT/glut.h>
#import "Colors.h"


@implementation Port

#pragma mark 1. Basic Object Level

- (void) dealloc {

	// Since this Port is going away, nil out its connecteee
	if (Port *pTo = coerceTo(Port, self.connectedTo)) {
		assert(pTo.connectedTo == self, (@"should be only reciprocal connections"));
//		pTo.connectedTo = nil;
		pTo->_connectedTo = nil;	// 180805 fix: std setter
	}
	self->_connectedTo	= nil;

	[super dealloc];
}

- (id) deepMutableCopy; {		Port *rv = [super deepMutableCopy];

	rv.connectedTo		= nil;
	rv.value			= self.value;
	rv.valuePrev		= self.valuePrev;
	rv.noCheck			= self.noCheck;
	rv.dominant			= self.dominant;
	return rv;
}

#pragma mark  2. 1-level Access
 // Value should never be NAN or INF
//- (void) setValue:(float)val; {
//	assert(     !isNan(val), (@"setting port value with NAN"));
//	assert(!std::isinf(val), (@"setting port value with INF"));
//	_value = val;
//}
//- (float) valueRaw;						{	return _value;				}
//- (void) setValueRaw:(float)val;		{	_value = val;				}


  // Currently, only 2-Port loops are supported (A->B; B->A)
 //   Someday N>2 Port loops (A->B; B->C; ... D->A) may be possible
//     affording a light-weight Rotator-like functionality
// PRESUMES Port is embedded in a Brain!!! (move self.brain... upward, ditto swf)
- (void) setConnectedTo:(Port *)target; {

	if (Port *selfCon2 = self->_connectedTo){	// Existing connection in self?
		if (target == selfCon2)		 				// Setting to same value
			return;										// do nothing
			 // (Don't remove and rewrite. That will aggrevate the automatic link removal protocol.)

		 // Unlink
		assert(selfCon2->_connectedTo==self, (@"Improper Connection"));
		selfCon2->_connectedTo	= nil;
		self->_connectedTo 		= nil;		  		// Remove the previous connection

//		[selfCon2.atom portCheck];					// reap unused elements (e.g. links)
		self.brain.someViewDirty = true;			// reView
	}

	if (!target)								// Setting to nil
		return;

	if (Port *targCon2 = target.connectedTo)	// Existing connection in self?
		if (targCon2 != self) {
			assert(targCon2->_connectedTo==target, (@"Improper Connection"));
			targCon2->_connectedTo	= nil;
			target->_connectedTo	= nil;
		}

	bool swf = [self downInWorld];				// Ends must have opposing world flips:
	bool cwf = [target downInWorld];
	if (!self.noCheck and !target.noCheck)
		assert(swf!=cwf, (@"SetConnectedTo ERROR: ends must have different world flip "
				"(%@.flipped=%d, %@.flipped=%d)", self.fullName16, swf, target.fullName16, cwf));

	assert(!target.connectedTo, (@"Connection's target already occupied"));
	 // Connections are weak references, as that seems to reduce critical loops
	self->_connectedTo 			= target;		// weak, no -retain
	target->_connectedTo 		= self;			// don't use accessor to set back path
}

- (void) setEnclosedPortWriteHeads:val; {		[super setEnclosedPortWriteHeads:val];

	self.writeHead = val;
}

- (int) portCount;	{	return 1;									}


#pragma mark  3. Deep Access
//- nameFull9links; {
- fullName9links; {
	Port *slf = self;
	id rv = @"";

	while (coerceTo(Link, slf.parent)) {			// If slf is a link,
		rv=[rv addF:@"%@->", slf.parent.name];			// print it
		slf = slf.otherPortIfLink.connectedTo;			// skip over it
	}
	if (slf == nil)
		return [rv addF:@"<nil>"];

	return [rv addF:@"%@", slf.fullName];
}

- (NSString *) nameInAtom; {
	Atom *a = mustBe(Atom, self.atom);
	return [@"" addF:@"%@.%@", a.name, self.name];
}

#pragma mark 6. Navigation
- (Atom *) atom;	{
	return coerceTo(Atom, self.parent);
}
- (Port *) portPastLinks; {
	return self.skipOverInterveningLinks.connectedTo;
}

 // If self connects to a Link, return the Port on the other end of the Link
- (Port *) skipOverInterveningLinks; {
	Port *rv = self;				// Start off

	 /// If rv is connected to an atom which is a Link:
	while (Link *link = coerceTo(Link, rv.connectedTo.atom))
		rv = rv.connectedTo.otherPortIfLink;		 /// Go to the other end of the link

	return rv;
}
 // move along a link, to it's other end
- (Port *) otherPortIfLink; {
	Port *rv=0;
	if (Link *link = coerceTo(Link, self.atom)) {
		 // Links have parts=[P, S]; find other end.
		int i			= (int)[link.parts indexOfObject:self];
		assert(link.parts.count==2 and i>=0 and i<2, (@"Link Malformed. (It has %d Ports)", (int)link.parts.count));
		rv				= link.parts[1-i];		 /// Go to the other end of the link
	}
	return rv;
}


 // 161130 Old and frail:
- (Port *) atomsOtherBests_otherPort; {
	id atom = self.atom;

	 // Bit's facing in its atom's frame determines the other Bit's facing
	bool otherDownInAtom = !self.flipped;
	Port *otherPort = [atom biggestBitOpeningDown:otherDownInAtom];
	return otherPort.connectedTo;
}

- (Port *) getOpenPort_openingDown:(bool)localDown;{	/// localDown is IGNORED!!!
	if (self.connectedTo == nil)
		return self;

	return [self similarOpenPort];
}

- (Port *) similarOpenPort; {
	if (!self.connectedTo)		// one found in right direction, open
		return self;					// it's open

	 /// move to connected Port and its Atom
	Port *connectedPort = self.portPastLinks;
	Atom *connectedAtom = coerceTo(Atom, connectedPort.atom);

	 // if it is already a Broadcast, just add another share
	Splitter *potentialBcast = coerceTo(Splitter, connectedPort.atom);
	if (potentialBcast.isABcast and connectedPort.flipped == 1) // rightside in .parent
		return [potentialBcast anotherShare];

	   ///   "AUTO-BCAST":   Add a new Broadcast to split the port	/auto Broadcast/auto-broadcast/
	  ///
	 ///	Where to add? the smallest enclosing net of self
	[self logEvent:@"<<++ Auto Broadcast ++>>"];
	Part *papaNet			= mustBe(Part, self.enclosingNet);	// must be enclosing net

	 ///	Make a Splitter with Broadcast Shares
	Splitter *newBcast 		= aBroadcast();
	newBcast.name			= [self.atom.name addF:@"%@", self.name];
	newBcast.flipped		= [self downInPart:papaNet];

	  /// Insert the Bcast in net:
	 ///	Find a spot to slip it inbetween existing
	Part *child = [self ancestorThatsChildOf:papaNet];
	NSInteger ind = [papaNet.parts indexOfObject:child];	// my atom
	assert(ind!=NSNotFound, (@"Broadcast index bad of %@", self.fullName));
	ind += ![self downInPart:papaNet];				//	after it if !self.flipped
	 ///	Insert in Net:
	[papaNet addPart:newBcast atIndex:(int)ind];
	newBcast.parent			= papaNet;
	 ///	Link:
	Share *newShare 		= [newBcast anotherShare];// Share 1 is to replicate old connection
	id selfCon2				= self.connectedTo;
	newShare.connectedTo	= selfCon2;	/// 1. move old connection to new share
	newBcast.pPort.connectedTo = self; /// 2. link newBcast to self
	return [newBcast anotherShare];	  /// 3. return second share is what started this
}

#pragma mark 8. Reenactment Simulator
 // Ports do nothing:
- (void) simulateDown:(bool)downLocal; { }

// ISSUE:
// latch wants to be counted in the unsettled, so simulations take the value out
- (int) unsettledPorts;	{
	 // unconnected Ports (e.g. P port) do NOT want to be counted.
	if (!self.connectedTo)					// if disconnected
		return 0;								// disregard
	if (self.value == self.valuePrev)		// if values are settled
		return 0;								// disregard
	return 1;
}

- (id) ppUnsettledString; {
	if ([self unsettledPorts])
		return [@"" addF:@"..%@: Port unsettled =%.2f (prev %.2f)\n",
									self.fullName16, self.valuePrev, self.value];
	return nil;
}
#pragma mark  7b Port Access
 // Receives values set by a (usually) different part
- (void) setValueTake:(float)val; {

	assert(     !isNan(val), (@"setting port value with NAN"));
	assert(!std::isinf(val), (@"setting port value with INF"));

  	 // set our value.  (Usually done from self)
	if (self.value != val) {
		[self logEvent:@"<=_/  %.2f (was %.2f)", val, self.value];
		self.value = val;
		[self.brain kickstartSimulator];			// set simulator starts up MODE
	}
}
- (float) valueTake;   {panic(@"Read is illegal. valueTake is write-only. ");return 0.0;}

 // GETTING Values:
- (float) valueGet; {						// unload value, set to unchanged
	if (self.brain.logDataPutsTakes and self.valueChanged)
		[self logEvent:@">=~.  %.2f (was %.2f)", self.value, self.valuePrev];

	self.valuePrev = self.value;// returning the inValue promotes it to the previous
		// N.B: getter with side effect! // 
	return self.value;
}
- (bool) valueChanged; {				// has another changed our value?
	return self.valuePrev!=self.value;
}

- (float) valUp; {		// Upward value in local frame is our value
	return self.value;
}
- (float) valDown; {	// Downward value in local frame is our connectee's value
	if (Port *b = coerceTo(Port, self.connectedTo))
		return b.value;
	return 0.0;			// or NaN?
}

#pragma mark 9. 3D Support
// \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\    //
//  \\ \\ \\ \\ \\ \\ \\ \\ \ 3D Support\\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\  //
//   \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\//
- (Bounds3f) gapAround:(View *)v; {
	float r = self.brain.bitRadius;
	return Bounds3f(-r, -r, -r,		 r,  0,  r);
}

- (void) boundIntoView:(View *)v; {
	assert(![v.name isEqualToString:self.brain.breakAtBoundOf],
						(@"breakAtBoundOf view %@", self.brain.breakAtBoundOf));
XX	v.bounds = [self gapAround:v];
}

 // our parent part (e.g. Known, Previous, Net, ...) may perform specific placements
- (void) placeIntoView:(View *)v; {
	Atom *parent = mustBe(Atom, self.parent);
XX	v.position = [parent positionOfPort:self inView:v];
}
							//		posn	  rad		inset
Hotspot zeroHotspot =		{	zeroVector3f, 0.0,		0.0		};
Hotspot hotspotNan  =		{	 nanVector3f, 0.0,		0.0		};
bool isNan(Hotspot hotspot) {	return isNan(hotspot.center);		}

- (Hotspot) portsHotspot; {
	return zeroHotspot;		// Hotspot is at zero of Port's coord sys
}							// radius, inset and outset are zero

- (Vector3f) peakSpotInView:(View *)commonView; {
	bool dn = [self downInPart:commonView.part];

	 // find hotspot of self, in commonView
	Hotspot hs = [self hotspotOfPortInView:commonView];
	Vector3f centerInCommon = hs.center;		// center of hs
	centerInCommon.y += hs.inset * (dn? 1:-1);	// inset bounds of hs

	 // 180216: allow Splitters to not observe "skyline"
	if (coerceTo(Splitter, self.atom).noPosition)
		return centerInCommon;

		 // HIghest part self is a part of..
	Part *hiPart  = [self ancestorThatsChildOf:commonView.part];
	View  *hiView = [commonView searchInViews4Part:hiPart];

XX	Bounds3f hiBoundsInCommon = hiView.bounds * hiView.position;

	if (!dn) {
		if (centerInCommon.y < hiBoundsInCommon.y1) // max of the two
			centerInCommon.y = hiBoundsInCommon.y1;
	} else
		if (centerInCommon.y > hiBoundsInCommon.y0) // min of the two
			centerInCommon.y = hiBoundsInCommon.y0;

	return centerInCommon;
}

//func keepoutOfPortInView(refView:View) -> KeepOut {
- (Hotspot) hotspotOfPortInView:(View *)refView; {

	 // Find basic Hotspot from port
	Hotspot hs = [self portsHotspot];			// find hs
//	bool hsDownInV = false;						// hs opens UP in it's own view

	View *view = [refView searchInViews4Part:self];		// View of p
	assert(view, (@"could not find view for self in refView"));
	assert(view!=refView, (@"must go thru while loop below once. Why not use -portsHotspot"));
	for (Part *part=self; view != refView; part=part.parent, view=view.superView) {
		assert(view, (@"gone off the top of View hierarchy!"));
		assert(view.part==part, (@"something is bad!"));
		if (view.isAtomic) {
			hs.center = zeroVector3f;
			hs.inset = hs.radius = self.brain.atomRadius;
		}

		 // move from v to v.superView
		hs.center = view.position * hs.center;	// convert to sv
//		hsDownInV ^= part.flipped;				// convert to sv
	}
	return hs;
}

#pragma mark 11. 3D Display
//// // // // // // // // // // // // // // // // // // // // // // // ///
/// // // // // // // // // // 3D Display  // // // // // // // // // ////
// // // // // // // // // // // // // // // // // // // // // // // // //
 // BAD: Plugs (by their nature) draw centered on the bottom surface, with limits on radius
- (void) drawFullView:(View *)v context:(RenderingContext *)rc; {
	float bitRadius  = self.brain.bitRadius;
	if (!coerceTo(Share, self)) {	// (still visible, bound viewable
		glPushMatrix();
			glRotatef(180, 1.0, 0, 0);					// hack fix

			  //////////////////////////////////
			float signalSize = self.brain.signalSize;		// how much signal to display 0<=x<=1
			float bitHeight  = self.brain.bitHeight;
			float valBackPct = 2 * signalSize;
			float valHeight  = 0.001;
			float valRadius  = 2 * signalSize * bitRadius;
			if (signalSize >= 0.5) {
				valRadius    = bitRadius;
				valHeight    = 2 * (signalSize - 0.5) * bitHeight;
				valBackPct   = 1.0;
			}
			float valCenter  = valHeight/2;
			float baseHeight = bitHeight - valHeight;		// of base
			float baseCenter = bitHeight - baseHeight/2;	// baseCenter + v

			FColor colorBody = colorBundlePorts_bias([self colorOf:1]);
			if (int i = self.atom.proxyColor)
				colorBody	= proxyFColor[i];
			FColor colorValue= self.colorOfValue;

			 // The Base area of port
			glPushMatrix();
				rc.color = colorBody;
				glTranslatef(0, baseCenter, 0);
				glRotatef(-90, 1,0,0);
				myGlSolidCylinder(bitRadius-0.01, baseHeight, 16, 1);	// (radius, length, ...)
			glPopMatrix();
			
			 // The Value area of port
			glPushMatrix();
				rc.color = colorValue;
				glTranslatef(0, valCenter-0.001/**/, 0);
				glRotatef(-90, 1,0,0);
				myGlSolidCylinder(valRadius, valHeight, 16, 1);
			glPopMatrix();

			if (signalSize>=1.0) {
				glPushMatrix();
					float k = signalSize - 0.5;	// runs between 0.5 and 1.0
					float h = 0.85*valHeight*k;	// optimized for aModulator
					glTranslatef(0, h, 0);
					glRotatef(90, 1,0,0);
					glutSolidCone(valRadius*2*k, h, 16, 1);//(base, height, slices, stacks)
//					glTranslatef(0, 1*valCenter/*-0.001*/, 0);
//					glRotatef(90, 1,0,0);
//					glutSolidCone(valRadius*2, 0.75*valHeight, 16, 1);

				glPopMatrix();
			}

			glPushMatrix();				// Attach Point
				rc.color = colorPurple;
				if (!self.connectedTo)
					glutSolidSphere(bitRadius/3, 8, 8);		// mark attachment  spot
//				rc.color = self.connectedTo? self.connectedTo.colorOfValue :colorPurple;
//				glutSolidSphere(bitRadius/3, 8, 8);		// mark attachment  spot
			glPopMatrix();
		glPopMatrix();

		if (self.brain.displayPortNames) {
			glPushMatrix();
				glTranslatefv(v.bounds.center());

				rc.color 				= colorGreen2b;
//				Vector3f inCameraShift 	= Vector3f(0, 0, 0);// in "facing camera" coordinates
				Vector3f inCameraShift 	= Vector3f(-3*bitRadius, 0, 0);// in "facing camera" coordinates
				Vector2f spotPercent   	= Vector2f(0.0, 0.5);		// 0=left side, .5=center y
				id string 				= [@"" addF:@"%.2f^", self.value];
				myGlDrawString(rc, string, -1, inCameraShift, spotPercent);

				rc.color 				= colorRed;
				inCameraShift 			= Vector3f(1*bitRadius, 0, 0);// in "facing camera" coordinates
				spotPercent   			= Vector2f(1.0, 0.5);		// 1=right side, .5=center y
				string 					= [@"" addF:@"%.2fv", self.connectedTo.value];
				myGlDrawString(rc, string, -1, inCameraShift, spotPercent);

			glPopMatrix();
		}
	}

	[super drawFullView:v context:rc];
}

- (FColor) colorOfValue; {
	FColor rv  = [self colorOfValue:self.value];
	return rv;
}
- (FColor) colorOfValue:(float)val; {
	bool downInWorld = self.downInWorld;
	int index  = downInWorld?
				2: // 1 --> opening down --> Red   colorOfBidirActivations[2]
				1; // 0 --> opening up   --> Green colorOfBidirActivations[1]
	FColor on  = colorOfBidirActivations[index];
	FColor off = colorOfBidirActivations[0];
	FColor rv  = lerp(on, off, val);
	return rv;
}

FColor colorOf2Ports(float localValUp, float localValDown, bool downInWorld) {
	localValUp   = isNan(localValUp)?   0.0: localValUp;		// nan --> 0
	localValDown = isNan(localValDown)? 0.0: localValDown;

	 // AGC/ signal compression:  POOR PERFORMANCE
	float pMax = max(localValUp, localValDown);
    if (pMax > 1.0)   {
        localValUp   /= pMax;
        localValDown /= pMax;
    }
	
	float valUp    = downInWorld? localValDown: localValUp;
	float valDown  = downInWorld? localValUp:   localValDown;

    FColor color	= {0, 0, 0, 1};
    for (int i=0; i<4; i++) {	// scan: -, s, d, sd
        float a		= i&1? valUp:	1.0 - valUp;
        float b		= i&2? valDown: 1.0 - valDown;
		float ab	= a * b;
		
        for (int j=0; j<3; j++)		// for values r, g, b
            color.d[j] += (colorOfBidirActivations[i]).d[j] * ab;
    }
	return color;
}

#pragma mark 12. Inspectors
- (void) sendPickEvent:(FwEvent *)fwEvent toModelOf:(View *)v; {

	NSPrintf(@"Port gets pickEvent '%@' Event(%d-clicks\n",self.fullName, fwEvent->clicks);
	NSPrintf(@"     mousePosition=%@ delta %@", pp(fwEvent->mousePosition), pp(fwEvent->deltaPosition));
	float dVal = fwEvent->deltaPosition.y/10;//0.1;
	if (1) {		/// Digitize to binary
		self.value = dVal > 0? 1.0: 0.0;   // up --> dVal>0 --> 1.0; dn --> 0.0
		NSPrintf(@" value = %f (digitized)\n", self.value);
	}
	else {			/// Limit to [0,1]
		self.value += dVal;		// add dVal in
		if (self.value < 0)			// Limit if <0
			self.value = 0;
		if (self.value > 1)			// Limit if >1
			self.value = 1;
		NSPrintf(@" value = %f (after limit)\n", self.value);
	}
}

#pragma mark 13. IBActions							// [[shift around]]

  // Bindings to a Port's value, formatted as Id, to be used by SliderBidirNsV.mm
 //
- (id) valueAsNSNumber; {
	return [NSNumber numberWithFloat:self.value];			// use standard accessors
}
- (void) setValueAsNSNumber:(id)value; {

	assert(coerceTo(NSNumber, value), (@"value not NSNumber"));
	float val = [value floatValue];				// use standard accessors
	assert(     !isNan(val), (@"setting port value with NAN"));
	assert(!std::isinf(val), (@"setting port value with INF"));

	[self logEvent:@"<-- %.2f (was %.2f) from GUI", val, self.value];
	self.value = val;
	[self.brain kickstartSimulator];			// set simulator starts up MODE
}

 // EXPERIMENTAL, doesn't yet work.
- (IBAction) inspectConnectee:(id)sender; {

	 // 1. Find CONNECTEE's Port
	Port *connPort			= self.connectedTo;

	 // 2. Find a View for this part
	SimNsWc *simNsWc		= self.brain.simNsWc;
	View *conView			= [simNsWc.simNsVc.rootFwV searchInViews4Part:connPort];
	assert(coerceTo(View, conView), (@"Couldn't find View for %@", connPort.fullName));

	 // 3. Create NsVc for CONNECTEE's
XX InspecVc *inspNsVc	= [simNsWc anInspectFor:conView forceNew:true];
	inspNsVc.simNsWc		= simNsWc;

	id w = [inspNsVc aContainingWindow];	// save window for later dismissal
	panic(@"debug me, window is lost");
}

#pragma mark 15. PrettyPrint
- (NSString *) pp1line:aux; {		id rv = [super pp1line:aux];
	Port *conPort = self.connectedTo;

	rv = [rv addF:@"%@", [self ppPortOutValues]];

	id msg=@"";
	int doit  = 5;
	if (doit == 1) {	/// Original Way
		if (conPort) {
			msg=[@"" addF:@"%@ %@ ", [conPort ppPortOutValues], conPort.fullName16];

			if (Link *ob0_atom = coerceTo(Link, conPort.parent)) {
				if (Port *ob1_bit = coerceTo(Port, conPort/*.companionPort*/)) {		// other end of the link
					if (Port *otherPort = ob1_bit.connectedTo) {	// what's connected there
						msg=[msg addF:@"->%@", otherPort.fullName16];
						if (coerceTo(Link, otherPort.atom))
							msg=[msg addF:@"!!LINK!!"];
					}
					else
						msg=[msg addF:@"-> -0-"];	// other end is nil
				}
				else
					msg=[msg addF:@"-> -?-"];		// other end not well defined
			}
		}
		else
			msg=@" ---";
	 }
	else if (doit == 2)		/// Recovery Method 2
		msg=[@"" addF:@" --> %@", conPort.fullName];
	else if (doit == 3)		/// Recovery Method 3
		msg=[@"" addF:@" --> %@ of %@", conPort.fullName, conPort.atom.name];
	else if (doit == 4) {	/// Recovery Method 4
		if (1 or !coerceTo(Link, conPort.parent)) {		/// Bail if this isn't a link
			if (conPort)
				msg=[@"" addF:@" --> %@ of %@, an atom", conPort.fullName, conPort.atom.name];
			else
				msg=[@"" addF:@" --> nil"];
		}
		else if (conPort) {		/// IS A LINK
		}
		else
			msg=[@"" addF:@" nil"];
	}
	else if (doit == 5) {	/// Recovery Method 5
		 // To Whom am I connected?
		Port *reference = self;				// Start off

		 /// If we are connected to anything, what is it sending to us
		if (conPort)
			msg=[@"" addF:@"%@ ", [conPort ppPortOutValues]];

		 /// Trace out Links, till some non-Link (including nil) is found
		while (Link *link = coerceTo(Link, reference.connectedTo.atom)) {
			Port *linkNearPort	= reference.connectedTo;
			Port *linkFarPort	= linkNearPort.otherPortIfLink;
			msg=[msg addF:@"%@.%@-> ", linkNearPort.atom.name, linkNearPort.name];
			reference = linkFarPort;
		}

		 /// Print out the non-Link:
		if (Port *referenceTo = reference.connectedTo)
			msg=[msg addF:@"%@", referenceTo.fullName16];
		else
			msg=[msg addF:@"-0-"];
	}
	rv=[rv addF:@" %@", msg];
	return rv;
}

- (NSString *) ppPortOutValues; {
	id oldVal		= self.valueChanged? [@"" addF:@"$%.2f", self.valuePrev] :@"";
	id noCheckMark	= self.noCheck?  @"^": @"[";	/// key '^' --> noCheck
	id dominantMark = self.dominant? @"!": @"]";	/// key '!' --> dominant
	id val			= [@"" addF:@"%.2f%@", self.value, oldVal];
	 // Output VALUE:		e.g. [1.00] or ^1.0!
	return [@"" addF:@"%@%.2f%@%@", noCheckMark, self.value, oldVal, dominantMark];
}

NSString *pp(Hotspot hs) {
	if (isNan(hs.center))
		return @"( nan )";
	NSString *rv = pp(hs.center);
	if (hs.inset!=0 or hs.radius!=0)
		rv=[rv addF:@"r%.0fi%.0f", hs.radius*10, hs.inset*10];
	return rv;
}

- (NSString *) strJustChanged; {	return self.valueChanged? @"!": @"=";	} // for GUI

@end
