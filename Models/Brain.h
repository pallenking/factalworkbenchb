//  Brain.h -- The brain of a Part tree, managing config, simulation C2012PAK C2015PAK

/* to do:
 */

//#import "Actor.h"
#import "Net.h"
#import "SystemTypes.h"
@class SimNsWc;
@class Part;
@class BundleTap;//, GenAtom;
@class View, PushButtonBidirNsV;

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
/*	Simulator Parameters control the operation of the Reenactment Simulator. 
	The forAllBrainParameters macro contains definition
	of each parameter, its type, initial value, and gui name in inspector .nib
 */

#define forAllBrainParameters			 \
                                         \
/** 4. Factory ********** CONSTRUCTION */\
/* BREAKS							   */\
    aFloat (curMenueTestNumber,     1   )\
    aFloat (curRegressTestNumber,   1   )\
    aInt   (breakAtWireNo,		   -1	)\
                                         \
/** 7. Reenactment Sim **** SIMULATION */\
/* Simulation						   */\
    aInt   (simEnable,              1   )/* simulator disabled if <=0		*/\
/* Simulation - RANDOM				   */\
    aInt   (randomSeed,				0   )/* initial random seed; 0-->seed from time*/\
/* GUI Clocking Mode				   */\
    aInt   (asyncData,				0   )/* mode for generators //xyzzy11c */\
                                         \
/** 9. 3D Support ************ VIEWING */\
	aString(breakAtViewOf,			@"" )\
	aString(breakAtBoundOf,			@"" )\
    aString(breakAtPositionOf,      @"" )\
    aString(debugPositionOf,		@"" )\
    aString(debugOverlapOf,			@"" )\
	aInt   (breakAtOverlapNo,       0   )\
    aString(breakAtRenderOf,        @"" )\
/* Animation						   */\
    aInt   (buildingEpoch,			0	)/* no epochs yet encountered		*/\
/* 3D Sizes							   */\
    aFloat (bitHeight,              1.0	)/* radius of Port					*/\
    aFloat (bitRadius,              1.0	)/* radius of Port					*/\
    aFloat (atomRadius,				1.0	)/* radius of Atom					*/\
    aFloat (factalHeight,           3.0	)/* factal skin height				*/\
    aFloat (factalWidth,            4.0	)/* factal skin height xx factal skin width/depth/bigRadius*/\
    aFloat (factalTorusSmallRad,    0.5	)/* factalTorusSmallRad				*/\
    aFloat (bundleHeight,           4.0	)/* length of tunnel taper			*/\
    aFloat (bundleRadius,           1.0	)/* radius of post					*/\
/**  Links *****************************/\
    aFloat (fluffLinkGap,           1.0	)/* between boss and worker, if link*/\
    aFloat (directLinkGap,          0.2	)/* between boss,    worker  if direct*/\
    aFloat (gapWhenStacking,        2.0	)\
    aFloat (linkRadius,				0.25)/* radius of link (<0.2? 1pix line: <0? ainvis*/\
    aFloat (linkEventRadius,		-1  )/* radius of link (<0.2? 1pix line: <0? ainvis*/\
    aBool  (displayInvisibleLinks,	0	)/* Ignore link invisibility */\
    aBool  (displayAsNonatomic,		0	)/* Ignore initiallyAsAtom marking  */\
                                         \
    aFloat (signalSize,             1.0 )/* size of bands that display		*/\
    aFloat (boundsDrapes,           0.0 )/* size of bounds box corners (.08)*/\
    aFloat (displayOffsetX,			0.0	)/* between skins and rest			*/\
    aFloat (displayOffsetZ,			0.0	)/* between skins and rest			*/\
	aString(displayValuesMode,		segregated)/* colors of ups and downs	*/\
    aBool  (reViewConstantly,		0   )/* reViewInView every display pass */\
/* 3D Animation						   */\
    aFloat (linkVelocityLog2,      -4.0 )/* link velocity = 10^(~) units/sec*/\
    aBool  (animateBirth,			1	)/* when OFF, new elt is in final place immediately	*/\
    aFloat (animBirthDecayLog10,   -4.0 )/* anim offset decays = 10^(~)		*/\
	aInt   (flashFrames,			10	)/* Flash lasts this number of frames*/\
/** 11. 3D Display ******** 3D Display */\
    aBool  (displayPortNames,		1	)\
    aBool  (displayLinkNames,		1	)\
    aBool  (displayAtomNames,		1	)\
    aBool  (displayLeafNames,		0	)\
    aFloat (displayLeafFont,		0	)/* default 0 is no leaf names*/\
    aBool  (displayNetNames,		1	)\
    aBool  (displayAxis,			1	)\
    aBool  (displayLabels,			1	)\
    aInt   (fontNumber,				6   )/* default font index; 0 small, 6 big*/\
    aFloat (rotRate,                0.0003/(2*M_PI))\
    aRContxt(render3DMode,          render3Dcartoon)\
    aBool  (picPan,					0	)/* picking object pans it to center*/\
    aBool  (simsScreen,				0	)\
										 \
/** 13. Test Values ****		       */\
    aFloat (testValue1,             0   )/* from gui, to whomever			*/\
    aFloat (testValue2,             0   )/* from gui, to whomever			*/\
                                         \
/** 14. Logging ********** CONSOLE LOG */\
/* Construction 					   */\
    aString(logAllocationEventsOf,  @"" )\
	aBool  (logTerminalSearching,	0	)/* log searching points of contacts*/\
	aInt   (logTerminalSrchStartsAt,0	)/* log number when to start terminal searching logging*/\
	aBool  (logNsVcAccess,			0	)/* log events opening Inspectors and */\
/* Breakpoint						   */\
    aInt   (breakAtLog,			   -1    )/* stop on this log entry			*/\
/* Simulation Data					   */\
    aInt   (logEvents,				1   )/* log important stages when data flows */\
    aInt   (logDataPutsTakes,       0   )/* log all individual flow between Plugs */\
    aInt   (logTime,         0   )/* log the passage of time			*/\
                                         \
/** 15. PrettyPrint ****** PrettyPrint */\
    aInt   (ppUseNicknames,			1	)/* pp strings with nick names		*/\
    aInt   (ppUseQuotes,			0	)/* pp quote strings				*/\
    aInt   (ppLineWidth,		   95	)/* columnar width of log printout	*/\
    aInt   (ppNameCols,			    8	)/* number of cols for names		*/\
    aInt   (ppIndentCols,			8	)/* number of cols for indentation	*/\
    aInt   (ppTransShowMutateable,	0	)/* pp as {+, +}, [+, and +] if mutable*/\
    aBool  (ppLinks,				0	)/* PrettyPrint includes Links		*/\
    aBool  (ppPorts,				0	)/* PrettyPrint includes Ports		*/\
    aBool  (ppPartsEarlyBits,		1	)/* early is global order			*/\
	aBool  (ppParameters,			0	)/* print self.parameters			*/\
	aBool  (ppObjectAddress,		0	)/* print self.parameters			*/\
    aBool  (ppDagHeight,            0   )/* print out dag sort order		*/\
    aBool  (ppRetainCount_,         0   )\
                                         \
/*************** EXECUTION ENVIRONMENT */\
/* BREAKS							   */\
    aString (dataDirectory,			@"" )\
                                         \
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

#pragma mark 0. Class Objects
extern Brain *currentBrain;
typedef void	(^SettledOperation)(void);

@interface Brain : Net

	 // supports only 1 simNsWc right now
	@property (nonatomic, assignWeak) SimNsWc *simNsWc;		// backpointer

	 // Brain Construction
    @property (nonatomic, retain) id 		sourceLineInfo;	// accessTestLibrary fills this in
	@property (nonatomic, retain) id 		camera;			// brain's camera, destined for SimNsWc
//	@property (nonatomic, retain) Path 		*commonPath;

    @property (nonatomic, assign) int 		logNumber;		// ppLog logging message number
    @property (nonatomic, assign) short 	wireNumber;		// connection wires


	 ///// Special Entities known to the Brain
	@property (nonatomic, retain) NSMutableArray *timingChains;// array of TimingChains

	 ///// Simulator Status
	@property (nonatomic, assign) int 		unsettledOwned;	// unsettled; owner must return
						// e.g. Event in Link, or GenAtom while playing a sound.
	 ///// Simulator Control
	@property (nonatomic, assign) int 		extraCycles;	// number of extra cycles needed to settle


	 ///// Simulation Internals
	@property (nonatomic, assign) double 	timeNow;		// current simulation time
	@property (nonatomic, assign) bool 		globalDagDirDown;// current simulation is going down
		///// Dirty bits: Took the simple approach:
	   /// Only one bit per Brain.
	  /// ReView (or reBound) all views when model changes
	 /// (perhaps causes more ReView/ReBound's than needed)
	@property (nonatomic, assign) bool 		someViewDirty;	// ReView  all View hierarchies
	@property (nonatomic, assign) bool 		boundsDirty;	// ReBound all View hierarchies

		///// Simulation PARAMETERS defined by forAllBrainParameters macro:
	   ///
      // Define @properties of all vars defined in forAllBrainParameters
	 //   ==1==. definition of a variables in .h
		 #define  aString(name_, value_) @property (nonatomic, assign) NSString *name_;
        #define    aBool(name_, value_) @property (nonatomic, assign) bool      name_;
       #define     aInt(name_, value_) @property (nonatomic, assign) int       name_;
      #define   aFloat(name_, value_) @property (nonatomic, assign) float     name_;
     #define  aDouble(name_, value_) @property (nonatomic, assign) double    name_;
    #define aRContxt(name_, value_) @property (nonatomic,assign)RenderModes name_;
   /***************************************/
  /***/	 forAllBrainParameters    /***/		// Define controls as object properties
 /***************************************/		 // (This line expands to dozens of Properties)
    #undef aString
     #undef aBool
      #undef aInt
       #undef aFloat
        #undef aDouble
         #undef aRContxt



+ (void)		setCurrentBrain:(Brain *)aBrain;

#pragma mark 1. Basic Object Level
+ (Brain *)		currentBrain;

#pragma mark 2. 1-level Access
- (void)		defaultSimParams; 
- (id)          getParameter:key;
- (void)        setParameter:key toValue:val;
- (id)			setSimParams:(NSDictionary *)params;
//- (void)		setRunStateLoud:(RunState)val;

#pragma mark 3. Deep Access
//- (Brain *)	brain;

#pragma mark 4. Factory								// Compound objects
   Brain		*aBrain_selectedBy(int menuNo, int regressNo, id saughtName);
   id			accessTestLibrary(int want_menuNo, int want_regressNo,
							id want_name, bool want_itBuild, id want_metaInfo);
- (id)			groomModel;

#pragma mark 7. Simulator Messages
- (bool)		settled;
- (bool)		processKey:(char)character modifier:(unsigned long)modifier;

#pragma mark 8. Reenactment Simulator
- (void)		cycleSimulator;
- (void)		kickstartSimulator;

#pragma mark 15. PrettyPrint
- (NSString *)	ppSimulationParameters;

@end
