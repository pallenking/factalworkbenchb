// Mirror.h -- an atom which mirrors its input to its output C2014PAK

#import "Atom.h"
@interface Mirror : Atom

    @property (nonatomic, assign) float gain;
    @property (nonatomic, assign) float offset;

@end
