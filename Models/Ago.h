//  Ago.h -- Connects the Haves and Wants of 3 ports WIP C2015PAK


/*! Concepts:

		.--o--.
	S	|L|	  |		<-- clockPrevious loads L
	   /       \
	  (  Ago    )
	   \       /
	P	|   |L|			L is standard (unclocked)
		 --o--

 */

#import "Atom.h"
@interface Ago : Atom

#pragma mark  7. Simulator Messages
- (void)			clockPrevious;

@end
