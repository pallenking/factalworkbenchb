//  WriteHead.mm -- Adds new elements to record unknowns C2014PAK

#import "WriteHead.h"
#import "Branch.h"
#import "Ago.h"

#import "Splitter.h"
#import "Share.h"
#import "Link.h"
#import "View.h"
#import "Actor.h"
#import "Path.h"
#import "Net.h"
#import "Leaf.h"
#import "Brain.h"
#import "Id+Info.h"
#import "GlCommon.h"
#import <GLUT/glut.h>
#import "Common.h"
#import "NSCommon.h"
#import "SimNsWc.h"
#import "FactalWorkbench.h"

@implementation WriteHead

#pragma mark 1. Basic Object Level

- (void) dealloc; {

	self.babyPrototype	= nil;		// bad
	self.actor			= nil;		// bad
	self.baby			= nil;		// ok

	[super dealloc];
}

#pragma mark 3. Deep Access

- (id) deepMutableCopy; {		WriteHead *rv = [super deepMutableCopy];

	panic(@"test this out:");
	rv.babyPrototype	= self.babyPrototype;	// Class
	rv.actor			= nil;	//self.actor;	// Actor * ??
	rv.bulge			= self.bulge;
	rv.baby				= nil;					// Part *  ??
	rv.babyInView		= self.babyInView;
	rv.canalOpen		= self.canalOpen;
	rv.animationOffset	= self.animationOffset;
	
	return rv;
}

#pragma mark  4a Factory Access						// Methods defined by Factory
- (id) atomsDefinedPorts;	{			id rv = [super atomsDefinedPorts];

	rv[@"P"]		= @"p d";
	rv[@"S"]		= @"p  ";
	return rv;
}

#pragma mark 4. Factory

//+ (WriteHead *) an:(id)etc {		return aWriteHead(etc);		}
WriteHead *aWriteHead(id etc) {
	id info = @{n(write),		// cuts down source
				behindOthers,
				etcEtc}.anotherMutable;

	WriteHead *newbie = anotherBasicPart([WriteHead class]);
XX	newbie = [newbie build:info];
	return newbie;
}

- (id) build:(id)info;	{					[super build:info];

//! xzzy1 WriteHead::	1. actor			:<anActor>		-- to put babies
//! xzzy1 WriteHead::	2. babyPrototype	:<aPart>		-- kind of prototype default=Hamming

	if (id actor = [self takeParameter:@"actor"])
//	if (id actor = [self parameterInherited:@"actor"])
		self.actor = actor;	  // N.B: cannot resolve to Part here
	
	self.babyPrototype = nil;
	if (id babyPrototype = coerceTo(NSString, [self takeParameter:@"babyPrototype"])) {
//	if (id babyPrototype = coerceTo(NSString, [self parameterInherited:@"babyPrototype"])) {
		self.babyPrototype = NSClassFromString(babyPrototype);
		assert(self.babyPrototype, (@"babyPrototype '%@' not an NSClass", babyPrototype));
	}
	return self;
}

#pragma mark 6. Navigation

 // WriteHeads automatically add as many uppers and lowers as wanted
- (Port *) getOpenPort_openingDown:(bool)openingDown; {

	 // Presume no existing open -- make a new one!
  	Port *port = [self addPart:[Port another]];
	port.flipped = openingDown;
	
	 // Set Names
	int newBitsIndex = 0;
	for (id subPort_ in self.parts) {
		Port *subPort = coerceTo(Port, subPort_);
		if (subPort == port)
			break;
		newBitsIndex += subPort.flipped==port.flipped;
	}
	port.name = [@"" addF:@"%@%d", port.flipped? @"P": @"S", newBitsIndex];
	return port;
}

#pragma mark 7-. Simulation Actions
- (void) reset; {									[super reset];
	
	self.babyInView		= false;		// not yet!
	self.canalOpen		= false;		// don't deliver yet
	self.baby			= nil;
	
	// Finish construction: resolve WriteHead.actor: string -> Part
	if (id actor = coerceTo(NSString, self.actor)) {
		Path *actorPath = [Path pathWithName:actor];
		XX		self.actor		= (id)[self resolveOutwardReference:actorPath openingDown:false];
	}
}

#pragma mark 7. Simulator Messages
///***///***///***///***///***///***///***///***///***///***///***///***///
///***///***///***///***///***///***///***///***///***///***///***///***///
///***///***///***///***///***///***///***///***///***///***///***///***///
- (id) receiveMessage:(FwEvent *)event; {
	if (event->fwType == sim_writeHeadConcieve) {
		[self logEvent:@"|| (WriteHead *) gets 'writeHeadConcieve'"];
		[self conceiveBaby];
		return self;
	}
	else if (event->fwType == sim_writeHeadLabor) {
		[self logEvent:@"||  (WriteHead *) gets 'writeHeadLabor'"];
		self.canalOpen = true;					// can birth
	}
	return nil;			 // do not call super needed
}						// because the Previous manages everything inside itself.

- (void) conceiveBaby {

	  /////////// Scan all WriteHead Ports (representing Unknowns)
	 /// Gather info:
	int realWorkers = 0;						// exclusive of splice Hamming
	id  workerList	= @[].anotherMutable;		// worker terms
	id  bossList	= @[].anotherMutable;		// bosses to attach to
	for (Port *whPort in self.parts) {			// (WriteHeadPORT)
		assert(coerceTo(Port, whPort), (@"WriteHead part is not Port"));

		 // Find the unknownPort (in either evidence or con Bundle)
		Port *whUserPort = whPort.connectedTo;	// starting with unknown
		while ((whUserPort=whUserPort.atomsOtherBests_otherPort))
			if (!coerceTo(Link, whUserPort.atom))
				break;
		Atom *whUserAtom 			= whUserPort.atom;
		Port *toPrimaryPort 		= whUserAtom.pPortIn;

XX		if (whPort.flipped) {  			////// Found a WORKER:
			if (whUserPort.value < 0.5) 		// if not active enough to bother
				[whUserAtom logEvent:@"value=%.2f: IGNORED as worker", whUserPort.value];
			else {
				[whUserAtom logEvent:@"value=%.2f: ADDED   as worker", whUserPort.value];
				[workerList addObject:whUserAtom];	//// ADD whPort
				realWorkers++;
			}
		}								/////// Found a BOSS:
		else if (toPrimaryPort.value < 0.5) /// Boss OFF
			[whUserAtom logEvent:@"value=%.2f: IGNORED as worker", whUserPort.value];
		else {								/// Boss ON

			  ///// Search downward thru MaxOr tree to the best Hamming
			 /// (best = ?.?)
			Port *bestMaxorPort 	= nil;
			Port *scanPort 			= whUserAtom.pPort;	// start scan
			for (; scanPort!=nil; scanPort=scanPort.atomsOtherBests_otherPort) {
				Class shareClass = coerceTo(Splitter, scanPort.atom).shareClass;
				if (shareClass == Hamming.class  or  shareClass == Ago.class)
					break;									// Stop scan
				if (shareClass == MaxOr.class)
					bestMaxorPort = scanPort;				// Record lowest
			}
			assert(scanPort, (@"FAILED to Find best Hamming in MaxOr tree"));

			   // This is the essence of the Splice algorithm, right here!
			  // 1. The new element will occupy the same MaxOr as the birthplace
			 //
			[bossList addObject:mustBe(Atom, bestMaxorPort.atom)];//// ADD whPort

			  // 2. The new element will chain off of old birthPort
			 //
			if ([scanPort.atom.name isEqualToString:@"squelch"])
				[whUserAtom logEvent:@"value=%.2f: IGNORED -- squelch", whUserPort.value];
			else {
				[whUserAtom logEvent:@"value=%.2f: ADDED   as worker splice-point", whUserPort.value];
				[workerList addObject:scanPort];	// add the splice
			}
		}
	}

	 // A hack:
	if (self.babyPrototype == [Branch class])	// Branches currently
//		panic(@"");
		[bossList addObject:self];					// do not dealwith contexts

	 // Resolve self.actor to a Part in the net if it's a string
	if (id actorStr = self.actor.asString) {
		Path *actorPath = [Path pathWithName:actorStr];
XX		self.actor = (id)[self resolveOutwardReference:actorPath openingDown:false];
	}
	mustBe(Actor, self.actor); //WriteHead could not find Actor

	if (realWorkers <= 0)
		return;

	
	 /////////////// Each Active CONtext operates independently ///////////////
	for (id con in bossList) {
		self.baby.writeHead 		= nil;		// disconnect any previous baby from us
		self.baby 					= nil;
		[self.brain setEnclosedPortWriteHeads:nil];	// remove previous users

		   ///////////////////////////////////////////////////////////
		  /////            Give birth to a new element            /////
		 ///////////////////////////////////////////////////////////////
		Class proto 				= self.babyPrototype;
		assert(proto, (@"WriteHead babyPrototype not specified"));
XR		if (Part *p=[proto conceiveBabyIn:self evi:workerList con:con]) {
			[self logEvent:@"          Adding New Element."];
		    ///\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
		     ///\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

XR			[self.brain groomModel];			// Wire up model

			 // Set up baby to (shortly) put the baby in the birth canal:
			self.babyInView		= false;		// not yet!	First draw sets canal place, and this to true
			self.canalOpen		= false;		// don't deliver yet
			self.baby.writeHead = self;			// baby gets animation at self

			[self.baby setEnclosedPortWriteHeads:self];	// every Part within baby relocated

			 // TRIGGER START of ANIMATION
			[self.baby reset];
			[self.brain kickstartSimulator];
		}
	}
}

- (Vector3f) animateBirthOfView:(View *)babysView; {

	 /// New part created (conceived): implant it into Views
	if (!self.babyInView) {
		self.babyInView	= true;						// mark as now in view
		[self logEvent:@"New baby '%@' is in view %@\n", self.baby.name, babysView.name];

		assert(self==babysView.part.writeHead, (@"not connected corrctly"));
	
		View *writeHeadsView = [babysView searchOut4Part:self except:nil];		// view from anywhere in tree
		assert(writeHeadsView, (@"could not writeHead's view"));
		assert(writeHeadsView.part==self, (@"writeHead's view malformed"));

		 // Calculation is done in outterView (OUTteR), writeHead's superView:
		View *outrView		= writeHeadsView.superView;

		 // Spot where we were born, in outr
		Vector3f birthInWh	= writeHeadsView.bounds.centerZBack();// opening of birth canal in WriteHead
		Matrix4f wh2outr	= [writeHeadsView xformToView:outrView];
		Vector3f birthInOutr= wh2outr * birthInWh;
		
		Vector3f babyInBaby	= babysView.bounds.center();
		Matrix4f baby2outr	= [babysView xformToView:outrView];
		Vector3f babyInOutr	= baby2outr * babyInBaby;

		 // offset calculated to make baby appear at birthspot, not final.  (but it decays to final)
		self.animationOffset= zeroVector3f;
		if (self.brain.animateBirth)
			self.animationOffset= birthInOutr - babyInOutr;
		assert(!isNan(self.animationOffset), (@"Animation Offset is NAN!"));
	}

	 /// In process of moving from writeHead into Mondel
	if (self.canalOpen and self.brain.simEnable>0) {
		  // a bit of a hack here. Hold if simEnable<=0, as there is just post birth
		float log10Decay	= self.brain.animBirthDecayLog10;
		float decay			= powf(10,log10Decay);
		float mult			= fmax(1.0 - decay, 0);
		self.animationOffset *= mult;				// decay
	}

	View *actorView			= [babysView searchOut4Part:self.actor except:nil];
	Matrix4f babiesPop2actr = [babysView.superView xformToView:actorView];
	Matrix3f actr2babiesPop = babiesPop2actr.getRotation().inverse();
	Vector3f rv				= actr2babiesPop * self.animationOffset;
	return rv;
}

#pragma mark 8. Reenactment Simulator
- (void) simulateDown:(bool)downLocal; {
	for (Port *port in self.parts) {
		mustBe(Port, port);
		[port.connectedTo valueGet];		// to clear "changed" status?
	}
}

#pragma mark 9. 3D Support
float writeHeadHeight =  7.0;		// Y
float writeHeadWidth  =  6.0;		// X
float writeHeadDeapth = 10.0;		// Z

- (Bounds3f) gapAround:(View *)v; {
	return Bounds3f(-writeHeadWidth/2,-writeHeadHeight/2,               0,
					 writeHeadWidth/2, writeHeadHeight/2, writeHeadDeapth);
}

- (Matrix4f) positionOfPort:(Port *)port inView:(View *)bitV; {
	Bounds3f parentBounds = bitV.superView.bounds;
	Vector3f bitSpot =
		[port.name hasPrefix:@"P"]? parentBounds.centerYBottom():
		[port.name hasPrefix:@"S"]? parentBounds.centerYTop():		zeroVector3f;
	bitSpot.z -= 2;

	return [port suggestedPositionOfSpot:bitSpot];
}

#pragma mark 11. 3D Display
- (void) drawFullView:(View *)v context:(RenderingContext *)rc; {
	float tau = 0.93;
	float bulge = self.babyInView and !self.canalOpen;
	self.bulge = tau*self.bulge + (1.0-tau)* bulge;  //(self.babyInView and !self.canalOpen);
	rc.color = lerp(colorRed, colorPink5, self.bulge);

	float diam = 3 + 0.5*self.bulge;
									// Write Head looks like a Blunderbust
	glPushMatrix();								// Barrel
		glTranslatef(0, 0, writeHeadDeapth*0.40);
		myGlSolidCylinder (1, writeHeadDeapth*0.75, 16, 3);//radius,  height,  slices,  stacks)
	glPopMatrix();

	glPushMatrix();								// Flare
		glRotatef(180, 0,1,0);
		glTranslatef(0, 0, -writeHeadDeapth);
		glutSolidCone(2, 2, 16, 3);
	glPopMatrix();
	glPushMatrix();								// Bulb
		glTranslatef(0, 0, diam);
		glutSolidSphere(diam, 16, 16);
	glPopMatrix();

	[super drawFullView:v context:rc];
}

- (NSString *) pp1line:aux; {		id rv=[super pp1line:aux];

	if (Actor *turt = coerceTo(Actor, self.actor))
		rv=[rv addF:@"actor='%@'", turt.fullName16];
	else if (self.actor.asString)
		rv=[rv addF:@"actor=''%@''", self.actor];
	else if (self.actor==nil)
		rv=[rv addF:@"actor='nil'"];
	else
		rv=[rv addF:@"actor= ?????"];

	return rv;
}

@end
