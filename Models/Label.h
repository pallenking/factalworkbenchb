//  Label.h -- display a text label in 3D space C2015PAK

#import "Part.h"

@interface Label: Part
    
    @property (nonatomic, retain) id  text;
    @property (nonatomic, assign) int fontNumber;

@end
