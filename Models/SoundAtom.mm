// SoundAtom.mm -- an Atom to connect to Generators C2014PAK

#import "SoundAtom.h"
#import "Brain.h"
#import "FactalWorkbench.h"
#import "View.h"
#import "Leaf.h"

#import "Common.h"
#import "NSCommon.h"
#import "Id+Info.h"
#import <GLUT/glut.h>
#import "GlCommon.h"

@implementation SoundAtom


#pragma mark 1. Basic Object Level

- (void) dealloc {
	self.sound			= nil;
	self.soundObject	= nil;

	[super dealloc];
}

#pragma mark  2. 1-level Access
- (void) setSound:sound; {

	 /// Standard Retain accessor:
	id prevSound = sound;
	_sound = [sound retain];
	[prevSound release];

	if (!sound)
		self.soundObject = nil;
	else if (id resourcePath = [[NSBundle mainBundle] pathForResource:sound ofType:@"m4a"]) {
		@try {////////////
			id x = [[[NSSound alloc] initWithContentsOfFile:resourcePath byReference:YES] autorelease];//NO
			self.soundObject = x;
		}	  ////////////
		@catch (NSException * e) {
			panic(@"ERROR Loading %@: '%@'", resourcePath, e.reason);
		}
	}
	else
		panic(@"Sound '%@' not found", sound);
	int i=33;
}

#pragma mark  3. Deep Access
- (id) deepMutableCopy; {				SoundAtom *rv = [super deepMutableCopy];

	rv.sound			= self.sound;	// presume its okay to share sound object
	rv.soundObject		= self.soundObject;
	rv.playing			= self.playing;

	return rv;
}

#pragma mark 4. Factory
SoundAtom *aSoundAtom(id etc) {
	id info = @{etcEtc}.anotherMutable;

	SoundAtom *newbie = anotherBasicPart([SoundAtom class]);
XX	newbie = [newbie build:info];
	return newbie;
}

- (id) build:(id)info;	{					[super build:info];

//!	xzzy1 SoundAtom:: 	3. sound			:string			--

	if (id sound		= [self takeParameter:@"sound"])
		self.sound		= sound;

	return self;
}

- (bool) applyProp:prop withVal:val; {

	if ([prop isEqualToString:@"sound"]) {	// e.g. "sound:di-sound" or
		panic(@"Must debug! old code was:");
		self.sound			= mustBe(NSString, val);		//sound's val must be string
		return true;							// found a spin property
	}
XR	return [super applyProp:prop withVal:val];
}


#pragma mark 8. Reenactment Simulator
- (void) simulateDown:(bool)downLocal;  {

	if (!downLocal) {				///////// going UP /////////

		Port *pPortCon = self.pPort.connectedTo;
		if (pPortCon.valueChanged) {			// Input = other guy's output
			float valPrev 	= pPortCon.valuePrev;
			float val 		= pPortCon.valueGet;// Get value from S

XX			self.sPort.valueTake = val;			// Pass it on to P

			 // Rising edge starts a Sound
			if (val>=0.5 and valPrev<0.5)	 	// Rising Edge +
				if (self.soundObject) {				// sound ==> play
					[self logEvent:@"starting sound '%@'", self.sound];
					if (self.soundObject.playing)
						NSPrintf(@"\n\n NOTE: Going TOO FAST\n\n");
					 // If this terminates the previous sound, it's okay!
					[self.soundObject play];			// start playing sound
					self.playing = true;				// delay loading primary.L
				}
		}
	}

	if (downLocal) {				///////// going DOWN ////////////

XX		if (self.sPortIn.valueChanged)
			self.pPort.valueTake = self.sPortIn.valueGet;
	}

	return;
}

#pragma mark 9. 3D Support

- (Bounds3f) gapAround:(View *)v;	{	return Bounds3f(-1.0, 0.0,-1.0,    1.0, 2.0, 1.0);}

#pragma mark 11. 3D Display
- (void) drawFullView:(View *)v context:(RenderingContext *)rc; {
	float r = 3 * self.brain.bitRadius;
	glPushMatrix();
		rc.color = self.soundObject.playing? colorOrange: colorYellow;
		glTranslatef(0, 0.5*r, 0);
		glScalef(r/2, r/3, r/2);
		glutSolidCube(1.0);
		glRotated(45, 0,1,0);
		glutSolidCube(1.0);
	glPopMatrix();

	[super drawFullView:v context:rc];				// PRIMARY
}

- (FColor)colorOf:(float)ratio;		{	return colorOrange;}

#pragma mark 15. PrettyPrint
- (NSString *)	pp1line:aux; {		id rv=[super pp1line:aux];

	if (self.sound)
		rv=[rv addF:@" sound:%@", self.sound];
	if (self.playing)
		rv=[rv addF:@"playing"];
	assert(!(self.playing and !self.sound), (@"should not be!!!"));

	return rv;
}

@end
