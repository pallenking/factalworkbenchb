// Part.mm -- a simulation object with physical and containment abilities C2011 King Software

/* To Do:
 */

#import "Part.h"
#import "ParameterPort.h"
#import "Brain.h"
#import "Bundle.h"
#import "Leaf.h"
#import "Actor.h"
#import "GenAtom.h"
#import "Previous.h"
#import "Label.h"
#import "Link.h"
#import "View.h"
#import "LinkView.h"
#import "WriteHead.h"
#import "Model01.h"
#import "Path.h"
#import "FactalWorkbench.h"
#import "SimNsV.h"
#import "SimNsVc.h"
#import "SimNsWc.h"

#import	"Id+Info.h"
#import "Common.h"
#import "NSCommon.h"
#import "GlCommon.h"
#import <GLUT/glut.h>
#include <stdio.h>			// sscanf
#import "vmath.hpp"
#include <math.h>

@implementation Part

#pragma mark 1. Basic Object Level

- init; {											self = [super init];

	self.placeSelf = self.placeParts = nilPlaceType;	// default: no placement info
	self.parts = [NSMutableArray array];	// space for my parts (as yet unborn)
	return self;
}

- (void) dealloc; {
	self.fullNameLldb	= nil;
	self.parts			= nil;			// second
	self.parameters		= nil;			// works differently if first: $wh.rc: 2->1
//	self.brainCache		= nil;			// invalidate cache (180805 ??)
	self.writeHead		= nil;			// 161231: /brain1/n1 releases writeHead

    [super dealloc];
}

#pragma mark 2. 1-level Access

- (id) addPart:(Part *)part atIndex:(int)index; {
	assert(self!=part, (@"cant be part of self"));
	assert(coerceTo(Part, part), (@"attempt to add a non-Part"));
	if (index >= 0)
		[self.parts insertObject:part atIndex:index];
	else
		[self.parts addObject:part];

    part.parent			= self;
	part.fullNameLldb	= part.fullName;
	self.brain.someViewDirty= true;		// BUGGY, perhaps ineffective
	return part;
}
 // Add at end:
- (id) addPart:(Part *)part; {	return [self addPart:part atIndex:-1];		}

- (Port *) partAtIndex:(int)index; {
	assert(index>=0 and index<[self.parts count], (@""));
	return self.parts[index];
}

- (Part *) partNamed:(NSString *)name; {
	for (id elt_ in self.parts)		// set fullName of parts
		if (Part *elt = coerceTo(Part, elt_))
			if ([elt.name isEqualToString:name])
				return elt;
	return nil;								// none found
}

- (NSUInteger) indexOfPart:part; {
	return [self.parts indexOfObject:part];
}

- (int) numParts;			{	return (int)[self.parts count];		}	// for GUI

- (id) parameter:(id)key;	{	return self.parameters[key];		}

 // Parameters are often removed (e.g. after processing)
- (id) takeParameter:(id)key; {
	if (id rv = self.parameters[key]) {
		[self.parameters removeObjectForKey:key];
		return rv;
	}
	return nil;
}

  // pull out all keys refering to subnodes with prefix
 //  e.g. prefix "con" matches "conAbcde":val and returns "Abcde":val
- (id) parametersWithPrefix:prefix; {
	id rv = @{}.anotherMutable;
	for (id aKey in self.parameters)
		if ([aKey hasPrefix:prefix]) {
			NSRange r = [aKey rangeOfString:prefix];
			int startPos = (int)r.length+1;
			if (startPos < [aKey length]) {
				NSString *subKey = [aKey substringFromIndex:startPos];
				rv[subKey] = self.parameters[aKey];
			}
		}
	return rv;
}

 // Parameters are inherited by parents:
- (id) parameterInherited:(id)key; {

	 // Case 1: Maybe it's defined in this part's parameters
	if (id rv = self.parameters[key])
		return rv;

	 // Case 2: Maybe we have a parent and it knows
	id parent = self.parent;
XR	if (id rv = [parent parameterInherited:key])
		return rv;

	 // Case 3: MaybeOur Brain knows
	if (id param = [self.brain getParameter:key])
		return param;
	
	 // Case 4: Not defined
	return nil;
}


#pragma mark  3. Deep Access

- (id) deepMutableCopy; {		Part *rv = [super deepMutableCopy];

	rv.fullNameLldb		= self.fullNameLldb.deepMutableCopy;

	rv.parent			= self.parent;
//	rv.parts			= self.parts.deepMutableCopy;	// 171217 FAILS
	for (id part in self.parts)	{ // do all keys and values:
		if (Part *pPart = coerceTo(Part, part))
			[rv addPart:pPart.deepMutableCopy];
		else if (NSNumber *n = coerceTo(NSNumber, part))
			[rv.parts addObject:n];
		else
			panic(@"");
	}
	rv.parameters		= self.parameters.deepMutableCopy;
	rv.brainCache		= self.brainCache;

	rv.placeSelf		= self.placeSelf;
	rv.placeParts		= self.placeParts;
	rv.flipped			= self.flipped;
	rv.latitude			= self.latitude;
	rv.spin				= self.spin;
	rv.shrink			= self.shrink;

	rv.initialDisplayMode = self.initialDisplayMode;
	rv.flash 			= self.flash;

	rv.jog				= self.jog;
	rv.writeHead		= self.writeHead;
	return rv;
}

 // Forward Part Dirty to brain Dirty
- (void)setViewDirty:(bool)val; 	{	self.brain.someViewDirty = val; }
- (void)setBoundsDirty:(bool)val; 	{	self.brain.boundsDirty   = val;	}

 // a handy iterator:
- (void) forAllParts:(PartOperation)partOperation  {
	for (id elt in self.parts) {
		if (NSNumber *n = coerceTo(NSNumber, elt))
			;
		else if (Part *p = coerceTo(Part, elt)) {
			partOperation(elt);
XR			[elt forAllParts:partOperation];
		}
		else
			panic(@"");
	}
}


- (Brain *) brain; {						// Lazy Getter:
	 // N.B: would like to /brainCache/brain/g
	if (self.brainCache)						// 1. Brain cached in self?
		return mustBe(Brain, self.brainCache);		// return it

	 // No Brain in Part, try parent's
	if (id parent = self.parent)				// 2. Has a parent
XR		if ((self.brainCache = coerceTo(Brain, [parent brain])))// If it has a brain
			return self.brainCache;					// use it and cache it

	return Brain.currentBrain;					// 3. get last used Brain
}

 ///// Set for us, and our subparts /////
- (void) setBrain:(Brain *)brain_; {
	self.brainCache = brain_;					// set myself
	panic(@"should the following be activated?");
//	for (id elt_ in self.parts)					// set all my parts
//		if (Part *elt = coerceTo(Part, elt_))
//			elt.brain = brain_;			//xyzzy SETbRAIN CACHE
}

///////////////////////////////
- (void) setName:val; {

	if (self.name and !coerceTo(Port, self) and val)
		[self logEvent:@"--renamed-> %@", val];

	[super setName:val];

	 // Set up full name
	if (self.parent)
		self.fullNameLldb = self.fullName;	// set my fullNameLldb
}

- (NSString *) fullName; {
	char c = coerceTo(Port, self)? '.': '/';
XX	NSString *parentsFullName = self.parent? self.parent.fullName: @"";
	return [parentsFullName addF:@"%c%@", c, self.name];
}

- (const char *) fullNameC; {	return self.fullName.UTF8String;		}

  // 150710: I learned that lldb summaries do not go through getters;
 // fullNameLldb must be called once in advance.
- (void) setAllFullNamesLldb; {
	self.fullNameLldb = self.fullName;	// set my fullNameLldb

	for (id part in self.parts)		// set fullNameLldb of parts
		if (Part *p = coerceTo(Part, part))
			[p setAllFullNamesLldb];
}

  /// Set Splitter distributions in whole network
- (void) setDistributions; {
	
	for (id part in self.parts)		// set fullNameLldb of parts
		if (Part *p = coerceTo(Part, part)) {
			//NSPrintf(@"===== %@: ['%@' setDistributions]\n", self.fullName, p.name);
			[p setDistributions];
		}
}

- (NSString *) fullName:(int)len; {
	id str = [self fullName], rv=0;
	int strOver = (int)[str length] - len;
	if (strOver >= 0)
		rv = [@"" addF:@"..%@", [str substringFromIndex:strOver+2]];		// skip start of string
	else
		rv = [@"" addF:@"%*s%@", -strOver, " ", str];
	return rv;
}

- (NSString *) fullName16;			{	return [self fullName:16];			}
- (const char *) fullName16C;		{  	return self.fullName16.UTF8String;	}

///////////////////////////////

- (int) portCount; {
	int rv = 0;
	for (Part *elt in self.parts)
		if (Part *part = coerceTo(Part, elt))
			rv += part.portCount;
	return rv;
}

- (void) setEnclosedPortWriteHeads:val; {
	for (id elt in self.parts)
		if (Part *p = coerceTo(Part, elt))
			[elt setEnclosedPortWriteHeads:val];
}

- (Net *) enclosingNet; {
	for (Part *s=self; s; s=s.parent)			// Search outward
		if (Net *n = coerceTo(Net, s))					// return first Net found
			return n;
	return nil;
}
- (id) enclosedByClass:(Class)cxlass; {
	if (id classString = coerceTo(NSString, cxlass))
		cxlass = NSClassFromString(classString);	 // get the appropriate class object

	for (Part *s=self; s; s=s.parent)			// Search outward
		if ([s isKindOfClass:cxlass])
			return s;
	return nil;
}
- (bool) hasAsAncestor:(Part *)ancestor; {
	for (Part *slf = self; slf.parent; slf=slf.parent)
		if (slf == ancestor)
			return true;
	return false;
}

Net *smallestNetEnclosing(Part *m1, Part *m2) {
	NSArray *a1 = [m1 ancestorArray];	// of Parts
	Net *rv = 0;

	 // just 1 Part supplied or both Parts the same
	if (!m2 or  m2 == m1)	{
 		 // find smallest parent that is a Net
		for (id m in [a1 reverseObjectEnumerator])
			if (Net *mNet = coerceTo(Net, m))
				return mNet;
	}

	 // 2 Parts supplied -- find the smallest Net they have in common
	else {
		NSArray *a2 = [m2 ancestorArray];
		for (int i=0, n=(int)min(a1.count, a2.count); i<n and a1[i]==a2[i]; i++)		// continue to leaf while matching
			if (Net *a1Net = coerceTo(Net, a1[i]))
				rv = a1Net;
	}
//	assert(rv, (@"smallestNetEnclosing finds nothing"));
	return rv;					// no Net in any of m1's parents
}
- (NSArray *) ancestorArray; {
	NSMutableArray *ancestors = @[].anotherMutable;//[NSMutableArray arrayWithCapacity:10];
	for (Part *ancestor = self; ancestor; ancestor=ancestor.parent)
		[ancestors insertObject:ancestor atIndex:0];
	return ancestors;
}
- (Part *) ancestorThatsChildOf:(Part *)anAncestor; {
	for (Part *p = self; p; p=p.parent)
		if (p.parent == anAncestor)
			return p;				// return last uncle before parent
	return nil;
}

- (Actor *) actor; {		return [self enclosedByClass:@"Actor"];		}
- (Leaf *)  leaf; {			return [self enclosedByClass:@"Leaf"];  	}

 // return an array of resolved references:
- (Part *) resolveInwardReference:(Path *)path openingDown:(bool)downInSelf except:exception; {
	mustBe(Path, path);

	 // full path matches self's name
	if ([path fullNameMatches:self]) {
		if (self.brain.logTerminalSearching)
			[self logEvent:@"   MATCHES Inward check"];
		return self;
	}
	if (self.brain.logTerminalSearching)
		[self logEvent:@"   FAILS   Inward check.       ...try subs:"];

	  // Go through all sub elements, checking for a match.
	 //
	for (id elt in self.parts)
		if (coerceTo(NSNumber, elt))
			;
		else if (elt != exception) {					// don't redo our entry tree
			bool downInEnt = !downInSelf ^ !self.flipped;

XR			if (id rv = [elt resolveInwardReference:path openingDown:downInEnt except:self])
				return rv;								// Found One
		}
	return nil;									// Nothing Found
}


- (Part *) resolveOutwardReference:(Path *)path openingDown:(bool)downInSelf; {
	id exception = nil;

	for (Part *selfNParents=self; selfNParents; selfNParents=selfNParents.parent) {
		if (self.brain.logTerminalSearching)
			[selfNParents logEvent:@"\nSearch Outward for %@ %@. Search Inwards...", path.pp, downInSelf?@"dn":@"up"];
XX		if (id rv = [selfNParents resolveInwardReference:path openingDown:downInSelf except:exception])
			return rv;

		 // Failed, go up to parent, and search all but us
		exception = selfNParents;				// don't re-search where we came from
		downInSelf ^= self.flipped;
	}
	return nil;							// till the brain
}

#pragma mark 3b. Facing Down

 // ================= to a related Part:
- (bool) downInPart:(Part *)part; {
	mustBe(Part, part);
	return !self.downInWorld ^ !part.downInWorld;
}

 //================= to the world:
- (bool) downInWorld;{
	char rv = 0;
	for (Part *p=self; p.parent; p=p.parent)
		rv ^= p.flipped;		// rv now applies from self
	return rv;
}

#pragma mark 4. Factory
id anotherBasicPart(Class class1) {
	assert(!coerceTo(NSString, class1), (@"anotherBasicPart: class=%lx ILLEGAL", (LUI)class1));

	 // Bare Bones object. Nothing of info but className here
XX	Part *newbie = [[[class1 alloc] init] autorelease];	 // Make and Initialize

	return mustBe(Part, newbie);
}

- (id) build:(id)info; {							int i=33;

//! xzzy1 Part::		0. named:<name>						-- Part has name name
//! xzzy1 Part::		0. name:<name>						-- Part has name name
//! xzzy1 Part::		1. initialDisplayMode:@int		-- When constructing views, do
//!	xzzy1													2=atomic, 4=open, 8=invisible
//!	xzzy1 Part::		2. parts			:@[]			-- insides
//! xzzy1 Part::		3. flipped			:@bool			-- Part is flipped in Y
//!	xzzy1 Part::		4. latitude			:@int/@""		--  0123parameterX
//!	xzzy1													0=north;1=capricorn;2=equator;3=cancer;4=south
//! xzzy1 Part::		5. placeSelf		:ArrayX			-- of info to build Part's Parts
//! xzzy1 Part::		6. placeParts		:<bool>
//! xzzy1 Part::		7. jog				:@""			-- delta "x,y,z"

	 //////// The Base of the call:  move flattened info to self.parameters
	self.parameters = flattenFromInto(info, self.parameters);// flatten info into "parameters" (TO be DOne).
	//[info removeAllObjects];	// 170122 info no longer relevant, dangerous to leave around, but can't remove

	[self logEvent:@"\nBUILD %@(%@)", self.className, self.parameters.pp];

	//brain  = Brain.shared;
	if (id name = [self takeParameter:@"name"]?: [self takeParameter:@"named"])
		self.name = name;

	if (id val = [self parameterInherited:@"initialDisplayMode"]) {
		if (id v = initialDisplayModeNames[val])	// symbolic name
			val = v;
		self.initialDisplayMode = (DisplayMode)[val intValue];
	}

	  //////// Now do normal configuration
	 // ADD PARTS into a Part
//	if (id parts = [self parameterInherited:@"parts"]) {
	if (id parts = [self takeParameter:@"parts"]) {
		if ([parts asNumber])
			assert([parts intValue]==0, (@""));		// @0 means no parts, silently
		else if ([parts isArray])
			for (id part in parts) {
				if (coerceTo(Part, part))			// Part is a PART:
					[self addPart:part];				// add part
				else if ([part isArray])			// Part is ARRAY:
					for (id compIj in part)				// add to self
XR						[self addBasicPart_kind:compIj];
				else if ([part isHash])	{			// Part is HASH:
					if (part[@"isa"])					// build elt per hash
						[self addBasicPart_kind:part];
					else								// modify self per hash
						[self build:part];					// DANGEROUS
//						panic(@"");
				}
				else if ([part asNumber])			// Part is NUMBER:
					[self.parts addObject:part];		// stopIfNotLevel marker
					//[self addPart:part];					// use back door
					  // 161005 Animation puts level mearkers in parts.
						// Later when viewing, if we're not to the level, we skip the rest
//	  				; // panic(@"");
			}
		else										// Scan through container Array
			panic(@"parts have unknown kind");
	}

	  ///// Placement of Part: parameters --> properties
	 ///
	if (id flip2 = [self takeParameter:@"flip"])				// FLIP:
		[self applyProp:@"flip" withVal:flip2];
	if (id val = [self takeParameter:@"spin"])					// SPIN:
		[self applyProp:@"spin" withVal:val];

	if (id latitude = [self takeParameter:@"latitude"])			// LATITUDE:
		self.latitude = [latitude intValue];
//	if (id flip2 = [self parameterInherited:@"flip"])			// FLIP:
//		[self applyProp:@"flip" withVal:flip2];
//	if (id val = [self parameterInherited:@"spin"])				// SPIN:
//		[self applyProp:@"spin" withVal:val];
//
//	if (id latitude = [self parameterInherited:@"latitude"])	// LATITUDE:
//		self.latitude = [latitude intValue];

	if (NSString *placeSelfStr = [self takeParameter:@"placeSelf"])	// [S,W][+,-][x,y,z] //parameterInherited
		self.placeSelf = str2placeType(placeSelfStr);
	if (NSString *placeParts = [self takeParameter:@"placeParts"]) //parameterInherited
		self.placeParts = str2placeType(placeParts);

	 // JOG POSITION (an expedient in certain compositions
	if (NSString *val = [self takeParameter:@"jog"]) {
//	if (NSString *val = [self parameterInherited:@"jog"]) {
		Vector3f jog;
		const char *s = val.UTF8String;
		sscanf(s, "%f,%f,%f", &jog.x, &jog.y, &jog.z);
		self.jog = jog;
	}												// PLACEMENT:

	return self;
}

// SwiftFactals: now groomModel
- (void) postBuild;						{					}

- (id) addBasicPart_kind:(id)classname; {
	Part *rv = anotherBasicPart(classname);
	if (Part *ps = coerceTo(Part, self)) {
		if (Part *pRv = coerceTo(Part, rv))
			[ps addPart:pRv];
	}
	else
		panic(@"");
	return rv;
}

 // shortcuts e.g: "s1" really is "spin":"1"
const id str2spin = @{
	    @"s0":@{spin$0},     @"s1":@{spin$1},     @"s2":@{spin$2},     @"s3":@{spin$3},
	@"spin_0":@{spin$0}, @"spin_1":@{spin$1}, @"spin_2":@{spin$2}, @"spin_3":@{spin$3},
							 @"sR":@{spin$1}, 						   @"sL":@{spin$3}, };

- (bool) applyPropNVal:(NSString *)propNVal; {

	NSArray *tokens = [propNVal componentsSeparatedByString:@":"];
	if (tokens.count == 1) {			// v--- e.g: "s3" --> "spin":@"3"
		if (id opts = coerceTo(NSDictionary, str2spin[tokens[0]])) {
			assert([opts count]==1, (@"should be just 1 by construction"));
			if (id key = [opts allKeys][0])			// key="spin", val=3
				return [self applyProp:key withVal:opts[key]];	// more parsed form
		}
	}
//	 // process key:value
//	else if (tokens.count == 2)
//		return [self applyProp:tokens[0] withVal:tokens[1]];
	else
		panic(@" add stuff here ");
	return false;
}

- (bool) applyProp:prop withVal:val; {

	if ([prop isEqualToString:@"name"] or	// e.g. "name:<nameStr>" or
		[prop isEqualToString:@"named"]) {	//		"named:<nameStr>"
		self.name = mustBe(NSString, val);
		return true;							// found a flip property
	}

	if ([prop isEqualToString:@"flip"]) {	// e.g. "flip:1"
		assert([val respondsToSelector:@selector(intValue)], (@"flip's val must be string or number"));
		int flipVal = [val intValue];
//		if (coerceTo(NSNumber, val) or coerceTo(NSString, val))
//			flipVal = [val intValue];
//		else
//			panic(@"flip's val must be string or number");
		self.flipped = !self.flipped ^ !flipVal;
		return true;							// found a flip property
	}

	if ([prop isEqualToString:@"spin"]) {	// e.g. "spin:3"
		int spinVal = 0;						// initiall no spinVal
		if (coerceTo(NSNumber, val))			// NSNumber
			spinVal = [val intValue];				// carries spin value
		else if (coerceTo(NSString, val)) {		// NSString

			 // Production: symbolic "r" --> 1
			const id lr2int   = @{  @"r":@"1", @"R":@"1", @"l":@"3", @"L":@"3"};
			if (id nVal = lr2int[val])
				val = nVal;						// symbolic spin --> numeric spin
			spinVal = [val intValue];		// N.B: "xxx" --> 0 !!
		}
		else
			panic(@"spin's val must be string or number");
		assert(spinVal>=0 and spinVal<4, (@"spinVal %d out of range", spinVal));

		self.spin += -spinVal + 4;			// add 4 more to insure 'C's "%" doesn't get neg arg1)
		self.spin %= 4;
		return true;						// found a spin property
	}
//
//	if ([prop isEqualToString:@"sound"]) {	// e.g. "sound:di-sound" or
//		panic(@"");
////		if (coerceTo(NSString, val)) {
////			Leaf *leaf			= mustBe(Leaf, self);
////			Port *genPort		= [leaf port4leafBinding:@"G"];
////			GenAtom *genAtom 	= mustBe(GenAtom, genPort.atom);
////			genAtom.sound		= val;
////		}
////		else
////			panic(@"sound's val must be string");
////
////		return true;							// found a spin property
//	}
	return false;
}

#pragma mark 4b. Factory Support:

id flattenFromInto(id from, id into) {
	if (into == nil)							// 'into' must exist
		into = @{};
	if (![into isMutable])						// 'into' must be changeable
		into = [into anotherMutable];

	if (from == nil)							// no from
		;											// no work; return an empty into.
	else if ([from asHash]) {

		 /// FIRST flatten all but "etc" key (into "into")
		for(id key in from) {
			if ([key isEqualToString:@"etc"])		// "etc" key done later
				continue;

			id newVal = from[key];
			if (id oldVal = into[key]) {		// already an element there:

				  ///// overwriting an array with an array merges contents
				 ///
				if (coerceTo(NSArray, newVal) and coerceTo(NSArray, oldVal)) {

					 //check for conflicts?
					for (id oldSub in oldVal) {
						for (id newSub in newVal)
							if ([oldSub isEqualTo:newSub]) {
								if (![newVal isMutable])
									newVal = [newVal anotherMutable];
								panic(@"got some code to write! (check merge)");
							}
					}
				}

				  ///// overwriting flip EXORs into it.
				 ///
				else if ([key isEqualToString:@"flip"]) {
					bool newFlip = [newVal intValue];
					bool oldFlip = [oldVal intValue];
							into[key] = [NSNumber numberWithInt:newFlip^oldFlip];

					[[@"" addF:@"NOTE: In flattening info: flip %d ^ %d --> %@.",
								 newFlip, oldFlip, into[key]] ppLog];
					 ///// someday:
					//[newElt applyPropNVal:tokens[1]])
					//[self applyProp:key withVal:newVal])
				}
				else {
					bool note = [newVal isEqualTo:oldVal] or [key isEqualToString:@"Share"];
					id msg = [@"" addF:@"%@: In flattening info '%@':'%@', ",
							 note? @"NOTE" :@"WARNING", [key pp], [newVal pp]];
					if ([newVal isEqualTo:oldVal])
						[[msg addF:@"existing '%@':'%@' has same value.", [key pp],[oldVal pp]] ppLog];
					else
						[[msg addF:@"overwrites preexisting '%@':'%@'.", [key pp],[oldVal pp]] ppLog];
				}
			}
			into[key] = newVal;					// copy val into
		}

		 /// THEN flatten etc (into "into")
		if (id fromEtc = from[@"etc"])
XR			flattenFromInto(fromEtc, into);	//recursive call; copy insides into
	}
	else if ([from asNumber] or [from asPart])
		;
	else
		panic(@"");
	return into;
}

#pragma mark 5. Wire

- (id) gatherWiresInto:(id)wirelist; {					int i=333;

//! xzzy2 Part:: PreviousClock:			<clockSource>
//! xzzy2 Part:: sub-parts

	 // Add Previous Clock.
	if ([self takeParameter:@"addPreviousClock"])
		if (Actor *actor = [self.parent enclosedByClass:@"Actor"]) { //To the tightest enclosing Actor
			[actor logEvent:@".previousClocks: adding (%@ *)'%@'", self.className, self.fullName ];
			[actor.previousClocks addObject:self];
		}

	for (id elt in self.parts)
		if (Part *part = coerceTo(Part, elt))
XX			[part gatherWiresInto:wirelist];
	return wirelist;
}

 //// The Parts model is scanned with gatherWiresInto, and calls addWireToList
typedef void (^WireBlock)();

 /// Add wire self:subPort to the reference(s) in targetArg (e.g P:@"ff")
- (void) addWireToList:wirelist
				source:sourceName	sourceModifiers:sourceModifiers
			    target:targetArg		  todInSelf:(bool)todInSelf
			   purpose:purpose;					{
	assert(!sourceName or coerceTo(NSString, sourceName), (@""));

	  /// SOURCE
	 // if a subPort is specified, the wire is added as if from it, not self
	Part *source		= self;		// default: source is us
	if (Port *port		= coerceTo(Port, [self partNamed:sourceName])) {// and exists in self as a Port
		source			= port;		// if source name is a Port of ours
		todInSelf	   ^= port.flipped;// Do now from the standpoint of self's named Port
	}

	  /// CONTAINING NET
	 // resolve potential others to Atoms or Ports:
	Net *sNet			= mustBe(Net, smallestNetEnclosing(source));
	bool self2SNetFlip	= [source downInPart:sNet];		// 0 ==> same world flip
	bool todInSNet		= !todInSelf ^ !self2SNetFlip;

	  /// TARGETs
	 // Parse targetArg returns 0 or more targetIds, each wanting wiring
	for (id targetOrig in [source parseTargets:targetArg]) {

		int wireNumber	= self.brain.wireNumber++;
		assert(wireNumber, (@"wraparound to 0 is error"));
		assert(wireNumber!=self.brain.breakAtWireNo, (@"Break at Extraction of wire"
					" %d (at logNumber %d)", wireNumber, self.brain.logNumber-1));
		 ///    PRINTOUT (Gather, line 1)
		[sNet logEvent:@"W%d(goal=%@): Source Atom is '%@'", wireNumber, purpose,
					source.fullName16];

		id target		= targetOrig;					// Decode target:
		if (coerceTo(NSString, target))					/// String ===> Path
			target			= [Path pathWithName:target];

		Path *targetPath = coerceTo(Path, target);
		if (targetPath) {								/// Path   ===> Part
			 /// Find Atom in Network by name / Path:
			[targetPath takeOptionsFrom:sourceModifiers];
			bool prevLts = self.brain.logTerminalSearching;
			self.brain.logTerminalSearching |= self.brain.logTerminalSrchStartsAt==self.brain.logNumber-1;
			bool tod	 = todInSNet ^ targetPath.flipPort;// despirate measure by user!

XX			target		 = [source resolveOutwardReference:targetPath openingDown:tod];

			self.brain.logTerminalSearching = prevLts;
		}
		else if (Part *targetPart=coerceTo(Part,target)){// Part Groom
			 /// Make a Path to the Part in Network:
			targetPath	 = [Path pathWithName:targetPart.fullName];	// make a dummy Path
			[targetPath takeOptionsFrom:sourceModifiers];
		}
		else
			panic(@"WTF");

		 /// Check for ERRORS
		bool typeGood = coerceTo(Atom,				target) or
						coerceTo(Port,				target) or
						coerceTo(Actor,				target) or
						coerceTo(Leaf,				target) or
						coerceTo(Bundle,			target);
		if (!target or !typeGood)

				 ////////////////////////////////////////////////////////////
			    /// Find Atom in Parameters; make a ParameterPort for it ///
			   ////////////////////////////////////////////////////////////
			  ///
			 /// (There is probably a better home for this, but here it goes...)
			if (id paramVal = [self parameterInherited:targetOrig])
				if ([paramVal respondsToSelector:@selector(floatValue)]) {
					typeGood			= true;	// messy

					 // Make a "dangling" Parameter Port to connect to.
					ParameterPort *pp	= [ParameterPort another];
					pp.parameterName	= targetOrig;
					pp.name				= targetOrig;
					pp.valueTake		= [paramVal floatValue];	// initial value
					pp.valuePrev		= pp.value + 0.01;			// indicate a change

					target				= pp;
					targetPath.direct	= true;
					targetPath.name		= targetOrig;
					// Changes (in parameters or the pp) to not reflect back to each other
				}

		if (!target or !typeGood) {

			 //// Handle Error: do it over again for debugger
			 ///    PRINTOUT (Gather, line 2, ERROR)
			[self logEvent:@"\n\n"];
			[self logEvent:@"target Atom= %@ tod%d\n#### '%@' NOT FOUND ####, IGNORING\n\n",
									targetPath.pp, todInSNet, targetPath.pp];

			if (1) {		// try again for debug
				printf("\n#######################################################\n");
				[self logEvent:@"refPart for '%@' resolves to wrong class '%@'; retry...",
												[targetOrig pp], [target className]];
				// perform resolveOutwardReference again, this time with logging temporarily ON
				bool lts = self.brain.logTerminalSearching;
				self.brain.logTerminalSearching = true;

XX				Part *refPart2 = [sNet resolveOutwardReference:targetPath openingDown:todInSNet];

				self.brain.logTerminalSearching = lts;
				printf("############################ IGNORING #################\n");
				assert(refPart2, (@"Part '%@' not found", [targetPath pp]));
			}
			continue;
		}

		 //// IT's GOOD, PRINTOUT (Gather, line 2, ERROR)
		[target logEvent:@"\\ is Target Atom, opens %s", todInSNet? "down": "up" ];
//		[target logEvent:@"  target='%@' tod%d  <--- Target Atom",  [targetArg pp], todInSNet];
		id target2			= coerceTo(ParameterPort, target)? nil: target;
		Net *conNet			= smallestNetEnclosing(source, target2);
		bool todInConNet	= todInSelf ^ [source downInPart:conNet];

		   ///////////////////////////////////////////////////////////////////
			/////// Do this part of wire later, outside of tree walk: ///////
			/////////////////////////////////////////////////////////////////
XX		WireBlock thisWire = ^() { //////////////////////////////////////////
			/////////////////////////////////////////////////////////////////
			///				IMPORTANT BLOCK VARIABLES:					  ///
			/// from:				source		(self or self's port	  ///
			/// to:					target		(Atom or Port)			  ///
			/// within sub-net:		conNet								  ///
			///	wire direction:		todInConNet	(target opens down in conNet)
			/// connection details:	targetPath	(noChk, dom, dir, invis)  ///
		   ///////////////////////////////////////////////////////////////////

			 // SELF			   -- the origin of the wire
			assert(wireNumber!=self.brain.breakAtWireNo, (@"Break at Wiring wire %d", wireNumber));
			bool	todInSelf		= todInConNet ^ [conNet downInPart:source];
			bool	podInSelf		= !todInSelf;	// Port Opening Down IN SELF

XX			Port    *selfPort		= [source getOpenPort_openingDown:podInSelf];

			 //// IT's GOOD, PRINTOUT (Wire, line 1)
			[conNet logEvent:@"is W%d's Context. Source Port is %@",
											wireNumber, selfPort.fullName16];
			 // Decode target to find target Port
			bool todInTarget		= todInConNet ^ [conNet downInPart:target];

XX			Port *targPort			= [target getOpenPort_openingDown:todInTarget];

			assert(targPort, (@"wirelists failed to find target port"));
			targPort.noCheck		= targetPath.noCheck;
			targPort.dominant		= targetPath.dominant;

			 //// IT's GOOD, PRINTOUT (Wire, line 2)
			id pp 					= targetPath.ppProperties;
			[target logEvent:@"\\ is Target Atom, opens %s %@",
				todInSNet? "down": "up", pp? [@"" addF:@", linkProp=<%@>", pp]: @""];

			  // Some checks if it doesn't exist or already connected
			if (	!targPort or targPort.connectedTo or
					!  selfPort or   selfPort.connectedTo) {
				assert(targPort,(@"target's Port not found"));
				assert(!targPort.connectedTo, (@"target's Port occupied"));
				assert(selfPort,(@"self's Port not found"));
				assert(!selfPort.connectedTo, (@"self's Port occupied"));
				NSPrintf(@"Bad wire; skipping...\n");
				return;			// don't build, on to next
			}

			  /// DIRECTION CHECKS:
			 /// Insure Boss and Worker Ports face in different directions
			bool targetBitWorldDown = targPort.downInWorld;
			bool   selfBitWorldDown =   selfPort.downInWorld;
			if (!targPort.noCheck and !selfPort.noCheck)
				assert(targetBitWorldDown!=selfBitWorldDown, (@"attempt to link Ports "
					"'%@' and '%@' both with worldDown=%d. \n\t\tConsider using path suffix '^'.",
					selfPort.fullName, targPort.fullName, targetBitWorldDown));

			 /// NO EXTRA LINK if direct==1 or source is link already
			if (coerceTo(Link, source) or targetPath.direct) {
				 /// Connect Link
				selfPort.connectedTo = targPort; 		// self <--> target (no link)
				 /// Configure Link
				if (targetPath.invisible)
					source.initialDisplayMode = (DisplayMode)(displayInvisible |source.initialDisplayMode);
			}
			 /// EXTRA LINK
			else {
				 /// Add new Link
				Link *link = aLink();		// Includes P and S Ports  //Link *link = anotherBasicPart([Link class]);
				[conNet addPart:link];
				assert(link.pPort and link.sPort, (@"Link Missing P and/or S Port"));

				 /// Connect Link
				if ( targetBitWorldDown != link.pPort.downInWorld ) {
XX					link.sPort.connectedTo = selfPort;
XX					link.pPort.connectedTo = targPort;
				}
				else { // Perhaps with P on top 170204, but why?
XX					link.sPort.connectedTo = targPort;
XX					link.pPort.connectedTo = selfPort;
				}

				 /// Configure Link
				if (id linkOptions = targetPath.linkOptions)
					for (id key in linkOptions) {
						id val = linkOptions[key];
						if ([key isEqualToString:@"l"])
							link.length = [val floatValue];
						else if ([key isEqualToString:@"min"])
							link.minColorVal = [val floatValue];
						else if ([key isEqualToString:@"max"])
							link.maxColorVal = [val floatValue];
					}
//					panic(@"");//link.length = [a floatValue];//want to add min max Value
				if (targetPath.invisible)
					link.initialDisplayMode = (DisplayMode)(displayInvisible | link.initialDisplayMode);//source.initialDisplayMode = (DisplayMode)(displayInvisible | source.initialDisplayMode);

			}//////////////////////////////////////////////////////////////
		};  ////////////////////////////////////////////////////////////////
		[wirelist addObject:[[thisWire copy] autorelease]];	// must copy, cause this stack goes away
	}
}

- (NSMutableArray *) parseTargets:targetArg;  {
	bool downInSelf = 0;
	NSMutableArray *parsedTargetArray = @[].anotherMutable;

	   // digest targetArg's many forms:

	 // NSArray				=== target is array; expand insides RECURSIVELY
	if (NSArray *targetArray = coerceTo(NSArray, targetArg))
		for (id aTarget in targetArray) 		// parse each target separately
XX			[parsedTargetArray addObjectsFromArray:[self parseTargets:aTarget]];

	 // Path				=== target is a Path
	else if (coerceTo(Path, targetArg))
		[parsedTargetArray addObject:targetArg];

	 // NSString			=== target is string
	else if (coerceTo(NSString, targetArg))
		[parsedTargetArray addObject:targetArg];

	 // Port or Atom		=== target is valid Part
	else if (coerceTo(Port, targetArg) or coerceTo(Atom, targetArg))
		[parsedTargetArray addObject:targetArg];

	 // NSNumber			=== target is nil
	else if (id aNumber = coerceTo(NSNumber, targetArg))
		assert([aNumber intValue]==0, (@"target @<n> with n!=0 is ERROR"));

	else if (targetArg==nil)
		panic(@"Parsing nil targetArg");

	 // NSArray				=== target is array; expand insides RECURSIVELY
	else if (NSDictionary *targetDict = coerceTo(NSDictionary, targetArg))
		[@"" addF:@"Parsing NSDictionary targetArg '%s' fails", [targetArg ppC]];

	else
		panic(@"Parsing unknown targetArg '%s' fails", [targetArg ppC]);

	return parsedTargetArray;	// an array containing all references
}

 // For all wires are in WireList wires:
void wireViaList(id wires) {
	for (id wire in wires)
		((WireBlock)wire)();	// Execute their associated block
}

- (void) orderPartsByConnections; {
	 // Check order of each sub
	for (id elt in self.parts)
		if (Part *part = coerceTo(Part, elt))
XX			[part orderPartsByConnections];
}

#pragma mark 6. Navigation

- (Port *) biggestBitOpeningDown:(bool)downInSelf; {
	Port *rv = nil;
	for (Port *port in self.parts) {
		if (port.flipped == downInSelf) {					// correct facing?
			if (!rv)
				rv = port;
			else
				[self logEvent:@"[getBit)????? : ignoring %@, alrady found %@",
															port.name, rv.name];
		}
	}
	return rv;
}

- (Port *) getOpenPort_openingDown:(bool)localDown;{
	return nil; //(id)panic(@"prototype method with no default");
}

 // ======== get Part by   PREDICATE
- (id) searchInsideFor:(PartPredicate)partPredicate; {
	if (partPredicate(self))		// is predicate true for us?
		return self;					// YES

	 // Check my parts too:
	for (id m_ in self.parts)	// is it in our parts
		if (Part *m = coerceTo(Part, m_))
			if (id rv = [m searchInsideFor:partPredicate])
				return rv;
	return nil;
}
- (id) searchOutsideFor:(PartPredicate)partPredicate;  {
	return [self searchOutsideFor:partPredicate except:nil];
}
- (id) searchOutsideFor:(PartPredicate)partPredicate except:exception;  {

	for (id sm_ in self.parts)
		if (Part *sm = coerceTo(Part, sm_))
			if (sm != exception)
				if (id rv = [sm searchInsideFor:partPredicate])
					return rv;

	 // search wider and wider
	Part *sp = self.parent;
	return [sp searchOutsideFor:partPredicate except:self];
}

#pragma mark 7-. Simulation Actions
- (void) reset; {
	
	// Check order of each sub
	for (id elt in self.parts)
		if (Part *part = coerceTo(Part, elt))
XR			[part reset];
}

#pragma mark 7. Simulator Messages
///***///***///***///***///***///***///***///***///***///***///***///***///
///***///***///***///***///***///***///***///***///***///***///***///***///
///***///***///***///***///***///***///***///***///***///***///***///***///

FwEvent makeFwEvent(FwType fwType) {
	FwEvent rv = {fwType};
//	rv.fwType = fwType;
	return rv;
}

 // Inject message
- (id) sendMessage:(FwType)fwType; {
	[self logEvent:@"|| all parts  sendMessage:%@.", fwTypeDefnNames[fwType]];

	FwEvent fwEvent = makeFwEvent(fwType);
	return [self receiveMessage:&fwEvent];
}

- (id) receiveMessage:(FwEvent *)fwEvent; {
//	[self logEvent:@"$$$$$$$$ all parts receiveMessage:%@.", fwTypeDefnNames[fwEvent->fwType]];

	for (Part *elt in self.parts)				// do for our parts too
		if (coerceTo(Part, elt))
			if (id rv = [elt receiveMessage:fwEvent])			// first responder
				return rv;										// takes message
	return nil;
}
+ (Part *) conceiveBabyIn:(WriteHead *)wh evi:evi con:con; {
	panic(@"Illegal babyPrototype");
	return nil;
}

// // Propigate cockPrevious to all contents registered in previousClocks
//- (void) clockPrevious;  {
//	;//[self logEvent:@"$$$$$$ clockPrevious -- ignored for now"];
//}

#pragma mark 8. Reenactment Simulator
//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//
//**//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//
//***//*//*//*//*//*//  Reenactment Simulator  //*//*//*//*//*//*//*//
//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*////
//**//*//*//*//*//*/s*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//

- (void) simulateDown:(bool)downLocal; {
	 // Step all my parts:
	if (self.parts.count) {

		NSEnumerator *parts = !downLocal? [self.parts objectEnumerator] :[self.parts reverseObjectEnumerator];	// goingDown: do backwards

		for (id elt in parts) {
			if (Part *part = coerceTo(Part, elt)) {
				bool downInEnt = [self downInPart:part] ^ downLocal;

XX				[part simulateDown:downInEnt];		// step all somponents
			}
		}
	}
}

- (int) unsettledPorts;	{
	assert(coerceTo(Net, self) or coerceTo(Atom, self), (@"%@ * illegal", self.className));
	int rv = 0;
	for (id elt in self.parts)
		if (coerceTo(Part, elt))
			rv += [elt unsettledPorts];
	return rv;
}

- (id) ppUnsettledString; {
	id rv=@"";
	for (id elt_ in self.parts)
		if (Part *elt = coerceTo(Part, elt_))
XR			if (id s = [elt ppUnsettledString])
				rv=[rv addF:@"%@", s];
	return rv;
}

#pragma mark 9. 3D Support
// \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\    //
//  \\ \\ \\ \\ \\ \\ \\ \\ \ 3D Support\\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\  //
//   \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\//

- (Bounds3f) gapAround:(View *)v;	{	return Bounds3f(-2,-2,-2, 2,0,2);}

- (View *) reViewIntoView:(View *)v; {

	 // Caller didn't supply a View
	if (!v) {
		v = [View another];		// Make a new one
		v.part = self;
		v.displayMode = self.initialDisplayMode;
	}
  	assert(![v.name isEqualToString:self.brain.breakAtViewOf],	(@"Break from breakAtViewOf '%@'", v.name));
	assert(v, (@"caller should provide non-nil v"));
	assert(v.part==self, (@"caller should ensure v.part==self"));

	  // 160515 Pruning Views causes finding places in an Atomic forms fail
	 // Thus do not prune -- comment out the following lines
//	if (v.displayMode & displayAtomic)
//		return v;						// nothing to View inside

	// Verify that the View structure represents the Part (self).
	 // OPEN QUESTIONS:
	  //	How are viewing perspectives defined?
	   //	How are animation handled?
		// Don't change structure unless needed. (How does this help animations?)

	  /// Insure the proper subViews, per my subPart, with minimal change
	 ///
	int iView=0;						// index in View of subVew being examined
	int nView=(int)v.subViews.count;	// index of max View +1
	for (id elt_ in self.parts) {

		   //// Numbers: signify build phases.
		  //// This allows sequenced build epochs:
		 //// Ignore the remaining parts if current phase too small
		if (NSNumber *num = coerceTo(NSNumber, elt_)) {
			float epochMinAnte = [num floatValue];
			assert(!isNan(epochMinAnte), (@""));
			float epochBeingViewed = self.brain.buildingEpoch;
			if (epochBeingViewed >= epochMinAnte)	// Display past here
				continue;					// ALLOW: continue down the list
			else
				break;						// LIMIT: process no more array elements
		}
		// 161006: BUG can't retract views 171113:??
		Part *elt = mustBe(Part, elt_);
		assert(![elt.name isEqualToString:self.brain.breakAtViewOf], (@"Break from breakAtViewOf '%@'", elt.name));

//		 //// Links: skip over 161006 XXX, links processed as others
//		assert(!coerceTo(Link, elt), (@"Links not allowd"));

		 //// Parts: may have subParts. Find its index in Part.parts[]
		int jView = -1;		 // index subview for subPart
		for (int i=iView; i<nView; i++) {		// paw through following views
			assert(i<v.subViews.count, (@""));
			View *svi = v.subViews[i];
			if (svi.part == elt) {
				assert(jView==-1, (@"WTF: found second view of subPart"));
				jView = i;						// move iView to last view matching
			}
		}

		 // View found in right spot -- rationalize its subViews:
		if (jView == iView)

XR			[elt reViewIntoView:v.subViews[iView]];	// get a view for it


		 // Make a brand new View, since NONE FOUND
		else if (jView < 0) {

XR			View *subView = [elt reViewIntoView:nil];	// get a view for it
			subView.superView = v;

			assert(iView<=v.subViews.count, (@"adding subView at bad index"));
			[v.subViews insertObject:subView atIndex:iView];
			nView++;
		}

		  // Garbage in between, to remove.
		 // view found after extra intervening Views -- delete all extras
		else while (iView < jView) {		// paw through following views
			[v.subViews removeObjectAtIndex:iView];	// remove as extra
			jView--;							// intervening smaller
			nView--;							// end smaller
		}
		iView++;									// next Part
	}
	return v;
}

// SwiftFactals: func rePack(inView view:View)
- (void) boundIntoView:(View *)v; {
	assert(![v.name isEqualToString:self.brain.breakAtBoundOf],
					(@"breakAtBoundOf view %@", self.brain.breakAtBoundOf));
	  // Bound and Place all contained Elements
	v.bounds = nanBounds3f;				// initially nil, to occupy elt 0's origin
	for (View *subView in v.subViews) {	// Subviews:
		Part *subElt = subView.part;
		 /// Bound
		if (subView.isAtomic)
			[subElt boundAsAtomIntoView:subView];	// bound atom subView
		else {
			[subElt suggestedSpinInView:subView];	// spin is part of bound
XR			[subElt boundIntoView:subView];			// bound subView
		}
		 /// Place
XR		[subElt placeIntoView:subView];				// place subView, so we can get our
	}

	if (isNan(v.bounds))				// If no element set bounds to !nan
		v.bounds = zeroBounds3f;			// use our un-gapped size as zero
}

- (void) boundAsAtomIntoView:(View *)v; {
	assert(![v.name isEqualToString:self.brain.breakAtBoundOf],
					(@"breakAtBoundOf view %@", self.brain.breakAtBoundOf));
	float a = self.brain.atomRadius;
	v.bounds = Bounds3f(-a,a, -a,a, -a,a);
}


#pragma mark 10. Placement

#define ddPrintf0(fmt...)	({	if (debug) NSPrintf(fmt);	 false;		})

- (void) placeIntoView:(View *)v; {
	bool debug = [v.name isEqualToString:self.brain.debugPositionOf];

	Part *parent = self.parent;
  	assert(![v.name isEqualToString:self.brain.breakAtPositionOf],
								(@"Break from breakAtPositionOf %@", v.name));

	  // DETERMINE PLACEMENT: it's from self, parent, or default:
	PlaceType placeType = self.placeSelf;	// Priority 1: specified by self
	if (!placeType)							// Priority 2: specified by of parent
		placeType = parent.placeParts;
	if (!placeType)							// Priority 3: not specified:
		placeType = str2placeType(@"+Ycc");		// default: cCtrplace above workers in +Y

	 //////////////   Compute Position from wires   //////////
	View *pop = v.superView;
	v.positionTranslation = zeroVector3f;		// remove nan spot, leave rotation part


	      //////////////////////////////////////////////////////////////
	     //////////////////////////////////////////////////////////////
	    ///////        Place ABOVE rest of existing           ////////
	   //////////////////////////////////////////////////////////////
	  //////////////////////////////////////////////////////////////
	 //
	if (placeType & isLink) {

		   // get the spot I will attach to, presuming I'm at the origin.
		  // N.B: Everything is spun and bound. Placing only deals with setting the position spot
		 // set up the flipped matrix to be flipped if our part is.

		// Concepts, not yet in here:	(worker/boss is to be depricated)
		// unk			-- Part to position, whose position is UNKnown
		// fix			-- Part unk is connected to, whose position is FIXed

		   //////// Position by fix'ed Ports self is connected to
		  ///
		 /// calculation performed in refView, aka pop
		View *refView = pop;
		__block float maxHeight			= -HUGE_VALF;
		__block float weightSum			= 0;			// -1 --> dominated
		__block Vector3f rLocn			= zeroVector3f;	// default return position is Nan
		__block Vector3f weightVectSum	= zeroVector3f;

		ddPrintf0(@"\n============ position %@ ===========\n", self.fullName);
		
		   /// For all enclosed subBits, looking for things to position us by
		  ///
		 //////////////////////////////////////////////////////////////////////
		[self searchInsideFor:^(Part *unk){ /// all Parts inside self #### BLOCK
	   //////////////////////////////////////////////////////////////////////////

			if (debug and 1)
				[self logEvent:@"Searching Inside:"];
//				[@" " ppLog:@{@"no-cr":@1}];			// get/print log number
			ddPrintf0(@"  Follow %@; ", unk.fullName16);

			   ///////// Find those Ports useful for positioning, IGNORE OTHERS
			  ///
										//// a. Ignore if not Port
			Port *unkP = coerceTo(Port, unk);
			if (!unkP)
				return ddPrintf0(				@"not Port\t-- XXX\n");
			ddPrintf0(@"Port; ");
			if (coerceTo(Link, unk.parent))
				return ddPrintf0(				@"Link!\t-- XXX\n");

										//// b. Ignore Ports named M (ad-hoc; switches mode)
			if ([unkP.name isEqualToString:@"M"])
				return ddPrintf0(				@"name=M!\t-- XXX\n");

										//// c. went through invisible link ////
			Link *aLinkInbetween = coerceTo(Link, unkP.connectedTo.atom);
			bool isInvisible = aLinkInbetween and (aLinkInbetween.initialDisplayMode & displayInvisible);
			if (isInvisible)				// invisible if invisible link connects
				return ddPrintf0(				@"link invisible\t-- XXX\n");

										//// d. Ignore if connectedTo ParameterPort
XX			Port *fixP = unkP.portPastLinks;
			if (coerceTo(ParameterPort, fixP))
				return ddPrintf0(				@".parameterPort\t-- XXX\n");

			if (!fixP)					//// e: fixP not found ////
				return ddPrintf0(				@"nothing linked to unkP\t-- XXX\n");
			ddPrintf0(@"-->%@; ", fixP.fullName16);

										//// f. If other end is INSIDE of unkno (self), IGNORE
			if ([self searchInsideFor:^(Part *m3) {		//####BLOCK inside BLOCK
						return m3==fixP; /* MATCH? */}])
				return ddPrintf0(				@"inside\t-- XXX\n");

										//// g. If other end is a Write Head (AD HOC)
			if (coerceTo(WriteHead, fixP.parent))
				return ddPrintf0(				@"WriteHead\t-- XXX\n");// do not position via WriteHead

			 /////////////////////////////////////////////////////////////////
XX			Part *enclosure 		= smallestNetEnclosing(fixP, self);
			 Net *commonNet 		= mustBe(Net, enclosure);
			 bool facingDown 		= [unkP downInPart:commonNet];
			 ddPrintf0(facingDown? @"facingDown; ": @"facingUp; ");
			 bool validFacing 		= facingDown; // if facingUp, default validFacing is bad

			 // The spot the UNKnown Port should be in commonView
			 View *commonView 		= [refView superViewForPart:commonNet];// find the view
			Vector3f unkSpotInCommon = [unkP peakSpotInView:commonView];
			 if (isNan(unkSpotInCommon))
				return ddPrintf0(				@"unk Spot NAN\t-- XXX\n");

			Vector3f weightVect(1.0, 1.0, 1.0);
			if (!facingDown) 							// if it is above
			 // Actors may position their contents by including
			  // information of the context bundle above.
			   // (This is rather ad-hoc and somewhat trickey code)
				if (Actor *actor 	= coerceTo(Actor, commonNet)) {// CommonNet is an Actor:
				  // FIXed is in an Actor's CONtext Bundle
					id fix2ActorsChild = [fixP ancestorThatsChildOf:commonNet];
					if (!fix2ActorsChild)
						return ddPrintf0(		@"fix2ActorsChild nil\t-- XXX\n");

					 // We may be positioning by an Actor's context, which is above
					if (fix2ActorsChild == actor.con) {
						Bundle *bundle 	= coerceTo(Bundle, fix2ActorsChild);
						if (!bundle)					// perhaps Actor in Actor
							bundle 		= mustBe(Actor, actor.con).evi;
						mustBe(Bundle, bundle);
						////////// fixed is inside an Actor's CONtext ///////
						if (bundle.positionPriorityXz != 0)	// allow placement
							validFacing = true;
						if (bundle.positionPriorityXz == 1)	// x has priority
							weightVect 	= Vector3f(0.001, 1.0, 100.0);
						if (bundle.positionPriorityXz == 2)	// z has priority
							weightVect 	= Vector3f(100.0, 1.0, 0.001);
					}
				}
			if (!validFacing)			//// g. pointing up, but not into a Context
				return ddPrintf0(				@"not in context\t-- XXX\n");


			    ///////////////////////////////////////////////////////////////
			   //// A FIXed Port was found for positioning the UNKnown Port ////
			  ///															/////


//			Vector3f unkSpotInCommon = [unkP peakSpotInView:commonView];
//			 if (isNan(unkSpotInCommon))
//				return ddPrintf0(				@"unk Spot NAN\t-- XXX\n");

			 // find spot above hotspot of fixed Port, in commonView
			Vector3f fixSpotInCommon = [fixP peakSpotInView:commonView];
			if (isNan(fixSpotInCommon))
				return ddPrintf0(				@"fix Spot NAN\t-- XXX\n");

			 // The new position of UNKnown, so that the FIXed Port an UNKnown Port coincide
			if (self.parent != commonNet)
				fixSpotInCommon.y 	= -1;			// 0? WTF!!!

			Vector3f newUnkPosn =   fixSpotInCommon   -   unkSpotInCommon  ;
			ddPrintf0(@"move:%@\n", pp(newUnkPosn));



			 // Direct (no link) or Dominated?
			bool direct = unkP.connectedTo==fixP;
			bool dom1	= unkP.dominant | unkP.connectedTo.dominant;
			bool dom2	= fixP.dominant | fixP.connectedTo.dominant;
			if (!direct and !dom1 and !dom2){

				 ////////////////// CONNECTION VIA LINK
				float gap = self.brain.fluffLinkGap;			// minimal distance above
				 // Links may override the lenght (160214: probably never used)


				Link *l1 = mustBe(Link, unkP.connectedTo.parent);
				if (!isNan(l1.length))
					gap = l1.length;
				newUnkPosn.y += gap;				// move up by gap

				 ////////////////// ACCUMULATE this position
				if (weightSum >= 0) {		// if not dominated
					rLocn += newUnkPosn * weightVect;	// bit by bit *
					weightVectSum += weightVect;
					weightSum 		+= 1;				// number of fixP's
				}
				if (facingDown)						// keep track of highest downward
					maxHeight 		= max(newUnkPosn.y, maxHeight);// (except height is max)
			}
			else {
				 ////////////////// DOMINATED CONNECTION
				float h				= newUnkPosn.y + self.brain.directLinkGap;
				maxHeight			= max(h, maxHeight);// (except height is max)
				rLocn				= newUnkPosn;
				assert(weightSum>=0, (@"currently 2 dominant Ports are illegal"));// not already dominated
				weightSum			= -1;			// enter dominant mode
			}
			return false;					// keep going thru ports in self
		}];													//## BLOCK
		 /////////////////////////////////////////////////////////////////////
		  ///////         END OF SCANNING ALL PARTS INSIDE            ///////
		   /////////////////////////////////////////////////////////////////

		 // if some positioners found:
		if (weightSum and !std::isinf(maxHeight)) {
			if (weightSum >= 0)				// unless dominated
				rLocn /=  weightVectSum;		// take average (by axis)
			rLocn.y 				= maxHeight;// height calculation from max
											//[self suggestedSpinInView:v];

			SimNsV *simNsV 			= mustBe(SimNsV, self.brain.simNsWc.simNsVc.view);
			Vector3f wiggle 		= v==simNsV.wiggleView? simNsV.wiggleOffset: zeroVector3f;
			v.positionTranslation 	= rLocn + self.jog + wiggle;

			placeType 				= nilPlaceType;	// Successful position
			ddPrintf0(@"========================= place:%@, weightSum=%.2f\n",
								pp(v.position.getTranslation()), weightSum);
			[v moveSoNoOverlapping];
		}
		else
			ddPrintf0(@"========================= No Place found\n");
		
		if (placeType != nilPlaceType) {	// Unsuccessful position; try
			placeType 				= str2placeType(@"+Ycc");
			ddPrintf0(@"view %@: No fix elements found\t-- Using placeType: %@", v.name, pp(placeType));
		}
	}


	      //////////////////////////////////////////////////////////////
	     //////////////////////////////////////////////////////////////
	    /////// Stack ABOVE rest of existing   e.g. " + < < " ////////
	   //////////////////////////////////////////////////////////////
	  //////////////////////////////////////////////////////////////
	 //
	if (placeType & aMask) {		// has stack axis "a" defined

		 // Get Bounds in papa:
		Bounds3f    myBounds_pop	= v.bounds * v.position;
		Bounds3f   popBounds_pop	= pop.bounds;
		Vector3f t1(0,0,0);
		if (!isNan(popBounds_pop)) { // skip if we are first
			Vector3f deltaCtrs 		= zeroVector3f;
			Vector3f sumSizes2 		= (myBounds_pop.size() + popBounds_pop.size())/2.0;
			Vector3f difSizes2 		= (myBounds_pop.size() - popBounds_pop.size())/2.0;
			float gap		 		= self.brain.gapWhenStacking;

			 // decode placeType: which axis is movement along?
			int aAxis 				= ((placeType&aMask)>>4)-1;	// major axis: x=0, ..z=2
			int  sign 				=   placeType&aNeg? -1:1;
			int bAlig 				=  (placeType&bMask)>>2;
			int cAlig 				=  (placeType&cMask)>>0;

			 // MOVE to stack new object in this direction (aAxis)
			deltaCtrs[aAxis] 		=(sumSizes2[aAxis] + gap) * sign;
			
			int bAxis 				= (aAxis + 1) % 3;
			 // ALIGNMENT along the bAxis: min,ctr,max
			float bMod 				= bAlig==cMin? -1: bAlig==cMax? 1: 0;
			deltaCtrs[bAxis]   		= difSizes2[bAxis] * bMod;

			int cAxis 				= (aAxis + 2) % 3;
			 // ALIGNMENT along the cAxis: min,ctr,max
			float cMod 				= cAlig==cMin? -1: cAlig==cMax? 1: 0;
			deltaCtrs[cAxis] 		= difSizes2[cAxis] * cMod;

			t1 						= popBounds_pop.center() + deltaCtrs;
		}

		SimNsV *simNsV = mustBe(SimNsV, self.brain.simNsWc.simNsVc.view);
		Vector3f wiggle = v==simNsV.wiggleView? simNsV.wiggleOffset: zeroVector3f;
		v.positionTranslation = t1 - myBounds_pop.center() + wiggle;
		 //((( 150515 BUG IN OBJC++ (or my understanding of it)
		  //    v.position.setTranslation(zeroVector3f);// does not change v.position
		   //    v.positionTranslation=zeroVector3f; 	// sets v.position[3] properly ?)))

		self.brain.boundsDirty 		= true;	// Not really, but pretend it's dirty
		placeType 					= nilPlaceType;// Successful position
	}

	assert(placeType==nilPlaceType, (@"nobody could place me"));
	[v includeBoundsInSuperview];
}

- (void) suggestedSpinInView:(View *)v; {
	 // Do computations in superView (aka pop)
	View *pop = v.superView;

	Vector3f myCenter_pop			= Vector3f();
	Matrix4f spinPoint				= Matrix4f();	// translation is place to spin about
	if (bool spinAbouCenter = 0) {
		 // assume no rotations:
		Bounds3f myBounds_pop		= v.bounds;			// pop's bounds is my bounds
		myCenter_pop				= myBounds_pop.center();// get center
		 // go to center
		spinPoint.setTranslation( spinPoint.getTranslation() + myCenter_pop );
	}

	float tv 						= self.brain.testValue1;
	float mag3 						= pow(2.0, tv/*self.shrink*/);//1.0;//
	//v.positionRotation.det();

	 // compute rotation components
	Matrix3f flipMtx 				= Matrix3f();			//// 1. Identity
	flipMtx.at(1,1)  				= self.flipped? -1:1;	//// 2. flipped:

	Matrix3f spinMtx = spinMatrices[self.spin].getRotation();/// 3. spin:

	float lat		 = self.latitude * M_PI_4 * 0.5;		//// 4. latitude:
	Matrix3f latMtx  = Matrix3f().createRotationAroundAxis(0, 0, lat);

	Matrix3f spin	 = flipMtx * spinMtx * latMtx * mag3;

	 // set rotation
	spinPoint.setRotation(spin);

	 // go back from center
	spinPoint.setTranslation(spinPoint.getTranslation() - myCenter_pop);

//	NSPrintf(@"%@ spin shrink: %f %@\n", self.fullName16, mag3, pp(spinPoint));

	v.position = spinPoint;
}


- (Matrix4f) suggestedPositionOfSpot:(const Vector3f &)place; {

	  /// a Part may have suggestions affecting internal space

	 /// flipped:
	Matrix4f flipMtx = Matrix4f();		// Identity
	flipMtx.at(1,1)  = self.flipped? -1.0: 1.0;

	 /// spun:
	Matrix4f spinMtx = spinMatrices[self.spin];

	 /// latitude:
	float lat		 = self.latitude * M_PI_4 * 0.5;
	Matrix4f latMtx  = Matrix4f().createRotationAroundAxis(0, 0, lat);

	Matrix4f rv		 = flipMtx * spinMtx * latMtx;

	 /// jog:
	rv.setTranslation(place);
//	rv.setTranslation(place + self.jog);

	return rv;
}

//#pragma mark Hot Spots


#pragma mark 11. 3D Display
//// // // // // // // // // // // // // // // // // // // // // // // ///
/// // // // // // // // // // 3D Display  // // // // // // // // // ////
// // // // // // // // // // // // // // // // // // // // // // // // //

- (void) drawFullView:(View *)v context:(RenderingContext *)rc; {

	if (self.brain.displayAxis)
		[self displayAxisInView:v context:rc];

	[self displayBoundingBoxInView:v context:rc];

	if (self.flash) {		// display flash?
		glPushMatrix();
			glTranslatefv(v.bounds.center());
			glScalefv(v.bounds.size());
			glutSolidCube (1.0);
		glPopMatrix();

		self.flash--;
	}

	 // TO ALL  subViews  
	for (View *sv in v.subViews)
XR		[sv renderWith:rc];

}

 // Atomic Parts are displayed as spherers.
- (void) drawAtomicInView:(View *)v context:(RenderingContext *)rc; {
	rc.color = colorWhite;
	float radius = 1? 1.0: 0.2;
	glutSolidSphere(radius, 8, 8);
	[self displayBoundingBoxInView:v context:rc];
}

- (void) displayBoundingBoxInView:(View *)v context:(RenderingContext *)rc; {

	 // how much are the drapes open?  There are 4 areas
	float x = self.brain.boundsDrapes;		// globally defined now
	float ctlGShade = 0.0;
	float ctlDrapes = 0.0;
								// Value = 0
	if (x <= 0.0005) {				// Region A: display box OFF
		ctlGShade=0.0;
		ctlDrapes=0.0;
	}							// Value = 0.0005
	else if (x < 0.25) {			// Region B: invisible <--> grey
		ctlGShade = x/0.75;
		ctlDrapes = 0.001;				// (display box begins grey shmoo
	}							// Value = 0.25
	else if (x <= 1.0) {			// Region C: wire <--> drapes
		ctlGShade = 0.99;
		ctlDrapes = (x-0.25)/0.75;		// drawing drapes
	}							// Value = 1.0
	else {							// Region D:
		ctlGShade = 1.0;
		ctlDrapes = 1.0;
	}							// Value > 1.0

	ctlGShade = min(ctlGShade, 0.10);	// 171010 Hack to keep drapes less harsh looking

	if (ctlDrapes > 0.0) {
		float middlePct = 0.2 + 0.3*ctlDrapes;

		 // fade color to light grey, just before going away
		//ctlGShade = 0.5;
		FColor color = lerp(colorBlack, colorBackground, ctlGShade);
		rc.color = color;

		glPushMatrix();
			glTranslatefv(v.bounds.center());

			 // 3D box with triangle corners
			Vector3f bs = v.bounds.size();
			Vector3f bb = bs * ctlDrapes;
			Vector3f vbs2 = v.bounds.size()/2;
			float x2=vbs2.x, y2=vbs2.y, z2=vbs2.z;
			float drapeWidth[] = {min(bs.x, bb.x), min(bs.y, bb.y), min(bs.z, bb.z)};
		
			for (int i=0; i<8; i++)	{			// for all 8 points in a cube:
				Vector3f vertexI = Vector3f(i&1?x2:-x2, i&2?y2:-y2, i&4?z2:-z2);

				for (int j0=0; j0<3; j0++)	{			// for the 3 lines that exit that point
					/*unit2                  unit1
						  \_               _/
					  drape2\___	   ___/drape1
							  \x\__	__/x/
						j2 z    \xxVxx/    j1 / y
								  \x/
								   * vertexI
								   |
								   |  j0 / x
								   |
								   |
								   * vertexJ0				*/
					Vector3f vertexJ0 = vertexI;					// other end of line
					(j0==0?vertexJ0.x: j0==1?vertexJ0.y: vertexJ0.z) *= -1;
					
					int j1 = (j0+1)%3;					// first perpendicular
					Vector3f unit1 = Vector3f(j1==0, j1==1, j1==2);
					Vector3f drape1 = unit1 * (drapeWidth[j1] * (i&(1<<j1)? -1: 1));
					
					int j2 = (j0+2)%3;					// second perpendicular
					Vector3f unit2 = Vector3f(j2==0, j2==1, j2==2);
					Vector3f drape2 = unit2 * (drapeWidth[j2] * (i&(1<<j2)? -1: 1));

					Vector3f midDrape = (drape1 + drape2) * middlePct;
					
					glBegin(GL_LINES);					// LINE
						glVertex3fv(vertexI);
						glVertex3fv(vertexJ0);
					glEnd();
					//NSPrintf(@"%@: %@ -> %@\n", self.name, pp(vertexI), pp(vertexJ0));

					Vector3f p1=vertexI;
					Vector3f normal = vertexJ0 - vertexI;
					normal.normalize();
					for (int k=0; k<1; k++) {
						glPushMatrix();
//							glTranslatefv(normal * 0.0); // move a little
							glBegin(GL_POLYGON);
							glNormal3fv(normal);
							glVertex3fv(p1);			// right angle
							glNormal3fv(normal);
							glVertex3fv(p1 + drape1);	// obtuse A
							glNormal3fv(normal);
							glVertex3fv(p1 + midDrape);	// middle of AB
							glNormal3fv(normal);
							glVertex3fv(p1 + drape2);	// obtuse B
							glNormal3fv(normal);
							glVertex3fv(p1);			// right angle
							glEnd();
						glPopMatrix();
						normal = -normal;
					}
				}
			}
		glPopMatrix();
	}
}

- (void) displayAxisInView:(View *)v context:(RenderingContext *)rc; {
	Vector3f min = v.bounds.point0();
	Vector3f max = v.bounds.point1();
	rc.color = colorRed;

	glBegin(GL_LINES);
		glVertex3f(min.x, 0, 0); glVertex3f(max.x, 0, 0);
		glVertex3f(0, min.y, 0); glVertex3f(0, max.y, 0);
		glVertex3f(0, 0, min.z); glVertex3f(0, 0, max.z);
	glEnd();

	 // mark the bounds ends of each axis with a jack4
	float endMarkLen = 0.1;
	glPushMatrix();
		glTranslatef(min.x,       0, 0);
		glutSolidSphere (endMarkLen, 4, 3);
//		myGlJack4(rc, endMarkLen);
		glTranslatef(max.x-min.x, 0, 0);
		glutSolidSphere (endMarkLen, 4, 3);
//		myGlJack4(rc, endMarkLen);
	glPopMatrix();
	glPushMatrix();
		glTranslatef(0, min.y,       0);
		glutSolidSphere (endMarkLen, 4, 3);
//		myGlJack4(rc, endMarkLen);
		glTranslatef(0, max.y-min.y, 0);
		glutSolidSphere (endMarkLen, 4, 3);
//		myGlJack4(rc, endMarkLen);
	glPopMatrix();
	glPushMatrix();
		glTranslatef(0, 0, min.z      );
		glutSolidSphere (endMarkLen, 4, 3);
//		myGlJack4(rc, endMarkLen);
		glTranslatef(0, 0, max.z-min.z);
		glutSolidSphere (endMarkLen, 4, 3);
//		myGlJack4(rc, endMarkLen);
	glPopMatrix();
}

- (FColor) colorOf:(float)ratio; {
	static FColor inside=  {0.7, 0.7, 0.7,  1},  outside=  {0.7, 0.7, 0.7,  1};
	return lerp(inside, outside, ratio);
}

#pragma mark 12. Inspectors

 // base class expands all parts into a view:
- (void) sendPickEvent:(FwEvent *)fwEvent toModelOf:(View *)v; {
	assert(v, (@"presumed not nil"));
	 // Toggel if Part is Atomic:

	if (v.isAtomic) {			// Was atomic
		 // perhaps it is display mode, forcing it?
		if (int mask = v.displayMode & displayAtomic)		// was ON
			v.displayMode = (DisplayMode)(v.displayMode & ~displayAtomic);// turn it OFF
		else												// was OFF
			v.displayMode = (DisplayMode)(v.displayMode |  displayAtomic);// turn it ON
	}

//	if (v.isAtomic)				// Was atomic, perhaps it was being forced:
//		v.displayMode = (DisplayMode)(v.displayMode & ~displayAtomic);// make sure nobody's forcing an Atom
//	
//	if (v.isAtomic)				// still atomic?
//		for (Part *part in self.parts) {// reView all sub-parts ??
//			panic(@"who goes there?");
//XX			[part reViewIntoView:v];
//		}
//
//	if (v.isAtomic) {			// still atomic --> it will remain so
//		v.displayMode = (DisplayMode)(v.displayMode | displayOpen);
//		printf("view picked could not be opened, as it has no parts\n");
//	}
//	else						// Was not atomic
//		v.displayMode = displayAtomic;	// force atomic
}

- initialDisplayModeStr; {
	id rv = @"";
	DisplayMode idm = self.initialDisplayMode;
	for (id key in initialDisplayModeNames)
		if ([initialDisplayModeNames[key] intValue] & idm)
			rv=[rv addF:@"%@ ", key];
	return rv;
}
- (NSString *) parametersStr;		{	return self.parameters.pp;		}

#pragma mark 14. Logging
////////////////////////////////////////////////////////////////////////
/////////////////////////// LOGGING FACILITY ///////////////////////////
////////////////////////////////////////////////////////////////////////
- (void) logEvent :(NSString *)msg, ...; {
//	[self logWithPrefix:@"" message:(NSString *)msg, ...; {
//
//- (void) logWithPrefix:(NSString *)prefix message:(NSString *)msg, ...; {
	Brain *b = self.brain?: Brain.currentBrain;
	if (!b.logEvents)
		return;

	va_list args;                       // leading \n and trailing \n apply to whole message
    va_start(args, msg);
	NSString *str = [[[NSString alloc] initWithFormat:msg arguments:args] autorelease];
    va_end(args);

	 // format printout of self.name:
	NSString *name = self.fullName16;
	bool downInWorld	= self.downInWorld;
	bool downInParent	= self.flipped;

	// print if it has changed
	double time = self.brain.timeNow;
	static double lastTime = 0;
	if (lastTime!=time and self.brain.logTime) {
		id delta = isNan(lastTime)? @"": [@"" addF:@"+%.3f; ", time-lastTime];
		NSPrintf(@"T=%.3f %@: - - - - - - - - - - - - - - - - - - - - %@\n", time,
			self.brain.globalDagDirDown? @"DOWN": @"UP  ", delta);
		lastTime = time;
	}

	   // msg special semantics of simulantion logs:
      //  leading '\n'     -- log IVal has extra leading  '\n' before message instead
     //  trailing '\n'    -- log IVal has extra trailing '\n' after  message instead
    while (str.length>0 and [str characterAtIndex:0] == '\n') {
        str = [str substringFromIndex:1];
        NSPrintf(@"\n");                           // append before whole message
    }

	if (coerceTo(Port, self))
		str=[@"" addF:@"%@   %@", [self fullName:18], str];
	else
		str=[@"" addF:@"%@     %@", name, str];

	/// START THE PRINTING:
//    printf("%-4d", self.brain.logNumber);		//=== log number
	NSPrintf(@"%-4d%@\n", self.brain.logNumber, str);				//=== VARARGS string

	logCheckBreaks(self.brain);////////1\\\\\\\\.
}

void logCheckBreaks(id brain_) {
	Brain *brain = coerceTo(Brain, brain_);	// non Parts have nil roots
	assert(brain, (@"no brains"));
	
    if (brain.logNumber==brain.breakAtLog)
				////////2\\\\\\\\3.
		panic(@"Event 'breakAtLog %d' encountered.", brain.logNumber);

    brain.logNumber++;		// go on to next log number
}

#pragma mark 15. PrettyPrint
    ///\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\///
   ///\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\///
  ///\\//\\//\\//\\//\\//\\//\\// PRINTOUT //\\//\\//\\//\\//\\//\\//\\//\\///
 ///\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\///
///\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\///

- (NSString *) ppPart:aux_; {
	id rv = @"";

	  // Unpack options
	 // Limits to the deapth of subview printout
	id aux = [aux_ anotherMutable]; //aux.anotherMutable BUG;
	if (id deapthNum = aux[@"deapth"]) {
		int deapth = [deapthNum intValue];
		if (deapth >= 0) {		// possible deapth limit?
			aux[@"deapth"] = [NSNumber numberWithInt:deapth - 1];
			if (deapth == 0)		// deapth exceeded

				return rv;				// print nothing more
	  	}
	}
	aux[@"indent"]			= aux[@"indent"]? [aux[@"indent"] addF:@" "] : @"";
	bool downInWorld		= !self.downInWorld;
	bool ppLinks			= self.brain.ppLinks!=0 or aux[@"ppLinks"]!=0; //<BUG
	bool ppPorts			= self.brain.ppPorts!=0 or aux[@"ppPorts"]!=0;
	bool ppPartsEarlyBits	= self.brain.ppPartsEarlyBits;


	  // 1. FIRST print parts that are Early Ports, as they are below the Atom
	 //
	int  partsRemaining		= (int)self.parts.count;	// all parts remain initially
	#define earlyPort(x) ppPartsEarlyBits and coerceTo(Port, (x))	and \
						 ((x).flipped!=downInWorld)					and \
						 (coerceTo(Part, (x)))
	for (Part *subPart in !downInWorld? [self.parts objectEnumerator]: [self.parts reverseObjectEnumerator]) {
		if (earlyPort(subPart)) {
			if (ppPorts)
XX				rv=[rv addF:@"%@", [subPart ppPart:aux]];
			partsRemaining--;							// no longer there
		}
	}

	  // 2. MAIN: print self on 1 line
	 //
XX	id selfStr = [self   pp1line:aux   ];		// (selfStr has no \n)

	 // trim trailing spaces
	selfStr = [selfStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];

	 // Mark the end of an Atom with "_" when it is done:
	NSRange underbarRange = {20,14};//{0, 5};
	if (partsRemaining<=0 and coerceTo(Atom, self)) {
		int underbarEnd		= (int)underbarRange.location + (int)underbarRange.length;
		if ([selfStr length] <= underbarEnd)
			selfStr=[selfStr addF:@"              "]; // now >=
		selfStr = [selfStr stringByReplacingOccurrencesOfString:@" " withString:@"_"
											options:0 range:underbarRange];
	}

	if (id fillChar =
		coerceTo(Leaf,		self)?		@"=":		// darkness 1
		coerceTo(Bundle,	self)?		@"%":		// darkness 2
		coerceTo(Actor,		self)?		@"@":		// darkness 3
		coerceTo(Brain,		self)?		@" ":		// darkness 0
		coerceTo(Net,		self)?		@"#":		// darkness 4
										nil)
		selfStr = [selfStr stringByReplacingOccurrencesOfString:@" "
			withString:fillChar options:0 range:(NSRange){20, 4}];	//{20, 4}{0, 5}

	rv = [rv addF:@"%@\n", selfStr];


	  // 3. LAST print lower Parts, some are Ports
	 //
	for (Part *subPart in !downInWorld? [self.parts objectEnumerator]:[self.parts reverseObjectEnumerator]) {
		if (earlyPort(subPart))
			continue;			// Skip Early Ports
		if (!ppLinks and (coerceTo(Link, subPart) or coerceTo(Label, subPart)))
			continue;			// Skip Links and Labels
		if (!ppPorts and coerceTo(Port, subPart))
			continue;			// not printing Ports today

		id str=nil;				// str will contain a \n
		if (NSNumber *num = coerceTo(NSNumber, subPart))
XX			str = [num intValue]==0? @"":	// =0:ignore, >0:Epoch
				[@"" addF:@"   ---- Epoch >=%.2f Task ----\n", [num floatValue]];
		else
XR			str = [subPart		ppPart:aux		];	// (str has \n)

		partsRemaining--;

			// Note: as of 160823, Nets are also Atoms
		if (!coerceTo(Net, self) and partsRemaining<=0) //coerceTo(Atom, self)
			str = [str stringByReplacingOccurrencesOfString:@" " withString:@"_"
									options:0 range:underbarRange];

		rv=[rv addF:@"%@", str];	 // (line contains ending \n)
	}
	return rv;
}

- (NSString *) ppSubs:sub prefix:kind; {
	id rv=@"";
	if ([sub count]) {
		id separator = [@"" addF:@"%d %@=(", (int)self.parts.count, kind];
		for (id port in sub) {
			rv=[rv addF:@"%@%@", separator, [port name]];
			separator=@",";
		}
		rv=[rv addF:@") "];
	}
	return rv;
}

- (NSString *) pp1line:aux; {		// id rv1 = [super pp1line:aux];

	 //// indentation to SuggestedPosition
	id indent = aux[@"indent"]? aux[@"indent"]: @"";
	char ind = ' ';
	if (Part *papa = self.parent)
		ind = 'a'+ ([papa indexOfPart:self] % 26);

	id rv=[self.ppSuggestedPosition addF:@"%@%2ld%c  ", indent, [indent length], ind];

	if (Part *m = coerceTo(Part, self))
		rv=[rv addF:@"%@ ", [m classCommonName6]];
	else
		panic(@"should never be here");//rv=[rv addF:@"!Part?"];

	 //// undo indentation
//	assert(brain, (@"'%@' has nil brain", self.fullName));
//	brain = self.brain;// should be done more centrally
	int indentEnd = self.brain.ppIndentCols - (int)[aux[@"indent"] length];
	if (indentEnd > 0)
		rv=[rv addN:indentEnd F:@""];

	 // Indent port names a port more:
	int indentMax=1, nameIndent=0;						// Default if not a Port
	if (Port *port = coerceTo(Port, self)) {
		NSInteger bitNo = [port.atom indexOfPart:port];
		if (bitNo!=NSNotFound) {
			nameIndent = (int)bitNo;
			nameIndent += nameIndent==0;						// promote primary
			nameIndent = nameIndent>indentMax? indentMax: nameIndent;
		}
	}
	rv=[rv addN:nameIndent F:@""];

	 //// print name, insure it's of minimal length (8)//7
	int nCols = self.brain.ppNameCols?: 8;			// may be called when brain is nil
	rv=[rv addN:nCols F:@"%@", self.name];

	rv=[rv addN:(indentMax - nameIndent) F:@""];

	if (self.brain.ppObjectAddress)
		rv=[rv addF:@"%p ", self];

	rv=[rv addF:@"%@", self.initialDisplayModeStr];

	if (!coerceTo(Port, self) and self.placeSelf != nilPlaceType)
		rv=[rv addF:@"self:%@ ", pp(self.placeSelf)];
	if (!coerceTo(Port, self) and self.placeParts != nilPlaceType)
		rv=[rv addF:@"subs:%@ ", pp(self.placeParts)];

	 /// Print Parameters (TO DO List)
	bool printParams = self.brain.ppParameters || [aux[@"ppParameters"] isTrueId];
	if (printParams and [self.parameters count] and !coerceTo(Port, self)) {	// hack
		NSString *parametersMsg = @"", *separator = @"";
		for (id task in [self.parameters allKeys]) {
			id val = [self.parameters[task] pp];
			parametersMsg = [parametersMsg addF:@"%@%@:%@", separator, task, val];
			separator = @",";
		}
		rv=[rv addF:@"parameters:{%@} ", parametersMsg];
	}
	if (self.flash)
		rv=[rv addF:@"FLASH "];

	 //// check parentage
	if (!self.brain)
		rv=[rv addF:@"NIL-BRAIN "];
	if (!self.parent and self!=self.brain)	// nil parent in root is expected and not noted.
		rv=[rv addF:@"NIL-PARENT "];
	if ([self.parent.parts indexOfObject:self] == NSNotFound)
		rv=[rv addF:@"NOT-IN-PARENT "];
	return rv;
}

 // want to link (in FactalInspector) to part.parts.count directly
- (NSString *) ppFlipped;	{	return self.flipped>0? @"flip":@"upright";}
- (NSString *) ppSuggestedPosition {
	id worldFlip = [self downInWorld]? @"F":@" ";
	id localFlip = self.flipped?       @"f":@" ";
				//	   012345678
	char latitude = [@" nCeEdcsS" characterAtIndex:self.latitude];
	NSString *rv = [@"" addF:@"%d%@%@%c%@", self.spin, localFlip, worldFlip,
			latitude, self.writeHead?@"W":@" "];
	return rv;
}

NSString *pp(Part *part) {
	NSString *rv = [@"" addF:@"(%@)'%@'", part.className, part.fullName16];
	return rv;
}
const char * ppC(Part *part) {			return pp(part).UTF8String;		}

NSString *pp(PlaceType placeType) {
	if (placeType == nilPlaceType)
		return @"plNil";
	if ( placeType&isLink )
		return @"plLink";

	 // decode placeType: which axis is movement along?
	int aAxis = ((placeType&aMask)>>4)-1;		// major axis
	char sign =   placeType&aNeg? '-':'+';
	int bAlig =  (placeType&bMask)>>2;
	int cAlig =  (placeType&cMask)>>0;

	id rv = [@"" addF:@"%c%@%@%@", sign,
						placeAxisNames[aAxis],
						placeTypeNames[bAlig],
						placeTypeNames[cAlig]];
	return rv;
}
//				 placeTypeNames
//		   placeTypeNames
//	 placeAxisNames
// + [xyz] [c><] [c><]	e.g:@"+Ycc"
// - [xyz] [c><] [c><]	e.g:@"-x<<"
// link


PlaceType str2placeType(NSString *placeString) {
	NSString *s = placeString.lowercaseString;
	if ([placeString isEqualToString:@"link"])
		return isLink;

	assert(placeString.length==4, (@"Length of PlaceType string '%@' BAD", placeString));
	PlaceType rv0=nilPlaceType, rv1=rv0, rv2=rv0, rv3=rv0;
	switch ([s characterAtIndex:0]) {
		case '+':		rv0 = aPos;			break;
		case '-':		rv0 = aNeg;			break;
		default:		panic(@"PlaceType string character 0 BAD");
	}
	switch ([s characterAtIndex:1]) {
		case 'x':		rv1 = aX;			break;
		case 'y':		rv1 = aY;			break;
		case 'z':		rv1 = aZ;			break;
		default:		panic(@"PlaceType string character 1 BAD");
	}
	switch ([s characterAtIndex:2]) {
		case 'c':		rv2 = cCtr;			break;
		case '>':		rv2 = cMax;			break;
		case '<':		rv2 = cMin;			break;
		case '?':		rv2 = cUndef;		break;
		default:		panic(@"PlaceType string character 2 BAD");
	}
	switch ([s characterAtIndex:3]) {
		case 'c':		rv3 = cCtr;			break;
		case '>':		rv3 = cMax;			break;
		case '<':		rv3 = cMin;			break;
		case '?':		rv3 = cUndef;		break;
		default:		panic(@"PlaceType string character 3 BAD");
	}
	PlaceType rv = (PlaceType) (rv0 | rv1 | (rv2<<2) | rv3);
	return rv;
}

Matrix4f spinMatrices[4] = {
	Matrix4f( 1, 0, 0, 0,
			  0, 1, 0, 0,
			  0, 0, 1, 0,
			  0, 0, 0, 1),	// 0 deg
	Matrix4f( 0, 0, 1, 0,
			  0, 1, 0, 0,
			 -1, 0, 0, 0,
			  0, 0, 0, 1),	// 90 deg
	Matrix4f(-1, 0, 0, 0,
			  0, 1, 0, 0,
			  0, 0,-1, 0,
			  0, 0, 0, 1),	// 180 deg
	Matrix4f( 0, 0,-1, 0,
			  0, 1, 0, 0,
			  1, 0, 0, 0,
			  0, 0, 0, 1),	// 90 deg
};

@end

	// These should be reactivated some day:
		//	int oo = self.adornOrientation;			// 0 1 2 3 4 5 6 7
		//	float a = oo>=4? 0: oo&1? -1: 1;	// 0 0 0 0 p m p m
		//	float b = oo<4?  0: oo&2? -1: 1;	// p p m m 0 0 0 0
		//	float d = oo>=4? 0: oo&2? -1: 1;	// 0 0 0 0 p p m m
		//	float e = oo<4?  0: oo&1? -1: 1;	// p m p m 0 0 0 0
		//	float orientationMatrix[] =   {a,0,b,0,  0,1,0,0,  e,0,d,0, 0,0,0,1};
		//	glMultMatrixf(orientationMatrix);
	
        // Bundle.orientation;		// pack 0:x 1:y 2:z
        // WorldTurtle.orientation:
        // (x,y,z) ---> (+z, y, +x) if 0
        //				(+z, y, -x) if 1
        //				(-z, y, +x) if 2
        //				(-z, y, -x) if 3
        //				(+x, y, +z) if 4
        //				(-x, y, +z) if 5
        //				(+x, y, -z) if 6
        //				(-x, y, -z) if 7
