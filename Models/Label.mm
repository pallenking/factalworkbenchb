//  Label.mm -- display a text label in 3D space C2015PAK

#import "Label.h"
#import "View.h"
#import "Common.h"
#import "Brain.h"
#import "NSCommon.h"
#import <GLUT/glut.h>
#import "GlCommon.h"
#import "Id+Info.h"
#import "FactalWorkbench.h"

@implementation Label

#pragma mark 1. Basic Object Level

- init; {							self = [super init];	// consider using init instead

	self.text			= 0;
	self.fontNumber		= -1;		// use default.
	return self;
}

- (void)dealloc   {
	self.text = 0;

	[super dealloc];
}

- (int) unsettledPorts;	{		return 0;						}

#pragma mark  3. Deep Access

- (id) deepMutableCopy;	{			Label *rv = [super deepMutableCopy];

	rv.text 			= self.text;
	rv.fontNumber 		= self.fontNumber;
	return rv;
}

#pragma mark 4. Factory 

Label *aLabel(id text, int fontNumber, id jog, id etc) {
	id info = @{@"text":text?:@0,
				@"fontNumber":[NSNumber numberWithInt:fontNumber],
				@"jog":jog?:@0, etcEtc};

	Label *newbie = anotherBasicPart([Label class]);
XX	newbie = [newbie build:info];
	return newbie;
}

- (id) build:(id)info; {	[super build:info];

//! xzzy1 Label::		1. text:			:@""			-- text of lable; multi-line
//! xzzy1 Label::		2. fontNumber:		:@<int>			-- text font size: 0=OFF, 1..6

	self.text = @"";
	if (id str = [self takeParameter:@"text"])
		self.text = str;
	if (id fontNumber = [self parameterInherited:@"fontNumber"])
		self.fontNumber = [fontNumber intValue];		// bug?: if not number, 0
	return self;
}

#pragma mark 9. 3D Support
- (void) placeIntoView:(View *)v;	{
	v.position = [self suggestedPositionOfSpot:self.jog];
}

#pragma mark 11. 3D Display
- (void) drawFullView:(View *)v context:(RenderingContext *)rc; {

	if (self.brain.displayLabels) {
		glPushMatrix();
			rc.color = colorPurple;
//			Vector3f inCameraShift = Vector3f(0.0, 0.0, 3);	// in "facing camera" coordinates
			myGlDrawString(rc, self.text, self.fontNumber, zeroVector3f);
		glPopMatrix();
	}
	[super drawFullView:v context:rc];				// PRIMARY
}

#pragma mark 15. PrettyPrint
- (NSString *) pp1line:aux; {		NSString *rv = [super pp1line:aux];

	rv=[rv addF:@"\"%@\"", self.text];
	return rv;
}

@end
