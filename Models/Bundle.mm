//  Bundle.mm -- A hierarchical structure of Leafs, one per port C2013PAK

/* to do:
 */
#import "Bundle.h"
#import "Brain.h"
#import "Leaf.h"
#import "Port.h"
#import "Atom.h"

#import <GLUT/glut.h>
#import "GlCommon.h"
#import "View.h"

#import "Id+Info.h"
#import "Path.h"

#import "FactalWorkbench.h"

#import "Common.h"
#import "NSCommon.h"
#import "SystemTypes.h"
#import "Tunnel.h"

extern NSString *bitArg;
@implementation Bundle

#pragma mark 1. Basic Object Level
- (id) init {											self = [super init];

	self.placeParts = str2placeType(@"+Xcc");	// set X default Bundle orientation
	return self;
}

- (void) dealloc; {
	self.bid				= nil;
	self.label				= nil;
//	self.positionPriorityXz	= 0;
    [super dealloc];
}


   // Bid acts as proxy for bundle
  //
 ////////
- (Port *) connectedTo; {
	return self.bid.connectedTo;
 }
- (void) setConnectedTo:(Port *)connectedTo; {
	self.bid.connectedTo = connectedTo;
 }
 ////////
- (float) value; {
	return self.bid.value;
 }
- (void) setValue:(float)value; {
	self.bid.value = value;
 }
 ////////
- (float) valuePrev; {
	return self.bid.valuePrev;
 }
- (void) setValuePrev:(float)valuePrev; {
	self.bid.valuePrev = valuePrev;
 }
 ////////
- (char) noCheck; {
	return self.bid.noCheck;
 }
- (void) setNoCheck:(char)noCheck; {
	self.bid.noCheck = noCheck;
 }
 ////////
- (char) dominant; {
	return self.bid.dominant;
 }
- (void) setDominant:(char)dominant; {
	self.bid.dominant = dominant;
 }

#pragma mark  3. Deep Access
- (id) deepMutableCopy; {		Bundle *rv = [super deepMutableCopy];

	rv.bid					= self.bid.deepMutableCopy;
	rv.positionPriorityXz	= self.positionPriorityXz;
	rv.label				= self.label;
	return rv;
}

- (int) nLeaves; {
	__block int nLeaves = 0;
	[self forAllLeafs:^(Leaf *leaf) {			//##BLOCK
		nLeaves++;
	}];
	return nLeaves;
}

#pragma mark 4: Factory
 /// Want Depricated
Bundle *aBundle(id structure, id proto, id etc) {
	id info = @{@"structure":(structure?:@0),
				@"proto"    :(proto    ?:@0),				etcEtc};
	return aBundle(info);
}

 /// Main Constructor
Bundle *aBundle(id etc) {
	etc = flattenFromInto(etc, 0);				// flatten early, so isa works. (normally done in build:
	NSString *isa = etc[@"isa"];				// (default class is Bundle)
	Class class_ = [isa isEqualToString:@"tunnel"]? [Tunnel class]: [Bundle class];

	 /// ==== Base, to build Bundle within
	Bundle *newbie = anotherBasicPart(class_);	// Build a new basic Bundle
XX	[newbie build:etc];
	
	//[self takeParameter:@"isa"];
	return newbie;
}


- (id) build:(id)info;	{								[super build:info];

//! xzzy1 Bundle::		1. structure			:<names>	-- names of the Bundle's leafs
//!	xzzy1 Bundle::		2. proto				:<type(s)>	-- kinds of leafs
//!	xzzy1 Bundle::		3. bid					:<?bid?		-- special bid port
//!	xzzy1 Bundle::		4. positionPriorityXz	:<(int)>	-- 0=?, 1=X, 2=Z

	if (id structure = [self takeParameter:@"structure"]) {
//	if (id structure = [self parameter:@"structure"]) {
		id proto = [self takeParameter:@"proto"];
//		id proto = [self parameterInherited:@"proto"];
		bool hasProto = proto and !coerceTo(NSNumber, proto);	// proto:@0 is no proto
		if (!hasProto)			// 171001 I do not understand
			proto = coerceTo(Tunnel, self)? aGenBcastLeaf() // Tunnel proto default
										  : aBcastLeaf();	// Bundle proto default
		
XX		[self applyConstructors:structure proto:proto];
	}

	if (id bid = [self parameter:@"bid"]) {
		panic(@"");
		self.bid = bid;
	}
	if (id positionPriorityXz = [self takeParameter:@"positionPriorityXz"])	//parameterInherited
		self.positionPriorityXz = [positionPriorityXz intValue];

	return self;
}

  // build onto bundle (i.e. self) per specification
 //  RETURN: augmentation bundle
/*

  // Specifying the elements, at current level:
1. Number	e.g: @2
2. Array	e.g: [v1, v2, v3], where each value v* is a Bundle, constructor, or parental property:
				Bundles may be simple Leafs or complex Bundles
				all valid constructor strings are allowed
				all properties are strings that apply to the parent e.g. "spin:L" or "minSize":"3,3,3"
3. Hash		e.g: {k1:v1, k2:v2, k3:v3}, where each pair may be:
				keys k* can be strings, naming the object
				values v* are as above.

constructor:
<leafSTR>		::= @"<name>"				// <name>
				::= @"<name>:<spin>"		// <spin> = spin$R, ...
				::= @"<name>:spin:<dir>"	// <dir> = L, R, ?
<leafPAIR>		::=

<name>			::= [[ a-zA-Z1-9 ???]] **

<bunListDesc>	::= "spin:L"|"sL" |"spin:R"|"sR" |"spin:2"|"s2" |<nil>


 */

/*
	Constructors:						// When applied to self (a bundle):
Number:		@0								// do nothing
			@N (N!=0)						// signal ERROR
Array:		@[<subSpec>,...]				// apply each element of Array to self
Hash:		@{<specKey>:<specVal>,...]		// apply each pair in Hash to self
String:		@"<string>"						// apply string to self
Bundle:		xx

 */

/*
	applyConstructors:con proto:proto
 */

- (void) applyConstructors:con proto:proto; {

	  //// ==== Parse con:  It has many forms:

	 ///// NUMBER: @0 is nil entry, @N (N!=0) is error
	if (NSNumber *structureNum = [con asNumber]) {
		int n = (int)structureNum.integerValue;
		assert(n==0, (@"ERROR '@%d' (%d!=0)",n,n));
	}										// Do Nothing more

	 ///// ARRAY: apply each element of con to self
	else if ([con isArray])				// Array of constructors to apply to bundle
		for (id subSpec in con)			// paw through array, build each
			[self applyASpec:subSpec subSpec:nil proto:proto];

	  ///// HASH: apply each par of con to self (key->con; val->subSpec
	 // <key>=name of new element; <val>=kind of element
	else if ([con isHash])				// Hash of name:kind
		for (id conKey in con)				// paw through hash
			[self applyASpec:conKey subSpec:con[conKey] proto:proto];
			// newElt		name		  constructor	prototypes
										//BUGGY:.name; = subSpecKey;
	  ///// STRING:
	else if ([con isString])
		[self applyASpec:con subSpec:nil proto:proto];

	  ///// Bundle (and Leaf):
	else if (coerceTo(Bundle, con))
		[self applyASpec:con subSpec:nil proto:proto];

	else
		panic(@"Append per structure '%s' not understood", [con ppC]);
}

/* Apply one specification/subSpecification to self
	- applyASpec:spec subSpec:subSpec proto:proto;
	HUNGARIAN: CONstructor SPECification

								FUNCTIONS

spec:___________subSpec:_//___FUNCTION_PERFORMED___________EXAMPLE___________

ARRAY					 // 4  PROCESS con's in array	// E.g: @[con1, ..]
HASH					 // 4  PROCESS key:con's in hash// E.g: @{name1:con1, ...}
BUNDLE					 // 5  ADD bundle				// E.g: (Leaf *)leaf
NUMBERs					 // 6  EPOCH NUMBER				// E.g: @3 is animation epoch 3
STRING:
 "<propNval>"		  0	 // 1a self.PROP=VAL			// E.g: "minSize:3,4,5"
 "<prop>"		   <val> // 1b self.PROP=VAL			// E.g: "minSize":"3,4,5"
 "<prop>:<val>"		  0	 // 2A self.PROP=VAL			// E.g: "minSize:3,4,5"

 "<name>"		   <spec>// 1c ADD(<spec>)				// E.g: "foo:"
 "<name>:<prop>"   <val> // 2B ADD(proto[0]).PROP=VAL	// E.g: "foo:spin":"R"
 "<name>:<propNval>"  0	 // 2c ADD(proto[0]).PROP=VAL	// E.g: "foo:spin_R"
 "<name>:@<i>"		  0	 // 2d ADD(proto[<i>]).PROP=VAL	// E.g: "foo:@1"
 "<name>:<prop>:<val>"0	 // 3a ADD(proto[0]).PROP=VAL	// E.g: "foo:spin:

If <name> is specified, a new is added so named. <prop>=<val> applies to it.
If no <name>			  new is added			 <prop>=<val> applies to self.
 */
- (Bundle *) applyASpec:spec subSpec:subSpec proto:proto; {
	Bundle *newElt	= nil;

	   // Interpret << spec >>, making appropriate changes to self, a Bundle

	 // Case 4: ARRAYs and HASHs -- add all, recursively	// E.g: @[name1, ..], or @{name1:kind1, ...}
	if ([spec isArray] or [spec isHash]) {

		newElt = anotherBasicPart(self.class);	// Build a new Bundle
XX		[newElt applyConstructors:spec proto:proto];
		[self addPart:newElt];
	}
	 // Case 5: BUNDLE -- ready immediately					// E.g: (Leaf *)leaf
	else if (coerceTo(Bundle, spec)) {
		newElt = spec;
		[self addPart:spec];						// silently reach inside and insert
	}
	 // Case 6: NUMBERs -- epoch marks						// E.g: @3 is animation epoch 3
	else if (coerceTo(NSNumber, spec))
		[self.parts addObject:spec];				// just pass it on, not as a part

	  //
	 // Case 1: STRINGs -- rich semantics
	else if (NSString *con1 = [spec asString]) {
		typedef void (^DelayedProperty)(Part *part);
		DelayedProperty newsProperty	= nil;			// to apply to new element
		Bundle *dummy					= nil;			// temporary decoder element

		 // Apply string constructor to newElt, either augment self or make new element below.
		NSArray *tokens = [con1 componentsSeparatedByString:@":"];
		switch (tokens.count) {
			default:
				panic(@"%@: unable to parse '%@' '%@'", self.fullName, con1, subSpec);

			break; case 1:					/// con1=tokens[0]; subSpec

				 // Case 1a: -- Try (0)propNVal					// E.g: "minSize:3,4,5"
				if ([self applyPropNVal:tokens[0]])
					assert(subSpec==0, (@"property %@ has non-nil subSpec", con1)); // no newElt

				 // Case 1b: -- Try (0)prop withVal (S)subSpecsr..// E.g: "minSize":"3,4,5"
				else if ([self applyProp:tokens[0] withVal:subSpec])
					;								// property -- no newElt

				 // Case 1c: -- name -- make new element
				else {
XX					newsProperty = ^(Part *p) {		//##BLOCK
						p.name = tokens[0];
					};
				}
			break; case 2: 					/// con1=tokens[0,..1]; subSpec
				  // Try each case, in succession:
				 // Case 2A: -- Try (0)prop : (1)val			// E.g:	"minSize":"3,0,0"
				if ([self applyProp:tokens[0] withVal:tokens[1]]) {
					assert(subSpec==0, (@"property %@ has non-nil subSpec", con1)); // no newElt
				}
				  // --- All decodings past here apply to the new bundle ---
				 // Case 2B: -- Try (0)name:(1)prop (s)val		// E.g: "<name>:minSize":"3,0,0"
				else if ([dummy=aLeaf() applyProp:tokens[1] withVal:subSpec]) {
XX					newsProperty = ^(Part *p) {		//##BLOCK
						[p applyProp:tokens[1] withVal:subSpec];
						p.name = tokens[0];
					};
				}
				 // Case 2c: -- Try (0)name:(1)propNVal			// E.g:	"<name>:sR"	// prop affects newbie
				else if ([dummy applyPropNVal:tokens[1]]) {
XX					newsProperty = ^(Part *p) {		//##BLOCK
						[p applyPropNVal:tokens[1]];
						p.name = tokens[0];
					};
				}

				 // Case 2d: -- (0)name : (1)number				// E.g: @"name:@1"	// number chooses prototype
				else if (subSpec==0 and [tokens[1] hasPrefix:@"@"])
					newsProperty = ^(Part *p) {		//##BLOCK
						p.name = tokens[0];
					};
				else {
					panic(@"%@: unable to parse '%@' '%@'", self.fullName, con1, subSpec);
					dummy = 0;
				}

			break; case 3:						/// con1=tokens[0,..2]
				  // --- All decodings past here apply to the new bundle ---
				 // case 5: name : prop : val
				Leaf *dummy = [self resolveProto:proto subSpec:subSpec tokens:tokens];
				if ([dummy applyProp:tokens[1] withVal:tokens[2]])
XX					newsProperty = ^(Part *p) {		//##BLOCK
						[p applyProp:tokens[1] withVal:tokens[2]];
						p.name = tokens[0];
					};
				else
					panic(@"%@: unable to parse '%@' '%@'", self.fullName, con1.pp, [subSpec pp]);
		}

		   // Make a new element, per newsName
		  //    Copy the properties of decoder into the new element.
		 //
		if (newsProperty) {
			id debugProto = proto;

			 // CASE 1: Make newElt from subSpec
			if ([subSpec isArray] or [subSpec isHash]) {

				newElt = anotherBasicPart(self.class);	// Build a new Bundle or Tunnel
				newsProperty(newElt);				// Apply delayed property
XX				[newElt applyConstructors:subSpec proto:proto];
			}

			 // CASE 2: See if a prototype exists, get a copy:
			else {
XX				newElt = [self resolveProto:proto subSpec:subSpec tokens:tokens];
XX				newsProperty(newElt);				// Apply delayed property to it
			}
			[self addPart:newElt];
			return newElt;
		}
	}

	else
		panic(@"Unknown Bundle spec type");

	return newElt;
}

- (Leaf *) resolveProto:proto subSpec:(id)subSpec tokens:(NSArray *)tokens; {
	   // PROTOTYPE for the new leaf has many forms:
	  //
	 /// Unwrap BLOCK (It's in a Block to delay evaluation till use.)
	typedef NSArray * (^ProtoBlock)();
	if ([proto isBlock])
		proto = ((ProtoBlock)proto)();			// Call it to get proto
	
	 /// Unwrap ARRAY
	int protoIndex = 0;						// default
	if (coerceTo(NSArray, proto)) {			/// ARRAY
		
		  // Get protoIndex into Array of protos (mostly in case it is an array)
		 // String of form: <key>:@<n>
		if (subSpec==0 and tokens.count== 2 and [tokens[1] hasPrefix:@"@"])
			protoIndex = [[tokens[1] substringFromIndex:1] intValue];
		
		 // String of form: @<n>: NSNumber:
		else if ([subSpec isNumber] or [subSpec isString])
			protoIndex = [subSpec intValue];		// convert it to an int
		assert(protoIndex>=0 and protoIndex<[proto count], (@"Element %d is not in proto '%@'", protoIndex, [proto pp]));
		proto = coerceTo(Bundle, proto[protoIndex]);
	}

	 // Proto should have resolved to a single Leaf (Net):
	assert(coerceTo(Net, proto), (@"Unable to obtain prototype"));
XX	Leaf *newElt = [proto deepMutableCopy];	// get a copy of leaf prototype
	return newElt;
}

#pragma mark 6. Navigation
  ////////////////////////////////////////////////////////////////////////////
 //////////////////////// Bundle Sub-Tree NAVIGATION ////////////////////////
////////////////////////////////////////////////////////////////////////////

- (void) forAllLeafs:(LeafOperation)leafOperation  {
	for (id elt in self.parts)
		if (Bundle *subBlk = coerceTo(Bundle, elt))		// ignore Ports and numbers
XR			[elt forAllLeafs:leafOperation];
}

  // Find Port in targetBundle whose name is leafPathStr.
 // Returns nil if Leaf not found, or if it has no "G" bindings
- (Port *)genPortOfLeafNamed:leafStr; {
	__block Leaf *soughtLeaf=0;

	 // go through my Leafs, looking for leafStr
	[self forAllLeafs:^(Leaf *leaf) {			//##BLOCK
		if ([leaf.name isEqualToString:leafStr])
			soughtLeaf = leaf;
	}];
	return [soughtLeaf port4leafBinding:@"G"];
}

#pragma mark 9. 3D Support
// \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\    //
//  \\ \\ \\ \\ \\ \\ \\ \\ \ 3D Support\\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\  //
//   \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\//

- (Bounds3f) gapAround:(View *)v; {
		return Bounds3f( -1, -2, -1,
						  1,  0,  1 );
}

 // our subviews have been placed, we sneak an adjustment in there:
- (void) placeIntoView:(View *)v; {

	  // Place 'ALL' Port specially:
	 //    the tip of the funnel
	if (Part *subModel = [self partNamed:@"ALL"]) {
		if (View *subView  = [v subViewFor:subModel])
			subView.positionTranslation = v.bounds.centerYBottom();
		else
			panic(@"View of element named ALL not found");
	}
	[super placeIntoView:v];
}

#pragma mark 11. 3D Display
//// // // // // // // // // // // // // // // // // // // // // // // ///
/// // // // // // // // // // 3D Display  // // // // // // // // // ////
// // // // // // // // // // // // // // // // // // // // // // // // //
- (void) drawFullView:(View *)v context:(RenderingContext *)rc; {
	 // change color if bandColor
	rc.color = colorWhite;

	  /// draw "Ears" for picking
	 ///
	Vector3f size	= v.bounds.size();
	float plateWidth = 0.9;
	glPushMatrix();
		rc.color	= colorGrey8;
		glTranslatefv(v.bounds.center());
		glTranslatef(0, 0, (size.z-plateWidth)/2);
		glScalef(2, 0.8, 1);									//(0.8, 2, 1)
		//					rad  cut  nLong, nHor
		myGlSolidHemisphere(0.5, 0.5, 8,     8);
	glPopMatrix();

	if (Port *p = coerceTo(Port, self.bandColor))
		rc.color	= p.colorOfValue;

	  /// display Bundle names
	 ///
	if (self.brain.displayAtomNames) {
		glPushMatrix();
			glTranslatefv(v.bounds.centerYBottom() + Vector3f(0,0.5,0));
			Vector3f inCameraShift = Vector3f(0.0, 0.0, 4);	// in "facing camera" coordinates
			Vector2f spotPercent = Vector2f(0.5, 0.5);		// spot in center of text billboard
			rc.color = colorRed;
			myGlDrawString(rc, self.name, -1, inCameraShift, spotPercent); //background=2 has background size bug
		glPopMatrix();
	}

	 // Attached generator may have registered a label
	if (self.label) {
		glPushMatrix();
			glTranslatefv(v.bounds.centerXLeft() + Vector3f(-3,0,0));
			Vector3f inCameraShift = Vector3f(0.0, 0.0, 4);
			Vector2f spotPercent = Vector2f(0.5, 0.5);
			rc.color = colorBlue;
			myGlDrawString(rc, self.label, 6, inCameraShift, spotPercent);
		glPopMatrix();
	}

	[super drawFullView:v context:rc];
}

- (FColor) colorOf:(float)ratio; {
	static FColor inside=  {0.9, 0.9, 1.0,  1},  outside=  {0.8, 0.8, 1.0,  1};
//	static FColor inside=  {0.7, 0.7, 0.8,  1},  outside=  {0.5, 0.5, 0.6,  1};
//	static FColor inside=  {0.8, 0.7, 0.7,  1},  outside=  {0.6, 0.5, 0.5,  1};
	return lerp(inside, outside, ratio);
}

@end
