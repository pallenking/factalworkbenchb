//  Bundle.h -- A hierarchical structure of Leafs, one per port C2013PAK


/*! a Bundle's part array contains either:
		a) sub-bundles
		b) Leafs

	Semantics of structure:
		Modify self:
		1.	"propNVal"							set prop of self to val
		2.	"property":value (from arg)			set property of self to value
				e.g @"spin":@"3"
		3.	"property:value"					set property of self to value

		New port in self
		4.	"name"								new port in self
		5.	"name:propNVal"						new port with propNVal in self
		6.	"name:property:value"				new port with property as value in self

	property
		spin, value:
			r,R		--> 1
			l,L		--> 3
		minSize, value:
			%f,%f,%f
	PropNVal
		Spin sub-Bundle wrt self:
			s0		--> spin$0,
			s1, sR	--> spin$R
			s2		--> spin$2
			s3, sL  --> spin$L
  */

#import "Net.h"
#import "Port.h"
@class Bundle;
@class Leaf;

@interface Bundle : Net //<PortProperty>

	@property (nonatomic, retain) Port *bid;
	@property (nonatomic, assign) char positionPriorityXz;	// how to place subBundles
		//	0 --	as context, exclude
		//	1 --	as context, x has priority
		//	2 --	as context, z has priority
	@property (nonatomic, retain) id label;					// Of pattern in Bundle
                                                            // For Morse Code character

#pragma mark  3. Deep Access
- (int) nLeaves;

#pragma mark 4: Factory
Bundle *aBundle(									id etc);
Bundle *aBundle(		 	id structure, id proto, id etc);
Bundle *aBundle(Class kind, id structure, id proto, id etc);

#pragma mark 6. Navigation

 // define an operation to be evaluated for each Leaf
typedef void		(^LeafOperation)(Leaf *);
- (void)			forAllLeafs:(LeafOperation)leafOperation;
- (Port *)			genPortOfLeafNamed:leafPathStr;

#pragma mark 15. PrettyPrint
///////////////////////////////// ... ...  //////////////////////////////////
//- (NSString *)		ppTerminalsWValues;

@end

