//  Ago.mm -- Connects the Haves and Wants of 3 ports

/*
 */

#import "Ago.h"
#import "Brain.h"
#import "Id+Info.h"
#import "FactalWorkbench.h"

#import "View.h"
#import "Common.h"
#import "NSCommon.h"
#import "GlCommon.h"
#import <GLUT/glut.h>

@implementation Ago

#pragma mark  4a Factory Access						// Methods defined by Factory
- (id) atomsDefinedPorts;	{				id rv = [super atomsDefinedPorts];
											// probably returns @"P"
	rv[@"S"]		= @"pc ";
	return rv;
}
atomsPortAccessors(sPort,		S, false)

#pragma mark 4. Factory 
Ago *anAgo(id pri, id etc) {
	id info = @{etcEtc}.anotherMutable;
	if (pri)
		info[@"P"] = pri;

	Ago *newbie = anotherBasicPart([Ago class]);
XX	newbie = [newbie build:info];
	return newbie;
}

- (id) build:(id)info;	{					[super build:info];

//!	xzzy1 Ago::			1.	bind previousClock toAgo:

	self.parameters[@"addPreviousClock"] = @1;
	return self;
}

#pragma mark 8. Reenactment Simulator
///*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//
//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//
////*//*//*//*//*//*//*//*//    Reenactment Simulator   //*//*//*//*//*//*//*//
///*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//
//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//

- (void) simulateDown:(bool)downLocal; {

	if (!downLocal)						//============: going UP
		float a = self.pPortIn.valueGet;					// just clear out Ports
//		float a = self.primary.connectedTo.valueGet;		// just clear out Ports

	else								//============: going DOWN
		float a = self.sPortIn.valueGet;					// just clear out Ports
//		float a = 0;//self.secondary.connectedTo.valueGet;	// just clear out Ports

	[super simulateDown:downLocal]; // just clear out Ports
}

- (id) receiveMessage:(FwEvent *)event; {

	if (event->fwType == sim_clockPrevious)
		[self clockPrevious];	/// Ago got -receiveMessage; send customers

	return nil;			 // do not call super needed
}						// because the Previous manages everything inside itself.

- (void) clockPrevious; {
	[self logEvent:@"|| -clockPrevious to Ago: %.2f->P, %.2f->S", self.sPortIn.valueGet, self.pPortIn.valueGet];

	 // Update LATCH
	self.pPort.valueTake = self.sPortIn.valueGet;
	self.sPort.valueTake = self.pPortIn.valueGet;

	self.flash = self.brain.flashFrames;
}

#pragma mark 9. 3D Support
// \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\    //
//  \\ \\ \\ \\ \\ \\ \\ \\ \ 3D Support\\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\  //
//   \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\//
float agoHeightRs = 3.0;

- (Bounds3f) gapAround:(View *)v; {
	float r = self.brain.bitRadius;
	Bounds3f b = Bounds3f(-2*r,	0.0, -2*r,
						   2*r, agoHeightRs*r,  2*r);
	return b;
}

- (Matrix4f) positionOfPort:(Port *)port inView:(View *)bitV; {

	if ([port.name isEqualToString:@"T"]) {// port part 2 -- CONTROL
		port.latitude	= 4;
		return [port suggestedPositionOfSpot:bitV.superView.bounds.centerXRight()];
	}
XR	return [super positionOfPort:port inView:bitV];
}

#pragma mark 11. 3D Display
//// // // // // // // // // // // // // // // // // // // // // // // ///
/// // // // // // // // // // 3D Display  // // // // // // // // // ////
// // // // // // // // // // // // // // // // // // // // // // // // //

- (void) drawFullView:(View *)v context:(RenderingContext *)rc; {
	float r = self.brain.bitRadius;
	rc.color = colorPurple4w;//colorPurple4;//colorPurple
	Vector3f size = v.bounds.size();
	glPushMatrix();
		glTranslatef(self.brain.displayOffsetX, v.bounds.center().y, self.brain.displayOffsetZ);
		glPushMatrix();
			glScalef(size.x, .5, .5);
			glutSolidCube(1.0);
		glPopMatrix();
		glPushMatrix();
			glScalef(.5, .5, size.z);
			glutSolidCube(1.0);
		glPopMatrix();
	glPopMatrix();

	[super drawFullView:v context:rc];				// PRIMARY
}

- (FColor)colorOf:(float)ratio;		{	return colorPurple4w;/*colorPurple4;*/}//colorOrange

#pragma mark 15. PrettyPrint
    ///\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\///
   ///\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\///
  ///\\//\\//\\//\\//\\//\\//\\// PRINTOUT //\\//\\//\\//\\//\\//\\//\\//\\///
 ///\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\///
///\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\///
- (NSString *) pp1line:aux; {		id rv = [super pp1line:aux];

//	rv=[rv addF:@"%@ ", [self ppViewAs:aux]];
//	rv=[rv addF:@"m%.1f", self.cruxLatch];
//	rv=[rv addF:@"m%.1f p%.1f '%@'", self.curLatch, self.prevLatch, self.mode];
	return rv;
}


@end
