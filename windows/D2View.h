// D2View.h -- override NSView, so drawRect goes back to stux methods

#import <Cocoa/Cocoa.h>
static const int maxPalette = 10;
typedef void (*CALLBACK) (void *delegateObject, void *window);

@interface D2View : NSView
  {
	NSWindow *window;

    CALLBACK delegateMethod;
    void *delegateObject;

    int numPaletteV1, numPaletteV2; // most colors map into this range
    NSColor *palette[maxPalette];
}
//    @property (nonatomic, retain) NSWindow *window;

//    @property (nonatomic, retain) id delegateObject;

-			initWithWindow:aWindow;

-			setDrawingMethod:(CALLBACK)method object:(void *)ob;

- (void)	makeColorPalette;
- (void)	setColor:(int)num;

- (void)	drawRect:(NSRect)dirtyRect;

- (void)	drawBox      :(NSRect)box color:(int)color_;
- (void)	drawRect     :(NSRect)box color:(int)color_;
- (void)	drawDottedRect:(NSRect)box color:(int)color_;
- (void)	putPoint     :(NSPoint)point color:(int)color_; 

@end
