// GlutViewin.mm -- generalized Window interface for Glut

#include "GlutViewin.h"
#import "Garden.h"

@implementation GlutViewin

#include "Plant.h"
#include "Common.hpp"
#import <GLUT/glut.h>
#import "zpr.h"
extern GLfloat backgroundColor[];

//http://www.gnustep.it/nicola/Tutorials/WindowsAndButtons/node18.html has createWindow

void glutInz (int &argc, char *argv[]) {
//    int argcc=1; char *argvv[2]={argv[0], 0};
//	glutInit(&argcc, argvv);    // Initialize Graphics Library (GL)
	glutInit(&argc, argv);    // Initialize Graphics Library (GL)
}

void glutRun (Garden *activeGarden_) {
    //	activeGarden = activeGarden_;
	glutMainLoop();
}

// Forward definitions for GLUT callbacks:
void reshape(int w, int h);
void idle(void);
void keyboard(unsigned char key, int x, int y);
void newFrame();
void reshape(int w, int h) {			// window resized
	glViewport(0, 0, (GLsizei) w, (GLsizei) h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if (w <= h) 
		glOrtho (-2.25, 2.25, -2.25*h/w, 2.25*h/w, -10.0, 10.0);
	else 
		glOrtho (-2.25*w/h, 2.25*w/h, -2.25, 2.25, -10.0, 10.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void idle(void) {
    glutPostRedisplay();	
}

// ==================================================================
// ====================== Instance Methods: =========================
// ==================================================================

-initX :(float)x y :(float)y w :(float)w h :(float)h;
{
    [super initX :x y :y w :w h :h];

    glutInitDisplayMode (GLUT_SINGLE | GLUT_RGB | GLUT_ACCUM | GLUT_DEPTH);
	glutInitWindowSize (w, h);
	glutInitWindowPosition (x, y);
    static int wn=1;
    char buff[20]; sprintf(buff, "window%d", wn++);
	glutWinNo = glutCreateWindow (buff);     // identifies window
	
    // LIGHTING:			// http://www.sjbaker.org/steve/omniv/opengl_lighting.html
	glEnable(GL_LIGHTING);
	glClearColor(backgroundColor[0], backgroundColor[1], backgroundColor[2], backgroundColor[3]);
//	glClearColor(0.7, 0.7, 0.8, 0.0);		//;
    
    
    //// AMBIENT:
	GLfloat lm_ambient[] = { 0.1, 0.1, 0.1, 1.0 };		//{ 1, 1, 1, 1.0 } { 0.6, 0.6, 0.2, 1.0 } { 0, 0, 0, 1.0 }
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lm_ambient);
	
    //// SPOT:
	GLfloat light_position0[] = { 10.0, 0.0, 10.0, 1.0 };	// { 0.0, 0.0, 10.0, 1.0 }
	glLightfv(GL_LIGHT0, GL_POSITION, light_position0);
	glEnable(GL_LIGHT0);
//	GLfloat light_position1[] = { 10.0, 0.0,-10.0, 1.0 };
//  glLightfv(GL_LIGHT1, GL_POSITION, light_position1);
//	glEnable(GL_LIGHT1);
//	GLfloat light_position2[] = { 10.0,-0.0,-10.0, 1.0 };
//	glLightfv(GL_LIGHT2, GL_POSITION, light_position2);
//	glEnable(GL_LIGHT2);
//	GLfloat light_position3[] = { 10.0,-0.0, 10.0, 1.0 };
//	glLightfv(GL_LIGHT3, GL_POSITION, light_position3);
//	glEnable(GL_LIGHT3);

	GLfloat mat_ambient[] = { 0.5, 0.5, 0.5, 1.0 };		//{ 1.0, 1.0, 1.0, 1.0 }
	glMaterialfv(GL_FRONT, GL_AMBIENT, mat_ambient);
    
	GLfloat mat_specular[] = { 0.4, 0.4, 0.4, 1.0 };	//{ 1.0, 1.0, 1.0, 1.0 }
	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
    
	glMaterialf(GL_FRONT, GL_SHININESS, 5.0);          //50.0
    
	glEnable(GL_DEPTH_TEST);
	glShadeModel(GL_SMOOTH);
	glShadeModel(GL_FLAT);
	
/// removed for the hell of it
	glutReshapeFunc(reshape);			// window reshaped
    glutIdleFunc(idle);

	glutDisplayFunc(newFrame);//    panic("newFrame went away");
	glutKeyboardFunc(keyboard);
	zprInit();
    return self;
}

- (void) display3D; {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glPushMatrix();
	glTranslatef (.1, -.9, .1);   //	glTranslatef (0, -1, 0); //	glTranslatef(0, 0.0f, 0.0f);
	
	[myGarden display3D];
	
	glPopMatrix();
    //    panic("xxx");
    glutSwapBuffers();
//	return self;
}

- runViewin {
    glutMainLoop();
    return self;
}

- (int) drawBoxX :(int)x0 y :(int)y0 w :(int)dx h :(int) dy color: (int)color; {

    glutSetWindow(glutWinNo);

   // glClear(GL_COLOR_BUFFER_BIT);//Clear the screen
// HACK CODE    
    glColor3f(1,0,0);//Change the object color to red
    
    glBegin(GL_TRIANGLES);//Start drawing a triangle
    glVertex2f(3,-4);//draw our first coordinate
    glVertex2f(3.5,-3);//Our second coordinate
    glVertex2f(4,-4);//Our last coordinate
    glEnd();//Stop drawing triangles
    
//    glFlush();//Draw everything to the screen
//    glutPostRedisplay();//Start drawing again
	return 0;
}

- (int) drawDottedRectX:(int)x0 y :(int)y0 w :(int)dx h :(int) dy color: (int)color; {
    panic("oops");
    return 0;
}

- (GeneralViewinPoint) mouseClick; {
    panic("oops");
    return GeneralViewinPointNULL;
}

- putPointX :(int)x_ y :(int)y_ pattern :(int)patrn color :(int) colr; {

    glutSetWindow(glutWinNo);
    static int patternPhase = 0;
    if (patrn & (1 << patternPhase++)) {
//        SDL_Rect rect = {x_, y_, 1, 1};
//        SDL_FillRect(sdlWin, &rect, colr);
    }
    patternPhase &= 0x1f;
    return 0;
}

- (int) displayViewin;			{

	if (self) {
        glutSetWindow(glutWinNo);
	//	glPopMatrix();
        glutSwapBuffers();
        return 0;
    }
    return -1; 
}

@end
