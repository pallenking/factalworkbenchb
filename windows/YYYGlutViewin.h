// GlutViewin.h -- generalized Window interface

#import "GeneralViewin.h"
//#import "Garden.h"

@interface GlutViewin : GeneralViewin {
//	GLUT_Surface *glutWin;
    int glutWinNo;
}

//- run;
- (int) drawBoxX :(int)x0 y :(int)y0 w :(int)dx h :(int) dy color: (int)color;
- (int) drawDottedRectX:(int)x0 y :(int)y0 w :(int)dx h :(int) dy color: (int)color;
- putPointX :(int)x_ y :(int)y_ pattern :(int)patrn color :(int) colr;

- (GeneralViewinPoint) mouseClick;
- (int) displayViewin;
//- (int) close;
//- setTarget :mainGarden;

void glutInz (int &argc, char *argv[]);
void glutRun (Garden *activeGarden_);

@end