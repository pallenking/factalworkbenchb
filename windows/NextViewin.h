// NextViewin.h -- GeneralViewin subclass for NextStep windows

#import "GeneralViewin.h"
#include "NextView.h"
//class NextView;
//static const int MAXdim=4, MAXside=5;

@interface NextViewin : GeneralViewin
{
    NSWindow *theNsWindow;
    NextView *myView;
}

- setDrawingMethod :(CALLBACK)method object :(void *)ob;
- (BOOL)isFlipped;

NSWindow *testNextViewinFoo();

@end
