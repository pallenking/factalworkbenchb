//  SimpleOpenGL.mm -- IntroToOpenGL

#import "SimpleOpenGL.h"
#include <iostream>//This is just for basic io, you really DON’T need it
#include <GLUT/GLUT.h>//GLUT Library, will make you life easier
#include <OpenGL/OpenGL.h>//OpenGL Library

void myInit(void);      // a function to initialize everything
void myInit(void){
    glClearColor(1.0,1.0,1.0,1.0);//This determines the background color, in this case White
    glMatrixMode(GL_PROJECTION);//Determines which matrix to apply operations to. Don’t worry about this for now just assume it has to be there
    glLoadIdentity();//Loads the Identity matrix
    gluOrtho2D(0, 640, 0, 480);//Set the size and projection of the buffer
}

void myDisplay(void);
void myDisplay(void){   // a function to display some stuff

#ifndef oldWay
    glClear(GL_COLOR_BUFFER_BIT);//Clear the buffer

    glColor3d(1,0,.5);//Set the color for this polygon
    glBegin(GL_POLYGON);//Let us begin drawing some points
    //Specify the points
    glVertex3i(100,  50, 0);
    glVertex3i(100, 130, 0);
    glVertex3i(150, 130, 0);
    glVertex3i(150,  50, 0);
    glVertex3i(100,  50, 0);
//    glVertex2i(100,50);
//    glVertex2i(100,130);
//    glVertex2i(150, 130);
//    glVertex2i(100,50);
    glEnd();//Ok we are done specifying points

    glColor3d(1,.5,0);//Set the color for this polygon
    glBegin(GL_POLYGON);//Let us begin drawing some points
    glVertex3i( 90,  50,  0);
    glVertex3i( 50,  90,  5);
    glVertex3i( 90, 130,  0);
    glVertex3i( 90,  50,  0);
    glEnd();//Ok we are done specifying points

    //extern void glVertex3i (GLint x, GLint y, GLint z);

    glPushMatrix();//NEW
    glTranslatef(0.0f,0.0f, -6);
    glRotatef(20,0,0,1);
    glRotatef(20,1,0,0);
    glColor3f(1,0,0);
    glutSolidCube(2);
    glPopMatrix(); //NEW
    glFlush();//Write this out to the screen
#else
    glLoadIdentity();
    glClearColor(1, 1, .7, 0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glPushMatrix();
    
    //#define simpleCanned3DStuff
    glTranslatef(0.0f,0.0f, -6);
    glRotatef(20,0,0,1);
    glRotatef(20,1,0,0);
    glColor3f(1,0,0);
    glutSolidCube(2);
    
	glPopMatrix();
//    glLoadIdentity();
//    glClearColor(1, 1, .7, 0);
//	  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//    glPushMatrix();
//    
//    //#define simpleCanned3DStuff
//    glTranslatef(0.0f,0.0f, -6);
//    glRotatef(20,0,0,1);
//    glRotatef(20,1,0,0);
//    glColor3f(1,0,0);
//    glutSolidCube(2);
//    
//	glPopMatrix();
#endif
}
int simpleMain (int argc, char **argv) {
    glutInit(&argc,argv);           //Init glut passing some args, if you know C++ you should know we are just passing the args straight thru from main
    glutInitDisplayMode(GLUT_SINGLE|GLUT_RGB);  //Specify the Display Mode, this one means there is a single buffer and uses RGB to specify colors
    glutInitWindowSize(640, 480);           //Set the window size
    glutInitWindowPosition(100,100);        //Where do we want to place the window initially?
    glutCreateWindow("My First Window");//Name the window and create it
    glutDisplayFunc(myDisplay);//Set the callback function, will be called as needed
    myInit();//Initialize the window
    glutMainLoop();//Start the main loop running, nothing after this will execute for all eternity (right now)
    return 0;//Return from this function.
}
