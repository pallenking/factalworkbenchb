// GlutWindow.hpp -- generalized Window interface

#import "GeneralWindow.h"
#import "Garden.h"

@interface GlutWindow : GeneralWindow {
//class GlutWindow : public GeneralWindow
    int glutWinNo;
//	GLUT_Surface *glutWin;
}

- (int) flipBuffers;
- (int) close;
//- (int) drawBoxX :(int)x0 y :(int)y0 w :(int)dx h :(int) dy color: (int)color;
//- (int) drawBox (int x0, int y0, int dx, int dy, int colr);

//- putPointX :(int)x_ y :(int)y_ pattern :(int)patrn color :(int) colr;
//- void putPoint(int x_, int y_, int patrn, int colr);


void glutRun (Garden *activeGarden_);
void glutInz (int argc, char *argv[]);

