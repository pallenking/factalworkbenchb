// NsWindow.cpp -- generalized NextStep to General Window interface

#ifdef LATER            // XCode requires special frufra to link Ns
#include "NsWindow.hpp"
#include "Common.hpp"

//int testMain ();
//int testMain ()
//{
//	int x, y;
//	NsWindow *myWindow = new NsWindow(50, 50, 100, 100);
//
//	for (x=0; x<3; x++)
//		for (y=0; y<3; y++)
//			myWindow->drawBox(x*50, y*50, 49, 49, (int)random()%5);
//
//	for (x=0; x<5; x++)
//	{
//		generalWindowPoint pt = myWindow->mouseClick();
//		printf("mouse clicked at (%d, %d)\n", pt.x, pt.y);
//	}
//
////	myWindow->drawBox( 25,  25, 150, 100, red);
////	sleep (1);
////	myWindow->drawBox(175, 125, 150, 100, blue);
////	sleep (1);
////	sleep (5);
//
//	myWindow->close();
//	return 0;
//}

NsWindow :: NsWindow (int x, int y, int w, int h)
	: GeneralWindow (x, y, w, h)
{
	unsigned long xforeground, xbackground;
	XEvent xevent;
	nextColor = 0;

	/* connect to the X server */
	xdisplay = XOpenDisplay ("");

	if (xdisplay == NULL) {
		fprintf (stderr, "cannot connect to server\n");
		exit (EXIT_FAILURE);
	}

	/* get default screen */
	xscreen = DefaultScreen (xdisplay);

	/* get black and white representation on current screen */	  
	xbackground = BlackPixel (xdisplay, xscreen);
	xforeground = WhitePixel (xdisplay, xscreen);

	/* Create window at (100,50), width w, height h, border width
	   2, in default root  */

	windowWidth = w;
	windowHeight = h;

	xwindow = XCreateSimpleWindow (xdisplay,
		DefaultRootWindow(xdisplay), x, y, windowWidth, windowHeight, 6,
		xforeground, xbackground);

	if (!xwindow) {
		fprintf (stderr, "cannot open window\n");
		exit (EXIT_FAILURE);
	}

	/* ask for exposure IVal */
	XSelectInput(xdisplay, xwindow, ButtonPressMask | ExposureMask);
//	XSelectInput(xdisplay, xwindow, ExposureMask);

	/* pop this window up on the screen */
	XMapRaised (xdisplay, xwindow);

	/* wait for the window showing up before continuing */
	XNextEvent (xdisplay, &xevent);

	/* set graphics context of rectangle to red */
	xgc= XCreateGC (xdisplay, xwindow, 0, 0);
	if (DisplayPlanes (xdisplay, xscreen) != 1)
		cmap = DefaultColormap (xdisplay, xscreen);

	makeSomeColors();
}

int NsWindow :: width () 	{	return this? windowWidth: 0; }
int NsWindow :: height () 	{	return this? windowHeight: 0; }

void NsWindow :: makeSomeColors () {
	if (!this) return;
	makeColor((char *)"black");
	makeColor((char *)"red");
//	makeColor((char *)"orange");
	makeColor((char *)"yellow");
	makeColor((char *)"green");
	makeColor((char *)"blue");
	makeColor((char *)"white");
//	makeColor((char *)"gray");
//	makeColor((char *)"violet");
}

int NsWindow :: makeColor (char *colorName)
{
	if (!this) return -1;
	if (nextColor >= maxColors)
		return -1;
	XColor *cs = &colorScreen[nextColor];
	XColor *ce = &colorExact[nextColor];
	if (!XAllocNamedColor (xdisplay, cmap, colorName, cs, ce))
	{
		printf("color '%s' not defined\n", colorName);
		return -1;
	}
	return nextColor++;
}

int NsWindow :: drawBox (int x0, int y0, int dx, int dy, int colr) {
	if (!this)
		return -1;
//	if (colr<0 || colr>=nextColor)
//		return -1;

	XSetForeground (xdisplay, xgc, colr);
//	XSetForeground (xdisplay, xgc, colorScreen[colr].pixel);
	XFillRectangle (xdisplay, xwindow, xgc, x0, y0, dx, dy);
	XFlush (xdisplay);		// flush X request queue to server [MOVE OUT!!!]

	return 0;
}

generalWindowPoint NsWindow :: mouseClick () {
	generalWindowPoint rv = {-1, -1};
	if (!this) return rv;
	for (int done=0; !done; )
	{
        XEvent xevent;
		XNextEvent (xdisplay, &xevent);		/* wait for next IVal */
		switch (xevent.type) 
		{
			case Expose:
				return rv;
			case ButtonPress:
				rv = (generalWindowPoint){xevent.xbutton.x, xevent.xbutton.y};
				return rv;
		}
	}
	return rv;
}

NsWindow :: ~NsWindow () {
	if (!this) return;
	close();
}

int NsWindow :: close () {
	if (!this) return -1;
	XDestroyWindow(xdisplay, xwindow);
	XCloseDisplay (xdisplay);
	return 0;
}
#endif
