// NsWindow.hpp -- generalized X Window interface

#ifdef LATER            // XCode requires special frufra to link Ns
//#include <X11/Xlib.h>
//#include <SDL.h>
#import "GeneralWindow.hpp"

class NsWindow : public GeneralWindow
{
  public:
	NsWindow(int x, int y, int w, int h);
	~NsWindow();
	int width();
	int height();

	int makeColor(char *colorName);
	void makeSomeColors();
	int drawBox(int x0, int y0, int dx, int dy, int color);
	generalWindowPoint mouseClick();
	int close();

// global for now:
	int windowWidth, windowHeight;					
	Display *xdisplay;
	Window xwindow;
	int xscreen;
//	constant int maxColors = 10;
	#define maxColors 10
	int nextColor;
	XColor colorScreen[maxColors], colorExact[maxColors];
	GC xgc;
	Colormap cmap;
	
	// -------------------- SDL stuff (temporary home
//	SDL_Surface *textureWin;
//	SDL_Surface *typeWin;
//	SDL_Surface *mapWin;
	
	const static int numRgb = 12;
	int rgb[numRgb];
	int numFirstColor;
	
	void SDL_open();
//	inline int int2colorNum(int i)	{ 
//		return numFirstColor + i%(numRgb - numFirstColor);
//	}
//	inline int int2color(int i) 	{ return rgb[int2colorNum(i)]; }
	
};

#endif