// sxw.cpp -- simple X Window
// http://www.cs.uwindsor.ca/meta-index/courses/95F/60-212/labs/lab9/xwin.html
// see also http://www.tronche.com/cours/xlib-tutorial/2nd-program-anatomy.html

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "sxw.h"

int testMain2();
int testMain2 () {
	int x, y;
	sxw *mySxw = new sxw(100, 100);

	for (x=0; x<3; x++)
		for (y=0; y<3; y++)
			mySxw->drawBox(x*50, y*50, 49, 49, ((int)random())%5);

	for (x=0; x<5; x++)
	{
		sxwPoint pt = mySxw->mouseClick();
		printf("mouse clicked at (%d, %d)\n", pt.x, pt.y);
	}

//	mySxw->drawBox( 25,  25, 150, 100, red);
//	sleep (1);
//	mySxw->drawBox(175, 125, 150, 100, blue);
//	sleep (1);
//	sleep (5);

	mySxw->close();
	return 0;
}

sxw :: sxw (int w, int h) {
	unsigned long xforeground, xbackground;
	XEvent xevent;
	nextColor = 0;

	/* connect to the X server */
	xdisplay = XOpenDisplay ("");

	if (xdisplay == NULL) {
		fprintf (stderr, "cannot connect to server\n");
		exit (EXIT_FAILURE);
	}

	/* get default screen */
	xscreen = DefaultScreen (xdisplay);

	/* get black and white representation on current screen */	  
	xbackground = BlackPixel (xdisplay, xscreen);
	xforeground = WhitePixel (xdisplay, xscreen);

	/* Create window at (100,50), width w, height h, border width
	   2, in default root  */

	windowWidth = w;
	windowHeight = h;

	xwindow = XCreateSimpleWindow (xdisplay,
		DefaultRootWindow(xdisplay), 100, 50, windowWidth, windowHeight, 6,
		xforeground, xbackground);

	if (!xwindow) {
		fprintf (stderr, "cannot open window\n");
		exit (EXIT_FAILURE);
	}

	/* ask for exposure event */
	XSelectInput(xdisplay, xwindow, ButtonPressMask | ExposureMask);
//	XSelectInput(xdisplay, xwindow, ExposureMask);

	/* pop this window up on the screen */
	XMapRaised (xdisplay, xwindow);

	/* wait for the window showing up before continuing */
	XNextEvent (xdisplay, &xevent);

	/* set graphics context of rectangle to red */
	xgc= XCreateGC (xdisplay, xwindow, 0, 0);
	if (DisplayPlanes (xdisplay, xscreen) != 1)
		cmap = DefaultColormap (xdisplay, xscreen);

	makeSomeColors();
}

int sxw :: width () 	{	return windowWidth; }
int sxw :: height () 	{	return windowHeight; }

void sxw :: makeSomeColors ()
{
	makeColor((char *)"black");
	makeColor((char *)"red");
//	makeColor((char *)"orange");
	makeColor((char *)"yellow");
	makeColor((char *)"green");
	makeColor((char *)"blue");
	makeColor((char *)"white");
//	makeColor((char *)"gray");
//	makeColor((char *)"violet");
}

int sxw :: makeColor (char *colorName)
{
	if (nextColor >= maxColors)
		return -1;
	XColor *cs = &colorScreen[nextColor];
	XColor *ce = &colorExact[nextColor];
	if (!XAllocNamedColor (xdisplay, cmap, colorName, cs, ce))
	{
		printf("color '%s' not defined\n", colorName);
		return -1;
	}
	return nextColor++;
}

int sxw :: drawBox (int x0, int y0, int dx, int dy, int colr)
{
	if (colr<0 || colr>=nextColor)
		return -1;

	XSetForeground (xdisplay, xgc, colorScreen[colr].pixel);

	/* Draw rectangle at (25,25), width 300, height, 200 */
	XFillRectangle (xdisplay, xwindow, xgc, x0, y0, dx, dy);

	/* flush X request queue to server */
	XFlush (xdisplay);

	return 0;
}

sxwPoint sxw :: mouseClick ()
{
	sxwPoint rv = {-1, -1};
	for (int done=0; !done; )
	{
        XEvent xevent;
		XNextEvent (xdisplay, &xevent);		/* WAIT for next event */
		switch (xevent.type) 
		{
			case Expose:
				return rv;
			case ButtonPress:
				rv = (sxwPoint){xevent.xbutton.x, xevent.xbutton.y};
				return rv;
		}
	}
	return rv;
}

sxw :: ~sxw ()
{
	close();
}

int sxw :: close ()
{
	XDestroyWindow(xdisplay, xwindow);
	XCloseDisplay (xdisplay);
	return 0;
}
