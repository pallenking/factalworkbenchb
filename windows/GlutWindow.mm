// GlutWindow.mm -- generalized Window interface for Glut

#include "GlutWindow.hpp"
#include "Plant.h"
#include "Common.hpp"
#import <GLUT/glut.h>
#import "zpr.h"
extern GLfloat backgroundColor[];

void glutInz (int argc, char *argv[]) {
	glutInit(&argc, argv);    // Initialize Graphics Library (GL) 
}	

//Garden *activeGarden=0;		// for now: ONLY one garden is active, and only one plant in it.
void glutRun (Garden *activeGarden_) {
//	activeGarden = activeGarden_;
	glutMainLoop();
}

// Forward definitions for GLUT callbacks:
void reshape(int w, int h);
void idle(void);
void keyboard(unsigned char key, int x, int y);
void newFrame();

void reshape(int w, int h) {			// window resized
	glViewport(0, 0, (GLsizei) w, (GLsizei) h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if (w <= h) 
		glOrtho (-2.25, 2.25, -2.25*h/w, 2.25*h/w, -10.0, 10.0);
	else 
		glOrtho (-2.25*w/h, 2.25*w/h, -2.25, 2.25, -10.0, 10.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void idle(void) {
    glutPostRedisplay();	
}

// ==================================================================
// ====================== Instance Methods: =========================
// ==================================================================

@implementation GlutWindow

-initX :(float)x y :(float)y w :(float)w h :(float)h;
{   [super initX:x y:y w:w h:h];
//GlutWindow::GlutWindow (int x, int y, int w, int h)
//: GeneralWindow (x, y, w, h)

    glutInitDisplayMode (GLUT_SINGLE | GLUT_RGB | GLUT_ACCUM | GLUT_DEPTH);
	glutInitWindowSize (w, h);
	glutInitWindowPosition (x, y);
    static int wn=1;
    char buff[20]; sprintf(buff, "window%d", wn++);
	glutWinNo = glutCreateWindow (buff);     // identifies window
	
    // LIGHTING:			// http://www.sjbaker.org/steve/omniv/opengl_lighting.html
	glEnable(GL_LIGHTING);
	glClearColor(backgroundColor[0], backgroundColor[1], backgroundColor[2], backgroundColor[3]);
//	glClearColor(0.7, 0.7, 0.8, 0.0);		//;
    
    
    //// AMBIENT:
	GLfloat lm_ambient[] = { 0.1, 0.1, 0.1, 1.0 };		//{ 1, 1, 1, 1.0 } { 0.6, 0.6, 0.2, 1.0 } { 0, 0, 0, 1.0 }
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lm_ambient);
	
    //// SPOT:
	GLfloat light_position0[] = { 10.0, 0.0, 10.0, 1.0 };	// { 0.0, 0.0, 10.0, 1.0 }
	glLightfv(GL_LIGHT0, GL_POSITION, light_position0);
	glEnable(GL_LIGHT0);
//	GLfloat light_position1[] = { 10.0, 0.0,-10.0, 1.0 };
//  glLightfv(GL_LIGHT1, GL_POSITION, light_position1);
//	glEnable(GL_LIGHT1);
//	GLfloat light_position2[] = { 10.0,-0.0,-10.0, 1.0 };
//	glLightfv(GL_LIGHT2, GL_POSITION, light_position2);
//	glEnable(GL_LIGHT2);
//	GLfloat light_position3[] = { 10.0,-0.0, 10.0, 1.0 };
//	glLightfv(GL_LIGHT3, GL_POSITION, light_position3);
//	glEnable(GL_LIGHT3);

	GLfloat mat_ambient[] = { 0.5, 0.5, 0.5, 1.0 };		//{ 1.0, 1.0, 1.0, 1.0 }
	glMaterialfv(GL_FRONT, GL_AMBIENT, mat_ambient);
    
	GLfloat mat_specular[] = { 0.4, 0.4, 0.4, 1.0 };	//{ 1.0, 1.0, 1.0, 1.0 }
	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
    
	glMaterialf(GL_FRONT, GL_SHININESS, 5.0);          //50.0
    
	glEnable(GL_DEPTH_TEST);
	glShadeModel(GL_SMOOTH);
	glShadeModel(GL_FLAT);
	
	glutReshapeFunc(reshape);			// window reshaped
    glutIdleFunc(idle);
	glutDisplayFunc(newFrame);
	glutKeyboardFunc(keyboard);
	zprInit();
}

GlutWindow::~GlutWindow () {
}

int GlutWindow :: close () {
	if (!this) return 0;
    //	XDestroyWindow(xdisplay, xwindow);
    //	XCloseDisplay (xdisplay);
	return 0;
}

int GlutWindow :: drawBox (int x0, int y0, int dx, int dy, int colr) { 
    glutSetWindow(glutWinNo);

   // glClear(GL_COLOR_BUFFER_BIT);//Clear the screen
// HACK CODE    
    glColor3f(1,0,0);//Change the object color to red
    
    glBegin(GL_TRIANGLES);//Start drawing a triangle
    glVertex2f(3,-4);//draw our first coordinate
    glVertex2f(3.5,-3);//Our second coordinate
    glVertex2f(4,-4);//Our last coordinate
    glEnd();//Stop drawing triangles
    
//    glFlush();//Draw everything to the screen
//    glutPostRedisplay();//Start drawing again
	return 0; 
}

void GlutWindow :: putPoint(int x_, int y_, int patrn, int colr) {
    glutSetWindow(glutWinNo);
    static int patternPhase = 0;
    if (patrn & (1 << patternPhase++)) {
//        SDL_Rect rect = {x_, y_, 1, 1};
//        SDL_FillRect(sdlWin, &rect, colr);
    }
    patternPhase &= 0x1f;
}

int GlutWindow :: flipBuffers() {
	if (this) {
        glutSetWindow(glutWinNo);
	//	glPopMatrix();
        glutSwapBuffers();
        return 0;
    }
    return -1; 
}

@end
