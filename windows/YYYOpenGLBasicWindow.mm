// OpenGLBasicWindow.mm -- STANDALONE TEST

//www.codeproject.com/KB/openGL/OpenGLWindowWithGLUT/OpenGLBasicWindowSource.zip
// Ercan Polat: 01/28/2008
// Project Description: This project creates a simple OpenGl window using GLUT library 
// This code is from "OpenGL Programming Guide Third Edition" page 18 Example 1-2

#include <GLUT/glut.h>
#import "OpenGLBasicWindow.h"

void FOO_GLUTdisplay(void) {
	//Clear all pixels
	glClear(GL_COLOR_BUFFER_BIT);

	//draw white polygon (rectangle) with corners at
	// (0.25, 0.25, 0.0) and (0.75, 0.75, 0.0)
	glColor3f(1.0,1.0,1.0);
	glBegin(GL_POLYGON);
		glVertex3f(0.25, 0.25, 0.0);
		glVertex3f(0.75, 0.25, 0.0);
		glVertex3f(0.75, 0.75, 0.0);
		glVertex3f(0.25, 0.75, 0.0);
	glEnd();
	
	// Don't wait start processing buffered OpenGL routines)
	glFlush();
}

void FOO_GLUTinit(void)
{
	//select clearing (background) color
	glClearColor(0.0, 0.0, 0.0, 0.0);

	//initialize viewing values 
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, 1.0, 0.0, 1.0, -1.0, 1.0);

}

int FOO_GLUTmain(int argc, char** argv) {

    // omit if called from within [NSApp run]
//	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(250,250);
	glutInitWindowPosition(100,100);
	glutCreateWindow("Hello World");
	FOO_GLUTinit();
	glutDisplayFunc(FOO_GLUTdisplay);
	glutMainLoop();       // Never return

    printf("################# FOOmain returns #############\n");
	return 0;
}
