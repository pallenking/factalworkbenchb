// sxw.h -- simple X Window

#include <X11/Xlib.h>

typedef struct {	int x; int y; } sxwPoint;

class sxw
{
  public:
	sxw(int w, int h);			// constructor
	~sxw();						// destructor
	int width();
	int height();

	int makeColor(char *colorName);
	void makeSomeColors();
	int drawBox(int x0, int y0, int dx, int dy, int color);
	sxwPoint mouseClick();
	int close();

// global for now:
	int windowWidth, windowHeight;					
	Display *xdisplay;
	Window xwindow;
	int xscreen;
//	constant int maxColors = 10;
	#define maxColors 10
	int nextColor;
	XColor colorScreen[maxColors], colorExact[maxColors];
	GC xgc;
	Colormap cmap;
};
