// NextViewin.mm -- GeneralViewin subclass for NextStep windows

// http://www.cs.uwindsor.ca/meta-index/courses/95F/60-212/labs/lab9/xwin.html
// see also http://www.tronche.com/cours/xlib-tutorial/2nd-program-anatomy.html

#include "NextViewin.h"
#import <Cocoa/Cocoa.h>
#include "Common.hpp"

@implementation NextViewin

- initRect :(NSRect)frame_; {
//- initX :(float)x y :(float)y w :(float)w h :(float)h;
    [super initRect :frame_];
//    [super initX:x y:y w:w h:h];
    
//    NSRect frame = NSMakeRect(x,y, w,h);
	theNsWindow = [[NSWindow alloc] initWithContentRect :frame
        styleMask :NSTitledWindowMask | NSResizableWindowMask |  NSMiniaturizableWindowMask
        backing :NSBackingStoreBuffered defer :NO];
    [theNsWindow setTitle :@"foo"];

	myView = [[NextView alloc] init];

    panic("oops"); //: [myView setGeneralViewin :self];
	[theNsWindow setContentView :myView];
    
    [theNsWindow setBackgroundColor:[NSColor greenColor]];
	[theNsWindow setLevel :NSFloatingWindowLevel];

/**/[theNsWindow display];
	[theNsWindow makeKeyAndOrderFront :self];
    
	[self makeColorsPalette];
    return self;
}

- setDrawingMethod :(CALLBACK)method object :(void *)ob; {
    assert(myView, ("inz error"));
    [myView setDrawingMethod :method object:ob];
    return self;
}
- (void) makeColorsPalette;        {
    assert(myView, ("myView unitialized"));
    [myView makeColorsPalette];
}

- (int) drawBoxX:(int)x0 y:(int)y0 w:(int)dx h:(int) dy color:(int)colorNum; {
    if (self and myView)
        return [myView drawBoxX :x0 y:y0 w:dx h: dy color:colorNum];
    panic("somebody is NULL");
    return -1;
}
- (int) drawDottedRectX:(int)x0 y:(int)y0 w:(int)dx h:(int) dy color:(int)colorNum; {
    if (self and myView)
        return [myView drawDottedRectX :x0 y:y0 w:dx h: dy color:colorNum];
    panic("somebody is NULL");
    return 0;
}

- putPointX :(int)x_ y :(int)y_ pattern :(int)patrn color :(int) colr; {
    if (self and myView)
        return [myView putPointX :x_ y :y_ pattern :patrn color : colr];
    panic("somebody is NULL");
    return self;
}

- (BOOL)isFlipped;          {   return TRUE;                                }
- (int) displayViewin;		{   if (self)[theNsWindow display]; return 0;   }

-  (GeneralViewinPoint) mouseClick;                  {
//GeneralViewinPoint NextViewin :: mouseClick () {
	GeneralViewinPoint rv = {-1, -1};
	if (!self) return rv;
	for (int done=0; !done; )
	{
        panic("xxxxx");
#ifdef xxx
        XEvent xevent;
		XNextEvent (xdisplay, &xevent);		/* wait for next IVal */
		switch (xevent.type) 
		{
			case Expose:
				return rv;
			case ButtonPress:
				rv = (GeneralViewinPoint){xevent.xbutton.x, xevent.xbutton.y};
				return rv;
		}
#endif
	}
	return rv;
}

- printContents :(int)indent; {
	printf("NextViewin:");
	printf("theNsWindow=%lx myView=%lx", (long unsigned int)theNsWindow, (long unsigned int)myView);
	printf("\n%*s\\", indent*3, "");
	return [super printContents :indent];
}

@end

#ifdef outski
//
//    NSBezierPath* aPath = [NSBezierPath bezierPath];
//    [aPath moveToPoint:NSMakePoint(0.0, 0.0)];
//    [aPath lineToPoint:NSMakePoint(10.0, 10.0)];
//    [aPath curveToPoint:NSMakePoint(18.0, 21.0)
//          controlPoint1:NSMakePoint(6.0, 2.0)
//          controlPoint2:NSMakePoint(28.0, 10.0)];
//    [aPath appendBezierPathWithRect:NSMakeRect(2.0, 16.0, 8.0, 5.0)];

///    myView = [[NextView alloc] initWithFrame :frame];


//    NSRect frame = NSMakeRect(200, 200, 100,100);
theNsView = [[NextView alloc] initWithFrame :frame];

//s    theNsWindow  = [NSWindow alloc];
//    theNsWindow = [[NSWindow alloc] initWithContentRect:[[NSScreen mainScreen] frame]];

[theNsWindow initWithContentRect :frame
 //   [theNsWindow initWithContentRect: [[NSScreen mainScreen] frame]
 //        styleMask :NSBorderlessWindowMask backing :NSBackingStoreBuffered defer :NO];
                       styleMask :NSResizableWindowMask  backing :NSBackingStoreBuffered defer :NO];
[theNsWindow setContentView :theNsView];
[theNsWindow autorelease];

[theNsWindow setBackgroundColor :[NSColor blueColor]];
//    [theNsWindow makeKeyAndOrderFront :NSApp];
[theNsWindow makeKeyAndOrderFront :self];

//Don't forget to assign window to a strong/retaining property!
//Under ARC, not doing so will cause it to disappear immediately;


//    [self setWindow:mylWindow];
//    [mylWindow release];

//    NSView *localView = [[NSView alloc] init];

//    [localView release];

//    [window setLevel:NSFloatingWindowLevel];

//  NSLog(@"Finished Launching %@", notification);

//    NSRect frame = NSMakeRect(200,700, 400,300);
//	NSWindow *localWindow = [[NSWindow alloc] initWithContentRect :frame
//                             //	NSWindow *localWindow = [[NSWindow alloc] initWithContentRect :[[NSScreen mainScreen] frame]
//                                                        styleMask :NSResizableWindowMask
//                                                          backing :NSBackingStoreBuffered
//                                                            defer :NO];
////!!	[self setWindow :localWindow];
////	[localWindow release];
//
//	NSView *localView = [[NSView alloc] init];
//	[localWindow setContentView :localView];
//	[localView release];
//
//	[localWindow setLevel :NSFloatingWindowLevel];
//	[localWindow makeKeyAndOrderFront :self];


#endif
