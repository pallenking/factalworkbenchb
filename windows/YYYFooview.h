// FooView.h -- wabbafa C2011

#import <Cocoa/Cocoa.h>

@interface FooView : NSView

- (int) drawBoxX:(int)x0 y:(int)y0 w:(int)dx h:(int) dy color:(int)color;
- (void)drawRect:(NSRect)dirtyRect;

- (int) displayViewin;

@end
