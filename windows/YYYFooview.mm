// FooView.m -- wabbafa C2011

#import "FooView.h"
#import "common.hpp"

@implementation FooView

- (int) drawBoxX :(int)x0 y :(int)y0 w :(int)dx h :(int) dy color :(int)color; {
    NSRect box = NSMakeRect(x0, y0, dx, dy);
    [self drawRect :box];
    return 0;
}

- (void)drawRect:(NSRect)dirtyRect {
    
 	[[NSColor redColor] set];
	[NSBezierPath fillRect :NSMakeRect(5,8, 5, 8)];

    NSString *hello = @"Hell";
    NSMutableDictionary *font_attributes = [[NSMutableDictionary alloc] init];
    NSFont *font = [NSFont fontWithName:@"Futura-MediumItalic" size:42];
    [font_attributes setObject:font forKey:NSFontAttributeName];
    
    [hello drawAtPoint :NSMakePoint(15, 75) withAttributes :font_attributes]; // draws in currently focused view
    [font_attributes release];
}

- (int) displayViewin; {
    [self display];
    return 0;
}

@end
