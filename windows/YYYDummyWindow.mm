// DummyWindow.cpp 
//  Created by say nono on 21.07.11.
//  Copyright 2011 www.say-nono.com. All rights reserved.

#include "DummyWindow.h"
#import <GLUT/glut.h>

void DummyWindow::setup() {
	 int argc = 1;
	 char *argv = (char*)"openframeworks";
	 char **vptr = &argv;
	 glutInit(&argc, vptr);
	 glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
	 glutInitWindowSize(100, 100);
	 glutCreateWindow("GLEW Test");
	 glutDisplayFunc(display);
	 glutReshapeFunc(resize_cb);
}

void DummyWindow::display(void){
}

void DummyWindow::resize_cb(int w, int h) {
}	
