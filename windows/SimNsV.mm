//  SimNsV.mm -- An OpenGL 3D NsView  C2012

/*! toDo to_do to do parameters:
 */

#import "SystemTypes.h"
#import "SimNsV.h"
#import "SimNsVc.h"
#import "View.h"
#import "Common.h"
#import "NSCommon.h"
#import "vmath.hpp"
#import "Id+Info.h"
#import "GlCommon.h"
#import <GLUT/glut.h>
#import "XCodes.h"
#import "Brain.h"
#import "SimNsWc.h"
#import "RenderingContext.h"

///////////// https://developer.apple.com/devcenter/mac/resources/opengl/

	/// OpenGL 4.3 API Reference Card:
////////// http://www.khronos.org/files/opengl43-quick-reference-card.pdf

/////////// http://www.lighthouse3d.com/opengl/picking/								//picking
////////// http://www.opengl.org/archives/resources/faq/technical/selection.htm#sele0020    // selection

////////      totaly different way to pan and zoom
/////// http://developer.apple.com/library/mac/#documentation/graphicsimaging/conceptual/OpenGL-MacProgGuide/opengl_drawing/opengl_drawing.html#//apple_ref/doc/uid/TP40001987-CH404-SW8

 ///// OVERVIEW:
////// http://developer.apple.com/library/mac/#documentation/GraphicsImaging/Conceptual/OpenGL-MacProgGuide/opengl_intro/opengl_intro.html

//// http://lists.apple.com/archives/mac-opengl/2003/Feb/msg00069.html
/// http://www.arcsynthesis.org/gltut/
// http://www.opengl.org/discussion_boards/showthread.php/147680-What-is-the-correct-way-to-pan-zoom-and-rotate

@implementation SimNsV

#pragma mark  1. Basic Object Level					// Basic objects

- initWithFrame:(NSRect)frame; {

	NSOpenGLPixelFormatAttribute attribs[] =   {
		NSOpenGLPFAAccelerated,
		NSOpenGLPFANoRecovery,
		NSOpenGLPFADoubleBuffer,
		NSOpenGLPFADepthSize, 32,
		0 };
	NSOpenGLPixelFormat *format = [[[NSOpenGLPixelFormat alloc] initWithAttributes:attribs] autorelease];

	self = [super initWithFrame:frame pixelFormat:format]; // inz View superclass
	 //-----------------------------------------------------

	RenderingContext *rc	= [[[RenderingContext alloc] init] autorelease];
	self.renderingContext	= rc;
	rc.renderMode			= self.simNsWc.brain.render3DMode;

	self.wiggleView			= 0;
	self.wiggleOffset		= Vector3f(0,0,0);

	return self;
}

- (void) dealloc; {
	if (logDeallocEvents)
		NSPrintf(@"=================== Dealloc SimNsV %p ===\n", self);

	self.renderingContext	= nil;
	self.soughtView			= nil;
	self.wiggleView			= nil;

	[super dealloc];
}

#pragma mark  2. 1-level Access

- (NSString *) name;		{	return @"SimNsV???";	}

- (void) setCameraConfig:(NSDictionary *)camera; {

	 // Configure camera position:
//!	xzzy1 SimNsV		1. camera.poleHeight	:@float			-- of interest point on pole
//!	xzzy1 SimNsV		2. camera.poleSpin		:@float			-- around p pole (in degrees)
//!	xzzy1 SimNsV		3. camera.horizonUp	:@float			-- gaze height above horizon (in degrees)
//!	xzzy1 SimNsV		4. camera.zoom			:@float			-- magnification (normal 1.0)
	id s;
	self.cameraPoleHeight	= (s=camera[@"poleHeight"]	)?	[s floatValue]: 0.0;
	self.cameraPoleSpin		= (s=camera[@"poleSpin"]	)?	[s floatValue]: 0.0;
	self.cameraHorizonUp	= (s=camera[@"horizonUp"]	)?	[s floatValue]: 0.0;
	float invZoom			= (s=camera[@"zoom"]		)?	[s floatValue]: 1.0;
	self.cameraZoom			= 1.0 / invZoom;
	assert(!isNan(self.cameraZoom), (@""));

	[self updateCamera];
	self.interestPoint		= Vector3f(0,0,0);

	self.rotation			= 0.0;
	self.scale				= 1.0;
}

//Matrix4f getCameraMatrix() {		return self.camera;		}

- (SimNsWc *)	simNsWc {
	return self.simNsVc.simNsWc;		//
}

 /// Called when bounds changed
-(void) reshape {												[super reshape];

	self.viewWidth = self.bounds.size.width;
}

//                        3D MODEL SPACK
//
//        model                 v                     v
//         coords:              |                     |
//                        \ ∏ Tmodel i/      getModelViewMatrix()
//                         \  Matrix /          \ MODELVIEW /mvm
//        world    =========    v   =========    \  Matrix /   ==== ^ =====
//         coords:              |                     |             |
//                       \Tcamera.inverse/            |			simNsV.camera()
//                        \   Matrix    /             |             |
//        camera   ==========   v    ================ v =========== ^  ====
//         coords:              |
//                        \ PROJECTION /    getProjectionMatrix()
//                         \  Matrix  /pm      uses glOrtho()
//        clip     ==========   v    ==============================
//         coords:              |
//                        \ Perspective/         (not used)
//                         \ division /
//        device                v
//         coords:              |
//                         \ Viewport /          A=| 1 0 0 |
//                          \ Matrix /             | 0 1 0 |
//        window                v
//         coords:
//
//                            SCREEN

- (Bounds3f) calcVolumeBounds {
	/*! Concepts:
	 *	The camera is positioned in the world with a matrix "camera".
	 *	The camera looks at a bounding box, within which all will be displayed.
	 *	The bounding box is defined by a: left,right, bottom, top, zNear, zFar
	 *		(analogous to the Vector3f's "boundsMin" and "boundsMax" (used elsewhere))
	 *///The camera bounding box must contain all vertices of the transformed world bounding box
	Bounds3f volumeBounds(nanBounds3f);
	Matrix4f camera = self.camera;

	View *rV = self.simNsVc.rootFwV;
	Bounds3f worldBounds = rV.bounds + rV.positionTranslation;// Notify the view of the 3D bounds:

	for (int i=0; i<8; i++)  {		// scan all 8 corners of the world bounding box:
		Vector3f cornerI		= worldBounds.corner(i);
		Vector3f wbcInCamera	= camera * cornerI;
		volumeBounds			= volumeBounds | wbcInCamera;	// extend viewVolumeBounds to include this point
	}
	Vector3f siz = volumeBounds.size();
	if (siz.length()==0)					// If the projection has zero extent
		return zeroBounds3f;					// Don't draw anything
	volumeBounds.setSize(siz * 1.05);		// add 5% margins around edges

	 // correct for aspect ratios
	float  left=volumeBounds.left(), bottom=volumeBounds.bottom(), zNear=volumeBounds.zNear();
	float right=volumeBounds.right(),   top=volumeBounds.top(),     zFar=volumeBounds.zFar();

	float clipRatio		= (top - bottom)/(right - left);
	float windowRatio	= self.bounds.size.height/(float)self.bounds.size.width;
	float ratio			= clipRatio / windowRatio;

	float boundryExpand = 1.05;	//3; leave a boarder around displayable
	float ratioH		= boundryExpand * (ratio>1? ratio: 1.0);
	float ratioV		= boundryExpand / (ratio<1? ratio: 1.0);
	
	float ctr			= (right + left)/2.0;
	float delta			= (right - left)/2.0 * ratioH;
	left				= ctr - delta;
	right				= ctr + delta;

	ctr					= (top + bottom)/2.0;
	delta				= (top - bottom)/2.0 * ratioV;
	bottom				= ctr - delta;
	top					= ctr + delta;

	if (int i=1) {				// 141016: a hack just to get things to work
		zNear			= -100;			// quick hack guess
		zFar			=  100;
	}
	return Bounds3f(left,bottom,zNear, right,top,zFar);
}

 ///					Compute Camera Position from pole parameters
static float deg2rad = M_PI / 180.0;
- (void) updateCamera; {

		// Imagine you had:
	   //	A. a (GoPro) camera (camera) mouned on a boom (stick), pointing to
	  //	B. a point of interest.
	 // move from origin to our effective interest point B
	Vector3f bPoint = self.interestPoint + Vector3f(0, self.cameraPoleHeight, 0);
	Matrix4f origin2B = Matrix4f().createTranslation(bPoint);
	
	 // spin vertically, as if standing in one spot
	Matrix4f spinAboutIy = Matrix4f().createRotationAroundAxis(0, self.cameraPoleSpin*deg2rad, 0);

	 // point boom up or down
	Matrix4f bEndBUp = Matrix4f().createRotationAroundAxis(self.cameraHorizonUp*deg2rad, 0, 0);

//	if (0) {
//		 // 150514, 160211: This seems right, but doesn't work
//		Matrix4f camera = origin2B * spinAboutIy * bEndBUp;
//		float zoom = self.cameraZoom;
//		camera.at(0,0) *= zoom;
//		camera.at(1,1) *= zoom;
//		camera.at(2,2) *= zoom;
//		self.camera = camera;
//	}
//	else {
		 // This works, but it modifies camera.at(3,3) to non-1
		Matrix4f camera = origin2B * spinAboutIy * bEndBUp * self.cameraZoom;
		assert(!isNan(camera), (@""));
//		camera.at(3,3) = 1.0;					// This seems right, but doesn't work
		self.camera = camera;
//	}
	// self.scale
	
	//assert(camera.at(3,3)==1.0, (@"why?"));
	self.cameraInverse = self.camera.inverse();
	assert(!isNan(self.cameraInverse), (@""));

	glutPostRedisplay();	// marks the current window as needing redisplay
}

#pragma mark 11. 3D Display

  // called to establish drawing context
- (void) prepareOpenGL {
									// v-- TAKE OUT?
	glClearColor(colorBackground.d[0], colorBackground.d[1], colorBackground.d[2], colorBackground.d[3]);

	float e=-1;	// 1, 0
	GLfloat lmodel_ambient[] =  {e, e, e, 0};
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);

	glEnable(GL_LIGHTING);			// TAKE OUT?
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_NORMALIZE);

	typedef struct    {		int gl_light; float pos[3]; } lightX;
	lightX lights[] = {
		{GL_LIGHT0, { 1.0,1,1}},
		{GL_LIGHT1, { 1.0,0,0}},	// IGNORED, seems to only be 1 light!
	};

	for (int i=0; i<2; i++)  {
		lightX light = lights[i];
		glEnable(light.gl_light);
		float light_ambient[4]	= { 0.2, 0.2, 0.2, 1.0 };
		glLightfv(light.gl_light, GL_AMBIENT,	light_ambient);
	
		float light_diffuse[4]	= { 0.8, 0.3, 0.1, 1.0 };
		glLightfv(light.gl_light, GL_DIFFUSE,	light_diffuse);
	
		float light_specular[4]	= { 0.8, 0.3, 0.1, 1.0 };
		glLightfv(light.gl_light, GL_SPECULAR,	light_specular);
	
		glLightfv(light.gl_light, GL_POSITION,	light.pos);
	}
	[super prepareOpenGL];
}

 // for display or pic commands
- (void)renderInMode:(RenderModes)renderMode; {
	SimNsWc *simNsWc = self.simNsWc;
	Brain *brain = simNsWc.brain;
	self.renderingContext.renderMode = renderMode;

	glPushMatrix();
		static uColors3 pickBackground =   {1,1,1};	// white background
		self.renderingContext.renderMode = renderMode;

		 // Light HnwObject to use:
		if (renderMode==render3Dlighting)	// realistic 3D lighting
			glEnable(GL_LIGHTING);
		else								// cartoon colors, and pic and tags
			glDisable(GL_LIGHTING);

		 // Colors for picking:
		if (renderMode==renderColorTags or renderMode==renderFindTagObject) {
			glDisable(GL_DITHER);
			glClearColor(pickBackground.r, pickBackground.g, pickBackground.b, 1);
		}
		else {
			glEnable(GL_DITHER);
			glClearColor(colorBackground.d[0], colorBackground.d[1], colorBackground.d[2], colorBackground.d[3]);
		}
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		 // Manage my drawing con
		[self.renderingContext reset];


		  // Render the View:
		 //
		if (brain.displayAxis) {		// Draw the axis
			glPushMatrix();
				if (0)
					glTranslatefv(self.interestPoint);
				self.renderingContext.color = colorGrey6; //colorRed;
				id labels = @[@"+X",@"-X", @"+Y",@"-Y", @"+Z",@"-Z"];
				float ticSpacing = 10.0, length = 100.0;
				for (int axisIndex=0; axisIndex<6; axisIndex++)
					drawAxis(self.renderingContext, length, labels, ticSpacing, axisIndex);
			glPopMatrix();
		}

		if (brain.someViewDirty or brain.boundsDirty) {
XX			[simNsWc.simNsVc buildRootFwVforBrain:brain];		// handle if view
			[simNsWc buildAutoOpenInspectors];
		}
		assert(simNsWc.simNsVc.rootFwV and !brain.someViewDirty and !brain.boundsDirty,
					(@"improper rootFwV=%lx", (LUI)simNsWc.simNsVc.rootFwV));


XX		[simNsWc.simNsVc.rootFwV renderWith:self.renderingContext];			//assertX(rV, (@"brain: could not build view from part"));


		 // A HACK OF LITTLE VALUE:
		if (0) {
			 // A rectangular solid of small cubes (to test the clips)
			self.renderingContext.color = colorBlue4;
			float x0=-10, xd=2, x1=10;
			float y0=-10, yd=2, y1=10;
			float z0=-10, zd=2, z1=10;
			float a = xd/4;

			for (int x=x0; x<x1; x+=xd)
				for (int y=y0; y<y1; y+=yd)
					for (int z=z0; z<z1; z+=zd) {
						glPushMatrix();
							float delX = randomDist(-a,a);
							float delY = randomDist(-a,a);
							float delZ = randomDist(-a,a);
							float d = delX+delY+delZ;
		//					if (d > 0)					// play-toy
							  {
								glTranslatef(x+delX, y+delY, z+delZ);
								glutSolidCube(0.25);
								glPopMatrix();
							}
						glPopMatrix();
				}
		}
		assert([self.renderingContext.drawstack count]==0, (@"renderingContext not empty after serialize"));
	
	glPopMatrix();
}


 // called to redraw the window
- (void) drawRect: (NSRect) bounds; {
		// (Bounds ignored here)


	 // 1. specify the bounds of the known world:
	glMatrixMode(GL_PROJECTION);//==========================================
	self.volumeBounds = self.calcVolumeBounds;
	Bounds3f vb = self.volumeBounds;
	if (vb.size().length() == 0) {		// if result has zero extent
		[@"volumeBounds has zero extent" ppLog];
		return;
	}
//	assert(vb.size().length(),(@""));	// if result has zero extent

	glLoadIdentity();
	glOrtho(vb.left(),vb.right(), vb.bottom(),vb.top(), vb.zNear(),vb.zFar()); //  gluPerspective (50.0*zoomFactor, (float)width/(float)height, zNear, zFar);
//	glFrustum(<#GLdouble left#>, <#GLdouble right#>, <#GLdouble bottom#>, <#GLdouble top#>, <#GLdouble zNear#>, <#GLdouble zFar#>)
//	gluPerspective(<#GLdouble fovy#>, <#GLdouble aspect#>, <#GLdouble zNear#>, <#GLdouble zFar#>)



	glMatrixMode(GL_MODELVIEW); //==========================================

	 // 2. Initialize GL_MODELVIEW stack before you start drawing polygons.
	glLoadIdentity();

	 // for testing, look down at the top of the pole
	//glMultMatrixf(Matrix4f().createRotationAroundAxis(-M_PI_2, 0, 0));

	  /// 3. CAMERA:  the inverse of the camera's position transform
	 ///
	glMultMatrixf(self.cameraInverse);		/// 2. The world, seen from the camera.


	 // The delegate draws the important stuff
	Brain *brain = self.simNsWc.brain;

XX	[self renderInMode:brain.render3DMode];	// invokes OpenGL Brain for window


	[[self openGLContext] flushBuffer];
//	[super drawRect: bounds];
}



#pragma mark 12. Inspectors modelPic
static int debugPrint34 = 1;		// debug
#define dNSPrintf if (debugPrint34) NSPrintf


		  //////// Find the Part (or HnwObject) under the mouse:
		 /////                   /////
		/////     PICK     /////
	   /////                   /////
///////////////////////////////////

- (View *) modelPic:(FwEvent &)fwEvent; {

	 // RENDER with uniqe color TAGs for each object, instead of optical colors
XX	[self renderInMode:renderColorTags];
	Vector3f screenPosition = fwEvent.mousePosition;
	unsigned char ubPixel[3] = {0x55, 0x55, 0x55};	// :H: Unsigned Byte style PIXEL

	  /// Phase 1: Retrieve the TAG under the mouse:
	 ///
	glGetError();				// Might be needed to clear errors
	glReadPixels(screenPosition.x, screenPosition.y, 1, 1, GL_RGB, GL_UNSIGNED_BYTE, ubPixel);
	GLenum gle				= glGetError(); // Get errors
	assert(gle == GL_NO_ERROR, (@"glReadPixels: Error code:%x", gle));

	uColors3 pixelUnderMouse = {ubPixel[0], ubPixel[1], ubPixel[2]};
	dNSPrintf(@"===SimNsWc== pic at (%.1f, %.1f) ", screenPosition.x,screenPosition.y);
	dNSPrintf(@"finds color %@", colorString(colorInt(pixelUnderMouse)));
	if (ubPixel[0]==0x55 && ubPixel[1]==0x55 && ubPixel[2]==0x55)
		dNSPrintf(@"\t(glReadPixels read no data (0 bytes)!)\n");

	 /// Phase 2: RENDER with same, retrieve the object with a matching the tag
	///
	SimNsWc *simNsWc 		= self.simNsWc;
	RenderingContext *rc 	= self.renderingContext;
	rc.soughtTag			= colorInt(pixelUnderMouse);
	rc.soughtView 			= nil;
	rc.soughtViewsCenter = nanVector3f;

XX	[self renderInMode:renderFindTagObject];

	 /// Phase 3: Special Cases:
	if ( rc.soughtTag == 0xffffff	// Hit Background
	  or rc.soughtView == nil )		// Hit nothing
		 rc.soughtView = simNsWc.simNsVc.rootFwV; // Use root View
	mustBe(View, rc.soughtView);	// finally, a good object

	dNSPrintf(@" of %@(%@)\n", rc.soughtView.name, rc.soughtView.part.fullName);
	if (![rc.soughtView isKindOfClass:[View class]]) {
		panic(@"Pick Error, color %s not a View", colorString(rc.soughtTag).UTF8String);		// ignore it.
		return nil;
	}
	return rc.soughtView;
}

#pragma mark 13. IBActions	(Keys, Mouse)

/////////////////////////////////////////////////////////////////////
///
///			K  K EEEE Y   Y  SSS		M   M  OO  U  U  SSS EEEE
///			K K  E     Y Y  S			MM MM O  O U  U S    E
///			KK   EEE    Y    SS		M M M O  O U  U  SS  EEE
///			K K  E      Y      S		M   M O  O U  U    S E
///			K  K EEEE   Y   SSS		M   M  OO   UU  SSS  EEEE
///
////////////////////////////////////////////////////////////////

 //////////// Attach the mouse/keyboard Events ////////////
- (BOOL) acceptsFirstResponder         {    return YES;    }
- (BOOL) becomeFirstResponder          {    return YES;    }
- (BOOL) resignFirstResponder          {    return YES;    }
- (bool) applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)theApplication
									   {    return YES;    }

 // ?2BAK: there is a better way to do this, passing it up the first responder chain
bool printNsEvents = 1;						// debug
bool awaitingKeyUp = 0;
//const unsigned char upKey = 253;			// some random unused value
//const unsigned long int FWKeyUpModifier = 1;

- (void) keyDown:(NSEvent *)theEvent; {
	assert(theEvent.type==NSKeyDown, (@"Sanity check"));

	 // filter out autorepeat key events
	if (awaitingKeyUp) {
		//assert(theEvent.ARepeat, (@"this works too!"));
		return;
	}
	awaitingKeyUp = 1;

	if (printNsEvents)
		printf("-------- keyDown\n");

	assert(theEvent.characters.length==1, (@"1 key at a time"));
	char character				= [theEvent.characters characterAtIndex:0];
	unsigned long modifier		= [theEvent modifierFlags];
	modifier &= 0xffffffff;	// 160715 for some reason some high 32 bits are set
	/* NSAlphaShiftKeyMask = 1 << 16,
	 NSShiftKeyMask      = 1 << 17,
	 NSControlKeyMask    = 1 << 18,
	 NSAlternateKeyMask  = 1 << 19,
	 NSCommandKeyMask    = 1 << 20,
	 NSNumericPadKeyMask = 1 << 21,
	 NSHelpKeyMask       = 1 << 22,
	 NSFunctionKeyMask   = 1 << 23,
	 */
	
XX	[self.simNsWc processKey:character modifier:modifier];	// processed by WC

}

- (void) keyUp:(NSEvent *)theEvent; {
	assert(theEvent.type==NSKeyUp, (@"Sanity check"));
	awaitingKeyUp = 0;				// filter out autorepeat key events

	if (printNsEvents)
		printf("-------- keyUp\n");

	assert(theEvent.characters.length==1, (@"1 key at a time"));
	char character = [theEvent.characters characterAtIndex:0];
	unsigned long modifier = [theEvent modifierFlags] | FWKeyUpModifier;

	[self.simNsWc processKey:character modifier:modifier];// delegate may process
												// we do not process any upKeys
}

//- (bool) processKey:(char)character modifier:(unsigned long)modifier; {
//	bool cmd = modifier & NSCommandKeyMask;
//	if (character == 'r' && cmd)				//// cmd r ////
//		panic(@"Press 'cmd r' again to rerun");		// break to debugger
//
//	else if (character == 'o') {				//// o ////
//		printf("\nOpenGl HnwObject View matrix:\n");
//		ogl();										// print OGL view matrix
//	}
//	else if (character == '?') {				//// ? ////
//		printf( "   === SimNsV      commands:\n"
//				"\t'o'		-- print OpenGl HnwObject View matrix		\n"
//				"\t'r'		-- toggel rotate mode						\n"
//				);
//		return false;
//	}
//	else
//		return false;
//	return true;
//}

 //  ====== LEFT MOUSE ======
- (void) mouseDown:(NSEvent *)theEvent {
	if (printNsEvents)
		printf("-------- left mouseDown (click %d)\n", (int)[theEvent clickCount]);

	 // N.B: make this dummy, to initialize previous values
	FwEvent fwEvent = [self makeFwEvent:theEvent];

	if (fwEvent.clicks == 1)
		self.soughtView = 0;		// First click forces Lazy Pic below

	[self retireFwEvent:fwEvent];

	self.mouseWasDragged = 0;
}

- (void) mouseDragged:(NSEvent *)theEvent; {
	FwEvent fwEvent = [self makeFwEvent:theEvent];

		 ///
	if (fwEvent.clicks == 1) {

		  /// WIGGLE = ALT, 1 click, drag
		 ///
		if (bool altKey = fwEvent.modifierFlags & NSAlternateKeyMask) {
			if (self.soughtView == 0)	// Lazy Pic: Only pic when needed
				self.soughtView = [self modelPic:(FwEvent &)fwEvent];
			self.wiggleView = self.soughtView;

			 // jog the selected element, in y only for now
			self.wiggleOffset += fwEvent.deltaPosition;	// AD HOC 3D<-2D

			self.soughtView.part.brain.boundsDirty = true;// FW View
		}
		else	/// Pan Zoom and Rotate
			[self panZoomRotate:fwEvent];
	}
	else if (fwEvent.clicks == 2) {

		if (self.soughtView == 0)	// Lazy Pic: Only pic when needed
			self.soughtView = [self modelPic:(FwEvent &)fwEvent];

		[self.simNsWc modelDispatch:fwEvent to:self.soughtView];
		if (printNsEvents)
			printf("-------- mouseDragged (click 2)\n");
	}
	else
		panic(@"-------- mouseDragged (click >3)");

	[self retireFwEvent:fwEvent];
	self.mouseWasDragged = 1;	// drag cancels pic
}

- (void) mouseUp:(NSEvent *)theEvent; {
	if (printNsEvents)
		printf("-------- mouseUp (click %ld)\n", [theEvent clickCount]);
	FwEvent fwEvent = [self makeFwEvent:theEvent];

	if (self.mouseWasDragged==0) {	// undragged up
		if (self.soughtView == 0)	// Lazy Pic: Only pic when needed
			self.soughtView = [self modelPic:(FwEvent &)fwEvent];

XX		[self.simNsWc modelDispatch:fwEvent to:self.soughtView];
	}

	if (self.wiggleView) {
		self.wiggleView					= nil; // stop any jiggling
		self.wiggleOffset = zeroVector3f;
		self.soughtView.part.brain.boundsDirty = true;// FW View
	}
	[self retireFwEvent:fwEvent];
}

 //  ====== RIGHT MOUSE ======
- (void) rightMouseDown:(NSEvent *)theEvent; {
	if (printNsEvents)
		printf("-------- rightMouseDown\n");
	FwEvent fwEvent = [self makeFwEvent:theEvent];
		; // nada
	[self retireFwEvent:fwEvent];
}

- (void) rightMouseDragged:(NSEvent *)theEvent; {
	if (printNsEvents)
		printf("-2-4---- rightMouseDragged\n");
	FwEvent fwEvent = [self makeFwEvent:theEvent];

	[self cameraSuperior2interest:fwEvent];

	[self retireFwEvent:fwEvent];
}

 //  ====== CENTER MOUSE ======
- (void) otherMouseDown:(NSEvent *)theEvent; {
	if (printNsEvents)
		printf("-------- rightMouseDragged\n");
	FwEvent fwEvent = [self makeFwEvent:theEvent];

	; // nada

	[self retireFwEvent:fwEvent];
}
- (void) otherMouseDragged:(NSEvent *)theEvent; {
	if (printNsEvents)
		printf("-------- rightMouseDragged\n");
	FwEvent fwEvent = [self makeFwEvent:theEvent];

		; // nada

	[self retireFwEvent:fwEvent];
}

 //  ====== SCROLL WHEEL ======
- (void) scrollWheel:(NSEvent *)theEvent; {
	if (printNsEvents)
		printf("--34---- scrollWheel  deltaY=%f\n", theEvent.deltaY);
	FwEvent fwEvent = [self makeFwEvent:theEvent];

	[self cameraNearer:fwEvent];
	
	[self retireFwEvent:fwEvent];
}

- (void) magnifyWithEvent:(NSEvent *)event; {
		float m = [event magnification];
		self.scale += m;
		printf("-------- magnifyWithEvent \t\t\t\tm=%f ==*==> scale=%f\n", m, self.scale);
}
- (void) rotateWithEvent:(NSEvent *)event; {
		float f = [event rotation];
		self.rotation += f;
		printf("-------- rotateWithEvent %f ==+==> %f\n", f, self.rotation);
}
- (void)swipeWithEvent:(NSEvent *)event; {
		printf("-------- swipeWithEvent\n");
}

/*
rotateWithEvent:
swipeWithEvent:
*/

#pragma mark 13.5 Pan Zoom Rotate
   //////////////////////////////////////////////////////////
  ////////////////// ZOOM, PAN, ROTATE /////////////////////
 //////////////////////////////////////////////////////////

- (FwEvent) makeFwEvent:(NSEvent *)nsEvent {
	 // build an FwEvent from nsEvent:
	FwEvent rv = {sim_nop};
	rv.nsType = [nsEvent type];
	rv.buttonNumber = [nsEvent buttonNumber];
	rv.modifierFlags = [nsEvent modifierFlags];

	if (nsEvent.type == NSScrollWheel)
		rv.scrollWheelDelta = nsEvent.deltaY;
	else {
		rv.clicks = (int)[nsEvent clickCount];

		NSPoint loc = [self convertPoint:[nsEvent locationInWindow] fromView:nil];
		Vector3f mousePosition = Vector3f(loc.x, loc.y, 0);		// "-"?
		
		rv.deltaPosition = mousePosition - self.lastMousePosition;	// how far since last
		rv.mousePosition = mousePosition;
		rv.deltaPercent  = rv.deltaPosition/self.viewWidth;
		self.lastMousePosition = mousePosition;
	}
	return rv;
}

//////////////////////// SUPPORT //////////////////////////

- (void) retireFwEvent:(FwEvent &)fwEvent; {
	[self updateCamera];
}

- (void) panZoomRotate:(FwEvent &)fwEvent; {
		if (mouse3Button) {
			if (printNsEvents)
				printf("-234---- mouseDragged (click 1) (mouse3)\n");
			[self cameraMoveAroundPole:fwEvent];
		}
		else if (mouseMacTouchpad) {
			if ((fwEvent.modifierFlags&NSControlKeyMask) != 0) {
				if (printNsEvents)
					printf("-2------ mouseDragged (click 1) (touchpad) control key\n");
				[self cameraSuperior2interest:fwEvent];
			}
			else {
				if (printNsEvents)
					printf("-234---- mouseDragged (click 1) (touchpad) ...\n");
				[self cameraMoveAroundPole:fwEvent];
			}
		}
		else
			panic(@"");
}

- (void) cameraMoveAroundPole:(FwEvent &)fwEvent; {

	float distX				= fwEvent.deltaPercent.x;
	float distY				= fwEvent.deltaPosition.y;
	float dx				= distX / deg2rad * 4; /*fudge*/
	float dy				= distY * self.cameraZoom/10.0;
	self.cameraPoleSpin		+= dx;
	self.cameraPoleHeight	-= dy;
	printf("========= distX:%f, dx:%f, spin:%f", distX, dx, self.cameraPoleSpin);
	assert(!isNan(self.cameraPoleHeight), (@""));
}

- (void) cameraNearer:(FwEvent &)fwEvent; {		// does not work well

	float d					= fwEvent.scrollWheelDelta;
	float delta				= d>0? 0.95: d==0? 1.0: 1.05;
	// N.B.: "feature": this implementation quantizes zoom sizes
	self.cameraZoom			*= delta;
}

- (void) cameraSuperior2interest:(FwEvent &)fwEvent; {	// does not work well

	self.cameraHorizonUp	-= fwEvent.deltaPercent.y / deg2rad;
//	self.cameraHorizonUp	-= fwEvent.deltaPercent.y / (50.0 * deg2rad);
//	self.cameraZoom			*= fwEvent.deltaPercent.x > 0? 0.95: 1.05;	//140620 broken
}

//////////////////////// //////////////////////////
- (void) printDisplayState:(NSString *)prefix; {
	Matrix4f m4f = getModelViewMatrix();
	NSPrintf(@"%@ camera=%@: camera-1=%@ mv=%@\n", prefix, pp(self.camera),
			pp(self.cameraInverse), pp(m4f));
}

//- (void) pD3v; {
//	[self printDisplayState:@""];
//
//	// test the guiVector3fAccessors below are actually there:
//	float f = [self interestPointx];
//	[self setInterestPointx: f];
//}

- (IBAction)quantize:(NSMenuItem *)sender; {
	panic(@"");
}

- (void) seeMeHere; { }

guiMatrix4fAccessors4(camera,				Camera,			_camera)
/* 170407 generates manually
- (float) camera00 { return _camera.at(0,0); } - (void) setCamera00:(float)val { _camera.at(0,0) = val; }
- (float) camera01 { return _camera.at(0,1); } - (void) setCamera01:(float)val { _camera.at(0,1) = val; }
- (float) camera02 { return _camera.at(0,2); } - (void) setCamera02:(float)val { _camera.at(0,2) = val; }
- (float) camera03 { return _camera.at(0,3); } - (void) setCamera03:(float)val { _camera.at(0,3) = val; }
- (float) camera10 { return _camera.at(1,0); } - (void) setCamera10:(float)val { _camera.at(1,0) = val; }
- (float) camera11 { return _camera.at(1,1); } - (void) setCamera11:(float)val { _camera.at(1,1) = val; }
- (float) camera12 { return _camera.at(1,2); } - (void) setCamera12:(float)val { _camera.at(1,2) = val; }
- (float) camera13 { return _camera.at(1,3); } - (void) setCamera13:(float)val { _camera.at(1,3) = val; }
- (float) camera20 { return _camera.at(2,0); } - (void) setCamera20:(float)val { _camera.at(2,0) = val; }
- (float) camera21 { return _camera.at(2,1); } - (void) setCamera21:(float)val { _camera.at(2,1) = val; }
- (float) camera22 { return _camera.at(2,2); } - (void) setCamera22:(float)val { _camera.at(2,2) = val; }
- (float) camera23 { return _camera.at(2,3); } - (void) setCamera23:(float)val { _camera.at(2,3) = val; }
- (float) camera30 { return _camera.at(3,0); } - (void) setCamera30:(float)val { _camera.at(3,0) = val; }
- (float) camera31 { return _camera.at(3,1); } - (void) setCamera31:(float)val { _camera.at(3,1) = val; }
- (float) camera32 { return _camera.at(3,2); } - (void) setCamera32:(float)val { _camera.at(3,2) = val; }
- (float) camera33 { return _camera.at(3,3); } - (void) setCamera33:(float)val { _camera.at(3,3) = val; }
 */
guiVector3fAccessors4(interestPoint,	InterestPoint,	_interestPoint)
@end



