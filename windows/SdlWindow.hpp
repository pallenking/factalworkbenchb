// SdlWindow.hpp -- generalized Window interface

#import "GeneralWindow.h"
//#include <SDL.h>
//#include <SDL/SDL.h>

void SDL_OPEN_SYS();

class SdlWindow : public GeneralWindow
{
  public:
	SdlWindow(int x, int y, int w, int h);
	~SdlWindow();
	int flipBuffers();
	virtual int close();
	virtual int drawBox (int x0, int y0, int dx, int dy, int colr);
    void putPoint(int x_, int y_, int patrn, int colr);

	SDL_Surface *sdlWin;
};
