// glut_example.h
// Stanford University, CS248, Fall 2000

void DrawCubeFace(float fSize);
void DrawCubeWithTextureCoords (float fSize);
void RenderObjects(void);
void display(void);
void reshapeAA(GLint width, GLint height);
void InitGraphics(void);
void MouseButton(int button, int state, int x, int y);
void MouseMotion(int x, int y);
void AnimateScene(void);
void SelectFromMenu(int idCommand);
void Keyboard(unsigned char key, int x, int y);
int BuildPopupMenu (void);
int GlutVewinMain(int argc, char** argv);
