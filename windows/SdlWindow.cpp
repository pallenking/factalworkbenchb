// SdlWindow.cpp -- generalized Window interface for SDL

#include "SdlWindow.hpp"
#include "NextPool.h"
#include "Common.hpp"

static int sdlWindowCount = 0;
void SDL_OPEN_SYS() {
    ;
}

SdlWindow::SdlWindow (int x, int y, int w, int h) 
		: GeneralWindow (x, y, w, h)
{  
	// make windows:
	assert(sdlWindowCount++ == 0, ("only one SDL window supported"));
	
	 // 100515 -- __NSAutoreleaseNoPool(): NSCFString autoreleased with no pool in place - just leaking
	 // http://developer.apple.com/mac/library/documentation/Cocoa/Reference/Foundation/Classes/NSAutoreleasePool_Class/Reference/Reference.html
	void *pool = openAutoreleasePool();				// BUG: teach SDL about NS pools
	createSharedNSApp();
	
	const SDL_version* v = SDL_Linked_Version();
	printf("SDL Version: %u.%u.%u\n", v->major, v->minor, v->patch);

	int rv = SDL_Init(SDL_INIT_VIDEO);
	//	printf("SDL_Init returned\n");
	
	assert(rv >= 0,  ("Could not initialize SDL: %s\n", SDL_GetError()));
	atexit(SDL_Quit);						// Register shutdown program
	
	char origin[100]; sprintf(origin, "SDL_VIDEO_WINDOW_POS=%d,%d", x, y);
	SDL_putenv (origin);
	sdlWin =SDL_SetVideoMode(w, h, 32, SDL_SWSURFACE | SDL_ANYFORMAT);
	
	closeAutoreleasePool(pool);
}

SdlWindow::~SdlWindow () {
}

int SdlWindow :: close () {
	if (!this) return 0;
//	XDestroyWindow(xdisplay, xwindow);
//	XCloseDisplay (xdisplay);
	return 0;
}

int SdlWindow :: drawBox (int x0, int y0, int dx, int dy, int colr) { 
	SDL_Rect rect1 = {x0, y0, dx, dy};		
	SDL_FillRect(sdlWin, &rect1, colr);
	return 0; 
}

void SdlWindow :: putPoint(int x_, int y_, int patrn, int colr) {
    static int patternPhase = 0;
    if (patrn & (1 << patternPhase++)) {
        SDL_Rect rect = {x_, y_, 1, 1};
        SDL_FillRect(sdlWin, &rect, colr);
    }
    patternPhase &= 0x1f;
}

int SdlWindow :: flipBuffers() {
	if (!this) return 0;
	SDL_Flip(sdlWin);		// display info
	return -1; 
}

#	ifdef xxx
GeneralWindow *gWind = new SdlWindow(100, 200, 300,400);  // TEST CODE
for (int i=0; i<5; i++)
{
	generalWindowPoint pt = gWind->mouseClick();
	printf("mouse clicked at (%d, %d)\n", pt.x, pt.y);
}
#	endif
