//  SimNsV.h -- An OpenGL 3D NsView  C2012

//		camera position around pole
//      camera transformation, bounds
//      OpenGL preparation
//		keyboard
//		mouse PZR
//      Build Views for Model
//		world bounds


#import "RenderingContext.h"
#import <Cocoa/Cocoa.h>
@class SimNsWc, SimNsVc, Brain;

//static char mouse3Button=0, mouseMacTouchpad=1; 	// MAC TOUCHPAD
static char mouse3Button=1, mouseMacTouchpad=0; 	// 3-button MOUSE

 /// 4 Stages of the OpenGL Pipeline, from screen to 3D model:
@interface SimNsV : NSOpenGLView

	@property (nonatomic, assignWeak) SimNsVc *simNsVc;

    @property (nonatomic, retain) RenderingContext *renderingContext;
								/// THIS SHOULD BE OPTIMIZED OUT!!!
     /// 2a. Mouse State
    @property (nonatomic, assign) int		mouseWasDragged;// to differentiate pic from pan/zoom
	@property (nonatomic, assign) Vector3f	lastMousePosition;
    @property (nonatomic, retain) View      *soughtView;    // of a mouse operation

     /// 2. Clip Planes: 2.5D (3D with Z into screen)
    @property (nonatomic, assign) Bounds3f	volumeBounds;	// In Z-Away coord system
	@property (nonatomic, assign) float		viewWidth;

     /// 3. Camera master state:
	 // Cylindrical coordinates
    @property (nonatomic, assign) float		cameraPoleHeight;// ** z
    @property (nonatomic, assign) float		cameraPoleSpin;	// ** spin around pole
    @property (nonatomic, assign) float		cameraHorizonUp;// ** above horizon
    @property (nonatomic, assign) float		cameraZoom;		//

	//					 b) as a single Camera transformation
    @property (nonatomic, assign) Matrix4f	camera;
    @property (nonatomic, assign) Matrix4f	cameraInverse;

    @property (nonatomic, assign) Vector3f	interestPoint;

     /// 4. Part Bounds:
//  @property (nonatomic, assign) Bounds3f	worldBounds;    // from main View

	// embryonic touchpad gestures
    @property (nonatomic, assign) float		scale;
    @property (nonatomic, assign) float		rotation;

    @property (nonatomic, retain) View      *wiggleView;    // being wiggled now
	@property (nonatomic, assign) Vector3f	wiggleOffset;


- (id)			initWithFrame:(NSRect)frame;
//- (id)		initWithFrame:(NSRect)frame camera:camera;

- (void) 		setCameraConfig:(NSDictionary *)camera;
- (SimNsWc *)	simNsWc;

- (void)        drawRect: (NSRect) bounds;
- (void)		renderInMode:(RenderModes)renderMode;
- (View *)		modelPic:(FwEvent &)fwEvent;

- (FwEvent)     makeFwEvent:(NSEvent *)nsType;


- (void)        retireFwEvent:(FwEvent &)fwEvent;
- (void)        updateCamera;



//- (void)      lookAtInterestPoint;

  ////////////////// ZOOM, PAN, ROTATE /////////////////////
- (void)        cameraMoveAroundPole:(FwEvent &)fwEvent;
- (void)        cameraSuperior2interest:(FwEvent &)fwEvent;


@end
