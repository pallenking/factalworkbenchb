// GeneralViewin.h -- generalized Window interface, shows a View Rectangle

#import "BaseObject.h"
@class Garden;

typedef struct {	int x; int y; } GeneralViewinPoint;
static const GeneralViewinPoint GeneralViewinPointNULL = {-1, -1};
static const int numRgb = 12;

@interface GeneralViewin : BaseObject {
    NSRect frame;
//	int windowX, windowY, windowWidth, windowHeight;
//	int rgb[numRgb];
	int numFirstColor;
    Garden *myGarden;
}

//public:
- initRect :(NSRect)frame;
//- initX :(float)x y :(float)y w :(float)w h :(float)h;
- setTarget :(id)mainGarden;
- runViewin;

- (int) width;
- (int) height;

//- (int) makeColor :(char *)colorName;
- (void) makeColorsPalette;
- (int) int2colorNum :(int)i;
- (void)display3D;

- (int) drawBoxX :(int)x0 y :(int)y0 w :(int)dx h :(int) dy color: (int)color;
- (int) drawDottedRectX:(int)x0 y :(int)y0 w :(int)dx h :(int) dy color: (int)color;
- putPointX :(int)x_ y :(int)y_ pattern :(int)patrn color :(int) colr;

- (GeneralViewinPoint) mouseClick;
- (int) displayViewin;
//- (int) close;

@end
