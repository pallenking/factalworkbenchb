// OpenGLViewin.h -- GeneralViewin subclass for NextStep windows

#import "GeneralViewin.h"
#import "PlantOpenGLView.h"

@interface OpenGLViewin : GeneralViewin
{
    NSWindow *theNsWindow;
    PlantOpenGLView *myView;
}

- (void)display3D;

@end
