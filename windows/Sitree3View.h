//
//  Sitree3View.h
//  sitree3
//
//  Created by Allen King on 8/3/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface Sitree3View : NSView

- (id)initWithFrame:(NSRect)frame;
- (void)dealloc;

@end
