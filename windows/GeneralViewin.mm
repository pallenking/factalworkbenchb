// GeneralViewin.mm -- generalized Window interface

#include "GeneralViewin.h"
#include "Common.hpp"
@implementation GeneralViewin

- initRect :(NSRect)frame_; {
//-initX :(float)x y :(float)y w :(float)w h :(float)h; {
    self = [super init];

    frame = frame_;
//    windowX = frame.origin.x;
//    windowY = frame.origin.y;
//	windowWidth = frame.size.width;
//    windowHeight = frame.size.height;

//	rgb[ 0] = 0x000000;		// black
//	rgb[ 1] = 0x404040;		// dark gray
//	rgb[ 2] = 0x808080;		// gray
//	rgb[ 3] = 0xc0c0c0;		// light gray
//	rgb[ 4] = 0xffffff;		// white
//	
//	numFirstColor = 5;
//	rgb[ 5] = 0xff0000;		// red
//	rgb[ 6] = 0xff7f00;		// orange
//	rgb[ 7] = 0xffff00;		// yellow
//	rgb[ 8] = 0x00ff00;		// green
//	rgb[ 9] = 0x00ffff;		// green-blue
//	rgb[10] = 0x0000ff;		// blue
//	rgb[11] = 0x7f00ff;		// violet
    return self;
}

- (char *)classNamePrefix; {
    static int nodeNum = 1;
    return mprintf((char *)"W%d", nodeNum++);
}
- setTarget :(id)mainGarden     {   myGarden = mainGarden;  return self;}
- runViewin                     {   panic("prototype");     return self;}

- (int) width;                  {	return frame.size.width;            }
- (int) height;                 {	return frame.size.height;           }
//- (int) width;                  {	return windowWidth;                 }
//- (int) height;                 {	return windowHeight;                }

- (void) makeColorsPalette;     {   panic("prototype");                 }
//- (int) makeColor :(char *)colorName; { return 0;                     }
- (int) int2colorNum :(int)i;   {
	return numFirstColor + i%(numRgb - numFirstColor);
}
- (void)display3D;              {   panic("prototype");                 }

- (int) drawBoxX :(int)x0 y :(int)y0 w :(int)dx h :(int) dy color: (int)color;
                                {   panic("prototype");     return 0;   }
- (int) drawDottedRectX :(int)x y :(int)y w :(int)w h :(int)h color: (int)color;
{	for (int i=0; i<w; i++)
        [self putPointX :x+i y:y pattern :0 color:color];
	for (int i=0; i<h; i++)
		[self putPointX :x+w-1 y:y+i pattern :0 color:color];
    //		putPoint(x+w-1, y+i, patrn, colr);
	for (int i=0; i<w; i++)
		[self putPointX :x+w-1-i y:y+h-1 pattern :0 color:color];
    //		putPoint(x+w-1-i, y+h-1, patrn, colr);
	for (int i=0; i<h; i++)
		[self putPointX :x y:y+h-1-i pattern :0 color:color];
    //		putPoint(x, y+h-1-i, patrn, colr);
    return 0;
}
//  {   panic("prototype");     return 0;   }
    
- putPointX :(int)x_ y :(int)y_ pattern :(int)patrn color :(int) colr;
                                {   panic("prototype");     return 0;   }
- (GeneralViewinPoint) mouseClick;{ panic("prototype");     return GeneralViewinPointNULL; }
- (int) displayViewin;			{   panic("prototype");     return -1;  }

- printContents :(int)indent; {
	printf("GeneralViewin: Garden=%#lx, window=%.0fx%.0f+%.0f,%.0f\n",
           (unsigned long int)myGarden, frame.origin.x, frame.origin.y, frame.size.width, frame.size.height);
//         (unsigned long int)myGarden, windowX, windowY, windowWidth, windowHeight);
    return self;
//	printf("\n%*s\\", indent*3, "");
//	return [super printContents :indent];
}




@end
