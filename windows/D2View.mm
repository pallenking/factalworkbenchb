// D2View.mm -- override NSView, so drawRect goes back to stux methods

/* to do:
		unify D2View and SimNsV
 */

#import "D2View.h"
#include "Common.h"
#include "NSCommon.h"

@implementation D2View

- initWithWindow:aWindow; {
    self = [self init];                    //    [self initWithFrame:￼]???
    window = aWindow;				//    [self setWindow:aWindow];
    [self makeColorPalette];

    [aWindow setContentView:self];
    [aWindow setTitle:@"D2View"];
    [aWindow setBackgroundColor:[NSColor greenColor]];
    //   [window setLevel:NSFloatingWindowLevel];
    //// [window orderFrontRegardless];
    [aWindow display];
    [aWindow makeKeyAndOrderFront:self];
    return self;
}

- (void)dealloc; {
//	self.window             = nil;  //?? window
//	self.delegateObject     = nil;  //?? delegateObject
    panicC("???");

    [super dealloc];
}

//- setGeneralViewin:(id)window_;   {    myNextViewin = window_;    return self; }
- setDrawingMethod:(CALLBACK)method object:(void *)ob; {
    delegateObject = (id)ob;
    delegateMethod = method;
    return self;
}

- (BOOL) isFlipped;            {    return true;                       }

- (void) makeColorPalette; {
    int i=0;
    // fixed allocation colors
    palette[i++] = [NSColor whiteColor]; //  palette[i++] = [NSColor colorBlack];
    palette[i++] = [NSColor redColor];
    palette[i++] = [NSColor yellowColor];
    palette[i++] = [NSColor greenColor];
    palette[i++] = [NSColor blueColor];
    numPaletteV1 = i;
    
    // to handle all the other color numbers
    palette[i++] = [NSColor magentaColor]; //    palette[i++] = [NSColor colorWhite];
    palette[i++] = [NSColor brownColor];  //need more 120905
    palette[i++] = [NSColor cyanColor];
    palette[i++] = [NSColor darkGrayColor];
    palette[i++] = [NSColor lightGrayColor];
    numPaletteV2 = i;

    assertC(i<=maxPalette, ("too many colors"));
    while (i < maxPalette)
        palette[i++] = 0;
}

- (void) setColor:(int)num; {
    int delta = numPaletteV2 - numPaletteV1;
    if (delta) {
        int n = numPaletteV1 + num%delta;
        assert(n>=0 and n<maxPalette, (@"color number %d->%d out of range",num, n));
        NSColor *c = palette[n];
        [c set];
    }
	else
        [[NSColor yellowColor] set];
}

// Kick off redraw, as NSView wants
- (void)drawRect:(NSRect)dirtyRect {
    if (delegateMethod == 0)
        printf("D2View with ZERO delegateMethod\n");
    else
        (* delegateMethod)(delegateObject, self);
}

///////// DRAWING PRIMITIVES:
- (void) drawBox:(NSRect)box color:(int)color_; {
    [self setColor:color_];
	[NSBezierPath fillRect:box];
}
- (void) drawRect:(NSRect)box color:(int)color_; {
    [self setColor:color_];
	[NSBezierPath strokeRect:box];
}
- (void) drawDottedRect:(NSRect)box color:(int)color_; {
    [self setColor:color_];
    NSBezierPath* aPath = [NSBezierPath bezierPath];
    static const double lineDash[2] =   {2, 2};
    [aPath setLineDash:lineDash count:2 phase:0.0];
    [aPath appendBezierPathWithRect:box];
    [aPath stroke];
}
- (void) putPoint:(NSPoint)point color:(int)color_; {
    panicC("THIS ROUTINE DOESN'T DO ANYTHING!!");
    [self setColor:color_];
    NSBezierPath* aPath = [NSBezierPath bezierPath];
    [aPath moveToPoint:point];
    [aPath lineToPoint:point];
    [aPath stroke];
}

@end
