//  Sitree3AppDelegate.mm
//  Created by Allen King on 8/2/11.

#import "Sitree3AppDelegate.h"
extern int newFrame();

@implementation sitree3AppDelegate

@synthesize window;

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    newFrame();
}

- (NSApplicationTerminateReply)applicationShouldTerminate:(NSApplication *)sender {
    //add to logFile!
    printf("---------- hey, I'm here!!! -------\n");
    return NSTerminateNow;
}


@end
