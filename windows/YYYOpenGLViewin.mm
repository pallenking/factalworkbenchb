// OpenGLViewin.mm -- GeneralViewin subclass for OpenGL windows

#import "OpenGLViewin.h"
#import <Cocoa/Cocoa.h>
#import "Common.hpp"

@implementation OpenGLViewin

- initRect :(NSRect)frame_;
//- initX :(float)x y :(float)y w :(float)w h :(float)h;
{   [super initRect :frame_];
//  [super initX:x y:y w:w h:h];
        
    // make a window object;
//    NSRect frame = NSMakeRect(x,y, w,h);
	theNsWindow = [[NSWindow alloc] initWithContentRect :frame
        styleMask :NSTitledWindowMask | NSResizableWindowMask |  NSMiniaturizableWindowMask
        backing :NSBackingStoreBuffered defer :NO];
    [theNsWindow setTitle :@"OpenGLViewin.mm"];

    // make OpenGL view
	myView = [[PlantOpenGLView alloc] init];
    [myView setGeneralViewin :self];
	[theNsWindow setContentView :myView];
    
//    [theNsWindow setBackgroundColor:[NSColor greenColor]];
	[theNsWindow setLevel :NSFloatingWindowLevel];

/**/[theNsWindow display];
	[theNsWindow makeKeyAndOrderFront :self];
    
    return self;
}

- (void)display3D;            {       [myGarden display3D];                }

- printContents :(int)indent; {
	printf("OpenGLViewin:");
	printf("theNsWindow=%lx myView=%lx", (long unsigned int)theNsWindow, (long unsigned int)myView);
	printf("\n%*s\\", indent*3, "");
	return [super printContents :indent];
}

@end
