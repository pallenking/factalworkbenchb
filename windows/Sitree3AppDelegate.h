//  Sitree3AppDelegate.h
//  Created by Allen King on 8/2/11.

#import <Cocoa/Cocoa.h>
#import "GeneralViewin.h"

@interface sitree3AppDelegate : NSObject <NSApplicationDelegate> {
    GeneralViewin *window;
}

@property (assign) IBOutlet GeneralViewin *window;

@end
