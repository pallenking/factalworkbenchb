//  LinkView.h -- View a particular link C2013PAK

/*
 (Philosophy: all work is done in Link; this class mostly just forwards
 */

#import "View.h"

@interface LinkView : View

    @property (nonatomic, retain) View *other1View;
    @property (nonatomic, retain) View *other0View;

    @property (nonatomic, assign) Vector3f other1Loc;// location superView's coords (e.g a view --> Splitter)
    @property (nonatomic, assign) Vector3f other0Loc;
        ///// Calculations for wires are done in the bosses superView's coordinate systems
@end
