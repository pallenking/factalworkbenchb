//  DownNUpButton.h -- NSButton which tells it's up/down state C2016PAK
// It's isPressed field helps PushButtonBidirNsV weed out keyboard autorepeats

#import <Cocoa/Cocoa.h>

@interface DownNUpButton : NSButton

    @property (nonatomic, assign)	bool	isPressed;

 // These accessors match NSSlider
- (float) value;
- (void) setValue:(float)val;
@end
