//  LinkView.mm -- View a particular link C2013PAK

#import "Link.h"
#import "LinkView.h"

#import "Common.h"
#import "NSCommon.h"
#import "WorldTurtleSettings.h"

@implementation LinkView

- init {
	self = [super init];
	self.bossView = nil;
    self.workerView = nil;
	
	// Help: Think about unifying with relaySpots
	self.bossLoc = vector3fZero;
    self.workerLoc = vector3fZero;
	return self;
}

- (void)normalizeTreesHeights;	{											}

/// ///////////////////////////////////////////////////////////////////////
// //////////////////////// Automatic Positioning ////////////////////////
/////////////////////////////////////////////////////////////////////////
#pragma mark - 3D Views
- (void)boundAndPlace;	{													}
	// LinkView's do not affect the bounds.
	// LinkView's place lines in "placeLine", AFTER all Models have places

- (void)placeAgain;		{	[(Link *)self.model adjustPlaceInView:self];	}

- (void)renderWith:(RenderingContext *)rc up:(int)viewedUpright;
						{	[(Link *)self.model renderLinkInView:self with:rc up:viewedUpright];}

#pragma mark - Pretty Print
 // override View's print routines for link
- (void)printShapes;	{	printf("Line%*s", 3, "");						}

- (void)printOfSuperview;{
	NSprintf(@"b(%@", self.bossView.name);
	NSprintf(@"%@%@)<-", stringForScaledVector(nil, self.bossLoc), self.bossView.model.name);
	NSprintf(@"w(%@", self.workerView.name);
	NSprintf(@"%@%@)", stringForScaledVector(nil, self.workerLoc), self.workerView.model.name);
}

- (void)printPositions; {
}

guiVector3fAccessors(bossLoc);
guiVector3fAccessors(workerLoc);

@end
