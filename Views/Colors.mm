//  Colors.mm -- simple color

/* to do
 */

#import "Colors.h"

// =================== PUBLIC UCHAR COLORS ==================

uColors3 colorTag(int colorInt) {
	return (uColors3)  {colorInt>>16, colorInt>>8, colorInt};
}
int colorInt(uColors3 colorTag) {
    return colorTag.r*(1<<16) + colorTag.g*(1<<8) + colorTag.b;
}

NSString *colorString(uColors3 color) {
	return [NSString stringWithFormat:@"%02x %02x %02x", color.r, color.g, color.b];
}
NSString *colorString(FColor color) {
	return [NSString stringWithFormat:@"%.3f %.3f %.3f", color.d[0], color.d[1], color.d[2]];
}

NSString *colorString(int color) {
	return colorString(colorTag(color));
}

 // mix==0 ==> use B
FColor lerp(FColor a, FColor b, float mixA) {
	float mixB = 1.0-mixA;
	
	FColor c;
	for (int i=0; i<4; i++)
		c.d[i] = mixA * a.d[i] + mixB * b.d[i];
	return c;
}

FColor colorBundlePorts_bias(FColor bias) {
	return lerp(bias, colorBundleBits, 1? 1.0: 0.3);//1.0);//0.2
}



