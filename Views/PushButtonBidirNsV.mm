// PushButtonBidirNsV.mm -- Bidirectional Button  C2015PAK

#import "Brain.h"
#import "Part.h"
#import "Port.h"
#import "Path.h"
#import "View.h"
#import "Bundle.h"
#import "Leaf.h"
#import "Brain.h"

#import "BundleTap.h"
#import "GenAtom.h"
#import "SoundAtom.h"

#import "Common.h"
#import "NSCommon.h"
#import "Id+Info.h"

#import "InspecVc.h"
#import "SimNsWc.h"
#import "TimingChain.h"

#import "PushButtonBidirNsV.h"
#import "DownNUpButton.h"

@implementation PushButtonBidirNsV

#pragma mark 1. Basic Object Level					// Basic objects

- (NSString *)	name;			{			return self.title;				}

- (void) dealloc;				{
	self.sound			= nil;

	[super dealloc];
}

#pragma mark 4. Factory								// Compound objects
- (void) buildBidirViewWithTarg:(Bundle *)targBun; {

	 // Create the Button:
	DownNUpButton *button = [[[DownNUpButton alloc] initWithFrame:NSZeroRect] autorelease];
	button.target = self;						// Left-clicks come to self
	button.action = @selector(buttonTapped:);
	[button sendActionOn: NSLeftMouseDownMask | NSLeftMouseUpMask];
	button.title = self.title;
	 // Install it:
	self.outboundNsControl = button;
	[self addSubview:button];

XX	[super buildBidirViewWithTarg:targBun];		// sets self.targBun = targBun;

	 // Insert PushButton sound into targetBun's GenAtom:
	Leaf *leaf = self.targetPort.leaf;
	[leaf applyProp:@"sound" withVal:self.sound];
//	if (SoundAtom *soundAtom = coerceTo(SoundAtom, self.targetPort.atom))
//		soundAtom.sound = self.sound;
}

- (void) layout {										[super layout];

	/*  170116PAK CAUSES:
	 *	PushButtonBidirNsV or one of its superclasses may have overridden -layout
	 *	without calling super. Or, something may have dirtied layout in the
	 *	middle of updating it. Both are programming errors in Cocoa Autolayout.
	 *	The former is pretty likely to arise if some pre-Cocoa Autolayout class
	 *	had a method called layout, but it should be fixed.
	 *
	 *	study updateViewConstraints;
	 */
	self.outboundNsControl.frame = self.bounds;
//	self.button.frame = self.bounds;
}

#pragma mark 7. Simulator Messages

- (IBAction)buttonTapped:(id)sender {
	int pressed						= (int)[sender isPressed];
	if (sender != self.outboundNsControl)
		NSPrintf(@"\n ****** Window was regenerated; Something changes here. (May be OK)\n\n");

	 // Find the brain we are connected to:
	View  *view						= mustBe(View, self.inspectNsVc.representedObject);
	Part  *part						= mustBe(Part, view.part);
	Brain *brain					= part.brain;

	  // A tapped Button with a clockEvent computes a 1:N code on downstroke,
	 //  		and clocks the state (Previous) on upstroke.

	  // For some reason, DownNUpButton issues a downPress occurs
	 // before an upPress. Detect and weed out
	if (pressed and !self.wasPressedOnce) {			/// ++ Button DOWN ++
		self.wasPressedOnce 	= true;
		[brain logEvent:@"\n=== DOWN EVENT: '%@' Button of PushButtonBidirNsV", self.title];// on button

		 // Insert 1:N code into network
		if (self.outputToPort)
			self.outputToPort.valueTake = 1.0;

		if (self.clockEvent) {
			 // Generate an event for press (Acts as a 1:N code in a Bundle):
			for (TimingChain *timingChain in brain.timingChains) {

XX				[timingChain takeEvent:self.clockEvent];// Gui Button generates a down event

				  /// These OPTIONS are set after the takeEvent, from tags in the .nib
				 /// Retract the assertion after the button is pressed:
				if (self.retract)
					timingChain.retractPort = coerceTo(Port, self.outputToPort);
			}
		}
	}
	else if (!pressed) {							/// ++ Button UP ++
		self.wasPressedOnce	= false;
		[brain logEvent:@"\n=== UP EVENT: '%@' Button of PushButtonBidirNsV", self.title];// on button

		if (self.clockEvent)
			for (TimingChain *timingChain in brain.timingChains)

XX				[timingChain releaseEvent];	// causes up event

	}
}
@end
