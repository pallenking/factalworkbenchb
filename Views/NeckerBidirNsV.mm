//  NeckerBidirNsV.mm -- to draw a visual image of the 7 necker cube elements. C2013PAK

// to_do:

#import "NeckerBidirNsV.h"
#import	"Bundle.h"
#import "Common.h"
#import "NSCommon.h"
#import "Port.h"
#import "Id+Info.h"

 //////////////////////////////////////////////////////////////////
// Necker5 has 16 possible vertices, 1..16. (0 is end of line)
//		 1		 4----7
// 	  (y)	    / \  / \
//			   /  9	 13 \
// 			  /	  10 14  \
//			 /    /  \    \
//		 0	1----3    6----8
//			 \    \  /    /
//	 		  \	  11 15	 /
//	 		   \  12 16 /
//				\ /  \ /
//	    -1		 2----5
//
//          0    1    2    3       (x)

static NSPoint vertices5[] =   {  {-1,-1},					// 0: ILLEGAL vertex number
	  {0,   0 },												// 1
	  {1,  -1 },  {1,   0 },  {1,   1  },					// 2,3,4
	  {2,  -1 },  {2,   0 },  {2,   1  },					// 5,6,7
	  {3,   0 },												// 8
	  {1.25,0.75},  {1.25,0.25},  {1.25,-0.25},  {1.25,-0.75},	// 9,10,11,12
	  {1.75,0.75},  {1.75,0.25},  {1.75,-0.25},  {1.75,-0.75}	// 13,14,15,16
};
static short paths5[] =   {
	9,14,0,
	15,12,0,
	1, 3,10, 3,11,3, 1,4,9, 4,7,13, 7,8,6,14, 6,15, 6,8,5,16, 5,2,12, 2,1, 0,
	11,16,0,
	10,13,0,
	0
};

@implementation Necker5View
- (void) buildBidirViewWithTarg:(Bundle *)targBun; {

	self.vertices	= vertices5;
	self.paths		= paths5;
	self.nSegments	= 5;
	assert(self.nSegments<maxSegments, (@"self.nSegments too big"));

	[super buildBidirViewWithTarg:targBun];
}

@end

 //////////////////////////////////////////////////////////////////
// Necker7 has 8 virteces, 1..8. (0 is end of line)
//			1		4---7
//		(y)		  /	  X   \    .
//			0	 1--3   6--8
//			      \   X   /
//		   -1       2---5
//			     0  1   2  3         (x)

static NSPoint vertices7[] =  {
	 {-1,-1},						// 0 ILLEGAL vertex
	  {0, 0},						//   1
	  {1,-1},   {1,0},   {1,1},		// 2,3,4,
	  {2,-1},   {2,0},   {2,1},		// 5,6,7
	  {3, 0}						//   8
};
static short paths7[] =   {
	1,3,0,							// a
	3,5,0,							// b
	3,7,0,							// c
	1,2,5,8,7,4,1,0,				// d
	4,6,0,							// e
	2,6,0,							// f
	6,8,0,							// g
	0
};

@implementation Necker7View
- (void) buildBidirViewWithTarg:(Bundle *)targBun; {

	self.vertices		= vertices7;
	self.paths			= paths7;
	self.nSegments		= 7;
	assert(self.nSegments<maxSegments, (@"self.nSegments too big"));

	[super buildBidirViewWithTarg:targBun];
}
@end

 //////////////////////////////////////////////////////////////////
//
@implementation NeckerBidirNsV

- (void) buildBidirViewWithTarg:(Bundle *)targBun; {


	  /// Over All 5 or 7 Segments, connecting them to portPath[i]:
	 ///
	NSArray *leafArray = [self.portPath componentsSeparatedByString:@","];
	for (int i=0; i<self.nSegments; i++) {
//XX	id genPort 				= [targBun genPortOfLeafNamed:leafArray[i]];
XX		if (Port *genPort		= [targBun genPortOfLeafNamed:leafArray[i]]) {

			/// self.value[i] 	--> genPort.value
			NSString *valueI 	= [@"" addF:@"value%d", i];
XX			[self bind:valueI toObject:genPort withKeyPath:@"value" options:nil];
			[[@"" addF:@"/\\/\\ NeckerBidirNsV.%@    --bind(float)--> "
						  "%@ -value", valueI, genPort.fullName16] ppLog];

			/// self.valueIn<i>	-->	genPortCon2.value
			Port *genPortCon2	= mustBe(Port, genPort.connectedTo);
			NSString *valueInI 	= [@"" addF:@"valueIn%d", i];
XX			[self bind:valueInI toObject:genPortCon2 withKeyPath:@"value" options:nil];
			[[@"" addF:@"/\\/\\ NeckerBidirNsV.%@    --bind(float)--> "
						  "%@ -value", valueI, genPort.fullName16] ppLog];

								//  topSide:    (B:) inboundFloatValue  <-- havePort.value
		}
		else if (NSNumber *genNumber = coerceTo(NSNumber, genPort))
			assert([genNumber intValue]==0, (@"@0 is ignore, others not permitted"));
		else
			[[@"" addF:@"/\\/\\ NeckerBidirNsV.value%d    --bind(float)--> "
			  "%@ -value ---  FAILED", i, genPort.fullName16] ppLog];
	}
	[super buildBidirViewWithTarg:targBun];
}

 // build setters for value array
#define valueSetterGetter(i)						\
- (float) value##i; {	return value[i];	}		\
- (void) setValue##i:(float)inboundFloatValue; {	\
	value[i] = inboundFloatValue;					\
	self.needsDisplay = true;						\
}													\
- (float) valueIn##i; {	return valueIn[i];	}		\
- (void) setValueIn##i:(float)inboundFloatValue; {	\
	valueIn[i] = inboundFloatValue;					\
	self.needsDisplay = true;						\
}
valueSetterGetter(0)
valueSetterGetter(1)
valueSetterGetter(2)
valueSetterGetter(3)
valueSetterGetter(4)
valueSetterGetter(5)
valueSetterGetter(6)
valueSetterGetter(7)
valueSetterGetter(8)
valueSetterGetter(9)

- (void) drawRect:(NSRect)dirtyRect {		[super drawRect:dirtyRect];

	printf("(NeckerBidirNsV *)%#lx drawRect\n", (unsigned long)self);

	InspecVc *vc = self.inspectNsVc;
    CGContextRef gc = (CGContextRef)[[NSGraphicsContext currentContext] graphicsPort];

	CGContextSetLineWidth(gc, 5.0);
	[self displayLines:valueIn color:colorRed   gc:gc];

	CGContextSetLineWidth(gc, 3.0);
	[self displayLines:value   color:colorGreen gc:gc];
}

- (void) displayLines:(float *)values color:(FColor)color gc:(CGContextRef)gc; {

	float side = 30;
	
	char done[maxSegments] 		= {0};				// all zero
	for (int i=0; i<self.nSegments; i++) {			// for all lines

		  /// The lightest lines are displayed first, so the darker ones dominate
		 ///   Find the leistUndisplayed
		int next2draw			=-1;
		float minVal 			= 1E8;				// very big
		for (int iChan=0; iChan<self.nSegments; iChan++){// paw through all values
			if (minVal > values[iChan] and done[iChan]==0) {	// find the least black (largest)
				minVal = values[iChan];
				next2draw = iChan;						// index of smallest value
			}
		}
		assert(next2draw>=0, (@"no value found"));

		 // find the next2draw'th path
		int maxK=0;									// search whole path's list
		for (int j=0; j!=next2draw and self.paths[maxK]; j++)
		  // not at next2draw, find the next path from it's zero terminaton
			while (self.paths[maxK++] != 0)					// paths end in 0
				;
		
		float val = values[next2draw];
		val = val<0? 0: val>1? 1: val;
		FColor myColor = lerp(color,   colorBackground, val);
		CGContextSetRGBStrokeColor(gc, myColor.d[0], myColor.d[1], myColor.d[2],  1);
		CGContextBeginPath(gc);
		for (int first=1; self.paths[maxK]; maxK++, first=0) {
			NSPoint p = self.vertices[self.paths[maxK]];
			if (first)
				CGContextMoveToPoint(   gc, (3 - p.x)*side, (p.y+1)*side+10);
			else
				CGContextAddLineToPoint(gc, (3 - p.x)*side, (p.y+1)*side+10);
		}
		CGContextDrawPath(gc,kCGPathStroke);
		done[next2draw] = 1;
	}
}
	
@end
