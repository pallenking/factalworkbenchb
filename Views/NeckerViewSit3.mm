//  NeckerView.mm -- to draw a visual image of the 7 necker cube elements. C2013PAK

/*
    the line 'static NeckerView *theOne=0':  GAG ME WITH A SPOON!! this is a kludge just want it to work
*/

#import "NeckerView.h"
#import "Common.h"

@implementation Necker5View
// Necker5 has 16 possible vertices, 1..16. (0 is end of line)
//		 1		 4----7
// 	  (y)	    / \  / \
//			   /  9	 13 \
// 			  /	  10 14  \
//			 /    /  \    \
//		 0	1----3    6----8
//			 \    \  /    /
//	 		  \	  11 15	 /
//	 		   \  12 16 /
//				\ /  \ /
//	    -1		 2----5
//          0    1    2    3       (x)

- (id)initWithFrame:(NSRect)frameRect {
    self = [super initWithFrame:frameRect];
	self.nSegments = 5;
	static NSPoint vertices[] = {{-1,-1},
		{0,0},													// 1
		{1,-1}, {1,0}, {1,1},									// 2,3,4
		{2,-1}, {2,0}, {2,1},									// 5,6,7
		{3,0},													// 8
		{1.25,0.75}, {1.25,0.25}, {1.25,-0.25}, {1.25,-0.75},	// 9,10,11,12
		{1.75,0.75}, {1.75,0.25}, {1.75,-0.25}, {1.75,-0.75}};	// 13,14,15,16
	self.vertices = vertices;
	
	static short paths[] = {
		9,14,0,
		15,12,0,
		1,3,10,3,11,3,1,4,9,4,7,13,7,8,6,14,6,15,6,8,5,16,5,2,12,2,1,0,
		11,16,0,
		10,13,0,
		0};
	self.paths = paths;
	return self;
}

@end

@implementation Necker7View
// Necker7 has 8 virteces, 1..8. (0 is end of line)
//			1		4---7
//		(y)		  /	  X   \    .
//			0	 1--3   6--8
//			      \   X   /
//		   -1       2---5
//			     0  1   2  3         (x)

- (id)initWithFrame:(NSRect)frameRect {
    self = [super initWithFrame:frameRect];
	self.nSegments = 7;
	static NSPoint vertices[] = {{-1,-1},
		{0,0},						//   1
		{1,-1}, {1,0}, {1,1},		// 2,3,4,
		{2,-1}, {2,0}, {2,1},		// 5,6,7
		{3,0}};						//   8
	self.vertices = vertices;
	
	static short paths[] = {
		1,3,0,						// a
		3,5,0,						// b
		3,7,0,						// c
		1,2,5,8,7,4,1,0,			// d
		4,6,0,						// e
		2,6,0,						// f
		6,8,0,						// g
		0};
	self.paths = paths;
	return self;
}
@end

static NeckerView *theOne=0;	// Help!

@implementation NeckerView
//- (void)awakeFromNib; {
//	assert(theOne==0, ("should only make one of these"));
//	theOne = self;
//}

- (id)initWithFrame:(NSRect)frameRect {
    self = [super initWithFrame:frameRect];
	self.nSegments = 0;
	assert(self.nSegments<maxSegments, ("self.nSegments too big"));
	
	assert(theOne==0, ("should only make one of these"));
	theOne = self;
	for (int i=0; i<self.nSegments; i++)
		val[i] = 0.0;
    return self;
}

+ (void)takeValues7:(float *)vals; {
	if (theOne) {
		for (int i=0; i<theOne.nSegments; i++)
			theOne->val[i] = vals[i];
		theOne.needsDisplay = 1;
	}
}

- (void)drawRect:(NSRect)dirtyRect {
    CGContextRef gc = (CGContextRef)[[NSGraphicsContext currentContext] graphicsPort];
	CGContextSetLineWidth(gc, 3.0);
	
	char done[maxSegments];
	for (int i=0; i<maxSegments; i++)
		done[i] = 0;

	  // there is a problem with overlapping lines.
	 // we order display, so the lightest are done first, and the darkest dominate
	float side = 30;
	float grey = 0.95;
	for (int i=0; i<self.nSegments; i++) {			// for all lines
		int nextChan=-1;
		float minVal = 1E8;
		for (int iChan=0; iChan<self.nSegments; iChan++){// paw through all values
			if (minVal > val[iChan] and done[iChan]==0) {	// find the least black (largest)
				minVal = val[iChan];
				nextChan = iChan;						// index of smallest value
		}	}
		assert(nextChan>=0, ("no value found"));

		 // find the nextChan'th path
		int maxK=0;
		for (int j=0; j!=nextChan and self.paths[maxK]; j++)
			while (self.paths[maxK++] != 0)						// paths end in 0
				;
		
		float v = grey * (1.0 - val[nextChan]);
		CGContextSetRGBStrokeColor(gc, v,v,v,  1);
		CGContextBeginPath(gc);
		for (int first=1; self.paths[maxK]; maxK++, first=0) {
			
			NSPoint p = self.vertices[self.paths[maxK]];
			if (first)
				CGContextMoveToPoint(gc, (p.x+0)*side, (p.y+1)*side+10);
			else
				CGContextAddLineToPoint(gc, (p.x+0)*side, (p.y+1)*side+10);
		}
		CGContextDrawPath(gc,kCGPathStroke);
		done[nextChan] = 1;
	}
}
	
@end
