//  RenderingContext.mm -- particulars while drawing this view C2013PAK

#import "RenderingContext.h"
#import "Atom.h"			// I wish this wasn't here!
#import "View.h"
#import "Common.h"
#import "GlCommon.h"
#import "Colors.h"
#import "NSCommon.h"
#import <GLUT/glut.h>
#include <stdlib.h>
#include <sys/time.h>
#import "Id+Info.h"

@implementation RenderingContext

RenderModes renderModeKindOfString(NSString *str) {
    int ak = render3DUndefined;
    for (NSString *s in renderModeKindArray) {
        if ([s isEqualToString:str])
			return (RenderModes)ak;
        ak++;
    }
    return render3DUndefined;
}

#pragma mark 1. Basic Object Level
- init {								self = [super init];

	self.drawstack = [NSMutableArray array];
	[self reset];
	return self;
}

- (id) deepMutableCopy; {				id rv = [super deepMutableCopy];
	panic(@"");
	return rv;
}

- (void) dealloc; {
	self.soughtView		= nil;
	self.drawingView	= nil;
	self.drawstack		= nil;
	
	[super dealloc];
}

- (void) reset; {
	assert(self.drawstack.count==0, (@"drawstack didn't empty"));
	self.colorCounter = 0x1;
    //	do not change: renderMode, colorReversed, soughtTag, soughtView, renderDebug
}

int bitFlip(int x);

 // Push a new (Fw)View onto Rendering Stack
- (void) startDrawingView:(View *)v; {

	 // PUSH head to stack:
	ViewGlObjectContext *newContext = [[[ViewGlObjectContext alloc] init] autorelease];
	[self.drawstack addObject:newContext];
	newContext.drawingView = self.drawingView;
	newContext.colorReversed = self.colorReversed;

	self.drawingView = v;
	
	self.colorReversed = bitFlip(self.colorCounter++);		// generate tags 
	if (self.renderMode == renderFindTagObject) {

		 /// Does this part have the desired color, of the picked object.
		if (self.soughtTag == self.colorReversed) {

			  // We just found the picked part!
			 //
			assert(isNan(self.soughtViewsCenter), (@"set twice during pic"));

			self.soughtView = v;						// at desired tag
			self.soughtViewsCenter = [v convertSpot:v.bounds.center() toSuperview:v.rootFwV];
		}
    }
}
- (void) finishDrawingView:(View *)v; {

	if (v==nil)
		;//printf("anonymous finishDrawingView -- cowardly continuing\n");
	else if (self.drawingView != v) {
		[self print];
		panic(@"finishDrawingView: %#lx is not at head of stack", (LUI)v);
	}
	
	ViewGlObjectContext *newContext = [self.drawstack lastObject];
	self.drawingView = newContext.drawingView;
	self.colorReversed = newContext.colorReversed;
	
	[self.drawstack removeLastObject];	// should free
}

 // spread sequential integers (1,2,3,... )
// to maximally separated colors (rgb) 8000000, 008000, 808000,...
int bitFlip(int x) 	{ // scramble: int[24] --> <MSB>RgbRgbRgbRgbRgbRgbRgbRgb<LSB>
	int r=0, g=0, b=0;
	for (int i=0; i<8; i++) {
		r = (r<<1) + ((x&1)!=0);
		g = (g<<1) + ((x&2)!=0);
		b = (b<<1) + ((x&4)!=0);
		x >>= 3;
	}
	return (r<<16) + (g<<8) + b;
}

- (void) setColor:(FColor)colorFromUser; {
	 // Blink per selection state:
	float toWhite=0, toBlack=0;					// final shade of grey
	if (self.drawingView.isSelected) {				// Selected blinks white
	  
		static float ramp;
		struct timeval timeNow;// use system clock for blinking
		gettimeofday(&timeNow, nil);
		long long timeMs = timeNow.tv_sec * 1000 + timeNow.tv_usec / 1000;
		long periodMs = 1000;
		
		ramp = (timeMs % periodMs)/(float)periodMs;
		
		// exponential weighting
		static float tau=0.2, fn0 = 1.0 - exp2f(-1.0/tau);
		ramp = (1.0 - exp2f(-ramp/tau))/fn0;

		toWhite = 1.0 - ramp;
	}
 // 161007 removed
//	else if (self.drawingView.heightLeaf > 0)
//		toBlack = (self.drawingView.heightLeaf-1)/6.0;				// grey on inner parts fades
//	printf("2white=%f, 2black=%f\n", toWhite, toBlack);

	float fudgeGrey = 0;
	FColor color4gl = (FColor)  {
		(colorFromUser.d[0] *(1.0-toBlack) ) *(1.0-toWhite) + toWhite + fudgeGrey,
		(colorFromUser.d[1] *(1.0-toBlack) ) *(1.0-toWhite) + toWhite + fudgeGrey,
		(colorFromUser.d[2] *(1.0-toBlack) ) *(1.0-toWhite) + toWhite + fudgeGrey,  1};

XX	_color = color4gl;			// access underlying color object
	
	if (self.renderMode==render3Dlighting) {
//		glMaterialfv(GL_FRONT,          GL_AMBIENT,		colorRed8.d);
		glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE,		color4gl.d);	//was
		glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR,	color4gl.d);
		glMaterialf( GL_FRONT_AND_BACK, GL_SHININESS,	100);
		glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION,	color4gl.d);	//was
	}
	else if (self.renderMode==render3Dcartoon)
		glColor3fv(color4gl.d);
	
	else /*if (pic == TRUE)*/ {			// use pic-color identity
		int cs = self.colorReversed;
		glColor3ub(cs>>16, cs>>8, cs>>0);
	}
}

- (NSString *) pp:aux; {
	panic(@"");
	id rv = [super pp:aux];
	return [rv addF:@" renderModes=%@ color=%@",
		renderModeKindArray[(int)self.renderMode], colorString(self.color) ];
}

- (void) print; {
	NSPrintf(@"(RenderingContext *)%p:\n  PRE:", self);
	NSPrintf(@"<%06x>    renderMode=%d,   ", self.colorCounter, self.renderMode);
	NSPrintf(@"tDes=%#x,oDes=%#lx\nHEAD:", self.soughtTag, (LUI)self.soughtView);
	
	NSPrintf(@"[%06x] ", self.colorReversed);
 	NSPrintf(@" (%12@ *)%#lx = %10@\n", [self.drawingView className], (LUI)self.drawingView, self.drawingView);
	int i=0;
	for (ViewGlObjectContext *cff in [self.drawstack reverseObjectEnumerator])   {
		NSPrintf(@"%4d: ", i++);
		[cff print];
	}
}
@end

  ////////////////////////////////////////////////////////////////////
 ////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////

@implementation ViewGlObjectContext 
- init {
	self.drawingView = nil;
	return self;
}
- (void) print; {
	NSPrintf(@"  {%06x} ", self.colorReversed);
 	NSPrintf(@" (%12@ *)%#lx = %10@\n", [self.drawingView className], (LUI)self.drawingView, self.drawingView);
}
@end



