//  Colors.h -- simple color

// =================== PUBLIC UCHAR COLORS ==================
typedef struct   {unsigned char r, g, b; } uColors3;

// =================== PUBLIC FLOAT COLORS ==================
typedef struct   { float d[4];           } FColor;

uColors3 colorTag (int          colorInt);
int      colorInt (uColors3     colorTag);

NSString *colorString (uColors3 colorTag);
NSString *colorString (int      colorInt);
NSString *colorString (FColor   color);

FColor lerp(FColor a, FColor b, float mix);

static const FColor colorInvis				=   {0.00, 0.00, 0.00, 0};

static const FColor colorGrey2				=   {0.20, 0.20, 0.20, 1};
static const FColor colorGrey4  			=   {0.40, 0.40, 0.40, 1};
static const FColor colorGrey6  			=   {0.60, 0.60, 0.60, 1};
static const FColor colorGrey7  			=   {0.70, 0.70, 0.70, 1};
static const FColor colorGrey8  			=   {0.80, 0.80, 0.80, 1};
static const FColor colorGrey9				=   {0.90, 0.90, 0.90, 1};
//static const FColor colorGrey95  			=   {0.95, 0.95, 0.95, 1};
static const FColor colorWhite9				=   {0.90, 0.90, 0.90, 1};
static const FColor colorWhite				=   {1.00, 1.00, 1.00, 1};
static const FColor colorBlack				=   {0.00, 0.00, 0.00, 1};

static const FColor colorRed				=   {1.00, 0.00, 0.00, 1};
static const FColor colorRed1				=   {0.90, 0.00, 0.00, 1};
static const FColor colorRed2				=   {0.80, 0.00, 0.00, 1};
static const FColor colorRed3				=   {0.70, 0.00, 0.00, 1};
static const FColor colorRed5				=   {0.50, 0.00, 0.00, 1};
static const FColor colorRed8				=   {0.20, 0.00, 0.00, 1};
/*
//static const FColor colorRed1				=   {1.00, 0.10, 0.10, 1};
//static const FColor colorRed2				=   {1.00, 0.20, 0.20, 1};
//static const FColor colorRed3				=   {1.00, 0.30, 0.30, 1};
//static const FColor colorRed5				=   {1.00, 0.50, 0.50, 1};
//static const FColor colorRed8				=   {1.00, 0.50, 0.50, 1};
 */
//static const FColor colorRed2b  			=   {0.80, 0.00, 0.00, 1};
//static const FColor colorRedA  			=   {0.70, 0.00, 0.00, 1};
//static const FColor colorPink  			=   {1.00, 0.70, 0.70, 1};
//static const FColor colorPink4  			=   {1.00, 0.40, 0.40, 1};
static const FColor colorPink5  			=   {1.00, 0.50, 0.50, 1};
static const FColor colorPink6  			=   {1.00, 0.60, 0.60, 1};
static const FColor colorPink7  			=   {1.00, 0.70, 0.70, 1};
static const FColor colorPink8  			=   {1.00, 0.80, 0.80, 1};
static const FColor colorPink9  			=   {1.00, 0.90, 0.90, 1};
//static const FColor colorPink2Transparent	=   {1.00, 0.80, 0.80, .05};
static const FColor colorOrange  			=   {1.00, 0.50, 0.00, 1};
static const FColor colorOrange5  			=   {1.00, 0.50, 0.00, 1};
static const FColor colorYellow  			=   {1.00, 1.00, 0.00, 1};
//static const FColor colorYellow8			=   {0.90, 0.90, 0.10, 1};
//static const FColor colorYellow5			=   {0.75, 0.75, 0.25, 1};
static const FColor colorBrown				=   {0.57, 0.39, 0.34, 1}; //{146/256., 101/256.,  86/256., 1};
//static const FColor colorBrown2				=   {0.33, 0.45, 0.00, 1}; //{ 84/256.,  37/256.,   0/256., 1};
//static const FColor colorBrownA				=	{0.73, 0.48, 0.15, 1}; //{188/256., 123/256.,  38/256., 1};
//static const FColor colorBrownB				=	{0.70, 0.70, 0.00, 1};
//static const FColor colorBrownC				=	{0.37, 0.24, 0.07, 1}; //{188/512., 123/512.,  38/512., 1};
//static const FColor colorBrownD				=	{0.33, 0.45, 0.03, 1}; //{ 84/256.,  37/256.,   8/256., 1};
//static const FColor colorBrownE				=	{0.70, 0.26, 0.02, 1}; //{178/256.,  66/256.,   4/256., 1};

static const FColor colorGreen  			=   {0.00, 1.00, 0.00, 1};
static const FColor colorGreen2b			=   {0.00, 0.80, 0.00, 1};
//static const FColor colorGreen2  			=   {0.40, 1.00, 0.40, 1};
static const FColor colorGreen8				=   {0.00, 0.50, 0.00, 1};

static const FColor colorBlue				=   {0.00, 0.00, 1.00, 1};
static const FColor colorBlue8  			=   {0.80, 0.80, 1.00, 1};
static const FColor colorBlue7  			=   {0.70, 0.70, 1.00, 1};
static const FColor colorBlue4				=   {0.40, 0.40, 1.00, 1};

static const FColor colorPurple				=   {0.82, 0.02, 0.92, 1};
static const FColor colorPurple8b			=   {0.20, 0.00, 0.20, 1};	// to black
static const FColor colorPurple6b			=   {0.40, 0.00, 0.40, 1};
static const FColor colorPurple4b			=   {0.60, 0.00, 0.60, 1};

static const FColor colorPurple4w			=   {1.00, 0.40, 1.00, 1};	// (to white) Previous
//static const FColor colorPurple0			=   {1.00, 0.00, 1.00, 1};

static const FColor colorDarkMagenta		=   {0.54, 0.00, 0.54, 1};
static const FColor colorXxx				=   {0.90, 0.90, 0.40, 1};
static const FColor colorBeige              =   {.000, 0.63, 0.41, 1};//{ 255/256.,161/256.,106/256, 1};

static const FColor &colorBundleBits        =   colorBrown;//colorPurple;
FColor colorBundlePorts_bias(FColor bias);

static const FColor *colorNone = 0;
static const FColor &colorOff               = colorGrey2;

static const FColor colorOfBidirActivations[4] = {
		colorWhite,			// <all zero>
		colorGreen,			// have
		colorRed,			// want
		colorBlack };		// haveNwant

static const FColor proxyFColor[] = {
	colorInvis,				// 0
	colorBlue4,				// 1
	colorPurple,			// 2
	colorPurple,			// 3
};

//static const FColor &colorAtom            = colorGreen2;
//static const FColor &colorLink            = colorXxx;
//static const FColor &colorWorldVariable   = colorRedA;
//static const FColor &colorFact            = colorRedA;
//static const FColor &colorCoincidence     = colorPink;
//static const FColor &colorBackground		= colorGrey8;
static const FColor &colorBackground		= colorWhite9;

//static const FColor &colorVariable        = colorBlue8;
//static const FColor &colorInformation     = colorBlue7;
////static const FColor &colorTurtle			= colorBlue4;
//static const FColor &colorCollapsed       = colorYellow8;
//static const FColor &colorCollapsedBlink	= colorYellow;

