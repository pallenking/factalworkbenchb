//  NeckerView.h -- to draw a visual image of the 7 necker cube elements. C2013PAK

/*
	An inspector instantiates this view to draw a stick figure in it.
 */

#import <Cocoa/Cocoa.h>
static const int maxSegments=10;

@interface NeckerView: NSView {
    float val[maxSegments];
    }
    @property (nonatomic, assign) int nSegments;
    @property (nonatomic, assign) NSPoint *vertices;
    @property (nonatomic, assign) short *paths;

+ (void)takeValues7:(float *)vals;

@end

@interface Necker5View: NeckerView
@end

@interface Necker7View: NeckerView
@end
