// SliderBidirNsV.h -- Bidirectional Slider  C2015PAK

#import "BidirNsV.h"
@class Port;

@interface SliderBidirNsV : BidirNsV

	 // These properties are filled in by IB:
    @property (nonatomic, assign)	float		meterFullHeight;// How high is meter

@end
