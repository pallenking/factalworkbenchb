//  RenderingContext.h -- particulars while drawing this view C2013PAK

/*	(This was written before I understood OpenGL drawing contexts)
		Holds associated details of how draw3D should render its view
 */

#import "HnwObject.h"
#import "SystemTypes.h"
#import "vmath.hpp"

@class Part;
@class Atom;
@class View;

@interface RenderingContext : HnwObject;//NSObject

	 // Stack to push onto:
	@property (nonatomic, retain) View 	   *drawingView;	// Top
	@property (nonatomic, retain) NSMutableArray *drawstack;// The rest

	 // View Drawing Modes:
	@property (nonatomic, assign) RenderModes renderMode;
	@property (nonatomic, assign) FColor 	color;

	 // Generate unique PICING colors for each View
	@property (nonatomic, assign) int 		colorCounter;
	@property (nonatomic, assign) int 		colorReversed;
	 // Results of PICING operation
	@property (nonatomic, assign) int		soughtTag;
	@property (nonatomic, retain) View	   *soughtView;
	@property (nonatomic, assign) Vector3f	soughtViewsCenter;

- (void) reset;
- (void) startDrawingView:(View *)v;
- (void) finishDrawingView :(View *)v;

- (void) print;
@end

  ////////////////////////////////////////////////////////////////////
 ////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////


@interface ViewGlObjectContext : NSObject
    @property (nonatomic, assign) id drawingView;		//??retain??
    @property (nonatomic, assign) int colorReversed;

- (void)print;
@end
