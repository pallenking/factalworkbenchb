//  GlCommon.mm -- a collection of OpenGL helpers C2013PAK

#import "Id+Info.h"
#import "GlCommon.h"		// for some reason, must be first so assert isn't undefined
#import "Common.h"
#import "NSCommon.h"
#import <OpenGL/gl.h>
#import <OpenGL/glu.h>
#import <GLUT/glut.h>
#include <OpenGL/glext.h>
#import "Brain.h"
#import "RenderingContext.h"
	
typedef struct { float width, height; void *font;} fontParams;
fontParams fonts[] = {
	{	0													},	// 0 is OFF
//	{	0.1495164,	0.2040551,	GLUT_BITMAP_TIMES_ROMAN_10	},	// 0
	{	0.1495164,	0.2052827,	GLUT_BITMAP_HELVETICA_10	},	// 1  0.1678571
	{	0.18,		0.20,		GLUT_BITMAP_HELVETICA_12	},	// 2  0.2112723
	{	0.24,		0.221875,	GLUT_BITMAP_8_BY_13			},	// 3  0.2126116
	{	0.27,		0.2462426,	GLUT_BITMAP_9_BY_15			},	// 4  0.2469122
	{	0.27,		0.35,		GLUT_BITMAP_HELVETICA_18	},	// 5  0.2943452
	{	0.3525298,	0.4401414,	GLUT_BITMAP_TIMES_ROMAN_24	},	// 6
};
int nFonts = sizeof(fonts)/sizeof(fontParams);

//	 // Enter the plane in which the text will be drawn
//	Matrix4f textMatrix = getModelViewMatrix().inverse();
//	textMatrix.setTranslation(zeroVector3f);
//	float mag = 1.0/textMatrix.at(2,2);		// broken. wants Z distance?
////	glMultMatrixf(textMatrix.data);
//
//	 // now at same point, but Z pointing to the camera
//
//	if (1) {
//		Vector3f plugX = textMatrix.row(0);		// vector into plug
//		glTranslatefv( plugX * radius );
//	}
//	else {		// OLD WAY
//		float heightZ = radius * mag;
//		Vector3f cameraZ = Vector3f(0, 0, 1);
//		Vector3f plugY = textMatrix.row(1);		// vector into plug
//		plugY.normalize();
//	//	Vector3f c1    = cameraZ.crossProduct(plugY);
//	//	c1.normalize();
//	//	Vector3f c2    = c1.crossProduct(plugY);
//	//	c2.normalize();
//		glTranslatefv( cameraZ * heightZ );
//	}
//
//	glMultMatrixf(textMatrix.data);

//	Matrix3f toCam3spin = toCam3inv.setDet(1.0);	// matrix "gain" to 1.0

//	 Matrix3<T> setDet(float determinant) const  {
////		float dete = det();		// won't compile 140720
//		float dete =
//	       + at(0,0) * at(1,1) * at(2,2)
//	       + at(1,0) * at(2,1) * at(0,2)
//	       + at(2,0) * at(0,1) * at(1,2)
//	       - at(2,0) * at(1,1) * at(0,2)
//	       - at(0,0) * at(2,1) * at(1,2)
//	       - at(1,0) * at(0,1) * at(2,2) ;
//		assertC(dete == dete);
//		float sign = (dete > 0) ^ (determinant > 0)? -1.0: 1.0;
//		float dete3 = dete>0? pow(dete, 1.0/3.0): -pow(-dete, 1.0/3.0);
//		float k = sign * determinant/dete3;
//	    return *this * k;
//	 }

void myGlDrawString(RenderingContext   *rc,
			NSString		   *str,
			int					fontNumber,
			Vector3f			inCameraShift,
			Vector2f			spotPercent,
			char				background) {
	Brain *brain = Brain.currentBrain;
	assert(str, (@"can't draw nil string"));

//	rc.color = colorBlue;
//	myGlJack3(rc, 6);					// tick cross

	 /// FONTS:
	fontNumber = fontNumber<0? brain.fontNumber: fontNumber;
	assert(fontNumber>=0 and fontNumber<nFonts, (@"illegal fontNumber"));
	fontParams font = fonts[fontNumber];
	if (fontNumber == 0)
		return;

	glPushMatrix();

		  /// Enter the plane in which the text will be drawn
		 // Hungarian: a Heading is the rotational portion of a Translation Matrix
		Matrix4f toCam4rot  = getModelViewMatrix();
		toCam4rot.setRotation(Matrix3f() * 2.8);
								  //// Now Facing Camera, constant MAG /////
		glLoadMatrixf(toCam4rot); //////////////////////////////////////////

		 // now at same point and zoom, but Z pointing to the camera
		glTranslatefv( inCameraShift );	// move to center of label
//		glutSolidSphere(0.03, 15, 15);	// debug mark

		 /// TEXT bounds
		float boxMargin  = 0.05;//0.5;
		 // Font table height/width are empirically defined, so Fudge factors needed
		float textHeight = 0.30  * font.height				+ 2 * boxMargin;//
		float textWidth  = 0.245 * font.width*str.length	+ 2 * boxMargin;//

 // DESIRED, BUT BROKEN
//		const char *strUTF8 = [str C];
//		const unsigned char *strUTF8U = (const unsigned char *)strUTF8;
//		int tWidth			= glutBitmapLength(font.font, strUTF8U);	// OPENGL_DEPRECATED(10_0, 10_9);
//		int tWidthOfA		= glutBitmapWidth(font.font, 'A');	// ### doesn't compile
//		int tHeight			= 2*tWidthOfA;						// SWAG GUESS
////	int tHeight			= glutBitmapHeight(font.font);  // number of pixels between "lines"
////	textWidth			= tWidth	/30;
////	textHeight			= tHeight	/30;

		Vector3f textBottomLeftCorner(-textWidth*spotPercent.x, -textHeight*spotPercent.y, 0);
		glTranslatefv(textBottomLeftCorner);
		glTranslatef(boxMargin, boxMargin, 0);// N.B.: theoretically, there should be a factor of /2 in the above

//		rc.color = colorRed;
//		glutSolidSphere(0.03, 15, 15);	// debug mark

								  ///////// Now in 2D Screen Spot //////////
		glRasterPos3f(0, 0.0, 0.001);///////////////////////////////////////

		 /// DRAW the text:
		for (const char *s =str.C; s[0]; s++)
XX			glutBitmapCharacter(font.font, *s);    //glutStrokeCharacter(GLUT_STROKE_MONO_ROMAN, *s);//GLUT_STROKE_ROMAN

		glTranslatef(-boxMargin, -boxMargin, 0);// N.B.: theoretically, there should be a factor of /2 in the above

		if (background==0)					// 0: no background
			;
		else if (background==1) {			// 1: SOLID BACKGROUND
			rc.color = colorRed8;							//?????
//			rc.color = colorBackground;
//			glBegin(GL_TRIANGLE_STRIP);				// background rectangle
			glBegin(GL_QUADS);						// background rectangle
				glVertex3f(0,         0,			0);
				glVertex3f(textWidth, 0,			0);
				glVertex3f(textWidth, textHeight,	0);
				glVertex3f(0,		  textHeight,	0);
//				glVertex3f(0,         0,			0);
			glEnd();
		}
		
		else if (background==2) {			// 2: BOX AROUND
			rc.color = colorWhite;
//			rc.color = (FColor){0.7, 0.7, 0.7};
			glBegin(GL_LINE_STRIP);
				glVertex3f(0,         0,			0);
				glVertex3f(textWidth, 0,			0);
				glVertex3f(textWidth, textHeight,	0);
				glVertex3f(0,		  textHeight,	0);
				glVertex3f(0,         0,			0);
			glEnd();
		}
		
		else if (background==3) {			// 3: BOX and BACKGROUND
//			rc.color = colorBackground;
			rc.color = colorWhite;
//			rc.color = colorGrey8;
			glBegin(GL_QUADS);
				glVertex3f(0,         0,			0);
				glVertex3f(textWidth, 0,			0);
				glVertex3f(textWidth, textHeight,	0);
				glVertex3f(0,		  textHeight,	0);
//				glVertex3f(0,         0,			0);
			glEnd();
			rc.color = colorWhite;
			glBegin(GL_LINE_STRIP);			// background rectangle  GL_TRIANGLE_STRIP
				glNormal3f(0,		  0,			1);
				glVertex3f(0,         0,			0);
				glVertex3f(textWidth, 0,			0);
				glVertex3f(textWidth, textHeight,	0);
				glVertex3f(0,		  textHeight,	0);
				glVertex3f(0,         0,			0);
			glEnd();
		}
		  
		else if (background==4) {			// 4: SHADOW  (broken)
			rc.color = colorBlack;
			for (int i=0; i<4; i++) {
				float shadow = 0.01;
				int xOff = shadow * (i&1? -1:1);
				int yOff = shadow * (i&2? -1:1);

				glRasterPos3f(xOff, yOff, 0);	// Capture this 3D point as the the 2D screen position
				for (const char *s =str.C; s[0]; s++)
					glutBitmapCharacter(font.font, *s);
			}
		}
		else panic(@"");

	glPopMatrix();
}

//void billboardCheatSphericalBegin() {
//
//	// get the current modelview matrix
//	float modelview[16];
//	glGetFloatv(GL_MODELVIEW_MATRIX , modelview);
//
//	// undo all rotations
//	// beware all scaling is lost as well 
//	for(int i=0; i<3; i++ )
//	    for(int j=0; j<3; j++ ) {
//		if ( i==j )
//		    modelview[i*4+j] = 1.0;
//		else
//		    modelview[i*4+j] = 0.0;
//	  }
//
//	// set the modelview with no rotations
//	glLoadMatrixf(modelview);
//}
//void billboardEnd() {
//
//	// restore the previously 
//	// stored modelview matrix
//	glPopMatrix();
//}



void myGLCable(Vector3f &p0, Vector3f &p1, float diam) {
	glPushMatrix();
		diam = fabs(diam);
		if (diam > 0.1) {			// THICK -- use scaled box
			 // assume p0 and p1 are vertical (in Y) for now.
			float len = p1.y - p0.y;
			glTranslatef(p0.x, p0.y + len/2, p0.z);		// to center of line
			glScalef(diam, len, diam);
			glutSolidCube(1.0);
		}
		else if (diam > 0.0001) {
			glBegin(GL_LINES);		// THIN -- use ogl line
				glVertex3fv(p0);
				glVertex3fv(p1);
			glEnd();
		}
		else
			;						// ZERO thickness -- don't draw anything
	glPopMatrix();
}

void drawAxis (RenderingContext *rc, float length, NSArray *labels, float ticSpacing, int axisIndex) {
	int axis = axisIndex/2, sign=axisIndex&1? -1: 1;
	#define axisPt(ax, len) Vector3f(ax==0?len:0, ax==1?len:0, ax==2?len:0)
	Vector3f endPoint = axisPt(axis, length) * sign;

	glBegin(GL_LINES);
		glVertex3fv(zeroVector3f);
		glVertex3fv(endPoint);
	glEnd();

	Matrix3f nowOgl = getModelViewMatrix().getRotation();
	//id foo = pp(nowOgl);
	float det = abs(nowOgl.det());					assert(det==det, (@"nan"));
	float len = pow(det, 0.333333);//0.333333//0.5//0.6666666

	for (float tickHeigh=ticSpacing; tickHeigh<length; tickHeigh+=ticSpacing) {
		Vector3f tick = axisPt(axis, tickHeigh) * sign;
		glPushMatrix();
			glTranslatefv(-tick);
			rc.color = colorRed;
			myGlJack3(rc, 0.5);					// tick cross
//			glutSolidSphere(0.666/len, 4, 4);		// tick mark
		glPopMatrix();
	}

	if (labels) {
		glPushMatrix();
			glTranslatefv(-endPoint);
			myGlDrawString(rc, labels[axisIndex], -1);
		glPopMatrix();
	}
}

 // 3 lines, along each axis
void myGlJack3(RenderingContext *rc, float radius) {
	glBegin(GL_LINES);
		glVertex3f(-radius, 0, 0);		glVertex3f(radius, 0, 0);
		glVertex3f(0, -radius, 0);		glVertex3f(0, radius, 0);
		glVertex3f(0, 0, -radius);		glVertex3f(0, 0, radius);
	glEnd();
}

 // 4 lines to all opposing vertices
void myGlJack4(RenderingContext *rc, float r) {
	glBegin(GL_LINES);
		glVertex3f(-r, -r, -r);			glVertex3f( r,  r,  r);
		glVertex3f( r, -r, -r);			glVertex3f(-r,  r,  r);
		glVertex3f( r,  r, -r);			glVertex3f(-r, -r,  r);
		glVertex3f(-r,  r, -r);			glVertex3f( r, -r,  r);
	glEnd();
}

 // in XY plane, nSlices: number of longitudinal (vetical) AROUND the Z axis
void myGlWireRing(GLdouble radius, GLint nSlices) {
	glBegin(GL_LINE_LOOP);
		for (int lon=0; lon<nSlices; lon++)  {
			double lo = 2.*M_PI*lon/nSlices;
			glVertex3f(cos(lo)*radius, sin(lo)*radius, 0);
		}
	glEnd();
}

void myGlSolidDisc(GLdouble radius, GLint slices) {
	myGlSolidCylinder(radius, 0, slices, 1);
}

void myGlSolidCylinder(GLdouble radius, GLdouble height, GLint slices, GLint stacks) {
	myGlSolidCylinderCommon(radius, height, slices, stacks, 1);
}
void myGlEndlessSolidCylinder(GLdouble radius, GLdouble height, GLint slices, GLint stacks) {
	myGlSolidCylinderCommon(radius, height, slices, stacks, 0);
}

void myGlSolidCylinderCommon(GLdouble radius, GLdouble height, GLint slices, GLint stacks, bool ends) {
	// from http://vis.uky.edu/~zhangqing/ray_trace/src/geometry.cpp
	
	// cylinder extends from -height/2 to height/2 in the Z axis
	
	char heightZero = height < epsilon;
	height = height<epsilon? epsilon: height;
	
	double angle = 2 * M_PI / (double)slices;
	double *sint = (double *)calloc( sizeof(double), slices+1);
	double *cost = (double *)calloc( sizeof(double), slices+1);
	assert(sint and cost, (@"allocation problem"));
	
	for(int i = 0; i < slices+1; i++ ) {
		sint[i] = sin(angle * i);
		cost[i] = cos(angle * i);
	}
	
	if (ends) {
		/* Cover the base and top */
		glBegin( GL_TRIANGLE_FAN );
		glNormal3d( 0.0, 0.0, -1.0 );
		glVertex3d( 0.0, 0.0, -height/2 );
		for(int j = 0; j <= slices; j++ )
			glVertex3d(cost[j]*radius, sint[j]*radius, -height/2);
		glEnd();
		
		glBegin( GL_TRIANGLE_FAN );
		glNormal3d( 0.0, 0.0, 1.0);
		glVertex3d( 0.0, 0.0, height/2);
		for(int j = slices; j >= 0; j-- )
			glVertex3d(cost[j]*radius, sint[j]*radius, height/2);
		glEnd( );
	}
	
	/* Step in z and radius as stacks are drawn. */
	if (!heightZero) {
		const double zStep = height / stacks;
		double z0=-height/2, z1=zStep;
		for(int i=1; i<=stacks; i++)
		  {
			if(i == stacks )
				z1 = height/2;
			
			glBegin( GL_QUAD_STRIP );
			for(int j = 0; j <= slices; j++ )
			  {
				glNormal3d(cost[j],        sint[j],       0.0);
				glVertex3d(cost[j]*radius, sint[j]*radius, z0);
				glVertex3d(cost[j]*radius, sint[j]*radius, z1);
			}
			glEnd( );
			
			z0 = z1;
			z1 += zStep;
		}
	}
	free( sint );
	free( cost );
	return;
	// Two other ways, not working (yet)
//	glutSolidCylinder (GLdouble radius, GLdouble heights, GLint slices, GLint stacks);
 //	gluSolidCylinder();
//	GLUquadricObj *qobj = gluNewQuadric();	// a parameter to gluCylinder
 //	gluQuadricDrawStyle(qobj, GLU_FILL);	// fill in the polygons
 //	gluQuadricNormals(qobj, GLU_SMOOTH);	// normals for each vertex
 //	gluCylinder(qobj, radius, radius, height, 15, 5); /* draws the actual cylinder */
}

void myGlCube3f(GLfloat width, GLfloat height, GLfloat depth) {
	static GLfloat myNormal[6][3] =   {  /* Normals for the 6 faces of a cube. */
		  {-1.0,  0.0,  0.0},
		  { 0.0,  1.0,  0.0},
		  { 1.0,  0.0,  0.0},
		  { 0.0, -1.0,  0.0},
		  { 0.0,  0.0,  1.0},
		  { 0.0,  0.0, -1.0},
	};
	static GLint myFaces[6][4] =   {  // Vertex indices for the 6 faces of the cube
		  {0, 1, 2, 3},
		  {3, 2, 6, 7},
		  {7, 6, 5, 4},
		  {4, 5, 1, 0},
		  {5, 6, 2, 1},
		  {7, 4, 0, 3} };

	GLfloat vert[8][3] =   {		// 8 3D points of vertices
		  {-width, -height,  depth},
		  {-width, -height, -depth},
		  {-width,  height, -depth},
		  {-width,  height,  depth},
		  { width, -height,  depth},
		  { width, -height, -depth},
		  { width,  height, -depth},
		  { width,  height,  depth},
	};
	
	for (int i = 0; i < 6; i++)   {
		glBegin(GL_QUADS);
		glNormal3fv( &myNormal[i][0]);
		glVertex3fv( &vert [myFaces[i][0]] [0]);
		glVertex3fv( &vert [myFaces[i][1]] [0]);
		glVertex3fv( &vert [myFaces[i][2]] [0]);
		glVertex3fv( &vert [myFaces[i][3]] [0]);
		glEnd();
	}
}

// n360 is the number of points in a full buzel (360 degrees)
// Hungarian: Top, Bottom

void myGlBezeledWedge3f(int n360, float height, bool ends,
		Vector3f &tSize_, float tRadius,
		Vector3f &bSize_, float bRadius) {

	Vector3f tSize = tSize_ - Vector3f(tRadius, 0, tRadius);
	Vector3f bSize = bSize_ - Vector3f(bRadius, 0, bRadius);

	 // Get stack storage for Top and Bottom shapes. Both have n360 Vector3f's
	assert(n360 != 0, (@"oops"));
	Vector3f *top	= (Vector3f *)alloca(n360 * sizeof(Vector3f));
	Vector3f *bottom= (Vector3f *)alloca(n360 * sizeof(Vector3f));

	float mx=1, mz=-1, swap;
	for (int i=0; i<n360; i++) {
		if ((i*4)%n360 == 0) {			// every 90 degrees
			swap=mx; mx=-mz; mz=swap;		// rotate mx and mz by 90deg
		}
		float theta = 2 * M_PI * (i+0.5) / n360;		// N.B: 0.5
		float sint	= sin(theta), cost = cos(theta);
		top[i]		= Vector3f( mx*tSize.x + tRadius*cost,  height,  mz*tSize.z + tRadius*sint);
		bottom[i]	= Vector3f( mx*bSize.x + bRadius*cost,  0,       mz*bSize.z + bRadius*sint);
		int j=33;
	}

	 // draw sides
	myGlTaperedExtrusion3f(n360, top, bottom);

	if (ends) {
		 // draw top
		glBegin(GL_TRIANGLE_FAN);
		glNormal3f( 0, -1, 0);
		for (int i=0; i<n360; i++)
			glVertex3fv( top[i] );
		glEnd();
	}
}

void myGlTaperedExtrusion3f(GLint nPoints, Vector3f *top, Vector3f *bottom) {

	glBegin(GL_QUAD_STRIP);
	for (int i=0; i<=nPoints; i++)   {
		int iPrev = (i - 1 + nPoints) % nPoints;
		int iNow  =  i				  % nPoints;
		int iNext = (i + 1)			  % nPoints;

		 // normal is average of neighboring top's crossed with vector to bottom
		Vector3f topNorm = (top[iPrev] + top[iNext]) * (top[iNow] + bottom[iNow]);
		glNormal3fv(  -topNorm   );
		glVertex3fv(   top[iNow] );

		 // normal is average of neighboring bottom's crossed with vector to top
		Vector3f bottomNorm = (bottom[iPrev] + bottom[iNext]) * (bottom[iNow] + top[iNow]);
		glNormal3fv(  -bottomNorm   );
		glVertex3fv(   bottom[iNow] );

		iPrev = i;
	}
	glEnd();
}

void myGlXYSquare2f(GLfloat w, GLfloat h) {
	glBegin(GL_POLYGON);				// pic rectangles
	glVertex3f(-w/2, -h/2,   0);					// x-y
	glVertex3f( w/2, -h/2,   0);
	glVertex3f( w/2,  h/2,   0);
	glVertex3f(-w/2,  h/2,   0);
	glEnd();
}

void myGlChironFoo(GLfloat w, GLfloat h, GLfloat ch) {
	glBegin(GL_POLYGON);				// pic rectangles
	glVertex3f(  0,    0,   0);					//
	glVertex3f( -w,    0,   0);					//
	glVertex3f(-ch,  -ch,   0);					//
	glVertex3f(  0,   -h,   0);					//
	glEnd();
}


// from sphtri1.c Paul Heckbert	1997
//http://www.cs.cmu.edu/afs/andrew/scs/cs/15-463/2001/pub/src/a3/raymain/sphtri1.c

#define FRAND() ((rand()&32767)/32767.)

void myGlSolidHemisphere(GLdouble radius, GLdouble cut, GLint nSlices, GLint nStacks) {
	//	fraction: latitude the sphere stops.
	//	nSlices: number of longitudinal (vetical) AROUND the Z axis
	//	nStacks: number of latitudinal (horizontal) subdivisions ALONG the Z axis.
	
	int lastLat=nStacks/2*cut;
    for (int lat=0; lat<lastLat; lat++) { 		// move from the North Pole to South
		glBegin(GL_TRIANGLE_STRIP);
		for (int lon=0; lon<=nSlices; lon++) {
			//glColor3f(FRAND(), FRAND(), FRAND());
			
			double la = 2.*M_PI*lat/nStacks;
			double lo = 2.*M_PI*lon/nSlices;
			float v1[3] =   {cos(lo)*sin(la)*radius, sin(lo)*sin(la)*radius, cos(la)*radius};
			glNormal3fv(v1);
			glVertex3fv(v1);
			
			la = 2.*M_PI*(lat+1)/nStacks;
			float v2[3] =   {cos(lo)*sin(la)*radius, sin(lo)*sin(la)*radius, cos(la)*radius};
			glNormal3fv(v2);
			glVertex3fv(v2);
			
		}
		glEnd();
    }
	//	last
	//	float z0 = cos(2.*M_PI*lastLat/nStacks)*radius
	//	la
	//
	//	glBegin( GL_TRIANGLE_FAN );
	//	glNormal3d( 0.0, 0.0, -1.0 );
	//	glVertex3d( 0.0, 0.0, -height/2 );
	//	for(int j = 0; j <= slices; j++ )
	//		glVertex3d(cost[j]*radius, sint[j]*radius, -height/2);
	//	glEnd();
}

//// wire tent
//		float causeSize=0.05, m=causeSize;
//		float dyTop = v.size.y;
//		[rc renderGlColor:colorTurtle];
//		float dx = v.size.x, dy = v.size.y, dz = v.size.z;
//		float v[][3] =   {// vertices of tent: i1  i2  i3
//			  {-dx,-dy+causeSize*2,-dz},		//0 *   *   *
//			  {-dx, dyTop,	  	 -dz*m},		//1 |*  |*  *
//			  { dx,-dy+causeSize*2,-dz},		//2 ||* *|   *
//			  { dx, dyTop,		 -dz*m},		//3 |||* *   *
//			  {-dx,-dy+causeSize*2, dz},		//4 *|||  *   *
//			  {-dx, dyTop,		  dz*m},		//5  *||  |*  *
//			  { dx,-dy+causeSize*2, dz},		//6   *|  *|   *
//			  { dx, dyTop,		  dz*m},		//7    *   *   *
//		};									//   z   x   y
//		glBegin(GL_LINES);
//		for (int i1=0; i1<4; i1++)   {
//			int i2=i1+(i1>=2)*2;
//			int i3 = 2*i1;
//			glVertex3fv(v[i1]);				// i1
//			glVertex3fv(v[i1+4]);
//			glVertex3fv(v[i2]);
//			glVertex3fv(v[i2+2]);
//			glVertex3fv(v[i3]);
//			glVertex3fv(v[i3+1]);
//		}
//		glEnd();

///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

 // Convenience method
void glTranslatefv(Vector3f translate) {
	glTranslatef(translate.x, translate.y, translate.z);
}
void glScalefv(Vector3f scale) {
	glScalef(scale.x, scale.y, scale.z);
}

//void printTranslation()		  {	NSPrintf(@"%@\n", strModelView_curWorldPosn());			}


Matrix4f getProjectionMatrix() {
	Matrix4f rv;
	glMatrixMode(GL_PROJECTION);//==========================================
	glGetFloatv(GL_MODELVIEW_MATRIX, rv.data);
	glMatrixMode(GL_MODELVIEW); //==========================================
	return rv;
}

Matrix4f getModelViewMatrix() {
	Matrix4f rv;
	glGetFloatv(GL_MODELVIEW_MATRIX, rv.data);
	return rv;
}

Vector3f getModelViewMatrixTranslation() {
	float rv[16];
	glGetFloatv(GL_MODELVIEW_MATRIX, rv);
	return Vector3f(rv[12], rv[13], rv[14]);	///12 = 3*4+0 = &at(3,0)
}

Vector3f getRasterPosition() {
	Vector3f rv;
	glGetFloatv(GL_CURRENT_RASTER_POSITION, rv);
	return rv;
}

//NSString *strModelView_curWorldPosn() {
//	Vector3f v = getModelViewMatrixTranslation();
//	return [NSString stringWithFormat:@"(%.4f %.4f %.4f)", v.x, v.y, v.z];
//}
void ogl() {
	Matrix4f wm = getModelViewMatrix();
	NSString *str = pp(wm);
	NSPrintf(@"%@\n", str);
//	for (int i=0; i<4; i++)
//		printf("%10.3f %10.3f %10.3f %10.3f\n", wm.at(i,0), wm.at(i,1), wm.at(i,2), wm.at(i,3));
}
