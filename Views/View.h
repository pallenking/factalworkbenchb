//  View.h -- a tree of PartViews per NSView C2013PAK

/*! 
    Each View has a unique user, and thus just one unique superView

    Capabilities of View:
        1. construction/maintence, both linear and recursive
        2. construction from a part
        3. bound and place
        4. 3D rendering with OpenGL
        5. printout for debugger
 
 
 // ATOMIC: Draw small shape, approx (1.0^3): (T:Cube, F:Sphere, B:Cylinder)
 */

#import "HnwObject.h"
#import "Part.h"
//@class Part;
@class RenderingContext;

#import "SystemTypes.h"
#import "vmath.hpp"


@interface View: HnwObject
     // Views are in a tree. Only one usage (parent/superview) per tree
	@property (nonatomic, assign) View		*superView;		// one parent
    @property (nonatomic, retain) NSMutableArray *subViews; // many children

	@property (nonatomic, retain) Part		*part;			// that this view represents

	  // EXTERNAL:
	 // position of self, in superview
    @property (nonatomic, assign) Matrix4f	position;        // this view's origin in superView

	   // INTERNAL
      // All of the content displayed exists within the bounds box.
     // The bounds of * below are in the local coordinate system
    @property (nonatomic, assign) Bounds3f	bounds;          // smaller parts


     // part properties relating to this view: They are of this Part's detail
    @property (nonatomic, assign) DisplayMode displayMode;	// the kind of the shape to be drawn (NEW WAY)

    @property (nonatomic, assign) unsigned char heightLeaf; // height in current tree
		// Brain has heightLeaf=0, first branch=1, ...
    @property (nonatomic, assign) unsigned char heightTree; // max height of whole tree

    @property (nonatomic, assign) char		isSelected;

	@property (nonatomic, assign) bool		boundsDirty;	// Shape of Views must be adjusted

- (Vector3f)	positionTranslation;
- (void)		setPositionTranslation:(Vector3f)translation;
- (Matrix3f)	positionRotation;
- (void)		setPositionRotation:(Matrix3f)rotation;

#pragma mark 1. Basic Object Level
 //#pragma mark - 1-level:
 //////////////////////////////////////////////////////////////////////////////
/////////////////////////////  NODE ACCESS  //////////////////////////////////
- (View *)		addView:(View *)v;
- (View *)		subViewFor:(Part *)m;

 // View Predicates, which determine how the view is handled
- (bool)		isAtomic;
- (bool)		is1stView;

#pragma mark 3. Deep Access
 //////////////////////////////////////////////////////////////////////////////
/////////////////////////////  TREE ACCESS  //////////////////////////////////
- (View *)		rootFwV;
- (View *)		superViewForPart:(Part *)part;
- (View *)		rootOfTreeOfMyKind;

- (View *)		searchInViews4Part:(Part *)part;
- (id)			searchOut4Part:(Part *)part except:(View *)alreadySearched;
- (void)		normalizeTreesHeights;

#pragma mark transformations
- (Vector3f)	originInView:(View *)v;
- (Vector3f)	convertSpot:    (Vector3f)aSpot toSuperview:(View *)refView;
- (Matrix4f)	convertPosition:(Matrix4f)aView toSuperview:(View *)refView;

- (Matrix4f)	xformToView:	 (View *)refView;
- (Matrix4f)	xformToSuperview:(View *)refView;	// refView must be superview of self

//- (Bounds3f)	boundsToSuperview;

#pragma mark 9. 3D Support
- (void)		placeLines;		// (in the full View tree)
- (void)		moveSoNoOverlapping;
- (void)		includeBoundsInSuperview;

#pragma mark 11. 3D Display
- (void)        setNeedsDisplay:(bool)val;
- (void)		renderWith:(RenderingContext *)rc;

#pragma mark 15. PrettyPrint
 ///\\//\\//\\//\\//\\//\\//\\// PRINTOUT //\\//\\//\\//\\//\\//\\//\\//\\//
- (NSString *)	ppNameNPart;
- (const char*) ppNameNPartC;
- (NSString *)	ppPosition:aux;

@end

