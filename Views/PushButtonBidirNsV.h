// PushButtonBidirNsV.h -- Bidirectional Button  C2015PAK

#import "BidirNsV.h"
@class Port, InspecVc, GenAtom, BundleTap;


@interface PushButtonBidirNsV : BidirNsV

//    @property (nonatomic, retain)  		Port 	*genPort; // direct drive
    @property (nonatomic, retain) IBOutlet 	NSString *sound;

	 // This was put in about 4/5/16, while working on Turtle34. Not sure why
	@property (nonatomic, assign)/*IBOutlet*/char	retract;

	 // weed out multiple UP's (Morse)BidirSliders
	@property (nonatomic, assign)			char	wasPressedOnce;

- (IBAction)	buttonTapped:(id)sender;
- (NSString *)	name;

@end
