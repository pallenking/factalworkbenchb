//  LinkView.mm -- View a particular link C2013PAK

#import "Link.h"
#import "LinkView.h"

#import "Common.h"
#import "NSCommon.h"
#import "Id+Info.h"
#import "vmath.hpp"

@implementation LinkView

#pragma mark 1. Basic Object Level
- init  {
	self = [super init];
	self.other1View = nil;
    self.other0View = nil;
	
	// Help: Think about unifying with relaySpots
	self.other1Loc = zeroVector3f;
    self.other0Loc = zeroVector3f;
	return self;
}

- (void) dealloc; {
	
	self.other1View		= nil;
	self.other0View		= nil;

	[super dealloc];
}

#pragma mark 9. 3D Support
// \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\   //
//  \\ \\ \\ \\ \\ \\ \\ \\ \ 3D Support\\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\  //
//   \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ //
- (void)normalizeTreesHeights;{												}

- (void)reBoundSchema;	{	/* Links have no bounds to set */				}
- (void)placeLines;		{	[(Link *)self.part adjustLinksPlaceInView:self]; }

 // might enhance:
- (View *) searchInViews4Part:(Part *)m; {		return nil;					}

#pragma mark 11. 3D Display
//// // // // // // // // // // // // // // // // // // // // // // // ///
/// // // // // // // // // // 3D Display  // // // // // // // // // ////
// // // // // // // // // // // // // // // // // // // // // // // // //
- (void)renderWith:(RenderingContext *)rc;
						{	[(Link*)self.part renderLinkInView:self with:rc];}

#pragma mark 15. PrettyPrint
- (NSString *) ppPosition:aux; {
	return [@"" addF:@"line w=%@%@, b=%@%@",
				self.other0View.part.fullName16,	pp(self.other0Loc),
				self.other1View.part.fullName16,	pp(self.other1Loc)];
}

guiVector3fAccessors4(other1Loc, Other1Loc, _other1Loc);
guiVector3fAccessors4(other0Loc, Other0Loc, _other0Loc);

@end
