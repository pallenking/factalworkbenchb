//  GlCommon.h -- a collection of OpenGL helpers C2013PAK

#import "vmath.hpp"
@class RenderingContext;
// Extend the names in gl*:

void myGlDrawString(	RenderingContext *rc,
		NSString *str,
		int fontNumber,									// -1 = use brain.fontNumber
		Vector3f inCameraShift	= zeroVector3f,
		Vector2f spotPercent	= Vector2f(0.5, 0.5),	// center of text billboard
		char background			= 0);					// 0  ==> no background

void myGLCable(Vector3f &p0, Vector3f &p1, float diam);

void myGlJack3(RenderingContext *rc, float length);
void myGlJack4(RenderingContext *rc, float length);
void drawAxis  (RenderingContext *rc, float length, NSArray *labels,   float ticSpacing, int axisIndex);

void myGlWireRing(GLdouble radius, GLint nSlices);
void myGlSolidDisc(GLdouble radius, GLint slices);
void myGlSolidCylinder(GLdouble radius, GLdouble heights, GLint slices, GLint stacks);
void myGlSolidCylinderCommon(GLdouble radius, GLdouble height, GLint slices, GLint stacks, bool ends);
void myGlEndlessSolidCylinder(GLdouble radius, GLdouble height, GLint slices, GLint stacks);
void myGlCube3f(GLfloat width, GLfloat height, GLfloat depth);
void myGlBezeledWedge3f(int n360, float height, bool ends, Vector3f &tSize, float tRadius, Vector3f &bSize, float bRadius);
void myGlTaperedExtrusion3f(GLint nPoints, Vector3f *top, Vector3f *bottom);
void myGlXYSquare2f(GLfloat width, GLfloat height);
void myGlChironFoo(GLfloat w, GLfloat h, GLfloat e);
void myGlSolidHemisphere(GLdouble radius, GLdouble cut, GLint slices, GLint stacks);

void glTranslatefv(Vector3f translate);
void glScalefv	  (Vector3f scale);
//void printTranslation();

Matrix4f getProjectionMatrix();
Matrix4f getModelViewMatrix();
Vector3f getModelViewMatrixTranslation();
Vector3f getRasterPosition();
NSString *strModelView_curWorldPosn();

void ogl();                     // print model view


// Don't forget:
//	void 	glutSolidCone (GLdouble base, GLdouble height, GLint slices, GLint stacks)
//	void 	glutSolidCube (GLdouble width)
//	void 	glutSolidCylinder (GLdouble radius, GLdouble height, GLint slices, GLint stacks)
//	void 	glutSolidDodecahedron (void)
//	void 	glutSolidIcosahedron (void)
//	void 	glutSolidOctahedron (void)
//	void 	glutSolidRhombicDodecahedron (void)
//	void 	glutSolidSierpinskiSponge (int num_levels, const GLdouble offset[3], GLdouble scale)
//	void 	glutSolidSphere (GLdouble radius, GLint slices, GLint stacks)
//	void 	glutSolidTeapot (GLdouble size)
//	void 	glutSolidTetrahedron (void)
//	void 	glutSolidTorus (GLdouble dInnerRadius, GLdouble dOuterRadius, GLint nSides, GLint nRings)
//	void 	glutWireCone (GLdouble base, GLdouble height, GLint slices, GLint stacks)
//	void 	glutWireCube (GLdouble width)
//	void 	glutWireCylinder (GLdouble radius, GLdouble height, GLint slices, GLint stacks)
//	void 	glutWireDodecahedron (void)
//	void 	glutWireIcosahedron (void)
//	void 	glutWireOctahedron (void)
//	void 	glutWireRhombicDodecahedron (void)
//	void 	glutWireSierpinskiSponge (int num_levels, const GLdouble offset[3], GLdouble scale)
//	void 	glutWireSphere (GLdouble radius, GLint slices, GLint stacks)
//	void 	glutWireTeapot (GLdouble size)
//	void 	glutWireTetrahedron (void)
//	void 	glutWireTorus (GLdouble dInnerRadius, GLdouble dOuterRadius, GLint nSides, GLint nRings)
