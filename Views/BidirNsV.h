// BidirNsV.h -- an abstract NSView class for bidirectional Gui widgets C2015PAK

#import <Cocoa/Cocoa.h>
#import "SystemTypes.h"
//#import "HnwObject.h"

@class Port, InspecVc, Bundle;

@interface BidirNsV : NSView

	 /// These properties are filled in by IB:
    @property (nonatomic, assignWeak) IBOutlet InspecVc *inspectNsVc;// Our View Controller 

	 /// Optional Characteristics, specified in IB
	@property (nonatomic, retain) IBOutlet NSString	*title;		// of BidirNsV
	@property (nonatomic, retain) IBOutlet NSString	*clockEvent;// Clock to send
	@property (nonatomic, retain) IBOutlet NSString	*portPath;	// Port to link data
    @property (nonatomic,assign)/*IBOutlet*/bool	topSide;	// on top, opens down

    @property (nonatomic, assign)		bool		initialized;// Kludge

	 /// NS Gui Items:
	@property (nonatomic, retain)		NSTextField	*nameTextField;

	 /// Target Bundle and Port in HnW Nework:
	@property (nonatomic, retain)		Port		*targetPort;// where in HnW Network we are connected
	@property (nonatomic, retain)		Port		*outputToPort;

	   // Channel A: A Control (such as a NSSlider or NSButton)
	  //   which has a -floatValue method
	 // @0 <= outboundNsControl.value <= @1, bound to self.(topSide? havePort:wantPort).value
	@property (nonatomic, retain)		NSControl	*outboundNsControl;

	  // Channel B: Meter Level shown in View
	 // 0 <= inboundFloatValue <= 1,          bound to self.(topSide? wantPort:havePort).value
	@property (nonatomic, assign)		float		inboundFloatValue;


- (void) buildBidirViewWithTarg:(Bundle *)targBun;

@end
