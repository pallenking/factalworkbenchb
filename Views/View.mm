//  View.mm -- a tree of Views per part C2013PAK

/* to do:
 2. setBounds v.s. getBounds
 3. collisions between Bundles pessimistic.
 */

#import "View.h"

#import "Brain.h"
#import "Id+Info.h"
#import "Bundle.h"		// KROCK
#import "Leaf.h"		// KROCK
#import "Splitter.h"	// KROCK
#import "Link.h"		// KROCK
#import "Share.h"		// KROCK
#import "Label.h"		// KROCK
#import "Part.h"		// KROCK
#import "WriteHead.h"	// KROCK
#import "Branch.h"
#import "Actor.h"

#import "LinkView.h"	// KROCK

#import "Common.h"
#import "NSCommon.h"
#import "GlCommon.h"
#import "vmath.hpp"
#include <stdlib.h>
#include <sys/time.h>
#import <GLUT/glut.h>

@implementation View

#pragma mark 1. Basic Object Level

- init; {									self = [super init];

	self.position		= Matrix4f();
	self.part			= nil;
    self.heightLeaf		= 0;
    self.heightTree		= 0;
	
	self.bounds			= nanBounds3f;		// start with empty bounds
	self.subViews		= [NSMutableArray array];
	self.superView		= nil;
	self.isSelected		= 0;
	self.displayMode	= displayOpen;

	return self;
}

- (void) dealloc {
	self.superView		= nil;
	self.subViews		= nil;
	self.part			= nil;

	[super dealloc];
}


 // wrapper to avoid a bug: if an OBJC variable contains a C++ object
//    Here self.position (in an OBJC object) has a variable position (a C++ Matrix4f)
- (Vector3f) positionTranslation;  {  return self.position.getTranslation();}
- (void) setPositionTranslation:(Vector3f)translation; {
	Matrix4f pos = self.position;		// pos is not in an OBJC object
	pos.setTranslation(translation);
	self.position = pos;
}
- (Matrix3f) positionRotation;  {  return self.position.getRotation();}
- (void) setPositionRotation:(Matrix3f)rotation; {
	Matrix4f pos = self.position;		// pos is not in an OBJC object
	pos.setRotation(rotation);
	self.position = pos;
}

 //////////////////////////////////////////////////////////////////////
//////////////////////////  NODE ACCESS  /////////////////////////////

- (View *) addView:(View *)v; {
	[self.subViews addObject:v];
	v.superView = self;	// set up return link
	return v;
}

- (View *) subViewFor:(Part *)m; {
	for (View *sv in self.subViews)
		if (sv.part == m)
			return sv;			// found subview for m
	return nil;				// no subview found
}

 /// View Status:
 /*
 isAtomic;
	self.part.brain.displayAsNonatomic	--> FALSE	// GUI force all to be atomic
	self.displayMode & displayAtomic	--> TRUE	// FORCE to atom
	self.displayMode & displayOpen		--> FALSE	// FORCE to be open
 */

- (bool) isAtomic; {

	 // GUI force to be open?
	Part *p = self.part;
	if (p.brain.displayAsNonatomic)
		return false;

	 // Branch's Actor call for atomic?
	if (coerceTo(Branch, p) or			// we are Branch
		coerceTo(Leaf, p)) {			// we are Leaf
//		coerceTo(Leaf, p.parent)*/) {		// our parent is a Leaf
			
		if (Actor *a = [p.parent enclosedByClass:@"Actor"])
			if (a.viewAsAtom)						// wants us to be atom
				return true;
	}

	char s = self.displayMode;
	if (s & displayAtomic)		// FORCE to atom
		return true;
	if (s & displayOpen)		// FORCE to be open
		return false;
	return false;
}
- (bool) is1stView; {
	return self.superView.part != self.part;
}

#pragma mark 3. Deep Access
 /////////////////////////////////////////////////////////////////////
/////////////////////////////  TREE ACCESS  /////////////////////////

- (id) deepMutableCopy; {				id rv = [super deepMutableCopy];
	panic(@"");
	return rv;
}

- (View *) rootFwV; {
	int loopDet = 1000;			// (defensive programming)
	View *rv = self;
	while (rv.superView)   {
		rv = rv.superView;
		assert(loopDet-- >0, (@"Tree %@ shouldn't be that deep", self.name));
	}
	return rv;
}

- (View *) superViewForPart:(Part *)part; {
	for (View *rv=self; rv; rv=rv.superView)
		if (rv.part == part)
			return rv;
	return nil;
}

- (View *) rootOfTreeOfMyKind; {
	if ([self.superView.part isKindOfClass:[self.part class]])
		return [self.superView rootOfTreeOfMyKind];
	return self;
}

 // Search my subtree for a View that of part me
- (View *) searchInViews4Part:(Part *)part; {
	if (part) {
		if (self.part == part)
			return self;
		for (View *sv in self.subViews)
XR			if (View *rv = [sv searchInViews4Part:part])
				return rv;
	}
	return nil;						// not in me or any of my descendents
}
- (id) searchOut4Part:(Part *)part except:(View *)alreadySearched; {

	if (id rv = [self searchInViews4Part:part])
		return rv;						// found inside of me

	  // GO WIDE, do the same up the tree (if superview exists):
	 //
	return [self.superView searchOut4Part:part except:self];
}

 // Trees are subsets of the current View of the same "kind":
- (void) normalizeTreesHeights {
	Part *sm = self.part;
	View *sv = self.superView;

	self.heightLeaf = self.heightTree = 0;
	bool innerBundle = coerceTo(Bundle, sm) and coerceTo(Bundle, sv.part);
	if (innerBundle) {
        self.heightLeaf = sv.heightLeaf+1;			// my height is one bigger
		self.heightTree = sv.heightLeaf+1;
		assert(self.heightTree < 255, (@"implementation limit"));
	}
	
    if (![self isAtomic])
        for (View *sv in self.subViews)
XR			[sv normalizeTreesHeights];

	if (innerBundle)				// if (self.part.class == sv.part.class)
        sv.heightTree = max(sv.heightTree, self.heightTree);
}

#pragma mark transformations

static int debugOriginInRoot=0;
- (Vector3f) originInView:(View *)v; {
	if (self==v)
		return zeroVector3f;	// tautology: our origin is at 0,0,0
	
	View *sv = self.superView;
	assert(sv, (@"shouldn't happen"));
	Vector3f shift2supersOrigin = self.positionTranslation; // and back to my superView's origin
	
	if (debugOriginInRoot) {
		NSPrintf(@"%@ ", self.name);
		
		NSPrintf(@"%@", pp(self.positionTranslation));
		NSPrintf(@"%@%@\n", pp(self.bounds), sv? @"": @"\n");
	}
	
	Vector3f sOr = [sv originInView:v];  //#### RECURSE UP to trunk
	
	return sOr + shift2supersOrigin;
}

// HUNGARIAN: Spot - orientationless point; Position = spot + heading
- (Vector3f) convertSpot:(Vector3f)aSpot toSuperview:(View *)refView; {

	 // these  fail with 'cat dog owl': WriteHead or Splitter, perhaps more
	//assert(coerceTo(Port, self.part), (@"These comments presume self is Share, or at least a Port"));
	 // aSpot is in Share's coords

	 // convert aSpot (in Share's coords) to matrix form for convertPosition
	Matrix4f aMatrix = Matrix4f().createTranslation(aSpot.x,aSpot.y,aSpot.z);
XX	Matrix4f rv = [self convertPosition:aMatrix toSuperview:refView];
	 // now rv in refView's coords

	return rv.getTranslation();
}

const int printConversion2 = 0;
- (Matrix4f) convertPosition:(Matrix4f)aMatrix toSuperview:(View *)refView; {
										// suspect spin?
	View *aView = self;
	if (printConversion2)
		NSPrintf(@"Attach position (Viewed in %@)  = %@\n", self.name, pp(aMatrix));

	while (aView!=refView and aView.superView) { // no match and E superView
		if (printConversion2)
			NSPrintf(@"           View %@'s position  =  %@\n", aView.name, pp(aView.position));
		aMatrix	= aView.position * aMatrix;

		aView	= aView.superView;	// go up
		if (printConversion2)
			NSPrintf(@"Attach position (Viewed in %@)  = %@\n", aView.name, pp(aMatrix));
	}
	if (printConversion2)
		printf("\n");
	
	if (aView!=refView) {	// refView not found.
		Matrix4f ref2root = [refView convertPosition:Matrix4f() toSuperview:aView];
		Matrix4f self2ref = ref2root.inverse() * aMatrix;
		return self2ref;
	}
	return aMatrix;
}

- (Matrix4f) xformToView:(View *)refView; {
	id rootFwV = self.rootFwV;
	Matrix4f self2root		= [self    xformToSuperview:rootFwV];
	Matrix4f refView2root	= [refView xformToSuperview:rootFwV];
	Matrix4f root2revView	= refView2root.inverse();
//	return root2revView * self2root;
	Matrix4f rv = self2root * root2revView;
	if (printConversion2)
		NSPrintf(@" %@ xformToView %@: %@\n", self.name, refView.name, pp(rv));
	return rv;
}

- (Matrix4f) xformToSuperview:(View *)refView; {
	Matrix4f rv;
	if (printConversion2)
		NSPrintf(@"   View '%@'s position in '%@'\n", self.name, refView.name);

	for (View *v=self; v != refView; ) {
		assert(v, (@"why?"));
		rv = v.position * rv;
		v  = v.superView;
		if (printConversion2)
			NSPrintf(@"      in '%@': %@\n", v.name, pp(rv));
	}
	return rv;
}

//- (Bounds3f) boundsToSuperview; {
//	Matrix4f selfPosn = self.position;
//	Vector3f p0 = selfPosn * self.bounds.point0();	// or self.bounds.point0 * selfPosn?
//	Vector3f p1 = selfPosn * self.bounds.point1();
//	Bounds3f rv(p0, p1);
//	return rv;
//}


#pragma mark 9. 3D Support
// \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\    //
//  \\ \\ \\ \\ \\ \\ \\ \\ \ 3D Support\\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\  //
//   \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\//
- (void) placeLines; {
	if ([self isAtomic])			// if CLOSED
		;								// explore no furthur
	else
		for (View *sv in self.subViews)
XR			[sv placeLines];
}


//////////////////////////////////////////////////////////////////////////////
    // SPOTS are areas which might overlap
   // knownSpots contain all the known spots
  //
 // They are of the following kinds
#define spotTypeKinds													\
	spotTypeKind(empty,			0),		/* nothing in entry			*/	\
	spotTypeKind(given,			2),		/* given/fixed, unknown at xxx*/\
	spotTypeKind(added,			3),		/* given/fixed, is not xxx	*/	\
	spotTypeKind(end,			-1),	/* end of active area		*/	\

//#define spotTypeKinds													\
//	spotTypeKind(given,			2),		/* given, already placed	*/	\
//	spotTypeKind(unexamined,	0),		/* has yet to be examined	*/	\
//	spotTypeKind(examined,		3),		/* has been examined		*/	\
//	spotTypeKind(invalid,		1),		/* removed from queue		*/	\
//	spotTypeKind(end,		   -1),		/* end of the knownSpots	*/	\
 // SpotTypes of the following kind
#define spotTypeKind(name, value)  state_##name =value
typedef enum : char {		spotTypeKinds}	SpotTypes;
 // SpotTypes print names
#undef spotTypeKind
#define spotTypeKind(name, value)  @value:@#name
id ppSpotTypes = @{		spotTypeKinds	};
  //
 // Everything that is known here about a spot
typedef struct {
	SpotTypes state;
	Bounds3f bounds;
//	float dist2self;
	View *view;									// origin of the spot
} SpotData;
  //
 // Define an ARRAY of SPOTS:
const int maxKnownSpots = 200;					// (static for now)
SpotData knownSpots[maxKnownSpots];
//
void pp(SpotData *val);
void pp(SpotData *knownSpots_) {
	for (SpotData *spot=knownSpots_; spot->state!=state_end and spot<&knownSpots_[maxKnownSpots]; spot++) {
		NSPrintf(@"%3d: ", (int)(spot-knownSpots_));
		pp(spot[0]);
	}
}
void pp(SpotData aSpot);
void pp(SpotData aSpot) {
	id vp = [@"" addN:13 F:@"%@", [aSpot.view ppNameNPart]];
	id vq = [@"" addN:10  F:@"%@", ppSpotTypes[[NSNumber numberWithInt:aSpot.state]]];
	NSPrintf(@"%@ %@ %@ Ctr=%@\n", vp, vq, pp(aSpot.bounds), pp3XZ(aSpot.bounds.center()));
}
	
//////////////////////////////////////////////////////////////////////////////
static float eps				= epsilon;
static char printOperations=0;					// for debug
#define debugLog(fmt...)				\
	if (printOperations and self.part.brain.logEvents) {\
		NSPrintf(fmt); }

/* ALGORITHM: Flood outward from self till a spot is found:

		KnownSpots contains all of the known spots at this level.
		It starts with the spot specified by self. It is set unspecified
		It loops till there are no unspecified knownSpots
			Pick the unspecified knownSpot that is closest to self
			If it overlaps, add the 4 spots around as unspecified knownSpots
			If no overlap, USE THIS SPOT
*/

- (void) moveSoNoOverlapping; {
	printOperations			= [self.name isEqualToString:self.part.brain.debugOverlapOf];
	printOperations			= 1;

	 // All computations are done in the superView

	 // Process self bounds
	Bounds3f selfBounds_pop = self.bounds * self.position;
	Vector3f selfSize_pop	= selfBounds_pop.size();
	Vector3f selfCenter_pop	= selfBounds_pop.center();
	debugLog (@"*** moveSoNoOverlapping in[%@]: %@ Ctr=%@\n", self.ppNameNPart,
						pp(selfBounds_pop), pp3XZ(selfBounds_pop.center()))

	 // Fill knownSpots[0...] with given spots
	int iSpot = 0;								// next free knownSpot

	for (View *v in self.superView.subViews) {

		 // ignore the special types of views:
		if (coerceTo(LinkView, v))
			continue;							// ignore Links
		if (coerceTo(Label, v.part))
			continue;							// ignore Labels
		if (v.bounds.length()<=epsilon)
			continue;							// ignore small bounds
		if (isNan(v.bounds))
			continue;							// ignore bad bounds
		if (v == self)
			break;								// ignore self and all that follow

		 // ignore siblings which have if no chance of collision from Y dimension
		Bounds3f vBounds = v.bounds * v.position;
		float ctrDistY =  vBounds.center().y - selfBounds_pop.center().y;	// between the 2
		float extentY  = (vBounds.size().y   + selfBounds_pop.size().y)/2;	// size
		if (extentY <= ctrDistY-eps || extentY <= -ctrDistY+eps)
			continue;							// no overlap

		 // ADD this spot. We must be sure not to bump into it.
		SpotData *spot = &knownSpots[iSpot++];
		assert(iSpot<maxKnownSpots, (@"make knownSpots bigger"));
		spot->state = state_given;				// don't bump into this
		spot->bounds = v.bounds * v.position;
		spot->view = v;

		debugLog(@"  Given %d[%@]: %@ C=%@", (int)(spot-knownSpots),
					spot->view.ppNameNPart, pp(spot->bounds), pp3XZ(spot->bounds.center()))
	}
	knownSpots[iSpot].state = state_end;		// mark end
	assert(iSpot<maxKnownSpots, (@"make knownSpots bigger"));

/* ALGORITHM: Flood outward from self till a spot is found:
 */

	// perhaps we don't overlap any given thing:
	SpotData *givenSpot=&knownSpots[0];	// start with self
	for (; givenSpot->state!=state_end; givenSpot++)
		if (givenSpot->state == state_given) {
			Bounds3f givenUnkBnds = givenSpot->bounds;
			if ((selfBounds_pop.x1 > givenUnkBnds.x0+eps and selfBounds_pop.x0 < givenUnkBnds.x1-eps and
				 selfBounds_pop.z1 > givenUnkBnds.z0+eps and selfBounds_pop.z0 < givenUnkBnds.z1-eps))
					break;		// overlap, kick out
		}
	if (givenSpot->state==state_end)
		return;								// NO OVERLAP AT ALL

	 // go through all given spots, enumerating the 4 points around each
	for (SpotData *givenSpot=&knownSpots[0]; givenSpot->state!=state_end; givenSpot++) {
		if (givenSpot->state!=state_given)
			continue;		// examine all given spots
		Bounds3f givenSpotBnds	= givenSpot->bounds;

		  // Go in all 4 directions from givenSpot. Either:
		 //
		Bounds3f newbieBnds		= selfBounds_pop; // newbie starts with self's size
		Vector3f size2 = (givenSpot->bounds.size() + selfBounds_pop.size())/2.0;
		for (int dir=0; dir<4; dir++) {
			Vector3f newbieCenter = givenSpotBnds.center();	// newbie starts at goodSpot center
			 // move to closest edge of my rectangle
			switch (dir) {
				case 0:		newbieCenter.x += size2.x;		break;
				case 1:		newbieCenter.z += size2.z;		break;
				case 2:		newbieCenter.x -= size2.x;		break;
				case 3:		newbieCenter.z -= size2.z;		break;
				default: panic(@"");				// newbie moves away
			}
			newbieBnds.setCenter(newbieCenter);		// move givenBounds to new center

			 // Does newbie overlap any given spot?
			SpotData *overlaps=&knownSpots[0];
			for (; overlaps->state!=state_end; overlaps++)
				if (overlaps->state==state_given) {
					Bounds3f overlapsBnds = overlaps->bounds;
					 // Does EXAMined spot OVERLAPs SPOT?
					if ((overlapsBnds.x1 > newbieBnds.x0+eps and overlapsBnds.x0 < newbieBnds.x1-eps and
						 overlapsBnds.z1 > newbieBnds.z0+eps and overlapsBnds.z0 < newbieBnds.z1-eps))
						break;			// YES, OVERLAP
				}
			 // If nothing overlaps
			if (overlaps->state==state_end) {

				 // ADD one of 4 SPOTs around placedSpot to knownSpots, to check later
				SpotData *newbie = &knownSpots[iSpot++];
				assert(iSpot+1<maxKnownSpots, (@"make knownSpots bigger"));
				debugLog(@"      Enque %d[]:%@ C=%@", (int)(newbie-knownSpots),
								pp(newbieBnds), pp3XZ(newbieBnds.center()));
				newbie->state		= state_added;
				newbie->bounds		= newbieBnds;		// as just computed
				newbie->view		= 0;				// not associated with a view
				newbie[1].state	= state_end;		// mark next as end
			}
		}
	}

	  // Find the closest of all potential spots.
	 //
	float closestDist = INFINITY;		//
	SpotData *closestSpot=0;
	for (SpotData *addedSpot=&knownSpots[0]; addedSpot->state!=state_end; addedSpot++) {
		if (addedSpot->state!=state_added)
			continue;						// ONLY ADDED SPOTS

		 // Compute min distance to self
		Vector3f d = addedSpot->bounds.center() - selfCenter_pop;;
		float dist2self = sqrt(d.x * d.x + d.z * d.z);
		if (closestDist > dist2self) {
			closestDist = dist2self;
			closestSpot = addedSpot;
		}
	}

	 // Move self by this amount
	Matrix4f posn = self.position;			// to be modified and returned
	Vector3f movedBy = closestSpot->bounds.center() - selfBounds_pop.center();
	movedBy.y = 0;		// HACK: keep same Y

	Vector3f newPosition = posn.getTranslation() + movedBy;
	debugLog(@"  Moved %@: %@ to %@", self.ppNameNPart, pp(movedBy), pp(newPosition));

	  // Objc++ bug: the following FAILS: self.position.setTranslation(newPosition);
	posn.setTranslation(newPosition);
	self.position = posn;			// move self to best place
}

- (bool) testTouching_ifsoMove:(View *)v; {
	float eps = epsilon;

	if (0) {
		if (coerceTo(LinkView, v))
			return FALSE;				// Links don't count
		if (coerceTo(Label, v.part))
			return FALSE;				// Labels don't count
		if (v.bounds.length()<=eps)
			return FALSE;				// tiny objects don't count
	}
	if (isNan(self.bounds))
		return FALSE;				// if no defined bounds, not touching

	assert(self.superView==v.superView, (@"assumed they must be in the same superView"));

	Bounds3f selfBounds(self.bounds * self.position);	// translate to superView
	Bounds3f    vBounds(   v.bounds *    v.position);	// (NB: should be pos*bounds)

	Vector3f a = selfBounds.point1() - vBounds.point0();// a = selfMax protrusion into vMin
	if (a.x<=eps or a.y<=eps or a.z<=eps)		// no part protrudes more than epsilon
		return FALSE;							// Totally above

	Vector3f b = vBounds.point1() - selfBounds.point0();// b: vMax protrudes into selfMin
	if (b.x<=eps or b.y<=eps or b.z<=eps)		// no part protrudes more than epsilon
		return FALSE;							// Totally below
	
	// INCLUSION!
	
	// find the dimension with minimal movement to clear
	float deltas[4] =   {a.x, a.z, b.x, b.z}; 	// y parts not allowed
	float dMin=HUGE_VALF;
	int iMin = -1;
	for (int i=0; i<4; i++) {
		if (dMin > deltas[i]) {
			dMin = deltas[i];
			iMin = i;
		}
	}
	float dx = iMin==0? -a.x :iMin==2? b.x :0;
	float dz = iMin==1? -a.z :iMin==3? b.z :0;
	Matrix4f moveMtx = Matrix4f().createTranslation(dx, 0, dz);
	self.position *= moveMtx;
	
	return TRUE;
}

- (void) includeBoundsInSuperview; {
//	[self flipBldr4x4_ok];

	if (View *papa = self.superView) {
		Vector3f selfBoundsMinInPop = self.position * self.bounds.point0();
		Vector3f rvMin = selfBoundsMinInPop, rvMax = rvMin;	// val 1 --> rvMin,rvMax
		
		Vector3f selfBoundsMaxInPop = self.position * self.bounds.point1();
		Vector3f bound[3] = {selfBoundsMaxInPop};			// val 2 --> bound[0]
		int n = 1;
		if (!isNan(papa.bounds)) {
			bound[n++] = papa.bounds.point0();				// papa bounds --> bound[1],[2]
			bound[n++] = papa.bounds.point1();
		}
		for (int i=0; i<n; i++) {							// max over bounds
			rvMin = minOf(rvMin, bound[i]);
			rvMax = maxOf(rvMax, bound[i]);
		}
		papa.bounds = Bounds3f(rvMin, rvMax);
	}
}

//#pragma mark 10. Placement
#pragma mark 11. 3D Display
//// // // // // // // // // // // // // // // // // // // // // // // ///
/// // // // // // // // // // 3D Display  // // // // // // // // // ////
// // // // // // // // // // // // // // // // // // // // // // // // //

- (void) setNeedsDisplay:(bool)val; {
	//self.brain.boundsDirty = true;
 }

 // draw part in this view
- (void) renderWith:(RenderingContext *)rc; {
	glPushMatrix();
		assert(![self.name isEqualToString:self.part.brain.breakAtRenderOf],
							(@"brain.breakAtRenderOf %@", self.name));
		 // 1. Register this view for picking
		[rc startDrawingView:self];
	
		 // 2. If we have a writeHead, animate the whole birth process
		if (WriteHead *writeHead = coerceTo(WriteHead, self.part.writeHead))
			if (!coerceTo(Port, self.part)) {		// Ports use writeHeads differently, to set link positions
				Vector3f delta = [writeHead animateBirthOfView:self];
				glTranslatefv(delta);
			 }

		 // 3. Translate from superView's coords to self's coords.
		glMultMatrixf(self.position);

		if (0 and self.part.brain.displayAxis) {		// Draw the axis
			rc.color = colorYellow;
			float r = self.part.brain.bitRadius;
			myGlJack3(rc, 1.5*r);		// tick at origin
		}

/**////////////////////////////////////////////////////////////////////////
/**/																		////
/**/	 //// 5, NOW IN self's COORDINATE SYSTEM, and READY TO DRAW:   ////
/**/	Part *p = self.part;												////
/**/	if (self.isAtomic)													////
/**/		[p drawAtomicInView:self context:rc];							////
/**/	else																////
/**/		[p     drawFullView:self context:rc];							////
/**/																		////
/**////////////////////////////////////////////////////////////////////////

		 // 6. cleanup from picking
		[rc finishDrawingView:self];	//// remove my part from drawing stack
		
	glPopMatrix();
}

#pragma mark 15. PrettyPrint
    ///\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\///
   ///\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\///
  ///\\//\\//\\//\\//\\//\\//\\// PRINTOUT //\\//\\//\\//\\//\\//\\//\\//\\///
 ///\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\///
///\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\///

- (NSString *) ppNameNPart; {	return [@"" addF:@"%@(%@)", self.name, self.part.name];	}
- (const char*)ppNameNPartC;{	return [self ppNameNPart].UTF8String;		}

- (NSString *) ppPart:aux; {
	id rv = @"";
	id oldIndent = aux[@"indent"]? aux[@"indent"]: @"";
	id indent	 = aux[@"indent"] = [oldIndent addF:@" "];

	 // a character (a..z) to represent the index in superView's subViews
	char ind = ' ';
	if (View *papaView = self.superView)
		ind = 'a'+ ([papaView.subViews indexOfObject:self] % 26);

	rv=[rv addF:@"%@%@%2ld%c  ", self.part.ppSuggestedPosition, indent, [indent length], ind];

	 // VIEW:
	rv=[rv addN:5 F:@"%@", self.name];

	 // MODEL name:
	if (Part *m = self.part)
		rv=[rv addN:self.part.brain.ppNameCols F:@"%@", m.name];
	else
		rv=[rv addN:self.part.brain.ppNameCols F:@"?"];

	rv=[rv addF:@"%@", [self ppPosition:aux]];
	 // this completes the 1-liner of Self

	if (!self.superView)
		rv=[rv addF:@" sV0"];

	 // Print all of the subViews:
	for (View *sv in self.subViews) {
		if (sv.superView != self)
			[[@"" addF:@"????? %@ has nil parent", sv.name] ppLog:nil];

		if (coerceTo(Port, sv.part) and !aux[@"ppPorts"])		// unwanted port
			;
		else if (coerceTo(Link, sv.part) and !aux[@"ppLinks"])	// unwanted link
			;
		else {
XX			id subVPpString = [sv ppPart:aux];	 // a line for each view
			rv=[rv addF:@"\n%@", subVPpString];
	}	}

	aux[@"indent"] = oldIndent;
	return rv;
}

- (NSString *) ppPosition:aux; {
	int nPpPosition = 22; //18
	 // POSN:		(e.g:  p=IY(0,20,0))
	id rv = [@"" addN:nPpPosition F:@"p=%@", pp(self.position)];

	//rv=[rv addF:@"%@", self.ppRetainCount];

	// OPTIONS:		(e.g: 0/0oO)
	char c = (self.displayMode & displayAtomic)?	'a':
			 (self.displayMode & displayOpen)?		'o':
													'-';
	if (coerceTo(Bundle, self.part))	// Bundles print height of leaf and tree
		rv=[rv addN:6 F:@"%d/%d", self.heightLeaf, self.heightTree];
	else
		rv=[rv addN:6 F:@""];			// Others
	rv=[rv addN:6 F:@"%c%@", c, [self isAtomic]? @"A":@"O"];

	 // ------- END STAGGARED INDENTS -------:
	int indentMore = self.part.brain.ppIndentCols - (int)[aux[@"indent"] length];
	if (indentMore > 0)
		rv=[rv addN:indentMore F:@""];

	 // BOUNDS:	(e.g. p-)
	rv=[rv addF:@"%@", pp(self.bounds)];

	return rv;
}

//- (void) linkerTest; {
//	float f = self.position22;
//	self.position22 = 345.6  + f*0;
//}
 // HELP: the above binds okay, but Error loading ViewInspector:
//		this class is not key value coding-compliant for the key position22.

//////////// GUI Interface Access methods:
guiMatrix4fAccessors4(position,		Position,		_position)
// cpp output expands to:
//- (float) position00 { return _position.at(0,0); } - (void) setPosition00:(float)val { _position.at(0,0) = val; }

- (void) marker3 {}

guiBounds3fAccessors4(bounds,		Bounds,			_bounds);
/*
- (float) boundsMinx { return _bounds.point0().x; } - (void) setBoundsMinx:(float)val { _bounds.point0().x = val; } 
- (float) boundsMiny { return _bounds.point0().y; } - (void) setBoundsMiny:(float)val { _bounds.point0().y = val; } 
- (float) boundsMinz { return _bounds.point0().z; } - (void) setBoundsMinz:(float)val { _bounds.point0().z = val; };
 */

//guiVector3fAccessors4(boundsMin,	BoundsMin,		_bounds.point0());
/*
- (float) boundsMinx { return _bounds.point0().x; } - (void) setBoundsMinx:(float)val { _bounds.point0().x = val; } 
- (float) boundsMiny { return _bounds.point0().y; } - (void) setBoundsMiny:(float)val { _bounds.point0().y = val; } 
- (float) boundsMinz { return _bounds.point0().z; } - (void) setBoundsMinz:(float)val { _bounds.point0().z = val; };
 */

//guiVector3fAccessors4(boundsMin,	BoundsMin,		_bounds.minPosn);
/*cpp output: No member named 'minPosn' in 'Bounds3<float>'
- (float) boundsMinx { return _bounds.minPosn.x; }
	- (void) setBoundsMinx:(float)val { _bounds.minPosn.x = val; } 
- (float) boundsMiny { return _bounds.minPosn.y; } 
	- (void) setBoundsMiny:(float)val { _bounds.minPosn.y = val; } 
- (float) boundsMinz { return _bounds.minPosn.z; } 
	- (void) setBoundsMinz:(float)val { _bounds.minPosn.z = val; };
 */

// ERROR IN WAIT:
//guiVector3fAccessors4(boundsMax,	BoundsMax,		_bounds.point1());
//guiVector3fAccessors4(boundsMax,	BoundsMax,		_bounds.maxPosn);

//guiVector3fAccessors4(boundsCenter,BoundsCenter,	_bounds.center());
//guiVector3fAccessors4(boundsSize,	BoundsSize,		_bounds.size);

@end



