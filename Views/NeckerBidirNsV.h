//  NeckerBidirNsV.h -- to draw a visual image of the 7 necker cube elements. C2013PAK

/*
	An inspector instantiates this view to draw a stick figure in it.
 */

#import "BidirNsV.h"
static const int maxSegments=10;

@interface NeckerBidirNsV: BidirNsV {
    float value[maxSegments];
    float valueIn[maxSegments];
}

    @property (nonatomic, assign) int				nSegments;
    @property (nonatomic, assign) NSPoint			*vertices;
    @property (nonatomic, assign) short				*paths;

@end

@interface Necker5View : NeckerBidirNsV
@end

@interface Necker7View : NeckerBidirNsV
@end
