// InspecV.mm -- an abstract NSView class for bidirectional Gui widgets C2015PAK

#import "InspecV.h"
#import "Path.h"
#import "Port.h"
#import "GenAtom.h"
#import "Bundle.h"
#import "Common.h"
#import "NSCommon.h"
#import "Id+Info.h"
#import "InspecVc.h"
#import "DownNUpButton.h"

@implementation InspecV

- (void) dealloc {
	self.inspectNsVc		= nil;

    [super dealloc];
}

#pragma mark 2. 1-level Access

//- (instancetype) initWithFrame:(NSRect)frameRect {	self=[super initWithFrame:frameRect];
//
//	self.needsLayout		= YES;
//	[self layout];
//	return self;
//}

	  /// When IB sets BidirNsV's outlet to its inspectNsVc, it comes here
	 ///   We register self with inspectNsVc, which enqueues it.
	///     Later, after nib is initialized, our -builtWithTarget is called
- (void) setInspectNsVc:(id)inspectNsVc; {

	_inspectNsVc	= inspectNsVc;			// weak reference

	 /// let our ViewController know we're here:
	if ([inspectNsVc respondsToSelector:@selector(registerView:)])
		[inspectNsVc registerView:self];
}

//- (NSSize)intrinsicContentSize {
//	return NSMakeSize(30.0, NSViewNoIntrinsicMetric);
//}
@end

