// InspecV.h -- an abstract NSView class for bidirectional Gui widgets C2015PAK

#import <Cocoa/Cocoa.h>
#import "SystemTypes.h"

@class Port, InspecVc, Bundle;

@interface InspecV : NSView

	 /// These properties are filled in by IB for each BidirNsV
    @property (nonatomic, assignWeak) IBOutlet InspecVc *inspectNsVc;// Our View Controller

@end

