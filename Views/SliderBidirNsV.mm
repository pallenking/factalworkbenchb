// SliderBidirNsV.mm -- Bidirectional Slider  C2015PAK

#import "SliderBidirNsV.h"
#import "SystemTypes.h"
#import "Path.h"
#import "Port.h"
#import "Bundle.h"
#import "Common.h"
#import "NSCommon.h"
#import "Id+Info.h"
#import "InspecVc.h"


@implementation SliderBidirNsV

#pragma mark 4. Factory								// Compound objects
- (void) buildBidirViewWithTarg:(Bundle *)targBun; {
	
	 // find target port: self.portPath in targetBundle
	NSString *portPath 			= coerceTo(NSString, self.portPath);
XX	id target = [targBun genPortOfLeafNamed:portPath];
	if (Port *targetPort 		= coerceTo(Port, target)){

		NSSlider *theSliderGui 	= [[[NSSlider alloc] initWithFrame:NSZeroRect] autorelease];
		theSliderGui.vertical 	= TRUE;
		self.outboundNsControl	= theSliderGui;
		[self addSubview:theSliderGui];

		[targBun logEvent:@"/\\/\\ SliderBidirNsV [Leaf '%@'] is Port %@", portPath, targetPort.fullName];
	}
	[super buildBidirViewWithTarg:targBun];
}

- (void) layout {										[super layout];

	float nameHeight = 22;			//???
	float sliderHeight = self.bounds.size.height - nameHeight;
	self.meterFullHeight 		= sliderHeight - 20;//17self.outboundNsControl.knobThickness;

	self.nameTextField.frame 	= NSMakeRect(0, sliderHeight , 45, nameHeight);
	
	float xPos = self.topSide? 15: 0;
	self.outboundNsControl.frame = NSMakeRect( xPos, 0, 15, sliderHeight );
}


#pragma mark 3. Draw

float buttonHeight = 14;

- (void) drawRect:(NSRect)dirtyRect {		[super drawRect:dirtyRect];

	if (!self.initialized) {
		[self layout];					// KLUDGE. don't know where to do this
		self.initialized = true;
	}

    CGContextRef gc = (CGContextRef)[[NSGraphicsContext currentContext] graphicsPort];
	
	float c1 = self.topSide? 0.6: 1.0;
	float c2 = self.topSide? 1.0: 0.6;
	float x, y, v;

	 // Display a reference
	CGContextSetLineWidth(gc, 3.0);
	x = self.topSide? 5: 20;
	v = 1;
	y = self.meterFullHeight * v;
	float grey = 0.7;
	CGContextSetRGBStrokeColor(gc, grey, grey, grey,  1);
	CGContextBeginPath(gc);
	CGContextMoveToPoint   (gc,  x,  0);
	CGContextAddLineToPoint(gc,  x,  y + buttonHeight);
	CGContextDrawPath(gc, kCGPathStroke);

	 // Display "inboundFloatValue" as a LINE:
	CGContextSetLineWidth(gc, 10.0);
	x = self.topSide? 5: 20;
	v = self.inboundFloatValue;
	y = self.meterFullHeight * v;
	CGContextSetRGBStrokeColor(gc, c1, c2, 0.6,  1);
	CGContextBeginPath(gc);
	CGContextMoveToPoint   (gc,  x,  0);
	CGContextAddLineToPoint(gc,  x,  y + buttonHeight/2);
	CGContextDrawPath(gc, kCGPathStroke);

	 // Display "theSlider's value" as a LINE:
	x = self.topSide? 20: 5;
	x += 2.0;
	v = self.outboundNsControl.floatValue;
//	v = self.outboundNsControl.intValue;
	y = self.meterFullHeight * v;
	CGContextSetRGBStrokeColor(gc, c2, c1, 0.6,  1);
	CGContextBeginPath(gc);
	CGContextMoveToPoint   (gc,  x,  0);
	CGContextAddLineToPoint(gc,  x,  y);
	CGContextDrawPath(gc, kCGPathStroke);

}
@end
