// BidirNsV.mm -- an abstract NSView class for bidirectional Gui widgets C2015PAK

#import "BidirNsV.h"
#import "InspecVc.h"
#import "Path.h"
#import "Port.h"
#import "GenAtom.h"
#import "Bundle.h"
#import "Common.h"
#import "NSCommon.h"
#import "Id+Info.h"
#import "DownNUpButton.h"

@implementation BidirNsV

#pragma mark 1. Basic Object Level					// Basic objects

- (void) dealloc; {

	[self unbind:@"inboundFloatValue"];
	[self.outboundNsControl unbind:@"floatValue"];
	[self.outboundNsControl unbind:@"value"];

	self.title				= nil;
	self.clockEvent			= nil;
	self.portPath			= nil;

	self.nameTextField		= nil;
	self.targetPort			= nil;
	self.outputToPort		= nil;

	self.outboundNsControl	= nil;

	[super dealloc];
}

#pragma mark 2. 1-level Access

- (instancetype) initWithFrame:(NSRect)frameRect {	self=[super initWithFrame:frameRect];

	self.needsLayout		= YES;
	[self layout];
	return self;
}

	  /// When IB sets BidirNsV's outlet to its inspectNsVc, it comes here
	 ///   We register self with inspectNsVc, which enqueues it.
	///     Later, after nib is initialized, our -builtWithTarget is called
- (void) setInspectNsVc:(id)inspectNsVc; {

	_inspectNsVc	= inspectNsVc;			// weak reference

	 /// let our ViewController know we're here:
	if ([inspectNsVc respondsToSelector:@selector(registerView:)])
		[inspectNsVc registerView:self];
	else
		assert(inspectNsVc==nil, (@"other non-nil are illegal!"));
}

 /// Soon, the method below will be called for our subclass:
- (void) buildBidirViewWithTarg:(Bundle *)targBun; {

	 // Label the ...BidirNsV:
	self.nameTextField = [[[NSTextField alloc] initWithFrame:NSZeroRect] autorelease];
	self.nameTextField.backgroundColor = [NSColor clearColor];
	self.nameTextField.editable = false;
	[self addSubview:self.nameTextField];

	  /// Configure ClockPath -- (no work to do here)

	  /// Configure PortPath
	 // find target port: self.portPath in targetBundle
	NSString *portPath = coerceTo(NSString, self.portPath);

XX	id target = [targBun genPortOfLeafNamed:portPath];

	if (Port *targetPort = coerceTo(Port, target)) {
		self.targetPort = targetPort;

		NSInteger suffixLoc 	= [portPath rangeOfString:@"/" options:NSBackwardsSearch].location;
		assert(suffixLoc==NSNotFound, (@"160314 portPath may never contain a '/'"));
		self.nameTextField.stringValue = portPath;	// text box follows label
		self.nameTextField.selectable = true;
		
		  /// Bind my "inboundFloatValue" property to inPort.value.
		 /// display value from here on meter
		Port  *inPort 			= self.targetPort.connectedTo?: self.targetPort;
XX		[self bind:@"inboundFloatValue" toObject:inPort withKeyPath:@"value" options:nil];
		[[@"" addF:@"/\\/\\ Gui %@        .inboundFloatValue --bind-> Port %@ .value", self.portPath, inPort.fullName16] ppLog];

		  /// Bind .outboundNsControl's.(float)Value to property outPort.value
		 /// Force value into network
		 // Bind my .outboundNsControl to outGen
		self.outputToPort		= self.targetPort;	// slider data goes here
		if ([self.outboundNsControl isKindOfClass:[DownNUpButton class]]) {
			[self.outboundNsControl bind:@"floatValue" toObject:self.outputToPort withKeyPath:@"value" options:nil];
			[[@"" addF:@"/\\/\\ Gui %@ .outboundNsControl .floatValue --bind-> Port %@ .value", self.portPath, self.outputToPort.fullName16] ppLog];
		}
		else if ([self.outboundNsControl isKindOfClass:[NSSlider class]]) {
			[self.outboundNsControl bind:@"value" toObject:self.outputToPort withKeyPath:@"valueAsNSNumber" options:nil];
			[[@"" addF:@"/\\/\\ Gui %@ .outboundNsControl .value --bind-> Port %@ .valueAsNSNumber", self.portPath, self.outputToPort.fullName16] ppLog];
		}
		else panic(@"");
	}

	else if (NSNumber *genNumber = coerceTo(NSNumber, target))
		assert([genNumber intValue]==0, (@"@0 is ignore, others not permitted"));
	else {
		self.nameTextField.stringValue = @"?";
		[[@"" addF:@"/\\/\\ target %@: %@ NOT FOUND", targBun.fullName, portPath] ppLog];
	}
}

  // Updating SliderBidirNsV float values
 //
- (void) setInboundFloatValue:(float)inboundFloatValue; {
	_inboundFloatValue = inboundFloatValue;

	[[@"" addF:@"/\\/\\  %@.inboundFloatValue = %f", self.portPath, inboundFloatValue] ppLog];

	self.needsDisplay = true;		// redisplay ourselves with the new value
}

- (NSSize)intrinsicContentSize {
	return NSMakeSize(30.0, NSViewNoIntrinsicMetric);
}
@end
