//  LinkView.h -- View a particular link C2013PAK

/*
 (Philosophy: all work is done in Link; this class mostly just forwards
 */

#import "View.h"

@interface LinkView: View

    @property (nonatomic, retain) View *bossView;
    @property (nonatomic, retain) View *workerView;

    @property (nonatomic, assign) Vector3f bossLoc;// location superView's coords (e.g a view --> Factal)
    @property (nonatomic, assign) Vector3f workerLoc;
        ///// Calculations for wires are done in the bosses superView's coordinate systems
@end
