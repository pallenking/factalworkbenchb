//  DownNUpButton.m -- NSButton which tells it's up/down state C2016PAK

#import "DownNUpButton.h"

@implementation DownNUpButton

- (void)mouseDown:(NSEvent *)event {
	self.isPressed = true;
    [super mouseDown:event];

	self.isPressed = false;
	[self.target performSelector:self.action withObject:self];
}

- (float) value;				{		return self.floatValue;	}
- (void) setValue:(float)val;	{		self.floatValue = val;	}

@end
