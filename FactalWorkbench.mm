//  FactalWorkbench.mm -- User Code interface to FactalWorkbench C2015PAK

#import "Id+Info.h"

//#import "GenAtom.h"
#import "Mirror.h"
#import "Known.h"
#import "Modulator.h"
#import "Previous.h"
#import "Ago.h"
#import "Rotator.h"
#import "Brain.h"

#import "Leaf.h"
#import "Tunnel.h"
#import "Link.h"
#import "Path.h"

#import "Actor.h"
#import "WriteHead.h"
#import "Splitter.h"
#import "Label.h"

////////
#import "FactalWorkbench.h"
#import "NSCommon.h"
#import "Common.h"
#import "Id+Info.h"


	  // ///////////////////////////////////////////////////////////////// //
	 // DASHBOAD:  control current simulation environment (1 per system)  //
	// ///////////////////////////////////////////////////////////////// //

#pragma mark - Dashboard
id userSimulationPreferences() {

	NSDictionary *tmp = @{	// N.B.: keys with leading spaces get pruned away

   // N.B: Leading space gets entry ignored

  ////////// Application configuration					///////////////////////
 ////
@"dataDirectory"		:@"/Users/allen/src/factal/factalWorkbenchb/",
//@"dataDirectory"		:@"/Users/allen/src/factalWorkbenchb/",
//@"dataDirectory"		:@"/Users/allen/src/FactalWorkbench/",

  ////////// 4. Factory              CONSTRUCTION       ///////////////////////
 ////
@"curRegressTestNumber"	:@"1",  		// start of "^r" tests	total>130
@"curMenueTestNumber"	:@"118",  		// start of "^m" tests	total>130
					//   ++++++  //
@"breakAtWireNo"		:@"-13",

// Add construction parameters here?

  ////////// 7. Reenactment Sim      SIMULATION         ///////////////////////
 //// ENABLE
@" simEnable"			:@"0",			// don't run simulator
@"randomSeed"			:@"7115",		// 0 --> from time();	//7115

 //// ENDING:
@" eventLimit"			:@"1",			// defaults to -1 OFF


  ////////// 9. 3D Support           VIEWING            ///////////////////////
 //// BREAKS

/// Break based on the name of the view.
// (There is one of these for each of th major phases of processing.)
@" breakAtViewOf"		:@"V44", 		// break on assigning the named View
@" breakAtBoundOf"		:@"V1", 		// break on bounding of named view
@" breakAtPositionOf"	:@"V63", 		// break on placing named view
//		to enable more helpful but "noisy" output in the console log, if needed use
@" debugPositionOf"		:@"V63", 		// debug placing named view
@" debugOverlapOf"		:@"V48", 		// debug work placing named view
@" breakAtOverlapNo" 	:@"3",			// break on the N'th placement move
@" breakAtRenderOf"		:@"V65",  		// break on placing named view
						// V44 V53 V55
 /////////// 9. 3D DISPLAY			 3D Display			///////////////////////
/// The first group control how FW chooses to draw Views of Models in 3D:
@" bundleHeight" 		:@10.0,			// length of tunnel end

@"render3DMode"			:@"3Dlighting",	// 3Dlighting 3Dcartoon 3DColorTags
@" linkVelocityLog2"	:@"-3",			// slow:-6, default:-4, fast:0
@" animateBirth" 		:@0,			// when OFF, new elt is in final place immediately
@"animBirthDecayLog10"	:@"-1",			//
@" linkRadius"			:@"0.1",		// (<0.2 -- OGL-1pix line)
@"signalSize"			:@"0.75",		// 1.4 Encompases most of Modulator
@" boundsDrapes" 		:@"0.2",		// some bounds visible	Default: 0=none
@"displayAxis"			:@"0",			// default is ON
///		Text for Labeling
@" fontNumber"			:@"7",			// default is 7 (big bold font)
@"displayPortNames"		:@"0",			// default is ON
@"displayAtomNames" 	:@"0",			// default is ON
@"displayLinkNames" 	:@"0",			// default is ON
@"displayNetNames"  	:@"0",			// default is ONs
@" displayLeafNames"  	:@"1",			// default is ON
@" simsScreen"			:@"1",			// put demo on alternate screen: default is OFF

					//   ++++++  //
@"fluffLinkGap" 		:@"3",			// length of links
@" gapWhenStacking"		:@"1",			// gap between bundles
@" picPan"				:@1,			// pan to center after object after pic

 /////////// 12. Inspectors          INSPECTORS         ///////////////////////

  ////////// 14. Logging             CONSOLE LOG        ///////////////////////
 //// LOGGING
@"logTerminalSrchStartsAt":@"10",
 					//   ++++++  //
@"logTime"		:@"1",			// log the passage of time
/// Simulation DAG
/// Simulation - DATA		// sumFloor
@" logEvents"			:@"0",			// log important stages when data flows
@"logDataPutsTakes" 	:@"1",			// log all individual flow between Plugs
/// Master Control			// for GHUnit: testValue1
@"logAllocationEventsOf":@1,

					//+++++++++++//
 					//   ++++++  // 4074//186//124
@"breakAtLog"			:@"-48",      	// Break on this log entry, <=0:OFF
					//   ++++++  //
					//+++++++++++//

 /////////// 15. PrettyPrint         PRETTY PRINT       ///////////////////////
@"ppDagHeight"			:@"1",			// defaults OFF
@" ppRetainCount_"		:@"1",			// pretty-print retain count in summary
@" ppTransShowMutateable":@"1",			// display {+ and [+ for mutables
@" ppLinks"				:@"1",			// defaults OFF (excludes Links and Labels)
@" ppPartsEarlyBits"	:@"0",			// defaults ON
@" ppLineWidth"			:@"95",
@"ppParameters"			:@"1",			// defaults OFF
@" ppObjectAddress"		:@"1",			// defaults OFF

   };
	  // Post Processing:
	 // strip off entries with leading spaces
	id rv = @{}.anotherMutable;
	for (NSString *key in tmp)
		if (![key hasPrefix:@" "])
			rv[key] = tmp[key];

	 // Add TIMESTAMP
	rv[@"time"] = [@"" addF:@"%s", fWallTime((char *)"%y%m%d.%H%M")];

	return rv;
}


	// CheckTest is called for every test defined in UserCode
   //   (These can be seen in UserCode.h as: r(*), R(name, *), M(name, *))
  // This execution lies under UserCode.mm
 #pragma mark - linkage from source code
// Ugly, but it's still working!
id		sceneMenuePath;

int		uWant_menuNo;
int		uWant_regressNo;
id		uWant_name;

bool	uWant_itBuilt;

int		uAt_menuNo;
int		uAt_regressNo;
int		uAt_allNo;

id		uBuilt_brain;
id		uBuilt_metaInfo;

Brain *checkTest(NSDictionary *sourceLineMetaInfo) {
	  // sourceLineMetaInfo comes from type of invocation macro
	 //(e.g. m(), M(), r(), and R())

	bool found=false, selectedWithX=0, menuBlock=0, regressBlock=0;
	uAt_allNo++;								// count all

	 // See if name matches
	if (uWant_name) {
		id thisBlockName = sourceLineMetaInfo[@"name"];
		if (sceneMenuePath)
			thisBlockName = [@"" addF:@"%@/%@", sceneMenuePath, thisBlockName];
		if ([thisBlockName isEqualToString:uWant_name])
			found = 1;
	}

	 // keep track of uAt_menuNo and atRegresNo, assuming not in selectedWithX mode
	id controlBlockKind = sourceLineMetaInfo[@"controlBlock"];
	if ((menuBlock = [controlBlockKind isEqualToString:@"menu"])) {// It's a Menu block
		found |= uAt_menuNo==uWant_menuNo;		// match current?
		//assert(found==0, (@""));
		uAt_menuNo++;							// go on to next
	}
	if ((regressBlock = [controlBlockKind isEqualToString:@"regress"])) {// It's a Regress block
		found |= uAt_regressNo==uWant_regressNo;	// match current?
		uAt_regressNo++;						// go on to next
	}

	 // now process selectedWithX mode
	if (uWant_regressNo<0 and uWant_menuNo<0 and !uWant_name)	// we want to find selected execute block
		found |= selectedWithX = sourceLineMetaInfo[@"selectedWithX"]!=0;

	if (found) {
		if (uBuilt_brain == 0) {				// use first one found
			id prefix = uWant_regressNo>=0?	[@"" addF:@"R%d,",  uWant_regressNo]:
						uWant_menuNo  >=0?	[@"" addF:@"M%d,",  uWant_menuNo   ]:
						regressBlock?		[@"" addF:@"R%dX,", uAt_regressNo-1]:
						menuBlock?			[@"" addF:@"M%dX,", uAt_menuNo   -1]:
											 @"x:";

			 /// Meta Info
			uBuilt_metaInfo[@"found"]= @1;

			id name					= sourceLineMetaInfo[@"name"]?: @"";
			uBuilt_metaInfo[@"name"]= name;

			id line					= sourceLineMetaInfo[@"line"]?: @-1;
			uBuilt_metaInfo[@"line"]= line;

			id nameStr				= [name length]? [@"" addF:@"%@: ", name]: @"";
//			id nameStr				= [name length]? [@"" addF:@"'%@' ", name]: @"";
			id windowName			= [@"" addF:@"%@%@L%@", nameStr, prefix, line];
			uBuilt_metaInfo[@"windowName"]	= windowName;
			uBuilt_metaInfo[@"sceneMenuePath"]= sceneMenuePath;
			if (!uWant_itBuilt)
				return nil;					// just want meatInfo; don't build

			 // 1. Build the Brain object:
XX			Brain *newbieBrain = anotherBasicPart([Brain class]);
			[Brain setCurrentBrain:newbieBrain];	// HACK: some low level tasks need this

			   /// must install user preferences here, so that ppLog works
			  // 2. Install user preverences into simulator
			 // Simulator Defaults:				(from Brain.h, macro forAllBrainParameters, esp. arg 2)
			[newbieBrain defaultSimParams];
			 // Transient Simulator Defaults:	(from FactalWorkbench.mm)
			[newbieBrain setSimParams:userSimulationPreferences()];

			NSLog(@"Building HnW Network: %@\n\n", windowName);
			    /// N.B: FRAGILITY -- This first call to ppLog can only be done now,
			   /// after currentBrain is set. This is because ppLog presumes a valid currentBrain.
			  /// Investigate this dependency: simParams have leaked into Id+Info!
			 /// ALSO: after all params set, so breakOnLine=1 will stop this line:
			[[@"" addF:@"\nBuilding HaveNWant Network: %@", windowName] ppLog];
			NSLog(@"Building HnW Network: %@\n\n", windowName);

			 // 3. Return to the macro in FactalWorkbench.h, and then to Brain::build:inf.
			return newbieBrain;
		}
		else 							// just report the others
			panic(@"Ignoring line %d (Already found %d with x).",
				[sourceLineMetaInfo[@"line"] intValue], [uBuilt_metaInfo[@"line"] intValue]);
	}
	return nil;
}


			// Factory methods, to make elements
#pragma mark Composite
///////////////////// COMPOSITE GENERATORS ///////////////////
id aMarkovPort(id hammings, id maxor, id etc)		{			return
	aNet(@{flip}, @[
		o(mBay,	aBayes(@"mAgo=", @0, @{flip})),
		o(mAgo,	anAgo(@0, @{flip})),
		o(mRot,	aModulator(@"mAgo=", @{spin$0})),
//		o(mRot,	aRotator(@"mAgo=", @{spin$0})),
		o(mMax,	aMaxOr(  @"mRot.S=", hammings?:@0)),		// enable
		o(mHam,	aHamming(@"mRot.T",   maxor?:@0)),			// terms
									]);
}

  // ////////////////////////////////////////////////////////////////////
 // A second attempt at getting Crux right:
//id didat_bundle = @[@"d_t", @"d_i", @"d_a"];
//id didat_sound_bundle= @[@"d_t:sound:t-sound", @"d_i:sound:di-sound", @"d_a:sound:da-sound"];
id didat_bundle 			= @[@"d_i", @"d_t", @"d_a"];
id didat_speak_bundle 		= @[@"d_i", @"d_t", @"d_a", @"speak"];

id didat_sound_bundle		= @[@"d_i:sound:di-sound", @"d_t:sound:t-sound", @"d_a:sound:da-sound"];
id didat_speak_sound_bundle	= @[@"d_i:sound:di-sound", @"d_t:sound:t-sound", @"d_a:sound:da-sound", @"speak:@1"];


id   dat_bundle = @[@"d_i", @"d_t", @"d_a"];
id   dat_sound_bundle= @[@"d_t:sound:t-sound", @"d_a:sound:da-sound"];
id     t_sound_bundle= @[@"d_t:sound:t-sound"];
id     t_bundle = @[@"d_t"];
//id wheel_bundle = @[@"A", @"B", @"C", @"D"];
id wheel_bundle =	@{
				@"x":@[spin_R, @"A", @"B", @"C", @"D"],
//				@"y":@[spin_R, @"A", @"B", @"C", @"D"],
//				@"w":@[spin_R, @"T", @"B", @"L", @"R"],
					};
//id y3x3_stux_bundle = @{spin$R,
//				@"joystick":@[spin_R, @"left", @"right", @"jump"],
//				@"mana":    @[spin_R, @"health", @"hunger"],
//				@"retina":  y3x3_bundle};

id aDiDa(id diArg, id daArg, id tArg, id etc) {					return
	aNet(@{etcEtc}, @[
		o(ago,	anAgo(@0)),
		o(iMod,	aModulator(diArg,	@{@"S":@"ago", @"T":@"d_i",		flip})),		// M->T
		o(tMod,	aModulator( tArg,	@{@"S":@"ago", @"T":@"d_t",		flip})),
		o(aMod,	aModulator(daArg,	@{@"S":@"ago", @"T":@"d_a",		flip})),
									]);
}

id stopIfNotLevel(int level) {	return [NSNumber numberWithInt:level];	}

#pragma mark - Leaf Definitions
//////////////////////////////////////////////////
/////     Leaf Ports (terminals of Bundles)   /////
//////////////////////////////////////////////////
 // use either bMain or bPrev, depending on whether extra ups
#define bMain		@"":@"main", @"+":@"main"
#define bMainPM		@"":@"main+", @"+":@"main+", @"-":@"main-"
#define bPrev		@"":@"prev.S", @"+":@"prev.S"
#define bPrevPM		@"":@"prev.S", @"+":@"prev.S", @"-":@"prev.T"

 // -------- Null element -------------------------------------------------------
id aNilLeaf(id etc1, id etc2) {											return
	aLeaf(@"nil",								etc1, @[
											 ], etc2	);}
id aGenLeaf(id etc1, id etc2) {											return
	aLeaf(@"gen", @{@"":@"gen", @"G":@"gen.P", @"R":@"gen.P"}, @[
		o(gen,	aGenAtom(					  @{etc1etc, flip})),
											 ], etc2	);}
id aGenSoundLeaf(id etc1, id etc2, id etc3) {											return
	aLeaf(@"genSound", @{@"":@"gen", @"G":@"gen.P", @"R":@"gen.P", @"SND":@"sound.P"}, @[
		o(gen,	aGenAtom(					  @{etc1etc, flip})),
		o(sound,aSoundAtom(@{@"P":@"gen=", 		etc2etc})),
											 ], etc3	);}
 // -------- Generic Splitter -------------------------------------------------------
id aSplitterLeaf		(id etc1, id etc2){							return
	aLeaf(@"splitter", @{}, @[
		o(main,	aSplitter(0, 0,	0,				etc1)),
															  ], etc2	);}
id aGenSplitterLeaf	(id etc1, id etc2, id etc3) {					return	 //////// STANDARD, for gen->broadcast
	aLeaf(@"genSplittert", @{bMain, @"G":@"gen.P", @"R":@"gen.P"}, @[
		 o(gen,	aGenAtom(					  @{etc1etc, flip})),
		 o(main,aSplitter(0, @"gen=", 0,		etc2)),
																 ], etc3	);}

// -------- Broadcast -------------------------------------------------------
id aBcastLeaf(id etc1, id etc2) {										return
	aLeaf(@"bcast", @{bMain, @"G":@"gen.P", @"R":@"gen.P"}, @[
		o(main,	aBroadcast(0, 0,				etc1)),
											 ], etc2	);}
id aGenBcastLeaf(id etc1, id etc2, id etc3) {							return	 //////// STANDARD, for gen->broadcast
	aLeaf(@"genBcast", @{bMain, @"G":@"gen.P", @"R":@"gen.P"}, @[
		o(gen,	aGenAtom(					  @{etc1etc, flip})),
		o(main,	aBroadcast(@"gen=", 0,			etc2)),
											 ], etc3	);}

id aGenSoundBcastLeaf(id etc1, id etc2, id etc3, id etc4) {				return
	aLeaf(@"genSoundBcast", @{bMain, @"G":@"gen.P", @"R":@"gen.P", @"SND":@"sound.P"}, @[		// R:NO STATE
		o(gen,	aGenAtom(					  @{etc1etc, flip})),
		o(sound,aSoundAtom(@{@"P":@"gen=", 		etc2etc})),
		o(main,	aBroadcast(@"sound=", 0,		etc3)),
											 ], etc4	 );}
id aGenMaxLeaf(id etc1, id etc2, id etc3) {								return
	aLeaf(@"genMax", @{bMain, @"G":@"gen.P", @"R":@"gen.P"}, @[		// R:NO STATE
		o(gen,	aGenAtom(					  @{etc1etc, flip})),
		o(main,	aMaxOr(@"gen=", 0,				etc2)),
											 ], etc3	 );}
id aGenSoundMaxLeaf(id etc1, id etc2, id etc3, id etc4) {				return
	aLeaf(@"genSoundMax", @{bMain, @"G":@"gen.P", @"R":@"gen.P", @"SND":@"sound.P"}, @[		// R:NO STATE
		o(gen,	aGenAtom(					  @{etc1etc, flip})),
		o(sound,aSoundAtom(@{@"P":@"gen=", 		etc2etc})),
		o(main,	aMaxOr(@"sound=", 0,			etc3)),
											 ], etc4	 );}

 ////////// DEFAULT CONTEXT #######################
id aGenMaxsqLeaf(id etc1, id etc2, id etc3) {							return
	aLeaf(@"genMaxSq", @{bMain, @"G":@"gen.P", @"R":@"gen.P"}, @[	// R:NO STATE
		o(gen,	aGenAtom(					  @{etc1etc, flip})),
		o(main,	aMaxOr(@"gen=", 0,				etc2)),
		o(squelch,aHamming(@"main,l=1", 0, @{@"jog":@"0, 0, 4"})),		// no sec:main @"0, -6, 3" @"0, -5, 4"
											 ], etc3	 );}
id aBcastMaxLeaf(id etc1, id etc2) {									return
	aLeaf(@"bcastMax", @{bMain}, @[									// R:NO STATE
		o(bcast,aBroadcast(0, 0,						@{flip})),
		o(main,	aMaxOr(@"bcast=", 0,			etc1)),
											 ], etc2	 );}
 // -------- Bayes -------------------------------------------------------
id aBayesLeaf(id etc1, id etc2) {										return
	aLeaf(@"bayes", @{bMain}, @[
		o(main,	aBayes(0, 0,					etc1)),
											 ], etc2	);}
id aGenBayesLeaf(id etc1, id etc2, id etc3) {							return
	aLeaf(@"genBayes", @{bMain, @"G":@"gen.P", @"R":@"gen.P"}, @[
		o(gen,	aGenAtom(					  @{etc1etc, flip})),
		o(main,	aBayes(@"gen=", 0,				etc2)),
										 	 ], etc3	);}
 // -------- Mod -------------------------------------------------------
id aModLeaf(id etc1, id etc2) {											return
	aLeaf(@"mod", @{bMain}, @[
		o(main,	aModulator(0,					etc1)),
											 ], etc2	);}
id anOrModBcastLeaf(id etc1, id etc2, id etc3, id etc4) {				return
	aLeaf(@"orModBcast", @{}, @[			//@"G":@"gen.P"
		o(gen,	aMaxOr(@"mod.P=", 0,		  @{etc1etc, flip})),
		o(mod,	aModulator(0,					etc2)),	// @"mod":@"???bid", spin$0})),
		o(main,	aBroadcast(@"mod.S=",0,			etc3)),
											 ], etc4	);}
 // -------- Rotator -------------------------------------------------------
id aRotLeaf(id etc1, id etc2) {											return
	aLeaf(@"rot", @{bMain}, @[
		o(main,	aRotator(0,						etc1)),
											 ], etc2	);}
id aRotBcastLeaf(id etc1, id etc2, id etc3) {							return
	aLeaf(@"rotBcast", @{bMain, @"G":@"gen.P", @"R":@"gen.P"}, @[
		o(rot,	aRotator(0,						etc1)),
		o(main,	aBroadcast(@"rot.T=", 0,	  @{etc2etc, @"latitude":@4, @"jog":@"0,-1.5,0"})),
											 ], etc3	);}
 // -------- Branch -------------------------------------------------------
id aBranchLeaf(id etc1, id etc2) {										return
	aLeaf(@"branch", @{bMain}, @[
//				aBranch(@{@"Share":@"Bulb", @"S":sproutSpot,  @"M@":sproutPredicate});
		o(main,	aBranch(						etc1)),
//		o(main,	aBranch(					  @{etc1etc})),
								], @{@"bandColor":@".L", etc2etc}	);}
 // -------- Bulb -------------------------------------------------------
id aBulbLeaf(id etc1, id etc2) {										return
	aLeaf(@"bulb", @{bMain}, @[
		o(main,	aBulb(0, 0,						etc1)),
											 ], etc2	);}
id aGenBulbLeaf(id etc1, id etc2, id etc3) {							return
	aLeaf(@"genBulb", @{bMain, @"G":@"gen.P", @"R":@"gen.P"}, @[
		o(gen,	aGenAtom(					  @{etc1etc, flip})),
		o(main,	aBulb(@"gen=", 0,				etc2)),
											 ], etc3	);}

 // -------- Previous -------------------------------------------------------
id	aGenPrevLeaf(id etc1, id etc2, id etc3)	{							return
	aLeaf(@"genPrev",    @{bPrevPM, @"G":@"gen.P", @"R":@"prev.L"}, @[
		o(gen,	aGenAtom(					  @{etc1etc, flip})),
		o(prev,	aPrevious(@"gen=",				etc2)),				//, spin$R
											 ], etc3	);}
 ////////// DEFAULT EVIDENCE with Previous's: #######################
id aGenPrevBcastLeaf(id etc1, id etc2, id etc3, id etc4) {				return
	aLeaf(@"genPrevBcast", @{bMainPM, @"G":@"gen.P", @"R":@"prev.L" }, @[
		o(gen,	aGenAtom(					  @{etc1etc, flip})),
		o(prev,	aPrevious(@"gen=",				etc2)),
		o(main+,aBroadcast(@"prev.S=", 0,		etc3)),
		o(main-,aBroadcast(@"prev.T=", 0,		etc3)),
											 ], etc4	 );}

id aGenSoundPrevBcastLeaf(id etc1, id etc2, id etc3, id etc4, id etc5) {	return
	aLeaf(@"genSoundPrevBcast", @{bMainPM, @"G":@"gen.P", @"R":@"prev.L", @"SND":@"sound.P"}, @[
		o(gen,	aGenAtom(					  @{etc1etc, flip})),
		o(sound,aSoundAtom(@{@"P":@"gen=", 		etc2etc})),
		o(prev,	aPrevious(@"sound=",			etc3)),
		o(main+,aBroadcast(@"prev.S=", 0,		etc4)),
		o(main-,aBroadcast(@"prev.T=", 0,		etc4)),
											 ], etc5	 );			 }

						// BASIS port to invoke Previous's connection to below:
id aPrevBcastLeaf(id etc1, id etc2, id etc3) {										return
	aLeaf(@"prevBcast", @{bMainPM, @"G":@"ERROR-not generatable", @"R":@"prev.L"}, @[
		o(prev,	aPrevious(0,				  @{etc1etc, spin$R})),
		o(main+,aBroadcast(@"prev.S=", 0,		etc2)),
		o(main-,aBroadcast(@"prev.T=", 0,		etc2)),
											 ], etc3	);}
id aFlipPrevLeaf(id etc1, id etc2)	{									return
	aLeaf(@"flipPrev", @{bPrevPM, @"G":@"prev.L"/*@0*/, @"R":@"prev.L"}, @[		// @"G":@"gen.P"
		o(gen,	aBroadcast(@"prev.T=", 0,	@{flip/*, @"latitude":@2*/})),
		o(prev,	aPrevious(0,				  @{etc1etc, flip, spin$1})),
											 ], etc2	);}
id aPrevLeaf(id etc1, id etc2)	{									return
	aLeaf(@"prev", @{bPrevPM, @"G":@"gen.P", @"R":@"prev.L"}, @[
		o(prev,	aPrevious(0,				    etc1)),					//, spin$1
											 ], etc2	);}
 // -------- Ago -------------------------------------------------------
id aGenAgoLeaf(id etc1, id etc2, id etc3) {								return
	aLeaf(@"genAgo", @{bMain, @"G":@"gen.P"}, @[
		o(gen,	aGenAtom(					  @{etc1etc, flip})),
		o(main,	anAgo(@"gen=",					etc2)),
											 ], etc3	);}
id anAgoMaxLeaf(id etc1, id etc2, id etc3) {							return
	aLeaf(@"agoMax", @{bMain}, @[
		o(ago,	anAgo(0,						etc1)),
		o(main,	aMaxOr(@"ago=", 0,				etc2)),
											 ], etc3	);}
 // -------- qState -------------------------------------------------------
id anAgoDistCombLeaf(id etc1, id etc2, id etc3, id etc4) {			return
	aLeaf(@"agoDistComb", @{@"":@"dist", @"in":@"comb"}, @[
		aBroadcast(0,		0,	@{n(dist),		etc2etc}),	//aBayes
//		anAgo     (0,			@{n(ago), 		etc1etc}),	//@"comb^"
//		aBroadcast(@"ago=",	0,	@{n(dist),		etc2etc}),	//aBayes
		aBroadcast(0,		0,	@{n(comb),flip, etc3etc, selfStackY}),		// name doesn't work!
											 ], etc4	);}

 // -------- Report an ERROR
id anError (id str) {
	panic(@"Error Message from Network:\n%@", mustBe(NSString, str));
	return nil;
}

 // ---------------------------------------------------------------

#pragma mark - Bundle Definitions
////////////////////////////////////////////////
/////     Bundles of Ports                  /////
////////////////////////////////////////////////
id _bundle			= @[];				// (empty bundle)
id a_bundle			= @[@"a"];
id ab_bundle		= @[@"a", @"b"];
id abc_bundle		= @[@"a", @"b", @"c"];
id abcd_bundle		= @[@"a", @"b", @"c", @"d"];
id abcde_bundle		= @[@"a", @"b", @"c", @"d", @"e"];
id abcdef_bundle	= @[@"a", @"b", @"c", @"d", @"e", @"f"];
id abcdefg_bundle	= @[@"a", @"b", @"c", @"d", @"e", @"f", @"g"];
id abcdefgh_bundle	= @[@"a", @"b", @"c", @"d", @"e", @"f", @"g", @"h"];
id abc_def_bundle	= @[@[spin_L, @"a", @"b", @"c"], @[spin_L, @"d", @"e", @"f"]];
id d_bundle			= @[@"d"];
id drm_bundle		= @[@"d", @"r", @"m"];
id drmfsltD_bundle	= @[@"d", @"r", @"m", @"f", @"s", @"l", @"d_t", @"D"];
id e_bundle			= @[@"e"];
id i_bundle			= @[@"i"];
id ij_bundle		= @[@"i", @"j"];
id ijk_bundle		= @[@"i", @"j", @"k"];
id z_bundle			= @[@"z"];
id yz_bundle		= @[@"y", @"z"];
id xy_bundle		= @[@"x", @"y"];
id xyz_bundle		= @[@"x", @"y", @"z"];
id wxyz_bundle		= @[@"w", @"x", @"y", @"z"];

#pragma mark * yNxN_bundle Definitions
id y0x0_bundle = @[];
id y1x1_bundle = @[spin_R,
		@[spin_R, @"y0x0:s1"], ];
id y2x2_bundle = @[spin_R,
		@[spin_R, @"y0x0:s1", @"y0x1"],
		@[spin_R, @"y1x0", @"y1x1"], ];
id y3x3_bundle = @[spin_R,
		@[spin_R, @"y0x0", @"y0x1", @"y0x2"],
		@[spin_R, @"y1x0", @"y1x1", @"y1x2"],
		@[spin_R, @"y2x0", @"y2x1", @"y2x2"], ];
id y4x4_bundle = @[spin_R,
		@[spin_R, @"y0x0", @"y0x1", @"y0x2", @"y0x3"],
		@[spin_R, @"y1x0", @"y1x1", @"y1x2", @"y1x3"],
		@[spin_R, @"y2x0", @"y2x1", @"y2x2", @"y2x3"],
		@[spin_R, @"y3x0", @"y3x1", @"y3x2", @"y3x3"], ];
id (^y4x4_bundle_)(id, id) = ^id (id info1, id info2) { return @[ info1,
		@[info2, @"y0x0", @"y0x1", @"y0x2", @"y0x3"],
		@[info2, @"y1x0", @"y1x1", @"y1x2", @"y1x3"],
		@[info2, @"y2x0", @"y2x1", @"y2x2", @"y2x3"],
		@[info2, @"y3x0", @"y3x1", @"y3x2", @"y3x3"], ]; };

id y5x5_bundle = @[spin_R,
		@[spin_R, @"y0x0", @"y0x1", @"y0x2", @"y0x3", @"y0x4"],
		@[spin_R, @"y1x0", @"y1x1", @"y1x2", @"y1x3", @"y1x4"],
		@[spin_R, @"y2x0", @"y2x1", @"y2x2", @"y2x3", @"y2x4"],
		@[spin_R, @"y3x0", @"y3x1", @"y3x2", @"y3x3", @"y3x4"],
		@[spin_R, @"y4x0", @"y4x1", @"y4x2", @"y4x3", @"y4x4"], ];
id y6x6_bundle = @[spin_R,
		@[spin_R, @"y0x0", @"y0x1", @"y0x2", @"y0x3", @"y0x4", @"y0x5"],
		@[spin_R, @"y1x0", @"y1x1", @"y1x2", @"y1x3", @"y1x4", @"y1x5"],
		@[spin_R, @"y2x0", @"y2x1", @"y2x2", @"y2x3", @"y2x4", @"y2x5"],
		@[spin_R, @"y3x0", @"y3x1", @"y3x2", @"y3x3", @"y3x4", @"y3x5"],
		@[spin_R, @"y4x0", @"y4x1", @"y4x2", @"y4x3", @"y4x4", @"y4x5"],
		@[spin_R, @"y5x0", @"y5x1", @"y5x2", @"y5x3", @"y5x4", @"y5x5"], ];

// Another way to do it, with blocks:
//id (^a_bundle)(id)		= ^id (id info1) { return @[@"a"				];};


