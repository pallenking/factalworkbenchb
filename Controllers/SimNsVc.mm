//  SimNsVc.mm -- View Controller for a 3D view of the simulation C2017PAK

#import "SimNsV.h"
#import "SimNsVc.h"
#import "SimNsWc.h"
#import "AppController.h"

#import "Brain.h"
#import "View.h"
#import "InspecVc.h"
#import "Common.h"
#import "NSCommon.h"
#import "Id+Info.h"

@implementation SimNsVc

#pragma mark 1. Basic Object Level					// Basic objects

- init; {						self = [super init];

	 /// Create just one View Controller for the simulation, for now:
	NSRect simsFrame_ 		= simsFrame(nibsWidth);
XX	SimNsV *simNsV			= [[[SimNsV alloc] initWithFrame:simsFrame_] autorelease];
	self.view				= simNsV;		// retained (rc++)
	simNsV.simNsVc			= self;			// weak backpointer

//	self.simNsWc.window.contentView	= simNsV;// retain (rc+=2) self.simNsWc==0!!!
	return self;
}

- (void) dealloc {
	if (logDeallocEvents)
		NSPrintf(@"=================== Dealloc SimNsVc %p ===\n", self);

	self.rootFwV			= nil;

	[super dealloc];					// rc--:   self.view: 8 --> 7
}

//- (void)viewDidLoad {
//    [super viewDidLoad];
//	panic(@"");
//	// Do view setup here.
//}

#pragma mark 11. 3D Display
//// // // // // // // // // // // // // // // // // // // // // // // ///
/// // // // // // // // // // 3D Display  // // // // // // // // // // /
// // // // // // // // // // // // // // // // // // // // // // // // //

// Build Out my View
- (void) buildRootFwVforBrain:(Brain *)brain; {
	
	if (!brain) {							// no Model
		self.rootFwV			= nil;		// no View; done
		return;
	}
	
	// Get a registered root View; Make an empty one if none.
	View *rootFwV				= self.rootFwV;
	if (!rootFwV) {	// (N.B: load self.rootFwV early, so debugger can print view tree as the View tree is being built)
		self.rootFwV			= rootFwV = [View another];
		rootFwV.part			= brain;
		rootFwV.displayMode		= brain.initialDisplayMode;
		brain.someViewDirty		= true;
	}
	
	if (brain.someViewDirty) {
		[brain logEvent:@"reView Views:"];
		
XX		[brain reViewIntoView:rootFwV];		/// 1. Mke a view of part

XX		[rootFwV normalizeTreesHeights];	/// 2. adorn stacking trees of same kind
		
		brain.someViewDirty		= false;
		brain.boundsDirty		= true;
		[@"" ppLog]; 							// extra newline
	}

	if (brain.boundsDirty) {
		[brain logEvent:@"reBound Views:"];
		
		 // Note 180209: awkward head of call, to be surfaced here
		if (rootFwV.isAtomic)				/// 3 Bound rootFwV
XX			[brain boundAsAtomIntoView:rootFwV];//		a. Atomic
		else {									//		b. Show Insides
XX			[brain suggestedSpinInView:rootFwV];//	spin is part of bound
XX			[brain boundIntoView:rootFwV];		//	Set Bounds (e.g. bounding boxes)
		}
							 				/// 4. Position rootFwV with center at orign
		rootFwV.position = [brain suggestedPositionOfSpot:-rootFwV.bounds.center()];
		
XX		[rootFwV placeLines];				/// 5. Set positions of links

		brain.boundsDirty = false;			/// 6. Mark whole tree to be reviewed (for all views)
		//self.simNsV.worldBounds = rootFwV.bounds + rootFwV.positionTranslation;// Notify the view of the 3D bounds:
		[@"" ppLog]; 							// extra newline
	}
//?	[self showWindow:self];			// Cause redisplay
	
}

@end
