//  InspecVc.h -- Manage Part Inspectors and Data Panels C170101PAK
//  An Inspect... may be either a Data Pannel or an

@class View;
@class InspecVc, InspecV, SimNsWc;

#import <Cocoa/Cocoa.h>
#import "SystemTypes.h"

@interface InspecVc : NSViewController

	   /// Deap inside NSViewController, there are variables that we use! Summary:
	  ///
	 /// Title of Inspect becomes window title (170709):
	//@property					  id			*title;

	 /// The view managed by this NsVc:
	//@property					  NSView 		*view;

	  /// The Fw View this NsVc represents:	(historically targetView)
	 /// 171229 alias fwView
	//@property					  id			representedObject;

	 /// Other superclass variables, perhaps useful for freeing
//	@property (nonatomic, retain) id			myTopLevelObjects;
	@property (nonatomic, retain) id			nibName33;


	@property (nonatomic, assignWeak) SimNsWc	*simNsWc;	   //(weak)
	@property (nonatomic, assignWeak) NSWindow	*window;  	   // STRONG 170628 // 180101 WEAK because window.contentView = (BidirNsV *)

	  // view controllers for representedObject
	 //
	@property (nonatomic, retain) NSMutableArray *subNsVcs;	   // of InspectNsVc's
	@property (nonatomic, retain) NSMutableArray *buildBidirNsVs;

	@property (nonatomic, assign) bool			viewPart;
	@property (nonatomic, assign) bool			viewView;
	@property (nonatomic, assign) bool			viewCamera;
	@property (nonatomic, assign) bool			viewSimulator;
	@property (nonatomic, assign) bool			isPortDataNsVc;// else isPartInspectNsVc

//	@property (nonatomic, assign) float			nibHeight;	   // current llc
    @property (nonatomic, assign) float			contentBottom; // 0>= y


- (InspecVc *)	initWithNibName:nibName33;

- (void) 			makeDataPannelFor:(View *)fwView;
- (void) 			makeInspectorFor:(View *)fwView;

- (id) 				aContainingWindow;

- (InspecVc *)	addSubInspectorWithNib:nibName33;

					 // all GuiViews should call this, to awaken:
- (void)			registerView:aView;

- (void)			inspectorSelected:(bool)selected;

- (IBAction)		inspectorPushButton:(id)sender;

@end


/* HOWTO make a new <CLASS>Inspector .xib (using XCode4.6.2)
 
 1. Add a <class>.xib file:
    ^n or (add new file) | os x | User Interface | 
                            kind:view |next| SaveAs: <class>.xib |create|
 2. Select Custom View:
    File Inspector [A]:				File Name --> <class>.xib (check)
    Use Auto Layout --> OFF
         [A] File Inspector     [D] Attributes      [G] Binding
         [B] Help Inspector     [E] Size            [H] Effects
         [C] Identity Inspector [F] Connections

 3. Select File's Owner, then
    [C] Identity Inspector:				Class --> NSViewController
    [F] Connection 
        Outlets						    view |circle|drag| -to-> CustomView
 
 3. Select Custom View, then
    [C] Identity Inspector:             Class --> NSView (check)
	[E] Size Inspector  				width-->368 height-->31
 
 4. Add lable to CustomView, 
	set its text to "<class>", 
	[D] TextController | blue
 
            ==================== For Each TextField: ===================
 5. Add a label (NSTextField) for variable <var1>
    *HnwObject Library | label |-->drag onto Custom View|, resize ...
 
 6. Add a value (NSTextField) for variable <var1>
    *HnwObject Library | TextField |-->drag onto Custom View|, resize ...
    Select: NSTextField
        Binding[G] | value |unfold|
            bindTo --> YES, File's Owner'
            ModelKeyPath --> self.representedObject.<var1> (no "_")
            =============================================================
 
7. Check File's Owner | [F]Connections | Reference Bindings
        ∫self.representedObject.<var1> --> value (TextField)

*/
