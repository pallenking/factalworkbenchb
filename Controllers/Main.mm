// main.mm -- main program of FactalWorkbench

#import "AppController.h"					//		    J   OO   BBB   SSSS
#import "SimNsWc.h"							//		    J  O  O  B  B  S
											//		    J  O  O  BBB   SSSS
int main(int argc, const char * argv[]) {	//		 J  J  O  O  B  B     S
	return NSApplicationMain(argc, argv);	//		  JJ    OO   BBB   SSSS
}
//    Web References Used:
// Hello, BundleTap!    http://developer.apple.com/LIBRARY/IOS/#documentation/IDEs/Conceptual/xcode_quick_start/010-Tutorial_Using_Xcode_to_Write_Hello_World/hello_world_tutorial.html
// XCode 4.1        http://developer.apple.com/library/mac/#documentation/IDEs/Conceptual/Xcode4TransitionGuide/InterfaceBuilder/InterfaceBuilder.html
// OpenGL           http://stackoverflow.com/questions/5395721/adding-an-opengl-framework-in-xcode-4
// Ogre 3D          http://will.thimbleby.net/ogre3d-tutorial/
// Mem MGT          http://developer.apple.com/library/mac/#documentation/Cocoa/Conceptual/MemoryMgmt/Articles/MemoryMgmt.html
//http://developer.apple.com/library/mac/#documentation/Cocoa/Conceptual/MemoryMgmt/Articles/mmRules.html#//apple_ref/doc/uid/20000994-BAJHFBGH


// For Royce:
//	int age = 4;
//
//	for (age = 0; age<10; age++)
//		printf("Royce is %d years old.\n", age);
//
//	return 0;

//			for (int age=0; age<7; age++)
//				printf("Royce is %d\n", age);
//		
//			exit(1);
	
// For Royce:
//	int age = 4;
//
//	for (age = 0; age<10; age++)
//		printf("Royce is %d years old.\n", age);
//
//	return 0;


//	const static int nBuff = 10000;
//	uint8_t buff[nBuff];
//
//	NSOutputStream *os = [NSOutputStream outputStreamToBuffer:buff capacity:nBuff];
//	[os open];
