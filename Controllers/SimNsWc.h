//  SimNsWc.h -- A WindowController controlling a HaveNWant Simulation C2012PAK

//		Build NSWindow for a Brain
//		Simulate Brain
//		Add a SimNsV to view in 3D

/// OUTSKI:
//		Inspectors for Part and Port
//		Keyboard Commands

/*!
	PICK,
	key command events
 */
#import <Cocoa/Cocoa.h>
#import "SystemTypes.h"

@class Brain;
@class Builder;
@class Net, Brain;
@class View;
@class Part;
@class Document;
@class SimNsV, SimNsVc;              // the OpenGL 3D view
@class RenderingContext;
@class InspecVc;
@class AppController;

#import "HnwObject.h"

@interface SimNsWc : NSWindowController

	 /// back to our owner
	@property (nonatomic, assignWeak) AppController *appController;

	 /// The Brain being simulated
	@property (nonatomic, retain) Brain			*brain;	 // a Model to simulate

	 /// The OpenGL 3D Window:
	@property (nonatomic, retain) SimNsVc 		*simNsVc;// (Only 1 right now) ViewController

	  /// Manage Inspectors:
	 // Views of pannels to be opened:
	@property (nonatomic, retain) NSMutableArray *autoOpenInspectors;// <fwView> needing inspectors
	 // Views that have been selected:
	@property (nonatomic, retain) NSMutableArray *selections;

	 // All inspectors (InspecVc or NSViewController):
	@property (nonatomic, retain) NSMutableArray *inspectNsVcs;


- (id)				initWithWindow:(NSWindow *)window;

- (void)			startSimulations;
- (void)			cycleSimulator;
- (void) 			stopSimulations;

- (bool)            processKey:(char)character modifier:(unsigned long)modifier;
- (void)			modelDispatch:(FwEvent &)fwEvent to:(View *)soughtView;

- (void)            buildAutoOpenInspectors;
- (InspecVc *) 	anInspectFor:(View *)fwView forceNew:(bool)forceNew;


@end
