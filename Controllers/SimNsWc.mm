//  SimNsWc.mm -- A WindowController controlling a HaveNWant Simulation C2012PAK

/*! TO DO:
 1. unify renderModes for paint and pic
 */

#import "Brain.h"
#import "View.h"				// why?
#import "BundleTap.h"

#import "Common.h"
#import "GlCommon.h"
#import "NSCommon.h"
#import "Id+Info.h"

#import "InspecVc.h"
#import "WorldModel.h"
#import "SimNsWc.h"
#import "SimNsV.h"
#import "SimNsVc.h"
#import "AppController.h"

#import <Foundation/NSDebug.h>
#include <stdio.h>				// sscanf
#import <GLUT/glut.h>

extern bool logNsVcAccess;

@implementation SimNsWc

#pragma mark 1. Basic Object Level					// Basic objects

- (id) initWithWindow:(NSWindow *)window; {	self=[super initWithWindow:window];

	self.simNsVc 			= [[[SimNsVc alloc] init] autorelease];
	self.simNsVc.simNsWc	= self;			// weak backpointer

	  /// Set up Window's contents
	 ///
	window.contentViewController = self.simNsVc;// retain (rc+=2)
	assert(window.contentView==self.simNsVc.view, (@"should set it!"));
	window.acceptsMouseMovedEvents = YES;

	  /// Add a Close Button
	 ///
	NSButton *closeButton	= [window standardWindowButton:NSWindowCloseButton];//	zoomButton:NSWindowZoomButton, minimizeButton:NSWindowMiniaturizeButton
	closeButton.target		= self.appController;
	closeButton.action		= @selector(closeSimWindow:);
	closeButton.enabled		= true;

	  /// INSPECTORS:
	 ///
	self.autoOpenInspectors	= @[].anotherMutable;
	self.selections			= @[].anotherMutable;
	self.inspectNsVcs		= @[].anotherMutable;

	return self;
}

- (void) dealloc {
	if (logDeallocEvents)
		printf("=================== Dealloc SimNsWc %p ===\n", self);

	self.brain				= nil;		//xyzzy21 DEALLOC
	self.simNsVc			= nil;
	
	assert(self.autoOpenInspectors.count == 0, (@"cautious"));
	self.autoOpenInspectors	= nil;

	for (InspecVc * inspectNsVc in self.inspectNsVcs) {
		printf(">>>>>>>>>>>>>>> Releasing (InspecVc *)%p.rc = %d--\n", inspectNsVc, (int)[inspectNsVc retainCount]);
	}
	self.inspectNsVcs		= nil;

	self.selections			= nil;

    [super dealloc];
}

#pragma mark  8. Reenactment Simulator
  //////////////////////////////////////////////////////////////////////////
 /////////////////////////////// TIME SIMULATION //////////////////////////
//////////////////////////////////////////////////////////////////////////
- (void) startSimulations; {

	if (self.brain.logEvents) {			// print Network to the log

		id aux = @{@"deapth":@-1, @"ppPorts":@1, @"ppParameters":@1}.anotherMutable;		//ppLinks
		[[@"" addF:@"HaveNWant Network:\n%@",   XX [self.brain ppPart:aux]   ] ppLog];
//		[@"HaveNWant Network:" ppLog];
//		pp(self.brain, 1);				//  (1: Links too)  printRootPart(1);
	}
	id name = self.brain.sourceLineInfo[@"windowName"];
	NSPrintf(@"\n\n================================================================================\n");
	[[@"" addF:@"BEGINNING SIMULATION: '%@'", name] ppLog:nil:@"="];
	NSPrintf(@"================================================================================\n\n");

	[self.brain kickstartSimulator];	// run another cycle

    [self cycleSimulator];
}

  /// Simulate a cycle of the Brain:
 ////// We are D3ViewDelegate //////
- (void) cycleSimulator {

	[self.brain cycleSimulator];					//// Simulate

	 // Test animations:
	if (self.brain.reViewConstantly)
		self.brain.someViewDirty = true;

	[[self.simNsVc.view window] display];			//// Display
    [self.simNsVc.view setNeedsDisplay:true];

	 // if more simulation is called for:
XX	[self performSelector:@selector(cycleSimulator) withObject:self afterDelay:0.0 inModes:@[NSEventTrackingRunLoopMode, NSDefaultRunLoopMode]];

}

- (void) stopSimulations; {

	id name = self.brain.sourceLineInfo[@"windowName"];
	NSPrintf(@"\n\n================================================================================\n");
	[[@"" addF:@" STOPPING  SIMULATION of '%@'", name] ppLog:nil:@"="];
	NSPrintf(@"================================================================================\n");

	[NSObject cancelPreviousPerformRequestsWithTarget:self];	// +++rc-=2
	  //  N.B: Removes perform-requests from the current run loop, not ALL run loops.

	for (InspecVc * inspectNsVc in self.inspectNsVcs) {
		printf(">>>>>>>>>>>>>>> Moving (InspecVc *)%p out of the way\n", inspectNsVc);
		inspectNsVc.window.frameOrigin = NSMakePoint(0, 0);
	}

	@autoreleasepool  {
		[self.window close];					// rc +=6 -->26
		self.window = nil;						// rc -=1 -->25
	}											// rc -=7 -->18
}

#pragma mark  7. Simulator Messages
- (bool) processKey:(char)character modifier:(unsigned long)modifier; {
	Brain *brain = self.brain;
	
	if (modifier & FWKeyUpModifier)		/////// Key UP ///////////
		 // we have no upKeys, but brain may
		return [brain processKey:character modifier:modifier];

										/////// Key DOWN /////////
	bool cmd = modifier & NSCommandKeyMask;
	if (character == 'r' and cmd)			//// cmd r ////
		panic(@"Press 'cmd r' again to rerun");		// break to debugger

	else if (character == 'o') {			//// o ////
		printf("\nOpenGl HnwObject View matrix:\n");
		ogl();										// print OGL view matrix
	}
	else if ( character == '\033' ) {		//// <escape> ////
		NSBeep();
		exit(0);								// exit program (hack: brute force)
	}
	
	else if ( character == 'v') {			//// v ////
		printf("\nViews of all Parts:\n");
		pp(self.simNsVc.rootFwV, 0);
	}
	else if ( character == 'V') {			//// V ////
		printf("\nViews of all Parts and Ports:\n");
		pp(self.simNsVc.rootFwV, 1);
	}
	else if ( character == 'W') {			//// W ////a
		printf("\nViews of all Parts, Ports, Links and Lables:\n");
		pp(self.simNsVc.rootFwV, 2);
	}
	else if ( character == 'P') {			//// P ////
		printf("\nre-Placing the Model into its Views:\n");
		brain.someViewDirty = true;
		brain.boundsDirty = true;

		[self buildAutoOpenInspectors];
		[self.simNsVc buildRootFwVforBrain:brain];
	}

	
	 //// Allow simulator to process key down stroke
XX	else if ([brain processKey:character modifier:modifier])
		int i=33;	// an event was decoded

	
	else if ( character == '?' ) {			////    ?    ////
		printf ("   === Window Controller      commands:			\n"
			"\t<esc>	-- exit program								\n"
			"\t'r'+cmd	-- rerun goes to gdb						\n"
			"\t'o'		-- print OpenGl HnwObject View matrix		\n"
			"\t'v'		-- print Views of all Parts					\n"
			"\t'V'		-- print Views of all Parts and Ports		\n"
			"\t'W'		-- print Views of all Parts, Ports and Links\n"
			"\t'P'		-- re-Place the Model into its Views		\n"
			"\t'?'		-- display this help message				\n");
		return false;
	}
	else
		return false;		// not decoded

	return true;
}

#pragma mark 8. Inspect Pannels
- (void) buildAutoOpenInspectors; {

		   //////////////////////////////////////////////////////
		  //// Building out new inspectors for guiData nibs ////
		 //////////////////////////////////////////////////////
	    ////
	   /// Process Auto-Open inspectors (perhaps a strange place to do it !^)
	  /// Find a different home for this:!!!
	 ///
	if(1) while (View *fwView 	= [self.autoOpenInspectors dequeFromHead] ) {
	
		  // Create INSPECTOR for Port Data:
		 //
XX		InspecVc *inspectNsVc =[self anInspectFor:fwView forceNew:false];
		inspectNsVc.simNsWc 	= self;			// weak backpointer

		 // Place in a Window, and retain that window
		NSWindow *window		= [inspectNsVc aContainingWindow];		// OLD

		 // seems not to work!
		[window makeKeyAndOrderFront:self];		//xyzzy22 mkaof
		 window.releasedWhenClosed = true;		//xyzzy22 //
		[window display];
	}
}


 // Create an inspect pannel for Class Inspector or DataPannel
- (InspecVc *) anInspectFor:(View *)fwView forceNew:(bool)forceNew; {
	mustBe(View, fwView);
	Part *fwPart 			= coerceTo(Part, fwView.part); // xyzzy14

	  /// 1. DESELECT existing inspector controllers
	 ///
	for (InspecVc *vc in mustBe(NSArray, self.inspectNsVcs))
		[mustBe(InspecVc, vc) inspectorSelected:FALSE];	// deselect all existing inspectors

	BundleTap 	*bundleTap 	= coerceTo(BundleTap, fwPart);
	BundleTap 	*guiBTap	= bundleTap.inspectorNibName? bundleTap: nil;
	InspecVc *rvNsVc 	= nil;				// return value

	   /// 2. FIND a Base InspectNsWc to use
	  ///
	 /// See if we can use an existing controller for soughtView.
	if (!forceNew) {

		 // See if there is an existing inspector for soughtView exists
		id existingInspector = nil;
		for (InspecVc *inspNsVc in self.inspectNsVcs) {		// simple to use last item found
			mustBe(InspecVc,	inspNsVc);
 
			 /// Seeking Port Data pannel; found one matching fwView:
			if (inspNsVc.isPortDataNsVc  and  guiBTap)
				if (inspNsVc.representedObject==fwView)	// matches us
					rvNsVc = inspNsVc;							// use it

			 /// Seeking Part Inspector; Found any Part inspector
			if (!inspNsVc.isPortDataNsVc and !guiBTap)
				rvNsVc = inspNsVc;							// Any one works
		}
	}

	if (rvNsVc ) {			/// RECYCLE existing baseNsVc:

		  /// REMOVE previous InspectNsVcs from our base NSViewController
		while (InspecVc *subNsVc	= rvNsVc.subNsVcs.dequeFromHead)
			[subNsVc.view removeFromSuperview];				// remove it from superview

		 // Restore old frame:
		NSView *rvNsV				= rvNsVc.view;
		float  height				= rvNsVc.contentBottom;
		NSRect rvNsVFrame			= rvNsV.frame;
		rvNsVFrame.size.height	   += height;
		rvNsV.frame					= rvNsVFrame;

		if (self.brain.logNsVcAccess) {
			[[@"" addF:@"After RECYCLE of %@:               rv.view.frame =%@",
							fwView.name, NSStringFromRect(rvNsV.frame)] ppLog];
			[[@"" addF:@"                                   rv.view.bounds=%@",
										NSStringFromRect(rvNsV.bounds)] ppLog];
		}
		 // checks:
		assert(rvNsVc.window, (@"recycled InspecVc should have a window by now"));
		assert(rvNsVc.window.contentView==rvNsVc.view, (@"paranoia"));
	}

	  /// Get an InspecVc (or make one) and fill it in
	 ///
	if (id guiNib = guiBTap.inspectorNibName) {	// Make a GUI PORT DATA pannel
		if (rvNsVc == nil)
XX			rvNsVc = [[[InspecVc alloc] initWithNibName:guiNib] autorelease];
		[rvNsVc makeDataPannelFor:fwView];
	}
	else {										// Make a GUI PART INSPECTOR pannel
		if (rvNsVc == nil)
XX			rvNsVc = [[[InspecVc alloc] initWithNibName:@"InspectBase"] autorelease];
		[rvNsVc makeInspectorFor: fwView];		// a PART INSPECTOR   is returned
	}
	[self.inspectNsVcs addObjectIfAbsent:rvNsVc];

	return rvNsVc;
}

#pragma mark 3D Pick
//	   ////////////////////////////////////////////////////////////////////////
//	  //////////////////////////////// PICKING ///////////////////////////// //
//	 //////////////////////////////////////////////////////////////////////  //
//  //http://www.glprogramming.com/red/chapter05.html						 //
// //http://programmingexamples.net/wiki/OpenGL/ColorPick					 //
////http://www.opengl.org/wiki/How_lighting_works							 //
/// NOT DOING IT THIS WAY: http://web.engr.oregonstate.edu/~mjb/cs553/Handouts/Picking/picking.pdf
//http://www.lighthouse3d.com/opengl/picking/index.php3?color1				 //

static int debugPrint34 = 1;		// ad hoc/hoakey debug
#define dNSPrintf if (debugPrint34) NSPrintf

- (void) modelDispatch:(FwEvent &)fwEvent to:(View *)soughtView; {

	if (fwEvent.clicks == 1) {			////    SINGLE/FIRST CLICK    /////

		  /// Make INSPECTOR for soughtView
		 ///
		bool altKey 		= fwEvent.modifierFlags & NSAlternateKeyMask;

XX	   InspecVc *inspNsVc= [self anInspectFor:soughtView forceNew:altKey];
		inspNsVc.simNsWc 	= self;		// weak backpointer

		 // Place in a Window, and retain that window
		NSWindow *w = [inspNsVc aContainingWindow];

		  /// Add soughtView to SELECTION, (perhaps clear Selection first)
		 ///
		if (!(fwEvent.modifierFlags & NSShiftKeyMask)) {// if not SHIFT key: Deselect all
			 // We may not be the only user of a View in .selections. (BUGGY)
			for (View *s in self.selections)					// all previous selections
				s.isSelected = 0;								// get deselected
			[self.selections removeAllObjects];
		}
		[self.selections addObject:soughtView];			// select this object
		soughtView.isSelected |= 1;

		  /// Pan the CENTER OF INTEREST to the center of the picked object
		 ///
		SimNsV *simNsV = mustBe(SimNsV, self.simNsVc.view);
		if (self.brain.picPan)
			if (!isNan(simNsV.renderingContext.soughtViewsCenter)) {
				Vector3f ctr = simNsV.renderingContext.soughtViewsCenter;
				dNSPrintf(@"   Center at %@\n", pp(ctr));
				simNsV.interestPoint = ctr;
			}

	}
	else if (fwEvent.clicks == 2) {		///// DOUBLE CLICK or DOUBLE DRAG   /////
		
		  //// Let fwPart handle it:
		 ///
		dNSPrintf(@"-------- mouseDragged (click %d)\n", (int)fwEvent.clicks);

		 /// Process the Event to the picked Part's View:
		Part *m = soughtView.part;

XX		[m sendPickEvent:&fwEvent toModelOf:soughtView];

XX		[self.simNsVc buildRootFwVforBrain:self.brain];	// model may have changed, so remake view
		[self buildAutoOpenInspectors];
	}
}

#pragma mark 13. IBActions							// [[shift around]]

- (BOOL)windowShouldClose:(id)sender; {
	NSPrintf(@"SimNsWc windowShouldClose");
	return true;//[super windowShouldClose:sender];
}
- (void) windowWillClose:(NSNotification *)notification; {
	NSPrintf(@"SimNsWc windowShouldClose");
	//return [super windowWillClose:notification];
}

@end
