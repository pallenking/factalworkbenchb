//  GuiXibViewController.mm -- View Controllers for GuiGenerator .xib's 2015PAK
#ifdef outski
//
//#import "GuiXibViewController.h"
//#import "SliderBidirView.h"
//#import "NeckerBidirView.h"
//#import "View.h"
//#import "Generator.h"
//#import "Id+Info.h"
//#import "Bundle.h"
//#import "Path.h"
//#import "Brain.h"
//#import "Common.h"
//#import "NSCommon.h"
//
//@implementation GuiXibViewController
//
//- (GuiXibViewController *) initWithNibName:(NSString *)nibNameOrNil
//										bundle:(NSBundle *)nibBundleOrNil ; {
//	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//	self.registeredClients = @[].anotherMutable;
////	@try {////////////
////		[self loadView];
////	  }	  ////////////
////	@catch (NSException * e) {
////		return (id)panic("Loading ERROR '%s'", e.reason.UTF8String);
////	  }
//
//	return self;
//  }
//
// // Clients are GuiViews, and all should call this when awakening
//- (void) registerView:aView; {
//
//	 // register aView if it is the first encounter
//	if ([self.registeredClients indexOfObject:aView] == NSNotFound) {
//XX		[self.registeredClients addObject:aView];
//	  }
//  }
//
//  // Wire up GuiViews:
//- (void) setRepresentedObject:(View *)guiEnvView;	{	[super setRepresentedObject:guiEnvView];
//
//	 // Insure Represented Object is a View
//	assert(coerceTo(View, guiEnvView), ("setting representedObject to nil!"));
//	if (Generator *guiEnvPart = coerceTo(Generator, guiEnvView.part)) {
//
//		 // If Generator has targetBundle
//		if (Bundle *targetBundle = guiEnvPart.targetBundle) {
//			targetBundle = mustBe(Bundle, guiEnvPart.targetBundle);
//			[[@"" addF:@"Generator '%@'s has target bundile '%@'", guiEnvPart.fullName, targetBundle.fullName] ppLog];
//
//			////////////// ////////////// ////////////// ////////////// //////////////
//			//////////////             HOOK UP GUI SLIDERS				//////////////
//			////////////// ////////////// ////////////// ////////////// //////////////
//
//			 // Go through all registered BidirSliders:
//			for (id aGuiView in self.registeredClients)
//				  //// BUILD OUT Each registered object:
//				[aGuiView buildBidirViewWithTarget:targetBundle];
//		  }
//		else
//			[[@"" addF:@"Generator '%@'s target bundile is nil", guiEnvPart.fullName] ppLog];
//	  }
//  }
//
//@end
#endif
