//  SimNsVc.h -- View Controller for a 3D view of the simulation C2017PAK

#import <Cocoa/Cocoa.h>
#import "SystemTypes.h"
@class View, Brain, SimNsWc;

@interface SimNsVc : NSViewController

	@property (nonatomic, assignWeak) SimNsWc *simNsWc;		// weak backpointer

	   /// Deap inside NSViewController, there are variables that we use! Summary:
	  ///
//	 // Title of Inspect, becomes window title:
//	//@property					id		*title;

	 /// The view managed by this NsVc:
	//@property					id		*view;

	 /// The targetView (a FactalWorkbench View class) this SimNsVc represents:
	//@property					id		*representedObject;		// ==? rootFwV


    @property (nonatomic, retain) View	*rootFwV;		// current (FW)View of Model

- (void)			buildRootFwVforBrain:(Brain *)brain;

@end
