//  AppController.mm -- FactalWorkbench's app delegate C20130

#import "AppController.h"
#import "Model01.h"
#import "Brain.h"
#import "View.h"
#import "SimNsV.h"
#import "SimNsVc.h"
#import "SimNsWc.h"
#import "FactalWorkbench.h"

#import "Id+Info.h"
#import "Common.h"
#import "NSCommon.h"
#import "time.h"


//	int majorVersion = 2, minorVersion = 991;	/// Until 160726
//	int majorVersion = 3, minorVersion = 001;	/// 160726: Release 3.001
//	int majorVersion = 3, minorVersion = 001;	/// 160826: Release 3.1
//	int majorVersion = 3, minorVersion = 002;	/// 170116: Release 3.2
//	int majorVersion = 3, minorVersion = 003;	/// 170315: Release 3.3
	int majorVersion = 3, minorVersion = 004;	/// 170315: Release 3.4 Morse, Improved
												/// 170315:	updated bitbucket
// new idea for printout
//	NSLog(@"💙 My application: %@", NSStringFromClass([self class]));
//	NSLog(@"\e[1;31mRed text here\e[m normal text here"); (doesn't work)


@implementation AppController

#pragma mark 1. Basic Object Level					// Basic objects

- (void)dealloc {
	self.simNsWc		= nil;
	self.sceneMenu		= nil;
	self.regressMenu	= nil;

	[super dealloc];
}

#pragma mark Application Bindings

-(void) applicationWillFinishLaunching:(NSNotification *)aNotification {

	  // Set Apple Event Manager so FactalWorkbench will recieve URL's
	 //	   OS X recieves "factalWorkbench://a/b" --> activates network "a/b"
	NSAppleEventManager *appleEventManager = [NSAppleEventManager sharedAppleEventManager];
	[appleEventManager setEventHandler:self
		andSelector:@selector(handleGetURLEvent:withReplyEvent:)
		forEventClass:kInternetEventClass andEventID:kAEGetURL];
}

 // (main() calls NSApplicationMain(), and then ends up here)
- (void)applicationDidFinishLaunching:(NSNotification *)notification   {
	NSLog(@"Factal Workbench -- Version %d.%d", majorVersion, minorVersion);
	NSPrintf(@"\n");
	
	 // Test Factal Workbench is not using ARC.
	id x = [[[NSObject alloc] init] autorelease];	// Errors if using ARC (illegal)

	[HnwObject resetObjectMonitors];		// pre
	
	if (1) {
		NSLog(@"  Looking for  Network in Model01.mm designated 'x'");
		NSPrintf(@"\n\n");

		   ////// BUILD Simulation Part selected by "x"
		  ///
XX		if (Brain *brain = aBrain_selectedBy(-1, -1, nil)) {
			[self logRunInfo:brain];

			 // Build a window for brain
XX			self.simNsWc = [self createASimNsWcFor:brain :@"Selected by 'x' in UserCode"];
		}
		else
			NSPrintf(@"		Use Scene menu to load a test.\n"
					  "       or designate a Network with 'x' in Model01.mm\n\n");
	}
	 /// DEBUG recieving URL by sending one:
	else if (1) {
		NSString* url = @"factalWorkbench://A. Small Machines/A. Umbrella";
		NSPrintf(@"\n");
		NSLog(@"applicationDidFinishLaunching: Debug option conjures openURL '%@'", url);

		url = [url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
		NSURL* urlUrl = [NSURL URLWithString:url];
XX		[self openURL:urlUrl];
	}
	else
		NSLog(@"   No Network selected");

	[self buildSceneMenu];
	
	  ////// Set first menue test from prefs
	id userPrefs = userSimulationPreferences();
	id curMenueTestNumber = userPrefs[@"curMenueTestNumber"];
	assert([curMenueTestNumber asString], (@""));
	self.curMenueTestNumber = [curMenueTestNumber intValue];

	  ////// Set first regress test from prefs
	id curRegressTestNumber = userPrefs[@"curRegressTestNumber"];
	assert([curRegressTestNumber asString], (@""));
	self.curRegressTestNumber = [curRegressTestNumber intValue];

	[self buildRegressionMenu];

	 // WAY OUT OF PLACE
	[self.simNsWc.window makeKeyAndOrderFront:self.simNsWc];//[window display]; // displayIfNeeded
	[self.simNsWc.window display];
}

- (void)handleGetURLEvent:(NSAppleEventDescriptor *)event withReplyEvent:(NSAppleEventDescriptor *)replyEvent {
	NSString *s	= [[event paramDescriptorForKeyword:keyDirectObject] stringValue];
	NSLog(@"\nhandleGetURLEvent:'%@'", s);

	NSURL *url	= [NSURL URLWithString:s];
	[self openURL:url];
}

- (void)openURL:(NSURL *)url {
	NSString *urlStr = url.absoluteString.stringByRemovingPercentEncoding;
	NSPrintf(@"\n");
	NSLog(@"openURL:%@ -> %@", url, urlStr);
	[HnwObject resetObjectMonitors];		// pre

	NSString *prefix = @"factalworkbench://";
	assert([[urlStr lowercaseString] hasPrefix:prefix], (@"URL does not have prefix '%@'", prefix));
	urlStr = [urlStr substringFromIndex:[prefix length]];

	   ////// BUILD Simulation Part per received URL.
	  ///
XX	if (Brain *brain = aBrain_selectedBy(-1, -1, urlStr)) {
		[self logRunInfo:brain];

		 // Build a window; install brain
XX		self.simNsWc = [self createASimNsWcFor:brain :@"Selected by factalWorkbench:// URL"];
	}
	else
		NSLog(@"URLString '%@' has no menu item", urlStr);
}


- (SimNsWc *) createASimNsWcFor:(Brain *)brain :(NSString *)banner; {

		/**/// http://stackoverflow.com/questions/15225508/display-window-on-all-screens :
	   /**/// NSWindow has a special initializer to specify which screen you want it on.
	  /**/// initWithContentRect:styleMask:backing:defer:screen:
	 /**/// The other form of the initializer assumes the main screen
	NSRect simsFrame_ = simsFrame(nibsWidth);	//  Compute Sim's Frame

	 // Build Window
XX	NSWindow *window = [[[NSWindow alloc]										// rc=11
		initWithContentRect	:simsFrame_
		styleMask			:NSTitledWindowMask | NSResizableWindowMask | NSMiniaturizableWindowMask
		backing				:NSBackingStoreBuffered
		defer				:NO			] autorelease];
	window.frameOrigin		= simsFrame_.origin; // WHY must I do this?
	window.title 			= brain.sourceLineInfo[@"windowName"];

	 // Build Window Controller
XX	SimNsWc *simNsWc		= [[[SimNsWc alloc] initWithWindow:window] autorelease];		// rc+=5
	self.simNsWc			= simNsWc;		// rc++
	 simNsWc.appController 	= self;			// weak backpointer
	simNsWc.brain 			= brain;		// HnW Network
	 brain.simNsWc			= simNsWc;		// weak backpointer

	 // Build Root View
XX	[simNsWc.simNsVc buildRootFwVforBrain:brain]; /// Create a (fw)rootFwV from model
	SimNsV *simNsV3			= coerceTo(SimNsV, simNsWc.window.contentView);
	[simNsV3 reshape];

	 // Build Inspectors
XX	[simNsWc buildAutoOpenInspectors];			// weak ref to self

	 // Move   Camera Configuration  from brain to simNsV
	SimNsV *simNsV 			= mustBe(SimNsV, simNsWc.simNsVc.view);	//window.contentView
	simNsV.cameraConfig		= brain.camera; brain.camera=nil;		// move it


XX	[simNsWc startSimulations];				// rc+=2 -->3: performSelector

	[window makeKeyAndOrderFront:self];		// rc++
	[window displayIfNeeded];	// displayIfNeeded///display
	return simNsWc;
}

- (void) logRunInfo:(Brain *)brain; {

	  /////  << LOG >>  each run, for posterity!
	 ///
	NSString *logOfRuns = @"logOfRuns";
	 // Locate file in dataDirectory, if it exists
	id dataDirectory = brain.dataDirectory;
	if (dataDirectory and [dataDirectory length])
		logOfRuns = [@"" addF:@"%@%@", dataDirectory, logOfRuns];
	 // 161000 didn't work:
	//[[[NSFileManager alloc] init] changeCurrentDirectoryPath:dataDirectory];
	//id currentpath = [[[NSFileManager alloc] init] currentDirectoryPath];
	FILE *breakLogNumberFile = fopen(logOfRuns.C, "a+");
	assert(breakLogNumberFile, (@"unable to open log file '%@' for writing: '%s'",
											logOfRuns, strerror(errno)));
//	fprintf(breakLogNumberFile, "%s, %d %s\n", fWallTime((char *)"%y%m%d.%H%M%S"),
//						brain.breakAtLog, [brain.sourceLineInfo[@"windowName"] C]);
	fclose(breakLogNumberFile);
}


#pragma mark 10. Placement
 //// MANAGE Screens, and main and aux frames
NSScreen *simsScreen() {			// screen the Demo simulations
	NSArray *screens = [NSScreen screens];
	int len = (int)screens.count;
	assert(len>0, (@"no screens"));
	NSScreen *screen = [screens objectAtIndex:len>1]; // [1], if it exists, else [0]
	assert(screen, (@""));
	return screen;
}

NSRect simsFrame(float nibsWidth) {
	 //// Compute Sim's Frame
	NSScreen *screen = simsScreen();
	NSRect simsFrame = NSMakeRect(0,260, 300,400);	// for small (!big) Screen
	bool simsScreen = Brain.currentBrain.simsScreen;
	if (simsScreen) {
		simsFrame = screen.frame;
		simsFrame.size.width -= nibsWidth;			// decrease sim's width by nib's
	}
	return simsFrame;
}

#pragma mark 13. IBActions							// [[shift around]]
	// NEW SEMANTICS (160828: NOT YET WORKING)
	//  e.g: with currentPath of "/a/b":
	//       "///newPath" --> "/a/b/newpath"
	//		 "//newPath"  --> "/a/newpath"
	//		 "/newPath"   --> "/newpath"

	 /// SCENES
- (void) buildSceneMenu  {
	id knownParentMenus = @{@"":self.sceneMenu}.anotherMutable;

	 // Go through all defined tests in testLibrary, indexed by menuIndex
	for ( int menuIndex=1; true; menuIndex++ ) {
		id menusMetaInfo=@{}.anotherMutable;

		 // Get the next TEST:
XX		accessTestLibrary(menuIndex, -1, nil, false, menusMetaInfo);// for Menu.Scene
		if (!menusMetaInfo[@"found"])
			break;						// done, exit loop

		 // Make new menue item with name from test library:
		id name = mustBe(NSString, menusMetaInfo[@"name"]);
		NSMenuItem *menuItem = [[NSMenuItem alloc] initWithTitle:name
							action:@selector(scheneAction:) keyEquivalent:@""];
		menuItem.tag = menuIndex;

		 // find our Parent (sub-)menu:
		NSString *sceneMenuePathStr = coerceTo(NSString, menusMetaInfo[@"sceneMenuePath"]?:@"huh");
		NSMenu		*sceneMenuePath = knownParentMenus[sceneMenuePathStr];

		if (sceneMenuePath == nil) {		// sceneMenuePath doesn't exist
			 // make a Sub Menu Item for the parent
			NSMenuItem *newParentMenuItem = [[NSMenuItem alloc]
					initWithTitle:sceneMenuePathStr
					action:@selector(scheneAction:)
					keyEquivalent:@""];
			[self.sceneMenu addItem:newParentMenuItem];	// insert into base (currently)

			 // make the menu of the subMenu:
			id parentSubMenu = [[[NSMenu alloc] initWithTitle:@"NFI"] autorelease];
			newParentMenuItem.submenu			= parentSubMenu;
			sceneMenuePath						= parentSubMenu;
			knownParentMenus[sceneMenuePathStr] = parentSubMenu; // remember the subMenu:
		}

		assert(sceneMenuePath!=nil, (@""));
		[sceneMenuePath addItem:menuItem];
	}
}

- (void) closeSimWindow:sender; {
//	SimNsWc *wc = self.simNsWc;
//	NSPrintf(@"\n\n===== closeSimWindow: %@ %lX \"%@\" =====\n", wc.className, (LUI)wc, wc.window.title);

	[self.simNsWc stopSimulations];						// rc -=2
	self.simNsWc								= nil;	// rc--
}

   ////////// Build new Scenario Window from Menu tag number
 ///
- (IBAction) scheneAction:(NSMenuItem *)sender {

	[self closeSimWindow:nil];

	[self performSelector:@selector(scheneAction2:) withObject:sender
		afterDelay:0.0 inModes:@[NSEventTrackingRunLoopMode, NSDefaultRunLoopMode]];

}
 /// Go through the event loop, so previous scene can go away, then come back
- (IBAction) scheneAction2:(NSMenuItem *)sender {

	[HnwObject resetObjectMonitors];

	int num = (int)[sender tag];
	NSPrintf(@"\n");
	NSLog(@"scheneAction: for Menu item number %d", num);

	   ////// BUILD Simulation Part per Scene Menue
	  ///
XX	if (Brain *brain = aBrain_selectedBy(num, -1, nil)) {
		[self logRunInfo:brain];

		 // Build a window; install brain
XX		self.simNsWc = [self createASimNsWcFor:brain :@"Scene Menu action"];
	}
	else
		printf("\n***** building menu number %d failed *****\n\n\n", num);
}
	 /// Regression Test Menue
- (void) buildRegressionMenu  {
	NSMenuItem *menueTestMenuItem = [self.regressMenu itemAtIndex:0];
	menueTestMenuItem.title = [@"" addF:@"Current Menue Test %d ++", self.curMenueTestNumber];

	NSMenuItem *regressTestMenuItem = [self.regressMenu itemAtIndex:1];
	regressTestMenuItem.title = [@"" addF:@"Current Regress Test %d ++", self.curRegressTestNumber];
}

- (IBAction) regressAction:(NSMenuItem *)sender {

	[self closeSimWindow:nil];

	[self performSelector:@selector(regressAction2:) withObject:sender
		afterDelay:0.0 inModes:@[NSEventTrackingRunLoopMode, NSDefaultRunLoopMode]];

}
 /// Go through the event loop, so previous scene can go away, then come back
- (IBAction) regressAction2:(NSMenuItem *)sender {

	[HnwObject resetObjectMonitors];

	int testKind = (int)[sender tag];
	if (testKind == 0) {				// Menue
		NSPrintf(@"\n\n\n\n");
		NSLog(@"regressAction: Menue Number = %d\n", self.curMenueTestNumber);

		   ////// BUILD Simulation Part per MENU ^m key
		  ///
XX		if (Brain *brain = aBrain_selectedBy(self.curMenueTestNumber, -1, nil)) {
			if (![brain.sourceLineInfo[@"name"] isEqualToString:@"LastMachine"]) {
				[self logRunInfo:brain];

				 // Build a window; install brain
XX				self.simNsWc = [self createASimNsWcFor:brain :@"Menue Test"];

				self.curMenueTestNumber++;
			}
			else
				printf("\n\nALL %d Menue Tests FINISHED\n", self.curMenueTestNumber);
		}
		else
			printf("Failed to build Menue Test %d\n", self.curMenueTestNumber);
	}
	else if (testKind == 1) {			// Regression
		NSPrintf(@"\n\n\n\n");
		NSLog(@"regressAction: Regress Number = %d", self.curRegressTestNumber);

		   ////// BUILD Simulation Part per REGRESSION ^r key
		  ///
		if (Brain *brain = aBrain_selectedBy(-1, self.curRegressTestNumber, nil)) {
			if (![brain.sourceLineInfo[@"name"] isEqualToString:@"LastMachine"]) {
				[self logRunInfo:brain];

				 // Build a window; install brain
XX				self.simNsWc = [self createASimNsWcFor:brain :@"Regress Test"];

				self.curRegressTestNumber++;
			}
			else
				printf("\n\nALL %d Regression Tests FINISHED\n", self.curRegressTestNumber);
		}
		else
			printf("Failed to build Regression Test %d\n", self.curRegressTestNumber);

	}
	else
		panic(@"");

	return [self buildRegressionMenu];
}

#pragma mark 15. PrettyPrint
void pp(NSScreen *screen) {			// print a screen
	NSArray *screens = [NSScreen screens];
	int ind = screens[0]==screen? 0: screens[1]==screen? 1: -1;
	NSRect f = screen.frame;
	NSPrintf(@"screen[%d]: (%.0f, %.0f, %.0f, %.0f)\n", ind,
			f.origin.x, f.origin.y, f.size.width, f.size.height);
}
void ppNSScreens() {				// print all screens
	for (NSScreen *screen in [NSScreen screens])
		pp(screen);
}
@end
