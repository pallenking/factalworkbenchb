//  InspecVc.mm -- Manage Part Inspectors and Data Panels C170101PAK

#import "InspecVc.h"
#import "InspecV.h"
#import "Part.h"
#import "Bundle.h"
#import "BundleTap.h"
#import "Splitter.h"
#import "WorldModel.h"
#import "Brain.h"

#import "View.h"
#import "Id+Info.h"
#import "Common.h"
#import "NSCommon.h"

#import "SimNsV.h"
#import "SimNsVc.h"
#import "SimNsWc.h"
#import "BidirNsV.h"
#import "AppController.h"

@implementation InspecVc

bool logNsVcAccess = false;//true;//			// Noisy operation for debug;
												// (plus inspecting messes up log numbers)

#pragma mark 1. Basic Object Level				// Basic objects

  /// INITIALIZE NSViewController:
 ///
- (InspecVc *) initWithNibName:nibName33; {
	self.nibName33 			= mustBe(NSString, nibName33);
	self.buildBidirNsVs		= @[].anotherMutable;
	self.viewPart			= true;
	self.subNsVcs			= @[].anotherMutable;

	if ([Brain currentBrain].logNsVcAccess)
		[[@"" addF:@"InitWithNib for nib '%@'", self.nibName33] ppLog];

	/* 180113 Note:
	When I load a nib into an NSViewController (NsVc),
		the loading appears to (somewhat gratuitously) retain the the calling NsVc.
	The loading can be either done simply by using
		the standard initWithNibName:bundle: method and then accessing self.view,
		or by manually using NSBundle's loadNibNamed:owner:topLevelObjects.
			180117: removed tlo options
	If I add a [self release] after the load, I get proper behavior,
		but I'm not sure why.
	*/

XX	self 					= [super initWithNibName:nibName33 bundle:nil];

	@try 					 {	[self view]; }		// if !bun: rc++ --> 2
	@catch (NSException * e) {	panic(@"Loading '%@.nib': ERROR:\n%@", self.nibName33, e.reason);}

	return self;
}

- (void) dealloc {
//	printf("\n"
//	"==========================================\n"
//	"========= [(InpsectNsVc *)%lx dealloc] ====\n"
//	"==========================================\n"
//	"\n\n", (LUI)self);
	//panic(@"");
//	self.myTopLevelObjects	= nil;
	self.nibName33			= nil;
	self.window				= nil;
	self.buildBidirNsVs		= nil;
	self.subNsVcs			= nil;

    [super dealloc];
}

- (void) loadView {			// Just for debug
	[super loadView];
}

		  //////////////////////////////////////////////////
		 ////    Make a WINDOW for the InspecVc     ////
		//////////////////////////////////////////////////

- (id) aContainingWindow; {
	View *fwView 				= mustBe(View, self.representedObject);

	if (self.window == nil) {		// only if newly made, bust be done after rv.bounds set

		int	selfRc0				= (int)[self retainCount];
XX		id win					= [NSWindow windowWithContentViewController:self];// 180101 rc=12
		if (logNsVcAccess)
			[[@"" addF:@">>>>>>>>>>>>>>> windowWithContentViewController (InspecVc *)%p.rc rc%d->rc%d",
							self, selfRc0, (int)[self retainCount] ] ppLog];
		
		self.window 			= win;	//xyzzy22 win
		if (logNsVcAccess)
			[[@"" addF:@"New window for %@:               rv.window.frame =%@",
						fwView.name, NSStringFromRect(self.window.frame)] ppLog];

			 /////////////////////////////////
			/// CREATE Inspector's Window ///
		   /////////////////////////////////
		  // position on right side of Sim's Main Frame
		 // frameHeightRation shows how far up
		float heightAlongRightSide = 0.5;			// in the middle

		Part   		*fwGuiGen	= coerceTo(Part,      fwView.part); // xyzzy14
		NSView 		*nibView	= coerceTo(NSView,    self.view);		//BidirNsV *nibView		= coerceTo(BidirNsV, rv.view);

		 /// POSITION: Place the Data Pannels:
		WorldModel 	*worldModel = coerceTo(WorldModel, fwView.part);
		BundleTap 	*bundleTap 	= coerceTo(BundleTap,  fwView.part);
		BundleTap 	*guiBTap	= bundleTap.inspectorNibName? bundleTap: nil;
	 	if (guiBTap) {
			heightAlongRightSide= guiBTap.heightAlongRightSide; // from Generator
//			if (isNan(heightAlongRightSide))							 // from nib:
//				heightAlongRightSide= [nibView.heightAlongRightSide floatValue];

			 /// Place with hint from name:
			if (isNan(heightAlongRightSide)) {							 // from name
				if ([self.nibName33 hasPrefix:@"Hi"]) // Hi* nibs
					heightAlongRightSide = 1;				// are placed on top
				else if ([self.nibName33 hasPrefix:@"Lo"])// Lo* nibs
					heightAlongRightSide = 0;				// are placed on bottom
				else
					heightAlongRightSide = 0.5;		// silent
			}
		}

		NSSize  nibsSize			= nibView.frame.size;
		NSRect  simsFrame_			= simsFrame(nibsSize.width);
		NSPoint pannelOrigin		= simsFrame_.origin;	// start at origin of main frame

		pannelOrigin.x		   		+= simsFrame_.size.width;		 // move to right of frame
		pannelOrigin.y		   		+= heightAlongRightSide * (simsFrame_.size.height - nibsSize.height);

		self.window.frameOrigin		= pannelOrigin;

		if (logNsVcAccess)
			[[@"" addF:@"Adjust position of %@:           rv.window.frame =%@\n",
						fwView.name, NSStringFromRect(self.window.frame)] ppLog];
	}
	  				 //////////////////////////////
	else {			/// RECYCLE an old Window: ///
				   //////////////////////////////
		self.window.contentSize	= self.view.bounds.size;
		if (logNsVcAccess)
			[[@"" addF:@"Recycled window for %@:          rv.window.frame= %@",
						fwView.name, NSStringFromRect(self.window.frame)] ppLog];
	}

//	[self.window orderFront:self];
	[self.window makeKeyAndOrderFront:self];
	[self.window display];

	return self.window;
}

 // make self represent a Data Pannel
- (void) makeDataPannelFor:(View *)fwView; {
	BundleTap *bundleTap 		= coerceTo(BundleTap, fwView.part);

	 // Connect Inspector to fwView
XX	self.representedObject		= fwView;

	self.isPortDataNsVc			= true;
	self.title = [NSString stringWithFormat:@"%@: (%@ *)%@",
		bundleTap.inspectorNibName,
		fwView.part.className,
		fwView.part.name
		];
	[self inspectorSelected:TRUE];
}

 // make self inspect an object
- (void) makeInspectorFor:(View *)partsView; {

	  /// Add Inspectors for all subclasses, down to HnwObject
	 ///
	self.contentBottom			= 0;
	self.representedObject		= partsView;

	 /// View PART and its SUBCLASSES:
	Part *part					= coerceTo(Part, partsView.part);// xyzzy14
	if (self.viewPart and part) {
		Class curClass			= [part class];
		for (;   curClass!=[NSObject class];   curClass=[curClass superclass]) {
			NSString *iNibName	= [curClass.description addF:@"Inspector"];

XX			InspecVc *rvNsVc = [self addSubInspectorWithNib:iNibName];
XX			rvNsVc.representedObject= partsView;

			 /// After PART: add its sub-parts
			if (curClass == [Part class]) {
				for (Port *subPart in part.parts) {
					if (View *subView  = coerceTo(View, [partsView subViewFor:subPart])) {
						NSString *iNibName	= coerceTo(Port, subPart)?
									@"ShortPortInspector": @"ShortPartInspector";
XX						InspecVc *rvNsVc2 = [self addSubInspectorWithNib:iNibName];
XX						rvNsVc2.representedObject= subView;
					}
				}
				break;
			}

			 /// After BRAIN: quit, don't show superclasses, it's too much
			if (curClass == [Brain class]) // expedient: Brain pannels too big
				break;
		}
	}

	 /// View VIEW HIERARCHY
	if (self.viewView) {
XX		InspecVc *rvNsVc		= [self addSubInspectorWithNib:@"ViewInspector"];
XX		rvNsVc.representedObject= partsView;
	}

	 /// View 3D / Camera
	if (self.viewCamera) {
XX		InspecVc *rvNsVc		= [self addSubInspectorWithNib:@"CameraInspector"];
XX		rvNsVc.representedObject= self.simNsWc.simNsVc.view;
	}

	self.isPortDataNsVc			= false;
	self.title					= [NSString stringWithFormat:@"(%@ *)%@ : %@",
								  partsView.part.className, partsView.part.name, partsView.name];

	  // nibs for inspector pannels for subclasses, View and SimNsV
	 //   extend below 0 by -height (height<0).
	float  height				= self.contentBottom;
	NSView *baseNsV				= self.view;

	NSRect baseNsVFrame			= baseNsV.frame;	// what it occupies in NsW's coords
	baseNsVFrame.size.height   -= height;
	baseNsV.frame				= baseNsVFrame;

	if (logNsVcAccess) {
		[[@"" addF:@"Resize %@ after subVs:           base.view.frame =%@",
							partsView.name, NSStringFromRect(baseNsV.frame)] ppLog];
		[[@"" addF:@"                                 base.view.bounds=%@",
										NSStringFromRect(baseNsV.bounds)] ppLog];
	}
}

#pragma mark  2. 1-level Access
// setView is early, before NidirNsV.inspectNsVc is set
- (void) setView:(NSView *)view;	{				[super setView:view];

	   // N.B: below, use view_. Using accessor self.view causes infinite loop:
	  //	as read of self.view somehow (in NS land) causes view to be set,
	 //      which gets us back here.
	if (logNsVcAccess)
		[[@"" addF:@"InspecVc for nib '%@' setView (%@ *)%lx",
		 [@"" addN:20 F:@"%@", self.nibName33], view.class, (LUI)self.view] ppLog];

	 // ??? 170122: removed temporarily, added back, removed again 171110
	//[self registerView:view_];
}

//- (void) setInspectNsVc:(NSView *)view_;	{ // too early, before NidirNsV.inspectNsVc is set
//	panic(@"setInspectNsVc undebugged");
//}

- (void)viewDidLoad;	{								[super viewDidLoad];
	if (logNsVcAccess) {
		id nibName33 = [@"" addN:20 F:@"%@", self.nibName33];
		[[@"" addF:@"InspecVc for nib '%@' viewDidLoad", nibName33] ppLog];
	}
}

- (void) awakeFromNib {									[super awakeFromNib];
	if (logNsVcAccess) {
		id nibName33 = [@"" addN:20 F:@"%@", self.nibName33];
		[[@"" addF:@"InspecVc for nib '%@' awakeFromNib", nibName33] ppLog];
	}
}

#pragma mark 12. Inspectors
- (InspecVc *) addSubInspectorWithNib:nibName33; {
	 // self is the base pannel of the inspector

	 /// 1. Create the View Controller for the nib:
	InspecVc *rvNsVc	= [[[InspecVc alloc] initWithNibName:nibName33] autorelease];
	assert(rvNsVc, (@"failed to make rv NSViewController for nib %@", nibName33));

	 /// 2. Add to views:
 	NSView *baseView		= self.view;
	[baseView addSubview:rvNsVc.view];	// add new view pannel to view hierarchy

	 /// 3. Add to view controllers:
	[self.subNsVcs addObject:rvNsVc];

	 /// 4. Position the new frame:
	float height			= rvNsVc.view.bounds.size.height;
	self.contentBottom	   -= height; // decrease by rvNib's height
	rvNsVc.view.frameOrigin	= (NSPoint){0, self.contentBottom};// move new NsVs view

	id nibName20 = [@"" addN:20 F:@"%@", nibName33];
	if (logNsVcAccess)
		[[@"" addF:@"subInspectorFor:    '%@' view.bounds=%@", nibName20,
									NSStringFromRect(rvNsVc.view.frame)] ppLog];

	 /// 5. (SOMEDAY) reposition self's frame each time, and eliminate code outside loop...
	return rvNsVc;
}

 // Color the background of the window, specially for the selected one:
- (void) inspectorSelected:(bool)selected {
	[self.view.window setBackgroundColor:!selected?
	[NSColor controlColor]:
	[NSColor selectedControlColor]];
}


//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

  /// 1. The BidirNsV's (subclass of NSView) come here to register themselves.
 ///
- (void) registerView:aView; {
	if (!coerceTo(InspecV, aView) and !coerceTo(BidirNsV, aView))
		panic(@"Illegal type of aView");

	   /// register aView if it is the first encounter
	  /// (There seem to be duplicate entries in the .nib, unclear why 170103)
	 ///
	//assert(coerceTo(InspecVc, aView), (@"downstream presumes this!!!"));
XX	if ([self.buildBidirNsVs addObjectIfAbsent:aView])
		[[@"" addF:@"Added to %lx: aView (%@ *)%lx", (LUI)self, [aView className], (LUI)aView] ppLog];
}

   /// 2. Loading the Nib sets NSViewController (NsVc) .representedObject
  ///	  Take this opportunity to set up BidirNsV's
 ///
- (void) setRepresentedObject:repObject { [super setRepresentedObject:repObject];

	if (logNsVcAccess) {
		id nibName33 = [@"" addN:20 F:@"%@", self.nibName33];
		id cName = [self className];
		id obName = [repObject respondsToSelector:@selector(name)]?
					[repObject name]: [@"" addF:@"%lx", (LUI)repObject];

		[[@"" addF:@"InspecVc[nib:%@] representedObject=(%@ *)%@",
								nibName33, cName, obName] ppLog];
	}

	if (View *v = coerceTo(View, repObject))
	  //170103: Do for Part inspectors too!
		if (BundleTap *bundleTap = coerceTo(BundleTap, v.part)) {

			 // If BundleTap has targetBundle
			if (Bundle *targetBundle = bundleTap.targetBundle) {
				targetBundle = mustBe(Bundle, bundleTap.targetBundle);
				[[@"" addF:@"BundleTap '%@' has target bundle '%@'",
								bundleTap.fullName, targetBundle.fullName] ppLog];

				///////// ////////////// ////////////// ////////////// /////////
				/////////             HOOK UP BidirNsV SLIDERS		   /////////
				///////// ////////////// ////////////// ////////////// /////////

				 // Go through all registered BidirSliders:
				for (id aBidirNsV in self.buildBidirNsVs)
					if (coerceTo(BidirNsV, aBidirNsV))

						  //// BUILD OUT Each registered object:
						[aBidirNsV buildBidirViewWithTarg:targetBundle];

					else panic(@"");

				self.buildBidirNsVs 	= nil;	// remove traces of construction
			}
			else
				[[@"" addF:@"BundleTap '%@'s target bundle is nil", bundleTap.fullName] ppLog];
		}
}

#pragma mark 13. IBActions
- (IBAction) inspectorPushButton:(id)sender; {

	 /// Forward to the correct Part.
	Part *part = [self.representedObject part];			// to represented Part
	 /// Use the method defined as the button's title (Hack)
	NSString *methodName  = mustBe(NSString, [sender alternateTitle]);
//	 /// would rather have this
//	NSString *methodName2 = [self buttonMethod];
	SEL sel = NSSelectorFromString(methodName);			// method from name
	if ([part respondsToSelector:sel])
		[part performSelector:sel withObject:sender];
	else
		panic(@"Part %@ does not respond to method '%@'", part.fullName, methodName);

	return;
}

 // For GUI, initiates redraw
- (void) setViewViewGUI		:(bool)val;	{[self helper:&_viewView     :val:@"viewView"];		}
- (void) setViewPartGUI		:(bool)val;	{[self helper:&_viewPart     :val:@"viewPart"];		}
- (void) setviewCameraGUI	:(bool)val;	{[self helper:&_viewCamera   :val:@"viewCamera"];		}
- (void) setViewSimulatorGUI:(bool)val;	{[self helper:&_viewSimulator:val:@"viewSimulator"];}

 // ^drag viewView in .nib makes this
//- (IBAction)viewViewGUI2:(id)sender {
//}

- (bool) viewViewGUI;			{		return _viewView;				}
- (bool) viewPartGUI;			{		return _viewPart;				}
- (bool) viewCameraGUI;			{		return _viewCamera;				}
- (bool) viewSimulatorGUI;		{		return _viewSimulator;			}

- (void) helper:(bool *)loc :(bool)val :label; {
	*loc = val;
	[[@"" addF:@"helper: label %@ = %lx", label, (LUI)val] ppLog];
//	self.view.needsDisplay = true;

	[self.window makeKeyAndOrderFront:self];
	[self.window display];     // displayIfNeeded
	self.representedObject = self.representedObject;	// 170114 doesn't cause redisplay!!
	self.view.needsDisplay = true;
//	[self.view setNeedsDisplay:true];

//
////	self.targetView = self.targetView;		// 170106 WTF?!
//
//	 // OLD WAY (Pre 170101 restructure: Now self.targetView is gone.
////	[self inspectTarget:self.targetView];
}

#pragma mark 15. PrettyPrint
- (NSString *) pp:aux; {
	id rv = [super pp:aux];
	return [rv 	addF:@" target='??'"];
}



@end
