  //  AppController.h -- FactalWorkbench's app delegate C20130

#import <Foundation/Foundation.h>

@class SimNsWc;

@interface AppController : NSObject

	@property (nonatomic, retain) 		   SimNsWc  *simNsWc;
	  // One of these per OGL simulation window,
		// but currently there is only one OGL window supported

	@property (nonatomic, retain) IBOutlet NSMenu 	*sceneMenu;
	@property (nonatomic, retain) IBOutlet NSMenu 	*regressMenu;

	@property (nonatomic, assign) int 				curMenueTestNumber;
	@property (nonatomic, assign) int 				curRegressTestNumber;

NSScreen	*simsScreen();
NSRect		simsFrame(float nibsWidth);

- (void) 	closeSimWindow:sender;

void		ppNSScreens();
void		pp(NSScreen *screen);

@end
